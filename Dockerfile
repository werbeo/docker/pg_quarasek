FROM postgres:11.0
LABEL maintainer="dve@vergien.net" description="PostgreSQL for Quarasek"

RUN localedef -i de_DE -c -f UTF-8 -A /usr/share/locale/locale.alias de_DE.UTF-8
ENV LANG de_DE.utf8

COPY docker-entrypoint-initdb.d /docker-entrypoint-initdb.d
