--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.14
-- Dumped by pg_dump version 9.5.14

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: quarasek; Type: DATABASE; Schema: -; Owner: quarasek
--

CREATE DATABASE quarasek WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'de_DE.UTF-8' LC_CTYPE = 'de_DE.UTF-8';


ALTER DATABASE quarasek OWNER TO quarasek;

\connect quarasek

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: author; Type: TABLE; Schema: public; Owner: quarasek
--

CREATE TABLE public.author (
    id bigint NOT NULL,
    first_name character varying(255),
    last_name character varying(255) NOT NULL
);


ALTER TABLE public.author OWNER TO quarasek;

--
-- Name: database; Type: TABLE; Schema: public; Owner: quarasek
--

CREATE TABLE public.database (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(2000),
    create_date timestamp without time zone
);


ALTER TABLE public.database OWNER TO quarasek;

--
-- Name: external_reference; Type: TABLE; Schema: public; Owner: quarasek
--

CREATE TABLE public.external_reference (
    id integer NOT NULL,
    database character varying NOT NULL,
    reference character varying NOT NULL,
    publication_id integer NOT NULL
);


ALTER TABLE public.external_reference OWNER TO quarasek;

--
-- Name: external_reference_id_seq; Type: SEQUENCE; Schema: public; Owner: quarasek
--

CREATE SEQUENCE public.external_reference_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.external_reference_id_seq OWNER TO quarasek;

--
-- Name: external_reference_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: quarasek
--

ALTER SEQUENCE public.external_reference_id_seq OWNED BY public.external_reference.id;


--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: quarasek
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO quarasek;

--
-- Name: institution; Type: TABLE; Schema: public; Owner: quarasek
--

CREATE TABLE public.institution (
    id bigint NOT NULL,
    name character varying(255)
);


ALTER TABLE public.institution OWNER TO quarasek;

--
-- Name: journal; Type: TABLE; Schema: public; Owner: quarasek
--

CREATE TABLE public.journal (
    id bigint NOT NULL,
    name character varying(255),
    pages character varying(255),
    volume character varying(255),
    issn character varying
);


ALTER TABLE public.journal OWNER TO quarasek;

--
-- Name: keyword; Type: TABLE; Schema: public; Owner: quarasek
--

CREATE TABLE public.keyword (
    id bigint NOT NULL,
    keyword character varying(255) NOT NULL
);


ALTER TABLE public.keyword OWNER TO quarasek;

--
-- Name: keyword_publication; Type: TABLE; Schema: public; Owner: quarasek
--

CREATE TABLE public.keyword_publication (
    keyword_id bigint NOT NULL,
    publication_id bigint NOT NULL
);


ALTER TABLE public.keyword_publication OWNER TO quarasek;

--
-- Name: pdf_link; Type: TABLE; Schema: public; Owner: quarasek
--

CREATE TABLE public.pdf_link (
    id bigint NOT NULL,
    url character varying(1000) NOT NULL
);


ALTER TABLE public.pdf_link OWNER TO quarasek;

--
-- Name: platform; Type: TABLE; Schema: public; Owner: quarasek
--

CREATE TABLE public.platform (
    id integer NOT NULL,
    name character varying NOT NULL
);


ALTER TABLE public.platform OWNER TO quarasek;

--
-- Name: platform_id_seq; Type: SEQUENCE; Schema: public; Owner: quarasek
--

CREATE SEQUENCE public.platform_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.platform_id_seq OWNER TO quarasek;

--
-- Name: platform_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: quarasek
--

ALTER SEQUENCE public.platform_id_seq OWNED BY public.platform.id;


--
-- Name: publication; Type: TABLE; Schema: public; Owner: quarasek
--

CREATE TABLE public.publication (
    id bigint NOT NULL,
    cite_id character varying(255) NOT NULL,
    description character varying(2000),
    doi character varying(255),
    givd_id character varying(255),
    issue character varying(255),
    thesis_type character varying(255),
    title character varying(255) NOT NULL,
    turboveg_id character varying(255),
    tv2address character varying(255),
    tv2anmerkung character varying(255),
    tv2autor_kuer character varying(255),
    tv2bemerk character varying(255),
    tv2booktitle character varying(255),
    tv2db1 character varying(255),
    tv2db2 character varying(255),
    tv2hrsg character varying(255),
    tv2plotgvrd character varying(255),
    tv2plots_mv character varying(255),
    tv2public character varying(255),
    tv2published character varying(255),
    tv2publisher character varying(255),
    tv2ref_new character varying(255),
    tv2school character varying(255),
    tv2verw_florenwerke character varying(255),
    type character varying(255) NOT NULL,
    update_date timestamp without time zone,
    url character varying(1000),
    year integer NOT NULL,
    institution_id bigint,
    journal_id bigint,
    need_review boolean DEFAULT false
);


ALTER TABLE public.publication OWNER TO quarasek;

--
-- Name: publication_authors; Type: TABLE; Schema: public; Owner: quarasek
--

CREATE TABLE public.publication_authors (
    publications_id bigint NOT NULL,
    authors_id bigint NOT NULL,
    authors_key integer NOT NULL
);


ALTER TABLE public.publication_authors OWNER TO quarasek;

--
-- Name: publication_databases; Type: TABLE; Schema: public; Owner: quarasek
--

CREATE TABLE public.publication_databases (
    publications_id bigint,
    databases_id bigint
);


ALTER TABLE public.publication_databases OWNER TO quarasek;

--
-- Name: publication_files; Type: TABLE; Schema: public; Owner: quarasek
--

CREATE TABLE public.publication_files (
    publication_id bigint NOT NULL,
    files_id bigint NOT NULL
);


ALTER TABLE public.publication_files OWNER TO quarasek;

--
-- Name: publication_keywords; Type: TABLE; Schema: public; Owner: quarasek
--

CREATE TABLE public.publication_keywords (
    publication_id bigint NOT NULL,
    keywords_id bigint NOT NULL
);


ALTER TABLE public.publication_keywords OWNER TO quarasek;

--
-- Name: publication_platforms; Type: TABLE; Schema: public; Owner: quarasek
--

CREATE TABLE public.publication_platforms (
    publication_id integer NOT NULL,
    platforms_id integer NOT NULL
);


ALTER TABLE public.publication_platforms OWNER TO quarasek;

--
-- Name: schema_version; Type: TABLE; Schema: public; Owner: quarasek
--

CREATE TABLE public.schema_version (
    version_rank integer NOT NULL,
    installed_rank integer NOT NULL,
    version character varying(50) NOT NULL,
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


ALTER TABLE public.schema_version OWNER TO quarasek;

--
-- Name: id; Type: DEFAULT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.external_reference ALTER COLUMN id SET DEFAULT nextval('public.external_reference_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.platform ALTER COLUMN id SET DEFAULT nextval('public.platform_id_seq'::regclass);


--
-- Data for Name: author; Type: TABLE DATA; Schema: public; Owner: quarasek
--

COPY public.author (id, first_name, last_name) FROM stdin;
1	V.	Glavac
2	T.	Raus
5	W.	Hilbig
8	A.	Stemmler
11	H.	Dierschke
12	M.	Wolter
15	H.	Korsch
18	R.	Egeling
21	S.	Lütt
25	Jürgen	Dengler
28	B.	Ruthsatz
31	G.	Jeckel
34	K.	Grimme
37	M.	Lauer
40	T.	Meier-Schomburg
46	R.	Baufeld
49	C.	Wiebe
52	R.	Hundt
55	H.	Wollert
59	K.	Kuhn
62	E.	Sauer
65	K.	Baeumer
68	P.	Boeker
72	R.	Nacke
73	M.	Schmidt
86	E.	Glotz
89	S.	Görs
92	P.	Haffner
95	J.	Kiem
98	W.	Libbert
101	C.	Seifert
107	Michael	Succow
114	J.	Bartsch
115	M.	Bartsch
121	B.	Dierssen
122	K.	Dierssen
125	A.	Vogel
128	E.	Schröder
131	J.	Herrmann
134	R.	Marstaller
137	E.	Kaiser
140	E.	Klapp
143	U.	Knüver
146	W.	Lötschert
149	K.	Markgraf
152	V.	Heidenreich
155	D.	Bönsel
156	P.	Schmidt
159	T.	Borsch
162	G.	Lang
165	R.	Kirsch-Stracke
168	T.	Gregor
171	M.	Denk
172	R.	Wittig
177	D.	Teuber
180	C.	Wedra
183	M.	Förster
184	C.	Hepting
187	H.	Hofmeister
190	B.	Krass
193	M.	Popken
194	W.	Licht
197	K.	Böger
205	W.h.	Diemont
208	L.	Reichhoff
209	W.	Böhnert
213	R.	Knapp
220	C.	Högel
223	A.	Rech
226	A.	Rühl
229	W.	Haffner
232	J. J.	Kieckbusch
235	P.	Schwartze
241	M.	Wulf
246	S.	Pflume
249	S.	Kruse
252	W.	Trautmann
257	O.	Denz
260	F. K.	Hartmann
261	G.	Jahn
266	A.	Krause
272	G.	Krauss
275	H.	Meusel
278	C.	Peppler
281	F.u.	Pieper
286	R.	Wendlandt
289	E.	Weinert
290	M.	Gulich
294	G.	Stöcker
299	P.	Schönfelder
302	D.	Zacharias
305	H.	Budde
308	W.	Schmidt
311	\N	Biologische Station Kreis Steinfurt
314	P.	Thomas
317	M.	Borck
320	K.	Meyer
325	G.	Kunzmann
328	W.	Beer
331	J.	Wattendorff
334	R.	Gradmann
337	M.	Beiter
340	A.	Hornung
343	H.	Roweck
344	V.	Schweikle
347	J.	Kuhn
348	W.	Kramer
351	A.	Sundermeier
358	J.	Genser
361	K.	Gauckler
364	C.	Altehage
369	M.	Altrock
377	A.	Arndt
380	U.	Asmus
383	A.	Baumgarten
386	Erwin	Bergmeier
390	B.	Nowak
393	\N	Bock
399	W.	Braun
403	R.	Schubert
406	K.	Buchwald
409	R.	Buchwald
412	W.	Brockhaus
415	R.	Büker
422	H.	Engel
425	E.	Burrichter
428	H.	Coenen
441	A.	Otte
442	H.	Nordmann
451	K.	Dransfeld
454	E.	Fischer
457	T.	Flintrop
460	E.	Foerster
463	T.	Franke
466	H.	Freitag
469	U.	Körtge
472	H.	Fritsch
475	Erhard Th.	Fröde
478	F.	Fukarek
481	K.-h.	Gaertner
484	C.	Ganzert
487	A.	Garniel
118	Josias	Braun-Blanquet
396	U.-O. v.	Borstel
56	Peter	Bolbrinker
374	M. R.	Amani
238	R.	Bürger-Arndt
269	Hans-Dieter	Knapp
71	Thilo	Heinken
490	S.	Gilcher
499	Th.	Müller
502	S.	Götz
104	E. K.	Spörle
43	Connie	Becker
24	Hauke	Drews
503	G.	Riegel
506	A.	Grüttner
509	F. J.	Weicherding
513	D.	Hanspach
516	S.	Harm
521	K.	Hauser
524	R.	Hausfeld
529	P.	Hillmann
532	A.	Hölzer
537	H.	Hollmann
540	K.	Horst
564	G.	Ilschner
567	L.	Jeschke
570	F.	Jonas
573	K.	Jöns
578	B.	Bohne
579	H.-J.	Bothmer
580	F.-J.	Grieger
581	H.-G.	Kmoch
584	G.	Klemm
588	A.	Kleyda
591	K.	Kloss
610	H.	Köhler
613	P.	Konczak
621	J.	Pusch
622	K.-J.	Barthel
625	S.	Hänel
626	F.	Müller
629	B.	Fleischer
644	H.	Krisch
651	L.	Kuhn
654	G.	Lein-Kottmeier
655	H.	Oertel
658	H.	Lenski
665	W.	Lippert
670	D.	Lüpnitz
679	E.	Manz
684	K.	Meisel
687	B.	Menke
693	T.	Müller
699	W.	Fischer
710	E.	Niemann
713	K.	Nigge
722	B.	Schulz
729	E.	Oberdorfer
740	R.	Oppermann
743	J.	Reichholf
744	J.	Pfadenhauer
747	A.	Pardey
767	M.	Petrak
770	\N	Dorda
773	Jörg	Pfadenhauer
776	H.	Pfalzgraf
779	H.	Pfeiffer
782	J.	Pfrogner
785	G.	Philippi
798	W.	Pietsch
805	J.	Pötsch
808	R.	Pollok
811	E.-W.	Raabe
816	H.	Reichert
819	K.	Reidl
822	A.	Reif
825	T.	Baumgartl
826	I.	Breitenbach
829	B.	Ribbe
832	W.	Richter
837	M. V.	Rochow
840	D.	Rodi
847	G.	Rosenthal
850	G.	Rosskopf
855	F.	Runge
858	M.	Wichmann
859	M.	Burkart
866	H.	Scheel
869	V.	Scherfose
872	G.	Schüchen
875	A.	Schumacher
886	W.	Schumacher
889	A.	Schwabe
892	A.	Kratochwil
895	K. F.	Schreiber
898	M.	Schwickerath
907	P.W.	Schwickert
910	O.	Sebald
915	P.	Seibert
920	E.	Siede
923	M.	Smollich
924	U.	Bernert
927	G.	Spatz
930	B.	Speidel
933	M.	Speier
936	S.	Springer
939	A.	Steinführer
942	H.	Sukopp
945	M.	Trentepohl
948	W.	Türk
961	I.	Ullmann
969	R.	Urban
972	G.	Verbücheln
977	F.	Vollmar
980	H.	Vollrath
985	H.	Walentowski
988	K.	Walther
1001	M.	Warneke
1004	H.e.	Weber
1013	M.	Weissbecker
1016	W.	Welss
1019	W.	Westhus
1022	H.	Wey
1025	G.	Wiegleb
1030	W.	Winterhoff
1033	B.	Wittig
1038	G.	Wolf
1043	O.	Haberkost
1046	C.	Janssen
1050	W.a.	Zahlheimer
1053	H.	Ahrens
1054	M.	Brinkoch
1055	R.	Gohde
1056	F.	Jork
1057	B.	Kartes
1058	P.	Lauser
1059	D.	Lohse
1060	M.	Schafmeister
1061	U.	Siekmann
1062	W.	Wette
1070	C.	Andres
1073	J. J.	Barkman
1078	U.	Bartram
1081	A.	Bartsch
1082	U.	Wegener
1083	E.	Wesarg
1086	J.	Baumgart
1100	K.	Beinhauer
1103	J.	Berlin-Wolf
1106	J.	Blank
1114	R.	Bornkamm
1123	W.	Eber
1093	Thomas	Becker
545	K. H.	Hülbusch
1131	F. H.	Jork
1141	S.	Zerbe
1142	A.	Brande
1143	F.	Gladitz
1146	M.	Bultmann
1157	G.	Butzke
676	J. L.	Lutz
966	J. O.	Först
673	Ute	Jandt
616	Werner	Konold
690	F. H.	Meyer
1047	Dieter	Brandes
585	Peter	König
1160	M.	Czygan
1161	B.	Scholtissk
1162	M.	Wehner
1165	I.	Dahlmann
1167	W.	Hardes
1168	B.	Kornelius
1175	C.	Rixen
1182	S.	Knoop
1187	K.	Dittrich-Bröskamp
1193	A.	Eichholz
1196	H.	Eichler
1199	S.	Ernst-Moritz
1205	A.	Fischer
1208	M.	Flecks
1211	A.	Frede
1216	H.	Geringhoff
1219	A.	Schlage
1220	R.	Schlage
1223	A.	Gross
1224	W.	Illig
1227	S.	Haack
1230	S.	Häcker
1233	J.	Halfmann
1236	U.	Hecker
1239	K. V. D.	Heide
1245	W.	Heinrich
1248	K.	Helmecke
1253	U.	Henze
1254	T.	Schlufter
1258	D.	Hessler
1261	G.	Hofmann
1266	K.	Holländer
1269	M.	Hölzel
1272	D.	Horch
1275	M.	Hullen
1278	A.	Ihl
1281	C.	Jäger
1294	J.	Kilian
1320	W.	Koch
1340	B.	Langenhorst
1343	K.	Lauckner
1346	L.	Straßl
1349	J.	Bammert
1352	H.	Lienenbecker
1355	W.	Lohmeyer
1373	R.	Moebus
1376	C.	Möller
1379	Bodo Maria	Möseler
1384	A.	Nagler
1387	F.	Neuenroth
1390	G.	Neuwirth
1395	H.	Opitz
1402	T.	Pfeiffer
1407	H.	Pless
1412	B.	Protz
1415	S.	Raehse
1418	R.	Reding
1421	F.-E.	Redslob
1428	H.	Rödel
1433	\N	Rögener. J.
1434	U.	Benz
1438	\N	Rogge
1442	T.	Rohde
1445	U.	Rohde
1448	O.	Röhlig
1451	J.	Rost
1454	S.	Rost
1457	H. V.	Rüden
1460	F.	Rüther
1463	C.	Scharf
1466	H.	Schlüter
1473	A.	Schneeweiss
1476	K.	Schneider
1479	M.	Schorr
1486	W.	Schubert
1489	H.	Schwochow
1492	W.-h.	Sommer
1495	H. V.	Suchodoletz
1498	M.	Tigges
1504	O. H.	Volk
1507	C.	Voll
1510	I.	Vollmer
1513	T.	Weber
1518	A.	Weidner
1524	N.	Wellnitz
1527	\N	Wersche Gmbh
1533	M.	Witschel
1546	D.	Zehm
1549	H.-j.	Zündorf
1560	P.	Blosat
1565	F.	Dudeck
1568	A.	Gerlach
1574	T.	Gönnert
1584	J.	Jäger
1587	S.	Jahn
1600	H.	Künne
1613	L.	Schröder
1649	G.	Ludwig
1657	\N	F.a. Obereimer
1667	R.	Petermann
1684	R.	Seifarth
1689	H.	Streitz
1726	D.	Schreiber
1729	K.	Megner
1732	M.	Weckesser
1735	K.	Kohls
1738	A.	Mölder
1741	Y.	Soyka
1744	B.	Lambertz
1747	R.	Ermert
1750	L.	Ebrecht
1753	B.	Albrecht
1756	D.	Garbitz
1759	S.	Melcher
1762	E.	Happe
1765	C.	Fischer
1771	C.	Franke
1776	L.	Steiner
1782	A.	Scamoni
1789	A.	Golisch
1792	H.	Bracht
1793	C.	Hobohm
1796	F.	Apffelstaedt
1800	I.	Lisbach
1808	H.	Ellenberg
1823	P.	Wolff
1828	A.	Siegl
1831	G.	Warthemann
1834	S.	Krause
1837	E.	Schaaf
1840	H. G.	Michiels
1843	R.	Straußberger
1850	J.	Brand
1853	G.	Luckwald
1864	B.	Gehlken
1867	I.	Hetzel
1779	Christian	Ahrns
1797	K.-G.	Bernhard
1134	Helge	Bruelheide
1854	\N	et al.
1202	Christiane	Evers
1868	R.	Fuchs
1869	P.	Keil
1870	T.	Schmitt
1873	A.	Beer
1878	J.	Schröder
1881	I.	Wenz
1887	B.	Schmitt
1891	F.	Wulf
1894	A.	Heerde
1166	H.-J.	Hahn
1597	E.-J.	Klauck
1325	Dieter	Korneck
1801	Cord	Peppler-Lisbach
1877	Maike	Eisenberg
1895	A.	Gnüchtel
1900	J.	Nitzsche
1903	M.	Brändel
1908	C.	Rüther
1909	J.	Klotz
1921	M.	Bernhardt-Römermann
1922	\N	Schmidt. W.
1925	E.	Füllekrug
1928	F.	Schuhwerk
1933	Andreas	Bettinger
1936	\N	Durka
1937	\N	W.
1938	A.	Hemp
1939	K.	Löblich-Ille
1942	S.	Walther
1945	D.	Herter
1950	A.	Kessling
1953	A.	Sendtko
1956	F.	Zintl
1968	C.	Hettwer
1974	B.	Krause
1981	K.s.	Romahn
1984	E.	Rexer
1987	T.	Merz
1990	T.	Paul
1993	A.	Sieben
1996	A.	Klocke
1999	M.	Nitschke
2002	M.	Scherrer
2008	R.	Suck
2013	J.	Messerschmidt
2016	W.	Klix
2023	W.	Hochhardt
2026	K.	Bürger
2029	H.	Butzke
2034	S.	Walter
2039	S.	Meisberger
2046	G.	Matzke
2054	I.	Kühn
2057	G.	Ellwanger
2060	M.	Koch
2069	M.	Gulski
2072	H.	Diekjobst
2075	F.	Bode
2078	W.	Krause
2088	B.	Beinlich
2089	W.	Klein
2092	W.	Goebel
2095	F.	Wöllner
2100	R.	Hierlmeier
2103	J.	Duty
2106	J.	Merkel
2109	T.	Kaiser
2112	H.-j.	Schuster
2117	U.	Meisterhans
2120	A.	Raaber
2123	A.	Faber
2127	S.	Künkele
2128	E.	Seitz
2142	T.	Wolf
2145	H.	Linhard
2146	C.	Linhard
2147	I.	Linhard
2151	Klaus	Dierßen
2158	G.	Kaule
2161	H.	Kersberg
2164	T.	Breunig
2167	Erich	Oberdorfer
2176	M.	Nebel
2179	M.	Kassebaum
2184	H.	Langer
2187	U.	Meyer
2190	N.	Müller
2193	W.	Odzuck
2198	A.	Scheunert
2201	D.	Peltzer
2204	M.	Lemke
2207	T.	Kompa
2210	A.	Emmerich
2213	W.	Schnelle
2217	\N	Ingenieurbüro Wasser und Umwelt
2233	\N	Bundesanstalt für Vegetationskunde (verschied. Autoren)
2236	G.	Ostermann
2241	W.	Wahrenburg
2250	A.	Bresinsky
2261	B.	Hachmöller
2272	W.	Zielonkowski
2275	S.	Liepelt
2282	M.	Kästner
2295	F.j.	Wagner
2298	W.	Hakes
2303	W.	Vigano
2310	H.	Roll
2319	W.	Meyer
2322	H.	Möller
2325	\N	Luckwald
2330	A.	Bröcker
2333	\N	Projektgruppe Halbtrockenrasen (xöxter)
2336	T.	Hagen
2339	A.	Leonhardt
2344	Illona	Leyer
2354	T.	Roßkamp
2357	E.	Spranger
2360	\N	Springer. S.
2365	M.	Von Rochow
2371	M.	Haass
2374	A.	Müller
2382	U.	Amarell
2385	H	Augustin
2390	R.	Boiselle
2393	G.	Brunner
2394	R.	Lindacher
2397	M.	Bushart
2398	P.	Leupold
2403	M.v.	Gaisberg
2408	G.	Grosse-Brauckmann
2409	B.	Streitz
2417	K.	Horn
2420	K.	Hueck
2423	G.	Kersting
2434	H.	Kürschner
2435	S.	Runge
2438	B.	Löffler
2442	J.	Marckardt
2453	W.	Pollmann
2083	K. H.	Biederbick
1874	Jörg	Ewald
1971	Karsten	Wesche
2139	Reinhard	Doll
2454	J.	Lethmate
2457	E.	Aichinger
2460	\N	Balazc
2476	K.	Lewejohann
2479	A.	Dutoit
2482	V.	Giacomini
2483	S.	Pigniatti
2490	B.	Pawlowskki
2491	M.	Sokolowski
2492	K.	Wallisch
2495	D.	Puscaru
2498	W.	Szafer
2503	W.	Besler
2506	Ch.	Düring
2507	U.	Wierer
1915	Thomas	Fartmann
2126	G. W.	Brielmaier
2051	A.-K.	Jackel
1916	Norbert	Hölzel
2368	G.	Buck-Feucht
1959	H. C.	Kläge
2347	Otti	Wilmanns
1912	Monika	Partzsch
2518	R.	Feldner
2521	R.	Frankl
2524	H. M.	Freiberg
2529	W.	Herter
2532	S.	Hofmann
2540	K.	Ketterer
2545	W.	Lorenz
2548	H.	Mages
2559	N.	Puchner
2568	E.	Simmerding
2571	F.	Still
2574	M.	Storch
2579	K.	Thiele
2582	M.	Wührer
2585	Th.	Zanker
2588	J.	Schrautzer
2589	D.	Jansen
2590	M.	Breuer
2591	O.	Nelle
2600	C.	Bach
2603	A.	Dörpinghaus
2606	M.	Dörsing
2609	D.	Ferber
2612	I.	Grüner
2615	R.	Guntermann
2618	S.	Hallmann
2621	L.	Hauswirth
2625	D.	Hinterlang
2630	M.	Jaletztke
2633	L.	Keller
2639	K.	Lauber
2642	B.	Linnemann
2648	A.	Poetschke
2651	A.	Poth
2654	M.	Ratai
2657	P.	Schneiders
2660	P.	Schnell
2663	N.	Süsselbeck
2666	A.	Tobias
2669	\N	Verbücheln
2673	M.	Wilhelm
2676	J.	Windisch
2679	K.	Wittjen
2682	E.	Zippel
2689	M.	Scheuerer
2694	F.	Schaumann
2697	C.	Stiersdorfer
2702	H.	Targan
2707	B.	Müller
2708	E.	Obermeier
2717	H.	Zeidler
2718	R.	Straub
2721	T.	Zum Felde
2740	M.	Beier
2743	C.	Bruns
2748	U.	Ebben
2751	I.	Henrion
2757	M.	Junglas
2762	H.	Leippert
2766	R.	Ortmanns
2769	\N	Papajewski
2775	B.	Pleßmann
2781	S.	Rauschert
2798	H.	Höper
2806	W.	Dinter
2809	U.	Eskuche
2817	H.	Von Glahn
2820	Werner	Härdtle
2825	K.	Hartmann
2834	P.	Detzel
2835	\N	Rtske
2836	M.	Krug
2837	T.	Ludemann
2841	A.	Schwabe-Braun
2844	W.	Wagner
2849	S.	Nawrath
2854	\N	Lanuv Nrw
2857	H.-g.	Täglich
2866	T.	Sperle
2888	E.	Rennwald
2925	U.	Sayer
2928	\N	Flintrop. T.
2992	M.	Karrer
2993	B.	Mies
2994	R.	Ltsch
2997	J.	Giegerich
3000	W.	Böcking
3007	I.	Grafe
3010	A.	Heinken
3013	A.	Kleinsteuber
3016	M.	Peintinger
3021	B.	Redecker
3030	E.	Schnelle
3033	R.	Rombach
3036	F.	Holst
3037	W.	Kintzel
3046	A.	Krohne
3051	E.	Zezschwitz
3054	S.	Meisel-jahn
3065	W.	Hüppe
3068	C.	Rasmus
3071	D.	Rödel
3074	C.	Seewald
3077	W.h.	Sommer
3080	K.	Taux
3085	A.	Vedder
3088	J.	Wattendorf
3091	H.	Böttcher
3095	G.	Plass
3098	S.	Schneider
3105	Ortrun	Krausch
3110	Lutz	Dietrich
3113	O.	Koßak
3118	K.	Baumann
3121	A.	Federschmidt
3124	F.	Kommraus
3127	A.	Krumbiegel
3130	A.	Lebender
3133	B.	Lange
3136	E.	Große
3141	H.	Quitt
3146	B.	Schlag
3151	H.	Spangenberg
3154	L.	Weigl
3157	S.	Wolf
2860	\N	Lwf - Bayerische Landesanstalt für Wald- und Forstwirtschaft
3040	K. H.	Knörzer
3160	L.	Meierott
3165	K.	Strahl
3168	B.	Suck
3169	R	Suck
3174	E.	Stückl
3179	G.	Hetzel
3185	P.	Eggensberger
3188	A.	Krüss
3204	A.	Kölbel
3210	U.	Becker
3215	S.	Wotke
3216	W.	Bücking
3221	P.	Maul
3226	H.	Bültmann
3233	H.	Krämer
3236	M.	Wittig
3239	V.	Glunk
2812	H. W.	Finkeldey
3043	W. U.	Kriebitzsch
3027	E.-G.	Mahn
3207	K.-H.	Mayer
2778	Richard	Pott
2636	Martin	Kleikamp
2645	E. M.	Lisges
3092	A. K.	Ranft
2772	Michael	Peters
3240	M.	Rudner
3243	K.	Fleischer
3244	M.	Streitberger
3249	K.	Süss
3253	R.	Mast
3260	K.-h.	Lenker
3261	V.	Tokhtar
3264	D.	Pretzell
3269	\N	E.
3272	M.	Diekmann
3273	M.	Bartels
3276	V.	Hammes
3277	D.	Remy
3280	D.	Schulze
3285	P.	Poschlod
3288	Michael	Kleyer
3295	F.	Schmale
3298	H.	Kelm
3301	K.	Kellner
3302	J.	Langbehn
3307	U.	Springemann
3308	P.	Meyer
3310	G.	Karste
3311	H.	Thiel
3312	T.	Schultz
3313	A.	Marten
3314	H.-b.	Schikora
3315	J.	Schmidt
3316	T.	Meineke
3317	W.	Wimmer
3318	O.	Wüstemann
3319	A.	Späth
3320	F.	Raimer
3327	A.	Gerth
3328	D.	Merten
3329	H.	Baumbach
3332	\N	Nationalparkverwaltung Harz
3335	B.	Weber
3340	G.	Ehmer
3343	D	Zuther
3346	B.	Winter
3349	F.	Zinner
3352	T.	Suk
3355	N.	Kühn
3358	J.	Faust
3366	I.	Dannenberg
3377	F.	Kretzschmar
3380	I.	Buchmann
3388	S.	Huck
3395	U.	Jäger
3398	S.	Lemme
3402	R.	Waldhardt
3403	D.	Simmering
3406	T.	Kundernatsch
3412	A.	Friedmann
3417	I.	Starke-Ottich
3418	O.	Tackenberg
3419	G.	Zizka
3424	P.	Lang
3425	M.	Frei
3428	\N	Groß
3429	\N	M.
3432	T.	Täuber
3440	A.	Matthews
3455	J.	Pallas
3460	C.	Roscher
3465	F.	Zickermann
3468	A.	Huwer
3471	A.	Schessl
3474	F.	Rebele
3477	S.	Altenfelder
3478	U.	Raabe
3479	H.	Albrecht
3482	K.	Henning
3491	M.	Scheideler
3492	M.	Smolis
3505	E.	Ludewig
3508	A.	Wagner
3511	P.	Sander
3516	K.	Ludewig
3517	L.	Korell
3518	F.	Löffler
3519	E.	Mosner
3520	M.	Scholz
3521	K.	Jensen
3524	K.	Arendt
3536	J.	Behrens
3542	P.	Bliss
3545	E.	Bochnig
3570	S.	Borowiec
3571	B.	Kaussmann
3574	J.	Kudoke
3575	T.	Lesnik
3580	G.	Bräuer
3583	H. D.	Busecke
3636	H.	Doll
3637	H.	Kaschube
3640	E.	Hemke
3651	M.	Stegemann
3654	G.	Schmidt
3657	H.	Fiedler
3658	H.	Pankow
3663	Th.	Fröde
3670	U.	Voigtländer
3673	B.	Funk
3678	W.	Gerhardt
3681	M.	Giersberg
3684	P.	Gollub
3687	P.	Gutte
3690	U.	Hauke
3693	A.	Hemke
3696	H.	Henker
3703	S.	Hilbrech
3704	K.	Lembke
3709	H.	Holdack
3726	Ch.	Kretschmer
3729	A.	Martin
3732	E.	Hoppe
3735	H.	Hoyer
3736	H.	Prill
3748	H.	Hurtig
3749	G.	Schulze
3752	F.	Jacobs
3755	Lebrecht	Jeschke
3539	Christian	Berg
3533	R.-P.	Bartz
3383	H. W.	Bennert
3498	Volker	Dittmann
3309	M.	Schmidt
3812	F.	Erdmann
3815	H.	Schmidt
3818	A.	Murr
3845	G.	Klafs
3848	H.	Klaus
3851	J.	Kleinke
3854	C.-l.	Klemm
3885	E.	Hacker
3929	K.	Krull
3946	M. V.	Lampe
3949	Elsbeth	Lange
3954	A.	Lindner
3959	S.	Lohr
3964	F.	Mattick
3967	U.	Meßner
3974	B.	Hülsmeyer
3977	W.	Mahnke
3980	R.	Pulz
3983	P.	Spittler
3250	C.	Storm
3450	W. R.	Müller-Stoll
3401	O.	Ginzler
3897	Dietrich	Kopp
3900	Hans-Dieter	Krausch
3267	Christine	Schmiege
3411	Timo	Conradi
3986	G.	Passarge
4030	W.	Poltz
4033	A.	Precker
4039	R.	Rehbein
4064	D.	Schmidt
4069	R.	Schmidt
4072	K.	Kaiser
4075	R.	Schöneich
4078	T.	Schoknecht
4081	S.	Slobodda
4094	H.-j.	Rudnitzki
4097	H.	Sluschny
4098	G.	Matthes
4127	H. D.	Szameitat
4130	S.	Thomas
4133	L.	Treichel
4138	K.	Voderberg
4141	E.	Fröde
4164	Th.	Weber
4167	K. A.	Wegener
4170	K.a.	Wegener
4225	N.	Krauss
4230	E.	Zabel
4233	P.	Polke
4236	H.	Abel
4237	M.	Hippke
4238	A.	Rieger
4242	\N	AG_Geobotanik Mecklenburg-Vorpommern
4246	J.	Berg
4250	H.-ch.	Thiess
4258	M.	Teppke
4261	J.	Bohlke
4262	G.	Hansen
4272	A.	Bolman
4273	B.	Nijhof
4276	K.	Brandt
4282	N.	Brielmann
4300	T.	Richter
4303	A.	Dücker
4309	P.	Duks
4319	F.	Griesel
4323	G.	Grünbauer
4324	T.	Cheung
4342	G.	Hemprich
4343	M.	Schumann
4350	I.	Henker
4354	C.	Herden
4355	B.	Jürgensen
4370	T.	Jacob
4374	C.	Jaschhof
4377	D.	Kästner
4381	H.	Koch
4388	D.	Kinder
4389	D.	Mittendorf
4400	K. W.	Kirsch
4407	N.	Krauß
4411	A.	Kremp
4414	I.	Kurschat
4417	M.	Lange
4420	Ch.	Linke
4424	B	Machatzki
4431	N.	Meyer
4434	B.	Mickel
4438	A.	Mohr
4445	K.	Hofmann
4452	\N	Naturschutzgesellschaft Hiddensee Und Boddenlandschaft
4459	J.	Overbeck
4462	Ch.	Paulson
4463	G.	Lennartz
4467	R.	Raskin
4474	P.	Tautz
4480	R.	Pivarci
4502	C.	Rudat
4505	T.	Schacht
4508	W.	Scheller
4512	U.	Schlüter
4516	M.	Schöttelndreier
4519	A.	Spieß
4526	J.	Stuhr
4530	G.	Uhle
4578	H.	Wefing
4582	W.	Wiehle
4586	Wissenschaftlicher Studen	Lamarck
4590	S.	Woidig
4630	\N	Ibs_schwerin
4637	T.	Geisel
4640	M. Von	Lampe
4643	K.	Schäfer
4646	Maike	Isermann
4655	R.	Mordhorst
4666	V.	Rowinsky
4667	U.	Neumann
4680	I.	Cöster
4681	B.	Degen
4682	V.	Thiele
4686	D.	Langrock
4689	J.	Schmiedel
4703	\N	Al. (biota Gmbh)
4721	S.	Seiberling
4734	T.	Fröde
4748	Tanja	John
4757	A.	Rösler
4762	P.	Fischer
4784	M.	Knapp
4790	S.	Roth
4793	E.	Mey
4805	K.	Vegelin
4815	Christian	Blümel
4835	K.	Kiphuth
4483	Tom	Polte
4494	Birgit	Litterski
4778	Annett	Folkowski
4773	Bärbel	Bohnacker
4770	Heike	Barth
4781	Florian	Jansen
4820	Landesarbeitsgemeinschaft	Vegetationskunde MV
4798	Wolfgang	Ostendorp
4751	Almut	Spangenberg
4720	Michael	Manthey
3894	U. v.	Köck
4449	Almut	Mrotzek
4286	Ute	Clausnitzer
4316	Ulrich	Fischer
4855	H.	Lemke
4862	A.	Friedel
4869	T.	Dietrich
4874	K. S.	Romahn
4885	C.	Linke
4888	Margret	Kießlich
4894	Gesine	Kratzert
4899	Ch.	Hoffmann
4914	H.	Krieger
4946	Arndt	Müller
4954	P.	Scharf
4957	H.	Gottschling
4960	M.	Fitschen
4966	S.	Stolze
4977	Ch.	Brinkmann
4984	C.	Sütering
4987	A.	Neumann
4990	H.	Rudolphi
4999	D.	Bossert
5005	I.	Herzberg
4523	Silke	von Stamm
4980	Thomas	Hinz
4333	Thorsten	Harder
4950	Andreas	Kaffke
3987	Harro	Passarge
4865	Margret	Seidenschnur
4879	Jörg	Petersen
4868	Jan-Hinnack	Schwarz
5008	Susanne	Jörns
5011	Moritz	Klußmann
5014	U.	Labitzke
5015	T.	Peschel
5018	A.	Laube
5021	J.	Lein
5027	S.	Romer
5030	Ch.	Ruth
5036	H.	Manthe
5039	Ch.	Lenk
5042	L.	Landgraf
5048	F.	Mikosch
5051	A.	Münich
5054	M.	Wüst
5063	S.	Skriewe
5068	H.	Schurbohm
5074	J.	Schulz
5077	Claudia	Schönemann
5080	A.	Schlundt
5083	K.	Schleicher
5090	B.	Hofemeister
5093	U. M.	Helke
5096	S.	Kiputh
5108	J.	Päzolt
5111	Romy	Zimmermann
5116	W.	Zahn
5121	K.	Gräf
5124	\N	Praktikum Wackerow
5143	A.	Middelschulte
5146	U.	Münchmeyer
5151	T.	Lemke
5152	\N	Hobbhahn
5155	C.	Tröltsch
5163	Ch.	Drafehn
5166	\N	Doing
5169	W. M. J.	Evers
5172	A. E.	Jansen
5175	A.	Noirfalise
5181	M. A.	Roisin
5184	N.	Sougnez
5187	M.	Tanghe
5194	\N	Vollmar
5197	R.	Markowski
5200	I.	Hensen
5201	M.	Kentrup
5204	D.	Huntke
5207	R.	Dubletten Knapp
5210	\N	OHNE BIBLIOREFERENZ
5215	\N	LANUV - Braun-Blanquetskala
5217	\N	LANUV - Braun-Blanquet (old)
5219	Martin	Freitag
5220	Wanja	Mathar
5221	Andrei A.	Yurtaev
5224	Claudia	Oehmke
5227	\N	AG Geoboatnik MV
5231	Katja	Jost
5232	Katja	Resagk
5238	Reinhild	Kindermann
1190	Martina	Egersdörfer
200	Reinhold	Tüxen
2221	Anja	Abdank
5158	Annett	Adler
5240	Juliane	Friemel
5241	Constanze	Tröltsch
4981	Ronny	Goldberg
5243	Frank	Mirschel
5244	Anett	Stolte
4971	Annette	Beitz
4993	Friedrich	Hacker
4937	Katja	Diedrich
4943	Meik	Neubauer
5246	M.	Neubauer
5247	Isabell	Kulb
4787	Roy	Latsch
5060	Jens	Tesmer
5248	Stefanie	Woelki
4974	Ramona	Mickel
4949	Ingo	Koska
4965	Alexander	Seuffert
4996	Frank	Effenberger
5024	Birger	Buhl
5071	Karsten	Schulz
5086	Juliane	Schäfer
5087	Falko	Hornschuch
5127	Dieter	Gremer
5250	Nina	Malkomes
5251	Tim	Steinhard
5252	Stefanie	Raabe
5253	Christian	Schröder
5254	Claudia	Schröder
5255	Kristian	Peters
5256	Anne	Petzold
5257	Christin	Kunze
5258	Katrin	Wiesen
1109	Udo	Bohn
5260	Karoline	Cremer
4925	U.	Berger-Landefeldt
5057	Tiemo	Timmermann
5002	Thomas	Beil
5266	Antje	Birger
4891	Annette	Köber
4848	Katrin	Brozio
4940	Frauke	Hennek
5271	Jana	Bürger
5273	Bärbel	Gerowitt
5274	Friederike	de Mol
5275	Christoph	von Redwitz
5276	Kristin	Hanzlik
5281	Stefan	Horn
696	W. F.	Müller
3437	Ute	Döring-Mederake
85	K. H.	Grosser
4810	AG Geobotanik	Mecklenburg-Vorpommern
5045	Dierk	Michaelis
5105	Anja	Prager
5283	M.	Manegold
5288	J.	Glaeser
5290	U.	Endres
5297	M.	Hellwig
5307	D.	Wiesner
5310	I.	Wagner
5313	E.	Rückerte
5316	A.	Mayer
5321	L.	Murmann-Kristel
5324	H.	Rehder
5329	B.	Küspert
5336	A.	Wörz
5341	C.	Damm
5344	A.	Weiskopf
5347	S.	Woike
5358	V.	Blüml
5361	P.	Gausmann
5368	C.	Morkel
5375	B.	Kubitz
5378	E.	Breitschwerdt
5380	R.	Tüxen
5382	D.	Haese
5385	Carsten	Hobohm
5388	D.	Metzing
5389	H.	Kuhbier
5390	B.	Küver
5393	W.	Böckelmann
5396	A.	Gerhardt
5406	F.	von Ahnen
5407	A.	Röttger
5410	M.	Schnaidt
5413	K.	Kiffe
5235	Wolfgang	Schmidt
5285	Dominic	Hopp
5178	E.	Meijer-Drees
5421	H.E.	Weber
5424	A	Schwabe
5426	K.H.	Kreeb
5431	P.	Janiesch
5434	M.	Kinder
5437	U.	Klinger
5440	E.J.	Weeda
5443	Martina	Mühl
5446	C.	Ringer
5451	W.	Christiansen
5460	V.	Westhoff
5463	H.	von Glahn
5470	J.	Kahle
5475	A.	Kamm
5480	O	Beinker
5483	J.	Eigner
5486	A.	Heykena
5489	P.	Wiemann
5490	W.	Domke
5497	A.	Hartlieb
5500	Gisela	Warnecke
5503	O.	Klement
5508	M	Wolbers
5511	E.W.	Raabe
5514	Janina	Spalke
5517	Volker	Scherfose
5522	C.	Müller
5525	Kerstin	Hansen
5528	Jan	Riediger
5531	R.	von Lemm
5532	D.	Wolff
5534	Fritz	Runge
5541	D.	Hahn
5544	A.	Fromke
5547	R.	Pott
5551	S.	Arens
5554	Rosemarie	Grella
5557	E. & Roßkamp	Preiß
5560	R. & Zander	Niedringhaus
5563	S.	Löwe
5566	K.	Rieck
5569	K.	Willkomm
5572	B.	Oltmanns
5577	C.	Wolfram
5580	W.	Härdtle
5587	M.	Isermann
5590	Oliviero de	Simone
5593	M.-K.	Schwederski
5596	S.	Heemann
5603	R	Janowsky
5606	P.	Weißkamp
5618	A	Gericke
5621	S.U.	Rexin
5624	D.	Eis
5627	E.	Munderloh
5629	E.A.	Albers
5632	A. et al.	Schwabe-Kratochwil
5657	M.	Ritz
5660	M.	Ikemeyer
5663	\N	Petersen. J.
5666	W. & R.J. de Wit (red.)	Meijer
5669	J.M.	Ninot
5672	P.W.F.M. & M.A.P. Horsthuis (red.)	Hommel
5675	M. & J.H. Ietswaart	Vroman
5678	J.	Meltzer
5681	T.G.	Hommes
5695	G.	Kiesel
5700	H.	Tippmann
5704	K.	Noritzsch
5707	Armin	Bischoff
5708	N.	Winter
5712	M.	Lindenschmidt
5716	K.	Gürgens
5722	Philip	Bedall
5723	Ines	Bruchmann
5724	Ina	Hoeft
5725	Anne	Lang
5728	Kai	Jensen
5730	Miriam	Fanigliulo
5731	Birgit	Seitz
5734	Gunnar	Waesch
5739	R	Vanesse
5742	\N	WWK-Umweltplanung, Warendorf
5743		WWK-Umweltplanung, Warendorf
5748	Vicky M.	Temperton
5749	Johannes	Kollmann
5750	Swantje	Löbel
5751	Annika	Kraft
5758	Jaime	García Márquez
5759	Carsten	Dormann
5760	Jan Henning	Sommer
5761	Marco	Schmidt
5762	Adjima	Thiombiano
5763	Sié	Sylvestre Da
5764	Cyrille	Chatelain
5765	Stefan	Dressler
5766	Wilhelm	Barthlott
5769	Norbert	Jürgens
5772	Wolfgang	Willner
5773	Paul	Heiselmayer
5776	Peter	Horchler
5777	Eva	Mosner
5778	Jan	Peper
5781	Kiril	Vassilev
5782	Zora	Dajis
5783	Renata	Cuterevska
5784	Iva	Apostolova
5787	Aleksander	Marinek
5790	Annett	Thiele
5791	Nadzeya	Liashchynskaya
5792	Tatsiana	Broska
5793	Susanne	Bärisch
5794	Arkadi	Skuratovich
5795	Dimitri	Dubovik
5796	Jazep	Stepanovich
5797	Galina	Ermolenko
5798	Oleg	Sozinov
5799	Anastasia	Sakovich
5804	Stefan	Meyer
5805	Benjamin	Krause
5810	Gerhard	Muche
5811	Ute	Schmiedel
5814	Desislava	Sopotlieva
5815	Hristo	Pedashenko
5816	Nikolay	Velev
5817	Kiril	Vasilev
5820	Fengjun	Zhao
5821	Hongyan	Liu
5824	Miguel	Alvarez
5825	Cristina	San Martín
5826	Carlos	Ramírez
5827	Javier	Amigo
5830	Alexey	Sorokin
5831	Valentin	Golub
5832	Natalia	Grechushkina
5833	Lyudmila	Nikolaychuk
5834	Viktoria	Bondareva
5837	Andrej	Chuvashov
5840	János	Csiky
5841	Zoltán	Botta-Dukát
5842	Ferenc	Horváth
5843	Konrád	Lájer
5846	Jesús	Rangel-Churio
5847	Jairo Humberto	Pinto-Zárate
5850	Christian	Dolnik
5853	Milan	Chytrý
5854	Dana	Michalcová
5857	Pavel-Dan	Turtureanu
5860	Tatiana	Chernenkova
5861	Elena	Basova
5862	Nata	Koroleva
5865	Solvita	Rusina
5868	Christophe	Hunziker
5871	Sergey	Yamalov
5872	Albert	Muldashev
5873	Artem	Bayanov
5874	Tatyana	Jirnova
5875	Aizek	Solomesch
5878	Václav	Zouhar
5881	Ulrich	Deil
5884	Judith	Toms
5887	Sigrid	Suchrow
5888	Nina	Pohlmann
5889	Martin	Stock
5894	Zora	Dajic Stevanovic
5895	Milicia	Petrovic
5896	Urban	ilc
5897	Svetlana	Acic
5902	Andrei	Zverev
5903	Andrey	Korolyuk
5906	Melanie	Rahmlow
5907	Patrick	Lampe
5910	Salza	Todorova
5911	Steffen	Boch
5912	Martin	Diekmann
5913	Cecilia	Dupré
5914	Gian Pietro	Giusso del Galdo
5915	Riccardo	Guarino
5916	Michael	Jeschke
5917	Kathrin	Kiehl
5918	Anna	Kuzemko
5919	Zdenka	Otýpková
5920	Robert	Peet
5921	Eszter	Ruprecht
5922	Anna	Szabó
5923	Ioannis	Tsiripidis
5926	Michaela	Dölle
5927	Andreas	Parth
5930	Monika	Wulf
5937	Laura	Cancellieri
5938	Bruno	Paura
5939	Andrea	Catorci
5940	Maurizio	Cutini
5941	Leonardo	Rosati
5944	Martin	Alt
5945	Anke	Jentsch
5946	Constanze	Buhk
5947	Manuel	Steinbauer
5950	Carl	Beierkuhnlein
5955	Laura	Facioni
5956	Eva	Del Vico
5957	Sabina	Burrascano
5958	Agnese	Tilia
5959	Carlo	Blasi
5962	Jürgen	Homeier
5965	Sarah	Jovan
5968	Jaanus	Paal
5971	Michele	De Sanctis
5972	Diego	D'Angeli
5973	Anna	Testi
5974	Fabio	Attorre
5977	Emiliano	Agrillo
5978	Francesco	Spada
5979	Nicola	Alessi
5982	Marco	Massimi
5985	Federica	Roncalli
5988	Simonetta	Valfré
5991	Laura	Casella
5994	Jens	Oldeland
5995	Manfred	Finckh
5996	Falko	Glöckler
5997	Gabriela	Lopez-Gonzalez
5998	Joop	Schaminée
6001	Bethany	Schulz
6002	Kevin	Dobelbower
6005	Thomas	Spiegelberger
6006	Claude	Bernard-Brunet
6009	Mathias	Becker
6010	Beate	Böhme
6011	Collins	Handa
6012	Matthias	Josko
6013	Hellen	Kamiri
6014	Matthias	Langensiepen
6015	Gunter	Menz
6016	Salome	Misana
6017	Neema	Mogha
6018	Emiliana	Mwita
6019	Helida	Oyieke
6020	Nomé	Sakané
6023	Philippe	Daget
6026	Andrew	Gray
6027	Thomas	Brandeis
6028	John	Shaw
6029	William	McWilliams
6030	Patrick	Miles
6033	Mark	Burkitt
6034	Simon	Lewis
6035	Oliver	Phillips
6042	Georgios	Fotiatidis
6043	Panayotis	Dimopoulos
6048	Georgios	Fotiadis
6049	Panayotis	Dimopolous
6052	Xavier	Castell
6053	Idoia	Biurrun
6054	Federico	Fernández-Gonzalez
6055	Carmen	Lence
6058	Úna	FitzPatrick
6059	Naomi	Kingston
6062	Linda	Olsvig-Whittaker
6065	Pietro Massimiliano	Bianco
6066	Pierangela	Angelini
6067	Emi	Morroni
6070	William	Cornwell
6071	David	Ackerly
6074	Tomá	Cerný
6075	Miroslav	rutek
6076	Petr	Petrík
6077	Jong-Suk	Song
6078	Milan	Valachovic
6083	Ladislav	Mucina
6086	Anikó	Csecserits
6087	Tamás	Rédei
6088	György	Kröel-Dulay
6091	Kseniya	Starichkova
6092	Tatyana	Ivakhnova
6095	Giovanna	Potenza
6096	Simonetta	Fascetti
6099	Leonid	Rasran
6100	Kati	Vogt
6103	Mattia	Azzella
6104	Mauro	Iberite
6107	Liene	Aunina
6110	Elena	Tikhonova
6111	Olga	Morozova
6112	Olga	Pesterova
6117	Heike	Hofmann
6118	Norbert	Schnyder
6121	Ben	Strohbach
6122	Fransiska	Kangombe
6125	Leslie	Powrie
6126	Michael Charles	Rutherford
6127	Kagiso	Mangwale
6130	James	Vanderhorst
6131	Elizabeth	Byers
6132	Brian	Streets
6135	Knud	Nielsen
6136	J	Bak
6137	M	Bruus
6138	C	Damgaard
6139	R	Ejrnæs
6140	J	Fredshavn
6141	B	Nygaard
6142	F	Skov
6143	B	Strandberg
6144	M	Strandberg
6147	Hazel	Broadbent
6148	Nick	Spencer
6149	Susan	Wiser
6152	Miquel	De Cáceres
6159	Pascal	Vittoz
6162	Christian	Roulier
6167	Zvjezdana	Stancic
6170	Nobuyuki	Tanaka
6173	Zygmunt	Kacki
6174	Michal	Sliwinski
6177	Gerd	Jünger
6178	Annett	Baasch
6179	Anita	Kirmer
6180	Antje	Lorenz
6181	Sabine	Tischew
6184	Jean-Marc	Hero
6185	Gregory	Lollback
6186	Naomi	Edwards
6187	Sarah	Butler
6188	Rochelle	Steven
6189	Jon	Shuker
6190	Clay	Simpkins
6191	Guy	Castley
6194	Kerstin	Bach
6199	Bradley	Boyle
6200	Brian	Enquist
6203	Mitchel	McClaran
6210	Jozef	ibík
6215	Mogha	Neema
6218	Thomas	Stalling
6223	Thomas	Wohlgemuth
6226	Jonathan	Lenoir
6227	Jens-Christian	Svenning
6228	Stefan	Dullinger
6229	Harald	Pauli
6230	Antoine	Guisan
6231	Niklaus	Zimmermann
6232	Jean-Claude	Gégout
6235	Flávia	Pezzini
6236	Pablo Hendrigo	Alves de Melo
6237	Dayane Mayely	Silva de Oliveira
6238	Rainer	Xavier de Amorim
6239	Fernando Oliveira	Gouvêa de Figueiredo
6240	Debora	Pignatari Drucker
6241	Flávio Rogério	de Oliveira Rodrigues
6242	Gabriela	Zuquim
6243	Thaise	Emilio
6244	Flávia Regina	Capellotto Costa
6245	William Ernest	Magnusson
6246	Adeilza	Felipe Sampaio
6247	Albertina	Pimentel Lima
6248	Ana Raquel	de Mesquita Garcia
6249	Angelo	Gilberto Manzatto
6250	Anselmo	Nogueira
6251	Cândida Pereira	da Costa
6252	Carlos Eduardo	de Araújo Barbosa
6253	Carolina	Bernardes
6254	Carolina	Volkmer de Castilho
6255	Catia Nunes	da Cunha
6256	Cintia Gomes	de Freitas
6257	Claymir	de Oliveira Cavalcante
6258	Diego	Oliveira Brandão
6259	Domingos	de Jesus Rodrigues
6260	Eliana Celestino	da Paixão Rodrigues dos Santos
6261	Fabricio	Beggiato Baccaro
6262	Françoise	Yoko Ishida
6263	Fernanda	Antunes Carvalho
6264	Gabriel	Massaine Moulatlet
6265	Jean-Louis Bernard	Guillaumet
6266	José Luiz Purri	Veiga Pinto
6267	Juliana	Schietti
6268	Julio Daniel	do Vale
6269	Lauren	Belger
6270	Luciano	Martins Verdade
6271	Marcelo	Petratti Pansonato
6272	Marcelo	Trindade Nascimento
6273	Márcia Cléia	Vilela dos Santos
6274	Mariana	Souza da Cunha
6275	Rafael	Arruda
6276	Reinaldo	Imbrozio Barbosa
6277	Ricardo	Laerte Romero
6278	Susamar	Pansini
6279	Tania	Pena Pimentel
6282	Stephan	Hennekens
6283	Wim	Ozinga
6286	Walter	Berendsohn
6287	Jirí	Danihelka
6288	Wolf-Henning	Kusber
6289	Flavia	Landucci
6290	Andreas	Müller
6291	Edoardo	Panfili
6292	Roberto	Venanzoni
6293	Eckhard	von Raab-Straube
6296	Emmanuel	Garbolino
6297	Patrice	De Ruffray
6298	Henry	Brisse
6299	Gilles	Grandjouan
6302	Luis	Cayuela
6303	Lucía	Gálvez-Bravo
6304	Ramón	Pérez Pérez
6305	Fábio	de Albuquerque
6306	Duncan	Golicher
6307	Rakan	Zahawi
6308	Neptalí	Ramírez-Marcial
6309	Cristina	Garibaldi
6310	Richard	Field
6311	José	Rey Benayas
6312	Mario	González-Espinosa
6313	Patricia	Balvanera
6314	Miguel	Ángel Castillo
6315	Blanca	Figueroa-Rangel
6316	Daniel	Griffith
6317	Gerald	Islebe
6318	Daniel	Kelly
6319	Miguel	Olvera-Vargas
6320	Stefan	Schnitzer
6321	Eduardo	Velázquez
6322	Guadalupe	Williams-Linera
6323	Steven	Brewer
6324	Angélica	Camacho-Cruz
6325	Indiana	Coronado
6326	Ben	de Jong
6327	Rafael	del Castillo
6328	Íñigo	Granzow-de la Cerda
6329	Javier	Fernández
6330	William	Fonseca
6331	Luis	Galindo-Jaimes
6332	Thomas	Gillespie
6333	Benigno	González-Rivas
6334	James	Gordon
6335	Johanna	Hurtado
6336	José	Linares
6337	Susan	Letcher
6338	Scott	Mangan
6339	Jorge	Meave
6340	Ernesto	Méndez
6341	Victor	Meza
6342	Susana	Ochoa-Gaona
6343	Chris	Peterson
6344	Viviana	Ruiz-Gutierrez
6345	Kymberley	Snarr
6346	Fernando	Tun Dzul
6347	Mirna	Valdez-Hernández
6348	Karin	Viergever
6349	David	White
6350	John	Williams
6351	Francisco	Bonet
6352	Regino	Zamora
6355	Thomas	Janßen
6356	Karen	Hahn
6357	Mipro	Hien
6358	Souleymane	Konaté
6359	Anne Mette	Lykke
6360	Ali	Mahamane
6361	Bienvenu	Sambou
6362	Brice	Sinsin
6363	Rüdiger	Wittig
6364	Georg	Zizka
6367	Roger	del Moral
6374	Daniela	Gigante
6377	Eric	Fegraus
6380	Susannah	Rennie
6383	John	Rodwell
6388	Michael	Lee
6389	Michael	Jennings
6390	Don	Faber-Langendoen
6393	Irene	Prisco
6394	Marta	Carboni
6395	Alicia Teresa	Rosario Acosta
6398	Andreas	Hemp
6401	Pavel	Shirokikh
6402	Vasilij	Martynenko
6403	Ayzik	Solomeshch
6416	Thomas	Michl
6417	Stefan	Huck
6424	John	Mulhouse
6425	Esteban	Muldavin
6430	Claudia	Bita-Nicolae
6433	Risto	Virtanen
6436	Lindsay	Maskell
6437	Simon	Smart
6438	Lisa	Norton
6439	Claire	Wood
6442	Nidia	Cuello
6445	Alireza	Naqinezhad
6450	Emin	Ugurlu
6453	Adrian	Indreica
6456	Erika	Mudrak
6457	Don	Waller
6458	Brad	Herrick
6463	Mélanie	Major
6464	Jocelyn	Gosselin
6467	Mohamed	Hatim
6472	Peter	Borchardt
6473	Udo	Schickhoff
6482	Yury	Semenishchenkov
6487	Mattia Martin	Azzella
6490	Edgar Andrés	Avella-Muñoz
6493	Annemária	Fenesi
6496	Deniz	Isik
6501	Jonathan	Etzold
6502	F	Münzner
6503	J	Peters
6504	J	Limberg
6505	M	Wenzel
6506	T	Dahms
6507	M	Manthey
6510	Jürgen	Kayser
6511	Hans-Gerd	Michiels
6514	Tatiana	Lysenko
6515	Olga	Kalmykova
6516	Anna	Mitroshenkova
6521	Steffi	Heinrichs
6522	Wulfard	Winterhoff
6525	Wieger	Wamelink
6526	Marjolein	van Adrichem
6527	Han	van Dobben
6528	Joep	Frissel
6529	Merel	den Held
6530	Veronique	Joosten
6531	Agnieszka	Malinowska
6532	Pieter	Slim
6533	Ruut	Wegman
6536	Forbes	Boyle
6537	Thomas	Wentworth
6538	Michael	Schafale
6539	Alan	Weakley
6542	Itziar	García-Mijangos
6543	Juan	Campos
6544	Mercedes	Herrera
6545	Javier	Loidi
6548	Rudolf	May
6553	Emmanuelle	Porcher
6554	Nathalie	Machon
6555	Daniel	Mathieu
6560	Gisèle	Weyembergh
6561	Filiep	Tjollyn
6562	Desiré	Paelinckx
6565	Victor	Chepinoga
6568	Birgit	Reger
6569	Ralf	Schüpferling
6570	Josefine	Beck
6571	Elke	Dietz
6572	Daniel	Morovitz
6573	Roman	Schaller
6574	Gerhard	Wilhelm
6577	Donald	Waller
6578	Kathryn	Amatangelo
6579	Sarah	Johnson
6580	David	Rogers
2379	Ansgar	Hoppe
6595	Holger	Frehse
6607	Birke	Heeren
\.


--
-- Data for Name: database; Type: TABLE DATA; Schema: public; Owner: quarasek
--

COPY public.database (id, name, description, create_date) FROM stdin;
5687	VegMV	Vegetationsdaten aus Mecklenburg-Vorpommern, siehe http://www.givd.info/ID/EU-DE-001	2016-12-20 20:36:51.374
5688	GVRD	German Vegetation Reference Database	2016-12-20 20:37:26.105
5690	Tuexenia	\N	2016-12-20 20:37:46.395
5691	vegetweb	Daten aus vegetweb 1.0 und Daten die keinem der anderen Projekte zugeordnet werden können.	2016-12-22 07:26:49.528
5720	GrassVeg.DE	GrassVeg.DE ist eine kollaborative Datenbank für Grasland-Vegetationsaufnahmen im weitesten Sinne aus Deutschland, die noch nicht in anderen EVA-Mitgliedsdatenbanken enthalten sind. Ziel ist es, (a) diese Daten für via EVA und sPlot für deutschlandweite, kontinentale und globale Analysen bereitzustellen und zugleich zu gewährleisten, dass Datenlieferanten einen angemessenen Nutzen aus der Verwendung ihrer Daten haben. GrassVeg.DE wird derzeit geleitet von Dr. Jürgen Dengler als Kustos und Dr. Thomas Becker als Stellvertretender Kustos. Mehr Informationen unter http://www.givd.info/ID/EU-DE-020 und https://www.bayceer.uni-bayreuth.de/ecoinformatics/en/forschung/gru/html.php?id_obj=139259.	2017-01-19 13:09:19.128
5689	CoastVeg	Sammlung von Maike Isermann	2017-04-05 13:30:23.16
\.


--
-- Data for Name: external_reference; Type: TABLE DATA; Schema: public; Owner: quarasek
--

COPY public.external_reference (id, database, reference, publication_id) FROM stdin;
\.


--
-- Name: external_reference_id_seq; Type: SEQUENCE SET; Schema: public; Owner: quarasek
--

SELECT pg_catalog.setval('public.external_reference_id_seq', 1, false);


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: quarasek
--

SELECT pg_catalog.setval('public.hibernate_sequence', 6741, true);


--
-- Data for Name: institution; Type: TABLE DATA; Schema: public; Owner: quarasek
--

COPY public.institution (id, name) FROM stdin;
75	Bearb. im Auftrag d. Regierungspräsidiums Kassel, 68 S. Unveröff. Gutachten
292	Unveröff. Gutachten
1064	Landschaftspflegerisches Gutachten.- 2. Projektarbeit Institut Landschaftspflege Naturschutz, Universität Hannover. 154 S
1170	Gutachten im Auftrag d. Bezirksregierung Braunschweig. 144 S
1256	Gutachten im Auftrag d. Thüringer Landesanstalt f. Umwelt Sonderhausen. 38 S
1436	Unveröff. Gutachten im Auftrag d. Landesamtes f. Umweltschutz Sachsen-Anhalt, Halle (Saale)
1440	Unveröff. Gutachten
1500	Gutachten. Berlin. 81 S
1522	Gutachten
1529	Gutachten i.A. von Gi ?
1572	Gutachten
1589	Gutachten
1592	Gutachten
1595	Gutachten
1604	Gutachten
1611	Gutachten
1617	Gutachten
1620	Gutachten
1623	Gutachten
1626	Gutachten
1629	Gutachten
1632	Gutachten
1635	Gutachten
1638	Gutachten
1641	Gutachten
1644	Gutachten
1647	Gutachten
1655	Gutachten
1659	Gutachten
1673	Gutachten
1676	Gutachten
1679	Gutachten
1682	Gutachten
1695	Gutachten
1698	Gutachten
1701	Gutachten
1704	Gutachten
1707	Gutachten
1710	Gutachten
1713	Gutachten
1716	Gutachten
1719	Gutachten
1722	Gutachten
1769	Ber. ü. d. Forschungsauftrag Nr. 2155 15h/0-04-8. Vegetationsuntersuchungen i. Südteil d. Lausitz, 4. Teilthema. Halle (Saale) u. Görlitz. 39 S
1856	Endbericht im Auftrag d. Niedersächsischen Landesverwaltungsamtes, Fachbehörde f. Naturschutz i. Hannover. 168 S. Helpensen
2215	Gutachten. Zerbst
2219	Regierungspräsidium Dessau
2229	Arbeiten a. d. Bundesanstalt f. Vegetationskartierung, Gutachten 221/6A
2440	Projekt-Abschlussbericht TU Dresden
2623	Ber. 2006, ABU, Bad Sassendorf-Lohne
2671	Gutachten
2753	Unveröff. Gutachten im Auftrag d. Märkischen Kreises. Arnsberg 1. Neheim-Hüsten
2764	Unveröff. Gutachten im Auftrag d. LÖLF
2839	Unveröff. Gutachten Zweckverband Naturschutzgroßprojekt (Auftraggeber). 228 S. Stuttgart
2864	Unveröff. Gutachten
2868	Unveröff. Gutachten
2871	Unveröff. Gutachten
2874	Unveröff. Gutachten
2877	Unveröff. Gutachten
2880	Unveröff. Gutachten
2883	Unveröff. Gutachten
2886	Unveröff. Gutachten
2890	Unveröff. Gutachten
2893	Unveröff. Gutachten
2896	Unveröff. Gutachten
2899	Unveröff. Gutachten
2902	Unveröff. Gutachten
2905	Unveröff. Gutachten
2908	Unveröff. Gutachten
2911	Unveröff. Gutachten
2914	Unveröff. Gutachten
2917	Unveröff. Gutachten
2920	Unveröff. Gutachten
2923	Unveröff. Gutachten
2930	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 100 S
2933	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 82 S
2936	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 132 S
2939	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 144 S
2942	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 116 S
2945	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 53 S
2948	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 107 S
2951	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 113 S
2954	Unveröff. Gutachten
2957	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 121 S
2960	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 138 S
2963	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 87 S
2966	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 73 S
2969	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 91 S
2972	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 38 S
2975	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 74 S
2978	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 51 S
2981	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 42 S
2984	Unveröff. Gutachten im Auftrag d. Regierungspräsidiums Kassel. 203 S
3198	Unveröff. Gutachten. 40 S
3496	Gutachten Stadt Augsburg, Amt für Grünordnung und Naturschutz, 75 S
3500	Unveröff. Gutachten im Auftrag der BfG, bearbeitet durch Volker Dittmann, IVL Leipzig
3503	Unveröff. Gutachten im Auftrag der BfG, bearbeitet durch Antje Birger, Umgeodat Halle (Saale
4240	Auftraggeber: StAUN Ueckermünde
4244	Auftraggeber: LM
4248	Auftraggeber: LAUN
4252	Auftraggeber: StAUN Schwerin
4255	Auftraggeber: StAUN Schwerin
4264	Auftraggeber: StAUN Schwerin, Abt. Wasserwirtschaft
4267	Auftraggeber: LAUN
4270	Auftraggeber: LAUN
4280	Auftraggeber: LM
4284	Auftraggeber: Ministerium f. Umwelt, Naturschutz u. Raumordnung d. Landes Brandenburg u. StAUN Lübz
4305	Auftraggeber: Nationalparkamt
4311	Auftraggeber: StAUN Rostock
4314	Auftraggeber: StAUN Rostock
4321	Auftraggeber: Nationalparkamt
4328	Auftraggeber: INA Insel Vilm
4331	Auftraggeber: StAUN Schwerin
4335	Auftraggeber: Nationalparkamt
4340	Auftraggeber: LAUN
4345	Auftraggeber: StAUN Lübz
4348	Auftraggeber: Landkreis Wismar
4352	Auftraggeber: LAUN
4357	Auftraggeber: LM
4360	Auftraggeber: StAUN Parchim
4368	Auftraggeber: Nationalparkamt
4372	Auftraggeber: StAUN Lübz
4379	Auftraggeber: StAUN Parchim/Lübz
4383	Auftraggeber: StAUN Lübz
4386	Auftraggeber: StAUN Lübz
4402	Auftraggeber: StAUN Schwerin
4405	Auftraggeber: StAUN Schwerin
4409	Auftraggeber: StAUN Greifswald
4422	Auftraggeber: Meliorationskombinat Neubrandenburg
4426	Auftraggeber: Nationalparkamt
4429	Auftraggeber: LAUN
4436	Auftraggeber: Nationalparkamt
4440	Auftraggeber: LM
4443	Auftraggeber: StAUN Neubrandenburg
4447	Auftraggeber: StAUN Neustrelitz
4454	Auftraggeber: Nationalparkamt
4457	Auftraggeber: Nationalparkamt
4465	Auftraggeber: LM
4469	Auftraggeber: LM
4472	Auftraggeber: Nationalparkamt
4476	Auftraggeber: Nationalparkamt
4487	Auftraggeber: LM
4490	Auftraggeber: StAUN Rostock
4498	Auftraggeber: LM
4510	Auftraggeber: StAUN Rostock, LAUN, LM
4514	Auftraggeber: StAUN Schwerin
4521	Auftraggeber: LAUN
4528	Auftraggeber: Nationalparkamt
4534	Auftraggeber: LAUN
4537	Auftraggeber: LAUN
4540	Auftraggeber: LM
4543	Auftraggeber: StAUN Parchim/Lübz
4546	Auftraggeber: StAUN Neustrelitz
4549	Auftraggeber: LAUN
4552	Auftraggeber: Nationalparkamt
4555	Auftraggeber: Nationalparkamt
4558	Auftraggeber: StAUN Neustrelitz
4561	Auftraggeber: I.L.N. Greifswald
4564	Auftraggeber: StAUN Neustrelitz
4567	Auftraggeber: Nationalparkamt
4570	Auftraggeber: StAUN Ueckermünde
4573	Auftraggeber: StAUN Neubrandenburg
4576	Auftraggeber: StAUN Schwerin
4580	Auftraggeber: Nationalparkamt
4584	Auftraggeber: StAUN Neustrelitz
4588	Auftraggeber: LAUN
4594	Auftraggeber: LAUN
4597	Auftraggeber: StAUN Teterow/Rostock
4600	Auftraggeber: LAUN
4603	Auftraggeber: StAUN Neubrandenburg
4608	Auftraggeber: LAUN
4611	Auftraggeber: Grünspektrum Neubrandenburg
4614	Auftraggeber: StAUN Neubrandenburg
4617	Auftraggeber: Grünspektrum Neubrandenburg
4624	Auftraggeber: StAUN Teterow/Rostock
4632	Auftraggeber: StAUN Schwerin
4635	Auftraggeber: StAUN Schwerin
4650	Auftraggeber: Nationalparkamt
4653	Auftraggeber: Nationalparkamt
4657	Auftraggeber: Nationalparkamt
4664	Auftraggeber: StAUN Rostock
4669	Auftraggeber: StAUN Parchim
4672	Auftraggeber: StAUN Schwerin
4675	Auftraggeber: LAUN
4678	Auftraggeber: LM
4684	Auftraggeber: StAUN Rostock
4691	Auftraggeber: Nationalparkamt
4705	Auftraggeber: Nationalparkamt
4708	institution missing
4755	Auftraggeber: Nationalparkamt
5034	UNiversität Greifswald
5135	institution missing
5138	institution missing
5141	institution missing
5226	Ernst-Moritz-Arndt-Universität
5234	Universität Greifswald
6616	Birkes Test Institution
6618	Birkes Test Institution
6634	Birkes Test Institution
6638	Birkes Test Institution
6640	Birkes Test Institution
6644	Birkes Test Institution
6648	Birkes Test Institution
6650	Birkes Test Institution
6652	Birkes Test Institution
6656	Birkes Test Institution
6658	Birkes Test Institution
6660	Birkes Test Institution
6662	Birkes Test Institution
6664	Birkes Test Institution
6666	Birkes Test Institution
6668	Birkes Test Institution
6674	Birkes Test Institution
6676	Birkes Test Institution
6678	Birkes Test Institution
6680	Birkes Test Institution
6682	Birkes Test Institution
6695	Birkes Test Institution
6703	Birkes Test Institution
6705	Birkes Test Institution
6713	Birkes Test Institution
6715	Birkes Test Institution
6719	Birkes Test Institution
6721	Birkes Test Institution
6723	Birkes Test Institution
6725	Birkes Test Institution
\.


--
-- Data for Name: journal; Type: TABLE DATA; Schema: public; Owner: quarasek
--

COPY public.journal (id, name, pages, volume, issn) FROM stdin;
4	Tuexenia	73-113	2	\N
7	Wiss. Z. Universität Halle, Math.-nat.R	817-866	11 (7)	\N
5768	Biodiversity & Ecology	25-39	4	1613-9801
14	Mitteilungen d. Floristisch-soziologischen AG N.F	203-217	18	\N
17	Landschaftspflege u. Naturschutz i. Thüringen	55-61, Thüringer Landesanstalt f	45 (2)	\N
20	Mitteilungen d. Pollichia	93-100	84	\N
23	Kieler Notizen zur Pflanzenkunde i. Schleswig-Holstein u. Hamburg Jahrgang	17 (4), 137-164, Uni Kiel	17 (4), 137-164, Uni Kiel.	\N
27	Kieler Notizen zur Pflanzenkunde i. Schleswig-Holstein u. Hamburg Jahrgang	32 , 57-95, Uni Kiel	32 , 57-95, Uni Kiel.	\N
30	Scripta Geobotanica	1-31	2	\N
33	Mitteilungen d. Floristisch-soziologischne AG NF	103-109	18	\N
36	Scripta Geobotanica	61-75	12	\N
39	Pollichia	461-493	23	\N
5771	Biodiversity & Ecology	7-7	4	1613-9801
5775	Biodiversity & Ecology	333-333	4	1613-9801
5780	Biodiversity & Ecology	367-367	4	1613-9801
51	Mitteilungen d. AG Geobotanik i. Schleswig-Holstein u. Hamburg	1-end	56	\N
5786	Biodiversity & Ecology	330-330	4	1613-9801
58	Natur u. Naturschutz i. Mecklenburg-Vorpommern	100-135	37	\N
5789	Biodiversity & Ecology	329-329	4	1613-9801
64	Decheniana Beih	1-186	1	\N
67	Decheniana Beih	1-77	3	\N
70	Decheniana Beih	1-101	4	\N
5801	Biodiversity & Ecology	335-336	4	1613-9801
5803	Biodiversity & Ecology	161-165	4	1613-9801
5807	Biodiversity & Ecology	366-366	4	1613-9801
5809	Biodiversity & Ecology	351-351	4	1613-9801
5813	Biodiversity & Ecology	111-123	4	1613-9801
5819	Biodiversity & Ecology	141-148	4	1613-9801
91	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	7-61	37	\N
94	Decheniana	63-140	143	\N
97	Ber. d. Bayerischen Botanischen Gesellschaft	165-180	62	\N
100	Verhandlungen d. Bot. Vereins d. Provinz Brandenburg	10-93	74	\N
5823	Biodiversity & Ecology	302-302	4	1613-9801
106	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	7-146	33	\N
109	Natur u. Naturschutz i. Mecklenburg-Vorpommern	79-108	5	\N
111	Mitteilungen Arbeitskreis heimischer Orchideen	68-93	6	\N
113	Mitteilungen d. Floristisch-soziologischne AG NF	77-81	22	\N
117	Pflanzensoziologie	1-229	4	\N
5829	Biodiversity & Ecology	443-443	4	1613-9801
124	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg Beih	1-510	39	\N
5836	Biodiversity & Ecology	422-422	4	1613-9801
130	Abhandlungen a. d. Landesmuseum f. Naturkunde z. Münster i. Westfalen	1-95	51 (2)	\N
5839	Biodiversity & Ecology	423-423	4	1613-9801
136	Wiss. Z. Universität Jena, Math.-nat.R	1038-1088	21 (5/6)	\N
139	Mitteilungen Thür. Bot. Gesellschaft	151-175	2(1)	\N
142	Z. Acker- Pflanzenbau	400-444	93 (4)	\N
5845	Biodiversity & Ecology	394-394	4	1613-9801
148	Jahrbuch Nass. Ver. Naturkunde	16-68	102	\N
5849	Biodiversity & Ecology	275-286	4	1613-9801
5852	Biodiversity & Ecology	427-427	4	1613-9801
158	Botanik u. Naturschutz i. Hessen	33-62	5	\N
161	Botanik u. Naturschutz i. Hessen	14-54	4	\N
164	Pflanzensoziologie	17, 451 S	17, 451 S. Jena.	\N
167	Botanik u. Naturschutz i. Hessen	137-143	20	\N
170	Botanik u. Naturschutz i. Hessen	5-19	13	\N
174	Botanik u. Naturschutz i. Hessen	11-40	11	\N
176	Botanik u. Naturschutz i. Hessen	77-102	6	\N
179	Botanik u. Naturschutz i. Hessen	121-154	10	\N
182	Botanik u. Naturschutz i. Hessen	5-32	5	\N
186	Botanik u. Naturschutz i. Hessen	145-151	20	\N
189	Tuexenia	443-473	10	\N
192	Mitteilungen d. Pollichia	77-104	85	\N
196	Mitteilungen d. Pollichia	105-152	85	\N
199	Botanik u. Naturschutz i. Hessen, Beih	3	3. 285 S. Frankfurt am Main.	\N
202	Mitteilungen d. Floristisch-soziologischne AG i. Niedersachsen	1	1.	\N
204	Mitteilungen d. Floristisch-soziologischne AG i. Niedersachsen	19-30	2	\N
207	Mitteilungen d. Floristisch-soziologischne AG i. Niedersachsen	74	4	\N
212	Naturschutz u. naturkundliche Heimatforschung Bez. Halle u. Magdeburg	16 (1979) 2	16 (1979) 2.	\N
215	Hess. Flor. Briefe	5-14	18 (206)	\N
217	Haussknechtia Beiheft	4, 1994	4, 1994. Jena.	\N
219	Archiv f. Naturschutz u. Landschaftsforschung	91-124	15 (2)	\N
5856	Biodiversity & Ecology	345-345	4	1613-9801
225	Mitteilungen d. Pollichia	82:101-141	82:101-141. Bad Dürkheim.	\N
228	Decheniana Beih	1-50	8	\N
231	Decheniana Beih	1-145	15	\N
234	Mitteilungen AG Geobot. Schleswig-Holst. Hamburg	1-130	55	\N
5859	Biodiversity & Ecology	415-415	4	1613-9801
5864	Biodiversity & Ecology	425-425	4	1613-9801
5867	Biodiversity & Ecology	319-320	4	1613-9801
248	Archiv f. Naturwissenschaftliches Dissertationen	1-238	9	\N
251	Tuexenia	271-298	6	\N
259	Diss. Botanicae	1-154	229	\N
268	Schriftenreihe f. Vegetationskunde	85-102	4	\N
277	Hercynia NF	1-372	2 (4)	\N
285	Mitteilungen d. Floristisch-soziologischne AG NF	389-68	17	\N
301	Naturschutz u. Landschaftspflege Niedersachsen	1-110	8	\N
304	Naturschutz u. Landschaftspflege Niedersachsen	1-150	35	\N
307	Decheniana	219-245	105/106	\N
339	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	31-106	66	\N
342	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	141-179	66	\N
346	Beih Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	121-158	78	\N
350	Beih Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	159-306	78	\N
5870	Biodiversity & Ecology	341-341	4	1613-9801
5877	Biodiversity & Ecology	291-291	4	1613-9801
5880	Biodiversity & Ecology	346-346	4	1613-9801
360	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	107-140	66	\N
363	Ber. d. Bayerischen Botanischen Gesellschaft	5-134	23	\N
366	Veröff. d. Naturwissenschaftlichen Vereins Osnabrück	17-36	29	\N
368	Veröff. d. Naturwissenschaftlichen Vereins Osnabrück	7-18	30	\N
371	Mitteilungen AG Geobot. Schleswig-Holst. Hamburg ? Kiel.	Mitteilungen AG Geobot	Mitteilungen AG Geobot. Schleswig-Holst. Hamburg ? Kiel.	\N
373	Mitteilungen Natur- u. Umweltschutz Hamburg	5-58	6	\N
5883	Biodiversity & Ecology	441-441	4	1613-9801
379	Mitteilungen d. Floristisch-soziologischne AG NF	99-103	5	\N
382	Hoppea	23-276	45	\N
385	Jahrbuch Naturwissenschaftliches Vereins Fürstentum Lüneburg	129-147	34	\N
388	Beitr. z. Naturkunde i. Wetterau	103-153	6 (2)	\N
392	Tuexenia	163-179	4	\N
5886	Biodiversity & Ecology	431-431	4	1613-9801
5891	Biodiversity & Ecology	368-369	4	1613-9801
5893	Biodiversity & Ecology	370-370	4	1613-9801
405	Wiss. Z. Universität Halle, Math.-nat.R	765-816	11	\N
408	Wiss. Rundbr. Zentralstelle Vegetationskartierung	62-64	12	\N
5899	Biodiversity & Ecology	417-417	4	1613-9801
414	Decheniana	102 B: 47-275	102 B: 47-275. Bonn.	\N
417	Natur u. Heimat	69-73	5 (3)	\N
419	Abhandlungen a. d. Landesmuseum d. Provinz Westfalen, Museum f. Naturkunde	108 S	10 (1)	\N
421	Beih Bot. Centralblatt	61, Abt	61, Abt. B, 452-558. Dresden.	\N
424	Abhandlungen a. d. Landesmuseum f. Naturkunde z. Münster i. Westfalen	1-59	13 (2)	\N
427	Abhandlungen a. d. Landesmuseum f. Naturkunde z. Münster i. Westfalen	31 (1)	31 (1). Münster.	\N
430	Arbeiten z. Rhein. Landeskunde	1-217	48	\N
432	Natur u. Heimat	89-96	27 (3)	\N
434	Mitteilungen d. Floristisch-soziologischne AG NF	59-70	13	\N
436	Mitteilungen d. Floristisch-soziologischne AG NF	458-479	14	\N
438	Mitteilungen d. Floristisch-soziologischne AG NF	111-143	21	\N
440	Colloques Phytosoc	613-631	13	\N
444	Naturschutz u. Landschaftspflege Nieders. Beih	1-83	4	\N
446	Tuexenia	139-183	1	\N
448	Mitteilungen d. Floristisch-soziologischne AG NF	157-202	18	\N
5901	Biodiversity & Ecology	290-290	4	1613-9801
453	Landschaftspflege u. Naturschutz Thüringen	10-19	3 (2)	\N
456	Jahrbuch Nass. Ver. Naturkunde	53-152	109	\N
5905	Biodiversity & Ecology	312-312	4	1613-9801
462	Schriftenreihe LÖLF NRW	1-68	8	\N
465	Ber. Naturforsch. Gesellschaft Bamberg	192 S	59 (2)	\N
468	Wiss. Z. PH Potsdam, Math.-Nat. Reihe	125-139	3 (1)	\N
471	Wiss. Z. PH Potsdam, Math.-nat.R	29-53	4 (1)	\N
474	Wiss. Z. PH Potsdam, Math.-Nat.R	151-166	7 (1/2)	\N
477	Wiss. Z. Universität Greifswald, Math.-Nat Reihe	277-305	7 (3/4)	\N
480	Pflanzensoziologie	1-321	12	\N
483	Mitteilungen AG Geobot. Schleswig-Holst. Hamburg	65-175	9	\N
486	Ber. d. Bayerischen Botanischen Gesellschaft	127-144	62	\N
489	Mitteilungen AG Geobot. Schleswig-Holst. Hamburg	1-322	45	\N
492	Hoppea	79-101	54	\N
494	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	169-246	20	\N
496	Naturschutz u. Landschaftspflege Baden-Württemberg	5-45	27/28	\N
498	Die Natur- u. Landschaftsschutzgebiete Baden-Württembergs	190-284	5	\N
5909	Biodiversity & Ecology	347-347	4	1613-9801
505	Hoppea	257-331	47	\N
508	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	161-271	62	\N
5925	Biodiversity & Ecology	321-322	4	1613-9801
515	Verhandlungen d. Bot. Vereins	31-75	7	\N
5929	Biodiversity & Ecology	358-358	4	1613-9801
520	Tuexenia	173-183	10	\N
528	Beitr. z. Naturkunde Niederschriften	1-91	38 (1)	\N
531	BSH NVN - Natur Special Report / Naturschutzverband Niedersachsen	1-100	18	\N
539	Abhandlungen d. Naturwissenschaftlichen Vereins i. Hamburg NF	15: Hamburg	15: Hamburg.	\N
544	Limnologica	101-163	4 (1)	\N
547	Natur u. Heimat	105-110	30 (4)	\N
549	Wiss. Z. Universität Halle, Math.-nat.R	883-928	3 (4)	\N
551	Wiss. Z. Universität Halle, Math.-nat.R	159-190	7 (1)	\N
553	Pflanzensoziologie	1-284	14	\N
555	Wiss. Z. Universität Halle	13 149-171	13 149-171. Halle (Saale).	\N
557	Ber. AG sächsischer Botaniker NF	155-181	5/6	\N
561	Archiv f. Naturschutz u. Landschaftsforschung	171-197	15 (3)	\N
569	Natur u. Naturschutz i. Mecklenburg-Vorpommern	2: Stralsund-Greifswald	2: Stralsund-Greifswald.	\N
572	Feddes Repertorium, Beih	71: A	71: A. Berlin.	\N
575	Schriften d. Naturwissenschaftlichen Vereins Schl.-Holstein	1-37	20 (2)	\N
583	Angewandte Pflanzensoziologie	1106-1144	2	\N
587	Gleditschia	245-300	21	\N
590	BSH NVN - Natur Special Report / Naturschutzverband Niedersachsen ?. Wardenburg.	BSH NVN - Natur Special Report / Naturschutzverband Niedersachsen ?	BSH NVN - Natur Special Report / Naturschutzverband Niedersachsen ?. Wardenburg.	\N
593	Feddes Repertorium, Beih	65-117	142	\N
603	Lauterbacher Sammlungen Beih	6-20	6	\N
607	Oberhessische naturwissenschaftliche Zeitschrift	67-75	41	\N
609	Oberhessische naturwissenschaftliche Zeitschrift	89-94	43	\N
615	Limnologica	147-201	6 (1)	\N
1156	Natur u. Heimat	39-45	14 (2)	\N
618	Agrar- u. Umweltforschung i. Baden-Württemberg	1-262	6	\N
620	Vegetation Limnologie, Naturschutz. Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg Beih	201-634	52 (2)	\N
624	Hercynia NF	23-45	36	\N
628	Hercynia NF	69-87	39	\N
631	Hercynia NF	53-99	34	\N
633	Wiss. Z. PH Potsdam, Math.-Naturwissenschaftliches Reihe	149-164	1 (2)	\N
5932	Biodiversity & Ecology	348-348	4	1613-9801
637	Limnologica	323-338	1 (4)	\N
639	Limnologica	493-515	4 (3)	\N
5934	Biodiversity & Ecology	349-349	4	1613-9801
5936	Biodiversity & Ecology	350-350	4	1613-9801
5943	Biodiversity & Ecology	399-400	4	1613-9801
648	Feddes Repertorium	357-427	85 (5/6)	\N
650	Ber. d. Bayerischen Botanischen Gesellschaft	283-302	61	\N
5949	Biodiversity & Ecology	365-365	4	1613-9801
657	Ber. der. Naturhistorischen Gesellschaft Hannover	109-158	133	\N
660	Mitteilungen d. Floristisch-soziologischne AG NF	26-58	4	\N
662	Feddes Repertorium, Beih	92-130	121	\N
664	Feddes Repertorium, Beih	1-95	114	\N
667	Ber. d. Bayerischen Botanischen Gesellschaft	67-122	39	\N
669	Jahrbuch Nass. Ver. Naturkunde	24-52	107	\N
672	Mainzer Naturwissenschaftliches Archiv	16-83	5/6	\N
5952	Biodiversity & Ecology	364-364	4	1613-9801
678	Ber. d. Bayerischen Botanischen Gesellschaft	64-124	28	\N
681	Mitteilungen Pollichia	221-321	74	\N
683	Beitr. z. Landespflege i. Rheinland-Pfalz	23-48	12	\N
5954	Biodiversity & Ecology	374-374	4	1613-9801
689	Oldenburger Jahrbuch	43-62	63	\N
692	Mitteilungen Staatsinst. Allg. Bot. Hamburg	137-203	11	\N
5961	Biodiversity & Ecology	398-398	4	1613-9801
698	Tuexenia	127-143	6	\N
701	Wiss. Z. Pädagog. Hochschule Potsdam, Math.-nat.R	95-150	7 (1/2)	\N
703	Gleditschia	235-244	20	\N
705	Gleditschia	245-272	20	\N
707	Gleditschia	273-302	20	\N
709	Gleditschia	303-326	20	\N
712	Archiv f. Naturschutz u. Landschaftsforschung	3-45	4 (1/2)	\N
715	Abhandlungen a. d. Landesmuseum f. Naturkunde z. Münster i. Westfalen	1-90	50 (2)	\N
717	Göttinger Florist. Rundbriefe	65-76	16 (3/4)	\N
719	Hoppea	325-344	44	\N
721	Botanik u. Naturschutz i. Hessen	5-71	6	\N
5964	Biodiversity & Ecology	446-446	4	1613-9801
726	Hess. Flor. Briefe	8-16	34 (1)	\N
728	Philippia	36-80	6 (1)	\N
731	Beitr. z. Naturkundlichen Forschung i. Südwestdeutschland	149-270	3 (2)	\N
733	Beitr. z. Naturkundlichen Forschung i. Südwestdeutschland	29-98	9 (2)	\N
735	Beitr. z. Naturkundlichen Forschung i. Südwestdeutschland	75-88	11 (2)	\N
5967	Biodiversity & Ecology	439-439	4	1613-9801
739	Beih Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	1-86	27	\N
742	Phytocoenologia	15-89	21 (1-2)	\N
746	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg ?.	Veröff	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg ?.	\N
752	Feddes Repertorium, Beih	194-231	135	\N
754	Feddes Repertorium, Beih	1-56	138	\N
756	Feddes Repertorium, Beih	5-55	137	\N
758	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	8, 91-113	8, 91-113. Rostock.	\N
760	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	10, 31-51	10, 31-51. Rostock.	\N
762	Pflanzensoziologie	1-324	13	\N
764	Tuexenia	245-265	7	\N
769	Decheniana	1-30	145	\N
781	Phyton	257-265	9 (3/4)	\N
787	Beitr. z. Naturkundlichen Forschung i. Südwestdeutschland	138-187	19	\N
789	Mitteilungen Bad. Landesver. Naturk. Naturschutz NF	477-484	8	\N
795	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	129-143	64/65	\N
800	Ber. AG sächsischer Botaniker NF	55-91	8	\N
802	Naturschutzarbeit i. d. Bezirken Halle Magdeburg	41-48	20 (2)	\N
804	Naturwissenschaftliche Beitr. Museum Dessau	67-78	3	\N
807	Wiss. Z. PH Potsdam, Math.-Naturwissenschaftliches Reihe	167-200	7 (1/2)	\N
810	Mitteilungen Natur- u. Umweltschutz Hamburg	60-155	6	\N
815	Die Heimat	41-49	87 (3)	\N
818	Beitr. z. Landespflege i. Rheinland-Pfalz	101-164	3	\N
821	Decheniana	71-98	139	\N
824	Abhandlungen d. Naturwissenschaftlichen Vereins Würzburg	177-246	30	\N
828	Hoppea	149-256	47	\N
831	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	14, 43-174	14, 43-174. Rostock.	\N
834	Z. Acker- Pflanzenbau	205-212	116 (2)	\N
836	Hercynia NF	352-393	11 (4)	\N
839	Pflanzensoziologie	1-137	8	\N
842	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	76-167	27/28	\N
844	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	46-61	27/28	\N
846	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Landschaftspflege Baden-Württemberg	31-67	31	\N
852	Hoppea	1-115	22 (1)	\N
854	Pflanzensoziologie	1-155	9	\N
857	Natur u. Heimat Beih	89 S	51	\N
861	Verhandlungen d. Bot. Vereins Berlin Brandenburg	145-175	133	\N
863	Tuexenia	273-301	5	\N
865	Angewandte Botanik	69-98	64	\N
868	Wiss. Z. PH Potsdam, Math.-Naturwissenschaftliches Reihe	201-230	7 (1/2)	\N
871	Ber. Naturwissenschaftlicher Verein f. Bielefeld & Umgegend ?.	Ber	Ber. Naturwissenschaftlicher Verein f. Bielefeld & Umgegend ?.	\N
874	Schriften d. Vereins f. Geschichte u. Naturgeschichte Baar	104-144	24	\N
877	Nachrichtenblatt Oberbergische AG Naturwissenschaftliches Heimatforschung	42-46	4	\N
879	Nachrichtenblatt Oberbergische AG Naturwissenschaftliches Heimatforschung	49-58	5	\N
881	Nachrichtenblatt Oberbergische AG Naturwissenschaftliches Heimatforschung	65-68	5	\N
883	Nachrichtenblatt Oberbergische AG Naturwissenschaftliches Heimatforschung	75-96	5	\N
885	Beitr. z. Naturkundlichen Forschung i. Südwestdeutschland	221-283	2 (2)	\N
888	Decheniana Beih	1-215	19	\N
5970	Biodiversity & Ecology	376-376	4	1613-9801
894	Veröff. Naturschutz ?.	Veröff	Veröff. Naturschutz ?.	\N
5976	Biodiversity & Ecology	408-408	4	1613-9801
5981	Biodiversity & Ecology	403-403	4	1613-9801
902	Pflanzensoziologie	1-278	6	\N
904	Bot. Jahrbuch	101-117	74 (1)	\N
906	Beitr. z. Landespflege i. Rheinland-Pfalz	9-99	3	\N
909	Flora u. Fauna i. Rheinland-Pfalz, Beih	1-136	4	\N
5984	Biodiversity & Ecology	405-405	4	1613-9801
5987	Biodiversity & Ecology	402-402	4	1613-9801
917	Landschaftspflege u. Vegetationskunde	1-123	3	\N
919	Tuexenia	455-478	14	\N
922	Landschaftspflege u. Vegetationskunde	59 S	2	\N
926	Mainzer Naturwissenschaftliches Archiv	277-312	24	\N
5990	Biodiversity & Ecology	406-406	4	1613-9801
932	Naturschutzparke, Mitteilungen Ver. Naturschutzpark eV	16-21	26	\N
935	Abhandlungen a. d. Landesmuseum f. Naturkunde z. Münster i. Westfalen	1-174	56 (3/4)	\N
938	Ber. d. Bayerischen Botanischen Gesellschaft	79-104	58	\N
5993	Biodiversity & Ecology	407-407	4	1613-9801
944	Bot. Jahrbücher f. Systematik, Pflanzengeschichte u. Pflanzengeographie	36-191	79 (1)	\N
947	Schriftenreihe d. Instituts f. Naturschutz Darmstadt	1-168	8 (1)	\N
6000	Biodiversity & Ecology	9-13	4	1613-9801
952	Ber. der. Naturhistorischen Gesellschaft Hannover	113-287	94-98	\N
954	Angewandte Pflanzensoziologie	64-98	8	\N
6004	Biodiversity & Ecology	436-436	4	1613-9801
958	Mitteilungen d. Floristisch-soziologischne AG NF	69-102	17	\N
960	Mitteilungen d. Floristisch-soziologischne AG NF	227-243	13	\N
963	Abhandlungen d. Naturwissenschaftlichen Vereins Würzburg	5-88	13	\N
965	Hoppea	5-190	36 (1)	\N
968	Mitteilungen d. Floristisch-soziologischne AG NF	87-110	22	\N
971	Ber. d. Bayerischen Botanischen Gesellschaft	1-75	62 (3)	\N
974	Abhandlungen a. d. Landesmuseum f. Naturkunde z. Münster i. Westfalen	223-227	48 (2/3)	\N
976	Abhandlungen a. d. Landesmuseum f. Naturkunde z. Münster i. Westfalen	1-88	49 (2)	\N
979	Ber. d. Bayerischen Botanischen Gesellschaft	13-97	27	\N
982	Landschaftspflege u. Vegetationskunde	1-128	4	\N
984	Hoppea	199-208	20	\N
987	Ber. d. Bayerischen Botanischen Gesellschaft	67-96	62	\N
990	Mitteilungen Thür. Bot. Gesellschaft Beih	1-50	2	\N
992	Mitteilungen d. Floristisch-soziologischne AG NF	253-268	19/20	\N
994	Abhandlungen d. Naturwissenschaftlichen Vereins i. Hamburg NF	1-123	20 (Suppl.)	\N
996	Abhandlungen d. Naturwissenschaftlichen Vereins i. Hamburg NF	187-212	25	\N
998	Tuexenia	145-193	6	\N
1000	Tuexenia	303-328	7	\N
1003	Hoppea	7-78	54	\N
1006	Drosera	1-5	76 (1)	\N
1008	Naturschutz u. Landschaftspflege Niedersachsen	3-168	9	\N
1010	Drosera	87-116	83 (2)	\N
6008	Biodiversity & Ecology	380-380	4	1613-9801
6022	Biodiversity & Ecology	63-76	4	1613-9801
1018	Ber. d. Bayerischen Botanischen Gesellschaft	47-52	54	\N
1021	Archiv f. Naturschutz u. Landschaftsforschung	45-64	30	\N
6025	Biodiversity & Ecology	293-293	4	1613-9801
1027	Mitteilungen d. Floristisch-soziologische AG NF	157-2	19/20	\N
1029	Naturschutz u. Landschaftspflege Niedersachsen	9-83	10	\N
1032	Beih Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	1-280	69	\N
1035	Abhandlungen d. Naturwissenschaftlichen Vereins z. Bremen	395-402	40 (4)	\N
1037	Schriftenreihe LÖLF NRW	1-228	5	\N
1040	Schriftenreihe f. Vegetationskunde	1-118	13	\N
1042	Bot. Rundbrief Bez. Neubrandenburg	1	1.	\N
1045	Botanischer Rundbrief Bez. Neubrandenburg.	Botanischer Rundbrief Bez	Botanischer Rundbrief Bez. Neubrandenburg.	\N
1049	Tuexenia	55-78	8	\N
1052	Hoppea	3-398	38	\N
1067	Abhandlungen u. Berichte d. Museums f. Naturkunde u. Vorgeschichte Magdeburg	233-262	6 (4)	\N
1069	Abhandlungen u. Berichte d. Museums f. Naturkunde u. Vorgeschichte Magdeburg	123-135	8	\N
1075	Ber. Int. Symp. Int. Vereinig. Vegetationskunde	5-20	1975	\N
1077	Vegetatio	105-116	85	\N
1085	Naturschutz u. naturkundliche Heimatforschung Bez. Halle u. Magdeburg	14-32	13	\N
1088	Botanik u. Naturschutz i. Hessen, Beih	117-125	2	\N
1092	Tuexenia	371-401	16	\N
1095	Tuexenia	153-206	18	\N
1097	Gleditschia	29-57	26	\N
1111	Schriftenreihe f. Vegetationskunde	1-330	15	\N
1113	Hercynia NF	106-114	15 (2)	\N
1116	Flora	23-67	146	\N
1118	Vegetatio	1-24	10 (1)	\N
1120	Mitteilungen d. Floristisch-soziologischne AG NF	181-208	8	\N
1122	Ber. d. Deutschen Botanischen Gesellschaft	253-270	71 (7)	\N
1125	Schriftenreihe f. Vegetationskunde	135-160	2	\N
1128	Jahrbuch St. Gallischen Naturwissenschaftlichen Gesellschaft	305-351	57 (2)	\N
1130	Ber. d. Schweizer Botanischen Gesellschaft	169-189	46	\N
1138	Tuexenia	205-233	11	\N
1140	Abhandlungen a. d. Landesmuseum f. Naturkunde z. Münster i. Westfalen	1-38	14 (3)	\N
1145	Verhandlungen d. Bot. Vereins Berlin Brandenburg	45-86	133	\N
1148	Philippia	331-380	6 (4)	\N
1152	Botanik u. Naturschutz i. Hessen	81-99	9	\N
1099		200		\N
6032	Biodiversity & Ecology	225-231	4	1613-9801
6037	Biodiversity & Ecology	95-103	4	1613-9801
6039	Biodiversity & Ecology	385-385	4	1613-9801
1174	Gleditschia	179-321	22	\N
1177	Kieler Exkursionsberichte zu vegetationskundlich-botanischen Exkusionen	1-175	5	\N
1179	Colloques Phytosoc	387-399	23	\N
1181	Scripta Geobotanica	1-246	6	\N
1184	Braunschweiger Naturkundliche Schriften	535-546	2 (3)	\N
6041	Biodiversity & Ecology	83-88	4	1613-9801
1189	Natur u. Heimat	17-24	48 (1)	\N
6045	Biodiversity & Ecology	390-390	4	1613-9801
6047	Biodiversity & Ecology	388-388	4	1613-9801
1198	Willdenowia Beih	5-204	6	\N
6051	Biodiversity & Ecology	389-389	4	1613-9801
1204	Phytocoenologia	161-211	27 (2)	\N
1207	Phytocoenologia	73-256	10 (1/2)	\N
6057	Biodiversity & Ecology	323-323	4	1613-9801
6061	Biodiversity & Ecology	395-395	4	1613-9801
1215	Abhandlungen d. Naturhistorischen Gesellschaft z. Nürnberg	1-92	29	\N
6064	Biodiversity & Ecology	304-304	4	1613-9801
1222	Mitteilungen d. Floristisch-soziologischne AG NF	105-109	21	\N
1226	Naturschutzarbeit i. d. Bezirken Halle Magdeburg	39-47	18 (1)	\N
6069	Biodiversity & Ecology	404-404	4	1613-9801
1232	Veröff. d. naturkundlichen Vereins Egge-Weser	192-215	2 (4)	\N
1235	Berliner Geogr. Abhandlungen	59-105	41	\N
1238	Natur u. Heimat	74-81	40 (3)	\N
6073	Biodiversity & Ecology	433-433	4	1613-9801
6080	Biodiversity & Ecology	300-301	4	1613-9801
6082	Biodiversity & Ecology	384-384	4	1613-9801
6085	Biodiversity & Ecology	310-310	4	1613-9801
1252	Phytocoenosis	227-244	7	\N
6090	Biodiversity & Ecology	393-393	4	1613-9801
6094	Biodiversity & Ecology	419-419	4	1613-9801
1263	Wissenschaftliche Mitteilungen Universität München - Meteorologisches Institut	5: München	5: München.	\N
1265	Wissenschaftliche Abhandlungen	26	26. 137 S. Akademie Verlag Berlin.	\N
6098	Biodiversity & Ecology	397-397	4	1613-9801
6102	Biodiversity & Ecology	371-371	4	1613-9801
6106	Biodiversity & Ecology	401-401	4	1613-9801
6109	Biodiversity & Ecology	410-410	4	1613-9801
6114	Biodiversity & Ecology	424-424	4	1613-9801
6116	Biodiversity & Ecology	296-296	4	1613-9801
6120	Biodiversity & Ecology	339-339	4	1613-9801
1287	Braunschweiger Geobotanische Arbeiten	1-216	2	\N
1289	Braunschweiger Naturkundliche Schriften	565-584	2(3)	\N
1291	Feddes Repertorium, Beih	280	44	\N
6124	Biodiversity & Ecology	298-298	4	1613-9801
6129	Biodiversity & Ecology	299-299	4	1613-9801
1299	Landschaftspflege u. Naturschutz Thüringen	47-55	11 (2)	\N
1301	Flora	468-510	168	\N
1303	Flora	177-215	169	\N
1305	Archiv f. Naturschutz u. Landschaftsforschung	219-248	13 (3)	\N
6134	Biodiversity & Ecology	440-440	4	1613-9801
6146	Biodiversity & Ecology	375-375	4	1613-9801
6151	Biodiversity & Ecology	318-318	4	1613-9801
6154	Biodiversity & Ecology	77-82	4	1613-9801
1315	Beitr. z. Vegetationskunde Hessen IV, Oberhessische Naturwissenschaftliche Zeitschrift	111-118	38	\N
1317	Oberhessische naturwissenschaftliche Zeitschrift	71-91	44	\N
6156	Biodiversity & Ecology	414-414	4	1613-9801
1322	Jahrbuch St. Gallischen Naturwissenschaftlichen Gesellschaft	1-144	61 (2)	\N
1327	Hess. Flor. Briefe	13-16	9 (100)	\N
1329	Schriftenreihe f. Vegetationskunde	1-196	7	\N
1331	Mitteilungen d. Floristisch-soziologischne AG NF	45-102	18	\N
1335	Mitteilungen d. Floristisch-soziologischne AG NF	266-269	9	\N
1337	Archiv f. Naturschutz u. Landschaftsforschung	142-163	1 (2)	\N
1339	Feddes Repertorium, Beih	167-227	139	\N
1348	Verhandlungen d. Bot. Vereins Berlin Brandenburg	177-217	133	\N
1351	Tuexenia	351-370	9	\N
1354	Ber. Naturwissenschaftlicher Verein f. Bielefeld & Umgegend	67-170	20	\N
1357	Mitteilungen d. Floristisch-soziologischne AG NF	59-76	4	\N
1360	Wiss. Z. Universität Halle, Math.-nat.R	177-208	6 (1)	\N
1364	Abhandlungen d. Sächsischen Akademie d. Wiss. z. Leipzig, math. phys. Klasse	1-138	49 (1)	\N
1366	Hercynia NF	352-372	5	\N
1368	Hercynia NF	225-257	6 (3)	\N
1370	Feddes Repertorium	437-455	81 (6/7)	\N
1372	Hercynia NF	8-98	1 (1)	\N
1381	Decheniana Beih	1-79	29	\N
1392	Wiss. Z. Universität Halle, Math.-nat.R	101-124	7	\N
1394	Botanik u. Naturschutz i. Hessen, Beih	90-99	2	\N
1399	Ber. der. Naturhistorischen Gesellschaft Hannover	177-201	133	\N
1401	Gleditschia	225-250	7	\N
1406	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	533-618	57/58	\N
1411	Naturschutz u. Landschaftspflege i. Brandenburg	27-3	3	\N
1417	Naturschutz Nordhessen	45-66	9	\N
1423	Ber. Naturwissenschaftlicher Verein f. Bielefeld & Umgegend	177-185	20	\N
1425	Archiv f. Naturschutz u. Landschaftsforschung	43-59	13 (1)	\N
1432	Ber. d. Bayerischen Botanischen Gesellschaft	151-172	45	\N
1462	Natur u. Heimat	172-175	28 (4)	\N
1468	Feddes Repertorium, Beih	260-350	135	\N
1470	Tuexenia	113-137	14	\N
1483	Wiss. Z. Universität Halle, Math.-nat.R	99-120	4 (1)	\N
1485	Wiss. Z. Universität Halle, Math.-nat.R	965-1012	8 (6)	\N
1333	Mitteilungen d. Floristisch-soziologischen AG N.F.	71-100	13	\N
1488	Feddes Repertorium, Beih	71-199	140	\N
6158	Biodiversity & Ecology	89-94	4	1613-9801
1503	Plesse-Archiv	183-283	14	\N
1506	Bot. Centralblatt Beih	577-598	57 B (3)	\N
6161	Biodiversity & Ecology	337-337	4	1613-9801
6164	Biodiversity & Ecology	342-342	4	1613-9801
6166	Biodiversity & Ecology	343-343	4	1613-9801
1517	Naturschutzarbeit i. d. Bezirken Halle Magdeburg	31-42	23 (1)	\N
6169	Biodiversity & Ecology	391-391	4	1613-9801
6172	Biodiversity & Ecology	308-308	4	1613-9801
6176	Biodiversity & Ecology	411-411	4	1613-9801
6183	Biodiversity & Ecology	363-363	4	1613-9801
1532	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	146-197	33	\N
1535	Ber. d. Reinhold-Tüxen-Gesellschaft	121-147	6	\N
1537	Carolinea	27-40	51	\N
1539	Ber. d. Bayerischen Botanischen Gesellschaft	189-219	62	\N
1541	Beih Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	1-212	17	\N
1543	Wiss. Z. Universität Rostock, Math.-nat.R	43-95	16 (1)	\N
1545	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	10, 73-101	10, 73-101. Rostock.	\N
1548	Natur u. Heimat	117-119	23	\N
1551	Hercynia NF	4-42	17 (1)	\N
6193	Biodiversity & Ecology	316-317	4	1613-9801
6196	Biodiversity & Ecology	442-442	4	1613-9801
6198	Biodiversity & Ecology	392-392	4	1613-9801
6202	Biodiversity & Ecology	288-288	4	1613-9801
1562	Mitteilungen d. Floristisch-soziologischne AG NF	239-257	18	\N
1564	Mitteilungen d. Floristisch-soziologischne AG NF	377-382	19/20	\N
6205	Biodiversity & Ecology	435-435	4	1613-9801
1570	Schriftenreihe f. Vegetationskunde	79-98	5	\N
6207	Biodiversity & Ecology	387-387	4	1613-9801
6209	Biodiversity & Ecology	409-409	4	1613-9801
6212	Biodiversity & Ecology	429-429	4	1613-9801
6214	Biodiversity & Ecology	315-315	4	1613-9801
6217	Biodiversity & Ecology	294-295	4	1613-9801
6220	Biodiversity & Ecology	344-344	4	1613-9801
6222	Biodiversity & Ecology	338-338	4	1613-9801
6225	Biodiversity & Ecology	340-340	4	1613-9801
6234	Biodiversity & Ecology	331-332	4	1613-9801
6281	Biodiversity & Ecology	265-274	4	1613-9801
6285	Biodiversity & Ecology	201-209	4	1613-9801
6295	Biodiversity & Ecology	15-24	4	1613-9801
6301	Biodiversity & Ecology	177-184	4	1613-9801
6354	Biodiversity & Ecology	211-224	4	1613-9801
6366	Biodiversity & Ecology	105-110	4	1613-9801
1615	Schriftenreihe f. Vegetationskunde	14 .Bonn-Bad Godesberg	14 .Bonn-Bad Godesberg.	\N
6369	Biodiversity & Ecology	434-434	4	1613-9801
6371	Biodiversity & Ecology	372-372	4	1613-9801
6373	Biodiversity & Ecology	373-373	4	1613-9801
6376	Biodiversity & Ecology	185-190	4	1613-9801
6379	Biodiversity & Ecology	287-287	4	1613-9801
6382	Biodiversity & Ecology	382-382	4	1613-9801
6385	Biodiversity & Ecology	381-381	4	1613-9801
6387	Biodiversity & Ecology	430-430	4	1613-9801
6392	Biodiversity & Ecology	233-241	4	1613-9801
6397	Biodiversity & Ecology	191-200	4	1613-9801
6400	Biodiversity & Ecology	292-292	4	1613-9801
6405	Biodiversity & Ecology	289-289	4	1613-9801
6407	Biodiversity & Ecology	353-353	4	1613-9801
6409	Biodiversity & Ecology	352-352	4	1613-9801
6411	Biodiversity & Ecology	362-362	4	1613-9801
6413	Biodiversity & Ecology	418-418	4	1613-9801
6415	Biodiversity & Ecology	386-386	4	1613-9801
1666	Archiv f. Forstwesen u. Landschaftsökologie	1-74	8, 1	\N
6419	Biodiversity & Ecology	361-361	4	1613-9801
1688	Archiv f. Forstwesen u. Landschaftsökologie	427-493	8, 5	\N
1693	Scripta Geobotanica	120 S	1	\N
1778	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	363-408	66	\N
1781	Hercynia NF	33-64	31 (1)	\N
1784	Naturschutzarbeit i. Berlin u. Brandenburg, Beih	5-63	4	\N
1786	Archiv f. Forstwesen u. Landschaftsökologie	388-408	7 (4/5)	\N
1788	Feddes Repertorium, Beih	56-140	138	\N
1791	Tuexenia	3-24	16	\N
1795	Tuexenia	25-38	16	\N
1799	Tuexenia	43-63	16	\N
1803	Tuexenia	311-336	16	\N
1805	Brandenburgische Naturschutzgebiete. Beitr. z. wissenschaftlichen Erschließung d. Naturschutzgebiete i. Berlin u. i. den Bezirken Potsdfam, Frankfurt (Oder) u. Cottbus	1-40	2	\N
1807	Abhandlungen u. Berichte d. Naturkundemuseums Görliz	1-102	39 (2)	\N
1810	Mitteilungen d. Floristisch-soziologischne AG i. Niedersachsen	1-135	5	\N
1822	Abhandlungen d. AG f. tier-u. pflanzengeographische Heimatforschung i. Saarland	101-122	8	\N
1830	Abhandlungen Delattinia	24-27	1998	\N
1836	Tuexenia	3-19	18	\N
1842	Naturwaldreservate i. Bayern	19-54	3	\N
1845	Naturwaldreservate i. Bayern	4:1-180	4:1-180. Eching.	\N
1847	Braunschweiger Naturkundliche Schriften	41-56	2(1)	\N
1849	Tuexenia	181-194	4	\N
1859	Feddes Repertorium	51-83	90 (1/2)	\N
1861	Tuexenia	475-486	10	\N
1863	Tuexenia	63-82	25	\N
1866	Tuexenia	183-194	25	\N
1872	Tuexenia	7-26	22	\N
1876	Tuexenia	93-119	25	\N
1880	Tuexenia	51-93	26	\N
1883	Tuexenia	95-119	26	\N
1886	Tuexenia	191-201	26	\N
1890	Tuexenia	203-221	26	\N
1893	Tuexenia	243-274	26	\N
1897	Tuexenia	339-353	26	\N
1899	Tuexenia	91-136	27	\N
1902	Tuexenia	167-194	27	\N
1905	Tuexenia	255-286	27	\N
1907	Tuexenia	185-227	28	\N
1911	Tuexenia	25-62	29	\N
1914	Tuexenia	63-82	29	\N
1918	Tuexenia	105-127	30	\N
1920	Feddes Repertorium	465-479	90 (7/8)	\N
1924	Waldoekologie online	83-99	3	\N
1927	Vegetatio	51-76	15 (1)	\N
6421	Biodiversity & Ecology	324-324	4	1613-9801
1932	Schriftenreihe f. Vegetationskunde	99-114	5	\N
6423	Biodiversity & Ecology	354-354	4	1613-9801
1941	Ber. d. Naturwissenschaftlichen Gesellschaft Bayreuth	20 (1988/89), S	20 (1988/89), S. 145 - 209.	\N
6427	Biodiversity & Ecology	438-438	4	1613-9801
6429	Biodiversity & Ecology	326-326	4	1613-9801
6432	Biodiversity & Ecology	412-412	4	1613-9801
6435	Biodiversity & Ecology	377-377	4	1613-9801
1955	Hoppea	393-454	54	\N
1958	Hoppea	345-411	45	\N
1961	Biol. Stud. Luckau	46-55	22	\N
6441	Biodiversity & Ecology	383-383	4	1613-9801
6444	Biodiversity & Ecology	447-447	4	1613-9801
1967	? Veröff	25	25. S. 117-130.	\N
1970	Tuexenia	153-171	19	\N
6447	Biodiversity & Ecology	305-305	4	1613-9801
6449	Biodiversity & Ecology	306-306	4	1613-9801
6452	Biodiversity & Ecology	314-314	4	1613-9801
6455	Biodiversity & Ecology	416-416	4	1613-9801
1983	Mitteilungen AG Geobot. Schleswig-Holst. Hamburg	1-92	54	\N
1986	Beih Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	1-248	79	\N
1989	Mitteilungen Pollichia	27-245	80	\N
6460	Biodiversity & Ecology	437-437	4	1613-9801
1995	Ber. d. Bayerischen Botanischen Gesellschaft Beih	1-155	6	\N
1998	Tuexenia	59-79	17	\N
6462	Biodiversity & Ecology	360-360	4	1613-9801
2004	Veröff. Geobot. Institut ETH Stiftung Rübel	1-115	2	\N
2007	Verhandlungen d. Bot. Vereins d. Prov. Brandenburg	68-74	104	\N
2010	Tuexenia	13-53	19	\N
2012	Hoppea	393-524	60	\N
6466	Biodiversity & Ecology	432-432	4	1613-9801
2018	Wiss. Z. PH Potsdam, Math.-nat.R	5-27	4 (1)	\N
2020	Botanik u. Naturschutz i. Hessen, Beih	153-162	2	\N
2022	Botanik u. Naturschutz i. Hessen, Beih	163-167	2	\N
2025	Schriftenreihe d. Instituts f. Landespflege Universität Freiburg	1-251	21	\N
6469	Biodiversity & Ecology	303-303	4	1613-9801
2031	Fortschritte i. d. Geologie v. Rheinland u. Westfalen	207-218	17	\N
2033	Veröff. Geobot. Institut Rübel Zürich	267-302	37	\N
2036	Abhandlungen aus dem Landesmuseum f. Naturkunde z. Münster in Westfalen	1-136	66 (4)	\N
2038	Ber. der. Naturhistorischen Gesellschaft Hannover	17-88	112	\N
6471	Biodiversity & Ecology	428-428	4	1613-9801
2043	Abhandlungen a. d. Landesmuseum f. Naturkunde z. Münster i. Westfalen	1-256	66 (1)	\N
2045	Ber. d. Naturwissenschaftlichen Gesellschaft Bayreuth	201-240	14	\N
2048	Tuexenia	303-315	9	\N
2050	Tuexenia	267-293	7	\N
6475	Biodiversity & Ecology	309-309	4	1613-9801
2056	Archiv f. Naturwissenschaftliches Dissertationen	192	12	\N
2059	Hercynia NF	173-189	31	\N
2063	Natur u. Landschaft	63-69	71	\N
2066	Wissenschaftliche Zeitschrift d. Pädagogischen Hochschule Potsdam, math.-nat. Reihe	85-128	5 (2)	\N
2068	Wissenschaftliche Zeitschrift d. Pädagogischen Hochschule Potsdam, math.-nat. Reihe	119-130	6 (1/2)	\N
2071	Mitteilungen AG Geobot. Schleswig-Holst. Hamburg	1-105	35	\N
6477	Biodiversity & Ecology	297-297	4	1613-9801
6479	Biodiversity & Ecology	357-357	4	1613-9801
6481	Biodiversity & Ecology	356-356	4	1613-9801
6484	Biodiversity & Ecology	426-426	4	1613-9801
2087	Hoppea	377-406	66	\N
2091	Beih Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	109-128	83	\N
2099	Limnologica	397-454, Berlin	7(2)	\N
2102	Hoppea	277-369	60	\N
2122	Hoppea	525-626	60	\N
2130	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	7-68	43	\N
2136	Ber. d. Bayerischen Botanischen Gesellschaft	187-214	54	\N
2141	Limnologica	379-408	11 (2)	\N
2144	Mitteilungen Ver. Forstl. Standortskunde u. Forstpflanzenzüchtung	27-46	36	\N
2149	Hoppea	371-464	67	\N
2153	Mitteilungen d. Floristisch-soziologischne AG NF	109-145	15/16	\N
2155	Tuexenia	185-209	1	\N
2157	Limnologica	475-545	1(5)	\N
2160	Ber. d. Bayerischen Botanischen Gesellschaft	77-83	43	\N
2163	Schriftenreihe d. Landesstelle f. Naturschutz u. Landschaftspflege NRW	1-207	4	\N
2166	Beih Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	29-95	80	\N
2169	Beitr. z. Naturdenkmalpflege	16 (2)	16 (2). 125 S. Neudamm.	\N
2171	Beitr. z. Naturkundlichen Forschung i. Südwestdeutschland	49-88	1	\N
2173	Ber. Naturforsch. Gesellschaft Freiburg i. Breisgau	213-247	34	\N
2175	Pflanzensoziologie	10, 564 S	10, 564 S. Jena.	\N
2183	Die Natur- u. Landschaftsschutzgebiete Baden-Württembergs	271-302	11	\N
2186	Englers Bot. Jahrbuch	355-422	77 (4)	\N
2192	Hoppea	323-341	50	\N
2195	Ber. ANL	27-63	11	\N
2197	Mitteilungen d. Floristisch-soziologischne AG NF	238-254	14	\N
2225	Wissenschaftliche Zeitschrift d. Pädagogischen Hochschule Potsdam, math.-nat. Reihe	383-396	25 (3)	\N
6486	Biodiversity & Ecology	359-359	4	1613-9801
6489	Biodiversity & Ecology	396-396	4	1613-9801
2232	Angewandte Pflanzensoziologie (Tüxen).	Angewandte Pflanzensoziologie (Tüxen)	Angewandte Pflanzensoziologie (Tüxen).	\N
6492	Biodiversity & Ecology	444-445	4	1613-9801
6495	Biodiversity & Ecology	413-413	4	1613-9801
2240	Landschaftspflege u. Naturschutz i. Thüringen	50-59	21 (3)	\N
6498	Biodiversity & Ecology	313-313	4	1613-9801
2245	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	132-139	43	\N
2247	Wissenschaftliche Zeitschrift d. Pädagogischen Hochschule Potsdam, Math.-Nat. Reihe	83-106	6 (1/2)	\N
2249	Phytocoenologia	307-448	17	\N
2252	Ber. Naturforsch. Gesellschaft Ausgburg	1-234	11	\N
2254	Naturschutzarbeit i. Mecklenburg	30-37	19 (3)	\N
2256	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	5-45	27/28	\N
2258	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	7-42	32	\N
2260	Limnologica	331-366	5(3)	\N
6500	Biodiversity & Ecology	307-307	4	1613-9801
2265	Wiss. Z. Universität Halle, Math. nat.R	165-224	9(1)	\N
6509	Biodiversity & Ecology	334-334	4	1613-9801
2269	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	452-465	24	\N
6513	Biodiversity & Ecology	327-327	4	1613-9801
2274	Schriftenreihe f. Naturschutz u. Landschaftspflege	40 S	2	\N
2277	Ber. Naturforsch. Gesellschaft Bamberg	65-84	59	\N
2279	Ber. d. Bayerischen Botanischen Gesellschaft	87-90	53	\N
2281	Hercynia NF	233-372	5 (3)	\N
6518	Biodiversity & Ecology	420-421	4	1613-9801
2286	Beih Bot. Centralblatt	137-207	61, Abt. B, (12)	\N
2288	Verhandlungen d. Bot. Vereins d. Provinz Brandenburg	72-132	78	\N
2290	Schriftenreihe f. Vegetationskunde	67-74	5	\N
2292	Wiss. Z. Universität Halle	17, 68 M, H	17, 68 M, H. 3, 377-439. Halle (Saale).	\N
2294	Landschaftspflege u. Naturschutz i. Thüringen	11-22	23 (1)	\N
2297	Schriftenreihe d. Fachhochschule Rottenburg	21	21. Rottenburg/Neckar; 165 S.	\N
2300	Tuexenia	381-406	11	\N
6520	Biodiversity & Ecology	41-48	4	1613-9801
6524	Biodiversity & Ecology	49-61	4	1613-9801
6535	Biodiversity & Ecology	125-132	4	1613-9801
2309	Beitr. z. Naturkundlichen Forschung i. Südwestdeutschland	259-287	34	\N
6541	Biodiversity & Ecology	243-253	4	1613-9801
2314	Beitr. z. Naturkundlichen Forschung i. Südwestdeutschland	295-327	34	\N
2316	Arch. Naturschutz Landsch.forschung	3-44	6, H. 1/2	\N
2318	Mitteilungen d. Osnabrücker Naturwissenschaftlichen Vereins	23, 131-184	23, 131-184. Osnabrück.	\N
6547	Biodiversity & Ecology	328-328	4	1613-9801
2324	Mitteilungen d. Floristisch-soziologischne AG NF	167-180	21	\N
6550	Biodiversity & Ecology	173-175	4	1613-9801
6552	Biodiversity & Ecology	149-160	4	1613-9801
6557	Biodiversity & Ecology	378-379	4	1613-9801
6559	Biodiversity & Ecology	325-325	4	1613-9801
6564	Biodiversity & Ecology	133-140	4	1613-9801
2341	Hoppea	409-452	50	\N
6567	Biodiversity & Ecology	311-311	4	1613-9801
6576	Biodiversity & Ecology	167-171	4	1613-9801
2349	Carolinea	5-16	46	\N
6582	Biodiversity & Ecology	255-264	4	1613-9801
6584	Journal of Vegetation Science	11791186	21	1100-9233
2356	Tuexenia	161-182	13	\N
2359	Tuexenia	203-245	13	\N
2362	Tuexenia	293-342	13	\N
2364	Beitr. z. Naturdenkmalpflege	281-292	14	\N
2370	Mitteilungen d. Vereins f. forstliche Standortskunde u. Forstpflanzenzüchtung	43-49	32(2)	\N
2373	Wiss. Z. Universität Halle	13 109-148	13 109-148. Halle (Saale).	\N
2376	Angewandte Pflanzensoziologie	1-90	1956	\N
2381	Abhandlungen a. d. Landesmuseum f. Naturkunde z. Münster i. Westfalen	1-103	64(1)	\N
2387	Hoppea	5-314	51	\N
2389	Decheniana Beih	27-41	37	\N
2392	Allgemeine Forst- u. Jagdzeitung	216-219	128	\N
2396	Hoppea	255-272	48	\N
2400	Hoppea	273-318	55	\N
2405	Hoppea	145-215	57	\N
2407	Abhandlungen u. Berichte d. Naturkundemuseums Görlitz	57-77	37 (1)	\N
2411	Botanik u. Naturschutz i. Hessen	64-76	4	\N
2414	Abhandlungen u. Berichte d. Naturkundemuseums Görlitz	77-109	35	\N
2419	Naturschutz u. Landschaftspflege Nieders	1-85	38	\N
2422	Beitr. z. Naturdenkmalpflege	108-214	14	\N
2427	Verhandlungen d. Bot. Vereins d. Prov. Brandenburg	24-62	106	\N
2429	Verhandlungen d. Bot. Vereins Berlin Brandenburg	111-158	130	\N
2431	Mainzer Naturwissenschaftliches Archiv	135-200	25	\N
2433	Mitteilungen d. Floristisch-soziologischne AG NF	141-144	9	\N
2437	Verhandlungen d. Bot. Vereins Berlin Brandenburg	79-110	130	\N
2446	Wiss. Z. Universität Jena, math.-Nat. Reihe	537-583	34	\N
2448	Hoppea	403-414	55	\N
2450	Mitteilungen d. Floristisch-soziologischne AG NF	91-98	5	\N
2452	Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	24-62	41	\N
2456	Tuexenia	71-94	23	\N
2487	Z. Hydrol	131-175	4	\N
2502	Veröff. Geobot. Institut Rübel	32, 318 S., Zürich	32, 318 S., Zürich.	\N
2505	Tuexenia	135-162	2	\N
2509	Hoppea Denkschrift	343-452	56	\N
2517	Hoppea Denkschrift	191-300	54	\N
2523	Ber. d. Bayerischen Botanischen Gesellschaft	123-158	71	\N
2528	Forstarchiv	48-55	27	\N
6604	journal	7-78	3	\N
2544	Ber. d. Bayerischen Botanischen Gesellschaft	71-84	30	\N
2547	Hoppea Denkschrift	301-349	54	\N
2593	Tuexenia	339-351	29	\N
2627	Crunoecia	5-117	1	\N
2684	Tuexenia	55-106	19	\N
2686	Archiv f. Forstwesen u. Landschaftsökologie	5, 46-95	5, 46-95. Berlin.	\N
2688	Schriftenreihe f. Vegetationskunde	21-78	8	\N
2691	Hoppea	565-573	54	\N
2699	Hoppea	217-330	57	\N
2701	Naturschutz u. naturkundliche Heimatforschung Bez. Halle u. Magdeburg	51-59	2	\N
2704	Hoppea	151-168	55	\N
2706	Tuexenia	17-52	14	\N
2710	Thaiszia	81-98	3	\N
2712	Beih Veröff. d. Landesstelle f. Naturschutz u. Landschaftspflege Baden-Württemberg	55-190	73	\N
2714	Ber. Freiburger Forstliche Forschung	52-74	29	\N
2716	Mitteilungen Naturwissenschaftliches Museum Aschaffenburg	3-354	19	\N
2720	Mitteilungen d. Floristisch-soziologischne AG NF	88-126	11/12	\N
2737	Gleditschia	79-96	27	\N
2747	Naturschutz Waldeck-Frankenberg, Sonderheft	62-70	1	\N
2756	Hercynia NF	85-95	8(2)	\N
2780	Abhandlungen a. d. Landesmuseum f. Naturkunde z. Münster i. Westfalen	1-75	47 (4)	\N
2783	Hercynia NF	195-258	27 (3)	\N
2785	Forschungen z. deutsch. Landeskunde	1-164	161	\N
2789	Stud. Phytolog. Nov. i. Honorem Jubilantis A.O. Horvat: 55-67. Pécs.	Stud	Stud. Phytolog. Nov. i. Honorem Jubilantis A.O. Horvat: 55-67. Pécs.	\N
2791	Phytocoenologia	637-650	23	\N
2797	Abhandlungen a. d. Landesmuseum f. Naturkunde z. Münster i. Westfalen	15 (3) 1-92	15 (3) 1-92. Münster.	\N
2800	Kieler Nat. Pflanzenk. Schleswig-Holst. Hamburg	16 (3 / 4) 37-67	16 (3 / 4) 37-67. Kiel.	\N
2803	Docum. Phytosociol. NS	235-252	1	\N
2811	Schriftenreihe der Landesanstalt Wasser u. Abfall Nordrhein-Westfalen	1-63	37	\N
2816	Tuexenia	213-231	1	\N
2819	Drosera	57-74	2/1981	\N
2822	Mitteilungen AG Geobot. Schleswig-Holst. u. Hamburg	1 - 72	40	\N
2824	Mitteilungen AG Geobot. Schleswig-Holst. u. Hamburg	1-441	48	\N
2831	Hercynia NF	193 -211	40	\N
2833	Verhandlungen d. Bot. Vereins Berlin Brandenburg	141-151	132	\N
2851	Botanik u. Naturschutz i. Hessen Beih	1-168	7	\N
2853	Hoppea	91-147	47	\N
2731		66-81		\N
2729		46-62		\N
2733		56-72		\N
2735		53-71		\N
6606	Tuexenia	463–474	25	\N
2989	Hoppea Denkschrift	475-876	64	\N
2991	Hoppea Denkschrift	325-350	66	\N
2996	Decheniana	191-218	150	\N
2999	Botanik u. Naturschutz i. Hessen	111-133	20	\N
3002	Waldschutzgebiete i. Baden-Württeberg	11-27	2	\N
3004	Drosera	121-139	2008 (1/2)	\N
3015	Carolinea	67-78	50	\N
3018	Carolinea	69-84	48	\N
3020	Carolinea	45-68	48	\N
3023	Archiv f. Naturwissenschaftliches Dissertationen	1-164	13	\N
3025	Tuexenia	241-304	19	\N
3029	Tuexenia	185-195	10	\N
3039	Naturschutzarbeit i. Mecklenburg-Vorpommern	66-74	44	\N
3042	Geobot. Mitteilungen	1-97	6	\N
3045	Scripta Geobotanica	1-66	14	\N
3053	Geol. Jahrbuch	33-70	11	\N
3056	Mitt.  Florist.-soziol. Arbeitsgem. NF	145-150	5	\N
3060	Mitt. Florist.-soziol. Arbeitsgem. NF	101-121	13	\N
3062	Abhandlungen Westfäl. Museum f. Naturkunde	44 (3) : 1-108	44 (3) : 1-108. Münster.	\N
3064	Tuexenia	497-513	10	\N
3067	Abhandlungen Westfäl. Museum f. Naturkunde	53 (1/2) : 1-313	53 (1/2) : 1-313. Münster.	\N
3073	Landschaftentw. Umweltforschutz Sonderheft	1-245	1	\N
3079	Ber. d. Naturwiss. Vereins Bielefeld	377-390	30	\N
3082	Oldenburger Jahrbuch	325-3	81	\N
3084	Rotenburger Schriften	3-52	26	\N
3090	Mitt. Florist.-soziol. Arbeitsgem. NF	222-226	8	\N
3162	Tuexenia	25-40	10	\N
3164	Tuexenia	197-222	10	\N
3171	Ber. Naturforsch. Gesellschaft Bamberg	1-102	57	\N
3173	Hoppea	30	30. Regensburg.	\N
3176	Hoppea	30	30. Regensburg.	\N
3178	Ber. d. Bayerischen Botanischen Gesellschaft	289-299	66/67	\N
3181	Göttinger Florist. Rundbriefe	76-84	16 (3/4)	\N
3184	Tuexenia	393-404	19	\N
3187	Ber. d. Bayerischen Botanischen Gesellschaft Beih	1-239	8	\N
3190	Carolinea	109-120	48	\N
3194	Phytocoenologia	83-128	18 (1)	\N
3196	Der Bayerische Wald	1-19	23	\N
3209	Ber. d. Arbeitsgemeinschaft sächsischer Botanisker NF	123-135	17	\N
3212	Tuexenia	169-208	30	\N
3214	Ber. d. Bayerischen Botanischen Gesellschaft	115-133	66/67	\N
3218	Berichte Freiburger Forstliche Forschung	46-56	12	\N
3220	Mitteilungen zur floristischen Kartierung in Sachsen-Anhalt	31-44	4	\N
3223	Tuexenia	151-182	25	\N
3225	Tuexenia	83-92	25	\N
3228	Tuexenia	425-443	25	\N
3230	Tuexenia	325-338	26	\N
3232	Tuexenia	137-141	27	\N
3235	Tuexenia	195-219	27	\N
3238	Tuexenia	215-235	29	\N
3242	Tuexenia	149-168	30	\N
3246	Tuexenia	209-229	30	\N
3248	Tuexenia	271-288	30	\N
3252	Tuexenia	289-318	30	\N
3255	Tuexenia	139-159	15	\N
3257	Braunschweiger Naturkundliche Schriften	205-221	5 (1)	\N
3259	Hoppea	30	30. Regensburg.	\N
3263	Tuexenia	447-467	19	\N
3266	Tuexenia	179-191	19	\N
3271	Tuexenia	281-318	32	\N
3275	Tuexenia	105-118	32	\N
3279	Tuexenia	119-140	32	\N
3294	Ber. d. Bayerischen Botanischen Gesellschaft	127-146	79	\N
3297	Verhandlungen d. Bot. Vereins der Provinz Brandenburg	59-152	79	\N
3300	Jb. Naturw. Verein Fstm. Lbg	211-220	39	\N
3304	Hess. Flor. Briefe	10-15	44 (1)	\N
3306	Gleditschia	257-259	13 (2)	\N
5355	Hercynia N.F	31-57	47	\N
3322	Schriftenreihe aus dem Nationalpark Harz	9, 120 S	9, 120 S.	\N
3324	Hercynia NF	219-238	41	\N
3326	Hercynia NF	111-124	45	\N
3331	Hercynia NF	73-92	44	\N
3360	Ber. d. Bayerischen Botanischen Gesellschaft	97-100	79	\N
3363	Hess. Flor. Briefe	1-4	48	\N
3365	Hess. Flor. Briefe	1-2	57	\N
3368	Verhandlungen d. Bot. Vereins d. Prov. Brandenburg	90-120	77	\N
3372	Hess. Flor. Briefe	1-4	67	\N
3376	Ber. d. Reinhold-Tüxen-Ges	127-143	8	\N
3379	Ber. d. Reinhold-Tüxen-Ges	179-193	6	\N
3382	Ber. d. Reinhold-Tüxen-Ges	195-227	6	\N
3385	Abhandlungen aus dem Westfälischen Museum für Naturkunde	31-42	65 (1/2)	\N
3387	Abhandlungen aus dem Westfälischen Museum für Naturkunde	5-38	61 (3)	\N
3390	Abhandlungen aus dem Westfälischen Museum für Naturkunde	39-75	61(3)	\N
3392	Abhandlungen aus dem Westfälischen Museum für Naturkunde	101-112	65 (1/2)	\N
3405	Tuexenia	151-184	28	\N
3408	Tuexenia	89-106	34	\N
3410	Tuexenia	107-130	34	\N
3414	Tuexenia	133-163	33	\N
3416	Tuexenia	49-92	33	\N
3421	Tuexenia	259-283	33	\N
3423	Tuexenia	73-85	31	\N
3427	Tuexenia	39-57	31	\N
3431	Tuexenia	141-166	32	\N
3439	Scripta Geobotanica	122 S	19	\N
3445	Abhandlungen aus dem Westfälischen Museum für Naturkunde	65 (3	65 (3)	\N
3447	Tuexenia	91-112	21	\N
3449	Wissenschaftliche Zeitschrift der MLU Halle-Wittenberg	1291-1316	5 (6)	\N
3452	Folia Geobotanica et Phytotaxonomica	1-48	27	\N
3457	Abhandlungen aus dem Westfälischen Museum für Naturkunde	62(3	62(3)	\N
3459	Abhandlungen aus dem Westfälischen Museum für Naturkunde	64(4	64(4)	\N
3462	Tuexenia	113-131	21	\N
3464	Beiträge zur naturkundlichen Forschung in Südwestdeutschland	34, 329-343	34, 329-343	\N
3467	Abhandlungen aus dem Westfälischen Museum für Naturkunde	58 (1	58 (1)	\N
3470	Tuexenia	31-53	32	\N
3473	Tuexenia	9-24	33	\N
3476	Tuexenia	247-270	34	\N
3481	Tuexenia	145-162	34	\N
3486	Augsburger Ökologische Schriften	79-108	2	\N
3488	Ber. ANL	181-214	16	\N
3494	Natur u. Heimat	117-125	43 (4)	\N
3515	Ber. Bayer. Bot. Ges	109-130	84	\N
3523	Flora	446-455	209	\N
3528	Limnologica	485-500	13	\N
3530	Limnologica	115-152	14	\N
3532	Bot. Rundbrief Bez. Neubrandenburg	9- 11	21	\N
3535	Naturschutzarbeit i. Mecklenburg	24-28	16 (3)	\N
3544	Naturschutzarbeit i. Mecklenburg	84-86	26	\N
3547	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	75-138	5	\N
3549	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	139-183	5	\N
3551	Bot. Rundbrief Bez. Neubrandenburg	5, 51-52	5, 51-52.	\N
3553	Bot. Rundbrief Bez. Neubrandenburg	7, 35-40	7, 35-40.	\N
3555	Naturschutzarbeit i. Mecklenburg	20 (3), 27-36	20 (3), 27-36.	\N
3557	Bot. Rundbrief Bez. Neubrandenburg	9, 81-82	9, 81-82.	\N
3559	Bot. Rundbrief Bez. Neubrandenburg	9, 85-87	9, 85-87.	\N
3561	Bot. Rundbrief Bez. Neubrandenburg	10, 16-26	10, 16-26.	\N
3563	Gleditschia	161-177	11	\N
3565	Bot. Rundbrief Bez. Neubrandenburg	17, 9-14	17, 9-14.	\N
3567	Bot. Rundbrief Bez. Neubrandenburg	18, 43-47	18, 43-47.	\N
3569	Bot. Rundbrief Bez. Neubrandenburg	20, 43-48	20, 43-48.	\N
3573	Gleditschia	211-263	15	\N
3577	Zesz. Nauk. AR w Szezecinie	5-12	139	\N
3579	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	31, 5-16	31, 5-16. Rostock.	\N
3585	Bot. Rundbrief Bez. Neubrandenburg	10, 77-82	10, 77-82.	\N
3587	Wiss. Z. Universität Rostock, Math.-nat.R	17, 325-347	17, 325-347.	\N
3589	Wiss. Z. Universität Rostock, Math.-nat.R	20, 53-59	20, 53-59.	\N
3591	Wiss. Z. Universität Rostock, Math.-nat.R	20, 61-67	20, 61-67.	\N
3593	Naturschutzarbeit i. Mecklenburg	19, 30-37	19, 30-37.	\N
3595	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	17, 111-134	17, 111-134. Rostock.	\N
3597	Gleditschia	157-163	5	\N
3599	Bot. Rundbrief Bez. Neubrandenburg	7, 3-13	7, 3-13.	\N
3601	Bot. Rundbrief Bez. Neubrandenburg	7, 14-23	7, 14-23.	\N
3603	Bot. Rundbrief Bez. Neubrandenburg	9, 17-26	9, 17-26.	\N
3605	Limnologica	11, 379-408	11, 379-408. Berlin.	\N
3607	Gleditschia	265-275	6	\N
3609	Feddes Repertorium	89, 475-492	89, 475-492. Berlin.	\N
3611	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	18, 99-102	18, 99-102. Rostock.	\N
3613	Natur u. Naturschutz i. Mecklenburg-Vorpommern	15, 17-61	15, 17-61.	\N
3615	Natur u. Naturschutz i. Mecklenburg-Vorpommern	15, 81-89	15, 81-89.	\N
3617	Gleditschia	259-271	7	\N
3619	Bot. Rundbrief Bez. Neubrandenburg	10, 83-86	10, 83-86.	\N
3621	Limnologica	13, 473-484	13, 473-484. Berlin.	\N
3623	Limnologica	14, 153-165	14, 153-165. Berlin.	\N
3625	Naturschutzarbeit i. Mecklenburg	25, 83-88	25, 83-88.	\N
3627	Gleditschia	241-267	10	\N
3629	Limnologica	15, 179-189	15, 179-189. Berlin.	\N
3631	Feddes Repertorium	281-324	100	\N
3633	Feddes Repertorium	199-216	102	\N
3635	Feddes Repertorium	217-317	102	\N
3639	Bot. Rundbrief Bez. Neubrandenburg	11, 69-80	11, 69-80.	\N
3642	Natur u. Naturschutz i. Mecklenburg-Vorpommern	15, 63-72	15, 63-72.	\N
3644	Bot. Rundbrief Bez. Neubrandenburg	11, 75-80	11, 75-80.	\N
3646	Bot. Rundbrief Bez. Neubrandenburg	13, 43-46	13, 43-46.	\N
3648	Bot. Rundbrief Bez. Neubrandenburg	20, 17-28	20, 17-28.	\N
3650	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	29, 112-128	29, 112-128. Rostock.	\N
3653	Bot. Rundbrief Bez. Neubrandenburg	6, 3-7	6, 3-7.	\N
3656	Wiss. Z. Universität Rostock, Math.-nat.R	13, 219-224	13, 219-224.	\N
3660	Naturschutzarbeit i. Mecklenburg	9 (3), 41-42	9 (3), 41-42.	\N
3662	Wiss. Z. Päd. Hochsch. Potsdam, Math.-nat.R	83-106	6	\N
3667	Pflanzensoziologie	12, S	12, S. 1-321. Fischer, Jena.	\N
3669	Mitteilungen d. Floristisch-soziologischne AG NF	14, 231-237	14, 231-237.	\N
3672	Bot. Rundbrief Bez. Neubrandenburg	13, 3-12	13, 3-12.	\N
3675	Bot. Rundbrief Bez. Neubrandenburg	7, 57-58	7, 57-58.	\N
3677	Bot. Rundbrief Bez. Neubrandenburg	10, 68-71	10, 68-71.	\N
3680	Bot. Rundbrief Bez. Neubrandenburg	11, 3-6	11, 3-6.	\N
3683	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	17, 95-109	17, 95-109. Rostock.	\N
3686	Bot. Rundbrief Bez. Neubrandenburg	11, 83-84	11, 83-84.	\N
3689	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	15, 116-121	15, 116-121. Rostock.	\N
3692	Bot. Rundbrief Bez. Neubrandenburg	17, 59-60	17, 59-60.	\N
3695	Bot. Rundbrief Bez. Neubrandenburg	13, 33-37	13, 33-37.	\N
3698	Naturschutzarbeit i. Mecklenburg	13 (1/2), 26-30	13 (1/2), 26-30.	\N
3702	Wiss. Mitteilungen d. Instituts f. Geogr. u. Geoökol. Leipzig	14, 17-59	14, 17-59.	\N
3706	Bot. Rundbrief Bez. Neubrandenburg	14, 77-82	14, 77-82.	\N
3708	Archiv f. Forstwesen u. Landschaftsökologie	7, 502-558	7, 502-558. Berlin.	\N
3711	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	5, 285- 335	5, 285- 335. Rostock.	\N
3713	Abhandlungen d. Naturwissenschaftlichen Vereins i. Hamburg NF	15	15. Hamburg.	\N
3717	Wiss. Z. Päd. Hochsch. Güstrow, Reihe Biol./Chemie/Polytechn	6, 19-21	6, 19-21.	\N
3719	Wiss. Z. Päd. Hochsch. Güstrow, Math.-nat.R	1976, H.1, 155-157	1976, H.1, 155-157.	\N
3721	Bot. Rundbrief Bez. Neubrandenburg	9, 88-90	9, 88-90.	\N
3723	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	14, 69-83	14, 69-83. Rostock.	\N
3725	Botanischer Rundbrief f. Mecklenburg-Vorpommern	22, 43-44	22, 43-44.	\N
3728	Bot. Rundbrief Bez. Neubrandenburg	18, 74	18, 74.	\N
3731	Wiss. Z. Päd. Hochsch. Güstrow, Math.-nat.R	19, 105-113	19, 105-113.	\N
3734	Natur u. Naturschutz i. Mecklenburg-Vorpommern	6, 139-151	6, 139-151.	\N
3738	Bot. Rundbrief Bez. Neubrandenburg	14, 83-86	14, 83-86.	\N
3740	Archiv f. Naturschutz u. Landschaftsforschung	8, 21-47	8, 21-47. Berlin.	\N
3742	Natur u. Naturschutz i. Mecklenburg-Vorpommern	10, 25-33	10, 25-33.	\N
3744	Vegetatio	61, 97-103	61, 97-103.	\N
3747	Wiss. Mitteilungen d. Instituts f. Geogr. u. Geoökol. Leipzig	14, 61-105	14, 61-105.	\N
3751	Naturschutzarbeit i. Mecklenburg	4, 20-35	4, 20-35.	\N
3754	Naturschutzarbeit i. Mecklenburg	16 (1/2), 49-52	16 (1/2), 49-52.	\N
3757	Feddes Repertorium, Beih	138, 161- 214	138, 161- 214. Berlin.	\N
3759	Naturschutzarbeit u. naturkundliche Heimatforschung i. den Bezirken Rostock, Schwerin u. Neubrandenburg	4, 27-30	4, 27-30.	\N
3761	Naturschutzarbeit u. naturkundliche Heimatforschung i. den Bezirken Rostock, Schwerin u. Neubrandenburg	2, 19-23	2, 19-23.	\N
3763	Naturschutzarbeit u. naturkundliche Heimatforschung i. den Bez. Rostock, Schwerin u. Neubrandenburg	2, 14-17	2, 14-17.	\N
3765	Naturschutzarbeit u. naturkundliche Heimatforschung i. den Bezirken Rostock, Schwerin u. Neubrandenburg	6, 22-27	6, 22-27.	\N
3767	Naturschutzarbeit u. naturkundliche Heimatforschung i. den Bezirken Rostock, Schwerin u. Neubrandenburg	5, 35-43	5, 35-43.	\N
3769	Archiv f. Naturschutz u. Landschaftsforschung	1, 54-84	1, 54-84. Berlin.	\N
3771	Naturschutzarbeit i. Mecklenburg	4, 66-73	4, 66-73.	\N
3773	Limnologica	1, 475-545	1, 475-545. Berlin.	\N
3775	Naturschutzarbeit i. Mecklenburg	6 (2/3), 23-25	6 (2/3), 23-25.	\N
3777	Naturschutzarbeit i. Mecklenburg	7 (1/2), 17-21	7 (1/2), 17-21.	\N
3779	Natur u. Naturschutz i. Mecklenburg-Vorpommern	2, 1-154	2, 1-154.	\N
3781	Naturschutzarbeit i. Mecklenburg	9 (2), 32-35	9 (2), 32-35.	\N
3783	Naturschutzarbeit i. Mecklenburg	9 (1), 48-50	9 (1), 48-50.	\N
3785	Natur u. Naturschutz i. Mecklenburg-Vorpommern	6, 111-138	6, 111-138.	\N
3787	Naturschutzarbeit i. Mecklenburg	11 (1), 37-38	11 (1), 37-38.	\N
3791	Naturschutzarbeit i. Mecklenburg	16 (1/2), 57-58	16 (1/2), 57-58.	\N
3793	Naturschutzarbeit i. Mecklenburg	17, 10-18	17, 10-18.	\N
3795	Naturschutzarbeit i. Mecklenburg	18 (1), 16-22	18 (1), 16-22.	\N
3797	Naturschutzarbeit i. Mecklenburg	18 (2/3), 17-23	18 (2/3), 17-23.	\N
3799	Naturschutzarbeit i. Mecklenburg	19, 49-52	19, 49-52.	\N
3801	Bot. Rundbrief Bez. Neubrandenburg	10, 72-76	10, 72-76.	\N
3803	Natur u. Naturschutz i. Mecklenburg-Vorpommern	18, 63-71	18, 63-71.	\N
3805	Naturschutzarbeit i. Mecklenburg	26, 5-12	26, 5-12.	\N
3807	Archiv f. Naturschutz u. Landschaftsforschung	25, 223-236	25, 223-236. Berlin.	\N
3809	Naturschutzarbeit i. Mecklenburg	29 (1), 2-16	29 (1), 2-16.	\N
3811	Hercynia NF	321-328	24	\N
3814	Naturschutzarbeit i. Mecklenburg	27, 57-71	27, 57-71.	\N
3817	Naturschutzarbeit u. naturkundliche Heimatforschung i. den Bez. Rostock, Schwerin u. Neubrandenburg	3, 2-24	3, 2-24.	\N
3820	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	30, 28-35	30, 28-35. Rostock.	\N
3822	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	8, 59-77	8, 59-77. Rostock.	\N
3824	Feddes Repertorium	589-605	84	\N
3826	Wiss. Z. Universität Rostock, Math.-nat.R	24, 805-810	24, 805-810.	\N
3828	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	22, 85-89	22, 85-89. Rostock.	\N
3830	Wiss. Abhandlungen d. Deutschen Akademie d. Landwirtschaftswissenschaften d. DDR	60, 55-178	60, 55-178.	\N
3832	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	22, 5-51	22, 5-51. Rostock.	\N
3834	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	14, 72-114	14, 72-114. Rostock.	\N
3836	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	26, 86- 111	26, 86- 111. Rostock.	\N
3838	Botanischer Rundbrief f. Mecklenburg-Vorpommern	22, 19-32	22, 19-32.	\N
3840	Botanischer Rundbrief f. Mecklenburg-Vorpommern	23, 47-60	23, 47-60.	\N
3842	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	19, 105-134	19, 105-134. Rostock.	\N
3844	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	22, 53-67	22, 53-67. Rostock.	\N
3847	Archiv f. Naturschutz u. Landschaftsforschung	287-302	13	\N
3850	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	11, 79- 84	11, 79- 84. Rostock.	\N
3853	Archiv f. Naturschutz u. Landschaftsforschung	14, 139- 146	14, 139- 146. Berlin.	\N
3856	Naturschutzarbeit i. Mecklenburg	18 (1), 26-27	18 (1), 26-27.	\N
3860	Bot. Rundbrief Bez. Neubrandenburg	11, 95- 97	11, 95- 97.	\N
3862	Mitteilungen d. Floristisch-soziologischne AG NF	8, 148-164	8, 148-164.	\N
3864	Wiss. Z. E.-M.-Arndt-Universität Greifswald, Math.-nat.R	11, 165-170	11, 165-170.	\N
3868	Archiv f. Naturschutz u. Landschaftsforschung	6, 103-121	6, 103-121. Berlin.	\N
3870	Z. f. Landeskultur	7, 239-249	7, 239-249.	\N
3872	Natur u. Naturschutz i. Mecklenburg-Vorpommern	7, 77-114	7, 77-114.	\N
3874	Z. Landeskultur	11, 133-144	11, 133-144.	\N
3876	Wiss. Z. E.-M.-Arndt-Universität Greifswald, Math.-nat.R	15, 9-40	15, 9-40.	\N
3878	Naturschutzarbeit i. Mecklenburg	19, 52-55	19, 52-55.	\N
3880	Bot. Rundbrief Bez. Neubrandenburg	13, 53-61	13, 53-61.	\N
3882	Wiss. Mitteilungen d. Instituts f. Geogr. u. Geoökol. Leipzig	14, 105- 133	14, 105- 133.	\N
3884	Wiss. Mitteilungen d. Instituts f. Geogr. u. Geoökol. Leipzig	24, 19-104	24, 19-104.	\N
3887	Gleditschia	85-106	12	\N
3891	Natur u. Naturschutz i. Mecklenburg-Vorpommern	18, 75-92	18, 75-92.	\N
3893	Natur u. Naturschutz i. Mecklenburg-Vorpommern	19, 51-80	19, 51-80.	\N
3896	Soziologie, Syndynamik, Synökologie. Gleditschia	16, 33-48	16, 33-48.	\N
3899	Wiss. Z. Techn. Universität Dresden	18, 329-340	18, 329-340.	\N
3902	Limnologica	3, 17-22	3, 17-22. Berlin.	\N
3904	Vegetatio	18, 240-245	18, 240-245.	\N
3908	Feddes Repertorium	85, 357-427	85, 357-427. Berlin.	\N
3910	Feddes Repertorium	85, 115-158	85, 115-158. Berlin.	\N
3912	Gleditschia	187-191	6	\N
3914	Archiv f. Naturschutz u. Landschaftsforschung	18, 121-140	18, 121-140. Berlin.	\N
3916	Gleditschia	101-115	8	\N
3920	Gleditschia	25-40	15	\N
3922	Meer u. Museum Stralsund	5, 14-24	5, 14-24.	\N
3924	Tuexenia	99-114	10	\N
3926	Phytocoenologica	19, 1-28	19, 1-28.	\N
3928	Drosera	89-116	2/1992	\N
3931	Bot. Rundbrief Bez. Neubrandenburg	13, 38	13, 38.	\N
3933	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	7, 240-280	7, 240-280. Rostock.	\N
3935	Wiss. Z. Universität Rostock, Math.-nat.R	16, 1-42	16, 1-42.	\N
3941	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	19, 135-148	19, 135-148. Rostock.	\N
3943	Wiss. Z. Universität Rostock, Math.-nat.R	68-78	32 (5)	\N
3945	Wiss. Z. Päd. Hochsch. Güstrow, Math.-nat. Fak	105-113	28 (2)	\N
3948	Wiss. Z. Universität Halle	41, 39-60	41, 39-60.	\N
3951	Schriftenreihe zur Ur- u. Frühgeschichte	38, 1-175	38, 1-175.	\N
3953	Feddes Repertorium, Beih	1-95	114	\N
3956	Wiss. Z. Universität Rostock, Math.-nat.R	25, 263-265	25, 263-265.	\N
3958	Limnologica	11, 229-305	11, 229-305. Berlin.	\N
3963	Päd. Hochschule Güstrow, Diss. A.	Päd	Päd. Hochschule Güstrow, Diss. A.	\N
3966	Bot. Centralblatt Beih	47, 2	47, 2. Abt. H. 2/3, 399-420.	\N
3969	Natur u. Naturschutz i. Mecklenburg-Vorpommern	22, 61-78	22, 61-78.	\N
3971	Ber. d. Deutschen Botanischen Gesellschaft	64, 222-240	64, 222-240.	\N
3973	Tuexenia	73-79	5	\N
3976	Gleditschia	161-196	4	\N
3979	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	9, 135-149	9, 135-149. Rostock.	\N
3982	Natur u. Naturschutz i. Mecklenburg-Vorpommern	3, 161-183	3, 161-183.	\N
3985	Botanica Marina	10, 240-251	10, 240-251.	\N
3989	Feddes Repertorium	231-258	84	\N
3991	Phyton	7, 142-151	7, 142-151.	\N
3993	Phyton	8, 10- 34	8, 10- 34.	\N
3995	Archiv f. Forstwesen u. Landschaftsökologie	8, 1-74	8, 1-74. Berlin.	\N
3997	Feddes Repertorium, Beih	138, 1-56	138, 1-56. Berlin.	\N
3999	Phyton	7, 22-31	7, 22-31.	\N
4003	Verhandlungen d. Bot. Vereins d. Prov. Brandenburg	98-100, 113-124	98-100, 113-124. Berlin-Dahlem.	\N
4005	Vegetatio	10, 209-228	10, 209-228.	\N
4007	Archiv f. Forstwesen u. Landschaftsökologie	11, 275-308	11, 275-308. Berlin.	\N
4009	Archiv f. Forstwesen u. Landschaftsökologie	11, 199- 241	11, 199- 241. Berlin.	\N
4011	Naturschutzarbeit i. Mecklenburg	4, 57-60	4, 57-60.	\N
4013	Archiv f. Forstwesen u. Landschaftsökologie	12, 1159-1176	12, 1159-1176. Berlin.	\N
4015	Feddes Repertorium, Beih	7- 18	140	\N
4017	Pflanzensoziologie	13, S	13, S. 1-324. Fischer, Jena.	\N
4019	Verhandlungen d. Bot. Vereins d. Prov. Brandenburg	101, 8-17	101, 8-17. Berlin-Dahlem.	\N
4021	Archiv f. Forstwesen u. Landschaftsökologie	15, 475-504	15, 475-504. Berlin.	\N
4023	Feddes Repertorium, Beih	145-158	74	\N
4025	Feddes Repertorium, Beih	413-435	80	\N
4027	Feddes Repertorium	88, 503-525	88, 503-525. Berlin.	\N
4029	Pflanzensoziologie	16, S	16, S. 1-298. Fischer, Jena.	\N
4032	Naturschutzarbeit u. naturkundliche Heimatforschung i. den Bez. Rostock, Schwerin u. Neubrandenburg	5, 18-22	5, 18-22.	\N
4035	Berichte-Reports des Geol.-Paläont. Instituts, Universität Kiel	61, 1-127	61, 1-127.	\N
4038	Gleditschia	309-365	18	\N
4041	Bot. Rundbrief Bez. Neubrandenburg	19, 39-40	19, 39-40.	\N
4043	Naturschutzarbeit u. naturkundliche Heimatforschung i. den Bez. Rostock, Schwerin u. Neubrandenburg	5, 27-31	5, 27-31.	\N
4045	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	13, 137-140	13, 137-140. Rostock.	\N
4047	Naturschutzarbeit i. Mecklenburg	13 (1/2), 31-41	13 (1/2), 31-41.	\N
4051	Wiss. Z. Universität Rostock, Math.-nat.R	22, 763-771	22, 763-771.	\N
4053	Archiv f. Forstwesen u. Landschaftsökologie	2, 232-244	2, 232-244. Berlin.	\N
4055	Feddes Repertorium, Beih	55-109	137	\N
4057	Feddes Repertorium, Beih	5-106	141	\N
4059	Wiss. Abhandlungen d. Deutschen Akademie d. Landwirtschaftswissenschaften zu Berlin	56, 1 - 340	56, 1 - 340.	\N
4061	Natur u. Naturschutz i. Mecklenburg-Vorpommern	3, 15-142	3, 15-142.	\N
4063	Feddes Repertorium	139-144	96	\N
4068	Wiss. Z. Universität Rostock, Math.-nat.R	13, 209-217	13, 209-217.	\N
4074	Naturschutzarbeit i. Mecklenburg	34 (2), 1-19	34 (2), 1-19.	\N
4077	Naturschutzarbeit i. Mecklenburg	22 (2), 50-54	22 (2), 50-54.	\N
4080	Bot. Rundbrief Bez. Neubrandenburg	20, 49-51	20, 49-51.	\N
4085	Feddes Repertorium	90, 481-518	90, 481-518. Berlin.	\N
4087	Bot. Rundbrief Bez. Neubrandenburg	10, 60-67	10, 60-67.	\N
4089	Naturschutzarbeit i. Mecklenburg	22 (1), 10-17	22 (1), 10-17.	\N
4091	Wiss. Z. Universität Greifswald, Math.-nat.R	32, 71-79	32, 71-79.	\N
4093	Gleditschia	261-270	13	\N
4096	Wiss. Z. Universität Greifswald, Math.-nat.R	30, 17-23	30, 17-23.	\N
4100	Bot. Rundbrief Bez. Neubrandenburg	17, 53-58	17, 53-58.	\N
4102	Naturschutzarbeit i. Mecklenburg	18 (2/3), 24-30	18 (2/3), 24-30.	\N
4104	Labus	30-34	6	\N
4106	Naturschutzarbeit i. Mecklenburg	19, 37- 39	19, 37- 39.	\N
4108	Labus	44-47	7	\N
4110	Natur u. Naturschutz i. Mecklenburg-Vorpommern	5, 79-108	5, 79-108.	\N
4112	Naturschutzarbeit i. Mecklenburg	11 (1), 17-20	11 (1), 17-20.	\N
4114	Naturschutzarbeit i. Mecklenburg, Sonderheft	1 120-135	1 120-135.	\N
4120	Feddes Repertorium	85, 57-113	85, 57-113. Berlin.	\N
4122	Archiv f. Naturschutz u. Landschaftsforschung	26, 225-242	26, 225-242. Berlin.	\N
4124	Hercynia N.F	298-305	24	\N
4129	Bot. Rundbrief Bez. Neubrandenburg	21, 33-36	21, 33-36.	\N
4132	Archiv f. Naturschutz u. Landschaftsforschung	19, 217-229	19, 217-229. Berlin.	\N
4135	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	30, 95- 151	30, 95- 151. Rostock.	\N
4137	Naturschutzarbeit i. Mecklenburg	18 (2/3), 42- 45	18 (2/3), 42- 45.	\N
4140	Feddes Repertorium, Beih	232- 260	135	\N
4143	Feddes Repertorium, Beih	138, 214-229	138, 214-229. Berlin.	\N
4145	Feddes Repertorium, Beih	19-26	140	\N
4147	Feddes Repertorium	171-176	74	\N
4149	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	12, 89- 126	12, 89- 126. Rostock.	\N
4151	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	13, 137-138	13, 137-138. Rostock.	\N
4155	Bot. Rundbrief Bez. Neubrandenburg	7, 24-34	7, 24-34.	\N
4157	Bot. Rundbrief Bez. Neubrandenburg	9, 27-31	9, 27-31.	\N
4159	Naturschutzarbeit i. Mecklenburg	24, 84-89	24, 84-89.	\N
4161	Bot. Rundbrief Bez. Neubrandenburg	20,, 41-42	20,, 41-42.	\N
4163	Botanischer Rundbrief f. Mecklenburg-Vorpommern	24, 47-48	24, 47-48.	\N
4166	Bot. Rundbrief Bez. Neubrandenburg	17, 15-18	17, 15-18.	\N
4169	Limnologica	14, 89-105	14, 89-105. Berlin.	\N
4172	Bot. Rundbrief Bez. Neubrandenburg	18, 63-68	18, 63-68.	\N
4174	Gleditschia	259-268	19	\N
4176	Gleditschia	95-105	20	\N
4178	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	73-101	10	\N
4180	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	85- 101	11	\N
4182	Naturschutzarbeit i. Mecklenburg	86-87	8 (2/3)	\N
4184	Wiss. Z. Universität Rostock, Math.-nat.R	43-95	16	\N
4186	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	66- 71	14	\N
4188	Archiv f. Forstwesen u. Landschaftsökologie	719-730	18	\N
4190	Naturschutzarbeit I. Mecklenburg	92-100	13 (1/2)	\N
4192	Wiss. Z. Universität Rostock, Math.-nat.R	773-783	22	\N
4194	Bot. Rundbrief Bez. Neubrandenburg	3-12	9	\N
4196	Natur u. Naturschutz i. Mecklenburg-Vorpommern	5-16	15	\N
4198	Bot. Rundbrief Bez. Neubrandenburg	40-45	11	\N
4200	Bot. Rundbrief Bez. Neubrandenburg	37- 42	11	\N
4202	Bot. Rundbrief Bez. Neubrandenburg	67-86	12	\N
4204	Bot. Rundbrief Bez. Neubrandenburg	75-76	14	\N
4206	Bot. Rundbrief Bez. Neubrandenburg	71-75	15	\N
4208	Bot. Rundbrief Bez. Neubrandenburg	51	17	\N
4210	Bot. Rundbrief Bez. Neubrandenburg	66	19	\N
4214	Bot. Rundbrief Bez. Neubrandenburg	19-22	21	\N
4216	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	60-72	29	\N
4218	Gleditschia	39-68	19	\N
4220	Naturschutzarbeit i. Mecklenburg	44-45	36	\N
4222	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	35-46	20	\N
4224	Bot. Rundbrief Bez. Neubrandenburg	43-49	11	\N
4227	Naturschutzarbeit i. Mecklenburg	45.48	35	\N
4694	Botanischer Rundbrief f. Mecklenburg-Vorpommern	30, 17-32	30, 17-32.	\N
4696	Botanischer Rundbrief f. Mecklenburg-Vorpommern	30, 33-38	30, 33-38.	\N
4698	Botanischer Rundbrief f. Mecklenburg-Vorpommern	30, 39-46	30, 39-46.	\N
4700	Botanischer Rundbrief f. Mecklenburg-Vorpommern	30, 109-118	30, 109-118.	\N
4702	Botanischer Rundbrief f. Mecklenburg-Vorpommern	30, 7-16	30, 7-16.	\N
4715	Botanischer Rundbrief f. Mecklenburg-Vorpommern	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern	\N
4723	Botanischer Rundbrief f. Mecklenburg-Vorpommern	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern	\N
4725	Botanischer Rundbrief f. Mecklenburg-Vorpommern	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern	\N
4727	Botanischer Rundbrief f. Mecklenburg-Vorpommern	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern	\N
4729	Botanischer Rundbrief f. Mecklenburg-Vorpommern	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern	\N
4731	Botanischer Rundbrief f. Mecklenburg-Vorpommern	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern	\N
4733	Botanischer Rundbrief f. Mecklenburg-Vorpommern	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern	\N
4738	Botanischer Rundbrief f. Mecklenburg-Vorpommern	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern	\N
4740	Tuexenia	251-282	17	\N
4742	Vegetatio	Vegetatio	Vegetatio	\N
4761	Mitteilungen AG Geobot. Schleswig-Holst. u. Hamburg. Kiel.	Mitteilungen AG Geobot	Mitteilungen AG Geobot. Schleswig-Holst. u. Hamburg. Kiel.	\N
4764	Tuexenia	119-151	18	\N
4814	Wiss. Z. Universität Halle, Math.-nat. R.	Wiss	Wiss. Z. Universität Halle, Math.-nat. R.	\N
4826	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	\N
4828	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	\N
4830	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	\N
4832	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	\N
4834	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	\N
4837	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	\N
4839	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	\N
4841	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	\N
4843	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	\N
4845	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	\N
4847	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	\N
4850	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	\N
4852	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	Botanischer Rundbrief f	Botanischer Rundbrief f. Mecklenburg-Vorpommern.	\N
4857	Gleditschia	17-26	28	\N
4859	Tuexenia	343-371	13	\N
4861	Mitteilungen d. Floristisch-soziologischne AG N.F.	Mitteilungen d	Mitteilungen d. Floristisch-soziologischne AG N.F.	\N
4212	Bot. Rundbrief Bez. Neubrandenburg	29-37	20	\N
4229	Botanischer Rundbrief f. Mecklenburg-Vorpommern	24, 3-20	24, 3-20.	\N
4717	Botanischer Rundbrief f. Mecklenburg-Vorpommern	43-47	32	\N
4232	Wiss. Z. Päd. Hochsch. Güstrow	133-163	1973 (2)	\N
4235	Wiss. Z. Päd. Hochsch. Güstrow	29-52	1974 (2)	\N
4290	Botanischer Rundbrief f. Mecklenburg-Vorpommern	17- 34	29	\N
4292	Feddes Repertorium	585- 596	103	\N
4294	Feddes Repertorium	631-642	103	\N
4297	Feddes Repertorium	597- 619	103	\N
4299	Feddes Repertorium	621- 630	103	\N
4302	Gleditschia	117- 145	21	\N
4363	Natur u. Naturschutz i. Mecklenburg-Vorpommern	6, 85-110	6, 85-110.	\N
4366	Natur u. Naturschutz i. Mecklenburg-Vorpommern	58-63	30	\N
4393	Botanischer Rundbrief f. Mecklenburg-Vorpommern	55- 64	27	\N
4395	Botanischer Rundbrief f. Mecklenburg-Vorpommern	49-56	29	\N
4397	Botanischer Rundbrief f. Mecklenburg-Vorpommern	123- 132	29	\N
4399	Botanischer Rundbrief f. Mecklenburg-Vorpommern	65- 80	27	\N
4433	Botanischer Rundbrief f. Mecklenburg-Vorpommern	73-78	29	\N
4461	Botanica Marina	8, 218-233	8, 218-233.	\N
4479	Feddes Repertorium	88, 141- 245	88, 141- 245. Berlin.	\N
4496	Botanischer Rundbrief f. Mecklenburg-Vorpommern	57-68	29	\N
4501	Botanischer Rundbrief f. Mecklenburg-Vorpommern	103-110	29	\N
4606	Natur u. Naturschutz i. Mecklenburg-Vorpommern	63-77	31	\N
4620	Botanischer Rundbrief f. Mecklenburg-Vorpommern	35- 48	29	\N
4622	Archiv d. Freunde d. Naturgeschichte Mecklenburgs	32, 207- 212	32, 207- 212. Rostock.	\N
4627	Botanischer Rundbrief f. Mecklenburg-Vorpommern	7- 36	27	\N
4629	Botanischer Rundbrief f. Mecklenburg-Vorpommern	69-72	29	\N
4873	Mitteilungen d. Floristisch-soziologischne AG N.F.	Mitteilungen d	Mitteilungen d. Floristisch-soziologischne AG N.F.	\N
4876	Mitteilungen AG Geobot. Schleswig-Holst. u. Hamburg. Kiel.	Mitteilungen AG Geobot	Mitteilungen AG Geobot. Schleswig-Holst. u. Hamburg. Kiel.	\N
4878	Tuexenia	197-228	14	\N
4905	Mitteilungen d. Floristisch-soziologischen AG N.F	71-100	13	\N
4907	Wissenschaftliche Zeitschrift d. Pädagogischen Hochschule Potsdam, math.-nat. Reihe ?. Potsdam.	Wissenschaftliche Zeitschrift d	Wissenschaftliche Zeitschrift d. Pädagogischen Hochschule Potsdam, math.-nat. Reihe ?. Potsdam.	\N
4909	Verhandlungen d. Bot. Vereins d. Provinz Brandenburg. Berlin-Dahlem.	Verhandlungen d	Verhandlungen d. Bot. Vereins d. Provinz Brandenburg. Berlin-Dahlem.	\N
4911	Naturschutz u. Landschaftspflege i. Brandenburg.	Naturschutz u	Naturschutz u. Landschaftspflege i. Brandenburg.	\N
4913	Verhandlungen d. Bot. Vereins Berlin u. Brandenburg.	Verhandlungen d	Verhandlungen d. Bot. Vereins Berlin u. Brandenburg.	\N
4916	Botanisches Centralblatt Beih	57 Abt	57 Abt. B: 1-76.	\N
4918	Verhandlungen d. Bot. Vereins d. Provinz Brandenburg. Berlin-Dahlem.	Verhandlungen d	Verhandlungen d. Bot. Vereins d. Provinz Brandenburg. Berlin-Dahlem.	\N
4920	Gleditschia	193-208	6	\N
4922	Gleditschia	225-250	7	\N
4924	Gleditschia	107-122	12	\N
5401	Flora	271-281	187	\N
5403	Tuexenia	263-268	14	\N
4927	Verhandlungen d. Bot. Vereins d. Provinz Brandenburg. Berlin-Dahlem.	Verhandlungen d	Verhandlungen d. Bot. Vereins d. Provinz Brandenburg. Berlin-Dahlem.	\N
4929	Naturschutzarbeit i. Berlin u. Brandenburg.	Naturschutzarbeit i	Naturschutzarbeit i. Berlin u. Brandenburg.	\N
4933	Feddes Repertorium, Beih	260-350	135	\N
4956	Gleditschia	265-286	15	\N
5145	Verhandlungen d. Bot. Vereins Berlin Brandenburg	63-111	126	\N
5160	Tuexenia	297-310	26	\N
5203	Tuexenia	217-236	18	\N
5206	Tuexenia	241-253	27	\N
5213	Gleditschia	199-227	10	\N
5223	Tuexenia	31-51	35	\N
5269	Biodiversity & Ecology	149-160	4	\N
5278	Weed Research	574-585	55 (6)	\N
5280	Journal Plant Diseases and Protection	107-120	119	\N
5292	Tuexenia	131-153	35	\N
5294	Dissertationes Botanicae	1-176	123	\N
5296	\N	331	\N	\N
5300	Mitteilungen florist. Kart. Sachsen-Anhalt (Halle 2014)	19-24	19	\N
5302	Ber. d. Bayerischen Botanischen Gesellschaft	141-150	76	\N
5304	Ber. d. Bayerischen Botanischen Gesellschaft	129-14	78	\N
5306	Ber. d. Bayerischen Botanischen Gesellschaft	289-299	66/67	\N
5309	Ber. d. Bayerischen Botanischen Gesellschaft 69/70	105-112	69/70	\N
5312	Ber. d. Bayerischen Botanischen Gesellschaft	177-180	69/70	\N
5315	Ber. d. Bayerischen Botanischen Gesellschaft	85-93	55	\N
5318	Ber. d. Bayerischen Botanischen Gesellschaft	175-190	63	\N
5320	Ber. d. Bayerischen Botanischen Gesellschaft	103-128	78	\N
5323	Dissertationes Botanicae	316	104	\N
5326	Oecologia	120-129	55	\N
5328	Tuexenia	131 - 142	10	\N
5331	Naturwiss. Gesellschaft Bayreuth, Bericht	81-157	22	\N
5333	Mitteilungen d. Badischen Landesvereins für Naturkunde und Naturschutz e.V.. Freiburg i. Br. N.F.	1-34	16	\N
5335	Tuexenia	135-139	1	\N
5338	Tuexenia	317-340	9	\N
5340	Veröff. Landesver. Sächs. Heimatsch. zur Erforsch. Pflanzenges. Sachsens	69-118	9	\N
5343	Hercynia N.F.	5-56	29	\N
5346	Tuexenia	101-148	8	\N
5349	Niederbergische Beiträge, Quellen und Forschungen zur Heimatkunde Niederbergs, Sonderreihe	\N	2	\N
5351	Tuexenia	193-208	12	\N
5183	Bull. Agronom. Gembloux	249-256	16 (3)	\N
5186	Coll. Phytosoc.	183-249	3	\N
5353	Mitteilungen zur floristischen Kartierung in Sachsen-Anhalt	25-31	20	\N
5357	\N	222	\N	\N
5360	\N	244	\N	\N
5363	\N	270	\N	\N
5367	Mitteilungen florist. Kart. Sachsen-Anhalt (Halle 2008)	53-75	13	\N
5370	\N	279	\N	\N
5372	Abhandlungen aus d. Westfäl. Museum f. Naturkunde	239	76	\N
5374	Ber. d. Bayerischen Botanischen Gesellschaft	201-210	44	\N
5377	Berichte d. Arbeitsgemeinschaft sächsischer Botaniker, N.F.	5-30	17	\N
5384	Drosera	27-32	96	\N
5387	Arbeiten aus der Forschungsstelle Küste	1-202	12	\N
5392	Drosera	71-76	2010	\N
5395	Mitt. Flor.-Soz. Arb. Gemeinschaft	183-204	5/6	\N
5398	Der Biologieunterricht	71 92	9(1)	\N
5405	Floristische Rundbriefe	62-67	28(1)	\N
5409	Praktikumsbericht	\N	\N	\N
5412	Diplomarbeit Universität Bremen	\N	\N	\N
5415	Tüxenia	283-291	9	\N
5417	Diplomarbeit Uni Bremen, 113 S.	\N	\N	\N
5420	Tuexenia	137-145	17	\N
5423	Tuexenia	367-371	23	\N
5428	Tuexenia	141-160	13	\N
5433	Drosera	33-50	2003	\N
5436	Drosera	1-19	97	\N
5439	Drosera	6-9	76	\N
5442	Drosera	47-56	86	\N
5445	Drosera	11-32	93	\N
5448	Drosera	125-137	94	\N
5450	Mitteilungen der Flor.-Soz. Arbeitsgemeinschaft N.F.	109-112	3	\N
5453	Mitt. der Flor.-Soz. Arbeitsgemeinschaft	64-65	5	\N
5455	Mitt. Flor.-Soz. Arbeitsgem. N.F.	205-234	6/7	\N
5457	Mitt. Flor.-soz. Arbeitsgem.	41-44	15/16	\N
5459	Mitt. flor.-soz. Arbeitsgem. N.F.	111-128	18	\N
5462	Mitt. Flor.-soz. Arbeitsgem.	116-129	10	\N
5465	Drosera	119-131	86(2)	\N
5467	Mitt. Flor.-soz. Arbeitsgem.	95-120	14	\N
5469	Mitt. Flor.-soz. Arbeitsgem.	56	8	\N
5472	Hausarbeit Universität Oldenburg	\N	\N	\N
5474	Floristische Rundbriefe	75-76	37	\N
5477	Universität Hannover, 198 S.	\N	\N	\N
5479	Tuexenia	149-170	11	\N
5482	Kiel. Notiz. Pflanzenkd. Schleswig-Holstein u. Hamburg	128-166	25/26	\N
5485	Dissertationes Botanicae 25, 105 S.	\N	\N	\N
5488	Mitt. AG Flor. Schl.-Holst. u. Hamburg	\N	13	\N
5492	Mitt. Staatsinst. Allg. Bot.	191-353	12	\N
5494	Tuexenia	219-248	6	\N
5496	Dipl.-Arbeit Universität Münster	\N	\N	\N
5499	Prüfungsarbeit Universität Hamburg	\N	\N	\N
5502	Staatsexamensarbeit. Bielefeld. 40 S.	\N	\N	\N
5505	Veröffentlichungen Institut für Meeresforschung Bremerhaven	279-379	2	\N
5507	Drosera	17-26	92(1)	\N
5510	Diplomarbeit. Westfälische Wilhelms-Universität Münster. 84 S	\N	\N	\N
5513	Mitt. AG Floristik in Schlesw.-Holst. u. Hamburg	\N	1	\N
5516	Diplomarbeit Universität Oldenburg	\N	\N	\N
5519	Diplomarbeit Universität Göttingen	\N	\N	\N
5521	Gutachten im Auftrage des niedersächsischen Landesverwaltungsamtes Naturschutz, Landschaftspflege, Vogelschutz.	\N	\N	\N
5524	Diplom-Arbeit Universität Oldenburg.	\N	\N	\N
5527	Diplomarbeit Universität Oldenburg	\N	\N	\N
5530	Diplomarbeit Universität Oldenburg.	\N	\N	\N
5537	Oldenburger Jahrbuch	203-213	75/76	\N
5540	Tuexenia	165-	7	\N
5543	Diplom-Arbeit Uni Hannover. 111 S	\N	\N	\N
5546	Dissertation Uni Hannover	\N	\N	\N
5549	Projektbericht	\N	\N	\N
5553	Bericht NLWKN	\N	\N	\N
5556	Diplomarbeit Universität Oldenburg	\N	\N	\N
5559	Oldenburg	\N	\N	\N
5562	Schriftenreihe der Nationalparkverwaltung Niedersächsisches Wattenmeer	1-168	11	\N
5565	Diplomarbeit Universität Hannover, 112 S.	\N	\N	\N
5568	Diplomarbeit Universität Hannover. 117 S.	\N	\N	\N
5571	Diplomarbeit Universität Hannover, 91 S.	\N	\N	\N
5574	Hamburger Vegetationsgeographische Mitteilungen	52-197	9	\N
5576	Schriftenreihe des Institutes für Landschaftspflege und Naturschutz	125 S.	\N	\N
5579	Mitt. Arbeitsgemein. Geobotanik Schleswig-Holstein und Hamburg	1-111	51	\N
5582	Mitt. Arbeitsgem. Geobotanik Schleswig-Holstein und Hamburg	1-142	34	\N
5584	Dissertationes Botanicae, 257, 1-227.	\N	\N	\N
5586	Mitt. Arbeitsgem. Geobotanik Schleswig-Holstein und Hamburg	1-166	26	\N
5589	Bericht NLKN	\N	\N	\N
5592	Diploma-Thesis Universität Oldenburg. 87 S.	\N	\N	\N
5595	Bachelorarbeit Universität Bremen. 63 S.	\N	\N	\N
5598	Diplomarbeit Universität Bremen. 78 S.	\N	\N	\N
5600	Schriftenreihe Nationalpark Nieders. Wattenmeer	1-175	9	\N
5602	Diplomarbeit Universität Bremen. 76 S.	\N	\N	\N
5605	Diplomarbeit Universität Hannover. 117 S.	\N	\N	\N
5609	NLVP	\N	\N	\N
5611	NLPV	\N	\N	\N
5613	NLÖ Hildesheim	\N	\N	\N
5615	Feddes Repertorium, Beihefte	92-130	121	\N
5617	Beitr. Bez.-Naturkundemuseums Stralsund	67-84	1	\N
5620	Projektbericht Uni Bremen, 125-180	\N	\N	\N
5623	Diplomarbeit Universität Oldenburg, 99 S.	\N	\N	\N
5626	Diplomarbeit Uni Oldenburg 80 S.	\N	\N	\N
5631	Diplomarbeit Uni Oldenburg, 150 S.	\N	\N	\N
5634	Praktikumsbericht Universität Darmstadt	\N	\N	\N
5636	Praktikumsbericht Universität Darmstadt	\N	\N	\N
5638	Praktikumsbericht Universität Darmstadt	\N	\N	\N
5640	Praktikumsbericht Universität Darmstadt	\N	\N	\N
5642	Praktikumsbericht Universität Darmstadt	\N	\N	\N
5644	Praktikumsbericht Universität Darmstadt	\N	\N	\N
5646	Drosera	31-36	2004	\N
5649	Schr. Naturw. Ver. Schlesw.-Holst.	3-24	31	\N
5651	unpublished	\N	\N	\N
5654	Studentreport	\N	\N	\N
5656	Student report	\N	\N	\N
5659	Hamburger Vegetationsgeographische Mitteilungen	1-99	4	\N
5662	Hamburger Vegetationsgeographische Mitteilungen	1-58	1	\N
5665	NLWKN Bericht, 85 S.	\N	\N	\N
5668	Stiching  Commissie voor de Vecht en het Oostelijk en Westelijk Plassengebied, Amsterdam: 128 pp.	\N	\N	\N
5671	Acta Botanica Barcinonesia	191-237	46	\N
5674	Plantensociologische Kring Nederland. 62 pp.	\N	\N	\N
5677	Gorteria	1-9	Deel 6 (1)	\N
5680	Verslag 6 pp. + bijlagen	\N	\N	\N
5683	Gorteria	34-38	6	\N
2727		51-71		\N
2134		64-84		\N
3665	Wiss. Z. E.-M.-Arndt-Universität Greifswald, Math.- nat.R	277-305	7	\N
1324	Wiss. Z. Universität Halle Sonderheft	3-51		\N
3370	Verhandlungen d. Bot. Vereins d. Provinz Brandenburg	72-137	78	\N
2378	Archiv f. Forstwesen u. Landschaftsökologie	499-541	9	\N
4001	Archiv f. Forstwesen u. Landschaftsökologie	499-541	9	\N
5685	Biodiversity & Ecology	355–355	4	\N
5694	Beih. Veröff. Natursch. Landschaftspfl. Bad.-Württ.	204	85	\N
5697	\N		\N	\N
5699	Ber. d. Arbeitsgemeinschaft sächsischer Botaniker N.F.	35-48	18	\N
5702	Ber. d. Arbeitsgemeinschaft sächsischer Botaniker N.F.	67-77	18	\N
5706	Ber. d. Arbeitsgemeinschaft sächsischer Botaniker N.F.	137-144.	18	\N
5710	Naturschutz im Land Sachsen-Anhalt. 30 Jahre Biosphärenreservat Mittelelbe. Forschung und Management im Biosphärenreservat Mittelelbe 46, Sonderheft 2009/1	49-56	\N	\N
5713	Natur und Heimat	82-84	46	\N
5715	Natur und Heimat	73-81	46	\N
5718	Mitteilungen der AG sächsischer Botaniker N.F.	159-167	VIII	\N
5287	Bulletin of the Eurasian Dry Grassland Group	10-15	28	\N
4719	Botanischer Rundbrief f. Mecklenburg-Vorpommern	49-68	32	\N
5727	Kieler Notizen zur Pflanzenkunde in Schleswig-Holstein und Hamburg	20-25	32	\N
5189	Bull Inst. Sci nat. Bel., Bruxelles	1-76	46 (30)	\N
5738	\N	200	\N	\N
5741	Commun. Centre Ecol. Forest. Rurale	26	13	\N
5177	Bull. Inst. Agronom. Stat. Rech. Gembloux	208-239	24 (2)	\N
5745	Unpublished	\N	\N	\N
5754	Perspectives in Plant Ecology, Evolution and Systematics	1-10	24	\N
5747		\N	\N	\N
5757	\N	33-78	2002	\N
\.


--
-- Data for Name: keyword; Type: TABLE DATA; Schema: public; Owner: quarasek
--

COPY public.keyword (id, keyword) FROM stdin;
\.


--
-- Data for Name: keyword_publication; Type: TABLE DATA; Schema: public; Owner: quarasek
--

COPY public.keyword_publication (keyword_id, publication_id) FROM stdin;
\.


--
-- Data for Name: pdf_link; Type: TABLE DATA; Schema: public; Owner: quarasek
--

COPY public.pdf_link (id, url) FROM stdin;
\.


--
-- Data for Name: platform; Type: TABLE DATA; Schema: public; Owner: quarasek
--

COPY public.platform (id, name) FROM stdin;
1	vegetweb
2	GIVD
5	flora-mv
\.


--
-- Name: platform_id_seq; Type: SEQUENCE SET; Schema: public; Owner: quarasek
--

SELECT pg_catalog.setval('public.platform_id_seq', 5, true);


--
-- Data for Name: publication; Type: TABLE DATA; Schema: public; Owner: quarasek
--

COPY public.publication (id, cite_id, description, doi, givd_id, issue, thesis_type, title, turboveg_id, tv2address, tv2anmerkung, tv2autor_kuer, tv2bemerk, tv2booktitle, tv2db1, tv2db2, tv2hrsg, tv2plotgvrd, tv2plots_mv, tv2public, tv2published, tv2publisher, tv2ref_new, tv2school, tv2verw_florenwerke, type, update_date, url, year, institution_id, journal_id, need_review) FROM stdin;
6	Hilbig_1962_000002	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen in der mitteldeutschen Ackerlandschaft. VII. Die Pflanzengesellschaften der Umgebung von Dehlitz (Saale), Kr. Weißenfeld	000002	Halle (Saale)	\N	\N	\N		\N	\N	\N	126	\N	Y			\N		\N	article	2016-05-04 11:04:28.35	\N	1962	\N	7	f
5767	García_Márquez2012	\N	10.7809/b-e.00057	\N	\N	\N	A methodological framework to quantify the spatial quality of biological databases	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:47.239	\N	2012	\N	5768	f
13	Dierschke_Wolter_1975_000004	\N	\N	\N	\N	\N	Laubwald-Gesellschaften der nördlichen Wesermünder Geest	000004	Göttingen	\N	\N	\N		\N	\N	\N	243	\N	Y			\N		\N	article	2016-05-04 11:04:28.927	\N	1975	\N	14	f
16	Korsch_2008_000005	\N	\N	\N	\N	\N	Flora und Vegetation des Stedtlinger Moores, aktueller Zustand und Analyse von Veränderungen	000005		\N	\N	\N		\N	\N	\N	10	\N	Y			\N		\N	article	2016-05-04 11:04:29.182	\N	2008	\N	17	f
19	Egeling_1997_000006	\N	\N	\N	\N	\N	Die Glatthaferwiesen in der unteren Salm-Aue (Regierungsbezirk Trier, Rheinland-Pfalz)	000006	Bad Dürkheim	\N	\N	\N		\N	\N	\N	39	\N	Y			\N		\N	article	2016-05-04 11:04:29.437	\N	1997	\N	20	f
90	Goers_1969_000029	\N	\N	\N	\N	\N	Die Vegetation des Landschaftsschutzgebietes "Kreuzweiher" im württembergischen Allgäu	000029	Ludwigsburg	\N	\N	\N		THOMAS	\N	\N	74	\N	Y			\N		\N	article	2016-05-04 11:04:36.081	\N	1969	\N	91	f
29	Ruthsatz_1970_000009	\N	\N	\N	\N	\N	Die Grünlandgesellschaften um Göttingen	000009	Göttingen	\N	RUT	\N		TABGESNE	TV	\N	144	\N	Y			\N		\N	article	2016-05-04 11:04:30.174	\N	1970	\N	30	f
32	Jeckel_1975_000010	\N	\N	\N	\N	\N	Die Sandtrockenrasen (Sedo-Scleranthetea) der Allerdünen bei Celle-Boye	000010	Göttingen	\N	\N	\N		\N	\N	\N	19	\N	Y			\N		\N	article	2016-05-04 11:04:30.417	\N	1975	\N	33	f
35	Grimme_1977_000011	\N	\N	\N	\N	\N	Wasser- und Nährstoffversorgung von Hangbuchenwäldern auf Kalk in der weiteren Umgebung Göttingens	000011	Göttingen	\N	\N	\N		\N	TV	\N	15	\N	Y			\N		\N	article	2016-05-04 11:04:30.928	\N	1977	\N	36	f
38	Lauer_1987_000012	\N	\N	\N	\N	\N	Florenentwicklung und Vegetationsdynamik im Lennebergwald	000012	Bad Dürkheim	\N	\N	\N		\N	\N	\N	42	\N	Y			\N		\N	article	2016-05-04 11:04:31.282	\N	1987	\N	39	f
5770	Jürgens2012	\N	10.7809/b-e.00054	\N	\N	\N	A roadmap towards the global view on vegetation	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:47.753	\N	2012	\N	5771	f
5774	Willner2012	\N	10.7809/b-e.00125	\N	\N	\N	Austrian Vegetation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:47.787	\N	2012	\N	5775	f
5779	Horchler2012	\N	10.7809/b-e.00158	\N	\N	\N	AuVeg  a database of German floodplain vegetation	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:47.902	\N	2012	\N	5780	f
5785	Vassilev2012	\N	10.7809/b-e.00123	\N	\N	\N	Balkan Dry Grasslands Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:47.957	\N	2012	\N	5786	f
57	Wollert_Bolbrinker_2002_000018	\N	\N	\N	\N	\N	Die Vegetation des Tals des Klenzer Mühlbaches zwischen der Bundesstraße 194 und der Mündung in das Peenetal (Landkreis Demmin, Mecklenburg-Vorpommern)	000018	Stralsund-Greifswald	\N	\N	\N		\N	\N	\N	129	\N	Y			\N		\N	article	2016-05-04 11:04:33.235	\N	2002	\N	58	f
5788	Marinek2012	\N	10.7809/b-e.00122	\N	\N	\N	Beech Forest Vegetation Database of SE Balkan	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.007	\N	2012	\N	5789	f
63	Sauer_1955_000020	\N	\N	\N	\N	\N	Die Wälder des Mittelterassengebietes östlich von Köln	000020	Bonn	\N	\N	\N		\N	\N	\N	174	\N	Y			\N		\N	article	2016-05-04 11:04:33.757	\N	1955	\N	64	f
69	Boeker_1957_000022	\N	\N	\N	\N	\N	Basenversorgung und Humusgehalte von Böden der Pflanzengesellschaften des Grünlandes	000022	Bonn	\N	\N	\N		LITTAB	TV	\N	112	\N	Y			\N		\N	article	2016-05-04 11:04:34.252	\N	1957	\N	70	f
5800	Thiele2012	\N	10.7809/b-e.00127	\N	\N	\N	Belarus Peatland Restoration Project Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.051	\N	2012	\N	5801	f
5802	Ewald2012#161	\N	10.7809/b-e.00071	\N	\N	\N	BERGWALD  the vegetation database of mountain forests in the Bavarian Alps	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.138	\N	2012	\N	5803	f
5806	Meyer2012	\N	10.7809/b-e.00157	\N	\N	\N	BioChangeFields  vegetation database of arable plant communties in Central Germany	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.155	\N	2012	\N	5807	f
5808	Wesche2012	\N	10.7809/b-e.00142	\N	\N	\N	BioChangeMeadows  German meadows in the 1950s, 1990s and in 2008	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.181	\N	2012	\N	5809	f
5812	Muche2012#204	\N	10.7809/b-e.00066	\N	\N	\N	BIOTA Southern Africa Biodiversity Observatories Vegetation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.204	\N	2012	\N	5813	f
93	Haffner_1990_000030	\N	\N	\N	\N	\N	Pflanzengesellschaften der Unteren Saar	000030	Bonn	\N	\N	\N		THOMAS	\N	\N	13	\N	Y			\N		\N	article	2016-05-04 11:04:36.366	\N	1990	\N	94	f
96	Kiem_1991_000031	\N	\N	\N	\N	\N	Flora und Vegetation einiger Feuchtgebiete des Rittens und seiner Umgebung	000031	München	\N	\N	\N		THOMAS	\N	\N	3	\N	Y			\N		\N	article	2016-05-04 11:04:36.629	\N	1991	\N	97	f
99	Libbert_1932_000032	\N	\N	\N	\N	\N	Die Vegetationseinheiten der neumärkischen Staubeckenlandschaft unter Berücksichtigung der angrenzenden Landschaften	000032	Berlin-Dahlem	\N	\N	\N		THOMAS	\N	\N	10	\N	Y			\N		\N	article	2016-05-04 11:04:36.919	\N	1932	\N	100	f
5818	Apostolova2012	\N	10.7809/b-e.00069	\N	\N	\N	Bulgarian Vegetation Database: historic background, current status and future prospects	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.252	\N	2012	\N	5819	f
108	Succow_1967_000035	\N	\N	\N	\N	\N	Pflanzengesellschaften der Zieseniederung (Ostmecklenburg)	000035	Stralsund-Greifswald	\N	\N	\N		THOMAS	\N	\N	112	\N	Y			\N		\N	article	2016-05-04 11:04:37.71	\N	1967	\N	109	f
22	Luett_1985_000007	\N	\N	\N	\N	\N	Die Vegetation der kalkreichen Niedermoorwiese am Dobersdorfer See, Kreis Plön	000007		\N	\N	\N		\N	\N	\N	48	\N	Y			\N		\N	article	2016-05-04 11:04:29.681	\N	1985	\N	23	f
105	Spoerle_1965_000034	\N	\N	\N	\N	\N	Untersuchungen über den Wasserhaushalt des Wirtschaftsgrünlandes im Neckartal zwischen Rottenburg und Pliezhausen	000034	Ludwigsburg	\N	\N	\N		THOMAS	\N	\N	1	\N	Y			\N		\N	article	2016-05-04 11:04:37.44	\N	1965	\N	106	f
112	Dierschke_Jeckel_1980_000037	\N	\N	\N	\N	\N	Flutrasen-Gesellschaften des Agropyro-Rumicion im Allertal (NW-Deutschland)	000037	Göttingen	\N	\N	\N		THOMAS	\N	\N	17	\N	Y			\N		\N	article	2016-05-04 11:04:38.19	\N	1980	\N	113	f
116	Bartsch_Bartsch_1940_000038	\N	\N	\N	\N	\N	Vegetationskunde des Schwarzwaldes	000038	Jena	\N	\N	\N		LITTAB	\N	\N	14	\N	Y			\N		\N	article	2016-05-04 11:04:38.427	\N	1940	\N	117	f
5822	Zhao2012	\N	10.7809/b-e.00098	\N	\N	\N	China Forest-Steppe Ecotone Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.29	\N	2012	\N	5823	f
5272	Buerger_2016_100720	\N	\N	\N	\N	\N	Testdatensatz Ackervegetation		\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	unknown	2016-07-22 08:49:40.055	\N	2016	\N	\N	t
5828	Alvarez2012	\N	10.7809/b-e.00230	\N	\N	\N	CL-Dataveg  a database of Chilean grassland vegetation	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.327	\N	2012	\N	5829	f
5835	Sorokin2012#240	\N	10.7809/b-e.00209	\N	\N	\N	Coastal Vegetation Database of North-Western Seas of Russia	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.365	\N	2012	\N	5836	f
5838	Sorokin2012	\N	10.7809/b-e.00210	\N	\N	\N	Coastal Vegetation Database of the Southern Seas of Russia	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.403	\N	2012	\N	5839	f
5844	Csiky2012	\N	10.7809/b-e.00183	\N	\N	\N	CoenoDat Hungarian Phytosociological Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.436	\N	2012	\N	5845	f
5848	Rangel-Churio2012#224	\N	10.7809/b-e.00084	\N	\N	\N	Colombian Páramo Vegetation Database (CPVD)  the database on high Andean páramo vegetation in Colombia	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.467	\N	2012	\N	5849	f
5851	Dolnik2012	\N	10.7809/b-e.00214	\N	\N	\N	Curonian Spit Biodiversity Plots	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.482	\N	2012	\N	5852	f
5855	Chytrý2012	\N	10.7809/b-e.00136	\N	\N	\N	Czech National Phytosociological Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.492	\N	2012	\N	5856	f
123	Dierssen_Dierssen_1984_000040	\N	\N	\N	\N	\N	Vegetation und Flora der Schwarzwaldmoore	000040	Ludwigsburg	\N	\N	\N		LITTAB	\N	\N	398	\N	Y			\N		\N	article	2016-05-04 11:04:38.941	\N	1984	\N	124	f
5858	Turtureanu2012	\N	10.7809/b-e.00203	\N	\N	\N	Database Beech Forests from Romanian Carpathians	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.504	\N	2012	\N	5859	f
135	Marstaller_1972_000044	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Schönberges bei Reinstädt (Kreis Jena-Thüringen)	000044	Jena	\N	\N	\N		TABGESNE	\N	\N	47	\N	Y			\N		\N	article	2016-05-04 11:04:41.263	\N	1972	\N	136	f
138	Kaiser_1960_000045	\N	\N	\N	\N	\N	Beiträge zur Vegetationskunde der thüringischen Rhön	000045	Jena	\N	\N	\N		LITTAB	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:04:41.49	\N	1960	\N	139	f
141	Klapp_1951_000046	\N	\N	\N	\N	\N	Borstgrasheiden der Mittelgebirge. Entstehung, Standort, Wert und Verbesserung	000046	Berlin, Hamburg	\N	\N	\N		LITTAB	\N	\N	60	\N	Y			\N		\N	article	2016-05-04 11:04:41.718	\N	1951	\N	142	f
129	Schroeder_1989_000042	\N	\N	\N	\N	\N	Der Vegetationskomplex der Sandtrockenrasen in der westfälischen Bucht	000042	Münster	\N	\N	\N		TV	\N	\N	578	\N	Y			\N		\N	article	2016-05-04 11:04:40.777	\N	1989	\N	130	f
5863	Chernenkova2012	\N	10.7809/b-e.00212	\N	\N	\N	Database Biodiversity of Murmansk Region	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.511	\N	2012	\N	5864	f
160	Borsch_1990_000052	\N	\N	\N	\N	\N	Die Vegetation extensiv genutzten und brachliegenden Grünlands im Naturschutzgebiet "Hinterste Neuwiese" (Vortaunus), Untersuchungen zur Sukzession auf Feuchtwiesen	000052	Frankfurt am Main	\N	\N	\N		\N	\N	\N	60	\N	Y			\N		\N	article	2016-05-04 11:04:43.085	\N	1990	\N	161	f
163	Lang_1973_000053	\N	\N	\N	\N	\N	Die Vegetation des westlichen Bodenseegebietes	000053	Jena	\N	LA	\N		BOHN	TV	\N	1024	\N	Y			\N		\N	article	2016-05-04 11:04:43.312	\N	1973	\N	164	f
169	Gregor_2001_000055	\N	\N	\N	\N	\N	Acidophile schafschwingelreiche Magerrasen im osthessischen Buntsandsteingebiet	000055	Frankfurt am Main	\N	\N	\N		\N	\N	\N	12	\N	Y			\N		\N	article	2016-05-04 11:04:43.807	\N	2001	\N	170	f
173	Denk_Wittig_1999_000056	\N	\N	\N	\N	\N	Die Vegetation der Streuobstwiesen im Main-Taunus-Kreis	000056	Frankfurt am Main	\N	\N	\N		\N	\N	\N	211	\N	Y			\N		\N	article	2016-05-04 11:04:44.057	\N	1999	\N	174	f
178	Teuber_1998_000058	\N	\N	\N	\N	\N	Felsgruslandschaften (Sedo-Scleranthetalia) im mittleren Lahn-Tal und im Gladenbacher Bergland	000058	Frankfurt am Main	\N	\N	\N		\N	\N	\N	122	\N	Y			\N		\N	article	2016-05-04 11:04:44.558	\N	1998	\N	179	f
181	Gregor_Wedra_1991_000059	\N	\N	\N	\N	\N	Vegetation unbewaldeter Kalkquellen des Main-Kinzig-Kreises	000059	Frankfurt am Main	\N	\N	\N		\N	\N	\N	29	\N	Y			\N		\N	article	2016-05-04 11:04:44.818	\N	1991	\N	182	f
188	Hofmeister_1990_000061	\N	\N	\N	\N	\N	Die Waldgesellschaften des Hildesheimer Waldes	000061	Göttingen	\N	\N	\N		\N	\N	\N	196	\N	Y			\N		\N	article	2016-05-04 11:04:45.331	\N	1990	\N	189	f
191	Ruthsatz_Krass_1998_000062	\N	\N	\N	\N	\N	Die Vegetation von Feuchtgrünlandbrachen im westlichen Hunsrück	000062	Bad Dürkheim	\N	\N	\N		\N	\N	\N	141	\N	Y			\N		\N	article	2016-05-04 11:04:45.593	\N	1998	\N	192	f
195	Popken_Licht_1998_000063	\N	\N	\N	\N	\N	Bodenkökologische Untersuchungen an wechselfeuchten Wiesen des östlichen Hunsrücks	000063	Bad Dürkheim	\N	\N	\N		\N	\N	\N	56	\N	Y			\N		\N	article	2016-05-04 11:04:45.823	\N	1998	\N	196	f
203	Libbert_1930_000066	\N	\N	\N	\N	\N	Die Vegetation des Fallsteingebietes	000066	Hannover	\N	\N	\N		\N	\N	\N	73	\N	Y			\N		\N	article	2016-05-04 11:04:46.555	\N	1930	\N	204	f
206	Diemont_1938_000067	\N	\N	\N	\N	\N	Zur Soziologie und Synoekologie der Buchen- und Buchenmischwälder der nordwestdeutschen Mittelgebirge	000067		\N	\N	\N		\N	\N	\N	183	\N	Y			\N		\N	article	2016-05-04 11:04:46.821	\N	1938	\N	207	f
214	Knapp_1969_000069	\N	\N	\N	\N	\N	Kalkvegetation in tiefen Lagen der Rhön und ihrem Vorland	000069	Darmstadt	\N	\N	\N		\N	\N	\N	15	\N	Y			\N		\N	article	2016-05-04 11:04:47.335	\N	1969	\N	215	f
216	Korsch_1994_000070	\N	\N	\N	\N	\N	Die Kalkflachmoore Thüringens, Flora, Vegetation und Dynamik	000070	Jena	\N	KOS	\N		TABGESNE	TV	\N	260	\N	Y			\N		\N	article	2016-05-04 11:04:47.592	\N	1994	\N	217	f
218	Knapp_Reichhoff_1975_000071	\N	\N	\N	\N	\N	Die Vegetation des Leutratals bei Jena	000071	Berlin	\N	KNR	\N		TABGESNE	TV	\N	119	\N	Y			\N		\N	article	2016-05-04 11:04:47.857	\N	1975	\N	219	f
224	Rech_1995_000073	\N	\N	\N	\N	\N	Vegetationskundliche und standortökologische Untersuchungen an Edellaubwäldern der mittleren Nahe	000073	Bad Dürkheim	\N	\N	\N		\N	\N	\N	29	\N	Y			\N		\N	article	2016-05-04 11:04:48.349	\N	1995	\N	225	f
147	Loetschert_1973_000048	\N	\N	\N	\N	\N	Die Pflanzengesellschaften im Rhein-, Main- und Taunusgebiet	000048	Wiesbaden	\N	\N	\N		LITTAB	\N	\N	7	\N	Y			\N		\N	article	2016-05-04 11:04:42.175	\N	1973	\N	148	f
230	Haffner_1969_000075	\N	\N	\N	\N	\N	Das Pflanzenkleid des Naheberglandes und des südlichen Hunsrück in ökologisch-geographischer Sicht	000075	Bonn	\N	\N	\N		\N	\N	\N	4	\N	Y			\N		\N	article	2016-05-04 11:04:48.96	\N	1969	\N	231	f
233	Kieckbusch_1998_000076	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen am Südufer der Schlei	000076	Kiel	\N	\N	\N		\N	\N	\N	128	\N	Y			\N		\N	article	2016-05-04 11:04:49.18	\N	1998	\N	234	f
5866	Dengler2012#149	\N	10.7809/b-e.00114	\N	\N	\N	Database Dry Grasslands in the Nordic and Baltic Region	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.521	\N	2012	\N	5867	f
5869	Hunziker2012	\N	10.7809/b-e.00132	\N	\N	\N	Database Dry Meadows and Pastures of Switzerland	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.531	\N	2012	\N	5870	f
5876	Yamalov2012	\N	10.7809/b-e.00089	\N	\N	\N	Database Meadows and Steppes of South Ural	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.545	\N	2012	\N	5877	f
247	Pflume_1999_000081	\N	\N	\N	\N	\N	Laubwaldgesellschaften im Harz	000081	Wiehl	\N	\N	0000000001		\N	\N	\N	431	\N	Y			\N		\N	article	2016-05-04 11:04:50.482	\N	1999	\N	248	f
250	Kruse_1986_000082	\N	\N	\N	\N	\N	Laubwald-Gesellschaften im Innerste-Bergland	000082	Göttingen	\N	SK	\N		BOHN	SPFLUME	\N	381	\N	Y			\N		\N	article	2016-05-04 11:04:50.704	\N	1986	\N	251	f
5879	Zouhar2012	\N	10.7809/b-e.00137	\N	\N	\N	Database of Czech Forest Classification System	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.577	\N	2012	\N	5880	f
5882	Alvarez2012#111	\N	10.7809/b-e.00228	\N	\N	\N	Database of Ephemeral Wetland Vegetation in Extra- and Oro-Tropical South America	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.59	\N	2012	\N	5883	f
258	Denz_1994_000085	\N	\N	\N	\N	\N	Natürliche Habichtskraut-Traubeneichenwälder bodensaurer Felsstandorte und ihre Vegetationskomplexe im Rheinischen Schiefergebirge und weiteren silikatischen Mittelgebirgen	000085	Berlin, Stuttgart	\N	\N	\N		\N	\N	\N	6	\N	Y			\N		\N	article	2016-05-04 11:04:51.447	\N	1994	\N	259	f
5885	Toms2012	\N	10.7809/b-e.00218	\N	\N	\N	Database of Forest Understory Vegetation, British Columbia  vascular plants and bryophytes in old-growth and clear-cut forests, Vancouver Island	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.604	\N	2012	\N	5886	f
387	Bergmeier_1986_000129	\N	\N	\N	\N	\N	Vegetation und Flora des NSG "Nachtweid von Dauernheim"	000129		\N	\N	\N		\N	\N	\N	35	\N	Y			\N		\N	article	2016-05-04 11:05:02.355	\N	1986	\N	388	f
5890	Suchrow2012#250	\N	10.7809/b-e.00159	\N	\N	\N	Database of German North Sea Salt Marshes  changes in vegetation and elevation	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.612	\N	2012	\N	5891	f
5892	Suchrow2012	\N	10.7809/b-e.00160	\N	\N	\N	Database of German North Sea Salt Marshes  vegetation and elevation	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.625	\N	2012	\N	5893	f
5270	Jansen_1998_100498b	\N	\N	\N	\N	\N	Vegetationsaufnahmen im überfluteten Polder zwischen Dänischer Wieck und L262	100498.1	\N	\N	\N	unveröffentlicht	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Universität Greifswald	\N	unknown	2016-07-22 08:50:57.208	\N	1998	\N	\N	t
5279	Hanzlik_Gerowitt_2012_null	\N	\N	\N	\N	\N	Occurrence and distribution of important weed species in German winter oilseed rape fields	100713	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	unknown	2016-07-22 08:55:33.673	\N	2012	\N	5280	t
267	Glavac_Krause_1969_000088	\N	\N	\N	\N	\N	Über bodensaure Wald- und Gebüschgesellschaften trockenwarmer Standorte im Mittelrheingebiet	000088	Bonn-Bad Godesberg	\N	\N	\N		\N	\N	\N	113	\N	Y			\N		\N	article	2016-05-04 11:04:52.154	\N	1969	\N	268	f
5898	Dajic_Stevanovic2012	\N	10.7809/b-e.00205	\N	\N	\N	Database of Halophytic Vegetation in Serbia	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.637	\N	2012	\N	5899	f
5900	Chytrý2012#137	\N	10.7809/b-e.00088	\N	\N	\N	Database of Masaryk University's Vegetation Research in Siberia	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.659	\N	2012	\N	5901	f
5904	Zverev2012	\N	10.7809/b-e.00108	\N	\N	\N	Database of Siberian Vegetation (DSV)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.667	\N	2012	\N	5905	f
5908	Dengler2012	\N	10.7809/b-e.00138	\N	\N	\N	Database Polygono-Poetea annuae of Germany	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.68	\N	2012	\N	5909	f
5924	Dengler2012#148	\N	10.7809/b-e.00115	\N	\N	\N	Database Species-Area Relationships in Palaearctic Grasslands	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.696	\N	2012	\N	5925	f
5928	Dölle2012#156	\N	10.7809/b-e.00149	\N	\N	\N	Database Temperate Deciduous and Coniferous Forests of the Solling Hills	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.76	\N	2012	\N	5929	f
5931	Wulf2012#273	\N	10.7809/b-e.00139	\N	\N	\N	Database Temperate Deciduous Forests of the Elbe-Weser Region	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.776	\N	2012	\N	5932	f
303	Zacharias_1996_000100	\N	\N	\N	\N	\N	Flora und Vegetation von Wäldern der QUERCO-FAGETEA im nördlichen Harzvorland Niedersachsens unter besonderer Berücksichtigung der Eichen-Hainbuchen-Mittelwälder	000100	Hannover	\N	\N	\N		\N	\N	\N	396	\N	Y			\N		\N	article	2016-05-04 11:04:55.148	\N	1996	\N	304	f
306	Budde_1952_000101	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der Wälder, Heiden und Quellen im Astengebirge, Westfalen	000101	Bonn	\N	\N	\N		\N	\N	\N	31	\N	Y			\N		\N	article	2016-05-04 11:04:55.403	\N	1952	\N	307	f
5933	Wulf2012#272	\N	10.7809/b-e.00140	\N	\N	\N	Database Temperate Deciduous Forests of the Prignitz Region	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.787	\N	2012	\N	5934	f
5935	Wulf2012	\N	10.7809/b-e.00141	\N	\N	\N	Database Temperate Deciduous Forests of the Uckermark Region	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.795	\N	2012	\N	5936	f
5942	Cancellieri2012	\N	10.7809/b-e.00188	\N	\N	\N	Deciduous Oak Forests Database of the Italian Peninsula	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.801	\N	2012	\N	5943	f
5948	Alt2012	\N	10.7809/b-e.00156	\N	\N	\N	Disturbances and Biodiversity at Grafenwöhr Training Area	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.822	\N	2012	\N	5949	f
5951	Alt2012#109	\N	10.7809/b-e.00155	\N	\N	\N	Disturbances and Biodiversity in the Fichtelgebirge	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.843	\N	2012	\N	5952	f
5953	Becker2012	\N	10.7809/b-e.00164	\N	\N	\N	Dry Grassland Central Germany Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.86	\N	2012	\N	5954	f
5960	Facioni2012	\N	10.7809/b-e.00187	\N	\N	\N	Dry Grasslands Database of Central Italy	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.868	\N	2012	\N	5961	f
5963	Homeier2012	\N	10.7809/b-e.00232	\N	\N	\N	Ecuador Forest Plots Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.893	\N	2012	\N	5964	f
338	Beiter_1991_000112	\N	\N	\N	\N	\N	Dauerbeobachtungsflächen in Naturschutzgebieten der Schwäbischen Alb	000112	Karlsruhe	\N	\N	\N		\N	\N	\N	36	\N	Y			\N		\N	article	2016-05-04 11:04:58.334	\N	1991	\N	339	f
341	Hornung_1991_000113	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen im Naturschutzgebiet "Bürgle" im Killertal	000113	Karlsruhe	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:04:58.557	\N	1991	\N	342	f
345	Roweck_Schweikle_1995_000114	\N	\N	\N	\N	\N	Böden und Vegetation im Wassereinzugsgebiet des Schmiecher Sees	000114	Karlsruhe	\N	\N	\N		\N	\N	\N	50	\N	Y			\N		\N	article	2016-05-04 11:04:58.781	\N	1995	\N	346	f
5966	Jovan2012	\N	10.7809/b-e.00226	\N	\N	\N	Epiphytic Macrolichen Community Composition Database  epiphytic lichen synusiae in forested areas of the US	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.899	\N	2012	\N	5967	f
5969	Paal2012	\N	10.7809/b-e.00166	\N	\N	\N	Estonian Vegetation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.905	\N	2012	\N	5970	f
359	Genser_1991_000119	\N	\N	\N	\N	\N	Die Wacholderheiden des "Eselsburger Tal" (Ostalb)	000119	Karlsruhe	\N	\N	\N		\N	TV	\N	66	\N	Y			\N		\N	article	2016-05-04 11:04:59.991	\N	1991	\N	360	f
362	Gauckler_1938_000120	\N	\N	\N	\N	\N	Steppenheide und Steppenheidewald der Fränkischen Alb in pflanzensoziologischer, ökologischer und geographischer Betrachtung	000120	München	\N	\N	\N		\N	\N	\N	207	\N	Y			\N		\N	article	2016-05-04 11:05:00.212	\N	1938	\N	363	f
365	Altehage_1960_000121	\N	\N	\N	\N	\N	Die Vegetationsverhältnisse des Naturschutzgebietes "Berger Keienvenn" im Kreise Lingen	000121	Osnabrück	\N	\N	\N		\N	\N	\N	2	\N	Y			\N		\N	article	2016-05-04 11:05:00.466	\N	1960	\N	366	f
367	Altehage_1962_000122	\N	\N	\N	\N	\N	Zwei ehemalige atlantische Florenstätten unweit Schüttorf und Friesoyte in Niedersachsen	000122		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:00.693	\N	1962	\N	368	f
370	Altrock_1987_000123	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen am Vollstedter See unter besonderer Berücksichtigung der Verlandungs-, Niedermoor- und Feuchtgrünland-Gesellschaften	000123		\N	\N	\N		\N	\N	\N	140	\N	Y			\N		\N	article	2016-05-04 11:05:00.922	\N	1987	\N	371	f
372	Altrock_1991_000124	\N	\N	\N	\N	\N	Die Grünland-Vegetation des Naturschutzgebietes "Duvenstedter Brook"	000124	Hamburg	\N	\N	\N		\N	\N	\N	28	\N	Y			\N		\N	article	2016-05-04 11:05:01.175	\N	1991	\N	373	f
5975	De_Sanctis2012	\N	10.7809/b-e.00196	\N	\N	\N	EVSItalia Database HABITAT OF ITALY	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.914	\N	2012	\N	5976	f
378	Arndt_1955_000126	\N	\N	\N	\N	\N	Kohldistelwiesen der Randgebiete des Oberspreewaldes	000126	Stolzenau/Weser	\N	\N	\N		\N	\N	\N	10	\N	Y			\N		\N	article	2016-05-04 11:05:01.629	\N	1955	\N	379	f
381	Asmus_1987_000127	\N	\N	\N	\N	\N	Die Vegetation der Fließgewässerränder im Einzugsgebiet der Regnitz. Eine pflanzen- und gesellschaftssoziologische Untersuchung zum Zustand der Ufervegetation an ausgewählten Gewässerabschnitten	000127	Regensburg	\N	\N	\N		\N	\N	\N	6	\N	Y			\N		\N	article	2016-05-04 11:05:01.862	\N	1987	\N	382	f
384	Baumgarten_1978_000128	\N	\N	\N	\N	\N	Pflanzensoziologische und landschaftskundliche Dokumentation des oberen Hardantales im Regierungsbezirk Lüneburg	000128	Lüneburg	\N	\N	\N		\N	\N	\N	171	\N	Y			\N		\N	article	2016-05-04 11:05:02.139	\N	1978	\N	385	f
5980	Agrillo2012#106	\N	10.7809/b-e.00191	\N	\N	\N	EVSItalia Database of Broadleaved Deciduous Submediterranean Forests	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.933	\N	2012	\N	5981	f
5983	Agrillo2012#105	\N	10.7809/b-e.00193	\N	\N	\N	EVSItalia Database of Broadleaved Temperate Deciduous Forests	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.949	\N	2012	\N	5984	f
5986	Agrillo2012#107	\N	10.7809/b-e.00190	\N	\N	\N	EVSItalia Database of Dry Grasslands	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.962	\N	2012	\N	5987	f
5989	Agrillo2012#104	\N	10.7809/b-e.00194	\N	\N	\N	EVSItalia Database of Evergreen Mediterranean Forests	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.974	\N	2012	\N	5990	f
5992	Agrillo2012	\N	10.7809/b-e.00195	\N	\N	\N	EVSItalia Database of Mires and Wetlands of Italy	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.987	\N	2012	\N	5993	f
5999	Dengler2012#151	\N	10.7809/b-e.00055	\N	\N	\N	Facilitating access to vegetation data  Introduction to the Special Volume	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:48.997	\N	2012	\N	6000	f
6003	Schulz2012	\N	10.7809/b-e.00223	\N	\N	\N	FIADB Vegetation Diversity and Structure Indicator (VEG)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.03	\N	2012	\N	6004	f
6007	Spiegelberger2012	\N	10.7809/b-e.00169	\N	\N	\N	FLOREM  a floristic database on mountain ecosystems	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.046	\N	2012	\N	6008	f
391	Bergmeier_Nowak_Wedra_1984_000130	\N	\N	\N	\N	\N	Silaum silaus- und Senecio aquaticus-Wiesen in Hessen. Ein Beitrag zu ihrer Systematik, Verbreitung und Ökologie	000130	Göttingen	\N	\N	\N		THOMAS	\N	\N	103	\N	Y			\N		\N	article	2016-05-04 11:05:02.627	\N	1984	\N	392	f
6021	Alvarez2012#113	\N	10.7809/b-e.00060	\N	\N	\N	Floristic classification of the vegetation in small wetlands of Kenya and Tanzania	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.059	\N	2012	\N	6022	f
6024	Daget2012	\N	10.7809/b-e.00091	\N	\N	\N	FLOTROP  a biogeographical database on rangeland vegetation for northern tropical Africa	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.105	\N	2012	\N	6025	f
6031	Gray2012	\N	10.7809/b-e.00079	\N	\N	\N	Forest Inventory and Analysis Database of the United States of America (FIA)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.114	\N	2012	\N	6032	f
404	Mahn_Schubert_1962_000135	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen in der mitteldeutschen Ackerlandschaft VI	000135	Halle (Saale)	\N	\N	\N		\N	\N	\N	89	\N	Y			\N		\N	article	2016-05-04 11:05:03.584	\N	1962	\N	405	f
407	Buchwald_1942_000136	\N	\N	\N	\N	\N	Zur soziologischen Zugehörigkeit von Fritillaria meleagris L.	000136	Hannover	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:03.842	\N	1942	\N	408	f
413	Budde_Brockhaus_1954_000138	\N	\N	\N	\N	\N	Die Vegetation des südwestfälischen Berglandes	000138	Bonn	\N	\N	\N		\N	\N	\N	69	\N	Y			\N		\N	article	2016-05-04 11:05:04.32	\N	1954	\N	414	f
426	Burrichter_1969_000143	\N	\N	\N	\N	\N	Das Zwillbrocker Venn, Westmünsterland, in moor- und vegetationskundlicher Sicht. Mit einem Beitrag zur Wald- und Siedlungsgeschichte seiner Umgebung	000143	Münster	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:05.621	\N	1969	\N	427	f
429	Coenen_1981_000144	\N	\N	\N	\N	\N	Flora und Vegetation der Heidegewässer und -moore auf den Maasterrassen im deutsch-niederländischen Grenzgebiet	000144	Bonn	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:05.875	\N	1981	\N	430	f
431	Dierschke_1967_000145	\N	\N	\N	\N	\N	Ein neues Vorkommen des Wasserseggen-Sumpfes (Lysimachio-Caricetum aqualis Neum. 1957) in Nordwestdeutschland	000145	Münster	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:06.135	\N	1967	\N	432	f
433	Dierschke_1968_000146	\N	\N	\N	\N	\N	Zur synsystematischen und syndynamischen Stellung einiger Calthion-Wiesen mit Ranunculus auricomus L. und Primula elatior im Wümme-Gebiet	000146	Göttingen	\N	\N	\N		\N	\N	\N	33	\N	Y			\N		\N	article	2016-05-04 11:05:06.368	\N	1968	\N	434	f
435	Dierschke_1969_000147	\N	\N	\N	\N	\N	Pflanzensoziologische Exkursionen im Harz. Bericht über die Tagung der Floristisch-soziologischen Arbeitsgemeinschaft in Osterode vom 14. bis 16. 1968	000147	Göttingen	\N	DI	\N		BOHN	TV	\N	8	\N	Y			\N		\N	article	2016-05-04 11:05:06.649	\N	1969	\N	436	f
437	Dierschke_1979_000148	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Holtumer Moores und seiner Randgebiete	000148	Göttingen	\N	\N	\N		\N	\N	\N	162	\N	Y			\N		\N	article	2016-05-04 11:05:06.923	\N	1979	\N	438	f
439	Dierschke_1986_000149	\N	\N	\N	\N	\N	Vegetationsdifferenzierung im Mikrorelief nordwestdeutscher sandiger Flußtäler am Beispiel der Meppener Kuhweide (Ems)	000149	Bailleul	\N	\N	\N		\N	\N	\N	14	\N	Y			\N		\N	article	2016-05-04 11:05:07.199	\N	1986	\N	440	f
443	Dierschke_Otte_Nordmann_1983_000150	\N	\N	\N	\N	\N	Die Ufervegetation der Fließgewässer des Westharzes und seines Vorlandes	000150	Hannover	\N	\N	\N		\N	\N	\N	67	\N	Y			\N		\N	article	2016-05-04 11:05:07.474	\N	1983	\N	444	f
445	Dierschke_Vogel_1981_000151	\N	\N	\N	\N	\N	Wiesen- und Magerrasen-Gesellschaften des Westharzes	000151	Göttingen	\N	DVO	\N		\N	\N	\N	361	\N	Y			\N		\N	article	2016-05-04 11:05:07.751	\N	1981	\N	446	f
416	Bueker_1938_000139	\N	\N	\N	\N	\N	Über die Vegetation einiger Pflanzenschutzgebiete im Kreis Tecklenburg	000139	Münster	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:04.58	\N	1938	\N	417	f
452	Dransfeld_1966_000154	\N	\N	\N	\N	\N	Kalkflachmoore in Thüringen	000154	Jena	\N	\N	\N		\N	\N	\N	10	\N	Y			\N		\N	article	2016-05-04 11:05:08.584	\N	1966	\N	453	f
6036	Lopez-Gonzalez2012	\N	10.7809/b-e.00064	\N	\N	\N	ForestPlots.net  managing permanent plot information across the tropics	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.136	\N	2012	\N	6037	f
461	Foerster_1983_000157	\N	\N	\N	\N	\N	Pflanzengesellschaften des Grünlandes in Nordrhein-Westfalen	000157	Recklinghausen	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:09.365	\N	1983	\N	462	f
464	Franke_1987_000158	\N	\N	\N	\N	\N	Pflanzengesellschaften der Fränkischen Teichlandschaft	000158	Bamberg	\N	\N	\N		\N	\N	\N	34	\N	Y			\N		\N	article	2016-05-04 11:05:09.616	\N	1987	\N	465	f
467	Freitag_1957_000159	\N	\N	\N	\N	\N	Vegetationskundliche Beobachtungen an Grünland-Gesellschaften im Nieder-Oderbruch	000159	Potsdam	\N	\N	\N		\N	\N	\N	31	\N	Y			\N		\N	article	2016-05-04 11:05:09.909	\N	1957	\N	468	f
473	Fritsch_1962_000161	\N	\N	\N	\N	\N	Die Pfeifengraswiesen und andere Grünlandgesellschaften des Teufelbruches bei Hennigsdorf	000161	Potsdam	\N	\N	\N		\N	\N	\N	63	\N	Y			\N		\N	article	2016-05-04 11:05:10.417	\N	1962	\N	474	f
479	Fukarek_1961_000163	\N	\N	\N	\N	\N	Die Vegetation des Darß und ihre Geschichte	000163	Jena	\N	\N	\N		\N	\N	\N	11	\N	Y			\N		\N	article	2016-05-04 11:05:10.894	\N	1961	\N	480	f
482	Gaertner_1961_000164	\N	\N	\N	\N	\N	Die Vegetationsverhältnisse der Wiesen am Barsbecker See	000164	Kiel	\N	\N	\N		\N	\N	\N	7	\N	Y			\N		\N	article	2016-05-04 11:05:11.148	\N	1961	\N	483	f
488	Garniel_1993_000166	\N	\N	\N	\N	\N	Die Vegetation der Karpfenteiche Schleswig-Holsteins. Inventarisierung - Sukzessionsprognose - Schutzkonzepte	000166	Kiel	\N	\N	\N		\N	\N	\N	17	\N	Y			\N		\N	article	2016-05-04 11:05:11.671	\N	1993	\N	489	f
491	Gilcher_1993_000167	\N	\N	\N	\N	\N	Vegetationskundliche Studien im Deusmaurer Moor zwischen Dietkirchen und Weickenhammer	000167	Regensburg	\N	\N	\N		\N	\N	\N	57	\N	Y			\N		\N	article	2016-05-04 11:05:11.896	\N	1993	\N	492	f
470	Freitag_Koertge_1958_000160	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Zarth bei Treuenbrietzen	000160	Potsdam	\N	\N	\N		\N	\N	\N	76	\N	Y			\N		\N	article	2016-05-04 11:05:10.123	\N	1958	\N	471	f
6038	Bergmeier2012#123	\N	10.7809/b-e.00174	\N	\N	\N	Greek Woodland Vegetation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.261	\N	2012	\N	6039	f
514	Hanspach_1989_000176	\N	\N	\N	\N	\N	Untersuchungen zur aktuellen Vegetation des Schraden (Bezirk Cottbus)	000176	Berlin	\N	\N	\N		\N	\N	\N	69	\N	Y			\N		\N	article	2016-05-04 11:05:13.842	\N	1989	\N	515	f
6040	Glöckler2012#170	\N	10.7809/b-e.00062	\N	\N	\N	Guide to GIVD's Fact Sheets	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.28	\N	2012	\N	6041	f
519	Harm_1990_000178	\N	\N	\N	\N	\N	Kleinseggenriede (Scheuchzerio-Caricetea fuscae) im Südwest-Harz	000178	Göttingen	\N	\N	\N		LITTAB	\N	\N	47	\N	Y			\N		\N	article	2016-05-04 11:05:14.366	\N	1990	\N	520	f
6044	Tsiripidis2012	\N	10.7809/b-e.00179	\N	\N	\N	Hellenic Beech Forests Database (Hell-Beech-DB)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.31	\N	2012	\N	6045	f
527	Heinken_1985_000181	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Fuhsetals zwischen Peine und Abbensen/Oelerse (Landkreis Peine)	000181	Peine	\N	\N	\N		\N	\N	\N	29	\N	Y			\N		\N	article	2016-05-04 11:05:15.04	\N	1985	\N	528	f
574	Joens_1934_000199	\N	\N	\N	\N	\N	Der Bültsee und seine Vegetation	000199		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:19.305	\N	1934	\N	575	f
530	Hillmann_1993_000182	\N	\N	\N	\N	\N	Das Naturschutzgebiet "Bornhorster Huntewiesen" - Geologie, Vegetation und Schutzstatus sowie vegtationskundliche Bestandsaufnahmen und Pflegekonzepte	000182	Wardenburg	\N	\N	\N		\N	\N	\N	9	\N	Y			\N		\N	article	2016-05-04 11:05:15.261	\N	1993	\N	531	f
6046	Dimopoulos2012	\N	10.7809/b-e.00177	\N	\N	\N	Hellenic Natura 2000 Vegetation Database (HelNatVeg)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.345	\N	2012	\N	6047	f
6050	Fotiadis2012	\N	10.7809/b-e.00178	\N	\N	\N	Hellenic Woodland Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.375	\N	2012	\N	6051	f
6056	Castell2012	\N	10.7809/b-e.00116	\N	\N	\N	Iberian and Macaronesian Vegetation Information System (SIVIM)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.413	\N	2012	\N	6057	f
6060	FitzPatrick2012	\N	10.7809/b-e.00184	\N	\N	\N	Irish Vegetation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.439	\N	2012	\N	6061	f
538	Hollmann_1972_000185	\N	\N	\N	\N	\N	Verbreitung und Soziologie der Schachblume Fritillaria meleadris L.	000185		\N	\N	\N		\N	\N	\N	130	\N	Y			\N		\N	article	2016-05-04 11:05:15.998	\N	1972	\N	539	f
476	Froede_1958_000162	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der Insel Hiddensee	000162	Greifswald	\N	\N	\N		\N	\N	\N	5	\N	Y			\N		\N	article	2016-05-04 11:05:10.633	\N	1958	\N	477	f
493	Goers_1951_000169	\N	\N	\N	\N	\N	Lebenshaushalt der Flach- u. Zwischenmoorgesellschaften im württembergischen Allgäu	000169	Ludwigsburg	\N	\N	\N		\N	\N	\N	17	\N	Y			\N		\N	article	2016-05-04 11:05:12.124	\N	1951	\N	494	f
548	Hundt_1954_000188	\N	\N	\N	\N	\N	Grünlandgesellschaften an der unteren Mulde und mittleren Elbe	000188	Halle (Saale)	\N	\N	\N		\N	\N	\N	64	\N	Y			\N		\N	article	2016-05-04 11:05:16.756	\N	1954	\N	549	f
550	Hundt_1958_000189	\N	\N	\N	\N	\N	Die Wiesenvegetation in der Nutheniederung bei Nedlitz, Grimme und Polenzko	000189	Halle (Saale)	\N	\N	\N		\N	\N	\N	35	\N	Y			\N		\N	article	2016-05-04 11:05:16.979	\N	1958	\N	551	f
552	Hundt_1964_000190	\N	\N	\N	\N	\N	Die Bergwiesen des Harzes, Thüringer Waldes und Erzgebirges	000190	Jena	\N	\N	\N		\N	\N	\N	60	\N	Y			\N		\N	article	2016-05-04 11:05:17.204	\N	1964	\N	553	f
554	Hundt_1964_000191	\N	\N	\N	\N	\N	Vegetation, Feuchtigkeitsverhältnisse und Ertragsverhältnisse der Wiesenflächen im Luhne-Rückhaltebecken bei Lengefeld (Thüringen)	000191	Halle (Saale)	\N	\N	\N		\N	\N	\N	11	\N	Y			\N		\N	article	2016-05-04 11:05:17.462	\N	1964	\N	555	f
556	Hundt_1965_000192	\N	\N	\N	\N	\N	Die Gebirgswiesen im Osterzgebirge	000192	Dresden	\N	\N	\N		\N	\N	\N	14	\N	Y			\N		\N	article	2016-05-04 11:05:17.688	\N	1965	\N	557	f
759	Passarge_1964_000272	\N	\N	\N	\N	\N	Über Pflanzengesellschaften des Hagenower Landes	000272	Rostock	\N	\N	\N		\N	\N	\N	146	133	Y			\N		\N	article	2016-05-04 11:05:37.087	\N	1964	\N	760	f
507	Gruettner_1987_000174	\N	\N	\N	\N	\N	Das Naturschutzgebiet "Briglirain" bei Furtwangen (Mittlerer Schwarzwald)	000174	Karlsruhe	\N	\N	\N		\N	\N	\N	73	\N	Y			\N		\N	article	2016-05-04 11:05:13.394	\N	1987	\N	508	f
560	Hundt_1975_000194	\N	\N	\N	\N	\N	Bestands- und Standortsveränderungen des Grünlandes in einem Rückhaltebecken als Folge des periodischen Wasseranstaus	000194	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:18.143	\N	1975	\N	561	f
6063	Olsvig-Whittaker2012	\N	10.7809/b-e.00100	\N	\N	\N	Israel Nature and Parks Authority (INPA) Biological Databases	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.456	\N	2012	\N	6064	f
568	Jeschke_1964_000197	\N	\N	\N	\N	\N	Die Vegetation der Stubnitz	000197		\N	\N	\N		\N	\N	\N	18	\N	Y			\N		\N	article	2016-05-04 11:05:18.886	\N	1964	\N	569	f
571	Jonas_1932_000198	\N	\N	\N	\N	\N	Der Hammrich. Die Vegetationseinheiten eines Flachmoores an der Unterems	000198	Berlin	\N	\N	\N		\N	\N	\N	24	\N	Y			\N		\N	article	2016-05-04 11:05:19.098	\N	1932	\N	572	f
6068	Casella2012	\N	10.7809/b-e.00192	\N	\N	\N	Italian National Vegetation Database (BVN/ISPRA)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.468	\N	2012	\N	6069	f
582	Klapp_Boeker_Bohne_Bothmer_Grieger_Kmoch_1954_000201	\N	\N	\N	\N	\N	Die Grünlandvegetation des Eifelkreises Daun und ihre Beziehungen zu den Bodengesellschaften	000201	Wien	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:19.839	\N	1954	\N	583	f
589	Kleyda_1993_000203	\N	\N	\N	\N	\N	Die Fließgewässer der Gemeinde Wardenburg, ihre Ufer- und Böschungsvegetation sowie die ökologische Zustandsbeschreibung der Stapelriede mit einem Konzept zur "naturnahen Umgestaltung"	000203	Wardenburg	\N	\N	\N		\N	\N	\N	20	\N	Y			\N		\N	article	2016-05-04 11:05:20.3	\N	1993	\N	590	f
592	Kloss_1965_000204	\N	\N	\N	\N	\N	Schoenetum, Juncetum subnodulosi und Betula pubescens-Gesellschaften der kalkreichen Moorniederungen Nordost-Mecklenburgs	000204	Berlin	\N	\N	\N		\N	\N	\N	284	\N	Y			\N		\N	article	2016-05-04 11:05:20.522	\N	1965	\N	593	f
6072	Cornwell2012	\N	10.7809/b-e.00220	\N	\N	\N	Jasper Ridge Woody Plant Community Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.489	\N	2012	\N	6073	f
6079	Cerný2012	\N	10.7809/b-e.00097	\N	\N	\N	Korean Forest Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.508	\N	2012	\N	6080	f
6081	Bergmeier2012#124	\N	10.7809/b-e.00173	\N	\N	\N	KRITI  the vegetation of Crete database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.54	\N	2012	\N	6082	f
6084	Mucina2012	\N	10.7809/b-e.00106	\N	\N	\N	Kuwait Vegetation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.544	\N	2012	\N	6085	f
602	Knapp_1951_000209	\N	\N	\N	\N	\N	Vegetationsaufnahmen von Wiesen des Vogelsberges	000209	Lauterbach/Hessen	\N	\N	\N		\N	\N	\N	42	\N	Y			\N		\N	article	2016-05-04 11:05:21.764	\N	1951	\N	603	f
6089	Csecserits2012	\N	10.7809/b-e.00182	\N	\N	\N	Long-term Database of Sandy Grassland of Fulophaza	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.55	\N	2012	\N	6090	f
606	Knapp_1974_000211	\N	\N	\N	\N	\N	Submontane Rasen im nördlichen Taunus in ihrer Bedeutung für diese Mittelgebirgs-Landschaft und ihre Entwicklung	000211	Gießen	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:22.265	\N	1974	\N	607	f
608	Knapp_1977_000212	\N	\N	\N	\N	\N	Moor-Rasen im Gießener Becken und in der nördlichen Wetterau	000212	Gießen	\N	\N	\N		\N	\N	\N	19	\N	Y			\N		\N	article	2016-05-04 11:05:22.501	\N	1977	\N	609	f
614	Konczak_1968_000214	\N	\N	\N	\N	\N	Die Wasser- und Sumpfpflanzengesellschaften der Havelseen um Potsdam	000214	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:22.987	\N	1968	\N	615	f
617	Konold_1984_000215	\N	\N	\N	\N	\N	Zur Ökologie kleiner Fließgewässer. Verschiedene Ausbauarten und ihre Bewertung	000215	Stuttgart	\N	\N	\N		\N	\N	\N	27	\N	Y			\N		\N	article	2016-05-04 11:05:23.248	\N	1984	\N	618	f
619	Konold_1987_000216	\N	\N	\N	\N	\N	Oberschwäbische Weiher und Seen, Teil II	000216	Karlsruhe	\N	\N	\N		\N	\N	\N	7	\N	Y			\N		\N	article	2016-05-04 11:05:23.468	\N	1987	\N	620	f
623	Pusch_Barthel_2003_000217	\N	\N	\N	\N	\N	Zum Vorkommen der Stipa-Arten im Kyffhäusergebirge	000217		\N	\N	\N		Pusch_Barthel_2003	\N	\N	28	\N	Y			\N		\N	article	2016-05-04 11:05:23.686	\N	2003	\N	624	f
6093	Sorokin2012#241	\N	10.7809/b-e.00207	\N	\N	\N	Lower Volga Valley Phytosociological Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.56	\N	2012	\N	6094	f
656	Lein-Kottmeier_Oertel_1991_000231	\N	\N	\N	\N	\N	Feuchtgrünlanderfassung in Hannover	000231	Hannover	\N	\N	\N		\N	\N	\N	90	\N	Y			\N		\N	article	2016-05-04 11:05:26.901	\N	1991	\N	657	f
659	Lenski_1953_000232	\N	\N	\N	\N	\N	Grünlanduntersuchungen im mittleren Oste-Tal	000232	Stolzenau/Weser	\N	\N	\N		\N	\N	\N	85	\N	Y			\N		\N	article	2016-05-04 11:05:27.223	\N	1953	\N	660	f
627	Haenel_Mueller_2006_000218	\N	\N	\N	\N	\N	Zur Verbreitung, Vergesellschaftung und Ökologie der Wiesen-Siegwurz (Gladiolus imbricatus L.) in Sachsen	000218		\N	\N	\N		H�nel_M�ller_2006	\N	\N	22	\N	Y			\N		\N	article	2016-05-04 11:05:23.901	\N	2006	\N	628	f
586	Klemm_Koenig_1993_000202	\N	\N	\N	\N	\N	Gosener Wiesen und NO-Teilo Seddinsee (Berlin-Köpenick) Flora und Vegetation (Teil 2)	000202	Berlin	\N	\N	\N		\N	\N	\N	22	\N	Y			\N		\N	article	2016-05-04 11:05:20.061	\N	1993	\N	587	f
5567	Rieck_1992_101070	\N	\N	\N	\N	\N	Die Xeroserie der Dünenkomplexe von Wangerooge.	101070	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:16.781	\N	1992	\N	5568	f
6097	Rosati2012	\N	10.7809/b-e.00186	\N	\N	\N	Lucanian Vegetation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.577	\N	2012	\N	6098	f
6101	Dolnik2012#154	\N	10.7809/b-e.00161	\N	\N	\N	Lübeck Beech Forest Biodiversity Plots	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.586	\N	2012	\N	6102	f
6105	Azzella2012	\N	10.7809/b-e.00189	\N	\N	\N	Macrophytes of Italian Volcanic Lakes Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.6	\N	2012	\N	6106	f
6108	Aunina2012	\N	10.7809/b-e.00198	\N	\N	\N	Mire Vegetation Database of Latvia	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.619	\N	2012	\N	6109	f
6113	Tikhonova2012	\N	10.7809/b-e.00211	\N	\N	\N	Moscow Region Forest Vegetation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.63	\N	2012	\N	6114	f
663	Libbert_1940_000234	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der Halbinsel Darß	000234	Berlin	\N	\N	\N		x	\N	\N	11	\N	Y			\N		\N	article	2016-05-04 11:05:27.731	\N	1940	\N	664	f
666	Lippert_1966_000235	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Naturschutzgebietes "Berchtesgaden"	000235	München	\N	\N	\N		\N	\N	\N	144	\N	Y			\N		\N	article	2016-05-04 11:05:28.002	\N	1966	\N	667	f
668	Loetschert_1984_000236	\N	\N	\N	\N	\N	Pflanzengesellschaften im Rhein-, Main- und Taunusgebiet II	000236	Wiesbaden	\N	\N	\N		\N	\N	\N	2	\N	Y			\N		\N	article	2016-05-04 11:05:28.253	\N	1984	\N	669	f
677	Lutz_1950_000239	\N	\N	\N	\N	\N	Über den Gesellschaftsanschluß oberpfälzischer Kiefernstandorte	000239	München	\N	\N	\N		\N	\N	\N	58	\N	Y			\N		\N	article	2016-05-04 11:05:28.947	\N	1950	\N	678	f
680	Manz_1987_000240	\N	\N	\N	\N	\N	Grünlandgesellschaften im Oberen Nahe-Bergland	000240	Bad Dürkheim	\N	MZE	\N		\N	\N	\N	195	\N	Y			\N		\N	article	2016-05-04 11:05:29.205	\N	1987	\N	681	f
682	Manz_1989_000241	\N	\N	\N	\N	\N	Grünlandgesellschaften magerer Standorte des südwestlichen Hunsrückvorlandes im Raum Birkenfeld	000241	Oppenheim	\N	\N	\N		\N	\N	\N	32	\N	Y			\N		\N	article	2016-05-04 11:05:29.46	\N	1989	\N	683	f
6115	Muche2012	\N	10.7809/b-e.00093	\N	\N	\N	Namib Desert Region Vegetation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.651	\N	2012	\N	6116	f
688	Menke_1964_000243	\N	\N	\N	\N	\N	Das Huntloser Torfmoor. Vegetationskundliche Studie über ein oldenburgisches Naturschutzgebiet	000243		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:29.932	\N	1964	\N	689	f
691	Meyer_1957_000244	\N	\N	\N	\N	\N	Über Wasser- und Stickstoffhaushalt der Röhrichte und Wiesen im Elballuvium bei Hamburg	000244		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:30.146	\N	1957	\N	692	f
711	Niemann_1964_000252	\N	\N	\N	\N	\N	Beiträge zur Vegetation und Standortgeographie in einem Gebirgsquerschnitt über dem mittleren Thüringer Wald	000252	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:32.135	\N	1964	\N	712	f
714	Nigge_1988_000253	\N	\N	\N	\N	\N	Nährstoffarme Feuchtgebiete im Südwesten der Westfälischen Bucht. Vegetation und Naturschutzsituation	000253	Münster	\N	\N	\N		\N	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:05:32.373	\N	1988	\N	715	f
716	Nowak_1983_000254	\N	\N	\N	\N	\N	Beobachtungen zur Soziologie und Ökologie von Juncus filiformis L. in Hessen	000254	Göttingen	\N	\N	\N		\N	\N	\N	46	\N	Y			\N		\N	article	2016-05-04 11:05:32.586	\N	1983	\N	717	f
718	Nowak_1985_000255	\N	\N	\N	\N	\N	Die Schachblumenwiesen im bayerisch-hessischen Sinntal	000255	Regensburg	\N	\N	\N		\N	\N	\N	46	\N	Y			\N		\N	article	2016-05-04 11:05:32.8	\N	1985	\N	719	f
720	Nowak_1992_000256	\N	\N	\N	\N	\N	Beiträge zur Kenntnis der Vegetation des Gladenbacher Berglands. II. Die Wiesengesellschaften der Klasse Molinio-Arrhenatheretea	000256	Frankfurt am Main	\N	\N	\N		\N	\N	\N	201	\N	Y			\N		\N	article	2016-05-04 11:05:33.056	\N	1992	\N	721	f
725	Nowak_Wedra_1985_000258	\N	\N	\N	\N	\N	Die Vegetation einer bemerkenswerten Wiesenfläche im Gladenbacher Bergland	000258	Darmstadt	\N	\N	\N		\N	\N	\N	20	\N	Y			\N		\N	article	2016-05-04 11:05:33.564	\N	1985	\N	726	f
727	Nowak_Wedra_1988_000259	\N	\N	\N	\N	\N	Die Vegetation. wahrscheinlich aus: Beiträge zur Kenntnis der Vegetation des Gladenbacher Berglands. I. Die Ackerunkrautgesellschaften	000259		\N	\N	\N		\N	\N	\N	5	\N	Y			\N		\N	article	2016-05-04 11:05:33.827	\N	1988	\N	728	f
730	Oberdorfer_1938_000260	\N	\N	\N	\N	\N	Ein Beitrag zur Vegetationskunde des Nordschwarzwaldes. Erläuterungen der vegetationskundlichen Karte Bühlertal-Herrenwies	000260	Karlsruhe	\N	\N	\N		\N	\N	\N	11	\N	Y			\N		\N	article	2016-05-04 11:05:34.051	\N	1938	\N	731	f
732	Oberdorfer_1950_000261	\N	\N	\N	\N	\N	Beitrag zur Vegetationskunde des Allgäu	000261	Karlsruhe	\N	\N	\N		\N	\N	\N	29	\N	Y			\N		\N	article	2016-05-04 11:05:34.32	\N	1950	\N	733	f
734	Oberdorfer_1952_000262	\N	\N	\N	\N	\N	Die Wiesen des Oberrheingebietes	000262	Karlsruhe	\N	\N	\N		\N	\N	\N	9	\N	Y			\N		\N	article	2016-05-04 11:05:34.539	\N	1952	\N	735	f
6119	Hofmann2012	\N	10.7809/b-e.00130	\N	\N	\N	National Inventory of Swiss Bryophytes (NISM)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.662	\N	2012	\N	6120	f
738	Oberdorfer_1982_000264	\N	\N	\N	\N	\N	Erläuterungen zur vegetationskundlichen Karte Feldberg 1:25 000	000264	Karlsruhe	\N	\N	\N		\N	\N	\N	2	\N	Y			\N		\N	article	2016-05-04 11:05:35.025	\N	1982	\N	739	f
741	Oppermann_1992_000265	\N	\N	\N	\N	\N	Das Ressourcenangebot verschiedener Grünland-Gesellschaften und dessen Nutzung durch Brutvögel	000265	Berlin, Stuttgart	\N	\N	\N		\N	\N	\N	12	\N	Y			\N		\N	article	2016-05-04 11:05:35.282	\N	1992	\N	742	f
6123	Strohbach2012	\N	10.7809/b-e.00095	\N	\N	\N	National Phytosociological Database of Namibia	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.682	\N	2012	\N	6124	f
751	Passarge_1955_000268	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der Wiesenlandschaft des Lübbenauer Spreewaldes	000268	Berlin	\N	\N	\N		\N	\N	\N	36	\N	Y			\N		\N	article	2016-05-04 11:05:35.977	\N	1955	\N	752	f
753	Passarge_1959_000269	\N	\N	\N	\N	\N	Pflanzengesellschaften zwischen Trebel, Grenz-Bach und Peene (O-Mecklenburg)	000269	Berlin	\N	\N	\N		\N	\N	\N	97	\N	Y			\N		\N	article	2016-05-04 11:05:36.291	\N	1959	\N	754	f
755	Passarge_1959_000270	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen in der Wiesenlandschaft des nördlichen Havellandes	000270	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:36.567	\N	1959	\N	756	f
757	Passarge_1962_000271	\N	\N	\N	\N	\N	Über Pflanzengesellschaften im nordwestlichen Mecklenburg	000271	Rostock	\N	\N	\N		\N	\N	\N	128	113	Y			\N		\N	article	2016-05-04 11:05:36.803	\N	1962	\N	758	f
671	Luepnitz_1967_000237	\N	\N	\N	\N	\N	Bemerkenswerte Pflanzengesellschaften am Ginsheimer Altrhein	000237	Mainz	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:28.464	\N	1967	\N	672	f
6128	Powrie2012	\N	10.7809/b-e.00096	\N	\N	\N	National Vegetation Database of South Africa	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.701	\N	2012	\N	6129	f
763	Peppler_1987_000274	\N	\N	\N	\N	\N	Nardetalia-Gesellschaften im Werra-Meißner-Gebiet	000274	Göttingen	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:37.591	\N	1987	\N	764	f
768	Petrak_1992_000276	\N	\N	\N	\N	\N	Vegetationsanalyse und historischer Rückblick zur Entwicklung von Schutzzielen im Naturschutzgebiet "Perlenbach-Fuhrtsbachtal" (Nordwesteifel)	000276	Bonn	\N	\N	\N		\N	\N	\N	11	\N	Y			\N		\N	article	2016-05-04 11:05:38.071	\N	1992	\N	769	f
6133	Vanderhorst2012	\N	10.7809/b-e.00227	\N	\N	\N	Natural Heritage Vegetation Database for West Virginia	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.719	\N	2012	\N	6134	f
6145	Nielsen2012	\N	10.7809/b-e.00165	\N	\N	\N	NATURDATA.DK  Danish monitoring program of vegetation and chemical plant and soil data from non-forested terrestrial habitat types	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.729	\N	2012	\N	6146	f
6150	Broadbent2012	\N	10.7809/b-e.00113	\N	\N	\N	New Zealand National Vegetation Databank	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.757	\N	2012	\N	6151	f
780	Pfeiffer_1961_000280	\N	\N	\N	\N	\N	Vegetationskundliche Beobachtungen am Ruger Slatt und Blanken Slatt	000280	Horn	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:39.092	\N	1961	\N	781	f
6153	Jansen2012#186	\N	10.7809/b-e.00061	\N	\N	\N	News from the Global Index of Vegetation-Plot Databases (GIVD): the metadata platform, available data, and their properties	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.767	\N	2012	\N	6154	f
6155	Szabó2012	\N	10.7809/b-e.00202	\N	\N	\N	Oak-Hornbeam Forest Database of the Transylvanian Basin	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.784	\N	2012	\N	6156	f
6157	Glöckler2012	\N	10.7809/b-e.00063	\N	\N	\N	Overview of the GIVD-registered databases	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.789	\N	2012	\N	6158	f
6160	Vittoz2012	\N	10.7809/b-e.00128	\N	\N	\N	Permanent.Plot.ch  a database for Swiss permanent vegetation plots	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.793	\N	2012	\N	6161	f
6163	Roulier2012#231	\N	10.7809/b-e.00133	\N	\N	\N	Phytobase C2005  vegetation database of Swiss floodplains (Braun-Blanquet plots)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.798	\N	2012	\N	6164	f
786	Philippi_1960_000282	\N	\N	\N	\N	\N	Zur Gliederung der Pfeifengraswiesen im südlichen und mittleren Oberrheingebiet	000282	Karlsruhe	\N	\N	\N		\N	\N	\N	1	\N	Y			\N		\N	article	2016-05-04 11:05:39.646	\N	1960	\N	787	f
6165	Roulier2012	\N	10.7809/b-e.00134	\N	\N	\N	Phytobase S2008  vegetation database of Swiss floodplains (synusial plots)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.804	\N	2012	\N	6166	f
6168	Stancic2012	\N	10.7809/b-e.00180	\N	\N	\N	Phytosociological Database of Non-Forest Vegetation in Croatia	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.808	\N	2012	\N	6169	f
794	Philippi_1989_000286	\N	\N	\N	\N	\N	Die Flache Quellbinse (Blysmus compressus) im Südschwarzwald und angrenzenden Gebieten	000286	Karlsruhe	\N	\N	\N		\N	\N	\N	23	\N	Y			\N		\N	article	2016-05-04 11:05:40.605	\N	1989	\N	795	f
799	Pietsch_1968_000288	\N	\N	\N	\N	\N	Die Verlandungsvegetation des Sorgenteiches bei Ruhland in der Oberlausitzer Niederung und ihre pflanzengeographische Bedeutung	000288	Dresden	\N	\N	\N		\N	\N	\N	39	\N	Y			\N		\N	article	2016-05-04 11:05:41.118	\N	1968	\N	800	f
801	Pietsch_1983_000289	\N	\N	\N	\N	\N	Vegetationsverhältnisse im NSG "Jeggauer Moor" - Teil 2	000289	Dessau	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:41.38	\N	1983	\N	802	f
803	Pietsch_1986_000290	\N	\N	\N	\N	\N	Vegetationsverhältnisse im NSG "Möster Birken"	000290	Dessau	\N	\N	\N		\N	\N	\N	9	\N	Y			\N		\N	article	2016-05-04 11:05:41.645	\N	1986	\N	804	f
809	Pollok_1991_000292	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen im Naturschutzgebiet "Duvenstedter Broo"k	000292	Hamburg	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:42.156	\N	1991	\N	810	f
6171	Tanaka2012	\N	10.7809/b-e.00104	\N	\N	\N	Phytosociological Relevé Database of Japan (PRDB)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.813	\N	2012	\N	6172	f
814	Raabe_1980_000294	\N	\N	\N	\N	\N	Der Wandel der Pflanzenwelt unserer Kalk-Quell-Moore	000294	Neumünster	\N	\N	\N		\N	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:05:42.67	\N	1980	\N	815	f
817	Reichert_1975_000295	\N	\N	\N	\N	\N	Die Quellmoore (Brücher) des südwestlichen Hunsrücks	000295	Oppenheim	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:42.882	\N	1975	\N	818	f
820	Reidl_1986_000296	\N	\N	\N	\N	\N	Zur Schutzwürdigkeit von Vegetation und Flora des Kamptales in Essen-Schönebeck	000296	Bonn	\N	\N	\N		\N	\N	\N	2	\N	Y			\N		\N	article	2016-05-04 11:05:43.103	\N	1986	\N	821	f
823	Reif_1989_000297	\N	\N	\N	\N	\N	Die Grünlandvegetation im Weihersgrund, einem Wiesental des Spessart	000297	Würzburg	\N	\N	\N		\N	\N	\N	151	\N	Y			\N		\N	article	2016-05-04 11:05:43.317	\N	1989	\N	824	f
827	Reif_Baumgartl_Breitenbach_1989_000298	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Grünlandes zwischen Mauth und Fürsterau (Hinterer Bayerische Wald) und die Geschichte ihrer Entstehung	000298	Regensburg	\N	\N	\N		\N	\N	\N	77	\N	Y			\N		\N	article	2016-05-04 11:05:43.577	\N	1989	\N	828	f
830	Ribbe_1976_000299	\N	\N	\N	\N	\N	Die Vegetationsverhältnisse im Wirtschaftsgrünland der Lewitz	000299	Rostock	\N	\N	\N		\N	\N	\N	50	\N	Y			\N		\N	article	2016-05-04 11:05:43.804	\N	1976	\N	831	f
833	Richter_1962_000300	\N	\N	\N	\N	\N	Versuche zur Bekämpfung des Wasserkreuzkrautes (Senecio aquaticus Huds.) auf nordwestdeutsche Weiden	000300	Berlin, Hamburg	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:44.106	\N	1962	\N	834	f
835	Richter_1974_000301	\N	\N	\N	\N	\N	Die Vegetationsdynamik der Talsperre Spremberg	000301	Leipzig	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:44.368	\N	1974	\N	836	f
838	Rochow_1951_000302	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Kaiserstuhls	000302	Jena	\N	\N	\N		\N	\N	\N	1	\N	Y			\N		\N	article	2016-05-04 11:05:44.584	\N	1951	\N	839	f
841	Rodi_1961_000303	\N	\N	\N	\N	\N	Die Vegetations- und Standortsgliederung im Einzugsgebiet der Lein (Kreis Schwäbisch Gmünd)	000303	Ludwigsburg	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:44.795	\N	1961	\N	842	f
843	Rodi_1961_000304	\N	\N	\N	\N	\N	Zwei neue Naturdenkmale bei Welzheim	000304	Ludwigsburg	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:45.031	\N	1961	\N	844	f
845	Rodi_1963_000305	\N	\N	\N	\N	\N	Die Streuwiesen- und Verlandungsgesellschaften des Welzheimer Waldes	000305	Ludwigsburg	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:45.256	\N	1963	\N	846	f
851	Rosskopf_1971_000307	\N	\N	\N	\N	\N	Pflanzengesellschaften der Talmoore an der Schwarzen und Weißen Laber im Oberpfälzer Jura	000307	Regensburg	\N	\N	\N		\N	\N	\N	112	\N	Y			\N		\N	article	2016-05-04 11:05:45.851	\N	1971	\N	852	f
806	Poetsch_1962_000291	\N	\N	\N	\N	\N	Die Grünland-Gesellschaften des Fiener Bruchs in West-Brandenburg	000291	Potsdam	\N	\N	\N		\N	\N	\N	116	\N	Y			\N		\N	article	2016-05-04 11:05:41.899	\N	1962	\N	807	f
856	Runge_1991_000309	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Naturschutzgebietes "Heiliges Meer" und ihre Änderungen in den letzten 90 Jahren	000309	Münster	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:46.276	\N	1991	\N	857	f
860	Wichmann_Burkart_2000_000310	\N	\N	\N	\N	\N	Die Vegetationszonierung des Grünlandes am Südufer des Gülper See	000310		\N	\N	\N		Wichmann_Burkart_2000	\N	\N	38	\N	Y			\N		\N	article	2016-05-04 11:05:46.546	\N	2000	\N	861	f
862	Ruthsatz_1985_000311	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Grünlandes im Raum Ingolstadt und ihre Verarmung durch die sich wandelnde landwirtschaftliche Nutzung	000311	Göttingen	\N	\N	\N		\N	\N	\N	174	\N	Y			\N		\N	article	2016-05-04 11:05:46.769	\N	1985	\N	863	f
853	Ruehl_1954_000308	\N	\N	\N	\N	\N	Das südliche Leinebergland	000308	Jena	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:46.069	\N	1954	\N	854	f
867	Scheel_1962_000313	\N	\N	\N	\N	\N	Moor- und Grünlandgesellschaften im oberen Briesetal nördlich von Berlin	000313	Potsdam	\N	\N	\N		\N	\N	\N	55	\N	Y			\N		\N	article	2016-05-04 11:05:47.241	\N	1962	\N	868	f
876	Schumacher_1933_000316	\N	\N	\N	\N	\N	Die Fadenbinse im Holpetal	000316	Gummersbach, Waldbröl	\N	\N	\N		\N	\N	\N	12	\N	Y			\N		\N	article	2016-05-04 11:05:48.017	\N	1933	\N	877	f
878	Schumacher_1934_000317	\N	\N	\N	\N	\N	Der Sodbruch bei Niederbraunfeld	000317	Gummersbach, Waldbröl	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:48.351	\N	1934	\N	879	f
880	Schumacher_1934_000318	\N	\N	\N	\N	\N	Die Sumpfwiese am "Froschpuhl" bei Au	000318	Gummersbach, Waldbröl	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:48.616	\N	1934	\N	881	f
882	Schumacher_1936_000319	\N	\N	\N	\N	\N	Die Wiesen im Gebiet der oberen Wiehl	000319	Gummersbach, Waldbröl	\N	\N	\N		\N	\N	\N	27	\N	Y			\N		\N	article	2016-05-04 11:05:48.867	\N	1936	\N	883	f
884	Schumacher_1937_000320	\N	\N	\N	\N	\N	Floristisch-soziologische Beobachtungen in Hochmooren des südlichen Schwarzwaldes	000320	Karlsruhe	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:49.081	\N	1937	\N	885	f
887	Schumacher_1977_000321	\N	\N	\N	\N	\N	Flora und Vegetation der Sötenicher Kalkmulde (Eifel)	000321	Bonn	\N	SMW	\N		\N	\N	\N	134	\N	Y			\N		\N	article	2016-05-04 11:05:49.332	\N	1977	\N	888	f
6175	Kacki2012	\N	10.7809/b-e.00199	\N	\N	\N	Polish Vegetation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.819	\N	2012	\N	6176	f
6182	Jünger2012	\N	10.7809/b-e.00154	\N	\N	\N	Post-Mining Vegetation Database Eastern Germany	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.827	\N	2012	\N	6183	f
1931	Speidel_1970_000701	\N	\N	\N	\N	\N	Grünlandgesellschaften im Hoch-Solling	000701	Bonn-Bad Godesberg	\N	\N	\N		nicht gefunden	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:22.723	\N	1970	\N	1932	f
6192	Hero2012	\N	10.7809/b-e.00112	\N	\N	\N	PPBio Australasia Long Term Ecological Research Sites  flora and fauna database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.842	\N	2012	\N	6193	f
6195	Bach2012	\N	10.7809/b-e.00229	\N	\N	\N	Project Database of Bolivian Ecoregions	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.869	\N	2012	\N	6196	f
6197	Csecserits2012#140	\N	10.7809/b-e.00181	\N	\N	\N	Regional Vegetation Database of Kiskunság	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.875	\N	2012	\N	6198	f
6201	Boyle2012	\N	10.7809/b-e.00086	\N	\N	\N	SALVIAS  the SALVIAS vegetation inventory database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.881	\N	2012	\N	6202	f
901	Schwickerath_1944_000326	\N	\N	\N	\N	\N	Das Hohe Venn und seine Randgebiete.- Vegetation, Boden und Landschaft	000326	Jena	\N	\N	\N		\N	\N	\N	287	\N	Y			\N		\N	article	2016-05-04 11:05:50.545	\N	1944	\N	902	f
903	Schwickerath_1945_000327	\N	\N	\N	\N	\N	Kritische Erörterungen zu dem von R. Tüxen aufgestellten Calthion	000327		\N	\N	\N		\N	\N	\N	13	\N	Y			\N		\N	article	2016-05-04 11:05:50.84	\N	1945	\N	904	f
905	Schwickerath_1975_000328	\N	\N	\N	\N	\N	Hohes Venn, Zitterwald, Schneifel und Hunsrück - Ein florengeographischer, vegetationskundlicher, bodenkundlicher und kartographischer Vergleich	000328	Oppenheim	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:51.057	\N	1975	\N	906	f
908	Schwickert_1992_000329	\N	\N	\N	\N	\N	Vegetationsgeographische Untersuchungen im Hohen Westerwald unter besonderer Berücksichtigung der Pflanzengesellschaften des montanen Grünlandes	000329	Landau	\N	\N	\N		TAB	\N	\N	84	\N	Y			\N		\N	article	2016-05-04 11:05:51.267	\N	1992	\N	909	f
6204	McClaran2012	\N	10.7809/b-e.00222	\N	\N	\N	Santa Rita Experimental Range Long Term Transect Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.889	\N	2012	\N	6205	f
916	Seibert_1962_000332	\N	\N	\N	\N	\N	Die Auenvegetation an der Isar nördlich von München und ihre Beeinflussung durch den Menschen	000332	München	\N	\N	\N		\N	\N	\N	239	\N	Y			\N		\N	article	2016-05-04 11:05:52.095	\N	1962	\N	917	f
918	Seifert_1994_000333	\N	\N	\N	\N	\N	Biozönologische Untersuchungen an tagaktiven Schmetterlingen in Nordosthessen	000333	Göttingen	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:52.368	\N	1994	\N	919	f
921	Siede_1960_000334	\N	\N	\N	\N	\N	Untersuchungen über die Pflanzengesellschaften im Flyschgebiet Oberbayerns	000334	München	\N	\N	\N		\N	\N	\N	94	\N	Y			\N		\N	article	2016-05-04 11:05:52.579	\N	1960	\N	922	f
925	Smollich_Bernert_1986_000335	\N	\N	\N	\N	\N	Beiträge zur Vegetation des östlichen Hunsrück (MTB 5911 Kisselbach)	000335	Mainz	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:52.793	\N	1986	\N	926	f
6206	Bergmeier2012	\N	10.7809/b-e.00176	\N	\N	\N	Segetal Weed Vegetation Database of Greece	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.895	\N	2012	\N	6207	f
931	Speidel_1962_000337	\N	\N	\N	\N	\N	Die Grünlandverhältnisse des Landschaftsschutzgebietes "Hoher Vogelsberg"	000337	Stuttgart	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:53.282	\N	1962	\N	932	f
934	Speier_1994_000338	\N	\N	\N	\N	\N	Vegetationskundliche und paläoökologische Untersuchungen zur Rekonstruktion prähistorischer und historischer Landnutzungen im südlichen Rothaargebirge	000338	Münster	\N	\N	\N		\N	\N	\N	24	\N	Y			\N		\N	article	2016-05-04 11:05:53.538	\N	1994	\N	935	f
937	Springer_1987_000339	\N	\N	\N	\N	\N	Pflanzengesellschaften im außeralpinen Teil des Kreises Berchtesgadener Land	000339	München	\N	\N	\N		\N	\N	\N	25	\N	Y			\N		\N	article	2016-05-04 11:05:53.784	\N	1987	\N	938	f
943	Sukopp_1959_000341	\N	\N	\N	\N	\N	Vergleichende Untersuchungen der Vegetation Berliner Moore unter besonderer Berücksichtigung der anthropogenen Veränderungen	000341	Stuttgart	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:54.201	\N	1959	\N	944	f
946	Trentepohl_1965_000342	\N	\N	\N	\N	\N	Die Vegetation schutzwürdiger Wiesen im Staatsforst Kranichstein ostwärts Darmstadt	000342		\N	\N	\N		\N	\N	\N	63	\N	Y			\N		\N	article	2016-05-04 11:05:54.452	\N	1965	\N	947	f
873	Schuechen_1972_000315	\N	\N	\N	\N	\N	Zur Ökologie der Quellen und Quellfluren im Einzugsgebiet der Schiltach (Mittelschwarzwald)	000315	Donaueschingen	\N	\N	\N		\N	\N	\N	70	\N	Y			\N		\N	article	2016-05-04 11:05:47.724	\N	1972	\N	874	f
951	Tuexen_1947_000344	\N	\N	\N	\N	\N	Der Pflanzensoziologische Garten in Hannover und seine bisherige Entwicklung	000344	Hannover	\N	\N	\N		\N	\N	\N	30	\N	Y			\N		\N	article	2016-05-04 11:05:54.964	\N	1947	\N	952	f
970	Urban_1991_000352	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Klammspitzkammes im NSG "Ammergebirge"	000352	München	\N	\N	\N		\N	\N	\N	252	\N	Y			\N		\N	article	2016-05-04 11:05:56.974	\N	1991	\N	971	f
967	Ullmann_Foerst_1980_000351	\N	\N	\N	\N	\N	Pflanzengesellschaften des NSG "Gangolfsberg" (Südliche Rhön) und seiner Randgebiete	000351	Göttingen	\N	\N	\N		\N	\N	\N	16	\N	Y			\N		\N	article	2016-05-04 11:05:56.762	\N	1980	\N	968	f
978	Vollmar_1947_000355	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Murnauer Moores, Teil 1	000355	München	\N	\N	\N		\N	\N	\N	20	\N	Y			\N		\N	article	2016-05-04 11:05:57.655	\N	1947	\N	979	f
983	Vollrath_1967_000357	\N	\N	\N	\N	\N	Lungenenzian (Gentiana pneumonanthe) und Färberscharte (Serratula tinctoria) im Oberpfälzer Wald	000357	Regensburg	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:58.077	\N	1967	\N	984	f
986	Walentowski_1991_000358	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der Rodungsinsel Bischofsreut im hinteren Bayerischen Wald (800 bis 1050m ü. NN.)	000358	München	\N	\N	\N		\N	\N	\N	35	\N	Y			\N		\N	article	2016-05-04 11:05:58.325	\N	1991	\N	987	f
989	Walther_1950_000359	\N	\N	\N	\N	\N	Die Pflanzengesellschaften im Grundwasserschadengebiet Lathen -Dörpen am Dortmund-Ems-Seitenkanal	000359	Weimar	\N	\N	\N		\N	\N	\N	56	\N	Y			\N		\N	article	2016-05-04 11:05:58.576	\N	1950	\N	990	f
991	Walther_1977_000360	\N	\N	\N	\N	\N	Die Vegetation der Gemeindeweide Fuhlkarren bei Meetschow (Kr. Lüchow-Dannenberg)	000360	Göttingen	\N	\N	\N		\N	\N	\N	23	\N	Y			\N		\N	article	2016-05-04 11:05:58.792	\N	1977	\N	992	f
993	Walther_1977_000361	\N	\N	\N	\N	\N	Die Vegetation des Elbtales. Die Flußniederung von Elbe und Seege bei Gartow (Kr. Lüchow-Dannenberg)	000361	Hamburg	\N	\N	\N		\N	\N	\N	18	\N	Y			\N		\N	article	2016-05-04 11:05:59.054	\N	1977	\N	994	f
995	Walther_1983_000362	\N	\N	\N	\N	\N	Bemerkenswerte Pflanzengesellschaften um Gorleben (Kreis Lüchow-Dannenberg)	000362	Hamburg	\N	\N	\N		\N	\N	\N	11	\N	Y			\N		\N	article	2016-05-04 11:05:59.262	\N	1983	\N	996	f
999	Walther_1987_000364	\N	\N	\N	\N	\N	Die natürliche und naturnahe Vegetation der Landschaften um Gorleben (Kreis Lüchow-Dannenberg, Niedersachsen) und ihre Gefährdung	000364	Göttingen	\N	\N	\N		\N	\N	\N	5	\N	Y			\N		\N	article	2016-05-04 11:05:59.691	\N	1987	\N	1000	f
1002	Warneke_1993_000365	\N	\N	\N	\N	\N	Die Flora und Vegetation des Naturschutzgebietes "Sippenauer Moor" im Landkreis Kelheim	000365	Regensburg	\N	\N	\N		\N	\N	\N	10	\N	Y			\N		\N	article	2016-05-04 11:05:59.905	\N	1993	\N	1003	f
1005	Weber_1976_000366	\N	\N	\N	\N	\N	Neue Ergebnisse zur Verbreitung und Soziologie von Juncus subnodulosus Schrank in West-Niedersachsen	000366	Oldenburg	\N	\N	\N		\N	\N	\N	11	\N	Y			\N		\N	article	2016-05-04 11:06:00.124	\N	1976	\N	1006	f
1007	Weber_1978_000367	\N	\N	\N	\N	\N	Vegetation des Naturschutzgebiets "Balksee und Randmoore" (Kreis Cuxhaven)	000367	Hannover	\N	\N	\N		\N	\N	\N	147	\N	Y			\N		\N	article	2016-05-04 11:06:00.375	\N	1978	\N	1008	f
1009	Weber_1983_000368	\N	\N	\N	\N	\N	Vegetation der Haaren-Niederung am Westrande der Stadt Oldenburg - Ein Beitrag zur Problematik brachgefallener Feuchtwiesen	000368	Oldenburg	\N	\N	\N		\N	\N	\N	54	\N	Y			\N		\N	article	2016-05-04 11:06:00.632	\N	1983	\N	1010	f
6208	Rusina2012	\N	10.7809/b-e.00197	\N	\N	\N	Semi-natural Grassland Vegetation Database of Latvia	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.9	\N	2012	\N	6209	f
6211	ibík2012	\N	10.7809/b-e.00216	\N	\N	\N	Slovak Vegetation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.905	\N	2012	\N	6212	f
6213	De_Sanctis2012#146	\N	10.7809/b-e.00111	\N	\N	\N	Socotra Vegetation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.911	\N	2012	\N	6214	f
6216	Alvarez2012#112	\N	10.7809/b-e.00092	\N	\N	\N	SWEA-Dataveg  vegetation of small wetlands in East Africa	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.916	\N	2012	\N	6217	f
6219	Stalling2012	\N	10.7809/b-e.00135	\N	\N	\N	Swiss Biodiversity Monitoring BDM (Z9 Mosses)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.94	\N	2012	\N	6220	f
6221	Stalling2012#244	\N	10.7809/b-e.00129	\N	\N	\N	Swiss Biodiversity Monitoring BDM (Z9 Plants)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.95	\N	2012	\N	6222	f
1017	Welss_1983_000371	\N	\N	\N	\N	\N	Cirsium canum (L.) All. in Bayern. - Erlanger Beiträge zur Flora Frankens 4. Folge	000371	München	\N	\N	\N		\N	\N	\N	5	\N	Y			\N		\N	article	2016-05-04 11:06:01.362	\N	1983	\N	1018	f
1020	Westhus_Niemann_1990_000372	\N	\N	\N	\N	\N	Veränderungen in der Wiesenvegetation des unteren Vessertales (Biosphärenreservat "Vessertal", Bez. Suhl, DDR)	000372	Berlin	\N	\N	\N		\N	\N	\N	19	\N	Y			\N		\N	article	2016-05-04 11:06:01.655	\N	1990	\N	1021	f
6224	Wohlgemuth2012	\N	10.7809/b-e.00131	\N	\N	\N	Swiss Forest Vegetation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.957	\N	2012	\N	6225	f
1026	Wiegleb_1977_000374	\N	\N	\N	\N	\N	Die Wasser- und Sumpfpflanzengesellschaften der Teiche in den Naturschutzgebieten "Priorteich-Sachsenstein" und "Itelteich" bei Walkenried im Harz	000374	Göttingen	\N	\N	\N		\N	\N	\N	100	\N	Y			\N		\N	article	2016-05-04 11:06:02.154	\N	1977	\N	1027	f
1028	Wiegleb_1979_000375	\N	\N	\N	\N	\N	Vegetation und Umweltbedingungen der Oberharzer Stauteiche heute und in Zukunft	000375	Hannover	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:02.381	\N	1979	\N	1029	f
1031	Winterhoff_1993_000376	\N	\N	\N	\N	\N	Die Pflanzenwelt des NSG "Eriskircher Ried" am Bodensee	000376	Karlsruhe	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:02.592	\N	1993	\N	1032	f
1034	Wittig_1987_000377	\N	\N	\N	\N	\N	Eine Quellmoorgesellschaft mit Carex pulicaris bei Verden	000377	Bremen	\N	\N	\N		\N	\N	\N	4	\N	Y			\N		\N	article	2016-05-04 11:06:02.802	\N	1987	\N	1035	f
1036	Wittig_1980_000378	\N	\N	\N	\N	\N	Die geschützten Moore und oligotrophen Gewässer der Westfälischen Bucht	000378	Münster	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:03.1	\N	1980	\N	1037	f
1039	Wolf_1979_000379	\N	\N	\N	\N	\N	Veränderungen der Vegetation und Abbau der organischen Substanz in aufgegebenen Wiesen des Westerwaldes	000379	Bonn-Bad Godesberg	\N	\N	\N		\N	\N	\N	352	\N	Y			\N		\N	article	2016-05-04 11:06:03.361	\N	1979	\N	1040	f
1041	Wollert_1981_000380	\N	\N	\N	\N	\N	Ergebnisse der floristischen Kartierung einiger Bach- und Flußtäler des Kreises Teterow (Mecklenburg). V. Die Wiesenniederung zwischen Klein Bützin und Striesenow	000380		\N	\N	\N		\N	\N	\N	11	\N	Y			\N		\N	article	2016-05-04 11:06:03.637	\N	1981	\N	1042	f
1044	Wollert_Haberkost_1981_000381	\N	\N	\N	\N	\N	Ergebnisse der floristischen Kartierung einiger Bach- und Flußtäler des Kreises Teterow (Mecklenburg). VI. Das Durchbruchstal der Ostpeene zwischen Karlsruhe und Peenhäuser	000381	Neubrandenburg	\N	\N	\N		\N	\N	\N	25	\N	Y			\N		\N	article	2016-05-04 11:06:03.954	\N	1981	\N	1045	f
1048	Zacharias_Janssen_Brandes_1988_000382	\N	\N	\N	\N	\N	Basenreiche Pfeifengras-Streuewiesen des Molinietum caeruleae W.Koch 1926, ihre Brachestadien und ihre wichtigsten Kontaktgesellschaften in Südost-Niedersachsen	000382	Göttingen	\N	\N	\N		\N	\N	\N	41	\N	Y			\N		\N	article	2016-05-04 11:06:04.191	\N	1988	\N	1049	f
1051	Zahlheimer_1979_000383	\N	\N	\N	\N	\N	Vegetationsstudien in den Donauauen zwischen Regensburg und Straubing als Grundlage für den Naturschutz	000383	Regensburg	\N	\N	\N		\N	\N	\N	519	\N	Y			\N		\N	article	2016-05-04 11:06:04.441	\N	1979	\N	1052	f
6233	Lenoir2012	\N	10.7809/b-e.00124	\N	\N	\N	The Alps Vegetation Database  a geo-referenced community-level archive of all terrestrial plants occurring in the Alps	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:49.969	\N	2012	\N	6234	f
2006	Krausch_1967_000730	\N	\N	\N	\N	\N	Phyteuma orbiculare L. in Brandenburg	000730	Berlin-Dahlem	\N	\N	\N		\N	\N	\N	9	\N	Y			\N		\N	article	2016-05-04 11:07:28.914	\N	1967	\N	2007	f
973	Verbuecheln_1986_000353	\N	\N	\N	\N	\N	Zum Vorkommen eines Junco-Molinietum im nördlichen Münsterland	000353	Münster	\N	\N	\N		\N	\N	\N	7	\N	Y			\N		\N	article	2016-05-04 11:05:57.225	\N	1986	\N	974	f
6280	Pezzini2012	\N	10.7809/b-e.00083	\N	\N	\N	The Brazilian Program for Biodiversity Research (PPBio) Information System	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.019	\N	2012	\N	6281	f
1074	Barkman_1977_000388	\N	\N	\N	\N	\N	Die Erforschung des Mikroklimas in der Vegetation. Theoretische und methodische Aspekte	000388	Vaduz	\N	BAR	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:05.669	\N	1977	\N	1075	f
1076	Barkman_1989_000389	\N	\N	\N	\N	\N	Fidelity and character-species, a critical evaluation	000389		\N	BAR	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:05.92	\N	1989	\N	1077	f
6284	Schaminée2012	\N	10.7809/b-e.00077	\N	\N	\N	The Dutch National Vegetation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.327	\N	2012	\N	6285	f
1084	Bartsch_Wegener_Wesarg_1976_000391	\N	\N	\N	\N	\N	Der Weinberg im NSG "Vorberg-Huy"	000391		\N	BWW	\N		TABGESNE	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:06:06.493	\N	1976	\N	1085	f
1087	Baumgart_1990_000392	\N	\N	\N	\N	\N	Halbtrocken- und Blaugras-Rasen. Festuco-Brometea Braun-Blanquet & Tüxen 1943	000392	Frankfurt am Main	\N	BAU	\N		TABGESNE	\N	\N	40	\N	Y			\N		\N	article	2016-05-04 11:06:06.707	\N	1990	\N	1088	f
1091	Becker_1996_000394	\N	\N	\N	\N	\N	Magerrasen-Gesellschaften auf Zechstein am südlichen Harzrand (Thüringen)	000394	Göttingen	\N	BEC	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:07.247	\N	1996	\N	1092	f
1094	Becker_1998_000395	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der Felsfluren und Magerrasen im unteren Unstruttal (Sachsen-Anhalt)	000395	Göttingen	\N	BEK	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:07.56	\N	1998	\N	1095	f
1096	Becker_1998_000396	\N	\N	\N	\N	\N	Zur Rolle von Mikroklima- und Bodenparametern bei Vegetationsabfolgen in Trockenrasen des unteren Unstruttals (Sachsen-Anhalt)	000396	Berlin	\N	BEK	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:07.83	\N	1998	\N	1097	f
6294	Dengler2012#150	\N	10.7809/b-e.00056	\N	\N	\N	The need for and the requirements of EuroSL, an electronic taxonomic reference list of all Euro-pean plants	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.348	\N	2012	\N	6295	f
6300	Garbolino2012	\N	10.7809/b-e.00074	\N	\N	\N	The phytosociological database SOPHY as the basis of plant socio-ecology and phytoclimatology in France	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.399	\N	2012	\N	6301	f
1117	Bornkamm_1961_000404	\N	\N	\N	\N	\N	Vegetation und Vegetations-Entwicklung auf Kiesdächern	000404		\N	BOK	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:09.88	\N	1961	\N	1118	f
1119	Bornkamm_1960_000405	\N	\N	\N	\N	\N	Die Trespen-Halbtrockenrasen im oberen Leinegebiet	000405		\N	BOK	\N		TABGESNE	\N	\N	139	\N	Y			\N		\N	article	2016-05-04 11:06:10.177	\N	1960	\N	1120	f
6353	Cayuela2012	\N	10.7809/b-e.00078	\N	\N	\N	The Tree Biodiversity Network (BIOTREE-NET): prospects for biodiversity research and conservation in the Neotropics	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.428	\N	2012	\N	6354	f
6365	Schmidt2012	\N	10.7809/b-e.00065	\N	\N	\N	The West African Vegetation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.677	\N	2012	\N	6366	f
1115	Bornkamm_null_000403	\N	\N	\N	\N	\N	Standortsbedingungen und Wasserhaushalt von Trespen-Halbtrockenrasen (Mesobromion) im oberen Leinegebiet	000403		\N	BOK	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:09.629	\N	1500	\N	1116	t
1121	Bornkamm_null_000406	\N	\N	\N	\N	\N	Die Bunte-Erdflechten-Gesellschaft im südwestlichen Harzvorland. Ein Beitrag zur floristischen Soziologie von Kryptogamengesellschaften	000406		\N	BOK	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:10.39	\N	1500	\N	1122	t
6368	del_Moral2012	\N	10.7809/b-e.00221	\N	\N	\N	Thirty Years of Permanent Vegetation Plots, Mount St. Helens, Washington	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.778	\N	2012	\N	6369	f
6370	Stock2012#247	\N	10.7809/b-e.00162	\N	\N	\N	TMAP Wadden Sea Sedimentation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.791	\N	2012	\N	6371	f
6372	Stock2012	\N	10.7809/b-e.00163	\N	\N	\N	TMAP Wadden Sea Vegetation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.8	\N	2012	\N	6373	f
1124	Bornkamm_Eber_1967_000407	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der Keuperhügel bei Friedland (Kr. Göttingen)	000407	Bonn-Bad Godesberg	\N	BEB	\N		TABGESNE	\N	\N	236	\N	Y			\N		\N	article	2016-05-04 11:06:10.648	\N	1967	\N	1125	f
1127	Braun-blanquet_1921_000408	\N	\N	\N	\N	\N	Prinzipien einer Systematik der Pflanzengesellschaften auf floristischer Grundlage	000408		\N	BBL	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:10.941	\N	1921	\N	1128	f
6375	Venanzoni2012	\N	10.7809/b-e.00075	\N	\N	\N	Toward an Italian national vegetation database: VegItaly	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.81	\N	2012	\N	6376	f
1137	Bruelheide_1991_000412	\N	\N	\N	\N	\N	Kalkmagerrasen im östlichen und westlichen Meißner-Vorland	000412	Göttingen	\N	BRU	\N		TABGESNE	\N	\N	211	\N	Y			\N		\N	article	2016-05-04 11:06:11.96	\N	1991	\N	1138	f
1139	Budde_1951_000413	\N	\N	\N	\N	\N	Die Trocken- und Halbtrockenrasen und verwandte Gesellschaften im Wesergebiet bei Höxter	000413	Münster	\N	BUD	\N		TABGESNE	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:06:12.176	\N	1951	\N	1140	f
1144	Zerbe_Brande_Gladitz_2000_000414	\N	\N	\N	\N	\N	Kiefer, Eiche und Buche in der Menzer Heide (N-Brandenburg). Veränderungen der Waldvegetation unter dem Einfluss des Menschen	000414		\N	\N	\N		Zerbe_et_al_2000	\N	\N	122	\N	Y			\N		\N	article	2016-05-04 11:06:12.437	\N	2000	\N	1145	f
1147	Bultmann_1993_000415	\N	\N	\N	\N	\N	Flora und Vegetation der Kalkmagerrasen an der unteren Diemel	000415		\N	BON	\N		TABGESNE	\N	\N	265	\N	Y			\N		\N	article	2016-05-04 11:06:12.728	\N	1993	\N	1148	f
6378	Fegraus2012	\N	10.7809/b-e.00085	\N	\N	\N	Tropical Ecology Assessment and Monitoring Network (TEAM Network)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.831	\N	2012	\N	6379	f
1151	Burkart_1997_000417	\N	\N	\N	\N	\N	Kalkmagerrasen und Glatthaferwiesen im Unteren Werraland	000417	Frankfurt am Main	\N	BUK	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:13.15	\N	1997	\N	1152	f
6381	Rennie2012	\N	10.7809/b-e.00171	\N	\N	\N	UK Environmental Change Network  Vegetation Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.846	\N	2012	\N	6382	f
1155	Burrichter_1954_000419	\N	\N	\N	\N	\N	Die Halbtrockenrasen im Teutoburger Wald bei Iberg und Laer	000419	Münster	\N	BUR	\N		TABGESNE	\N	\N	5	\N	Y			\N		\N	article	2016-05-04 11:06:13.622	\N	1954	\N	1156	f
6384	Rodwell2012	\N	10.7809/b-e.00170	\N	\N	\N	UK National Vegetation Classification Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.859	\N	2012	\N	6385	f
6386	Kuzemko2012	\N	10.7809/b-e.00217	\N	\N	\N	Ukrainian Grasslands Database	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.872	\N	2012	\N	6387	f
1112	Boehnert_Reichhoff_1978_000402	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des NSG "Steckby-Lödderitzer Forst"	000402	Leipzig	\N	BRF	\N		TABGESNE	\N	\N	21	\N	Y			\N		\N	article	2016-05-04 11:06:09.375	\N	1978	\N	1113	f
1178	Dierschke_1995_000425	\N	\N	\N	\N	\N	Syntaxonomical survey of Molinio-Arrhenatheretea in Central Europe	000425	Bailleul	\N	DIE	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:15.125	\N	1995	\N	1179	f
1183	Dierschke_Knoop_1986_000427	\N	\N	\N	\N	\N	Kalk-Magerrasen und Saumgesellschaften des Langenberges und Tönneckenkopfes am Nordrand des Harzes	000427	Braunschweig	\N	DKN	\N		TABGESNE	\N	\N	27	\N	Y			\N		\N	article	2016-05-04 11:06:15.612	\N	1986	\N	1184	f
6391	Peet2012#215	\N	10.7809/b-e.00080	\N	\N	\N	VegBank  a permanent, open-access archive for vegetation-plot data	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.883	\N	2012	\N	6392	f
1197	Eichler_1970_000432	\N	\N	\N	\N	\N	Flora und Vegetation des Hakels	000432		\N	EIC	\N		TABGESNE	\N	\N	15	\N	Y			\N		\N	article	2016-05-04 11:06:16.72	\N	1970	\N	1198	f
6396	Prisco2012	\N	10.7809/b-e.00076	\N	\N	\N	VegDunes  a coastal dune vegetation database for the analysis of Italian EU habitats	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.914	\N	2012	\N	6397	f
1203	Evers_1997_000434	\N	\N	\N	\N	\N	Die Festuco-Brometea-Gesellschaften im nördlichen Harzvorland Niedersachsens	000434	Stuttgart, Braunschweig	\N	JAS	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:17.288	\N	1997	\N	1204	f
1206	Fischer_1982_000435	\N	\N	\N	\N	\N	Mosaik und Syndynamik der Pflanzengesellschaften von Lößböschungen im Kaiserstuhl	000435	Stuttgart, Braunschweig	\N	FIA	\N		TABGESNE	\N	\N	108	\N	Y			\N		\N	article	2016-05-04 11:06:17.501	\N	1982	\N	1207	f
6399	Hemp2012#177	\N	10.7809/b-e.00090	\N	\N	\N	Vegetation Database East Africa	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.94	\N	2012	\N	6400	f
1214	Gauckler_1957_000438	\N	\N	\N	\N	\N	Die Gipshügel in Franken, ihr Pflanzenkleid und ihre Tierwelt	000438	Nürnberg	\N	GAU	\N		TABGESNE	\N	\N	4	\N	Y			\N		\N	article	2016-05-04 11:06:18.125	\N	1957	\N	1215	f
1221	Glavac_Schlage_Schlage_1979_000440	\N	\N	\N	\N	\N	Das Gentiano-Koelerietum Knapp 1942 am Kleinen Dörnberg bei Zierenberg (Kreis Kassel)	000440		\N	GLS	\N		TABGESNE	\N	\N	100	\N	Y			\N		\N	article	2016-05-04 11:06:18.581	\N	1979	\N	1222	f
1225	Gross_Illig_Reichhoff_Wegener_1982_000441	\N	\N	\N	\N	\N	Die Flächennaturdenkmale im Schwefeltal bei Rübeland	000441	Dessau	\N	GIR	\N		TABGESNE	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:06:18.821	\N	1982	\N	1226	f
6404	Shirokikh2012	\N	10.7809/b-e.00087	\N	\N	\N	Vegetation Database Forest of Southern Ural	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.953	\N	2012	\N	6405	f
1188	Dittrich-Broeskamp_1988_000429	\N	\N	\N	\N	\N	Blaugras-Trockenrasen am Räuschenberg bei Höxter	000429	Münster	\N	DBR	\N		TABGESNE	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:06:16.046	\N	1988	\N	1189	f
1237	Hecker_1980_000445	\N	\N	\N	\N	\N	Ein schutzwürdiger Kalk-Magerrasen (Mesobrometum) bei Rüthen - Meiste (Kreis Soest)	000445	Münster	\N	HEC	\N		TABGESNE	\N	\N	23	\N	Y			\N		\N	article	2016-05-04 11:06:19.7	\N	1980	\N	1238	f
6406	Peppler-Lisbach2012#217	\N	10.7809/b-e.00144	\N	\N	\N	Vegetation Database Forests and Grasslands of the Lower Werra Region	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.98	\N	2012	\N	6407	f
1173	Dengler_1994_000423	\N	\N	\N	\N	\N	Flora und Vegetation von Trockenrasen und verwandten Gesellschaften im Bioshärenreservat Schorfheide-Chorin	000423	Berlin	\N	DGL	\N		TABGESNE	\N	\N	348	\N	Y			\N		\N	article	2017-01-23 16:32:08.966	\N	1994	\N	1174	f
1176	Dengler_Rixen_1997_000424	\N	\N	\N	\N	\N	Große Botanische Exkursionen 1996. Harz, Kyffhäuser und Uckermark	000424		\N	DGR	\N		TABGESNE	\N	\N	6	\N	Y			\N		\N	article	2017-01-23 16:33:16.779	\N	1997	\N	1177	f
6408	Peppler-Lisbach2012#218	\N	10.7809/b-e.00143	\N	\N	\N	Vegetation Database Forests of the Oldenburg Region	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.99	\N	2012	\N	6409	f
6410	Hemp2012	\N	10.7809/b-e.00153	\N	\N	\N	Vegetation Database Frankenalb	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:50.999	\N	2012	\N	6411	f
6412	Acic2012	\N	10.7809/b-e.00206	\N	\N	\N	Vegetation Database Grassland Vegetation of Serbia	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.009	\N	2012	\N	6413	f
6414	Bergmeier2012#122	\N	10.7809/b-e.00175	\N	\N	\N	Vegetation Database Isoeto-Nano-Juncetea in Greece and the Aegean Region	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.03	\N	2012	\N	6415	f
6418	Michl2012	\N	10.7809/b-e.00152	\N	\N	\N	Vegetation Database Main-Kinzig + Bergland	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.04	\N	2012	\N	6419	f
6420	Michl2012#202	\N	10.7809/b-e.00117	\N	\N	\N	Vegetation Database Mulgedio-Aconitetea and Related Vegetation Types	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.059	\N	2012	\N	6421	f
6422	Peppler-Lisbach2012	\N	10.7809/b-e.00145	\N	\N	\N	Vegetation Database Nardus Swards of Germany	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.069	\N	2012	\N	6423	f
6426	Mulhouse2012	\N	10.7809/b-e.00225	\N	\N	\N	Vegetation Database of Central New Mexico  desert grass- and shrubland net-primary production quadrat data	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.08	\N	2012	\N	6427	f
6428	Heinken2012#174	\N	10.7809/b-e.00119	\N	\N	\N	Vegetation Database of Deciduous Forests on Acidic Soils in NW Europe	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.099	\N	2012	\N	6429	f
1251	Helmecke_1978_000450	\N	\N	\N	\N	\N	Auswertung von Dauerflächenbeobachtungen mittels mathematisch-statistischer Methoden	000450		\N	HEL	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:20.927	\N	1978	\N	1252	f
6431	Bita-Nicolae2012	\N	10.7809/b-e.00200	\N	\N	\N	Vegetation Database of Dry Grasslands in the Southeast Romania	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.109	\N	2012	\N	6432	f
6434	Virtanen2012	\N	10.7809/b-e.00167	\N	\N	\N	Vegetation Database of Eurasian Tundra	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.121	\N	2012	\N	6435	f
1262	Hofmann_1960_000453	\N	\N	\N	\N	\N	Meteorologisches Instrumentenpraktikum	000453		\N	HOG	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:21.609	\N	1960	\N	1263	f
1264	Passarge_1957_000454	\N	\N	\N	\N	\N	Waldgesellschaften des nördlichen Havellandes	000454		\N	\N	\N		\N	\N	\N	266	\N	Y			\N		\N	article	2016-05-04 11:06:21.872	\N	1957	\N	1265	f
6440	Maskell2012	\N	10.7809/b-e.00172	\N	\N	\N	Vegetation Database of Great Britain: Countryside Survey	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.137	\N	2012	\N	6441	f
6443	Cuello2012	\N	10.7809/b-e.00233	\N	\N	\N	Vegetation Database of Guaramacal, Andes, Venezuela	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.178	\N	2012	\N	6444	f
6446	Naqinezhad2012#210	\N	10.7809/b-e.00101	\N	\N	\N	Vegetation Database of Iran	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.197	\N	2012	\N	6447	f
6448	Naqinezhad2012#209	\N	10.7809/b-e.00102	\N	\N	\N	Vegetation Database of Mountain Wetlands of Iran	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.218	\N	2012	\N	6449	f
1231	Haecker_1984_000443	\N	\N	\N	\N	\N	Die Vegetationsverhältnisse des Stockberges bei Ottbergen	000443		\N	HAC	\N		TABGESNE	\N	\N	53	\N	Y			\N		\N	article	2016-05-04 11:06:19.284	\N	1984	\N	1232	f
1288	Janssen_1986_000463	\N	\N	\N	\N	\N	Die Vegetation des Ösels (Kreis Wolfenbüttel)	000463	Braunschweig	\N	JAB	\N		TABGESNE	\N	\N	23	\N	Y			\N		\N	article	2016-05-04 11:06:23.991	\N	1986	\N	1289	f
1290	Kaiser_1926_000464	\N	\N	\N	\N	\N	Die Pflanzenwelt des hennebergisch-fränkischen Muschelkalkgebietes	000464	Berlin	\N	KAI	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:24.29	\N	1926	\N	1291	f
6451	Ugurlu2012	\N	10.7809/b-e.00110	\N	\N	\N	Vegetation Database of Oak Communities in Turkey	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.231	\N	2012	\N	6452	f
1298	Knapp_1974_000467	\N	\N	\N	\N	\N	Beitrag zur Vegetation des Naturschutzgebietes "Borntal" bei Schierwitz (Bezirk Gera)	000467	Jena	\N	KNH	\N		TABGESNE	\N	\N	12	\N	Y			\N		\N	article	2016-05-04 11:06:24.973	\N	1974	\N	1299	f
1300	Knapp_1979_000468	\N	\N	\N	\N	\N	Geobotanische Studien an Waldgrenzstandorten des hercynischen Florengebietes, Teil 2	000468		\N	KNH	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:25.228	\N	1979	\N	1301	f
1302	Knapp_1980_000469	\N	\N	\N	\N	\N	Geobotanische Studien an Waldgrenzstandorten des hercynischen Florengebietes, Teil 3	000469		\N	KNH	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:25.442	\N	1980	\N	1303	f
1304	Knapp_Reichhoff_1973_000470	\N	\N	\N	\N	\N	Pflanzengesellschaften xerothermer Standorte des Naturschutzgebietes "Wipperdurchbruch" in der Hainleite	000470	Berlin	\N	KNR	\N		TABGESNE	\N	\N	82	\N	Y			\N		\N	article	2016-05-04 11:06:25.711	\N	1973	\N	1305	f
6454	Indreica2012	\N	10.7809/b-e.00204	\N	\N	\N	Vegetation Database of Oak Forests of Romania	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.246	\N	2012	\N	6455	f
6459	Mudrak2012	\N	10.7809/b-e.00224	\N	\N	\N	Vegetation Database of Observatory Woods, Wisconsin  mapped vegetation	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.262	\N	2012	\N	6460	f
6461	Heinken2012	\N	10.7809/b-e.00151	\N	\N	\N	Vegetation Database of Pine Forests on Acidic Soils in Germany	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.293	\N	2012	\N	6462	f
1314	Knapp_1971_000475	\N	\N	\N	\N	\N	Felsfluren im Bereich des Werra-Tales	000475		\N	KNA	\N		TABGESNE	\N	\N	12	\N	Y			\N		\N	article	2016-05-04 11:06:27.02	\N	1971	\N	1315	f
1316	Knapp_1978_000476	\N	\N	\N	\N	\N	Trockenrasen und Therophyten-Fluren auf Kalk-, Sand-, Grus- und Schwermetall-Böden im mittleren Hessen	000476	Gießen	\N	KNA	\N		TABGESNE	\N	\N	49	\N	Y			\N		\N	article	2016-05-04 11:06:27.295	\N	1978	\N	1317	f
6465	Major2012	\N	10.7809/b-e.00219	\N	\N	\N	Vegetation Database of Québec (MRNF)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.306	\N	2012	\N	6466	f
1321	Koch_1926_000478	\N	\N	\N	\N	\N	Die Vegetationseinheiten der Linthebene unter Berücksichtigung der Verhältnisse in der Nordostschweiz	000478		\N	KOC	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:27.796	\N	1926	\N	1322	f
1347	Strassl_2000_000489	\N	\N	\N	\N	\N	Die Vegetation im oberen Briesetal bei Berlin	000489		\N	\N	\N		Stra�l_2000	\N	\N	71	\N	Y			\N		\N	article	2016-05-04 11:06:30.746	\N	2000	\N	1348	f
1326	Korneck_1960_000480	\N	\N	\N	\N	\N	Das Mesobrometum collinum agrostidetosum tenuis (subass. nov.)	000480	Darmstadt	\N	KOR	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:28.332	\N	1960	\N	1327	f
1328	Korneck_1974_000481	\N	\N	\N	\N	\N	Xerothermvegetation in Rheinland-Pfalz und Nachbargebieten	000481	Bonn-Bad Godesberg	\N	KOR	\N		TABGESNE	\N	\N	423	\N	Y			\N		\N	article	2016-05-04 11:06:28.555	\N	1974	\N	1329	f
1338	Krausch_1961_000486	\N	\N	\N	\N	\N	Die kontinentalen Steppenrasen (Festucetalia vallesiacae) in Brandenburg	000486	Berlin	\N	KRA	\N		TABGESNE	\N	\N	129	\N	Y			\N		\N	article	2016-05-04 11:06:29.899	\N	1961	\N	1339	f
2377	Passarge_1960_000871	\N	\N	\N	\N	\N	Waldgesellschaften NW-Mecklenburgs	000871	Berlin	\N	\N	\N		\N	\N	\N	70	\N	Y			\N		\N	article	2016-09-22 11:40:24.886	\N	1960	\N	2378	f
6468	Hatim2012	\N	10.7809/b-e.00099	\N	\N	\N	Vegetation Database of Sinai in Egypt	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.329	\N	2012	\N	6469	f
1353	Lienenbecker_1971_000491	\N	\N	\N	\N	\N	Die Pflanzengesellschaften im Raum Bielefeld-Halle	000491	Bielefeld	\N	LIE	\N		TABGESNE	\N	\N	15	\N	Y			\N		\N	article	2016-05-04 11:06:31.235	\N	1971	\N	1354	f
1356	Lohmeyer_1953_000492	\N	\N	\N	\N	\N	Beitrag zur Kenntnis der Pflanzengesellschaften in der Umgebung von Höxter a. d. Weser	000492		\N	LOH	\N		TABGESNE	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:06:31.457	\N	1953	\N	1357	f
6470	ilc2012	\N	10.7809/b-e.00215	\N	\N	\N	Vegetation Database of Slovenia	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.347	\N	2012	\N	6471	f
6474	Borchardt2012	\N	10.7809/b-e.00105	\N	\N	\N	Vegetation Database of South-Western Kyrgyzstan  the walnut-wildfruit forests and alpine pastures	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.36	\N	2012	\N	6475	f
1334	Krausch_null_000484	\N	\N	\N	\N	\N	Vorschläge zur Gliederung der mitteleuropäischen Sand- und Silikat- Trockenrasen	000484		\N	KRA	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:29.335	\N	1500	\N	1335	t
1336	Krausch_null_000485	\N	\N	\N	\N	\N	Mikroklimatische Untersuchungen an Steppenpflanzen-Gesellschaften des Randhänge des Oderbruches	000485	Berlin	\N	KRA	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:29.599	\N	1500	\N	1337	t
6476	Finckh2012	\N	10.7809/b-e.00094	\N	\N	\N	Vegetation Database of Southern Morocco	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.387	\N	2012	\N	6477	f
6478	Dölle2012#157	\N	10.7809/b-e.00148	\N	\N	\N	Vegetation Database of Strict Forest Reserves in NW-Germany	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.402	\N	2012	\N	6479	f
6480	Dölle2012#158	\N	10.7809/b-e.00147	\N	\N	\N	Vegetation Database of Successional Permanent Plots in Göttingen	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.42	\N	2012	\N	6481	f
6483	Semenishchenkov2012	\N	10.7809/b-e.00213	\N	\N	\N	Vegetation Database of Sudost-Desna Interfluve Area	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.443	\N	2012	\N	6484	f
6485	Dölle2012	\N	10.7809/b-e.00150	\N	\N	\N	Vegetation Database of Temperate Deciduous Forests of the Göttinger Wald	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.46	\N	2012	\N	6486	f
6488	Rosati2012#229	\N	10.7809/b-e.00185	\N	\N	\N	Vegetation Database of the Cilento National Park	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.483	\N	2012	\N	6489	f
1359	Mahn_1957_000493	\N	\N	\N	\N	\N	Über die Vegetations- und Standortsverhältnisse einiger Porphyrkuppen bei Halle	000493	Halle (Saale)	\N	MAH	\N		TABGESNE	\N	\N	7	\N	Y			\N		\N	article	2016-05-04 11:06:31.668	\N	1957	\N	1360	f
6491	Rangel-Churio2012	\N	10.7809/b-e.00231	\N	\N	\N	Vegetation Database of the Colombian Caribbean Region	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.511	\N	2012	\N	6492	f
1363	Mahn_1965_000495	\N	\N	\N	\N	\N	Vegetationsaufbau und Standortsverhältnisse der kontinental beeinflußten Xerothermrasengesellschaften Mitteldeutschlands	000495		\N	MAH	\N		TABGESNE	\N	\N	34	\N	Y			\N		\N	article	2016-05-04 11:06:32.093	\N	1965	\N	1364	f
1365	Marstaller_1968_000496	\N	\N	\N	\N	\N	Die Xerothermflora der Gipshänge bei Jena (Ostthüringen) unter besonderer Berücksichtigung der Bunten Erdflechten-Gesellschaft	000496		\N	MAR	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:32.343	\N	1968	\N	1366	f
1369	Marstaller_1970_000498	\N	\N	\N	\N	\N	Die natürlichen Saumgesellschaften des Verbandes Geranion sanguinei Th. Müller 1961 der Muschelkalkgebiete Mittelthüringens	000498	Berlin	\N	MAR	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:32.864	\N	1970	\N	1370	f
1371	Meusel_1937_000499	\N	\N	\N	\N	\N	Mitteldeutsche Vegetationsbilder. 1. Die Steinklöbe bei Nebra und der Ziegelrodaer Forst	000499		\N	MEU	\N		TABGESNE	\N	\N	10	\N	Y			\N		\N	article	2016-05-04 11:06:33.116	\N	1937	\N	1372	f
6494	Ruprecht2012	\N	10.7809/b-e.00201	\N	\N	\N	Vegetation Database of the Dry Grasslands from the Transylvanian Basin	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.529	\N	2012	\N	6495	f
6497	Ugurlu2012#259	\N	10.7809/b-e.00109	\N	\N	\N	Vegetation Database of the Grassland Communities in Anatolia	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.557	\N	2012	\N	6498	f
1391	Neuwirth_1958_000506	\N	\N	\N	\N	\N	Pflanzensoziologische und ökologische Untersuchungen an Hängen des Lindbusches, der Harslebener Berge und des Steinholzes	000506	Halle (Saale)	\N	NEW	\N		TABGESNE	\N	\N	32	\N	Y			\N		\N	article	2016-05-04 11:06:34.809	\N	1958	\N	1392	f
1393	Nowak_1990_000507	\N	\N	\N	\N	\N	Glatthafer- und Goldhafer-Wiesen (Arrhenatheretalia elatioris Pawlowski 1928)	000507	Frankfurt am Main	\N	NOW	\N		TABGESNE	\N	\N	144	\N	Y			\N		\N	article	2016-05-04 11:06:35.047	\N	1990	\N	1394	f
1398	Pardey_1991_000509	\N	\N	\N	\N	\N	Das Naturschutzgebiet "Karlsberg" bei Hildesheim - Flora, Vegetation, Zustand und Entwicklungsmöglichkeiten	000509	Hannover	\N	PAR	\N		TABGESNE	\N	\N	4	\N	Y			\N		\N	article	2016-05-04 11:06:35.549	\N	1991	\N	1399	f
1400	Passarge_1979_000510	\N	\N	\N	\N	\N	Über Xerothermrasen im Seelower Odergebiet	000510	Berlin	\N	PAS	\N		TABGESNE	\N	\N	100	\N	Y			\N		\N	article	2016-05-04 11:06:35.812	\N	1979	\N	1401	f
6499	Naqinezhad2012	\N	10.7809/b-e.00103	\N	\N	\N	Vegetation Database of the Hyrcanian Area	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.578	\N	2012	\N	6500	f
1405	Philippi_1984_000512	\N	\N	\N	\N	\N	Trockenrasen, Sandfluren und thermophile Saumgesellschaften des Tauber-Main-Gebietes	000512		\N	PHI	\N		TABGESNE	\N	\N	93	\N	Y			\N		\N	article	2016-05-04 11:06:36.377	\N	1984	\N	1406	f
6508	Etzold2012	\N	10.7809/b-e.00126	\N	\N	\N	Vegetation Database of the Shahdag Region, Azerbaijan	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.592	\N	2012	\N	6509	f
6512	Kayser2012	\N	10.7809/b-e.00120	\N	\N	\N	Vegetation Database of the Upper Rhine Alluvial Plain Forests	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.658	\N	2012	\N	6513	f
1416	Raehse_1986_000516	\N	\N	\N	\N	\N	Zur Flora und Vegetation des Landschaftsschutzgebietes "Kalkberge und Diebachaue" bei Heiligenrode, Landkreis Kassel	000516		\N	RAE	\N		TABGESNE	\N	\N	9	\N	Y			\N		\N	article	2016-05-04 11:06:37.406	\N	1986	\N	1417	f
6517	Lysenko2012	\N	10.7809/b-e.00208	\N	\N	\N	Vegetation Database of the Volga and the Ural Rivers Basins	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.679	\N	2012	\N	6518	f
1422	Redslob_1971_000518	\N	\N	\N	\N	\N	Der Enzian-Zwenkenrasen am Kriegerdenkmal von Lämershagen (Kreis Bielefeld)	000518		\N	RED	\N		TABGESNE	\N	\N	1	\N	Y			\N		\N	article	2016-05-04 11:06:37.956	\N	1971	\N	1423	f
1424	Reichhoff_1973_000519	\N	\N	\N	\N	\N	Homogenitäts- und Strukturuntersuchungen an xerothermen Rasengesellschaften und trockenen Ausbildungen der Glatthaferwiese im NSG "Leutratal" bei Jena	000519	Berlin	\N	REI	\N		TABGESNE	\N	\N	24	\N	Y			\N		\N	article	2016-05-04 11:06:38.174	\N	1973	\N	1425	f
6519	Ewald2012#162	\N	10.7809/b-e.00058	\N	\N	\N	Vegetation databases provide a close-up on altitudinal tree species distribution in the Bavarian Alps	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.705	\N	2012	\N	6520	f
1431	Rodi_1974_000522	\N	\N	\N	\N	\N	Trockenrasengesellschaften des nordwestlichen Tertiärhügellandes	000522	München	\N	ROD	\N		TABGESNE	\N	\N	20	\N	Y			\N		\N	article	2016-05-04 11:06:39.184	\N	1974	\N	1432	f
6523	Heinrichs2012	\N	10.7809/b-e.00059	\N	\N	\N	Vegetation dynamics of beech forests on limestone in central Germany over half a century  effects of climate change, forest management, eutrophication or game browsing?	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.718	\N	2012	\N	6524	f
6534	Wamelink2012	\N	10.7809/b-e.00067	\N	\N	\N	Vegetation relevés and soil measurements in the Netherlands: the Ecological Conditions Database (EC)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.746	\N	2012	\N	6535	f
6540	Peet2012	\N	10.7809/b-e.00081	\N	\N	\N	Vegetation-plot database of the Carolina Vegetation Survey	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.85	\N	2012	\N	6541	f
6546	Biurrun2012	\N	10.7809/b-e.00121	\N	\N	\N	Vegetation-Plot Database of the University of the Basque Country (BIOVEG)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.904	\N	2012	\N	6547	f
6549	Ewald2012	\N	10.7809/b-e.00073	\N	\N	\N	VegetWeb  the national online-repository of vegetation plots from Germany	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.948	\N	2012	\N	6550	f
1380	Moeseler_1989_000502	\N	\N	\N	\N	\N	Die Kalkmagerrasen der Eifel	000502	Bonn	\N	MOS	\N		TABGESNE	\N	\N	283	\N	Y			\N		\N	article	2016-05-04 11:06:33.84	\N	1989	\N	1381	f
6551	Jansen2012	\N	10.7809/b-e.00070	\N	\N	\N	VegMV  the vegetation database of Mecklenburg-Vorpommern	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.974	\N	2012	\N	6552	f
6556	Porcher2012	\N	10.7809/b-e.00168	\N	\N	\N	Vigie-flore  a nationwide monitoring programme for common plant species and plant communities in France	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:51.996	\N	2012	\N	6557	f
6558	Becker2012#120	\N	10.7809/b-e.00118	\N	\N	\N	VIOLETEA  heavy metal grasslands	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:52.034	\N	2012	\N	6559	f
1367	Marstaller_null_000497	\N	\N	\N	\N	\N	Die xerothermen Pflanzengesellschaften waldfreier Sonderstandorte im Buntsandsteingebiet des mittleren Saaletales (Thüringen)	000497		\N	MAR	\N		TABGESNE	\N	\N	17	\N	Y			\N		\N	article	2016-05-04 11:06:32.646	\N	1500	\N	1368	t
6563	Weyembergh2012	\N	10.7809/b-e.00068	\N	\N	\N	VLAVEDAT  the vegetation database of Flanders (northern Belgium)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:52.048	\N	2012	\N	6564	f
6566	Chepinoga2012	\N	10.7809/b-e.00107	\N	\N	\N	Wetland Vegetation Database of Baikal Siberia (WETBS)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:52.082	\N	2012	\N	6567	f
6575	Reger2012	\N	10.7809/b-e.00072	\N	\N	\N	WINALPecobase  ecological database of mountain forests in the Bavarian Alps	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:52.1	\N	2012	\N	6576	f
6581	Waller2012	\N	10.7809/b-e.00082	\N	\N	\N	Wisconsin Vegetation Database  plant community survey and resurvey data from the Wisconsin Plant Ecology Laboratory	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:52.176	\N	2012	\N	6582	f
6583	Jansen2010	\N	10.1111/j.1654-1103.2010.01209.x	\N	\N	\N	<prt>Plant names in vegetation databases - a neglected source of bias</prt>	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-07-19 14:44:52.223	\N	2010	\N	6584	f
1469	Schmidt_1994_000534	\N	\N	\N	\N	\N	Kalkmagerrasen- und Felsband-Gesellschaften im mittleren Werratal	000534	Göttingen	\N	SCT	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:41.969	\N	1994	\N	1470	f
6667	Heeren_1999	\N	\N	\N	\N	\N	Birkes Clusteranalyse 19	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-10 15:02:48.132	\N	1999	6668	\N	f
1482	Schubert_1954_000540	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der Bottendorfer Höhe	000540	Halle (Saale)	\N	SUB	\N		TABGESNE	\N	\N	22	\N	Y			\N		\N	article	2016-05-04 11:06:43.243	\N	1954	\N	1483	f
1487	Schubert_1963_000542	\N	\N	\N	\N	\N	Die Sesleria-varia-reichen Pflanzengesellschaften in Mitteldeutschland	000542	Berlin	\N	SCW	\N		TABGESNE	\N	\N	71	\N	Y			\N		\N	article	2016-05-04 11:06:43.715	\N	1963	\N	1488	f
6701	Heeren_1990	\N	\N	\N	\N	\N	Birkes Testtitel	\N	\N	\N	\N	\N	Mein großes Werk	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-20 10:12:38.254	\N	1990	\N	\N	f
1502	Tigges_1979_000547	\N	\N	\N	\N	\N	Flora und Vegetation von Westberg und Aschenburg unter besonderer Berücksichtigung des Naturschutzes	000547		\N	TIG	\N		TABGESNE	\N	\N	58	\N	Y			\N		\N	article	2016-05-04 11:06:44.929	\N	1979	\N	1503	f
1505	Volk_1937_000548	\N	\N	\N	\N	\N	Über einige Trockenrasengesellschaften des Würzburger Wellenkalks	000548		\N	VOL	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:45.191	\N	1937	\N	1506	f
6673	Heeren_1998	\N	\N	\N	\N	\N	Birkes Clusteranalyse 20	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-11 09:29:36.389	\N	1998	6674	\N	f
1516	Wegener_1986_000552	\N	\N	\N	\N	\N	Rasengesellschaften des NSG "Bockberg" im Harz	000552	Dessau	\N	WEG	\N		TABGESNE	TV	\N	11	\N	Y			\N		\N	article	2016-05-04 11:06:46.172	\N	1986	\N	1517	f
6675	Heeren_1997	\N	\N	\N	\N	\N	Birkes Clusteranalyse 21	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-11 09:31:59.347	\N	1997	6676	\N	f
6677	Heeren_1996	\N	\N	\N	\N	\N	Birkes Clusteranalyse 22	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-11 09:33:20.984	\N	1996	6678	\N	f
6608	Heeren_2017	\N	\N	\N	\N	\N	Testtitel	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Birkes Test Institution	\N	book	2017-10-05 15:49:49.076	\N	2017	\N	\N	f
1531	Winterhoff_1965_000557	\N	\N	\N	\N	\N	Die Vegetation der Muschelkalkfelshänge im hessischen Werrabergland	000557	Ludwigsburg	\N	WIN	\N		TABGESNE	\N	\N	232	\N	Y			\N		\N	article	2016-05-04 11:06:47.302	\N	1965	\N	1532	f
1534	Witschel_1994_000558	\N	\N	\N	\N	\N	Die Arealgrenzen des Xerobrometum Br.-Bl.15 em. 31 im Südwesten des Verbreitungsgebietes	000558		\N	WIS	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:47.553	\N	1994	\N	1535	f
1536	Witschel_1993_000559	\N	\N	\N	\N	\N	Zur Synsystematik der Trinia glauca-reichen Trockenrasen im südlichen Oberrheinraum	000559	Karlsruhe	\N	WIS	\N		TABGESNE	\N	\N	27	\N	Y			\N		\N	article	2016-05-04 11:06:47.76	\N	1993	\N	1537	f
1538	Witschel_1991_000560	\N	\N	\N	\N	\N	Die Trinia glauca-reichen Trockenrasen in Deutschland und ihre Entwicklung seit 1800	000560	München	\N	WIS	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:48.02	\N	1991	\N	1539	f
1540	Witschel_1980_000561	\N	\N	\N	\N	\N	Xerothermvegetation und dealpine Vegetationskomplexe in Südbaden. Vegetationskundliche Untersuchungen und die Entwicklung eines Wertungsmodells für den Naturschutz	000561	Karlsruhe	\N	WIS	\N		TABGESNE	\N	\N	347	\N	Y			\N		\N	article	2016-05-04 11:06:48.268	\N	1980	\N	1541	f
1542	Wollert_1967_000562	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der Oser Mittelmecklenburgs unter besonderer Berücksichtigung der Trockenrasengesellschaften	000562	Rostock	\N	WLT	\N		TABGESNE	\N	\N	45	\N	Y			\N	Universität Rostock	\N	article	2016-05-04 11:06:48.54	\N	1967	\N	1543	f
1544	Wollert_1964_000563	\N	\N	\N	\N	\N	Die Vegetation auf dem Heidberg bei Teterow	000563	Rostock	\N	WLT	\N		TABGESNE	\N	\N	21	\N	Y			\N		\N	article	2016-05-04 11:06:48.78	\N	1964	\N	1545	f
1547	Zehm_1963_000564	\N	\N	\N	\N	\N	Über den Enzian-Zwenkenrasen der Paderborner Hochfläche	000564	Münster	\N	ZEH	\N		TABGESNE	\N	\N	4	\N	Y			\N		\N	article	2016-05-04 11:06:48.997	\N	1963	\N	1548	f
1561	Blosat_Schmidt_1975_000570	\N	\N	\N	\N	\N	Laubwaldgesellschaften im Unteren Eichsfeld	000570		\N	BS	\N		BOHN	\N	\N	137	\N	Y			\N		\N	article	2016-05-04 11:06:50.504	\N	1975	\N	1562	f
1563	Burrichter_Wittig_1977_000571	\N	\N	\N	\N	\N	Der Flattergras-Buchenwald in Westfalen	000571		\N	BW	\N		BOHN	\N	\N	139	\N	Y			\N		\N	article	2016-05-04 11:06:50.808	\N	1977	\N	1564	f
1569	Gerlach_1970_000573	\N	\N	\N	\N	\N	Wald- u. Forstgesellschaften im Solling	000573	Bonn-Bad Godesberg	\N	GE	\N		BOHN (Tab1)	Gerlach_1970	\N	264	\N	Y			\N		\N	article	2016-05-04 11:06:51.305	\N	1970	\N	1570	f
6633	Heeren_2013	\N	\N	\N	\N	\N	Birkes Clusteranalyse 3	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-10 14:21:42.78	\N	2013	6634	\N	f
6637	Heeren_2012	\N	\N	\N	\N	\N	Birkes Clusteranalyse 4	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-10 14:23:44.017	\N	2012	6638	\N	f
6639	Heeren_2010	\N	\N	\N	\N	\N	Birkes Clusteranalyse 6	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-10 14:24:13.415	\N	2010	6640	\N	f
6643	Heeren_2009	\N	\N	\N	\N	\N	Birkes Clusteranalyse 7	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-10 14:27:29.935	\N	2009	6644	\N	f
6647	Heeren_2008	\N	\N	\N	\N	\N	Birkes Clusteranalyse 8	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-10 14:29:24.416	\N	2008	6648	\N	f
6649	Heeren_2006	\N	\N	\N	\N	\N	Birkes Clusteranalyse 10	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-10 14:44:13.492	\N	2006	6650	\N	f
6651	Heeren_2007	\N	\N	\N	\N	\N	Birkes Clusteranalyse 11	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-10 14:46:06.758	\N	2007	6652	\N	f
6655	Heeren_2005	\N	\N	\N	\N	\N	Birkes Clusteranalyse 13	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-10 14:48:15.117	\N	2005	6656	\N	f
6657	Heeren_2004	\N	\N	\N	\N	\N	Birkes Clusteranalyse 14	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-10 14:50:02.364	\N	2004	6658	\N	f
6659	Heeren_2003	\N	\N	\N	\N	\N	Birkes Clusteranalyse 15	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-10 14:56:31.565	\N	2003	6660	\N	f
6661	Heeren_2002	\N	\N	\N	\N	\N	Birkes Clusteranalyse 16	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-10 14:57:52.065	\N	2002	6662	\N	f
6663	Heeren_2001	\N	\N	\N	\N	\N	Birkes Clusteranalyse 17	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-10 15:00:15.6	\N	2001	6664	\N	f
6665	Heeren_2000	\N	\N	\N	\N	\N	Birkes Clusteranalyse 18	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-10 15:01:49.006	\N	2000	6666	\N	f
6679	Heeren_1995	\N	\N	\N	\N	\N	Birkes Clusteranalyse 23	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-11 09:35:41.577	\N	1995	6680	\N	f
6681	Heeren_1994	\N	\N	\N	\N	\N	Birkes Clusteranalyse 24	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-11 09:37:45.145	\N	1994	6682	\N	f
6694	Heeren_1992	\N	\N	\N	\N	\N	Birkes Clusteranalyse 26	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-13 10:30:31.259	\N	1992	6695	\N	f
6702	Heeren_1988	\N	\N	\N	\N	\N	Birkes Clusteranalyse 30	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-24 15:40:20.58	\N	1988	6703	\N	f
6596	Frehse_1982	\N	\N	\N	\N	dipl	Test	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Uni Greifswald	\N	thesis	2017-10-18 10:14:31.936	\N	1982	\N	\N	f
6700	Frehse_1981	\N	\N	\N	\N	\N	Test	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2017-10-18 13:20:55.954	\N	1981	\N	\N	f
6704	Heeren_1987	\N	\N	\N	\N	\N	Birkes Clusteranalyse 31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-30 10:34:48.569	\N	1987	6705	\N	f
6712	Heeren_1986	\N	\N	\N	\N	\N	Birkes Clusteranalyse 31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-30 11:14:24.539	\N	1986	6713	\N	f
6714	Heeren_1985	\N	\N	\N	\N	\N	Birkes Clusteranalyse 31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-30 11:16:53.104	\N	1985	6715	\N	f
6718	Heeren_1984	\N	\N	\N	\N	\N	Birkes Clusteranalyse 31	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-30 11:20:49.738	\N	1984	6719	\N	f
6720	Heeren_1983	\N	\N	\N	\N	\N	Clusteranalyse	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-11-07 09:26:47.36	\N	1983	6721	\N	f
6722	Heeren_1982	\N	\N	\N	\N	\N	Clusteranalyse	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-11-07 09:30:15.736	\N	1982	6723	\N	f
6605	Hoppe_2005_100716	\N	\N	\N	\N	\N	Das Reinhold-Tüxen-Archiv am Institut für Geobotanik der Universität Hannover	100716	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-09-20 10:31:51.716	\N	2005	\N	6606	f
6614	Heeren_2016	\N	\N	\N	\N	\N	Birkes Testpublikation	\N	\N	\N	\N	\N	Birkes Testbuch	\N	\N	\N	\N	\N	\N	\N	\N	\N	Birkes Testhochschule	\N	book	2017-10-06 10:12:49.276	\N	2016	\N	\N	f
6615	Heeren_2015	\N	\N	\N	\N	\N	Birkes Clusteranalyse	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-06 10:41:20.606	\N	2015	6616	\N	f
6617	Heeren_2014	\N	\N	\N	\N	\N	Birkes Clusteranalyse 2	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-10-10 13:59:47.085	\N	2014	6618	\N	f
6724	Heeren_1981	\N	\N	\N	\N	\N	Clusteranalyse	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2017-11-08 15:17:28.61	\N	1981	6725	\N	f
1665	Passarge_1959_000607	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen in den Wäldern der Jungmoränenlandschaft um Dargun/Ostmecklenburg	000607	Berlin	\N	PA	\N		BOHN	\N	\N	183	\N	Y			\N		\N	article	2016-05-04 11:06:59.692	\N	1959	\N	1666	f
1692	Schmidt_1970_000618	\N	\N	\N	\N	\N	Untersuchungen über die Phosphorversorgung niedersächsischer Buchenwaldgesellschaften	000618	Göttingen	\N	SW	\N		BOHN	\N	\N	48	\N	Y			\N		\N	article	2016-05-04 11:07:02.337	\N	1970	\N	1693	f
2174	Oberdorfer_1957_000791	\N	\N	\N	\N	\N	Süddeutsche Pflanzengesellschaften	000791	Jena	\N	\N	\N		\N	\N	\N	2	\N	Y			\N		\N	article	2016-05-04 11:07:43.861	\N	1957	\N	2175	f
6603	Frehse_2005	\N	\N	\N	\N	\N	Jawoll1	\N	\N	\N	blub	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2018-03-20 11:11:32.447	\N	2005	\N	6604	f
1777	Steiner_Buchwald_1991_000647	\N	\N	\N	\N	\N	Vegetationskundliche und hydrochemische Untersuchungen im Naturschutzgebiet "Bisten" (Südschwarzwald)	000647	Karlsruhe	\N	\N	\N		\N	TV	\N	116	\N	Y			\N		\N	article	2016-05-04 11:07:09.466	\N	1991	\N	1778	f
1780	Ahrns_Hofmann_1998_000649	\N	\N	\N	\N	\N	Vegetationsdynamik und Florenwandel im ehemaligen mitteldeutschen Waldschutzgebiet "Hainich" im Intervall 1963-1995	000649	Halle (Saale)	\N	\N	\N		\N	TV	\N	102	\N	Y			\N		\N	article	2016-05-04 11:07:09.679	\N	1998	\N	1781	f
1783	Scamoni_1975_000650	\N	\N	\N	\N	\N	Die Wälder um Chorin. Vegetation und Grundlagen für die Erschließung und Pflege eines Landschaftsschutzgebietes	000650		\N	\N	\N		\N	\N	\N	357	\N	Y			\N		\N	article	2016-05-04 11:07:09.969	\N	1975	\N	1784	f
1785	Passarge_1958_000651	\N	\N	\N	\N	\N	Beobachtungen über Waldgesellschaften im Jungmoränengebiet um Flensburg und Schleswig	000651	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:10.194	\N	1958	\N	1786	f
1787	Hofmann_1959_000652	\N	\N	\N	\N	\N	Die Wälder des Meininger Muschelkalkgebietes	000652	Berlin	\N	\N	\N		\N	\N	\N	90	\N	Y			\N		\N	article	2016-05-04 11:07:10.411	\N	1959	\N	1788	f
1790	Golisch_1996_000653	\N	\N	\N	\N	\N	Buchenwälder im Kreis Lippe (NRW) mit einer Übersicht über die Querco-Fagetea	000653	Göttingen	\N	\N	\N		\N	\N	\N	200	\N	Y			\N		\N	article	2016-05-04 11:07:10.654	\N	1996	\N	1791	f
1798	Apffelstaedt_Bernhard_1996_000655	\N	\N	\N	\N	\N	Vegetations- und populationsbiologische Untersuchungen zur Dynamik von Naturwaldzellen und Windwurfflächen in Nordrhein-Westfalen	000655	Göttingen	\N	\N	\N		\N	\N	\N	18	\N	Y			\N		\N	article	2016-05-04 11:07:11.157	\N	1996	\N	1799	f
1804	Grosser_1966_000657	\N	\N	\N	\N	\N	Urwald Weißwasser	000657		\N	\N	\N		\N	TV	\N	43	\N	Y			\N		\N	article	2016-05-04 11:07:11.703	\N	1966	\N	1805	f
1806	Grosser_1964_000658	\N	\N	\N	\N	\N	Die Wälder am Jagdschloß bei Weißwasser (OL). Waldkundliche Studien in der Muskauer Heide	000658	Leipzig	\N	\N	\N		\N	TV	\N	311	\N	Y			\N		\N	article	2016-05-04 11:07:11.953	\N	1964	\N	1807	f
1809	Ellenberg_1939_000659	\N	\N	\N	\N	\N	Über Zusammensetzung, Standort und Stoffproduktion bodenfeuchter Eichen- und Buchen-Mischwaldgesellschaften Nordwestdeutschlands	000659	Hannover	\N	\N	\N		\N	TV	\N	15	\N	Y			\N		\N	article	2016-05-04 11:07:12.213	\N	1939	\N	1810	f
1821	Haffner_1978_000665	\N	\N	\N	\N	\N	Zur Verbreitung und Vergesellschaftung von Quercus pubescens an Obermosel und Unterer Saar	000665		\N	\N	\N		\N	TV	\N	16	\N	Y			\N		\N	article	2016-05-04 11:07:13.672	\N	1978	\N	1822	f
1829	Bettinger_Siegl_1998_000668	\N	\N	\N	\N	\N	Auwälder im Saarland	000668		\N	\N	\N		\N	TV	\N	24	\N	Y			\N		\N	article	2016-05-04 11:07:14.424	\N	1998	\N	1830	f
1835	Krause_Schumacher_1998_000670	\N	\N	\N	\N	\N	Pflanzensoziologische Gliederung der Waldmeister-Buchenwälder (Galio odorati-Fagetum Sougnez & Thill 1959) in der nordrhein-westfälischen Eifel. (Buchenwälder in der nordrhein-westfälischen Eifel, Teil II)	000670	Göttingen	\N	\N	\N		\N	TV	\N	31	\N	Y			\N		\N	article	2016-05-04 11:07:15.002	\N	1998	\N	1836	f
1841	Michiels_1996_000672	\N	\N	\N	\N	\N	Standort und Vegetation einiger ausgewählter Eichen-Naturwaldreservate in Bayern	000672	Eching	\N	\N	\N		\N	\N	\N	106	\N	Y			\N		\N	article	2016-05-04 11:07:15.5	\N	1996	\N	1842	f
1846	Hofmeister_1984_000674	\N	\N	\N	\N	\N	Das Gentiano-Koelerietum Knapp 1942 im Mittelleine-Innerste-Bergland	000674	Braunschweig	\N	\N	\N		\N	\N	\N	57	\N	Y			\N		\N	article	2016-05-04 11:07:16.011	\N	1984	\N	1847	f
1848	Passarge_1984_000675	\N	\N	\N	\N	\N	Montane Frischwiesensäume	000675	Göttingen	\N	PAS	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:16.236	\N	1984	\N	1849	f
1860	Haerdtle_1990_000679	\N	\N	\N	\N	\N	Buchenwälder auf Melgelhängen in Schleswig-Holstein	000679	Göttingen	\N	\N	\N		\N	\N	\N	16	\N	Y			\N		\N	article	2016-05-04 11:07:17.266	\N	1990	\N	1861	f
1858	Passarge_1979_000678	\N	\N	\N	\N	\N	Über vikariierende Trifolio-Geranietea-Gesellschaften in Mitteleuropa	000678	Berlin	\N	\N	\N		\N	\N	\N	89	\N	Y			\N		\N	article	2016-05-04 11:07:17.002	\N	1979	\N	1859	f
1896	Heerde_Mueller_Gnuechtel_2006_000689	\N	\N	\N	\N	\N	Verbreitung, Soziologie und Ökologie von Carex buekii Wimm. in Sachsen	000689	Göttingen	\N	\N	\N		vegetweb	\N	\N	28	\N	Y			\N		\N	article	2016-05-04 11:07:19.714	\N	2006	\N	1897	f
1865	Gehlken_2005_000681	\N	\N	\N	\N	\N	Zur synsystematischen Stellung von Eupatorium cannabinum-Gesellschaften	000681	Göttingen	\N	\N	\N		vegetweb	\N	\N	14	\N	Y			\N		\N	article	2016-05-04 11:07:17.769	\N	2005	\N	1866	f
1871	Hetzel_Fuchs_Keil_Schmitt_2006_000682	\N	\N	\N	\N	\N	Bodensaure Buchenwälder im Übergang vom Bergischen Land zum Niederrheinischen Tiefland	000682	Göttingen	\N	\N	\N		vegetweb	\N	\N	127	\N	Y			\N		\N	article	2016-05-04 11:07:18.021	\N	2006	\N	1872	f
1875	Beer_Ewald_2005_000683	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen rezent streugenutzter Kiefernwälder auf Binnendünen des niederbayerischen Tertiärhügellandes	000683	Göttingen	\N	\N	\N		vegetweb	\N	\N	100	\N	Y			\N		\N	article	2016-05-04 11:07:18.281	\N	2005	\N	1876	f
1844	Straussberger_1999_000673	\N	\N	\N	\N	\N	Untersuchungen zur Entwicklung bayerischer Kiefern-Naturwaldreservate auf nährstoffarmen Standorten	000673	Eching	Auch Diss. Forstwiss. Fak. Universität München	\N	0000000003		\N	\N	\N	106	\N	Y			\N		\N	article	2016-05-04 11:07:15.797	\N	1999	\N	1845	f
1882	Wenz_Dierschke_2006_000685	\N	\N	\N	\N	\N	Helio-thermophile Saumgesellschaften auf Xerothermstandorten des Nahe-Gebietes	000685	Göttingen	\N	\N	\N		vegetweb	\N	\N	148	\N	Y			\N		\N	article	2016-05-04 11:07:18.81	\N	2006	\N	1883	f
1885	Evers_2006_000686	\N	\N	\N	\N	\N	Ephemerenfluren (Sedo-Scleranthetalia) im nördlichen Harzvorland	000686	Göttingen	\N	\N	\N		vegetweb	\N	\N	62	\N	Y			\N		\N	article	2016-05-04 11:07:19.036	\N	2006	\N	1886	f
1892	Wulf_2006_000688	\N	\N	\N	\N	\N	Pflanzengesellschaften des Wirtschaftsgrünlands im Altkreis Schmalkalden (Thüringer Wald/Rhön) und ihre Entwicklung zwischen 1960 und 2000	000688	Göttingen	\N	\N	\N		vegetweb	\N	\N	168	\N	Y			\N		\N	article	2016-05-04 11:07:19.501	\N	2006	\N	1893	f
2399	Bushart_Leupold_1994_000878	\N	\N	\N	\N	\N	Die Sanddünengebiete bei Altdorf	000878	Regensburg	\N	\N	\N		HEINKEN_DIP	\N	\N	28	\N	Y			\N		\N	article	2016-05-04 11:08:04.517	\N	1994	\N	2400	f
1901	Brandes_Nitzsche_2007_000691	\N	\N	\N	\N	\N	Verbreitung, Ökologie und Soziologie von Ambrosia artemisiifolia L. in Mitteleuropa	000691	Göttingen	\N	\N	\N		vegetweb	\N	\N	62	\N	Y			\N		\N	article	2016-05-04 11:07:20.272	\N	2007	\N	1902	f
1910	Ruether_Klotz_2009_000694	\N	\N	\N	\N	\N	Verbreitung, Vergesellschaftung und Ökologie von Lathraea squamaria in Süddeutschland, mit einem Überblick zur Situation in Mitteleuropa	000694	Göttingen	\N	\N	\N		vegetweb	\N	\N	14	\N	Y			\N		\N	article	2016-05-04 11:07:21.024	\N	2009	\N	1911	f
1906	Dierschke_Becker_2008_000693	\N	\N	\N	\N	\N	Die Schwermetall-Vegetation des Harzes - Gliederung, ökologische Bedingungen und syntaxonomische Einordnung	000693	Göttingen	\N	\N	\N		vegetweb	\N	\N	120	\N	Y			\N		\N	article	2016-05-04 11:07:20.802	\N	2008	\N	1907	f
1940	Reif_Durka_W._Hemp_Loeblich-Ille_1988_000703	\N	\N	\N	\N	\N	Die Bärwurz (Meum athamanticum Jacq.) im nördlichen Frankenwald. Ihre Vergesellschaftung, ihre Standorte sowie deren Bewirtschaftung und Erhalt	000703	145 - 209	\N	\N	\N		\N	TV	\N	195	\N	Y			\N		\N	article	2016-05-04 11:07:23.293	\N	1988	\N	1941	f
1862	Becker_2005_000680	\N	\N	\N	\N	\N	Luzula divulgata Kirschner (Schlanke Hainsimse)? Verbreitung, Vergesellschaftung und Standort einer Art xerothermer Eichenwälder in Mitteldeutschland	000680	Göttingen	\N	\N	\N		vegetweb	\N	\N	37	\N	Y			\N		\N	article	2016-12-21 10:15:18.592	\N	2005	\N	1863	f
1913	Partzsch_2009_000695	\N	\N	\N	\N	\N	Populationsstruktur und Vergesellschaftung von Dictamnus albus L. in thermophilen Säumen des unteren Unstruttals (Sachsen-Anhalt)	000695	Göttingen	\N	\N	\N		vegetweb	\N	\N	41	\N	Y			\N		\N	article	2016-05-04 11:07:21.229	\N	2009	\N	1914	f
1919	Passarge_1979_000697	\N	\N	\N	\N	\N	Über azidophile Waldsaumgesellschaften	000697	Berlin	\N	\N	\N		\N	\N	\N	71	\N	Y			\N		\N	article	2016-05-04 11:07:21.762	\N	1979	\N	1920	f
1904	Becker_Braendel_Dierschke_2007_000692	\N	\N	\N	\N	\N	Die Schwermetallrasen und Trockenrasen der Bottendorfer Hügel in Thüringen	000692	Göttingen	\N	\N	\N		vegetweb	\N	\N	206	\N	Y			\N		\N	article	2016-05-04 11:07:20.535	\N	2007	\N	1905	f
1954	Sendtko_1993_000709	\N	\N	\N	\N	\N	Die Flora und Vegetation der Kalkmagerrasen am Schloßberg und Hutberg bei Kallmünz (Landkreis Regensburg)	000709	Regensburg	\N	\N	\N		TAB	\N	\N	174	\N	Y			\N		\N	article	2016-05-04 11:07:24.468	\N	1993	\N	1955	f
1957	Zintl_1987_000710	\N	\N	\N	\N	\N	Floristische und vegetationskundliche Untersuchungen am Schutzfelsen und im NSG "Max-Schultze-Steig" bei Regensburg als Grundlage für den Naturschutz	000710	Regensburg	\N	\N	\N		TAB	\N	\N	61	\N	Y			\N		\N	article	2016-05-04 11:07:24.674	\N	1987	\N	1958	f
1926	Fuellekrug_1967_000699	\N	\N	\N	\N	\N	Die Waldgesellschaften an der Schanze bei Bad Gandersheim und ihre räumliche Gliederung	000699		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:22.314	\N	1967	\N	1927	f
1966	Altehage_1950_000714	\N	\N	\N	\N	\N	Die Vegetation des Weustenteichgebietes bei Emlichheim	000714		\N	\N	\N		TAB	\N	\N	1	\N	Y			\N		\N	article	2016-05-04 11:07:25.725	\N	1950	\N	1967	f
1969	Hettwer_1999_000715	\N	\N	\N	\N	\N	Schatthangwälder und Felsspalten-Gesellschaften auf Jura-Gestein im Alfelder Bergland (Süd-Niedersachsen)	000715	Göttingen	\N	\N	\N		\N	\N	\N	61	\N	Y			\N		\N	article	2016-05-04 11:07:25.973	\N	1999	\N	1970	f
1982	Romahn_1998_000722	\N	\N	\N	\N	\N	Die Vegetation der Kremper und Nordoer Heide. Vegetationskundliche Untersuchungen auf einem Standortsübungsplatz der Bundeswehr	000722	Kiel	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:27.109	\N	1998	\N	1983	f
1985	Rexer_1994_000723	\N	\N	\N	\N	\N	Das Naturschutzgebiet "Schenkenwald" im Kreis Ravensburg. Der größte zusammenhängende Laubwaldkomplex im südlichen Oberschwaben	000723	Karlsruhe	\N	\N	\N		TV	\N	\N	25	\N	Y			\N		\N	article	2016-05-04 11:07:27.357	\N	1994	\N	1986	f
1988	Merz_1993_000724	\N	\N	\N	\N	\N	Untersuchungen zur Vegetationsentwicklung auf Weinbergsbrachen am Gangelsberg bei Duchroth/Landkreis Bad Kreuznach	000724	Bad Dürkheim	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:27.57	\N	1993	\N	1989	f
1994	Sieben_Otte_1992_000726	\N	\N	\N	\N	\N	Nutzungsgeschichte, Vegetation und Erhaltungsmöglichkeiten einer historischen Agrarlandschaft in der südlichen Frankenalb	000726	München	\N	\N	\N		\N	\N	\N	64	\N	Y			\N		\N	article	2016-05-04 11:07:28.001	\N	1992	\N	1995	f
1997	Klocke_1997_000727	\N	\N	\N	\N	\N	Laubwald-Gesellschaften trockenwarmer Standorte im nördlichen Sollingvorland und im Wesertal bei Bodenwerder	000727	Göttingen	\N	\N	0000000006		\N	\N	\N	97	\N	Y			\N		\N	article	2016-05-04 11:07:28.21	\N	1997	\N	1998	f
2003	Scherrer_1925_000729	\N	\N	\N	\N	\N	Vegetationsstudien im Limmatal	000729	Zürich	\N	\N	\N		\N	\N	\N	14	\N	Y			\N		\N	article	2016-05-04 11:07:28.661	\N	1925	\N	2004	f
2011	Fischer_1999_000732	\N	\N	\N	\N	\N	Die Vegetation des Naturschutzgebietes "Weiße Laaber bei Wattersberg" und seiner Umgebung	000732	Regensburg	\N	\N	\N		\N	\N	\N	156	\N	Y			\N		\N	article	2016-05-04 11:07:29.378	\N	1999	\N	2012	f
2017	Klix_Krausch_1958_000734	\N	\N	\N	\N	\N	Das natürliche Vorkommen der Rotbuche in der Niederlausitz. Beitr. z. Flora u. Vegetation Brandenburgs 19	000734	Potsdam	\N	\N	\N		\N	\N	\N	11	\N	Y			\N		\N	article	2016-05-04 11:07:29.921	\N	1958	\N	2018	f
2021	Bergmeier_1990_000736	\N	\N	\N	\N	\N	Eichen-Hainbuchen-Wälder (Carpinion betuli Oberdorfer 1953). In: NOWAK, B. (1990): Beiträge zur Kenntnis hessischer Pflanzengesellschaften. Ergebnisse der Pflanzensoziologischen Sonntagsexkursionen der Hessischen Botanischen Arbeitsgemeinschaft	000736	Frankfurt am Main	\N	\N	\N		\N	TV	\N	16	\N	Y			\N		\N	article	2016-05-04 11:07:30.485	\N	1990	\N	2022	f
2024	Hochhardt_1996_000737	\N	\N	\N	\N	\N	Vegetationskundliche und faunistische Untersuchungen in den Niederwäldern des Mittleren Scharzwaldes unter Berücksichtigung ihrer Bedeutung für den Arten- und Biotopschutz	000737		\N	\N	\N		\N	TV	\N	295	\N	Y			\N		\N	article	2016-05-04 11:07:30.783	\N	1996	\N	2025	f
2030	Butzke_1969_000739	\N	\N	\N	\N	\N	Über die Böden der feuchten Eichen-Hainbuchenwälder im zentralen Teil des westfälischen Münsterlandes (Kernmünsterland)	000739	Krefeld	\N	\N	\N		\N	TV	\N	11	\N	Y			\N		\N	article	2016-05-04 11:07:31.251	\N	1969	\N	2031	f
2035	Walter_2004_000741	\N	\N	\N	\N	\N	Die Wälder des Rothaargebirges und ihre Veränderungen im 20. Jahrhundert	000741	Münster	\N	\N	\N		\N	TV	\N	147	\N	Y			\N		\N	article	2016-05-04 11:07:31.777	\N	2004	\N	2036	f
2037	Ellenberg_1968_000742	\N	\N	\N	\N	\N	Wald- und Feldbau im Knyphauser Wald, einer Heide-Aufforstung in Ostfriesland	000742	Hannover	\N	\N	\N		\N	TV	\N	61	\N	Y			\N		\N	article	2016-05-04 11:07:31.993	\N	1968	\N	2038	f
2044	Speidel_1972_000745	\N	\N	\N	\N	\N	Das Wirtschaftsgrünland der Rhön. Vegetation, Ökologie und landwirtschaftlicher Wert	000745		\N	\N	\N		LITTAB	\N	\N	15	\N	Y			\N		\N	article	2016-05-04 11:07:32.805	\N	1972	\N	2045	f
2047	Matzke_1989_000746	\N	\N	\N	\N	\N	Die Bärwurzwiesen (Meo-Festucetum Bartsch 1940) der West-Eifel	000746	Göttingen	\N	\N	\N		\N	TV	\N	38	\N	Y			\N		\N	article	2016-05-04 11:07:33.103	\N	1989	\N	2048	f
2049	Bergmeier_1987_000747	\N	\N	\N	\N	\N	Magerrasen und Therophytenfluren im NSG "Wacholderheiden bei Niederlemp" (Lahn-Dill-Kreis, Hessen)	000747	Göttingen	\N	\N	\N		\N	TV	\N	32	\N	Y			\N		\N	article	2016-05-04 11:07:33.309	\N	1987	\N	2050	f
2406	Glotz_1961_000881	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen im Neißetal	000881	Leipzig	\N	\N	\N		HEINKEN_DIP	\N	\N	1	\N	Y			\N		\N	article	2016-05-04 11:08:05.235	\N	1961	\N	2407	f
2058	Ellwanger_1998_000750	\N	\N	\N	\N	\N	Waldgesellschaften und thermophile Säume auf Gips und Stinkschiefer im westlichen Harzfelder Holz (Landkreis Nordhausen, Thüringen) - Eine Untersuchung unter besonderer berücksichtigung der Schutzwürdigkeit des Harzfelder Holzes	000750		\N	\N	\N		\N	\N	\N	29	\N	Y			\N		\N	article	2016-05-04 11:07:34.105	\N	1998	\N	2059	f
2062	Koch_Bernhardt_1996_000751	\N	\N	\N	\N	\N	Zur Entwicklung und Pflege von Kalkmagerrasen. Untersuchungen zur Vegetationsentwicklung und zum Samenpotential im Naturschutzgebiet Silberberg, Landkreis Osnabrück	000751		\N	\N	\N		\N	\N	\N	11	\N	Y			\N		\N	article	2016-05-04 11:07:34.351	\N	1996	\N	2063	f
2067	Krausch_1960_000753	\N	\N	\N	\N	\N	Die Vegetationsverhältnisse des Naturschutzgebietes Lutzketal bei Guben. Beiträge zur Flora und Vegetation Brandenburgs 29	000753	Potsdam	\N	\N	\N		\N	\N	\N	31	\N	Y			\N		\N	article	2016-05-04 11:07:34.786	\N	1960	\N	2068	f
2070	Gulski_1985_000754	\N	\N	\N	\N	\N	Landschaftsökologische Untersuchungen im Hellbachtal (Kreis Herzogtum Lauenburg)	000754	Kiel	\N	\N	\N		\N	\N	\N	67	\N	Y			\N		\N	article	2016-05-04 11:07:35.084	\N	1985	\N	2071	f
2086	Ewald_2005_000760	\N	\N	\N	\N	\N	Schlusswaldgesellschaften des Werdenfelser Landes (Bayerische Alpen)	000760	Regensburg	\N	\N	\N		\N	\N	\N	96	\N	Y			\N		\N	article	2016-05-04 11:07:36.543	\N	2005	\N	2087	f
2090	Beinlich_Klein_1995_000761	\N	\N	\N	\N	\N	Kalkmagerrasen und mageres Grünland: bedrohte Biotoptypen der Schwäbischen Alb	000761	Karlsruhe	\N	\N	\N		\N	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:07:36.806	\N	1995	\N	2091	f
2152	Grosse-Brauckmann_Dierssen_1973_000782	\N	\N	\N	\N	\N	Zur historischen und aktuellen Vegetation im Poggenpohlsmoor bei Dötlingen (Oldenburg)	000782	Todenmann, Göttingen	\N	\N	\N		\N	\N	\N	18	\N	Y			\N		\N	article	2016-05-04 11:07:41.771	\N	1973	\N	2153	f
2121	Raaber_1999_000772	\N	\N	\N	\N	\N	Die Waldgesellschaften im unteren Tal der Schwarzen Laber	000772	Regensburg	\N	\N	\N		\N	\N	\N	222	\N	Y			\N		\N	article	2016-05-04 11:07:39.264	\N	1999	\N	2122	f
2135	Braun_1983_000777	\N	\N	\N	\N	\N	Die Pfeifengras-Streuwiesen (Molinion) des Murnauer Mooses und ihre Standortsverhältnisse	000777	München	\N	\N	\N		\N	\N	\N	32	\N	Y			\N		\N	article	2016-05-04 11:07:40.596	\N	1983	\N	2136	f
2140	Doll_1978_000779	\N	\N	\N	\N	\N	Drei bemerkenswerte Seen im südlichen Mecklenburg und ihre Vegetation	000779	Berlin	\N	\N	\N		\N	\N	\N	16	\N	Y			\N		\N	article	2016-05-04 11:07:41.077	\N	1978	\N	2141	f
2143	Wolf_1992_000780	\N	\N	\N	\N	\N	Die Vegetation des Bannwaldes" Wilder See-Hornisgrinde" am Ruhestein, Nordschwarzwald	000780		\N	\N	\N		\N	\N	\N	153	\N	Y			\N		\N	article	2016-05-04 11:07:41.296	\N	1992	\N	2144	f
2148	Linhard_Linhard_Hierlmeier_Linhard_2006_000781	\N	\N	\N	\N	\N	Flora und Vegetation im Gebiet der Erlau mit Anmerkungen zur Fauna	000781	Regensburg	\N	\N	\N		\N	\N	\N	180	\N	Y			\N		\N	article	2016-05-04 11:07:41.518	\N	2006	\N	2149	f
2154	Jeckel_1981_000783	\N	\N	\N	\N	\N	Die Vegetation des Naturschutzgebietes "Breites Moor" (Kreis Celle, Nordwest-Deutschland)	000783	Göttingen	\N	\N	\N		\N	\N	\N	29	\N	Y			\N		\N	article	2016-05-04 11:07:41.978	\N	1981	\N	2155	f
2156	Jeschke_1963_000784	\N	\N	\N	\N	\N	Die Wasser- und Sumpfvegetation im Naturschutzgebiet "Ostufer der Müritz"	000784	Berlin	\N	\N	\N		\N	\N	\N	17	\N	Y			\N		\N	article	2016-05-04 11:07:42.183	\N	1963	\N	2157	f
2159	Kaule_1972_000785	\N	\N	\N	\N	\N	Zum Vorkommen von Carex chordorrhiza Ehrh. in Bayern	000785	München	\N	\N	\N		\N	\N	\N	27	\N	Y			\N		\N	article	2016-05-04 11:07:42.392	\N	1972	\N	2160	f
2162	Kersberg_1968_000786	\N	\N	\N	\N	\N	Die Prümer Kalkmulde (Eifel) und ihre Randgebiete. - Landschaftsökologische und vegetationskundliche Untersuchungen	000786	Recklinghausen	\N	\N	\N		\N	\N	\N	2	\N	Y			\N		\N	article	2016-05-04 11:07:42.622	\N	1968	\N	2163	f
2165	Breunig_1994_000787	\N	\N	\N	\N	\N	Flora und Vegetation der Sandhausener Dünen "Pferdstrieb" und "Pflege Schönau Galgenbuckel"	000787	Karlsruhe	\N	\N	\N		\N	\N	\N	31	\N	Y			\N		\N	article	2016-05-04 11:07:42.879	\N	1994	\N	2166	f
2168	Oberdorfer_1936_000788	\N	\N	\N	\N	\N	Erläuterung zur vegetationskundlichen Karte des Oberrheingebietes bei Bruchsal (Badisches Meßtischblatt Bruchsal 46 z.T. und Teile der angrenzenden Blätter)	000788		\N	\N	\N		\N	\N	\N	18	\N	Y			\N		\N	article	2016-05-04 11:07:43.131	\N	1936	\N	2169	f
2170	Oberdorfer_1936_000789	\N	\N	\N	\N	\N	Bemerkenswerte Pflanzengesellschaften und Pflanzenformen des Oberrheingebietes	000789	Karlsruhe	\N	\N	\N		\N	\N	\N	2	\N	Y			\N		\N	article	2016-05-04 11:07:43.349	\N	1936	\N	2171	f
2172	Oberdorfer_1934_000790	\N	\N	\N	\N	\N	Die höhere Pflanzenwelt am Schluchsee	000790	Freiburg	\N	\N	\N		\N	\N	\N	10	\N	Y			\N		\N	article	2016-05-04 11:07:43.567	\N	1934	\N	2173	f
2182	Lang_1983_000794	\N	\N	\N	\N	\N	Die Vegetation des Mindelseegebietes und ihre Geschichte	000794	Karlsruhe	\N	\N	\N		\N	\N	\N	13	\N	Y			\N		\N	article	2016-05-04 11:07:44.581	\N	1983	\N	2183	f
2185	Langer_1958_000795	\N	\N	\N	\N	\N	Die Vegetationsverhältnisse des Benninger Riedes und ihre Verknüpfung mit der Vegetationsgeschichte des Memminger Tales	000795	Stuttgart	\N	\N	\N		\N	\N	\N	42	\N	Y			\N		\N	article	2016-05-04 11:07:44.787	\N	1958	\N	2186	f
2194	Odzuck_1987_000798	\N	\N	\N	\N	\N	Die Pflanzengesellschaften im Quadranten 8037/1 (Glonn; bayer. Alpenvorland)	000798	Laufen/Salzach	\N	\N	\N		\N	\N	\N	18	\N	Y			\N		\N	article	2016-05-04 11:07:45.419	\N	1987	\N	2195	f
2196	Philippi_1969_000799	\N	\N	\N	\N	\N	Besiedlung alter Ziegeleigruben in der Rheinniederung zwischen Speyer und Mannheim	000799	Todenmann, Rinteln	\N	\N	\N		\N	\N	\N	17	\N	Y			\N		\N	article	2016-05-04 11:07:45.634	\N	1969	\N	2197	f
2129	Brielmaier_Kuenkele_Seitz_1976_000774	\N	\N	\N	\N	\N	Zur Verbreitung von Liparis loeselii (L.) Rich. in Baden-Württemberg	000774	Ludwigsburg	\N	\N	\N		\N	\N	\N	27	\N	Y			\N		\N	article	2016-05-04 11:07:39.788	\N	1976	\N	2130	f
2285	Kaestner_1941_000834	\N	\N	\N	\N	\N	Über einige Waldsumpfgesellschaften, ihre Herauslösung aus den Waldgesellschaften und ihre Neueinordnung	000834	Dresden	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:54.158	\N	1941	\N	2286	f
2224	Fischer_1980_000808	\N	\N	\N	\N	\N	Beitrag zur Gründlandvegetation der Gülper Havelaue	000808	Potsdam	\N	\N	\N		\N	\N	\N	34	\N	Y			\N		\N	article	2016-05-04 11:07:47.956	\N	1980	\N	2225	f
2231	Ellenberg_1952_000811	\N	\N	\N	\N	\N	Auswirkungen der Grundwassersenkung auf die Wiesengesellschaften am Seitenkanal westlich Braunschweig	000811		\N	\N	\N		\N	\N	\N	51	\N	Y			\N		\N	article	2016-05-04 11:07:48.677	\N	1952	\N	2232	f
2239	Pietsch_1984_000814	\N	\N	\N	\N	\N	Das NSG "Saukopfmoor" im Thüringer Wald - Vegetation, Ökologie und Maßnahmen zur Erhaltung	000814	Halle (Saale)	\N	\N	\N		\N	\N	\N	36	\N	Y			\N		\N	article	2016-05-04 11:07:49.401	\N	1984	\N	2240	f
2244	Winterhoff_1976_000816	\N	\N	\N	\N	\N	Ein Fundort des Bachsteinbrechs (Saxifraga aizoides L.) im württembergischen Alpenvorland	000816	Ludwigsburg	\N	\N	\N		\N	\N	\N	12	\N	Y			\N		\N	article	2016-05-04 11:07:49.858	\N	1976	\N	2245	f
2248	Buchwald_1988_000818	\N	\N	\N	\N	\N	Die Bedeutung der Vegetation für die Habitatbindung einiger Libellenarten der Quellmoore und Fließgewässer	000818	Berlin-Stuttgart	\N	\N	\N		\N	\N	\N	99	\N	Y			\N		\N	article	2016-05-04 11:07:50.277	\N	1988	\N	2249	f
2251	Bresinsky_1959_000819	\N	\N	\N	\N	\N	Die Vegetationsverhältnisse der weiteren Umgebung Augsburgs	000819	Augsburg	\N	\N	\N		\N	\N	\N	2	\N	Y			\N		\N	article	2016-05-04 11:07:50.48	\N	1959	\N	2252	f
2253	Doll_1976_000820	\N	\N	\N	\N	\N	Neue Naturschutzgebiete im Kreis Neustrelitz	000820	Greifswald	\N	\N	\N		\N	\N	\N	25	\N	Y			\N		\N	article	2016-05-04 11:07:50.745	\N	1976	\N	2254	f
2259	Krausch_1967_000823	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Stechlinsee-Gebietes III. Grünlandgesellschaften und Sandtrockenrasen	000823	Berlin	\N	\N	\N		\N	\N	\N	15	\N	Y			\N		\N	article	2016-05-04 11:07:51.521	\N	1967	\N	2260	f
2264	Meusel_1960_000825	\N	\N	\N	\N	\N	Verbreitungskarten mitteldeutscher Leitpflanzen. 9. Reihe	000825	Halle (Saale)	\N	\N	\N		\N	\N	\N	1	\N	Y			\N		\N	article	2016-05-04 11:07:51.976	\N	1960	\N	2265	f
2255	Goers_1960_000821	\N	\N	\N	\N	\N	Das Pfrunger Ried - Die Pflanzengesellschaften eines oberschwäbischen Moorgebietes	000821	Ludwigsburg	\N	\N	\N		\N	\N	\N	18	\N	Y			\N		\N	article	2016-05-04 11:07:51.018	\N	1960	\N	2256	f
2268	Oberdorfer_1956_000827	\N	\N	\N	\N	\N	Die Vergesellschaftung der Eissegge (Carex frigida) in alpinen Rieselfluren des Schwarzwaldes, der Alpen und der Pyrenäen	000827	Ludwigsburg	\N	\N	\N		\N	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:07:52.469	\N	1956	\N	2269	f
2273	Seibert_Zielonkowski_1972_000829	\N	\N	\N	\N	\N	Landschaftsplan "Pupplinger und Anholdinger Au"	000829	München	\N	\N	\N		\N	\N	\N	4	\N	Y			\N		\N	article	2016-05-04 11:07:52.938	\N	1972	\N	2274	f
2276	Liepelt_Suck_1984_000830	\N	\N	\N	\N	\N	Das Kalkflachmoor bei Pünzendorf - Bestand, Pflege und Entwicklung	000830	Bamberg	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:53.183	\N	1984	\N	2277	f
2278	Liepelt_1982_000831	\N	\N	\N	\N	\N	Einige bemerkenswerte Pflanzenfunde am Leyer Berg bei Neunkirchen am Brand	000831	München	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:53.432	\N	1982	\N	2279	f
2257	Goers_1964_000822	\N	\N	\N	\N	\N	Beiträge zur Kenntnis basiphiler Flachmoorgesellschaften(Tofieldietalia). Teil 2	000822	Ludwigsburg	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:51.271	\N	1964	\N	2258	f
2280	Fuellekrug_1968_000832	\N	\N	\N	\N	\N	Die Vegetation am Nordosthang des Bulkes bei Seesen	000832	Jena	\N	\N	\N		fehlt noch	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:53.731	\N	1968	\N	2281	f
2287	Libbert_1938_000835	\N	\N	\N	\N	\N	Flora und Vegetation des neumärkischen Plönetales	000835	Berlin-Dahlem	\N	\N	\N		nicht digitalisiert - POLEN	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:54.385	\N	1938	\N	2288	f
2289	Lohmeyer_Krause_1975_000836	\N	\N	\N	\N	\N	Zur Kenntnis der Vegetationskunde des Katzenlochbach-Tales bei Bonn	000836	Bonn-Bad Godesberg	\N	\N	\N		\N	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:07:54.603	\N	1975	\N	2290	f
2291	Meusel_1968_000837	\N	\N	\N	\N	\N	Verbreitungskarten mitteldeutscher Leitpflanzen. 11. Reihe	000837		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:54.828	\N	1968	\N	2292	f
2293	Pietsch_1986_000839	\N	\N	\N	\N	\N	Vegetation, Ökologie und Vorschläge zur Erhaltung des Kalkfkachmoores im NSG "Alperstedter Ried" im Thüringer Becken	000839	Halle (Saale)	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:55.08	\N	1986	\N	2294	f
2296	Wagner_2004_000840	\N	\N	\N	\N	\N	Die Wiesen an den Keuperhängen bei Tübingen	000840	Rottenburg/Neckar; 165 S	\N	\N	\N		\N	\N	\N	39	\N	Y			\N		\N	article	2016-05-04 11:07:55.328	\N	2004	\N	2297	f
2299	Hakes_1991_000841	\N	\N	\N	\N	\N	Das Galio odorati-Fagenion im Habichtswald bei Kassel - Untersuchungen zur ökologischen Feingliederung	000841	Göttingen	\N	\N	\N		\N	\N	\N	102	\N	Y			\N		\N	article	2016-05-04 11:07:55.574	\N	1991	\N	2300	f
2308	Philippi_1975_000845	\N	\N	\N	\N	\N	Quellflurgesellschaften der Allgäuer Alpen	000845	Karlsruhe	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:56.469	\N	1975	\N	2309	f
2313	Sebald_1975_000847	\N	\N	\N	\N	\N	Zur Kenntnis der Quellfluren und Waldsümpfe des Schwäbisch-Fränkischen Waldes	000847	Karlsruhe	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:56.923	\N	1975	\N	2314	f
2323	Moeller_1979_000851	\N	\N	\N	\N	\N	Das Chrysosplenio oppositifolii-Alnetum glutinosae (Meij. Drees 1936), eine neue Alno-Padion-Assoziation	000851	Göttingen	\N	\N	\N		\N	\N	\N	33	\N	Y			\N		\N	article	2016-05-04 11:07:57.91	\N	1979	\N	2324	f
2355	Rosskamp_1993_000863	\N	\N	\N	\N	\N	Die Grünlandvegetation der ostfriesischen Insel Wangeroge	000863	Göttingen	\N	\N	\N		\N	\N	\N	403	\N	Y			\N		\N	article	2016-05-04 11:08:00.839	\N	1993	\N	2356	f
2315	Schlueter_1966_000848	\N	\N	\N	\N	\N	Vegetationsgliederung und -kartierung eines Quellgebietes im Thüringer Wald als Grundlage zu Beurteilung des Wasserhaushaltes	000848	Jena	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:57.177	\N	1966	\N	2316	f
2317	Tuexen_Diemont_1936_000849	\N	\N	\N	\N	\N	Weitere Beiträge zum Klimaxproblem des westeuropäischen Festlandes	000849	Osnabrück	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:57.432	\N	1936	\N	2318	f
2340	Reif_Leonhardt_1991_000857	\N	\N	\N	\N	\N	Die Wald- und Forstgesellschaften im Fichtelgebirge	000857	Regensburg	\N	\N	\N		\N	\N	\N	214	\N	Y			\N		\N	article	2016-05-04 11:07:59.437	\N	1991	\N	2341	f
2348	Wilmanns_1988_000860	\N	\N	\N	\N	\N	Können Trockenrasen derzeit trotz Immissionen überleben?-Eine kritische Analyse des Xerobrometum im Kaiserstuhl	000860	Karlsruhe	\N	\N	\N		\N	\N	\N	30	\N	Y			\N		\N	article	2016-05-04 11:08:00.096	\N	1988	\N	2349	f
2358	Spranger_Tuerk_1993_000864	\N	\N	\N	\N	\N	Die Halbtrockenrasen (Mesobromion erecti Br.-Bl. et Moor 1938) der Muschelkalkstandorte NW-Oberfrankens im Rahmen ihrer Kontak- und Folgegesellschaften	000864	Göttingen	\N	\N	\N		\N	\N	\N	270	\N	Y			\N		\N	article	2016-05-04 11:08:01.058	\N	1993	\N	2359	f
2363	Braun-Blanquet_1931_000866	\N	\N	\N	\N	\N	Zur Vegetation der oberrheinischen Kalkhügel	000866	Neudamm	\N	\N	\N		\N	\N	\N	4	\N	Y			\N		\N	article	2016-05-04 11:08:01.53	\N	1931	\N	2364	f
2375	Mueller_1956_000870	\N	\N	\N	\N	\N	Über die Bodenwasser-Bewegung unter einigen Grünland-Gesellschaften des mittleren Wesertales und seiner Randgebiete	000870		\N	\N	\N		\N	\N	\N	24	\N	Y			\N		\N	article	2016-05-04 11:08:02.531	\N	1956	\N	2376	f
2369	Buck-feucht_1986_000868	\N	\N	\N	\N	\N	Vergleich alter und neuer Waldvegetationsaufnahmenim Forstbezirk Kirchheim unter Teck	000868	Freiburg	\N	\N	\N		\N	\N	\N	60	\N	Y			\N		\N	article	2016-05-04 11:08:02.006	\N	1986	\N	2370	f
2372	Haass_1964_000869	\N	\N	\N	\N	\N	Die Vegetations- und Standortsverhältnisse im Bereich der Gemarkung Poris-Lengefeld (Stadt Gera)	000869	Halle (Saale)	\N	\N	\N		\N	\N	\N	13	\N	Y			\N		\N	article	2016-05-04 11:08:02.241	\N	1964	\N	2373	f
2380	Hoppe_2002_000872	\N	\N	\N	\N	\N	Die Bewässerungswiesen Nordwestdeutschlands - Geschichte, Wandk und heutige Situation	000872	Münster	\N	TV	\N		\N	\N	\N	42	\N	Y			\N		\N	article	2016-05-04 11:08:03.022	\N	2002	\N	2381	f
2386	Augustin_1991_000874	\N	\N	\N	\N	\N	Die Waldgesellschaften des Oberpfälzer Waldes	000874	Regensburg	\N	\N	\N		HEINKEN_DIP	\N	\N	43	\N	Y			\N		\N	article	2016-05-04 11:08:03.574	\N	1991	\N	2387	f
2388	Bohn_Lohmeyer_1999_000875	\N	\N	\N	\N	\N	Wälder, Mantel- und Saumgesellschaften auf Blockhalden und in deren Kontaktbereich	000875	Bonn	Aufn. überwiegend aus: LOHMEYER, W. & BOHN, U. (1972): Karpatenbirkenwälder als kennzeichnende Gehölzgesellschaften d. Hohen Rhön u. ihre Schutzwürdigkeit. - Natur u. Landschaft 47: 196-200.	\N	0000000007		HEINKEN_DIP	\N	\N	18	\N	Y			\N		\N	article	2016-05-04 11:08:03.842	\N	1999	\N	2389	f
2391	Boiselle_Oberdorfer_1957_000876	\N	\N	\N	\N	\N	Der Pfälzer Wald, ein natürliches Verbreitungsgebiet der Kiefer	000876		\N	\N	\N		HEINKEN_DIP	\N	\N	15	\N	Y			\N		\N	article	2016-05-04 11:08:04.062	\N	1957	\N	2392	f
2395	Brunner_Lindacher_1994_000877	\N	\N	\N	\N	\N	Flechtenreiche Kiefernwälder des Nürnberger Reichswaldes	000877	Regensburg	\N	\N	\N		HEINKEN_DIP	\N	\N	46	\N	Y			\N		\N	article	2016-05-04 11:08:04.267	\N	1994	\N	2396	f
2404	Gaisberg_1996_000880	\N	\N	\N	\N	\N	Naturnahe Waldgesellschaften am Hohen Bogen im nördlichen Bayerischen Wald	000880	Regensburg	\N	\N	\N		HEINKEN_DIP	\N	\N	12	\N	Y			\N		\N	article	2016-05-04 11:08:05.024	\N	1996	\N	2405	f
2418	Horn_1997_000885	\N	\N	\N	\N	\N	Verbreitung, Ökologie und Gefährdung der Flachbärlappe (Diphasiastrum spp., Lycopodiaceae, Pteridophyta) in Niedersachsen und Bremen	000885		\N	\N	\N		HEINKEN_DIP	\N	\N	5	\N	Y			\N		\N	article	2016-05-04 11:08:06.175	\N	1997	\N	2419	f
2421	Hueck_1931_000886	\N	\N	\N	\N	\N	Erläuterungen zur vegetationskundlichen Karte des Endmoränengebietes von Chorin (Uckermark)	000886	Neudamm	\N	\N	\N		\N	\N	\N	5	\N	Y			\N		\N	article	2016-05-04 11:08:06.435	\N	1931	\N	2422	f
2426	Klemm_1969_000888	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des nordöstlichen Unterspreewald-Randgebietes. 1. Teil	000888	Berlin-Dahlem	\N	\N	\N		HEINKEN_DIP	\N	\N	195	\N	Y			\N		\N	article	2016-05-04 11:08:06.953	\N	1969	\N	2427	f
2428	Klemm_1997_000889	\N	\N	\N	\N	\N	Die Wuhlheide in Berlin-Köpenick	000889		\N	\N	\N		HEINKEN_DIP	\N	\N	23	\N	Y			\N		\N	article	2016-05-04 11:08:07.205	\N	1997	\N	2429	f
2430	Korneck_1987_000890	\N	\N	\N	\N	\N	Pflanzengesellschaften des Mainzer-Sand-Gebietes	000890		\N	\N	\N		HEINKEN_DIP	\N	\N	44	\N	Y			\N		\N	article	2016-05-04 11:08:07.465	\N	1987	\N	2431	f
2432	Krausch_1962_000891	\N	\N	\N	\N	\N	Der Sandnelken-Kiefrnwald an seiner Westgrenze in Brandenburg	000891		\N	\N	\N		HEINKEN_DIP	\N	\N	6	\N	Y			\N		\N	article	2016-05-04 11:08:07.677	\N	1962	\N	2433	f
2413	Grosser_1956_000883	\N	\N	\N	\N	\N	Landschaftsbild und Heidevegetation in der Lüneburger und in der Lausitzer Heide	000883	Leipzig	\N	\N	\N		HEINKEN_DIP	\N	\N	12	\N	Y			\N		\N	article	2016-05-04 11:08:05.676	\N	1956	\N	2414	f
2445	Marstaller_1985_000895	\N	\N	\N	\N	\N	Die Waldgesellschaften des Ostthüringer Buntsandsteingebietes. Teil 5	000895	Jena	\N	\N	\N		HEINKEN_DIP	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:08:08.695	\N	1985	\N	2446	f
2447	Merkel_1994_000896	\N	\N	\N	\N	\N	Schneeheide-Kiefernwälder in Oberfranken	000896	Regensburg	\N	\N	\N		HEINKEN_DIP	\N	\N	28	\N	Y			\N		\N	article	2016-05-04 11:08:08.911	\N	1994	\N	2448	f
2449	Passarge_1955_000897	\N	\N	\N	\N	\N	Die Ufervegetation des Briesener Sees	000897		\N	\N	\N		HEINKEN_DIP	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:08:09.175	\N	1955	\N	2450	f
2451	Philippi_1970_000898	\N	\N	\N	\N	\N	Die Kiefernwälder der Schwetzinger Hardt (nordbadische Rheinebene)	000898	Ludwigsburg	\N	\N	\N		HEINKEN_DIP	\N	\N	94	\N	Y			\N		\N	article	2016-05-04 11:08:09.406	\N	1970	\N	2452	f
2486	Koch_1928_000911	\N	\N	\N	\N	\N	Die höhere Vegetation der subalpinen Seen und Moorgebiete ders Val Piora (St. Gotthard-Massiv)	000911		\N	\N	0000000009		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:08:12.637	\N	1928	\N	2487	f
2501	Tuexen_Oberdorfer_1958_000916	\N	\N	\N	\N	\N	Die Pflanzenwelt Spaniens, II. Teil: Eurosibirische Phanerogamen-Gesellschaften Spaniens	000916		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:08:13.776	\N	1958	\N	2502	f
2516	Ewald_Fischer_1993_000922	\N	\N	\N	\N	\N	Montane und hochmontane Waldgesellschaften am nördlichen Abfall der Benediktenwand (Bayerische Kalkalpen)	000922	Regensburg	\N	\N	\N		\N	\N	\N	154	\N	Y			\N		\N	article	2016-05-04 11:08:15.209	\N	1993	\N	2517	f
2522	Frankl_2001_000924	\N	\N	\N	\N	\N	Die Bergkiefer (Pinus mugo Turra) in den Tannheimer Bergen - ein Beitrag zur Kenntnis nordalpiner Latschengebüsche	000924	München	\N	\N	\N		\N	\N	\N	110	\N	Y			\N		\N	article	2016-05-04 11:08:15.711	\N	2001	\N	2523	f
2527	Hartmann_1956_000926	\N	\N	\N	\N	\N	Waldgesellschaften der Bergmischwaldstufe aus dem bayerischen Randalpengebiet in ihrem Aufbau und in ihrer waldbaulichen Bedeutung	000926		\N	\N	\N		\N	\N	\N	28	\N	Y			\N		\N	article	2016-05-04 11:08:16.259	\N	1956	\N	2528	f
2543	Knapp_1954_000932	\N	\N	\N	\N	\N	Über die subalpinen Buchenmischwälder in den nördlichen Ostalpen	000932	München	\N	\N	\N		\N	\N	\N	9	\N	Y			\N		\N	article	2016-05-04 11:08:17.607	\N	1954	\N	2544	f
2592	Schrautzer_Jansen_Breuer_Nelle_2009_000951	\N	\N	\N	\N	\N	Succession and management of calcareous dry grasslands in the Northern Franconian Jura, Germany	000951	Göttingen	\N	\N	\N		vegetweb	\N	\N	70	\N	Y			\N		\N	article	2016-05-04 11:08:21.969	\N	2009	\N	2593	f
2508	Duering_Wierer_1995_000918	\N	\N	\N	\N	\N	Die subalpine und alpine Vegetation der Soierngruppe im Naturschutzgebiet Karwendelgebirge	000918	Regensburg	\N	\N	\N		\N	\N	\N	67	\N	Y			\N		\N	article	2016-05-04 11:08:14.227	\N	1995	\N	2509	f
2626	Hinterlang_1992_000965	\N	\N	\N	\N	\N	Vegetationsökologie der Weichwasserquellgesellschaften zentraleuropäischer Mittelgebirgen	000965	Solingen	\N	\N	\N		\N	\N	\N	330	\N	Y			\N		\N	article	2016-05-04 11:08:24.73	\N	1992	\N	2627	f
2700	Stoecker_1965_000991	\N	\N	\N	\N	\N	Die Reliktvorkommen der Kiefer am nördlichen Harzrand	000991		\N	\N	\N		HEINKEN_DIP	\N	\N	12	\N	Y			\N		\N	article	2016-05-04 11:08:31.027	\N	1965	\N	2701	f
2683	Heinken_Zippel_1999_000984	\N	\N	\N	\N	\N	Die Sand-Kiefernwälder (Dicrano-Pinion) im norddeutschen Tiefland: syntaxonomische, standörtliche und geographische Gliederung	000984	Göttingen	\N	\N	\N		\N	\N	\N	437	\N	Y			\N		\N	article	2016-05-04 11:08:29.184	\N	1999	\N	2684	f
2685	Passarge_1956_000985	\N	\N	\N	\N	\N	Die Wälder des Oberspreewaldes	000985	Berlin	\N	\N	\N		HEINKEN_DIP	\N	\N	15	\N	Y			\N		\N	article	2016-05-04 11:08:29.429	\N	1956	\N	2686	f
2687	Rodi_1975_000986	\N	\N	\N	\N	\N	Die Vegetation des nordwestlichen Tertiär-Hügellandes (Oberbayern)	000986	Bonn-Bad Godesberg	\N	\N	\N		\N	\N	\N	67	\N	Y			\N		\N	article	2016-05-04 11:08:29.688	\N	1975	\N	2688	f
2690	Scheuerer_1993_000987	\N	\N	\N	\N	\N	Cladonia stellaris am Bayrischen Pfahl - ein Beitrag zur Kenntnis autochtoner Kiefernwälder	000987	Regensburg	\N	\N	\N		HEINKEN_DIP	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:08:29.988	\N	1993	\N	2691	f
2698	Stiersdorfer_1996_000990	\N	\N	\N	\N	\N	Naturnahe Waldgesellschaften im Bayerischen Wald zwischen Schwarzem Regen und Arber-Kaitersbergzug	000990	Regensburg	\N	\N	\N		\N	\N	\N	12	\N	Y			\N		\N	article	2016-05-04 11:08:30.775	\N	1996	\N	2699	f
2703	Targan_1994_000992	\N	\N	\N	\N	\N	Wald- und Forstgesellschaften der Unteren Mark und der Adelsdorfer Mark (westlich Forchheim/Oberfranken)	000992	Regensburg	\N	\N	\N		\N	\N	\N	45	\N	Y			\N		\N	article	2016-05-04 11:08:31.306	\N	1994	\N	2704	f
2711	Wolf_1993_000995	\N	\N	\N	\N	\N	Die Missen im Landkreis Calw	000995	Karlsruhe	\N	\N	\N		HEINKEN_DIP	\N	\N	40	\N	Y			\N		\N	article	2016-05-04 11:08:32.12	\N	1993	\N	2712	f
2713	Wolf_Winterhoff_2001_000996	\N	\N	\N	\N	\N	Die Vegetation der Bannwälder "Franzosenbusch" und "Kartoffelacker"	000996		\N	\N	\N		HEINKEN_DIP	\N	\N	7	\N	Y			die namen		\N	article	2016-05-04 11:08:32.378	\N	2001	\N	2714	f
2715	Zerbe_1999_000997	\N	\N	\N	\N	\N	Die Wald- und Forstgesellschaften des Spessarts mit Vorschlägen zu deren zukünftigen Entwicklung	000997		\N	\N	\N		HEINKEN_DIP	\N	\N	56	\N	Y			\N		\N	article	2016-05-04 11:08:32.66	\N	1999	\N	2716	f
2784	Ruehl_1967_001023	\N	\N	\N	\N	\N	Das Hessische Bergland. Eine forstlich-vegetationsgeographische Übersicht	001023	Bonn-Bad Godesberg	\N	\N	\N		\N	\N	\N	24	\N	Y			\N		\N	article	2016-05-04 11:08:38.732	\N	1967	\N	2785	f
2736	Heinken_1999_001006	\N	\N	\N	\N	\N	Sand-Kiefernwälder in Mittelbrandenburg: Vegetationskomplex und Waldsukzession am Beispiel der Glauer Berge	001006	Berlin	\N	\N	\N		HEINKEN_DIP	\N	\N	66	\N	N			\N		\N	article	2016-05-04 11:08:34.808	\N	1999	\N	2737	f
2746	Frede_1993_001010	\N	\N	\N	\N	\N	Die Felsvegetation des Naturdenkmals am Kahlen-Berg bei Adorf (Nordhessen)	001010	Bad Wildungen	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:08:35.788	\N	1993	\N	2747	f
2755	Hilbig_1971_001013	\N	\N	\N	\N	\N	Kalkschuttgesellschaften in Thüringen	001013	Leipzig	\N	\N	\N		\N	\N	\N	20	\N	Y			\N		\N	article	2016-05-04 11:08:36.452	\N	1971	\N	2756	f
2782	Rauschert_1990_001022	\N	\N	\N	\N	\N	Übersicht über die Pflanzengesellschaften des südlichen Teiles der DDR - XV. Die xerothermen Gebüschgesellschaften (Berberidion Br.-Bl. 52 und Prunion fruticosae Tx. 52)	001022	Leipzig	\N	\N	\N		\N	\N	\N	16	\N	Y			\N		\N	article	2016-05-04 11:08:38.527	\N	1990	\N	2783	f
2796	Burrichter_1953_001029	\N	\N	\N	\N	\N	Die Wälder des Meßtischblattes Iburg, Teutoburger Wald	001029	Münster	\N	\N	\N		HEINKEN	\N	\N	13	\N	Y			\N		\N	article	2016-05-04 11:08:40.238	\N	1953	\N	2797	f
2802	Dierschke_1979_001031	\N	\N	\N	\N	\N	Laubwald-Gesellschaften im Bereich der unteren Aller und Leine (Nordwest-Deutschland)	001031	Lille	\N	\N	\N		\N	\N	\N	40	\N	Y			\N		\N	article	2016-05-04 11:08:40.737	\N	1979	\N	2803	f
2788	Schlueter_1987_001025	\N	\N	\N	\N	\N	Zur Vegetationsökologie von Xerothermkomplexen im buchenreichen Muschelkalk-Bergland Mittelthüringens	001025		\N	\N	\N		\N	\N	\N	37	\N	Y			\N		\N	article	2016-05-04 11:08:39.187	\N	1987	\N	2789	f
2810	Eskuche_1978_001034	\N	\N	\N	\N	\N	Bodenwasserhaushalt von Wäldern in der Westfälischen Bucht und im Wesergebiet nördlich von Minden	001034	Krefeld	\N	\N	\N		\N	\N	\N	14	\N	Y			\N		\N	article	2016-05-04 11:08:41.421	\N	1978	\N	2811	f
2821	Haerdtle_1989_001038	\N	\N	\N	\N	\N	Potentielle natürliche Vegetation. Ein Beitrag zur Kartierungsmethode am Beispiel der Topographischen Karte 1623 Owschlag	001038	Kiel	\N	\N	\N		\N	\N	\N	6	\N	Y			\N		\N	article	2016-05-04 11:08:42.425	\N	1989	\N	2822	f
2815	Foerster_1981_001036	\N	\N	\N	\N	\N	Waldgesellschaften der Bückeberge	001036	Göttingen	\N	\N	\N		\N	\N	\N	83	\N	Y			\N		\N	article	2016-05-04 11:08:41.912	\N	1981	\N	2816	f
2830	Heinken_2007_001042	\N	\N	\N	\N	\N	Vegetation und Standort bodensaurer Buchenwälder am Arealrand - am Beispiel Mittelbrandenburgs	001042		\N	\N	\N		\N	\N	\N	56	\N	Y			\N		\N	article	2016-05-04 11:08:43.413	\N	2007	\N	2831	f
2832	Konczak_1999_001043	\N	\N	\N	\N	\N	Die Flaum-Eiche in Ostbrandenburg	001043		\N	\N	\N		\N	\N	\N	2	\N	Y			\N		\N	article	2016-05-04 11:08:43.624	\N	1999	\N	2833	f
2734	Braun_1978_001005	\N	\N	\N	\N	\N	Die Pflanzendecke. In: Bodenkarte von Bayern 1:25.000. Erläuterungen zum Kartenblatt Nr. 7644 Triftern	001005	München	\N	\N	\N	Booktitle	\N	\N	\N	3	\N	N	In: Bodenkarte von Bayern 1:25.000. Erläuterungen zum Kartenblatt Nr. 7644 Triftern: 53-71. München		\N		\N	book	2016-09-22 11:34:40.417	\N	1978	\N	2735	f
2728	Braun_1973_001002	\N	\N	\N	\N	\N	Die Pflanzendecke. In: Bodenkarte von Bayern 1:25.000. Erläuterungen zum Blatt Nr.6434 Hersbruck	001002	München	\N	\N	\N	Booktitle	\N	\N	\N	8	\N	N	In: Bodenkarte von Bayern 1:25.000. Erläuterungen zum Blatt Nr.6434 Hersbruck: 46-62. München		\N		\N	book	2016-09-22 11:34:06.035	\N	1973	\N	2729	f
2850	Nawrath_1995_001048	\N	\N	\N	\N	\N	Feuchtgebiete der Umgebung von Bad Homburg vor der Höhe. Floristische und vegetationskundliche Untersuchung unter besonderer Berücksichtigung der Feuchtwiesen	001048		\N	\N	\N		\N	\N	\N	191	\N	Y			\N		\N	article	2016-05-04 11:08:44.853	\N	1995	\N	2851	f
2852	Scheuerer_1993_001049	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen am Scheuchenberg (Landkreis Regensburg) als Grundlage für den Naturschutz	001049	Regensburg	\N	\N	\N		\N	\N	\N	177	\N	Y			\N		\N	article	2016-05-04 11:08:45.139	\N	1993	\N	2853	f
2823	Haerdtle_1995_001039	\N	\N	\N	\N	\N	Vegetation und Standort der Laubwaldgesellschaften (Querco-Fagetea) im nördlichen Schleswig-Holstein	001039	Kiel	\N	\N	\N		\N	\N	\N	138	\N	Y			\N		\N	article	2016-05-04 11:08:42.697	\N	1995	\N	2824	f
2818	Von_Glahn_1981_001037	\N	\N	\N	\N	\N	Über den Flattergras- oder Sauerklee-Buchenwald (Oxali-Fagetum) der niedersächsischen und holsteinischen Moränenlandschaften	001037	Oldenburg	\N	\N	\N		\N	\N	\N	42	\N	Y			\N		\N	article	2016-05-04 11:08:42.124	\N	1981	\N	2819	f
2988	Ruether_2003_001094	\N	\N	\N	\N	\N	Die Waldgesellschaften des Vorderen Bayerischen Waldes, mit einem Beitrag zur jüngeren Waldgeschichte	001094	Regensburg	\N	\N	\N		\N	\N	\N	711	\N	Y			\N		\N	article	2016-05-04 11:08:56.998	\N	2003	\N	2989	f
3078	Sommer_1989_001125	\N	\N	\N	\N	\N	Die Waldgesellschaften im Bereich der geplanten A 33 zwischen Steinhagen und Halle	001125	Bielefeld	\N	\N	\N		HEINKEN2	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:09:05.04	\N	1989	\N	3079	f
3001	Boecking_2004_001098	\N	\N	\N	\N	\N	Vegetation und Flora des Bannwaldes "Conventwald"	001098		\N	\N	0000000010		\N	\N	\N	34	\N	Y			\N		\N	article	2016-05-04 11:08:58.065	\N	2004	\N	3002	f
2990	Zielonkowski_2005_001095	\N	\N	\N	\N	\N	Ein Beitrag zur vegetationskundlichen Dokumentation der oberbayerischen Osterseen	001095	Regensburg	\N	\N	\N		\N	\N	\N	154	\N	Y			\N		\N	article	2016-05-04 11:08:57.243	\N	2005	\N	2991	f
2995	Karrer_Mies_Ltsch_1997_001096	\N	\N	\N	\N	\N	Boden- und vegetationskundliche Untersuchungen auf Acker- und Grünlandbrachen in Solingen (Bergisches Land): Nährstoffdynamik und Vegetationsmosaik	001096	Bonn	\N	\N	\N		\N	\N	\N	15	\N	Y			\N		\N	article	2016-05-04 11:08:57.562	\N	1997	\N	2996	f
2904	Sperle_2004_001066	\N	\N	\N	\N	\N	Grunddatenerhebung Jagsttal bei Krautheim	001066		\N	\N	\N		\N	\N	\N	10	\N	N			\N		\N	report	2016-05-04 11:08:49.544	\N	2004	2905	\N	f
2998	Giegerich_Wittig_2007_001097	\N	\N	\N	\N	\N	Die Sandtrockenrasen des Gailenbergs und ihre Vegetationsdynamik (Landkreis Offenbach, Stadt Mühlheim a. Main)	001097	Frankfurt am Main	\N	\N	\N		\N	\N	\N	57	\N	Y			\N		\N	article	2016-05-04 11:08:57.82	\N	2007	\N	2999	f
3003	Peppler-lisbach_2009_001099	\N	\N	\N	\N	\N	Standortökologische Untersuchungen in Laubwäldern des Oldenburger Raumes	001099		\N	\N	\N		\N	\N	\N	49	\N	Y			\N		\N	article	2016-05-04 11:08:58.323	\N	2009	\N	3004	f
3014	Kleinsteuber_1992_001103	\N	\N	\N	\N	\N	Die Bärwurz (Meum athamanticum JACQ.) im Nordschwarzwald	001103	Karlsruhe	\N	\N	\N		\N	\N	\N	22	\N	Y			\N		\N	article	2016-05-04 11:08:59.328	\N	1992	\N	3015	f
3017	Peintinger_1990_001104	\N	\N	\N	\N	\N	Bestandsschwankungen bei seltenen Pflanzenarten in Pfeifengraswiesen des westlichen Bodenseegebietes	001104	Karlsruhe	\N	\N	\N		\N	\N	\N	11	\N	Y			\N		\N	article	2016-05-04 11:08:59.578	\N	1990	\N	3018	f
3019	Schwabe_1990_001105	\N	\N	\N	\N	\N	Syndynamische Prozesse in Borstgrasrasen: Reaktionsmuster von Brachen nach erneuter Rinderbeweidung und Lebensrhythmus von Arnica montana L.	001105	Karlsruhe	\N	\N	\N		\N	\N	\N	33	\N	Y			\N		\N	article	2016-05-04 11:08:59.824	\N	1990	\N	3020	f
3022	Redecker_2001_001106	\N	\N	\N	\N	\N	Schutzwürdigkeit und Schutzperspektive der Stromtal-Wiesen an der unteren Mittelelbe. Ein vegetationskundlicher Beitrag zur Leitbildentwicklung	001106	Nümbrecht	\N	\N	\N		\N	\N	\N	246	\N	Y			\N		\N	article	2016-05-04 11:09:00.028	\N	2001	\N	3023	f
3024	Hemp_1999_001107	\N	\N	\N	\N	\N	Die Vegetation offener Kalkschutthalden der Nördlichen Frankenalb	001107	Göttingen	\N	\N	\N		TV	\N	\N	218	\N	Y			\N		\N	article	2016-05-04 11:09:00.321	\N	1999	\N	3025	f
3038	Holst_Kintzel_2001_001111	\N	\N	\N	\N	\N	Vorkommen und Vergesellschaftung des Heilziest (Betonica officinalis L.) im Landkreis Parchim	001111		\N	\N	\N		TV	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:01.299	\N	2001	\N	3039	f
3044	Kriebitzsch_1978_001113	\N	\N	\N	\N	\N	Stickstoffnachlieferung in sauren Waldböden Nordwestdeutschlands	001113	Göttingen	\N	\N	\N		HEINKEN1	\N	\N	32	\N	Y			\N		\N	article	2016-05-04 11:09:01.868	\N	1978	\N	3045	f
3052	Lohmeyer_Zezschwitz_1982_001116	\N	\N	\N	\N	\N	Einfluß von Reliefform und Exposition auf Vegetation, Humusform und Humusqualität	001116	Hannover	\N	\N	\N		HEINKEN2	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:09:02.597	\N	1982	\N	3053	f
3055	Meisel-jahn_1955_001117	\N	\N	\N	\N	\N	Die pflanzensoziologische Stellung der Hauberge des Siegerlandes	001117	Stolzenau	\N	\N	\N		HEINKEN2	\N	\N	56	\N	Y			\N		\N	article	2016-05-04 11:09:02.822	\N	1955	\N	3056	f
3041	Knoerzer_1957_001112	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der Wälder im nördlichen Rheinland zwischen Niers und Nieder-Rhein und experimentelle Untersuchungen über den Einfluß einiger Baumarten auf ihre Krautschicht	001112	Köln	\N	\N	\N		HEINKEN1	\N	\N	34	\N	Y			\N		\N	article	2016-05-04 11:09:01.535	\N	1957	\N	3042	f
3061	Pott_1982_001119	\N	\N	\N	\N	\N	Das Naturschutzgebiet "Hiddeser  Bent - Donoper Teich" in vegetationsgeschichtler und pflanzensoziologischer Sicht	001119	Münster	\N	\N	\N		HEINKEN2	\N	\N	30	\N	Y			\N		\N	article	2016-05-04 11:09:03.285	\N	1982	\N	3062	f
3063	Pott_1990_001120	\N	\N	\N	\N	\N	Die nacheiszeitliche Ausbreitung und pflanzensoziologische Stellung von Ilex aquifolium	001120	Göttingen	\N	\N	\N		HEINKEN2	\N	\N	29	\N	Y			\N		\N	article	2016-05-04 11:09:03.54	\N	1990	\N	3064	f
3081	Taux_1981_001126	\N	\N	\N	\N	\N	Wald- und Forstgesellschaften des Rasteder Geestrandes	001126	Oldenburg	\N	\N	\N		HEINKEN2	\N	\N	53	\N	Y			\N		\N	article	2016-05-04 11:09:05.334	\N	1981	\N	3082	f
3111	Dietrich_null_001137	\N	\N	\N	\N	\N	?	001137		\N	\N	unpubliziert		LAU_ST	\N	\N	62	\N	N	\N		\N		\N	unpublished	2016-05-04 11:09:07.892	\N	1500	\N	\N	t
3059	Mueller-stoll_Krausch_1968_001118	\N	\N	\N	\N	\N	Der acidophile Kiefern-Traubeneichenwald und seine Kontaktgesellschaften in Mittel-Brandenburg	001118	Todenmann	\N	\N	\N		HEINKEN2	\N	\N	86	\N	Y			\N		\N	article	2016-05-04 11:09:03.036	\N	1968	\N	3060	f
3083	Tuexen_1967_001127	\N	\N	\N	\N	\N	Die Lüneburger Heide	001127	Rotenburg/Wümme	\N	\N	\N		HEINKEN2	\N	\N	15	\N	Y			\N		\N	article	2016-05-04 11:09:05.589	\N	1967	\N	3084	f
3161	Meierott_1990_001155	\N	\N	\N	\N	\N	Die Linum perenne-Gruppe in Nordbayern	001155	Göttingen	\N	\N	\N		\N	\N	\N	13	\N	Y			\N		\N	article	2016-05-04 11:09:12.501	\N	1990	\N	3162	f
3163	Ullmann_1990_001156	\N	\N	\N	\N	\N	Naturräumliche Gliederung der Vegetation auf Straßenbegleitflächen im westlichen Unterfranken	001156	Göttingen	\N	\N	\N		\N	\N	\N	141	\N	Y			\N		\N	article	2016-05-04 11:09:12.716	\N	1990	\N	3164	f
3170	Suck_Suck_1982_001158	\N	\N	\N	\N	\N	Pflanzengesellschaften des Friesener Albtraufs bei Bamberg	001158	Bamberg	\N	\N	\N		\N	\N	\N	4	\N	Y			\N		\N	article	2016-05-04 11:09:13.267	\N	1982	\N	3171	f
3172	Zielonkowski_1972_001159	\N	\N	\N	\N	\N	Formenkreis, Verbreitung und Vergesellschaftung der Festuca ovina im Raum Regensburg	001159	Regensburg	\N	\N	\N		\N	\N	\N	82	\N	Y			\N		\N	article	2016-05-04 11:09:13.522	\N	1972	\N	3173	f
3177	Springer_1996_001161	\N	\N	\N	\N	\N	Bemerkenswerte Pflanzengesellschaften in Südbayern	001161	München	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:14.015	\N	1996	\N	3178	f
3180	Hetzel_Ullmann_1983_001162	\N	\N	\N	\N	\N	Neue und bemerkenswerte Ruderalpfanzen aus Würzburg und Umgebung	001162	Göttingen	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:14.231	\N	1983	\N	3181	f
3186	Eggensberger_1994_001164	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der subalpinen und alpinen Stufe der Ammergauer Alpen und ihre Stellung in den Ostalpen	001164	München	\N	\N	\N		\N	\N	\N	1057	\N	Y			\N		\N	article	2016-05-04 11:09:15	\N	1994	\N	3187	f
3193	Wilmanns_1989_001167	\N	\N	\N	\N	\N	Vergesellschaftung und Strategie-Typen von Pflanzen mitteleuropäischer Rebkulturen	001167	Berlin, Stuttgart	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:15.818	\N	1989	\N	3194	f
3195	Reif_Oberdorfer_1990_001168	\N	\N	\N	\N	\N	Die Birkenberge im Bayerischen Wald	001168		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:16.033	\N	1990	\N	3196	f
3782	Jeschke_1966_100109	\N	\N	\N	\N	\N	Die Alte Straminke bei Zingst	100109		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:10.14	\N	1966	\N	3783	f
3208	Mayer_1999_001173	\N	\N	\N	\N	\N	Beobachtungen über naturnahe Waldgesellschaften in der Sächsischen Schweiz	001173		\N	\N	\N		TV	\N	\N	13	\N	Y			\N		\N	article	2016-05-04 11:09:17.227	\N	1999	\N	3209	f
3211	Becker_Becker_2010_001174	\N	\N	\N	\N	\N	Einfluss der Umwelt- und Landnutzung auf artenreiche Wiesen und Weiden im nordwestdeutschen Mittelgebirgsraum	001174	Göttingen	\N	\N	\N		VW_T304	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:17.524	\N	2010	\N	3212	f
3213	Ewald_1996_001175	\N	\N	\N	\N	\N	Graslahner-Rasengesellschaften in der montanen Waldstufe der Tegernseer Kalkalpen	001175	München	\N	\N	\N		\N	\N	\N	37	\N	Y			\N		\N	article	2016-05-04 11:09:17.736	\N	1996	\N	3214	f
3219	Baumann_1999_001177	\N	\N	\N	\N	\N	Vegetation, Verbreitung und Gefährdung basenreich-nährstoffarmer Sümpfe im sachsen-anhaltinischen Harz	001177	Halle (Saale)	\N	\N	\N		\N	\N	\N	26	\N	Y			\N		\N	article	2016-05-04 11:09:18.235	\N	1999	\N	3220	f
3222	Otte_Maul_2005_001178	\N	\N	\N	\N	\N	Verbreitungsschwerpunkte und strukturelle Einnischung der Stauden-Lupine (Lupinus polyphyllus Lindl.) in Bergwiesen der Rhön	001178	Göttingen	\N	\N	\N		VW_T256	\N	\N	30	\N	Y			\N		\N	article	2016-05-04 11:09:18.449	\N	2005	\N	3223	f
3224	Fuchs_2005_001179	\N	\N	\N	\N	\N	Erlen- und Birkenbruchwald-Gesellschaften im Ruhrgebiet	001179	Göttingen	\N	\N	\N		VW_T254	\N	\N	40	\N	Y			\N		\N	article	2016-05-04 11:09:18.703	\N	2005	\N	3225	f
3229	Krumbiegel_2006_001181	\N	\N	\N	\N	\N	Bolboschoenus laticarpus-Röhrichte an der Mittelelbe, eine bisher verkannte Gesellschaft	001181	Göttingen	\N	\N	\N		VW_T268	\N	\N	37	\N	Y			\N		\N	article	2016-05-04 11:09:19.2	\N	2006	\N	3230	f
3231	Klauck_2007_001182	\N	\N	\N	\N	\N	Geranium phaeum L. in Saumgesellschaften und Versaumung	001182	Göttingen	\N	\N	\N		VW_T272	\N	\N	16	\N	Y			\N		\N	article	2016-05-04 11:09:19.411	\N	2007	\N	3232	f
3175	Linhard_Stueckl_1972_001160	\N	\N	\N	\N	\N	Xerotherme Vegetationseinheiten an Südhängen des Regen- und Donautales im kristallinen Bereich	001160	Regensburg	\N	\N	\N		\N	\N	\N	174	\N	Y			\N		\N	article	2016-05-04 11:09:13.78	\N	1972	\N	3176	f
3237	Wittig_Wittig_2009_001184	\N	\N	\N	\N	\N	Trittgesellschaften der nordrhein-westfälischen Dörfer	001184	Göttingen	\N	\N	\N		VW_T293	\N	\N	43	\N	Y			\N		\N	article	2016-05-04 11:09:19.839	\N	2009	\N	3238	f
3189	Kruess_Rohde_1990_001165	\N	\N	\N	\N	\N	Pflegeproblematik und Bestandesentwicklung in den Naturschutzgebieten "Sandhausener Dünen"	001165	Karlsruhe	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:15.255	\N	1990	\N	3190	f
3245	Fleischer_Streitberger_Fartmann_2010_001186	\N	\N	\N	\N	\N	Zur Ökologie der Wiesen-Glockenblume (Campanula patula) und des Echten Tausendgüldenkrauts (Centaurium erythraea) im Magergrünland Nordwestdeutschlands	001186	Göttingen	\N	\N	\N		VW_T305	\N	\N	80	\N	Y			\N		\N	article	2016-05-04 11:09:20.355	\N	2010	\N	3246	f
3247	Gehlken_2010_001187	\N	\N	\N	\N	\N	Beitrag zur Abgrenzung und Untergliederung des Filagini-Vulpietum myuros Oberd. 1938	001187	Göttingen	\N	\N	\N		VW_T306	\N	\N	61	\N	Y			\N		\N	article	2016-05-04 11:09:20.578	\N	2010	\N	3248	f
3254	Mast_1995_001189	\N	\N	\N	\N	\N	Naturnahe Vegetation an Quellstandorten im Weser-Leinebergland (Süd-Niedersachsen)	001189	Göttingen	\N	\N	\N		\N	\N	\N	122	\N	Y			\N		\N	article	2016-05-04 11:09:21.095	\N	1995	\N	3255	f
3256	Mast_1996_001190	\N	\N	\N	\N	\N	Grünland-Gesellschaften an Quellstandorten im westlichen Weser-Leinebergland (Süd-Niedersachsen)	001190	Braunschweig	\N	\N	\N		\N	\N	\N	29	\N	Y			\N		\N	article	2016-05-04 11:09:21.35	\N	1996	\N	3257	f
3262	Wittig_Lenker_Tokhtar_1999_001192	\N	\N	\N	\N	\N	Zur Soziologie von Arten der Gattung Oenothera L. im Rheintal von Arnheim (NL) bis Mulhouse (F)	001192	Göttingen	\N	\N	\N		\N	\N	\N	134	\N	Y			\N		\N	article	2016-05-04 11:09:21.87	\N	1999	\N	3263	f
3265	Pretzell_Reif_1999_001193	\N	\N	\N	\N	\N	Erlenbruchwälder im Oberrheingraben und ihre Degradationsstadien	001193	Göttingen	\N	\N	\N		\N	\N	\N	103	\N	Y			\N		\N	article	2016-05-04 11:09:22.127	\N	1999	\N	3266	f
3274	Diekmann_Bartels_2012_001195	\N	\N	\N	\N	\N	Das Sumpf-Greiskraut (Senecio paludosus) in Deutschland - Ökologie und Vergesellschaftung	001195	Göttingen	\N	\N	\N		Diekmann_2012_Tuexenia32	TUEXENIA	\N	39	\N	Y			\N		\N	article	2016-05-04 11:09:22.578	\N	2012	\N	3275	f
3293	Braun_2009_001202	\N	\N	\N	\N	\N	Ein Niedermoor wächst über Hochmoortorf	001202	München	\N	\N	\N		Braun_2009	\N	\N	96	\N	Y			\N		\N	article	2016-05-04 11:09:24.188	\N	2009	\N	3294	f
3299	Kelm_1992_001204	\N	\N	\N	\N	\N	Zum Vorkommen der Berberitze (Berberis vulgaris) in Wäldern bei Lüneburg	001204	Lüneburg	\N	\N	\N		\N	\N	\N	20	\N	Y			\N		\N	article	2016-05-04 11:09:24.854	\N	1992	\N	3300	f
3303	Frede_Kellner_Langbehn_1995_001205	\N	\N	\N	\N	\N	Der Steppenfenchel (Seseli annuum L.) und seine Begleitvegetation bei Bad Wildungen	001205	Darmstadt	\N	\N	\N		\N	\N	\N	3	\N	Y			\N		\N	article	2016-05-04 11:09:25.107	\N	1995	\N	3304	f
3305	Fischer_1985_001206	\N	\N	\N	\N	\N	Zum Vorkommen von Gagea bohemica ssp. saxatilis im Potsdamer Gebiet	001206	Berlin	\N	\N	\N		\N	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:09:25.334	\N	1985	\N	3306	f
3323	Krumbiegel_2008_001208	\N	\N	\N	\N	\N	Zur Vergesellschaftung von Gentianella campestris ssp. baltica am Ostufer der Müritz (Mecklenburg-Vorpommern) und bei Ballenstedt am Nordharzrand (Sachsen-Anhalt)	001208		\N	\N	\N		\N	\N	\N	20	\N	Y			\N		\N	article	2016-05-04 11:09:25.926	\N	2008	\N	3324	f
3325	Krumbiegel_2012_001209	\N	\N	\N	\N	\N	Die Vergesellschaftung von Urtica subinermis (R. Uechtr.) Hand & Buttler an der Mittelelbe zwischen Elster (Sachsen-Anhalt) und Lenzen (Brandenburg)	001209		\N	\N	\N		\N	\N	\N	50	\N	Y			\N		\N	article	2016-05-04 11:09:26.131	\N	2012	\N	3326	f
3359	Faust_2009_001220	\N	\N	\N	\N	\N	Der Lothringer Lein (Linum leonii Schultz) in Bayern	001220	München	\N	\N	\N		\N	\N	\N	3	\N	Y			\N		\N	article	2016-05-04 11:09:28.685	\N	2009	\N	3360	f
3362	Raabe_1955_001221	\N	\N	\N	\N	\N	Teucrium chamaedrys in der Rhön	001221	Darmstadt	\N	\N	\N		Raabe_1955	\N	\N	3	\N	Y			\N		\N	article	2016-05-04 11:09:28.902	\N	1955	\N	3363	f
3367	Dannenberg_1937_001223	\N	\N	\N	\N	\N	Festuca psammophila Krajina, ihr verwandtschaftlicher Zusammenhang und ihre Stellung in der Vegetation der Mark Brandenburg	001223		\N	\N	\N		Dannenberg_1937	\N	\N	13	\N	Y			\N		\N	article	2016-05-04 11:09:29.367	\N	1937	\N	3368	f
3371	Korneck_1957_001225	\N	\N	\N	\N	\N	Der Jakobsberg bei Ockenheim	001225	Darmstadt	\N	\N	\N		Korneck_1957	\N	\N	4	\N	Y			\N		\N	article	2016-05-04 11:09:29.849	\N	1957	\N	3372	f
3375	Hundt_1996_001227	\N	\N	\N	\N	\N	Zur Veränderung der Wiesenvegetation Mitteldeutschlands unter dem Einfluß einer starken Bewirtschaftungsintensität	001227		\N	\N	\N		Hundt_1996	\N	\N	6	\N	Y			\N		\N	article	2016-05-04 11:09:30.278	\N	1996	\N	3376	f
3258	Zielonkowski_null_001191	\N	\N	\N	\N	\N	Wildgrasfluren der Umgebung Regensburgs Vegetationskundliche Untersuchungen an einem Beitag zur Landespflege	001191	Regensburg	\N	\N	\N		\N	\N	\N	361	\N	Y			\N		\N	article	2016-05-04 11:09:21.605	\N	1500	\N	3259	t
3378	Kretzschmar_1994_001228	\N	\N	\N	\N	\N	Zur Bedeutung der Samenbank in Böden unter Wiesengesellschaften	001228		\N	\N	\N		Kretzschmar_1994	\N	\N	6	\N	Y			\N		\N	article	2016-05-04 11:09:30.494	\N	1994	\N	3379	f
3381	Buchmann_1994_001229	\N	\N	\N	\N	\N	Wildkrautgesellschaften genutzter Weinberge an der Nahe	001229		\N	\N	\N		Buchmann_1994	\N	\N	166	\N	Y			\N		\N	article	2016-05-04 11:09:30.706	\N	1994	\N	3382	f
3384	Bennert_2003_001230	\N	\N	\N	\N	\N	Bestandsdynamik, Ökologie und Soziologie von Botrychium simplex in der Senne (Nordrhein-Westfalen)	001230		\N	\N	\N		Bennert_2003	\N	\N	5	\N	Y			\N		\N	article	2016-05-04 11:09:30.956	\N	2003	\N	3385	f
3386	Wittig_1999_001231	\N	\N	\N	\N	\N	Vegetation, Flora und Schutzwürdigkeit des geplanten Waldnaturschutzgebietes "Glindfeld" im Hochsauerland	001231		\N	\N	\N		Wittig_1999_1	\N	\N	23	\N	Y			\N		\N	article	2016-05-04 11:09:31.171	\N	1999	\N	3387	f
3389	Wittig_Huck_Wittig_1999_001232	\N	\N	\N	\N	\N	Verbreitung, Vergesellschaftung und Ökologie der Bärlappe (Lycopodiaceae) im Zentrum des Rothaargebirges	001232		\N	\N	\N		Wittig_1999_2	\N	\N	116	\N	Y			\N		\N	article	2016-05-04 11:09:31.394	\N	1999	\N	3390	f
3391	Wittig_2003_001233	\N	\N	\N	\N	\N	Saumgesellschaften mit Dominanz von Bachauen(wald)-Arten in Luzulo-Fageten und Fichtenforsten des Rothaargebirges	001233		\N	\N	\N		Wittig_2003	\N	\N	40	\N	Y			\N		\N	article	2016-05-04 11:09:31.65	\N	2003	\N	3392	f
3451	Mueller-stoll_1992_001254	\N	\N	\N	\N	\N	Pflanzensoziologische Untersuchungen der Laubwald-Gesellschaften der Umgebung von Tharandt bei Dresden	001254		\N	\N	\N		Mueller-Stoll_1992	\N	\N	132	\N	Y			\N		\N	article	2016-05-04 11:09:36.412	\N	1992	\N	3452	f
3404	Otte_ginzler_Waldhardt_Simmering_2008_001237	\N	\N	\N	\N	\N	Die Allmendeweide NSG "Kanzelstein" bei Eibach (Lahn-Dill Kreis, Hessen): Wandel und Zustand eines Biotopkomplexes der vorindustriellen Kulturlandschaft	001237		\N	\N	\N		T281	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:32.615	\N	2008	\N	3405	f
3407	Walentowski_Kundernatsch_Fischer_Ewald_2014_001238	\N	\N	\N	\N	\N	Naturwaldreservatsforschung in Bayern - Auswertung von Vegetationsdaten zur wald-ökologischen Dauerbeobachtung	001238		\N	\N	\N		T34XX_Walentowski	\N	\N	24	\N	Y			\N		\N	article	2016-05-04 11:09:32.88	\N	2014	\N	3408	f
3409	Dierschke_2014_001239	\N	\N	\N	\N	\N	Sekundärsukzession auf Kahlschlagflächen eines Buchenwaldes	001239		\N	\N	\N		T34XX_Dierschke	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:33.1	\N	2014	\N	3410	f
3413	Conradi_Friedmann_2013_001240	\N	\N	\N	\N	\N	Plant communities and environmental gradients in mires of the Ammergauer Alps (Bavaria, Germany)	001240		\N	\N	\N		T336_Conradi	\N	\N	115	\N	Y			\N		\N	article	2016-05-04 11:09:33.312	\N	2013	\N	3414	f
3430	Rudner_Gross_M._2012_001246	\N	\N	\N	\N	\N	Funktionelle Pflanzentypen für Borstgrasrasen im Schwarzwald	001246		\N	\N	\N		T328_Rudner	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:34.511	\N	2012	\N	3431	f
3422	Remy_2011_001244	\N	\N	\N	\N	\N	Altgewässer und ihre Bedeutung für die Wasservegetation	001244		\N	\N	\N		T316_Remy	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:34.074	\N	2011	\N	3423	f
3426	Lang_Frei_Ewald_2011_001245	\N	\N	\N	\N	\N	Waldgesellschaften und Standortabhängigkeit der Vegetation vor Beginn der Redynamisierung der Donauaue zwischen Neuburg und Ingolstadt	001245		\N	\N	\N		T314_Lang	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:34.288	\N	2011	\N	3427	f
3444	Geringhoff_2003_001251	\N	\N	\N	\N	\N	Zur Syntaxonomie des Vaccinio-Callunetum Büker 1942 unter besonderer Berücksichtigung der Bestände im Rothaargebirge	001251		\N	\N	\N		Geringhoff_2003	\N	\N	219	\N	Y			\N		\N	article	2016-05-04 11:09:35.659	\N	2003	\N	3445	f
3446	Hemp_2001_001252	\N	\N	\N	\N	\N	Die Dolomitsand-Trockenrasen (Helichryso-Festucetum) der Frankenalb	001252		\N	\N	\N		Hemp_2001	\N	\N	107	\N	Y			\N		\N	article	2016-05-04 11:09:35.865	\N	2001	\N	3447	f
3448	Hundt_1956_001253	\N	\N	\N	\N	\N	Grünlandvegetationskartierung im Unstruttal bei Straußfurt	001253		\N	\N	\N		Hundt_1956	\N	\N	43	\N	Y			\N		\N	article	2016-05-04 11:09:36.158	\N	1956	\N	3449	f
3420	Gregor_Boensel_Starke-Ottich_Tackenberg_Wittig_Zizka_2013_001243	\N	\N	\N	\N	\N	Epilobium brachycarpum: a fast-spreading neophyte in Germany	001243		\N	\N	\N		T3311_Gregor	\N	\N	69	\N	Y			\N		\N	article	2016-05-04 11:09:33.814	\N	2013	\N	3421	f
3456	Pallas_2000_001256	\N	\N	\N	\N	\N	Zur Systematik und Verbreitung der europäischen bodensauren Eichenmischwälder (Quercetalia roboris Tüxen 1931)	001256		\N	\N	\N		Pallas_2000	\N	\N	89	\N	Y			\N		\N	article	2016-05-04 11:09:36.873	\N	2000	\N	3457	f
3458	Pallas_2002_001257	\N	\N	\N	\N	\N	Artenarme bodensaure Eichenmischwälder (Deschampsio-Quercetum Passarge 1966) in Nordwestdeutschland	001257		\N	\N	\N		Pallas_2002	\N	\N	460	\N	Y			\N		\N	article	2016-05-04 11:09:37.092	\N	2002	\N	3459	f
3463	Seibert_1975_001259	\N	\N	\N	\N	\N	Veränderung der Auenvegetation nach Anhebung des Grundwasserspiegels in den Donauauen bei Offingen	001259		\N	\N	\N		Seibert_1975	\N	\N	57	\N	Y			\N		\N	article	2016-05-04 11:09:37.548	\N	1975	\N	3464	f
3466	Zickermann_1996_001260	\N	\N	\N	\N	\N	Vegetationsgeschichtliche, moorstratigraphische und pflanzensoziologische Untersuchungen zur Entwicklung seltener Moorökosysteme in Nordwestdeutschland	001260		\N	\N	\N		Zickermann_1996	\N	\N	372	\N	Y			\N		\N	article	2016-05-04 11:09:37.798	\N	1996	\N	3467	f
3469	Huwer_Wittig_2012_001261	\N	\N	\N	\N	\N	Changes in the species composition of hedgerows	001261		\N	\N	\N		T322_Huwer_Wittig	\N	\N	346	\N	Y			\N		\N	article	2016-05-04 11:09:38.005	\N	2012	\N	3470	f
3472	Ewald_Schessl_2013_001262	\N	\N	\N	\N	\N	Kiefer am Scheideweg: Heidewälder in der nördlichen Münchener Ebene	001262		\N	\N	\N		T331_Ewald_Schessl	\N	\N	42	\N	Y			\N		\N	article	2016-05-04 11:09:38.264	\N	2013	\N	3473	f
3475	Rebele_2014_001263	\N	\N	\N	\N	\N	Artenzusammensetzung und Diversität von Calamagrostis epigejos-Dominanzbeständen auf Brachflächen und ehemaligen Rieselfeldern in Berlin	001263		\N	\N	\N		T3413_Rebele	\N	\N	90	\N	Y			\N		\N	article	2016-05-04 11:09:38.523	\N	2014	\N	3476	f
3485	Mueller_1991_001266	\N	\N	\N	\N	\N	Auenvegetation des Lech bei Augsburg und ihre Veränderungen infolge von Flußbaumaßnahmen	001266		\N	\N	\N		Mueller_1991	\N	\N	133	\N	Y			\N		\N	article	2016-05-04 11:09:39.223	\N	1991	\N	3486	f
3493	Scheideler_Smolis_1983_001269	\N	\N	\N	\N	\N	Die Halbtrockenrasen am Bielenberg (Kr. Höxter). Entwicklung, Zustand, Schutz- und Pflegeproblematik	001269	Münster	\N	SCS	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:40.048	\N	1983	\N	3494	f
3522	Ludewig_Korell_Loeffler_Mosner_Scholz_Jensen_2014_001277	\N	\N	\N	\N	\N	unveröff. Daten zu: Vegetation patterns of floodplain meadows along the climatic gradient at the Middle Elbe River	001277		\N	\N	0000000015		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:42.003	\N	2014	\N	3523	f
3514	Springer_2014_001276	\N	\N	\N	\N	\N	Pflanzengesellschaften im Truderinger Wald in München	001276		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:41.785	\N	2014	\N	3515	f
3527	Arendt_1981_100002	\N	\N	\N	\N	\N	Pflanzengesellschaften von Fließgewässern als Indikator der Gewässerverschmutzungen, dargestellt am Beispiel des Uecker- und Havelsystems	100002	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:42.571	\N	1981	\N	3528	f
3529	Arendt_1982_100003	\N	\N	\N	\N	\N	Soziologisch-ökologische Charakteristik der Pflanzengesellschaften von Fließgewässern des Ücker- und Havelsystems	100003	Berlin	\N	\N	\N		\N	\N	\N	11	11	Y			\N		\N	article	2016-05-04 11:09:42.804	\N	1982	\N	3530	f
3531	Arendt_1989_100004	\N	\N	\N	\N	\N	Fließgewässer als bedeutende Biotope für den Florenschutz	100004		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:43.059	\N	1989	\N	3532	f
3543	Bliss_1983_100008	\N	\N	\N	\N	\N	Weberknechte aus der Umgebung der Biologischen Station Fauler Ort im Naturschutzgebiet "Ostufer der Müritz" (Arachnida, Opiliones)	100008		\N	\N	\N		\N	\N	\N	1	1	Y			\N		Rothmaler	article	2016-05-04 11:09:44.099	\N	1983	\N	3544	f
3546	Bochnig_1959_100009	\N	\N	\N	\N	\N	Das Waldschutzgebiet Eldena bei Greifswald	100009	Rostock	\N	\N	\N		\N	\N	\N	7	7	Y			\N		\N	article	2016-05-04 11:09:44.39	\N	1959	\N	3547	f
3548	Bochnig_1959_100010	\N	\N	\N	\N	\N	Vegetationskundliche Studien im Naturschutzgebiet Insel Vilm bei Rügen	100010	Rostock	\N	\N	\N		\N	\N	\N	28	28	Y			\N		Rothmaler 1953	article	2016-05-04 11:09:44.646	\N	1959	\N	3549	f
3550	Bolbrinker_1975_100011	\N	\N	\N	\N	\N	Das Braune Zyperngras (Cyperus fuscus L.) am Malchiner See entdeckt	100011		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:09:44.916	\N	1975	\N	3551	f
3552	Bolbrinker_1977_100012	\N	\N	\N	\N	\N	Das Dammer Koppel-Soll, ein neues Flächennaturdenkmal im Kreis Teterow	100012		\N	\N	\N		\N	\N	\N	37	37	Y			\N		\N	article	2016-05-04 11:09:45.155	\N	1977	\N	3553	f
3554	Bolbrinker_1977_100013	\N	\N	\N	\N	\N	Zur Verbreitung und zum soziologischen Verhalten der Grünlichen Waldhyazinthe (Platanthera chlorantha L.) in Mecklenburg	100013		\N	\N	\N		\N	\N	\N	30	30	Y			\N		\N	article	2016-05-04 11:09:45.383	\N	1977	\N	3555	f
3556	Bolbrinker_1978_100014	\N	\N	\N	\N	\N	Zwei neue Funde des Schwimmenden Wassersternlebermooses (Ricciocarpus natans) im Kreis Teterow	100014		\N	\N	\N		\N	\N	\N	11	11	Y			\N		\N	article	2016-05-04 11:09:45.604	\N	1978	\N	3557	f
3558	Bolbrinker_1978_100015	\N	\N	\N	\N	\N	Der Kubaspinat (Claytonia perfoliata DONN ex WILLD.) - ein Neophyt in Mecklenburg	100015		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:09:45.826	\N	1978	\N	3559	f
3562	Bolbrinker_1984_100017	\N	\N	\N	\N	\N	Zum Vorkommen des Elatino- alsinastri-Juncetum tenageiae LIBB.33 in Mittelmecklenburg	100017	Berlin	\N	\N	\N		\N	\N	\N	84	84	Y			\N		\N	article	2016-05-04 11:09:46.352	\N	1984	\N	3563	f
3564	Bolbrinker_1985_100018	\N	\N	\N	\N	\N	Floristische Beobachtungen in Tongrubengewässern bei Neukalen	100018		\N	\N	\N		\N	\N	\N	16	16	Y			\N		\N	article	2016-05-04 11:09:46.577	\N	1985	\N	3565	f
3566	Bolbrinker_1986_100019	\N	\N	\N	\N	\N	Potamogeton trichioides (CHAM. et SCHLDL.) in Kleingewässern Mittelmecklenburgs	100019		\N	\N	\N		\N	\N	\N	16	16	Y			\N		\N	article	2016-05-04 11:09:46.9	\N	1986	\N	3567	f
3568	Bolbrinker_1988_100020	\N	\N	\N	\N	\N	Zur Wiederbesiedlung und Entwicklung der Vegetation in ausgetorften Torflagerstätten ursprünglicher Feldsölle	100020		\N	\N	\N		\N	\N	\N	28	28	Y			\N		\N	article	2016-05-04 11:09:47.178	\N	1988	\N	3569	f
3576	Borowiec_Kudoke_Lesnik_1990_100022	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen zum Vorkommen des Euphorbio-Melandrietum G. Müller 64 im Brüssower Raum und in den angrenzenden polnischen Gebieten. Teil I. Halmfrucht	100022		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:47.683	\N	1990	\N	3577	f
3578	Borowiec_Kudoke_Lesnik_1991_100023	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen zum Vorkommen des Euphorbio-Melandrietum G. Müller 64 im Brüssower Raum und in den angrenzenden polnischen Gebieten. Teil II. Hackfrucht	100023	Rostock	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:47.942	\N	1991	\N	3579	f
3584	Busecke_1979_100025	\N	\N	\N	\N	\N	Die Vegetation des Kleinen Laubahnsees (Nossentiner Heide/Mecklenburg)	100025		\N	\N	\N		\N	\N	\N	13	13	Y			\N		\N	article	2016-05-04 11:09:48.398	\N	1979	\N	3585	f
3586	Doll_1968_100026	\N	\N	\N	\N	\N	Über die Verbreitung von Taraxacum laevigatum (WILLD.DC. in Mecklenburg	100026		\N	\N	\N		\N	\N	\N	8	8	Y			\N		\N	article	2016-05-04 11:09:48.622	\N	1968	\N	3587	f
3588	Doll_1971_100027	\N	\N	\N	\N	\N	Das Große Wiesensoll bei Parchim (Mecklenburg)	100027		\N	\N	\N		\N	\N	\N	37	37	Y			\N		\N	article	2016-05-04 11:09:48.904	\N	1971	\N	3589	f
3590	Doll_1971_100028	\N	\N	\N	\N	\N	Das Schünsoll bei Weberin (Kr.Schwerin)	100028		\N	\N	\N		\N	\N	\N	85	85	Y			\N		\N	article	2016-05-04 11:09:49.186	\N	1971	\N	3591	f
3592	Doll_1976_100029	\N	\N	\N	\N	\N	Neue Naturschutzgebiete im Kreis Neustrelitz	100029		\N	\N	\N		DUBLETTE000820	\N	\N	34	34	Y			000820		\N	article	2016-05-04 11:09:49.509	\N	1976	\N	3593	f
3594	Doll_1977_100030	\N	\N	\N	\N	\N	Botanisch interessante Gebiete aus dem Kreis Neustrelitz I	100030	Rostock	\N	\N	\N		\N	\N	\N	65	65	Y			\N		\N	article	2016-05-04 11:09:49.794	\N	1977	\N	3595	f
3596	Doll_1977_100031	\N	\N	\N	\N	\N	Die Vegetation des Naturschutzgebietes Useriner Horst	100031	Berlin	\N	\N	\N		\N	\N	\N	23	23	Y			\N		\N	article	2016-05-04 11:09:50.038	\N	1977	\N	3597	f
3598	Doll_1977_100032	\N	\N	\N	\N	\N	Der Drevitzer See bei Alt Schwerin (Kr.Waren)	100032		\N	\N	\N		\N	\N	\N	52	52	Y			\N		\N	article	2016-05-04 11:09:50.307	\N	1977	\N	3599	f
3600	Doll_1977_100033	\N	\N	\N	\N	\N	Die Drei Pöhle bei Neustrelitz	100033		\N	\N	\N		\N	\N	\N	30	30	Y			\N		\N	article	2016-05-04 11:09:50.644	\N	1977	\N	3601	f
3602	Doll_1978_100034	\N	\N	\N	\N	\N	Der Schmollitz-See bei Zirtow, Kr. Neustrelitz	100034		\N	\N	\N		\N	\N	\N	36	36	Y			\N		\N	article	2016-05-04 11:09:50.919	\N	1978	\N	3603	f
3604	Doll_1978_100035	\N	\N	\N	\N	\N	Drei bemerkenswerte Seen im südlichen Mecklenburg und ihre Vegetation	100035	Berlin	\N	\N	\N		DUBLETTE000779	\N	\N	82	82	Y			000779		\N	article	2016-05-04 11:09:51.164	\N	1978	\N	3605	f
3606	Doll_1978_100036	\N	\N	\N	\N	\N	Drei bemerkenswerte Moorgebiete aus dem Kreis Neustrelitz	100036	Berlin	\N	\N	\N		\N	\N	\N	35	35	Y			\N		\N	article	2016-05-04 11:09:51.424	\N	1978	\N	3607	f
3608	Doll_1978_100037	\N	\N	\N	\N	\N	Die Vegetation des Neustädter Sees (Kr.Ludwigslust)	100037	Berlin	\N	\N	\N		\N	\N	\N	62	62	Y			\N		\N	article	2016-05-04 11:09:51.659	\N	1978	\N	3609	f
3612	Doll_1979_100039	\N	\N	\N	\N	\N	Die Vegetation der Kalkhorst bei Neustrelitz	100039		\N	\N	\N		\N	\N	\N	40	40	Y			\N		\N	article	2016-05-04 11:09:52.172	\N	1979	\N	3613	f
3614	Doll_1979_100040	\N	\N	\N	\N	\N	Der Waschsee bei Mechow (Kr. Neustrelitz)	100040		\N	\N	\N		\N	\N	\N	28	28	Y			\N		\N	article	2016-05-04 11:09:52.464	\N	1979	\N	3615	f
3616	Doll_1979_100041	\N	\N	\N	\N	\N	Die Vegetation des Langhäger Sees (Kr. Neustrelitz)	100041	Berlin	\N	\N	\N		\N	\N	\N	12	12	Y			\N		\N	article	2016-05-04 11:09:52.744	\N	1979	\N	3617	f
3618	Doll_1979_100042	\N	\N	\N	\N	\N	Das Flächennaturdenkmal Ostufer des Zotzen-Sees	100042		\N	\N	\N		\N	\N	\N	20	20	Y			\N		\N	article	2016-05-04 11:09:53.014	\N	1979	\N	3619	f
3620	Doll_1981_100043	\N	\N	\N	\N	\N	Das ökologisch-soziologische Verhalten von Najas major s.l	100043	Berlin	\N	\N	\N		\N	\N	\N	87	87	Y			\N		\N	article	2016-05-04 11:09:53.255	\N	1981	\N	3621	f
3622	Doll_1982_100044	\N	\N	\N	\N	\N	Der Große Boden-See im Kreis Neustrelitz	100044	Berlin	\N	\N	\N		\N	\N	\N	45	45	Y			\N		\N	article	2016-05-04 11:09:53.491	\N	1982	\N	3623	f
3624	Doll_1982_100045	\N	\N	\N	\N	\N	Das Seechen am Gr.Beutel-See bei Templin	100045		\N	\N	\N		\N	\N	\N	11	11	Y			\N		\N	article	2016-05-04 11:09:53.762	\N	1982	\N	3625	f
3626	Doll_1983_100046	\N	\N	\N	\N	\N	Die Vegetation des Gr.Fürstenseer Sees im Kr. Neustrelitz	100046	Berlin	\N	\N	\N		\N	\N	\N	141	141	Y			\N		\N	article	2016-05-04 11:09:54.037	\N	1983	\N	3627	f
3628	Doll_1984_100047	\N	\N	\N	\N	\N	Der Tüzer See im Kreis Altentreptow	100047	Berlin	\N	\N	\N		\N	\N	\N	30	30	Y			\N		\N	article	2016-05-04 11:09:54.352	\N	1984	\N	3629	f
3632	Doll_1991_100049	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der stehenden Gewässer in Mecklenburg-Vorpommern.Teil 1. Lemnetea - Wasserlinsengesellschaften	100049	Berlin	\N	\N	\N		\N	\N	\N	190	190	Y			\N		\N	article	2016-05-04 11:09:54.936	\N	1991	\N	3633	f
3634	Doll_1991_100050	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der stehenden Gewässer in Mecklenburg-Vorpommern.Teil 1.Potamogetonetae Tx.et Prsg.42 - Laichkrautgesellschaften	100050	Berlin	\N	\N	\N		\N	\N	\N	524	524	Y			\N		\N	article	2016-05-04 11:09:55.164	\N	1991	\N	3635	f
3638	Doll_Doll_Kaschube_1981_100051	\N	\N	\N	\N	\N	Floristische Mitteilungen aus dem Kreis Altentreptow II	100051		\N	\N	\N		\N	\N	\N	21	21	Y			\N		\N	article	2016-05-04 11:09:55.443	\N	1981	\N	3639	f
3641	Doll_Hemke_1979_100052	\N	\N	\N	\N	\N	Das Naturschutzgebiet Degenmoor bei Wesenberg (Kr. Neustrelitz)	100052		\N	\N	\N		\N	\N	\N	30	30	Y			\N		\N	article	2016-05-04 11:09:55.73	\N	1979	\N	3642	f
3647	Doll_Kintzel_1988_100055	\N	\N	\N	\N	\N	Die Vegetation des Flächennaturdenkmales Enziansoll (Kreis Lübz)	100055		\N	\N	\N		\N	\N	\N	18	18	Y			\N		\N	article	2016-05-04 11:09:56.544	\N	1988	\N	3648	f
3649	Doll_Kintzel_1989_100056	\N	\N	\N	\N	\N	Die Vegetation des Flächennaturdenkmales Riederfelder Moor (Kr.Lübz)	100056	Rostock	\N	\N	\N		\N	\N	\N	30	30	Y			\N		\N	article	2016-05-04 11:09:56.798	\N	1989	\N	3650	f
3652	Doll_Stegemann_1976_100057	\N	\N	\N	\N	\N	Das NSG "Grundloser See" bei Ahrensberg (Kr. Neustrelitz)	100057		\N	\N	\N		\N	\N	\N	10	10	Y			\N		\N	article	2016-05-04 11:09:57.077	\N	1976	\N	3653	f
3643	Doll_Kaschube_Et_Al._1980_100053	\N	\N	\N	\N	\N	Floristische Mitteilungen aus dem Kreis Altentreptow I	100053		\N	\N	\N		\N	\N	\N	2	2	Y			\N		\N	article	2016-05-04 11:09:55.981	\N	1980	\N	3644	f
3659	Fiedler_Pankow_1966_100059	\N	\N	\N	\N	\N	Ein neuentdecktes Vorkommen der Blasenbinse (Scheuchzeria palustris L.) in der Nähe Rostocks	100059		\N	\N	\N		\N	\N	\N	6	6	Y			\N		\N	article	2016-05-04 11:09:57.553	\N	1966	\N	3660	f
3661	Fischer_1960_100060	\N	\N	\N	\N	\N	Pflanzengesellschaften der Heiden und oligotrophen Moore der Prignitz	100060		\N	\N	\N		\N	\N	\N	13	13	Y			\N		\N	article	2016-05-04 11:09:57.812	\N	1960	\N	3662	f
3666	Fukarek_1961_100062	\N	\N	\N	\N	\N	Die Vegetation des Darß und ihre Geschichte	100062		\N	\N	\N		DUBLETTE000163	\N	\N	500	500	Y			000163		\N	article	2016-05-04 11:09:58.237	\N	1961	\N	3667	f
3668	Fukarek_1969_100063	\N	\N	\N	\N	\N	Ein Beitrag zur potentiellen natürlichen Vegetation in Mecklenburg	100063		\N	\N	\N		\N	\N	\N	13	13	Y			\N		\N	article	2016-05-04 11:09:58.529	\N	1969	\N	3669	f
3676	Funk_1979_100066	\N	\N	\N	\N	\N	Zum Vorkommen der Blauen Himmelsleiter (Polemonium caeruleum L.) im Trebeltal nördlich Quitzenow	100066		\N	\N	\N		\N	\N	\N	13	13	Y			\N		\N	article	2016-05-04 11:09:59.348	\N	1979	\N	3677	f
3679	Gerhardt_Doll_1980_100067	\N	\N	\N	\N	\N	Bemerkenswerte Pflanzengesellschaften am Gr. Kelpin-See (Kr.Templin)	100067		\N	\N	\N		\N	\N	\N	11	11	Y			\N		\N	article	2016-05-04 11:09:59.662	\N	1980	\N	3680	f
3682	Giersberg_1977_100068	\N	\N	\N	\N	\N	Mikroklimatische Studien zur Verbreitung von Matricaria chamomilla und Tripleurospermum inodorum in der jungpleistozänen Ackerlandschaft des Nordens der DDR	100068	Rostock	\N	\N	\N		\N	\N	\N	3	3	Y			\N		\N	article	2016-05-04 11:09:59.881	\N	1977	\N	3683	f
3685	Gollub_1981_100069	\N	\N	\N	\N	\N	Acker-Schwarzkümmel (Nigella arvensis L.) wiederentdeckt	100069		\N	\N	\N		\N	\N	\N	3	3	Y			\N		\N	article	2016-05-04 11:10:00.155	\N	1981	\N	3686	f
3691	Hauke_Gollub_1985_100071	\N	\N	\N	\N	\N	Ajuga chamaepitis (L.)SCHREBER in Mecklenburg	100071		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:00.724	\N	1985	\N	3692	f
3694	Hemke_Hemke_1982_100072	\N	\N	\N	\N	\N	Über das Sumpfknabenkraut (Orchis palustris JACQ.) im Norden der DDR	100072		\N	\N	\N		\N	\N	\N	3	3	Y			\N		\N	article	2016-05-04 11:10:00.987	\N	1982	\N	3695	f
3697	Henker_1970_100073	\N	\N	\N	\N	\N	Beitrag zur Kenntnis der Flora Südwest- Mecklenburgs	100073		\N	\N	\N		\N	\N	\N	4	4	Y			\N		\N	article	2016-05-04 11:10:01.286	\N	1970	\N	3698	f
3688	Gutte_Koehler_1975_100070	\N	\N	\N	\N	\N	Zur Flora von Wismar	100070	Rostock	\N	\N	\N		\N	\N	\N	10	10	Y			\N		\N	article	2016-05-04 11:10:00.426	\N	1975	\N	3689	f
3705	Hilbrech_Kintzel_Lembke_1983_100076	\N	\N	\N	\N	\N	Zur Verbreitung einiger Dorfstraßenpflanzen im Kreis Lübz	100076		\N	\N	\N		\N	\N	\N	3	3	Y			\N		\N	article	2016-05-04 11:10:02.065	\N	1983	\N	3706	f
3707	Hofmann_1958_100077	\N	\N	\N	\N	\N	Die eibenreichen Waldgesellschaften Mitteldeutschlands	100077	Berlin	\N	\N	\N		\N	\N	\N	5	5	Y			\N		\N	article	2016-05-04 11:10:02.279	\N	1958	\N	3708	f
3710	Holdack_1959_100078	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der Quellmoore auf Jasmund (Rügen)	100078	Rostock	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:02.498	\N	1959	\N	3711	f
3712	Hollmann_1972_100079	\N	\N	\N	\N	\N	Verbreitung und Soziologie der Schachblume Fritillaria meleagris L	100079	Hamburg	\N	\N	\N		\N	\N	\N	4	4	Y			\N		\N	article	2016-05-04 11:10:02.745	\N	1972	\N	3713	f
3716	Holst_1968_100081	\N	\N	\N	\N	\N	Ein Vorkommen von Epipactis palustris	100081		\N	\N	\N		\N	\N	\N	4	4	Y			\N		\N	article	2016-05-04 11:10:03.217	\N	1968	\N	3717	f
3718	Holst_1976_100082	\N	\N	\N	\N	\N	Mitteilungen zur Flora Mecklenburgs: Zum Auftreten von Ambrosia artemisiifolia L. in Stavenhagen (Bezirk Neubrandenburg)	100082		\N	\N	\N		\N	\N	\N	3	3	Y			\N		\N	article	2016-05-04 11:10:03.436	\N	1976	\N	3719	f
3720	Holst_1978_100083	\N	\N	\N	\N	\N	Zum Vorkommen von Astragalus cicer L. (Kicher-Tragant) bei Tützen (Kreis Altentreptow)	100083		\N	\N	\N		\N	\N	\N	4	4	Y			\N		\N	article	2016-05-04 11:10:03.649	\N	1978	\N	3721	f
3724	Holst_1990_100085	\N	\N	\N	\N	\N	Gypsophila scorzonerifolia SER.in Güstrow	100085		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:04.078	\N	1990	\N	3725	f
3671	Fukarek_Voigtlaender_1982_100064	\N	\N	\N	\N	\N	Zur Verbreitung von Apium repens im Norden der DDR	100064		\N	\N	\N		\N	\N	\N	57	57	Y			\N		\N	article	2016-05-04 11:09:58.843	\N	1982	\N	3672	f
3730	Holst_Martin_1981_100087	\N	\N	\N	\N	\N	Funde von Peplis portula L. im Kreis Güstrow/Bezirk Schwerin	100087		\N	\N	\N		\N	\N	\N	27	27	Y			\N		\N	article	2016-05-04 11:10:04.581	\N	1981	\N	3731	f
3733	Hoppe_Pankow_1968_100088	\N	\N	\N	\N	\N	Ein Beitrag zur Kenntnis der Vegetation der Boddengewässer südlich der Halbinsel Zingst und der Insel Bock (südl.Ostsee)	100088		\N	\N	\N		\N	\N	\N	44	44	Y			\N		\N	article	2016-05-04 11:10:04.823	\N	1968	\N	3734	f
3737	Hoyer_Prill_1983_100089	\N	\N	\N	\N	\N	Neue Fundorte von Platanthera chorantha (Cust.) Rehb	100089		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:05.081	\N	1983	\N	3738	f
3739	Hundt_1968_100090	\N	\N	\N	\N	\N	Geobotanische Untersuchungen zur Ermittlung der Weidewürdigkeit von Grünlandflächen bei Quoltitz auf der Halbinsel Jasmund (Rügen)	100090	Berlin	\N	\N	\N		\N	\N	\N	25	25	Y			\N		\N	article	2016-05-04 11:10:05.303	\N	1968	\N	3740	f
3741	Hundt_1972_100091	\N	\N	\N	\N	\N	Die Trollius europaeus-Polygonum bistorta- Ges.am Ossen auf der Insel Rügen	100091		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:05.546	\N	1972	\N	3742	f
3743	Hundt_1985_100092	\N	\N	\N	\N	\N	Phytosociological and ecological aspects of the Dunes on the Isle of Rügen, Baltic Sea	100092		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:05.844	\N	1985	\N	3744	f
3746	Hundt_Succow_1984_100093	\N	\N	\N	\N	\N	Vegetationsformen des Graslandes der DDR	100093		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:06.151	\N	1984	\N	3747	f
3750	Hurtig_Schulze_1962_100094	\N	\N	\N	\N	\N	Standort und Bestockung im Waldschutzgebiet Anklamer Stadtbruch	100094		\N	\N	\N		\N	\N	\N	4	4	Y			\N		\N	article	2016-05-04 11:10:06.414	\N	1962	\N	3751	f
3753	Jacobs_Jeschke_1973_100095	\N	\N	\N	\N	\N	Schutzwürdige Biotope im Kreis Anklam	100095		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:06.663	\N	1973	\N	3754	f
3756	Jeschke_1959_100096	\N	\N	\N	\N	\N	Die Pflanzengesellschaften einiger Seen bei Feldberg in Mecklenburg	100096	Berlin	\N	\N	\N		\N	\N	\N	145	145	Y			\N		\N	article	2016-05-04 11:10:06.895	\N	1959	\N	3757	f
3758	Jeschke_1959_100097	\N	\N	\N	\N	\N	Der Mittelsee bei Langwitz, ein neues Naturschutzgebiet	100097		\N	\N	\N		\N	\N	\N	11	11	Y			\N		\N	article	2016-05-04 11:10:07.171	\N	1959	\N	3759	f
3760	Jeschke_1959_100098	\N	\N	\N	\N	\N	Einstweilige Sicherung von Naturschutzgebieten	100098		\N	\N	\N		\N	\N	\N	8	8	Y			\N		\N	article	2016-05-04 11:10:07.406	\N	1959	\N	3761	f
3762	Jeschke_1959_100099	\N	\N	\N	\N	\N	Die Seekanne, Nymphoides peltata (GMEL.)O.KTZE., im östlichen Mecklenburg	100099		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:07.689	\N	1959	\N	3763	f
3727	Holst_Kretschmer_Et_Al._1986_100086	\N	\N	\N	\N	\N	Zum Vorkommen von Nigella arvensis L. - Acker-Schwarzkümmel	100086		\N	\N	\N		\N	\N	\N	2	2	Y			\N		\N	article	2016-05-04 11:10:04.316	\N	1986	\N	3728	f
3766	Jeschke_1960_100101	\N	\N	\N	\N	\N	Das Quasliner Moor, ein neues Naturschutzgebiet in Mecklenburg	100101		\N	\N	\N		\N	\N	\N	7	7	Y			\N		\N	article	2016-05-04 11:10:08.201	\N	1960	\N	3767	f
3768	Jeschke_1961_100102	\N	\N	\N	\N	\N	Die Vegetation des Naturschutzgebietes Mümmelken-Moor auf der Insel Usedom	100102	Berlin	\N	\N	\N		\N	\N	\N	106	106	Y			\N		\N	article	2016-05-04 11:10:08.491	\N	1961	\N	3769	f
3770	Jeschke_1962_100103	\N	\N	\N	\N	\N	Das Quellgebiet am Nordufer des Krüselinsees	100103		\N	\N	\N		\N	\N	\N	12	12	Y			\N		\N	article	2016-05-04 11:10:08.761	\N	1962	\N	3771	f
3772	Jeschke_1963_100104	\N	\N	\N	\N	\N	Wasser- und Sumpfvegetation im Naturschutzgebiet Ostufer der Müritz	100104	Berlin	\N	\N	\N		DUBLETTE000784	\N	\N	416	416	Y			000784		\N	article	2016-05-04 11:10:08.981	\N	1963	\N	3773	f
3774	Jeschke_1963_100105	\N	\N	\N	\N	\N	Ergebnisse der Inventarisierung schutzwürdiger Moore und Gewässer in Mecklenburg	100105		\N	\N	\N		\N	\N	\N	67	67	Y			\N		\N	article	2016-05-04 11:10:09.209	\N	1963	\N	3775	f
3776	Jeschke_1964_100106	\N	\N	\N	\N	\N	Das Blaue Wasser bei Eldena - ein botanisches Naturdenkmal	100106		\N	\N	\N		\N	\N	\N	31	31	Y			\N		\N	article	2016-05-04 11:10:09.429	\N	1964	\N	3777	f
3778	Jeschke_1964_100107	\N	\N	\N	\N	\N	Die Vegetation der Stubnitz (NSG "Jasmund" auf Rügen)	100107		\N	\N	\N		DUBLETTE000197	\N	\N	947	947	Y			000197		\N	article	2016-05-04 11:10:09.691	\N	1964	\N	3779	f
3784	Jeschke_1968_100110	\N	\N	\N	\N	\N	Die Vegetation der Insel Ruden (Naturschutzgebiet Peenemünder Haken und Struck)	100110		\N	\N	\N		\N	\N	\N	120	120	Y			\N		\N	article	2016-05-04 11:10:10.407	\N	1968	\N	3785	f
3786	Jeschke_1968_100111	\N	\N	\N	\N	\N	Das Hechtsoll bei Gubkow	100111		\N	\N	\N		\N	\N	\N	8	8	Y			\N		\N	article	2016-05-04 11:10:10.665	\N	1968	\N	3787	f
3790	Jeschke_1973_100113	\N	\N	\N	\N	\N	Die Buxbaum Segge (Carex buxbaumii) in NO-Mecklenburg	100113		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:11.187	\N	1973	\N	3791	f
3792	Jeschke_1974_100114	\N	\N	\N	\N	\N	Die Wacholderdrift am Spukloch im Naturschutzgebiet Ostufer der Müritz	100114		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:11.496	\N	1974	\N	3793	f
3794	Jeschke_1975_100115	\N	\N	\N	\N	\N	Zur Orchideenflora des Naturschutzgebietes Wallberg bei Alt-Gatschow - ein Beitrag zum Problem der Pflege von Naturschutzgebieten	100115		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:11.727	\N	1975	\N	3795	f
3796	Jeschke_1975_100116	\N	\N	\N	\N	\N	Die Bretziner Heide im Kreis Hagenow	100116		\N	\N	\N		\N	\N	\N	50	50	Y			\N		\N	article	2016-05-04 11:10:11.961	\N	1975	\N	3797	f
3798	Jeschke_1976_100117	\N	\N	\N	\N	\N	Veränderungen des Röhrichtgürtels der Seen in unseren Naturschutzgebieten	100117		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:12.232	\N	1976	\N	3799	f
3800	Jeschke_1979_100118	\N	\N	\N	\N	\N	Zur Flora des Kleinen Weißen Sees bei Wesenberg in Mecklenburg	100118		\N	\N	\N		\N	\N	\N	5	5	Y			\N		\N	article	2016-05-04 11:10:12.537	\N	1979	\N	3801	f
3802	Jeschke_1982_100119	\N	\N	\N	\N	\N	Die Bedeutung des Naturschutzgebietes Ostufer der Müritz für die Erhaltung bedrohter Pflanzenarten	100119		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:12.844	\N	1982	\N	3803	f
3804	Jeschke_1983_100120	\N	\N	\N	\N	\N	Landeskulturelle Probleme des Salzgraslandes an der Küste	100120		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:13.084	\N	1983	\N	3805	f
3806	Jeschke_1985_100121	\N	\N	\N	\N	\N	Vegetationsveränderungen in den Küstenlandschaften durch Massentourismus und Nutzungsintensivierung	100121	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:13.312	\N	1985	\N	3807	f
3808	Jeschke_1986_100122	\N	\N	\N	\N	\N	Mecklenburgische Regenmoore als Naturschutzgebiete	100122		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:13.572	\N	1986	\N	3809	f
3810	Jeschke_1987_100123	\N	\N	\N	\N	\N	Vegetationsdynamik des Salzgraslandes im Bereich der Ostseeküste der DDR unter dem Einfluß des Menschen	100123		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:13.8	\N	1987	\N	3811	f
3813	Jeschke_Erdmann_1984_100124	\N	\N	\N	\N	\N	Grasland auf Niedermoor - landeskulturelle Probleme	100124		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:14.103	\N	1984	\N	3814	f
3816	Jeschke_Schmidt_1959_100125	\N	\N	\N	\N	\N	Landeskulturelle Bearbeitung der LPG-Flur Rotes Banner Groß Nemerow, Kreis Neubrandenburg	100125		\N	\N	\N		\N	\N	\N	26	26	Y			\N		\N	article	2016-05-04 11:10:14.355	\N	1959	\N	3817	f
3819	Kaussmann_Murr_1990_100126	\N	\N	\N	\N	\N	Das Brachypodietum pinnati LIBB.30 (Fieder-Zwenken-Steppenrasen) im Randowtal	100126	Rostock	\N	\N	\N		\N	\N	\N	15	15	Y			\N		\N	article	2016-05-04 11:10:14.614	\N	1990	\N	3820	f
3821	Kaussmann_Kudoke_1962_100127	\N	\N	\N	\N	\N	Leitpflanzen des Rostocker Raumes III	100127	Rostock	\N	\N	\N		\N	\N	\N	9	9	Y			\N		\N	article	2016-05-04 11:10:14.882	\N	1962	\N	3822	f
3823	Kaussmann_Kudoke_1973_100128	\N	\N	\N	\N	\N	Die ökologisch- soziologischen Artengruppen der Ackerunkrautvegetation für den Norden der DDR	100128	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:15.196	\N	1973	\N	3824	f
3825	Kaussmann_Kudoke_1975_100129	\N	\N	\N	\N	\N	Studien zur Vegetationszusammensetzung und zur Bodenentwicklung eines mesophilen Trockenrasens bei Rerik	100129		\N	\N	\N		\N	\N	\N	4	4	Y			\N		\N	article	2016-05-04 11:10:15.418	\N	1975	\N	3826	f
3827	Kaussmann_Kudoke_Murr_1982_100130	\N	\N	\N	\N	\N	Ein Fiederzwenken-Steppenrasen (Brachypodietum pinnati LIBB. 30) im Randowtal	100130	Rostock	\N	\N	\N		\N	\N	\N	4	4	Y			\N		\N	article	2016-05-04 11:10:15.637	\N	1982	\N	3828	f
3829	Kaussmann_Kudoke_Murr_1982_100131	\N	\N	\N	\N	\N	Die Vegetations- und Standorteinheiten der Ackerflächen im Meßtischblatt Thurow bei Neustrelitz	100131		\N	\N	\N		\N	\N	\N	26	26	Y			\N		\N	article	2016-05-04 11:10:15.944	\N	1982	\N	3830	f
3831	Kaussmann_Kudoke_Murr_1982_100132	\N	\N	\N	\N	\N	Die Vegetationsverhältnisse des Wilsickower Os	100132	Rostock	\N	\N	\N		\N	\N	\N	76	76	Y			\N		\N	article	2016-05-04 11:10:16.192	\N	1982	\N	3832	f
3833	Kaussmann_Ribbe_1968_100133	\N	\N	\N	\N	\N	Die Vegetationsverhältnisse im Naturschutzgebiet Töpferberg	100133	Rostock	\N	\N	\N		\N	\N	\N	133	133	Y			\N		\N	article	2016-05-04 11:10:16.434	\N	1968	\N	3834	f
3835	Kintzel_1986_100134	\N	\N	\N	\N	\N	Ruderal- und Segetalarten in den Dörfern des Kreises Lübz	100134	Rostock	\N	\N	\N		\N	\N	\N	30	30	Y			\N		\N	article	2016-05-04 11:10:16.699	\N	1986	\N	3836	f
3839	Kintzel_1991_100136	\N	\N	\N	\N	\N	Zur Situation der geschützten Pflanzen im Kreis Lübz	100136		\N	\N	\N		\N	\N	\N	26	26	Y			\N		\N	article	2016-05-04 11:10:17.136	\N	1991	\N	3840	f
3841	Kintzel_Ribbe_1979_100137	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen der Trockenrasen auf den Inseln im Naturschutzgebiet Quasliner Moor (Kreis Lübz) I	100137	Rostock	\N	\N	\N		\N	\N	\N	41	41	Y			\N		\N	article	2016-05-04 11:10:17.416	\N	1979	\N	3842	f
3843	Kintzel_Ribbe_1982_100138	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen der Trockenrasen auf den Inseln im Naturschutzgebiet Quasliner Moor (Kreis Lübz) II	100138	Rostock	\N	\N	\N		\N	\N	\N	35	35	Y			\N		\N	article	2016-05-04 11:10:17.685	\N	1982	\N	3844	f
3849	Klaus_Kudoke_1965_100140	\N	\N	\N	\N	\N	Vegetationskundliche Beobachtungen im Naturschutzgebiet Heiligen-See nördlich von Markgrafenheide	100140	Rostock	\N	\N	\N		\N	\N	\N	8	8	Y			\N		\N	article	2016-05-04 11:10:18.197	\N	1965	\N	3850	f
3846	Klafs_Jeschke_Et_Al._1973_100139	\N	\N	\N	\N	\N	Genese und Systematik wasserführender Ackerhohlformen in den Nordbezirken der DDR	100139	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:17.964	\N	1973	\N	3847	f
3855	Klemm_1975_100142	\N	\N	\N	\N	\N	Das Lebermoos Jamesoniella undulifolia (NEES)K.MÜLLER neu für Mecklenburg	100142		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:18.744	\N	1975	\N	3856	f
3859	Klemm_1981_100144	\N	\N	\N	\N	\N	Dianthus armeria L. - eine seltene Art für Mecklenburg	100144		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:19.216	\N	1981	\N	3860	f
3861	Kloss_1960_100145	\N	\N	\N	\N	\N	Ackerunkrautgesellschaften der Umgebung von Greifswald (Ostmecklenburg)	100145		\N	\N	\N		\N	\N	\N	106	106	Y			\N		\N	article	2016-05-04 11:10:19.425	\N	1960	\N	3862	f
3863	Kloss_1962_100146	\N	\N	\N	\N	\N	Kalkholde Birkenbruchwälder in Ost- Mecklenburg	100146		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:19.677	\N	1962	\N	3864	f
3867	Kloss_1966_100149	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Grünlandes der Friedländer Großen Wiese (Ostmecklenburg)	100149	Berlin	\N	\N	\N		\N	\N	\N	98	98	Y			\N		\N	article	2016-05-04 11:10:20.097	\N	1966	\N	3868	f
3869	Kloss_1966_100150	\N	\N	\N	\N	\N	Soziologische Artengruppen im Grünland der Friedländer Großen Wiese (Ostmecklenburg)	100150		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:20.309	\N	1966	\N	3870	f
3871	Kloss_1969_100151	\N	\N	\N	\N	\N	Salzvegetation an der Boddenküste Westmecklenburgs (Wismar-Bucht)	100151		\N	\N	\N		\N	\N	\N	157	157	Y			\N		\N	article	2016-05-04 11:10:20.56	\N	1969	\N	3872	f
3873	Kloss_1970_100152	\N	\N	\N	\N	\N	Veränderungen in der Grünlandvegetation der Friedländer Großen Wiese von 1960-1967	100152		\N	\N	\N		\N	\N	\N	25	25	Y			\N		\N	article	2016-05-04 11:10:20.813	\N	1970	\N	3874	f
4067	Schmidt_1964_100243	\N	\N	\N	\N	\N	Pionierstadien und Sukzessionen der Vegetation am Steilufer	100243		\N	\N	\N		\N	\N	\N	9	9	Y			\N		\N	article	2016-05-04 11:10:42.261	\N	1964	\N	4068	f
3875	Kloss_Succow_1966_100153	\N	\N	\N	\N	\N	Karten zur Pflanzengeographie Mecklenburgs.Dritte Reihe.Salz- und Strandpflanzen (1.Teil)	100153		\N	\N	\N		\N	\N	\N	6	6	Y			\N		\N	article	2016-05-04 11:10:21.11	\N	1966	\N	3876	f
3877	Knapp_1976_100154	\N	\N	\N	\N	\N	Zur Salzflora von Gager auf Mönchgut (Insel Rügen)	100154		\N	\N	\N		\N	\N	\N	5	5	Y			\N		\N	article	2016-05-04 11:10:21.373	\N	1976	\N	3878	f
3879	Knapp_1982_100155	\N	\N	\N	\N	\N	Orobanche arenaria BORKH.und Orobanche purpurea JACQ.bei Waren	100155		\N	\N	\N		\N	\N	\N	8	8	Y			\N		\N	article	2016-05-04 11:10:21.622	\N	1982	\N	3880	f
3881	Knapp_1984_100156	\N	\N	\N	\N	\N	Vegetationskomplexe xerothermer Waldgrenzstandorte und anthropogener Trockenrasen im Gebiet der DDR	100156		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:21.877	\N	1984	\N	3882	f
3883	Knapp_1987_100157	\N	\N	\N	\N	\N	Waldvegetationsformen auf Mineralbodenstandorten im pleistozänen Tiefland der DDR	100157		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:22.133	\N	1987	\N	3884	f
3886	Knapp_Hacker_1984_100158	\N	\N	\N	\N	\N	Zur Einbürgerung von Telekia speciosa (Schreb. Baumg.in Mecklenburg	100158	Berlin	\N	\N	\N		\N	\N	\N	52	52	Y			\N		\N	article	2016-05-04 11:10:22.344	\N	1984	\N	3887	f
3895	Koeck_1988_100162	\N	\N	\N	\N	\N	Verbreitung, Ausbreitungsgeschichte, Soziologie und Ökologie von Corispermum leptopterum (Aschers. Iljin in der DDR. II	100162		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:23.329	\N	1988	\N	3896	f
3898	Kopp_1969_100163	\N	\N	\N	\N	\N	Der standörtliche Weiserwert der Wald-Bodenvegetation im nordostdeutschen Tiefland	100163		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:23.538	\N	1969	\N	3899	f
3901	Krausch_1965_100164	\N	\N	\N	\N	\N	Zur Gliederung des Scirpo-Phragmitetum medioeuropaeum W.Koch 26	100164	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:23.808	\N	1965	\N	3902	f
3903	Krausch_1969_100166	\N	\N	\N	\N	\N	Über die Unkrautvegetation von Schafschwingel-Feldern in SW-Mecklenburg und N-Brandenburg	100166		\N	\N	\N		\N	\N	\N	17	17	Y			\N		\N	article	2016-05-04 11:10:24.064	\N	1969	\N	3904	f
3907	Krisch_1974_100168	\N	\N	\N	\N	\N	Wirtschaftsgrünland, Röhrichte und Seggenriede der Ryckniederung (Nordost-Mecklenburg)	100168	Berlin	\N	\N	\N		DUBLETTE000227	\N	\N	356	356	Y			000227		\N	article	2016-05-04 11:10:24.583	\N	1974	\N	3908	f
3909	Krisch_1974_100169	\N	\N	\N	\N	\N	Zur Kenntnis der Pflanzengesellschaften der mecklenburgischen Boddenküste	100169	Berlin	\N	\N	\N		\N	\N	\N	184	184	Y			\N		\N	article	2016-05-04 11:10:24.879	\N	1974	\N	3910	f
3911	Krisch_1978_100170	\N	\N	\N	\N	\N	Ein Zwischenmoor bei Züssow mit Scheuchzeria palustris L	100170	Berlin	\N	\N	\N		\N	\N	\N	14	14	Y			\N		\N	article	2016-05-04 11:10:25.153	\N	1978	\N	3912	f
3913	Krisch_1978_100171	\N	\N	\N	\N	\N	Die Abhängigkeit der Phragmites-Röhrichte am Greifswalder Bodden von edaphischen Faktoren und von der Exponiertheit des Standortes	100171	Berlin	\N	\N	\N		\N	\N	\N	4	4	Y			\N		\N	article	2016-05-04 11:10:25.408	\N	1978	\N	3914	f
3915	Krisch_1981_100172	\N	\N	\N	\N	\N	Agropyron x obtusiusculum LANGE als Neophyt am Greifswalder Bodden	100172	Berlin	\N	\N	\N		\N	\N	\N	24	24	Y			\N		\N	article	2016-05-04 11:10:25.665	\N	1981	\N	3916	f
3921	Krisch_1989_100175	\N	\N	\N	\N	\N	Die Brackwasser-Röhrichte des Greifswalder Boddens	100175		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:26.484	\N	1989	\N	3922	f
3923	Krisch_1990_100176	\N	\N	\N	\N	\N	Die Tangwall- und Spülsaumvegetation der Boddenküste	100176	Göttingen	\N	\N	\N		\N	\N	\N	124	124	Y			\N		\N	article	2016-05-04 11:10:26.695	\N	1990	\N	3924	f
3925	Krisch_1990_100177	\N	\N	\N	\N	\N	Ökologisch-soziologische Artengruppen und Pflanzengesellschaften im Geolitoral der Boddenküste (Ostsee)	100177		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:26.908	\N	1990	\N	3926	f
3927	Krisch_1992_100178	\N	\N	\N	\N	\N	Systematik und Ökologie der Bolboschoenus- und der Phragmites-Brackwasserröhrichte der vorpommerschen Boddenküste (Ostsee)	100178		\N	\N	\N		\N	\N	\N	87	87	Y			\N		\N	article	2016-05-04 11:10:27.201	\N	1992	\N	3928	f
3930	Krull_1982_100179	\N	\N	\N	\N	\N	Lilium martagon L. bei Peckatel (Kr. Neustrelitz)	100179		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:27.45	\N	1982	\N	3931	f
3932	Kudoke_1961_100180	\N	\N	\N	\N	\N	Vegetationsverhältnisse im Naturschutzgebiet Peetscher Moor bei Bützow	100180	Rostock	\N	\N	\N		\N	\N	\N	274	274	Y			\N		\N	article	2016-05-04 11:10:27.673	\N	1961	\N	3933	f
3890	Knapp_Voigtlaender_1982_100160	\N	\N	\N	\N	\N	Das Grünland im Naturschutzgebiet Ostufer der Müritz	100160		\N	\N	\N		\N	\N	\N	136	136	Y			\N		\N	article	2016-05-04 11:10:22.798	\N	1982	\N	3891	f
3892	Knapp_Voigtlaender_1983_100161	\N	\N	\N	\N	\N	Die Pflanzenwelt des NSG "Ostufer der Feisneck" bei Waren	100161		\N	\N	\N		\N	\N	\N	119	119	Y			\N		\N	article	2016-05-04 11:10:23.073	\N	1983	\N	3893	f
3940	Kudoke_1979_100184	\N	\N	\N	\N	\N	Untersuchungen zu Wurzelverhältnissen einiger Ackerunkrautgemeinschaften des Rostocker Raumes I. Mitt	100184	Rostock	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:28.554	\N	1979	\N	3941	f
3942	Kudoke_1983_100185	\N	\N	\N	\N	\N	Untersuchungen zu Wurzelverhältnissen einiger Ackerunkrautgemeinschaften des Rostocker Raumes II. Mitt	100185		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:28.846	\N	1983	\N	3943	f
3944	Kudoke_1990_100186	\N	\N	\N	\N	\N	Erneute vegetationskundliche Untersuchungen in Arnoseris-minima-Fluren des Rostocker Raumes	100186		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:29.058	\N	1990	\N	3945	f
3947	Lampe_1992_100187	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen an stehenden Kleingewässern der Rostocker Umgebung	100187		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:29.307	\N	1992	\N	3948	f
3952	Libbert_1940_100189	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der Halbinsel Darß (Vorpommern)	100189	Berlin	\N	\N	\N		DUBLETTE000234	\N	\N	198	198	Y			000234		\N	article	2016-05-04 11:10:29.794	\N	1940	\N	3953	f
3955	Lindner_1976_100190	\N	\N	\N	\N	\N	Verbreitungskarten der produktionsbiologisch wichtigen Makrophyten und Makrophytengesellschaften in der Boddenkette südlich des Zingst und des Darß	100190		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:30.09	\N	1976	\N	3956	f
3957	Lindner_1978_100191	\N	\N	\N	\N	\N	Soziologisch-ökologische Untersuchungen an der submersen Vegetation in der Boddenkette südlich des Darß und Zingst (südliche Ostsee)	100191	Berlin	\N	\N	\N		\N	\N	\N	158	158	Y			\N		\N	article	2016-05-04 11:10:30.337	\N	1978	\N	3958	f
3962	Martin_1985_100193	\N	\N	\N	\N	\N	Die Segetalvegetation in den Winterungen Raps, Wintergerste und Winterweizen auf dem Territorium des Kreises Güstrow	100193		\N	\N	\N		\N	\N	\N	1876	1876	Y			\N		\N	article	2016-05-04 11:10:30.756	\N	1985	\N	3963	f
3965	Mattick_1931_100194	\N	\N	\N	\N	\N	Mikroklimatische und Vegetationsuntersuchungen auf der Insel Vilm (Rügen)	100194		\N	\N	\N		\N	\N	\N	7	7	Y			\N		\N	article	2016-05-04 11:10:30.98	\N	1931	\N	3966	f
3970	Meusel_1951_100196	\N	\N	\N	\N	\N	Vegetationskundliche Studien über mitteleuropäische Waldgesellschaften III. Über einige Waldgesellschaften der Insel Rügen	100196		\N	\N	\N		\N	\N	\N	5	5	Y			\N		\N	article	2016-05-04 11:10:31.437	\N	1951	\N	3971	f
3968	Messner_1985_100195	\N	\N	\N	\N	\N	Die Vegetation des Küstenvogel- schutzgebietes NSG "Inseln Böhmke und Werder"	100195		\N	\N	\N		\N	\N	\N	78	78	Y			\N		\N	article	2016-05-04 11:10:31.185	\N	1985	\N	3969	f
3978	Pankow_Mahnke_1963_100199	\N	\N	\N	\N	\N	Die Vegetation der Insel Walfisch	100199	Rostock	\N	\N	\N		\N	\N	\N	8	8	Y			\N		\N	article	2016-05-04 11:10:32.218	\N	1963	\N	3979	f
3981	Pankow_Pulz_1965_100200	\N	\N	\N	\N	\N	Die Vegetation des Naturschutzgebietes Sabelsee	100200		\N	\N	\N		\N	\N	\N	78	78	Y			\N		\N	article	2016-05-04 11:10:32.476	\N	1965	\N	3982	f
3972	Mueller-Stoll_Pietsch_1985_100197	\N	\N	\N	\N	\N	Das Samolo-Cyperetum fusci, eine neue Eu-Nanocyperion flavescentis-Gesellschaft aus Mitteleuropa	100197	Göttingen	\N	\N	\N		\N	\N	\N	6	6	Y			\N		\N	article	2016-05-04 11:10:31.745	\N	1985	\N	3973	f
3988	Passarge_Passarge_1973_100202	\N	\N	\N	\N	\N	Zur soziologischen Gliederung von Sandstrandgesellschaften der Ostseeküste	100202	Berlin	\N	\N	\N		\N	\N	\N	63	63	Y			\N		\N	article	2016-05-04 11:10:32.984	\N	1973	\N	3989	f
3990	Passarge_1957_100203	\N	\N	\N	\N	\N	Über Kahlschlaggesellschaften im baltischen Buchenwald von Dargun (Ost-Mecklenburg)	100203		\N	\N	\N		\N	\N	\N	21	21	Y			\N		\N	article	2016-05-04 11:10:33.209	\N	1957	\N	3991	f
3992	Passarge_1959_100204	\N	\N	\N	\N	\N	Zur Gliederung der Polygono-Chenopodion- Gesellschaften im Nordostdeutschen Flachland	100204		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:33.505	\N	1959	\N	3993	f
3996	Passarge_1959_100206	\N	\N	\N	\N	\N	Pflanzengesellschaften zwischen Trebel, Grenz-Bach und Peene (O-Mecklenburg)	100206	Berlin	\N	\N	\N		DUBLETTE000269	\N	\N	0	79	Y			000269		\N	article	2016-05-04 11:10:34.062	\N	1959	\N	3997	f
3998	Passarge_1959_100207	\N	\N	\N	\N	\N	Zur geographischen Gliederung der Agrostidion spica-venti-Gesellschaften im nordostdeutschen Flachland	100207		\N	\N	\N		\N	\N	\N	6	6	Y			\N		\N	article	2016-05-04 11:10:34.289	\N	1959	\N	3999	f
4002	Passarge_1960_100209	\N	\N	\N	\N	\N	Zur soziologischen Gliederung binnenländischer Corynephorus-Rasen im nordostdeutschen Flachland	100209	Berlin-Dahlem	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:34.839	\N	1960	\N	4003	f
4004	Passarge_1961_100210	\N	\N	\N	\N	\N	Zur soziologischen Gliederung der Salix cinerea-Gebüsche Norddeutschlands	100210		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:35.076	\N	1961	\N	4005	f
4006	Passarge_1962_100211	\N	\N	\N	\N	\N	Zur Gliederung und Systematik der Kiefernforstgesellschaften im Hagenower Land	100211	Berlin	\N	\N	\N		\N	\N	\N	40	20	Y			\N		\N	article	2016-05-04 11:10:35.31	\N	1962	\N	4007	f
4008	Passarge_1962_100212	\N	\N	\N	\N	\N	Waldgesellschaften des Eichenwaldgebietes von SW-Mecklenburg und der Altmark	100212	Berlin	\N	\N	\N		\N	\N	\N	6	6	Y			\N		\N	article	2016-05-04 11:10:35.597	\N	1962	\N	4009	f
4010	Passarge_1962_100214	\N	\N	\N	\N	\N	Ein bemerkenswertes Heidegebiet im Kreis Hagenow	100214		\N	\N	\N		\N	\N	\N	21	21	Y			\N		\N	article	2016-05-04 11:10:35.868	\N	1962	\N	4011	f
5570	Willkomm_1993_101071	\N	\N	\N	\N	\N	Die Dünenvegetation der Insel Juist.	101071	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:16.845	\N	1993	\N	5571	f
4012	Passarge_1963_100215	\N	\N	\N	\N	\N	Zur soziologischen Gliederung von Kiefernwäldern im nordöstlichen Mitteleuropa	100215	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:36.137	\N	1963	\N	4013	f
4016	Passarge_1964_100217	\N	\N	\N	\N	\N	Pflanzengesellschaften des nordostdeutschen Flachlandes I	100217		\N	\N	\N		DUBLETTE000273	\N	\N	0	\N	Y			000273		\N	article	2016-05-04 11:10:36.726	\N	1964	\N	4017	f
4018	Passarge_1964_100218	\N	\N	\N	\N	\N	Zur soziologischen Gliederung binnenländischer Calluna-Heiden im nordostdeutschen Flachland	100218	Berlin-Dahlem	\N	\N	\N		\N	\N	\N	51	51	Y			\N		\N	article	2016-05-04 11:10:37.005	\N	1964	\N	4019	f
4020	Passarge_1966_100220	\N	\N	\N	\N	\N	Waldgesellschaften der Prignitz	100220	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:37.246	\N	1966	\N	4021	f
4022	Passarge_1967_100221	\N	\N	\N	\N	\N	Über Saumgesellschaften im nordostdeutschen Flachland	100221	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:37.495	\N	1967	\N	4023	f
4024	Passarge_1969_100222	\N	\N	\N	\N	\N	Zur soziologischen Gliederung mitteleuropäischer Weißkleeweiden	100222	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:37.763	\N	1969	\N	4025	f
4026	Passarge_1977_100223	\N	\N	\N	\N	\N	Über Initialfluren der Sedo-Scleranthetea auf pleistozänen Böden	100223	Berlin	\N	\N	\N		\N	\N	\N	80	80	Y			\N		\N	article	2016-05-04 11:10:38.033	\N	1977	\N	4027	f
3984	Pankow_Spittler_Et_Al._1967_100201	\N	\N	\N	\N	\N	Beitrag zur Kenntnis der Pflanzengesellschaften vor der Insel Langenwerder (Ostsee, Wismar-Bucht)	100201		\N	\N	\N		\N	\N	\N	50	50	Y			\N		\N	article	2016-05-04 11:10:32.717	\N	1967	\N	3985	f
4031	Poltz_1960_100225	\N	\N	\N	\N	\N	Die Waldvegetation der Lewitz	100225		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:38.523	\N	1960	\N	4032	f
4037	Precker_Knapp_1990_100227	\N	\N	\N	\N	\N	Das Teufelsmoor bei Horst, Kr.Rostock - landeskulturelle Nachnutzung eines industriell abgetorften Regenmoores	100227	Berlin	\N	\N	\N		\N	\N	\N	102	102	Y			\N		\N	article	2016-05-04 11:10:38.984	\N	1990	\N	4038	f
4040	Rehbein_Berg_1987_100228	\N	\N	\N	\N	\N	Zum Vorkommen der Wiesen- Gerste (Hordeum secalinum SCHREB.= H.nodosum auct.) in der Elbeniederung bei Stapel, Kr.Hagenow	100228		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:39.194	\N	1987	\N	4041	f
4042	Ribbe_1960_100229	\N	\N	\N	\N	\N	Die Flora des Naturschutzgebietes Töpferberg	100229		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:39.475	\N	1960	\N	4043	f
4044	Ribbe_1967_100230	\N	\N	\N	\N	\N	Zum Vorkommen von Thesium ebracteatum HAYNE in Mecklenburg	100230	Rostock	\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:39.728	\N	1967	\N	4045	f
4046	Ribbe_1970_100231	\N	\N	\N	\N	\N	Floristische Notitzen aus der Lewitz	100231		\N	\N	\N		\N	\N	\N	23	23	Y			\N		\N	article	2016-05-04 11:10:39.942	\N	1970	\N	4047	f
4050	Ribbe_1973_100233	\N	\N	\N	\N	\N	Die Vegetationsverhältnisse auf der Düne Hühnerberg bei Garwitz (Kr.Parchim)	100233		\N	\N	\N		\N	\N	\N	70	70	Y			\N		\N	article	2016-05-04 11:10:40.412	\N	1973	\N	4051	f
4052	Scamoni_1953_100235	\N	\N	\N	\N	\N	Über lerchenspornreiche Waldgesellschaften im Bereich des Diluviums der DDR	100235	Berlin	\N	\N	\N		\N	\N	\N	15	15	Y			\N		\N	article	2016-05-04 11:10:40.623	\N	1953	\N	4053	f
4054	Scamoni_1957_100236	\N	\N	\N	\N	\N	Vegetationsstudien im Waldschutzgebiet Fauler Ort und in den angrenzenden Waldungen	100236	Berlin	\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:40.872	\N	1957	\N	4055	f
4056	Scamoni_1964_100237	\N	\N	\N	\N	\N	Karte der natürlichen Vegetation der Deutschen Demokratischen Republik (1:500000) mit Erläuterungen	100237	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:41.123	\N	1964	\N	4057	f
4058	Scamoni_1963_100238	\N	\N	\N	\N	\N	Natur, Entwicklung und Wirtschaft einer Jungpleistozänen Landschaft, dargestellt am Gebiet des Meßtischblattes Thurow (Kr. Neustrelitz) Teil 1	100238		\N	\N	\N		\N	\N	\N	888	888	Y			\N		\N	article	2016-05-04 11:10:41.333	\N	1963	\N	4059	f
4060	Scamoni_1965_100239	\N	\N	\N	\N	\N	Vegetationskundliche und standortkundliche Untersuchungen in mecklenburgischen Wald- schutzgebieten	100239		\N	\N	\N		\N	\N	\N	242	242	Y			\N		\N	article	2016-05-04 11:10:41.544	\N	1965	\N	4061	f
4062	Scamoni_1985_100240	\N	\N	\N	\N	\N	Über das ökologische und soziolgische Verhalten von Rhamnus carthatica L. und Frangula alnus MILL. in der planaren Stufe der DDR	100240	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:41.795	\N	1985	\N	4063	f
4073	Schneider_Kaiser_1991_100245	\N	\N	\N	\N	\N	Landschaftskundliche Erhebungen im Müritz-Steilufergebiet bei Rechlin (Gemarkung Klopzow)	100245		\N	\N	\N		\N	\N	\N	34	34	Y			\N		\N	article	2016-05-04 11:10:42.685	\N	1991	\N	4074	f
4079	Schoknecht_1988_100247	\N	\N	\N	\N	\N	Ein Vorkommen von Urtica kioviensis ROGOW.in Mittelmecklenburg	100247		\N	\N	\N		\N	\N	\N	5	5	Y			\N		\N	article	2016-05-04 11:10:43.182	\N	1988	\N	4080	f
4084	Slobodda_1979_100249	\N	\N	\N	\N	\N	Die Moosvegetation ausgewählter Pflanzengesellschafen des NSG "Peenewiesen" bei Gützkow unter Berücksichtigung der ökologischen Bedingungen eines Flußtalmoor-Standortes	100249	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:43.6	\N	1979	\N	4085	f
4086	Slobodda_1979_100250	\N	\N	\N	\N	\N	Schwarzerlen-Eschenwald-Ausbildungen im Wasserscheidemoor des Kuckucksgrabens bei Müssentin (Kr.Demmin)	100250		\N	\N	\N		\N	\N	\N	9	9	Y			\N		\N	article	2016-05-04 11:10:43.811	\N	1979	\N	4087	f
4088	Slobodda_1979_100251	\N	\N	\N	\N	\N	Überblick über die Vegetation des unteren Peenetales nordöstlich von Anklam (Talabschnitt Rothe Moor)	100251		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:44.029	\N	1979	\N	4089	f
4092	Slobodda_1985_100253	\N	\N	\N	\N	\N	Vegetationsformen und ihre Darstellung in einer Phytotopkarte am Beispiel der Schmachter See-Niederung bei Binz/Rügen	100253	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:44.807	\N	1985	\N	4093	f
4095	Slobodda_Rudnitzki_1981_100254	\N	\N	\N	\N	\N	Beitrag zur Genese und naturräumlichen Charakteristik einer holozänen Niederung (Wosteritzer Seen auf Rügen) durch Auswertung vegetationsgeographischer Untersuchungsergebnisse	100254		\N	\N	\N		\N	\N	\N	5	5	Y			\N		\N	article	2016-05-04 11:10:45.065	\N	1981	\N	4096	f
4076	Schoeneich_1979_100246	\N	\N	\N	\N	\N	Die Wanderdüne bei Stixe (Neuhauser Elbdünen) als Naturschutzgebiet im Kreis Hagenow	100246		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:42.892	\N	1979	\N	4077	f
4101	Stegemann_Doll_1975_100256	\N	\N	\N	\N	\N	Das Schwarze-See-Bruch bei Goldenbaum (Kr. Neustrelitz)	100256		\N	\N	\N		\N	\N	\N	7	7	Y			\N		\N	article	2016-05-04 11:10:45.578	\N	1975	\N	4102	f
4103	Stegemann_Doll_1976_100257	\N	\N	\N	\N	\N	Der Mümmelsee bei Drewin	100257		\N	\N	\N		\N	\N	\N	10	10	Y			\N		\N	article	2016-05-04 11:10:45.892	\N	1976	\N	4104	f
4105	Stegemann_Doll_1976_100258	\N	\N	\N	\N	\N	Zur Flora und Vegetation des Comthureyer Berges bei Wokuhl	100258		\N	\N	\N		\N	\N	\N	10	10	Y			\N		\N	article	2016-05-04 11:10:46.132	\N	1976	\N	4106	f
4107	Doll_Stegemann_1977_100259	\N	\N	\N	\N	\N	Der Kleine Kulowsee bei Fürstensee	100259		\N	\N	\N		\N	\N	\N	7	7	Y			\N		\N	article	2016-05-04 11:10:46.406	\N	1977	\N	4108	f
4109	Succow_1967_100260	\N	\N	\N	\N	\N	Pflanzengesellschaften der Ziesenniederung (Ostmecklenburg)	100260		\N	\N	\N		DUBLETTE000035	\N	\N	251	251	Y			000035		\N	article	2016-05-04 11:10:46.675	\N	1967	\N	4110	f
4111	Succow_1968_100261	\N	\N	\N	\N	\N	Das Kalkflachmoor im Augraben-Tal zwischen Zehlendorf und Recknitz (Kr.Güstrow)	100261		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:46.888	\N	1968	\N	4112	f
4113	Succow_1969_100262	\N	\N	\N	\N	\N	Die anthropogene Umwandlung der ursprünglichen Vegetation der nordmecklenburgischen Flußtalmoore. 2.Kolloquium Naturschutz im Ostseeraum	100262		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:47.101	\N	1969	\N	4114	f
4099	Sluschny_Matthes_Et_Al._1985_100255	\N	\N	\N	\N	\N	Die Bitterkraut- Sommerwurz (Orobanche picridis F.SCHULTZ) - neu in Mecklenburg	100255		\N	\N	\N		\N	\N	\N	4	4	Y			\N		\N	article	2016-05-04 11:10:45.297	\N	1985	\N	4100	f
4119	Succow_1974_100265	\N	\N	\N	\N	\N	Vorschlag einer systematischen Neugliederung der mineral-bodenwasserbeeinflußten wachsenden Moorvegetation unter Ausklammerung des Gebirgsraumes	100265	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:47.784	\N	1974	\N	4120	f
4121	Succow_1986_100266	\N	\N	\N	\N	\N	Standorts- und Vegetationswandel der intensiv landwirtschaftlich genutzten Niedermoore der DDR	100266	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:48.038	\N	1986	\N	4122	f
4123	Succow_1987_100267	\N	\N	\N	\N	\N	Zum aktuellen Vegetationswandel des Graslandes auf Niederungsstandorten der DDR	100267		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:48.245	\N	1987	\N	4124	f
4128	Szameitat_1989_100269	\N	\N	\N	\N	\N	Impatiens glandulifera ROYLE als Bachbegleiter in Ostmecklenburg	100269		\N	\N	\N		\N	\N	\N	6	6	Y			\N		\N	article	2016-05-04 11:10:48.705	\N	1989	\N	4129	f
4134	Treichel_1990_100271	\N	\N	\N	\N	\N	Angewandt-vegetationskundliche Untersuchungen in den Ackerfluren des Meßtischblattes (2042) Gnoien (Bezirk Neubrandenburg) und die Ermittlung der Standorteinheiten	100271	Rostock	\N	\N	\N		\N	\N	\N	3	3	Y			\N		\N	article	2016-05-04 11:10:49.209	\N	1990	\N	4135	f
4136	Treichel_Pankow_1975_100272	\N	\N	\N	\N	\N	Über ein Vorkommen des Langblättrigen Blauweiderich, Pseudolysimachion longifolium (L.)OPIZ im Kreis Malchin	100272		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:49.423	\N	1975	\N	4137	f
4139	Voderberg_1955_100273	\N	\N	\N	\N	\N	Die Vegetationsentwicklung der neugeschaffenen Insel Bock	100273	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:49.705	\N	1955	\N	4140	f
4142	Voderberg_Froede_1959_100274	\N	\N	\N	\N	\N	Die Vegetationsentwicklung auf der Insel Bock	100274	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:49.954	\N	1959	\N	4143	f
4144	Voderberg_Froede_1963_100275	\N	\N	\N	\N	\N	Die Vegetationsentwicklung auf der Insel Bock in den Jahren 1956-1961	100275	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:50.184	\N	1963	\N	4145	f
4146	Voderberg_Froede_1967_100276	\N	\N	\N	\N	\N	Abschließende Betrachtung der Vegetationsentwicklung auf der Insel Bock in den Jahren 1946-1966	100276	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:50.48	\N	1967	\N	4147	f
4165	Weber_1985_100285	\N	\N	\N	\N	\N	Floristische Erfassung des NSG "Zahren-See"	100285		\N	\N	\N		\N	\N	\N	8	8	Y			\N		\N	article	2016-05-04 11:10:52.792	\N	1985	\N	4166	f
4168	Wegener_1982_100286	\N	\N	\N	\N	\N	Wasserpflanzengesellschaften im Ryck-, Riene- und Bachgraben und ihre hydrochemischen Umweltbedingungen	100286	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:52.997	\N	1982	\N	4169	f
4171	Wegener_1986_100287	\N	\N	\N	\N	\N	Beiträge zur Flora von Mecklenburg	100287		\N	\N	\N		\N	\N	\N	3	3	Y			\N		\N	article	2016-05-04 11:10:53.239	\N	1986	\N	4172	f
4173	Wegener_1991_100288	\N	\N	\N	\N	\N	Pflanzengesellschaften an der Südküste des Greifswalder Boddens	100288	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:53.445	\N	1991	\N	4174	f
4175	Wegener_1992_100289	\N	\N	\N	\N	\N	Wasserpflanzengesellschaften in Ziese und Beck	100289	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:53.729	\N	1992	\N	4176	f
4177	Wollert_1964_100290	\N	\N	\N	\N	\N	Die Grasheiden Mecklenburgs II.Die Vegetationsverhältnisse auf dem Heidberg bei Teterow	100290	Rostock	\N	\N	\N		DUBLETTE000563	\N	\N	18	18	Y			000563		\N	article	2016-05-04 11:10:53.973	\N	1964	\N	4178	f
4179	Wollert_1965_100291	\N	\N	\N	\N	\N	Die Unkrautgesellschaften der Oser Mittelmecklenburgs	100291	Rostock	\N	\N	\N		\N	\N	\N	9	9	Y			\N		\N	article	2016-05-04 11:10:54.247	\N	1965	\N	4180	f
4181	Wollert_1965_100292	\N	\N	\N	\N	\N	Die Vegetation des NSG "Wallberg" bei Alt Gatschow	100292		\N	\N	\N		\N	\N	\N	6	6	Y			\N		\N	article	2016-05-04 11:10:54.532	\N	1965	\N	4182	f
4185	Wollert_1968_100294	\N	\N	\N	\N	\N	Floristische Mitteilungen aus Mittelmecklenburg II	100294	Rostock	\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:55.005	\N	1968	\N	4186	f
4187	Wollert_1969_100295	\N	\N	\N	\N	\N	Zur Verbreitung und zum soziologischen Verhalten des Feldahorns (Acer campestre L.) in Mittelmecklenburg	100295	Berlin	\N	\N	\N		\N	\N	\N	5	5	Y			\N		\N	article	2016-05-04 11:10:55.246	\N	1969	\N	4188	f
4189	Wollert_1970_100296	\N	\N	\N	\N	\N	Zur soziologischen Gliederung und Stellung der Grenzhecken Mittelmecklenburgs und deren Säume	100296		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:55.483	\N	1970	\N	4190	f
4191	Wollert_1973_100297	\N	\N	\N	\N	\N	Der Ahorn-Hang-Wald (Adoxo-Aceretum pseudoplatani) in den Erosionsrinnen zum Malchiner und Teterower Becken (Mittelmecklenburg)	100297		\N	\N	\N		\N	\N	\N	55	55	Y			\N		\N	article	2016-05-04 11:10:55.721	\N	1973	\N	4192	f
4193	Wollert_1978_100298	\N	\N	\N	\N	\N	Ergebnisse der floristischen Kartierung einiger Bach- und Flußtäler des Kreises Teterow (Mecklenburg) I.Das Tal der Polchow	100298		\N	\N	\N		\N	\N	\N	48	48	Y			\N		\N	article	2016-05-04 11:10:56.005	\N	1978	\N	4194	f
4195	Wollert_1979_100299	\N	\N	\N	\N	\N	Zur Flora und Vegetation der Abhänge von Stauchmoränen des Malchiner Beckens bei Remplin, Kreis Malchin (Mecklenburg)	100299		\N	\N	\N		\N	\N	\N	54	54	Y			\N		\N	article	2016-05-04 11:10:56.245	\N	1979	\N	4196	f
4197	Wollert_1980_100300	\N	\N	\N	\N	\N	Zur Verbreitung von Trockenrasenarten an den Hängen des Ostufers des Kummerower Sees	100300		\N	\N	\N		\N	\N	\N	15	15	Y			\N		\N	article	2016-05-04 11:10:56.525	\N	1980	\N	4198	f
4199	Wollert_1981_100301	\N	\N	\N	\N	\N	Die Wiesenniederung zwischen Klein Bützin und Striesenow	100301		\N	\N	\N		\N	\N	\N	11	11	Y			\N		\N	article	2016-05-04 11:10:56.761	\N	1981	\N	4200	f
4201	Wollert_1981_100302	\N	\N	\N	\N	\N	Der Alant-Kriechrasen - eine seltene Agropyro-Rumicion-Gesellschaft am Südostufer des Malchiner Sees (Mecklenburg)	100302		\N	\N	\N		\N	\N	\N	6	6	Y			\N		\N	article	2016-05-04 11:10:57	\N	1981	\N	4202	f
4203	Wollert_1983_100303	\N	\N	\N	\N	\N	Über ein bemerkenswertes Vorkommen der Trollblume (Trollius europaeus L.) bei Carlruh, Kreis Malchin (Mecklenburg)	100303		\N	\N	\N		\N	\N	\N	2	2	Y			\N		\N	article	2016-05-04 11:10:57.252	\N	1983	\N	4204	f
4148	Voigtlaender_1966_100277	\N	\N	\N	\N	\N	Ackerunkrautgesellschaften im Gebiet um Feldberg	100277	Rostock	\N	\N	\N		\N	\N	\N	181	181	Y			\N		\N	article	2016-05-04 11:10:50.702	\N	1966	\N	4149	f
4207	Wollert_1985_100305	\N	\N	\N	\N	\N	Die Mauerrauten-Gesellschaft (Asplenietum ruta-murariae Tx.37) an der Stadtmauer von Neubrandenburg	100305		\N	\N	\N		\N	\N	\N	4	4	Y			\N		\N	article	2016-05-04 11:10:57.877	\N	1985	\N	4208	f
4209	Wollert_1987_100306	\N	\N	\N	\N	\N	Zum Eindringen des Zurückgebogenen Fuchsschwanzes (Amaranthus retroflexus L.) in Hackunkrautgesellschaften des Malchiner Beckens	100306		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:58.154	\N	1987	\N	4210	f
4213	Wollert_1989_100308	\N	\N	\N	\N	\N	Verbreitung, Standortansprüche und soziologische Bindung von Ornithogalum nutans L. und O.boucheanum (KUNTH) ASCHER. in Mittelmecklenburg	100308		\N	\N	\N		\N	\N	\N	16	16	Y			\N		\N	article	2016-05-04 11:10:58.655	\N	1989	\N	4214	f
4215	Wollert_1989_100309	\N	\N	\N	\N	\N	Über einige für Mittel- und Ostmecklenburg neue Ruderalpflanzengesellschaften	100309	Rostock	\N	\N	\N		\N	\N	\N	47	47	Y			\N		\N	article	2016-05-04 11:10:58.947	\N	1989	\N	4216	f
4217	Wollert_1991_100310	\N	\N	\N	\N	\N	Die Ruderalvegetation des Meßtischblattes Teterow (2241; Mittelmecklenburg)	100310	Berlin	\N	\N	\N		\N	\N	\N	12	12	Y			\N		\N	article	2016-05-04 11:10:59.221	\N	1991	\N	4218	f
4219	Wollert_1993_100311	\N	\N	\N	\N	\N	Über ein bemerkenswertes Vorkommen der Trollblume (Trollius europaeus L.) im Landkreis Malchin	100311		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:59.502	\N	1993	\N	4220	f
4221	Wollert_Bolbrinker_1980_100312	\N	\N	\N	\N	\N	Zur Verbreitung sowie zum ökologischen und soziologischen Verhalten von Ceratophyllum submersum L. in Mittelmecklenburg	100312	Rostock	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:59.766	\N	1980	\N	4222	f
4223	Wollert_Haberkost_1981_100313	\N	\N	\N	\N	\N	Das Durchbruchstal der Ostpeene zwischen Karlsruhe und Peenhäuser	100313		\N	\N	\N		DUBLETTE000381	\N	\N	25	25	Y			000381		\N	article	2016-05-04 11:11:00.055	\N	1981	\N	4224	f
4714	Moeller_1997_100470	\N	\N	\N	\N	\N	Kleiner Orant im Waldweg	100470	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	5	5	Y			\N		\N	article	2016-05-04 11:11:39.905	\N	1997	\N	4715	f
4693	Wollert_1997_100461	\N	\N	\N	\N	\N	Zum Vorkommen wärmeanspruchsvoller ruderaler Halbtrockenrasen des Verbandes Convolvulo-Agropyrion Görs 1966 in Mecklenburg-Vorpommern	100461		\N	\N	\N		\N	\N	\N	29	29	Y			\N		\N	article	2016-05-04 11:11:37.618	\N	1997	\N	4694	f
4695	Kintzel_1997_100462	\N	\N	\N	\N	\N	Das Convolvulo-Brometum carinati ass. nov. - ein neuer ruderaler Halbtrockenrasen in Mecklenburg-Vorpommern	100462		\N	\N	\N		\N	\N	\N	30	30	Y			\N		\N	article	2016-05-04 11:11:37.85	\N	1997	\N	4696	f
4697	Clausnitzer_1997_100463	\N	\N	\N	\N	\N	Ein bemerkenswertes Massenvorkommen von Potamogeton alpinus in der Recknitz (Mecklenburg-Vorpommern)	100463		\N	\N	\N		\N	\N	\N	86	86	Y			\N		\N	article	2016-05-04 11:11:38.147	\N	1997	\N	4698	f
4699	Kintzel_1997_100464	\N	\N	\N	\N	\N	Floristische Beiträge aus dem Landkreis Parchim (III)	100464		\N	\N	\N		\N	\N	\N	15	15	Y			\N		\N	article	2016-05-04 11:11:38.399	\N	1997	\N	4700	f
4701	Rehbein_Berg_1997_100465	\N	\N	\N	\N	\N	Bemerkenswerte Pflanzen und Pflanzengesellschaften aus dem Rostocker Raum I: Melampyrum cristatum L	100465		\N	\N	\N		\N	\N	\N	17	17	Y			\N		\N	article	2016-05-04 11:11:38.609	\N	1997	\N	4702	f
4722	Manthey_Seiberling_1998_100473	\N	\N	\N	\N	\N	Verbreitung, Standort und Vergesellschaftung von Eleocharis parvula	100473	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	21	21	Y			\N		\N	article	2016-05-04 11:11:40.702	\N	1998	\N	4723	f
4724	Rehbein_Berg_1998_100474	\N	\N	\N	\N	\N	Bemerkenswerte Pflanzen und Pflanzengesellschaften aus dem Rostocker Raum II: Scorzonera humilis L	100474	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	13	13	Y			\N		\N	article	2016-05-04 11:11:40.917	\N	1998	\N	4725	f
4726	Bolbrinker_1998_100475	\N	\N	\N	\N	\N	Die Rote Wasserlinse (Lemna turionifera) auf den Gewässern Mecklenburg-Vorpommerns	100475	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	35	35	Y			\N		\N	article	2016-05-04 11:11:41.15	\N	1998	\N	4727	f
4728	Bolbrinker_1998_100476	\N	\N	\N	\N	\N	Ein neues Vorkommen des Quirl-Tännel (Elatine alsinastrum) in Mecklenburg-Vorpommern	100476	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:11:41.422	\N	1998	\N	4729	f
4730	Fischer_1998_100477	\N	\N	\N	\N	\N	Rhynchospora fusca - Wiederfund auf Hiddensee und ehemalige Vorkommen in Mecklenburg-Vorpommern	100477	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	3	3	Y			\N		\N	article	2016-05-04 11:11:41.717	\N	1998	\N	4731	f
4228	Wollert_1992_100315	\N	\N	\N	\N	\N	Die Vegetation des Kalk-Zwischenmoores Wendischhagen am Malchiner See (Mecklenburg)	100315		\N	\N	\N		\N	\N	\N	33	33	Y			\N		\N	article	2016-05-04 11:11:00.609	\N	1992	\N	4229	f
4231	Zabel_1973_100316	\N	\N	\N	\N	\N	Unkrautgesellschaften der Winterkulturen des Güstrower Beckens	100316		\N	\N	\N		\N	\N	\N	88	88	Y			\N		\N	article	2016-05-04 11:11:00.875	\N	1973	\N	4232	f
4234	Zabel_Polke_1974_100317	\N	\N	\N	\N	\N	Unkrautgesellschaften der Sommerkulturen (Hackfrüchte) des Güstrower Beckens	100317		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:11:01.123	\N	1974	\N	4235	f
4671	Voigtlaender_1996_100455	\N	\N	\N	\N	\N	Pflege- und Entwicklungsplan für das NSG "Santower See", Teil 2	100455		\N	\N	\N		\N	\N	\N	150	150	Y			\N		\N	report	2016-05-04 11:11:36.117	\N	1996	4672	\N	f
4289	Clausnitzer_1996_100332	\N	\N	\N	\N	\N	Zur bachbegleitenden Vegetation von Beke und Waidbach	100332		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:11:04.82	\N	1996	\N	4290	f
4291	Doll_1992_100333	\N	\N	\N	\N	\N	Die Vegetation des Krüselinsees bei Feldberg in Mecklenburg	100333	Berlin	\N	\N	\N		\N	\N	\N	58	58	Y			\N		\N	article	2016-05-04 11:11:05.04	\N	1992	\N	4292	f
4293	Doll_1992_100334	\N	\N	\N	\N	\N	Die Vegetation der Krummen Seen bei Lychen und Kratzeburg in Mecklenburg-Vorpommern	100334	Berlin	\N	\N	\N		\N	\N	\N	48	48	Y			\N		\N	article	2016-05-04 11:11:05.314	\N	1992	\N	4294	f
4298	Doll_1992_100336	\N	\N	\N	\N	\N	Die Vegetation des Clanssees bei Feldberg in Mecklenburg	100336	Berlin	\N	\N	\N		\N	\N	\N	67	67	Y			\N		\N	article	2016-05-04 11:11:05.758	\N	1992	\N	4299	f
4301	Doll_Richter_1993_100337	\N	\N	\N	\N	\N	Die Vegetation des Neuendorfer Moores bei Gadebusch	100337	Berlin	\N	\N	\N		\N	\N	\N	63	63	Y			\N		\N	article	2016-05-04 11:11:06.031	\N	1993	\N	4302	f
4365	Isermann_1993_100357	\N	\N	\N	\N	\N	Zur Kenntnis der Vegetation und Morphologie unterschiedlich stark genutzter Dünen auf dem Darß im Nationalpark Vorpommersche Boddenlandschaft	100357		\N	\N	\N		\N	\N	\N	104	104	Y			\N		\N	article	2016-05-04 11:11:10.77	\N	1993	\N	4366	f
4432	Meyer_Voigtlaender_1996_100378	\N	\N	\N	\N	\N	Zur Verbreitung und Soziologie des Rankenden Lerchensporns (Corydalis claviculata (L.)LAM. et DC.) in Mecklenburg-Vorpommern	100378		\N	\N	\N		\N	\N	\N	12	12	Y			\N		\N	article	2016-05-04 11:11:16.028	\N	1996	\N	4433	f
4392	Kintzel_1995_100365	\N	\N	\N	\N	\N	Floristische Beiträge aus dem Landkreis Parchim (I)	100365		\N	\N	\N		\N	\N	\N	17	17	Y			\N		\N	article	2016-05-04 11:11:12.817	\N	1995	\N	4393	f
4394	Kintzel_1996_100366	\N	\N	\N	\N	\N	Der Hainwachtelweizen-Saum (Trifolio medii-Melampyretum nemorosi) - eine seltene Saumgesellschaft in Mecklenburg-Vorpommern	100366		\N	\N	\N		\N	\N	\N	18	18	Y			\N		\N	article	2016-05-04 11:11:13.069	\N	1996	\N	4395	f
4396	Kintzel_1996_100367	\N	\N	\N	\N	\N	Floristische Beiträge aus dem Landkreis Parchim (II)	100367		\N	\N	\N		\N	\N	\N	6	6	Y			\N		\N	article	2016-05-04 11:11:13.287	\N	1996	\N	4397	f
4398	Kintzel_Holst_1995_100368	\N	\N	\N	\N	\N	Beitrag zur Ruderalvegetation in Lübz	100368		\N	\N	\N		\N	\N	\N	66	66	Y			\N		\N	article	2016-05-04 11:11:13.549	\N	1995	\N	4399	f
4339	Harder_1995_100350	\N	\N	\N	\N	\N	Ursachen des Vegetationswandels im Peenetal	100350		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	report	2016-05-04 11:11:09.045	\N	1995	4340	\N	f
4460	Overbeck_1965_100386	\N	\N	\N	\N	\N	Die Meeresalgen und ihre Gesellschaften an den Küsten der Insel Hiddensee (Ostsee)	100386		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:11:17.934	\N	1965	\N	4461	f
4478	Pietsch_1977_100391	\N	\N	\N	\N	\N	Beitrag zur Soziologie und Ökologie der europäischen Littorelletea- und Utricularietea- Gesellschaften	100391	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:11:19.118	\N	1977	\N	4479	f
5366	Schubert_2008_001315	\N	\N	\N	\N	\N	Vegetationsdynamik in einigen Naturschutzgebieten Sachsen-Anhalts	001315	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 21:15:09.141	\N	2008	\N	5367	f
4500	Richter_1996_100399	\N	\N	\N	\N	\N	Bemerkungen über einen Parietaria officinalis - Bestand in Nordwestmecklenburg	100399		\N	\N	\N		\N	\N	\N	6	6	Y			\N		\N	article	2016-05-04 11:11:21.066	\N	1996	\N	4501	f
4495	Rehbein_Litterski_Et_Al._1996_100397	\N	\N	\N	\N	\N	Bemerkenswerte Pflanzen und Pflanzengesellschaften aus dem Uecker-Randow-Kreis	100397		\N	\N	\N		\N	\N	\N	19	19	Y			\N		\N	article	2016-05-04 11:11:20.567	\N	1996	\N	4496	f
4435	Mickel_1993_100379	\N	\N	\N	\N	\N	Kartierung des Alten Bessins	100379		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	report	2016-05-04 11:11:16.292	\N	1993	4436	\N	f
4605	Wollert_Bolbrinker_1993_100432	\N	\N	\N	\N	\N	Zur Naturausstattung des NSG "Teterower Heidberge (Appelhäger Forst)", Landkreis Güstrow	100432		\N	\N	\N		\N	\N	\N	96	96	Y			\N		\N	article	2016-05-04 11:11:30.003	\N	1993	\N	4606	f
4619	Wollert_1996_100437	\N	\N	\N	\N	\N	Zur Vegetation der Moore im Bereich der Lobusnaht zwischen dem Krakower und Malchiner Hauptendmoränenbogen im Raum Carlsdorf-Nienhagen (Mittelmecklenburg)	100437		\N	\N	\N		\N	\N	\N	112	112	Y			\N		\N	article	2016-05-04 11:11:31.256	\N	1996	\N	4620	f
4621	Wollert_Bolbrinker_1993_100438	\N	\N	\N	\N	\N	Zur Wildkrautflora und -vegetation einer stillgelegten Ackerfläche am Nordwestufer des Malchiner Sees	100438	Rostock	\N	\N	\N		\N	\N	\N	7	7	Y			\N		\N	article	2016-05-04 11:11:31.55	\N	1993	\N	4622	f
4626	Wollert_Bolbrinker_1995_100440	\N	\N	\N	\N	\N	Zur Vegetation naturnaher Bäche in Mittelmecklenburg und ihrer Bedeutung für den Naturschutz	100440		\N	\N	\N		\N	\N	\N	53	53	Y			\N		\N	article	2016-05-04 11:11:32.105	\N	1995	\N	4627	f
4628	Holst_Zabel_1996_100441	\N	\N	\N	\N	\N	Das Slater Moor bei Parchim - ein Gebiet mit besonderer floristischer und pflanzensoziologischer Bedeutung	100441		\N	\N	\N		\N	\N	\N	3	3	Y			\N		\N	article	2016-05-04 11:11:32.374	\N	1996	\N	4629	f
4737	Doll_1982_100480	\N	\N	\N	\N	\N	Der Siedenbollentiner See im Kreis Altentreptow	100480	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:11:42.457	\N	1982	\N	4738	f
4741	Pietsch_1985_100482	\N	\N	\N	\N	\N	Chorologische Phänomene in Wasserpflanzengesellschaften Mitteleuropas	100482		\N	\N	\N		\N	\N	\N	10	10	Y			\N		\N	article	2016-05-04 11:11:42.953	\N	1985	\N	4742	f
4760	Pietsch_1984_100490	\N	\N	\N	\N	\N	Zur Soziologie und Ökologie von Myriophyllum alterniflorum D.C. in Mitteleuropa	100490		\N	\N	\N		\N	\N	\N	62	62	Y			\N		\N	article	2016-05-04 11:11:44.635	\N	1984	\N	4761	f
4763	Fischer_1998_100491	\N	\N	\N	\N	\N	Sandtrockenrasen von Binnendünen in der Unteren Mittelelbe-Niederung zwischen Dömitz und Boizenburg	100491	Göttingen	\N	\N	\N		\N	\N	\N	232	232	Y			\N		\N	article	2016-05-04 11:11:44.883	\N	1998	\N	4764	f
4813	Doll_1964_100510	\N	\N	\N	\N	\N	Zur Ökologie und Soziologie von Coronopus squamatus (FORSK.) ASCHERS	100510		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:11:49.332	\N	1964	\N	4814	f
4825	Berg_Kintzel_1999_100515	\N	\N	\N	\N	\N	Vergleichende vegetationskundliche Untersuchungen im FND Hühnerberg (Lewitz) 1972 und 1997	100515	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	22	22	Y			\N		\N	article	2016-05-04 11:11:50.547	\N	1999	\N	4826	f
4829	Sluschny_1999_100517	\N	\N	\N	\N	\N	Das Große Büchsenkraut Lindernia dubia (L.) Penell neu für Mecklenburg-Vorpommern	100517	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	4	4	Y			\N		\N	article	2016-05-04 11:11:51.112	\N	1999	\N	4830	f
4833	Fischer_1999_100519	\N	\N	\N	\N	\N	Bemerkenswerte Neu- und Wiederfunde und Gefährdungen von Trockenrasenarten im mecklenburgischen Elbtal	100519	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:11:51.627	\N	1999	\N	4834	f
4877	Taeuber_1994_100537	\N	\N	\N	\N	\N	Vegetationsuntersuchungen auf einem Panzerübungsgelände im Naturschutzgebiet Lüneburger Heide	100537	Göttingen	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:11:56.341	\N	1994	\N	4878	f
4840	Kintzel_1999_100522	\N	\N	\N	\N	\N	Floristische Beiträge aus dem Landkreis Parchim (V)	100522	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	30	30	Y			\N		\N	article	2016-05-04 11:11:52.384	\N	1999	\N	4841	f
4842	Bolbrinker_Wollert_2000_100523	\N	\N	\N	\N	\N	Das Cephalarietum pilosae JOUANNE 1927 in Mecklenburg	100523	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	16	16	Y			\N		\N	article	2016-05-04 11:11:52.633	\N	2000	\N	4843	f
4844	Fischer_2000_100524	\N	\N	\N	\N	\N	Bromus racemosus HUDS. - ein bemerkenswertes Vorkommen im Peenetal bei Trantow sowie Verbreitung in Mecklenburg-Vorpommern	100524	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	13	13	Y			\N		\N	article	2016-05-04 11:11:52.897	\N	2000	\N	4845	f
4858	Passarge_1993_100530	\N	\N	\N	\N	\N	Lianenschleier-fluviatile und ruderale Staudengesellschaften in den planaren Elb- und Oderauen	100530	Göttingen	\N	\N	\N		\N	\N	\N	4	4	Y			\N		\N	article	2016-05-04 11:11:54.501	\N	1993	\N	4859	f
4860	Passarge_1960_100531	\N	\N	\N	\N	\N	Cynoglossum officinale-Carduus nutans-Ass	100531	Floristisch-soziologischne AG N.F	\N	\N	\N		\N	\N	\N	14	14	Y			\N		\N	article	2016-05-04 11:11:54.818	\N	1960	\N	4861	f
4875	Romahn_1998_100536	\N	\N	\N	\N	\N	Die Vegetation der Kremper und Nordoer Heide	100536		\N	\N	\N		DUBLETTE000722	\N	\N	0	\N	Y			000722		\N	article	2016-05-04 11:11:56.091	\N	1998	\N	4876	f
4846	Schlueter_Sluschny_2000_100525	\N	\N	\N	\N	\N	Der Queckenreis Leersia oryzoides (L.) SW. im mecklenburgischen Elbtal wiederentdeckt - Zur Verbreitung der Art in Mecklenburg-Vorpommern	100525	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	5	5	Y			\N		\N	article	2016-05-04 11:11:53.149	\N	2000	\N	4847	f
4904	Krausch_1968_100555	\N	\N	\N	\N	\N	Die Sandtrockenrasen (Sedo Scleranthetea) in Brandenburg	100555		\N	\N	\N		DUBLETTE000483	\N	\N	0	\N	Y			000483		\N	article	2016-05-04 11:11:58.79	\N	1968	\N	4905	f
4906	Arndt_1956_100556	\N	\N	\N	\N	\N	Beiträge zur Vegetation Brandenburgs 9 ? Die Rotstraußgrasflur in der Niederlausitz	100556		\N	\N	\N		\N	\N	\N	10	10	Y			\N		\N	article	2016-05-04 11:11:59.041	\N	1956	\N	4907	f
4908	Bornkamm_1977_100557	\N	\N	\N	\N	\N	Zu den Standortbedingungen einiger Sand-Therophytenrasen in Berlin (West)	100557		\N	\N	\N		\N	\N	\N	27	27	Y			\N		\N	article	2016-05-04 11:11:59.259	\N	1977	\N	4909	f
4912	Fartmann_1997_100559	\N	\N	\N	\N	\N	Die Vegetation der Trockenrasen und des Feuchtgrünlandes im Naturpark Märkische Schweiz (Ostbrandenburg)	100559		\N	\N	\N		\N	\N	\N	227	55	Y			\N		\N	article	2016-05-04 11:11:59.772	\N	1997	\N	4913	f
4915	Krieger_1937_100560	\N	\N	\N	\N	\N	Die flechtenreichen Pflanzengesellschaften der Mark Brandenburg	100560	B: 1-76	\N	\N	\N		\N	\N	\N	151	41	Y			\N		\N	article	2016-05-04 11:12:00.063	\N	1937	\N	4916	f
4919	Passarge_1978_100563	\N	\N	\N	\N	\N	Bemerkenswerte Pflanzengesellschaften auf märkischem Gebiet	100563	Berlin	\N	\N	\N		\N	\N	\N	17	17	Y			\N		\N	article	2016-05-04 11:12:00.562	\N	1978	\N	4920	f
4921	Passarge_1979_100564	\N	\N	\N	\N	\N	Die Xerothermrasen im Seelower Odergebiet	100564	Berlin	\N	\N	\N		DUBLETTE000510	\N	\N	0	54	Y			000510		\N	article	2016-05-04 11:12:00.833	\N	1979	\N	4922	f
4923	Passarge_1984_100565	\N	\N	\N	\N	\N	Ruderalgesellschaften am Seelower Oderbruchrand	100565	Berlin	\N	\N	\N		\N	\N	\N	8	8	Y			\N		\N	article	2016-05-04 11:12:01.108	\N	1984	\N	4924	f
4926	Berger-landefeldt_Sukopp_1965_100566	\N	\N	\N	\N	\N	Zur Synökologie der Sandtrockenrasen, insbesondere der Silbergrasflur	100566		\N	\N	\N		\N	\N	\N	6	6	Y			\N		\N	article	2016-05-04 11:12:01.331	\N	1965	\N	4927	f
4928	Wegener_1985_100567	\N	\N	\N	\N	\N	Zur Brandenburger Hügelflora ? Auswirkungen des Nutzungswandels auf Steppenrasengesellschaften	100567		\N	\N	\N		\N	\N	\N	9	9	Y			\N		\N	article	2016-05-04 11:12:01.621	\N	1985	\N	4929	f
5512	Raabe_1950_101049	\N	\N	\N	\N	\N	Über die Vegetationsverhältnisse der Insel Fehmarn	101049	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:15.344	\N	1950	\N	5513	f
4932	Schlueter_1955_100569	\N	\N	\N	\N	\N	Das Naturschutzgebiet Strausberg	100569	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:12:02.139	\N	1955	\N	4933	f
4883	Jansen_2000_100539	\N	\N	\N	\N	\N	Vegetationsaufnahmen Mecklenburg-Vorpommern	100539		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y	Unveröff		\N		\N	unpublished	2016-05-04 11:11:56.836	\N	2000	\N	\N	f
5022	Lein_2001_100615	\N	\N	\N	\N	\N	Renaturierung eines degradierten Flußtalmoores	100615		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:09.511	\N	2001	\N	\N	f
5261	Cremer_2013_null	\N	\N	\N	\N	\N	Welchen Einfluss hat die Nutzungsintensität auf mesotrophe nasse Niedermoore?	\N	\N	\N	\N	\N	\N	\N	\N	Institut für Botanik und Landschaftsökologie	\N	\N	\N	\N	\N	\N	Universität Greifswald	\N	thesis	2016-04-19 20:45:11.404	\N	2013	\N	\N	f
3	Glavac_Raus_1982_000001	\N	\N	\N	\N	\N	Über die Pflanzengesellschaften des Landschafts- und Naturschutzgebietes "Dönche" in Kassel	000001	Göttingen	\N	\N	\N		\N	\N	\N	276	\N	Y			\N		\N	article	2016-05-04 11:04:27.826	\N	1982	\N	4	f
5144	Middelschulte_1993_100659	\N	\N	\N	\N	\N	Vegetations- und bodenkundliche Untersuchungen im Bollwintal in der Schorfheide	100659		\N	\N	\N		Middelschulte_1993	\N	\N	162	\N	Y			\N		\N	article	2016-05-04 11:12:20.084	\N	1993	\N	5145	f
5222	Freitag_Mathar_Yurtaev_Hoelzel_2015_100702	\N	\N	\N	\N	\N	Floristic composition and environmental determinants of pine forests in the hemiboreal zone of Western Siberia	100702	Göttingen	\N	\N	\N	\N	\N	\N	\N	0	\N	Y	\N	\N	\N	\N	\N	article	2016-05-04 11:12:26.843	\N	2015	\N	5223	f
5205	Huntke_2007_100680	\N	\N	\N	\N	\N	Die Verbreitung von Drosera anglica Huds. in Niedersachsen früher und heute - Ausmaß und Ursachen des Rückgangs eines Hochmoorspezialisten	100680	Göttingen	\N	\N	\N		VW_T274	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:12:25.098	\N	2007	\N	5206	f
5212	Passarge_1983_001279	\N	\N	\N	\N	\N	Feuchtvegetation im Seelower Oderbruch.	001279	Berlin	\N	\N	\N	\N	Passarge_1983		\N	0	\N	Y		\N	\N	\N	\N	article	2016-05-04 11:12:25.883	\N	1983	\N	5213	f
5214	Brunner_2005_001278	\N	\N	\N	\N	\N	Die aktuelle Vegetation des Nürnberger Reichswaldes. Untersuchungen zur Pflanzensoziologie und Phytodiversität als Grundlage für den Naturschutz.	001278	\N	\N		\N	\N	Brunner_2005		\N	0	\N	Y	Diss. Univ. Erlangen	\N	\N	\N	\N	thesis	2016-05-04 11:12:26.176	\N	2005	\N	\N	f
5233	Jost_Resagk_2004_100706	\N	\N	\N	\N	\N	Vergleichende Untersuchungen an Wald- und Offenlandsöllen im Bereich der Rosenthaler Staffel / nördliche Uckermark	100706	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	thesis	2016-05-04 11:12:27.765	\N	2004	5234	\N	f
5159	Litterski_Adler_Et_Al._2006_100664	\N	\N	\N	\N	\N	Dreifelderwirtschaft - Chance für den Segetalartenschutz auf ertragsschwachen Standorten	100664	Göttingen	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:12:21.296	\N	2006	\N	5160	f
50	Wiebe_1998_000016	\N	\N	\N	\N	\N	Ökologische Charakterisierung von Erlenbruchwäldern und ihren Entwässerungsstadien: Vegetation und Standortverhältnisse	000016	Kiel	\N	\N	\N		\N	\N	\N	20	\N	Y			\N		\N	article	2016-05-04 11:04:32.682	\N	1998	\N	51	f
66	Baeumer_1937_000021	\N	\N	\N	\N	\N	Verbreitung und Vergesellschaftung des Glatthafers (Arrhenatherum elatius) und Goldhafers (Trisetum flavescens) im nördlichen Rheinland	000021	Bonn	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:04:33.985	\N	1937	\N	67	f
110	Succow_1970_000036	\N	\N	\N	\N	\N	Zur Verbreitung und Soziologie der Orchideen in den mecklenburgischen Talmooren	000036		\N	\N	\N		\N	\N	\N	27	\N	Y			\N		\N	article	2016-05-04 11:04:37.947	\N	1970	\N	111	f
5132	Gremer_Clausnitzer_1998_100654	\N	\N	\N	\N	\N	Vegetationsaufnahmen aus dem Metelmoor	100654		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y	\N		\N		\N	unpublished	2016-05-04 11:12:19.131	\N	1998	\N	\N	f
5239	Kindermann_2016_100707	\N	\N	\N	\N	\N	Vegetationskundliche Erfassung und naturschutzfachliche Bewertung der Wiesen um Hinterhermsdorf (Sächsische Schweiz)	100707	\N	\N	\N	73 Aufnahmen	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Universität Greifswald	\N	unknown	2016-04-14 10:14:38.839	\N	2016	\N	\N	t
5211	OHNE_BIBLIOREFERENZ_null_001999	\N	\N	\N	\N	\N	DATEN LAU, NP_HARZ u.a.	001999	\N	\N	\N	\N	\N			\N	0	\N			\N	\N	\N	\N	unknown	2016-05-04 11:12:25.619	\N	1500	\N	\N	t
166	Kirsch-Stracke_2007_000054	\N	\N	\N	\N	\N	Weitere Beiträge zur Kenntnis hessischer Pflanzengesellschaften: Felsspalten- und Mauerfugen-Gesellschaften (einschließlich Parietarietalia)	000054	Frankfurt am Main	\N	\N	\N		\N	\N	\N	21	\N	Y			\N		\N	article	2016-05-04 11:04:43.539	\N	2007	\N	167	f
276	Meusel_1939_000091	\N	\N	\N	\N	\N	Die Vegetationsverhältnisse der Gipsberge im Kyffhäuser und im südlichen Harzvorland. Ein Beitrag zur Steppenheidefrage	000091	Halle (Saale)	\N	MEU	\N		TABGESNE	TV	\N	87	\N	Y			\N		\N	article	2016-05-04 11:04:52.915	\N	1939	\N	277	f
349	Kuhn_Kramer_1995_000115	\N	\N	\N	\N	\N	Vegetation und Flora des Schmiecher Sees (Gefäßpflanzen)	000115	Karlsruhe	\N	\N	\N		\N	\N	\N	154	\N	Y			\N		\N	article	2016-05-04 11:04:59.064	\N	1995	\N	350	f
5176	Noirfalise_1956_100670	\N	\N	\N	\N	\N	La Hetraie Ardennaise	100670		\N	NOI	unpubliziert		Heinken2	\N	\N	117	\N	Y			\N		\N	unpublished	2017-03-15 15:48:06.929	\N	1956	\N	5177	f
5182	Noirfalise_Roisin_1981_100672	\N	\N	\N	\N	\N	La hetraie á millet étalé en Belgique (Milio-Fagetum)	100672		\N	NVA	unpubliziert		Heinken2	\N	\N	310	\N	N			\N		\N	unpublished	2017-03-15 15:50:03.647	\N	1981	\N	5183	f
455	Fischer_1987_000155	\N	\N	\N	\N	\N	Die Vegetation der Naturschutzgebiete "Aubachtal", "Viehweide am Bartenstein", "Feuerhecke" und "Bermeshube" im hessischen Westerwald	000155	Wiesbaden	\N	\N	\N		\N	\N	\N	102	\N	Y			\N		\N	article	2016-05-04 11:05:08.88	\N	1987	\N	456	f
485	Ganzert_1991_000165	\N	\N	\N	\N	\N	Die Vegetation des Grünlandes in den Loisach-Kochelsee-Mooren (Teil 2)	000165	München	\N	\N	\N		\N	\N	\N	192	\N	Y			\N		\N	article	2016-05-04 11:05:11.407	\N	1991	\N	486	f
4732	Kintzel_1998_100478	\N	\N	\N	\N	\N	Floristische Beiträge aus dem Landkreis Parchim (IV)	100478	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	9	9	Y			\N		\N	article	2016-05-04 11:11:41.931	\N	1998	\N	4733	f
630	Fleischer_2001_000219	\N	\N	\N	\N	\N	Beitrag zur Kenntnis der Flora und Vegetation der Bienitz bei Leipzig	000219		\N	\N	\N		Fleischer_2001	\N	\N	195	\N	Y			\N		\N	article	2016-05-04 11:05:24.139	\N	2001	\N	631	f
632	Krausch_1955_000220	\N	\N	\N	\N	\N	Beobachtungen an Wiesengesellschaften im Neißetal bei Guben	000220	Potsdam	\N	\N	\N		\N	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:05:24.367	\N	1955	\N	633	f
636	Krausch_1963_000222	\N	\N	\N	\N	\N	Zur Soziologie der Juncus acutiflorus-Quellwiesen Brandenburgs	000222	Berlin	\N	\N	\N		\N	\N	\N	42	\N	Y			\N		\N	article	2016-05-04 11:05:24.855	\N	1963	\N	637	f
638	Krausch_1966_000223	\N	\N	\N	\N	\N	Das Caricetum appropinquatae und andere Flachmoorgesellschaften im Springbruch bei Potsdam	000223	Berlin	\N	\N	\N		\N	\N	\N	79	\N	Y			\N		\N	article	2016-05-04 11:05:25.073	\N	1966	\N	639	f
132	Herrmann_1989_000043	\N	\N	\N	\N	\N	Grünlandgesellschaften im nördlichen Fulda-Werra-Bergland	000043		\N	\N	\N		LITTAB	\N	\N	40	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 113 S	\N	thesis	2016-05-04 11:04:41.037	\N	1989	\N	\N	f
647	Krisch_1974_000227	\N	\N	\N	\N	\N	Wirtschaftsgrünland, Röhrichte und Seggenriede der Ryckniederung (Nordost-Mecklenburg)	000227	Berlin	\N	\N	\N		\N	\N	\N	143	\N	Y			\N		\N	article	2016-05-04 11:05:26.145	\N	1974	\N	648	f
649	Ganzert_1990_000228	\N	\N	\N	\N	\N	Die Vegetation des Grünlandes in den Loisach-Kochelsee-Mooren	000228	München	\N	\N	\N		Ganzert_1990	\N	\N	198	\N	Y			\N		\N	article	2016-05-04 11:05:26.396	\N	1990	\N	650	f
661	Libbert_1940_000233	\N	\N	\N	\N	\N	Pflanzensoziologische Beobachtungen während einer Reise durch Schleswig-Holstein im Juli 1939	000233	Berlin	\N	\N	\N		x	\N	\N	1	\N	Y			\N		\N	article	2016-05-04 11:05:27.475	\N	1940	\N	662	f
745	Oppermann_Reichholf_Pfadenhauer_1987_000266	\N	\N	\N	\N	\N	Beziehungen zwischen Vegetation und Fauna in Feuchtwiesen. Untersucht am Beispiel von Schmetterlingen und Heuschrecken in zwei Feuchtgebieten Oberschwabens	000266		\N	\N	\N		\N	\N	\N	38	\N	Y			\N		\N	article	2016-05-04 11:05:35.541	\N	1987	\N	746	f
761	Passarge_1964_000273	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des nordostdeutschen Flachlandes. I	000273	Jena	\N	\N	\N		\N	\N	\N	52	\N	Y			\N		\N	article	2016-05-04 11:05:37.322	\N	1964	\N	762	f
788	Philippi_1963_000283	\N	\N	\N	\N	\N	Zur Soziologie von Anagallis tenella, Scutellaria minor und Wahlenbergia hederacea im südlichen und mittleren Schwarzwald	000283	Freiburg i.Br.	\N	\N	\N		\N	\N	\N	17	\N	Y			\N		\N	article	2016-05-04 11:05:39.866	\N	1963	\N	789	f
864	Ruthsatz_1990_000312	\N	\N	\N	\N	\N	Vegetationskundlich-ökologische Nachweis- und Voraussagemöglichkeiten für den Erfolg von Extensivierungsmaßnahmen in Feuchtgrünlandgebieten	000312	Hamburg	\N	\N	\N		\N	\N	\N	39	\N	Y			\N		\N	article	2016-05-04 11:05:46.986	\N	1990	\N	865	f
870	Scherfose_1993_000314	\N	\N	\N	\N	\N	Bestandesentwicklung und Vergesellschaftung der Schachblume (Fritillaria meleagris L.) im NSG "Tiergarten" (Erweiterung Schachblumenwiesen, Kreis Warendorf)	000314		\N	\N	\N		\N	\N	\N	13	\N	Y			\N		\N	article	2016-05-04 11:05:47.502	\N	1993	\N	871	f
893	Schwabe_Kratochwil_1986_000323	\N	\N	\N	\N	\N	Schwarzwurzel- (Scorzonera humilis-) und Bachkratzdistel- (Cirsium rivulare-) reiche Vegetationstypen im Schwarzwald: Ein Beitrag zur Erhaltung selten werdender Feuchtwiesen-Typen	000323	Naturschutz ?	\N	\N	\N		\N	\N	\N	45	\N	Y			\N		\N	article	2016-05-04 11:05:49.85	\N	1986	\N	894	f
962	Ullmann_1972_000349	\N	\N	\N	\N	\N	Das Zeubelrieder Moor	000349	Würzburg	\N	\N	\N		\N	\N	\N	18	\N	Y			\N		\N	article	2016-05-04 11:05:56.293	\N	1972	\N	963	f
964	Ullmann_1977_000350	\N	\N	\N	\N	\N	Die Vegetation des südlichen Maindreiecks	000350	Regensburg	\N	\N	\N		\N	\N	\N	8	\N	Y			\N		\N	article	2016-05-04 11:05:56.549	\N	1977	\N	965	f
981	Vollrath_1965_000356	\N	\N	\N	\N	\N	Das Vegetationsgefüge der Itzaue als Ausdruck hydrogeologischen und sedimentologischen Geschehens	000356	München	\N	\N	\N		\N	\N	\N	15	\N	Y			\N		\N	article	2016-05-04 11:05:57.865	\N	1965	\N	982	f
997	Walther_1986_000363	\N	\N	\N	\N	\N	Die Vegetation des Maujahn 1984. Wiederholung der vegetationskundlichen Untersuchung eines wendländischen Moores	000363	Göttingen	\N	\N	\N		enth�lt auch Aufnahmen von T�xen 1962: TV000740	\N	\N	39	\N	Y			\N		\N	article	2016-05-04 11:05:59.478	\N	1986	\N	998	f
284	Tuexen_1974_000094	\N	\N	\N	\N	\N	Das Lahrer Moor	000094	Göttingen	\N	\N	\N		THOMAS	\N	\N	4	\N	Y			\N		\N	article	2016-05-04 11:04:53.651	\N	1974	\N	285	f
1066	Altehage_1937_000385	\N	\N	\N	\N	\N	Die Steppenheide bei Rothenburg-Könnern im unteren Saaletal	000385		\N	ALH	\N		TABGESNE	\N	\N	38	\N	Y			\N		\N	article	2016-05-04 11:06:04.979	\N	1937	\N	1067	f
1068	Altehage_1951_000386	\N	\N	\N	\N	\N	Das Caricetum humilis der Neuen Göhle bei Freyburg a. d. Unstrut und seine Übergänge in den subkontinentalen Eichenmischwald	000386		\N	ALH	\N		TABGESNE	\N	\N	18	\N	Y			\N		\N	article	2016-05-04 11:06:05.242	\N	1951	\N	1069	f
1110	Bohn_1981_000401	\N	\N	\N	\N	\N	Vegetationskarte der Bundesrepublik Deutschland 1:200 000 Potentielle natürliche Vegetation - Blatt CC 5518 Fulda	000401		\N	BOH	\N		TABGESNE	\N	\N	16	\N	Y			\N		\N	article	2016-05-04 11:06:09.112	\N	1981	\N	1111	f
1129	Braun-blanquet_1936_000409	\N	\N	\N	\N	\N	Über die Trockenrasengesellschaften des Festucion vallesiacae in den Ostalpen	000409		\N	BBL	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:11.161	\N	1936	\N	1130	f
674	Jandt_1999_000238	\N	\N	\N	\N	\N	Kalkmagerrasen am Südharzrand und im Kyffhäuser	000238		\N	\N	\N		\N	\N	\N	270	\N	N	Diss. Bot. 322: 1-246	Diss. Bot. 322: 1-246	\N		\N	book	2016-05-04 11:05:28.719	\N	1999	\N	\N	f
1234	Halfmann_1986_000444	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen an der Graburg (Nord-Hessen) als Grundlage für Pflege- und Erhaltungsmaßnahmen zur Sicherung von Pflanzengesellschaften und Biotopen	000444		\N	HAL	\N		TABGESNE	\N	\N	75	\N	Y			\N		\N	article	2016-05-04 11:06:19.489	\N	1986	\N	1235	f
1286	Janssen_1992_000462	\N	\N	\N	\N	\N	Flora und Vegetation von Halbtrockenrasen (Festuco-Brometea) im nördlichen Harzvorland Niedersachsens unter besonderer Berücksichtigung ihrer Isolierung in der Agrarlandschaft	000462	Braunschweig	\N	JAS	\N		TABGESNE	\N	\N	210	\N	Y			\N		\N	article	2016-05-04 11:06:23.737	\N	1992	\N	1287	f
1330	Korneck_1975_000482	\N	\N	\N	\N	\N	Beitrag zur Kenntnis mitteleuropäischer Felsgrus-Gesellschaften (Sedo-Scleranthetalia)	000482		\N	KOR	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:28.852	\N	1975	\N	1331	f
1350	Schwabe_Kratochwil_Bammert_1989_000490	\N	\N	\N	\N	\N	Sukzessionsprozesse im aufgelassenen Weidfeld-Gebiet des "Bannwald Flüh" (Südschwarzwald) 1976-1988 - Mit einer vergleichenden Betrachtung statistischer Auswertungsmethoden	000490	Göttingen	\N	\N	\N		Schwabe_1989	\N	\N	41	\N	Y			\N		\N	article	2016-05-04 11:06:30.966	\N	1989	\N	1351	f
1410	Pless_1995_000514	\N	\N	\N	\N	\N	Pflanzensoziologische Untersuchungen der Trockenrasen an den Hängen des Odertales zwischen Seelow und Frankfurt (Oder). Ein Vergleich des Zustandes ausgewählter Bestände aus den 50er Jahren mit den heutigen	000514		\N	PLS	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:06:36.945	\N	1995	\N	1411	f
1484	Schubert_Mahn_1959_000541	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen in der mitteldeutschen Ackerlandschaft. I. Die Pflanzengesellschaften der Gemarkung Friedeburg (Saale)	000541	Halle (Saale)	\N	SUM	\N		TABGESNE	\N	\N	67	\N	Y			\N		\N	article	2016-05-04 11:06:43.502	\N	1959	\N	1485	f
2770	Papajewski_1983_001018	\N	\N	\N	\N	\N	Ökologisches Gutachten über das NSG "Hohe Ley"	001018		\N	\N	unpubliziert		\N	\N	\N	4	\N	N	Unveröff. Mskr		\N		\N	unpublished	2016-05-04 11:08:37.551	\N	1983	\N	\N	f
5400	Bernhardt_1992_101007	\N	\N	\N	\N	\N	Besiedlungsstrategien an sandigen Extremstandorten im Tidebereich.	101007	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:12.386	\N	1992	\N	5401	f
1802	Lisbach_Peppler-lisbach_1996_000656	\N	\N	\N	\N	\N	Magere Glatthaferwiesen im Südöstlichen Pfälzerwald und im Unteren Werraland. - Ein Beitrag zur Untergliederung des Arrhenatheretum elatioris Braun 1915	000656	Göttingen	\N	\N	\N		\N	\N	\N	255	\N	Y			\N		\N	article	2016-05-04 11:07:11.412	\N	1996	\N	1803	f
1614	Krause_Schroeder_1979_000589	\N	\N	\N	\N	\N	Vegetationskarte der Bundesrepublik Deutschland 1:200.000 - Potentielle natürliche Vegetation - Blatt CC 3118 Hamburg-West	000589		\N	KS	\N		BOHN	\N	\N	107	\N	Y			\N		\N	article	2016-05-04 11:06:55.096	\N	1979	\N	1615	f
1889	Schmitt_Fartmann_2006_000687	\N	\N	\N	\N	\N	Die Heidenelken-reichen Silikat-Magerrasen der Medebacher Bucht (Südwestfalen/Nordhessen): Ökologie, Syntaxonomie und Management	000687	Göttingen	\N	\N	\N		vegetweb	\N	\N	54	\N	Y			\N		\N	article	2016-05-04 11:07:19.249	\N	2006	\N	1890	f
2009	Suck_1999_000731	\N	\N	\N	\N	\N	Die natürlichen Waldgesellschaften des Schneifel-Hauptkammes (Westliche Hocheifel) und ihre Ersatzgesellschaften	000731	Göttingen	\N	\N	\N		\N	\N	\N	81	\N	Y			\N		\N	article	2016-05-04 11:07:29.127	\N	1999	\N	2010	f
2019	Bergmeier_1990_000735	\N	\N	\N	\N	\N	Meso- und eutraphente Buchen-Wälder (Fagion sylvaticae). In: NOWAK, B. (1990): Beiträge zur Kenntnis hessischer Pflanzengesellschaften. Ergebnisse der Pflanzensoziologischen Sonntagsexkursionen der Hessischen Botanischen Arbeitsgemeinschaft	000735	Frankfurt am Main	\N	\N	\N		\N	TV	\N	33	\N	Y			\N		\N	article	2016-05-04 11:07:30.19	\N	1990	\N	2020	f
2042	Fartmann_2004_000744	\N	\N	\N	\N	\N	Die Schmetterlingsgemeinschaften der Halbtrockenrasen-Komplexe des Diemeltales. Biozönologie von Tagfaltern und Widderchen in einer alten Hudelandschaft	000744	Münster	\N	\N	\N		\N	TV	\N	175	\N	Y			\N		\N	article	2016-05-04 11:07:32.55	\N	2004	\N	2043	f
2098	Krausch_1970_000764	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Stechlinsee-Gebietes. V. Wälder, Hecken und Saumgesellschaften	000764		\N	\N	\N		\N	\N	\N	228	\N	Y			\N		\N	article	2016-05-04 11:07:37.512	\N	1970	\N	2099	f
2101	Hierlmeier_1999_000765	\N	\N	\N	\N	\N	Waldgesellschaften im Gebiet zwischen Falkenstein und Rachel in Nationalpark Bayerischer Wald	000765	Regensburg	\N	\N	\N		\N	\N	\N	257	\N	Y			\N		\N	article	2016-05-04 11:07:37.731	\N	1999	\N	2102	f
2246	Fischer_1960_000817	\N	\N	\N	\N	\N	Pflanzengesellschaften der Heiden und oligotrophen Moore der Prignitz. Beiträge zur Flora und Vegetation Brandenburgs 27	000817	Potsdam	\N	\N	\N		\N	\N	\N	126	\N	Y			\N		\N	article	2016-05-04 11:07:50.068	\N	1960	\N	2247	f
2410	Grosse-Brauckmann_Streitz_1990_000882	\N	\N	\N	\N	\N	Das Pyrolo-Pinetum an der nördlichen Bergstraße: Eine von der Vernichtung bedrohte, bemerkenswerte Waldgesellschaft	000882	Frankfurt am Main	\N	\N	\N		HEINKEN_DIP	\N	\N	1	\N	Y			\N		\N	article	2016-05-04 11:08:05.462	\N	1990	\N	2411	f
2455	Pollmann_Lethmate_2003_000899	\N	\N	\N	\N	\N	Zur Frage der Buche auf Sandböden in Nordwest-Deutschland: Ökologische Potenz von Fagus sylvatica L. unter extremen Standortbedingungen im Riesenbecker Osning	000899	Göttingen	\N	\N	\N		HEINKEN_DIP	\N	\N	29	\N	Y			\N		\N	article	2016-05-04 11:08:09.62	\N	2003	\N	2456	f
2504	Besler_Bornkamm_1982_000917	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen im Gebiet des Spieser bei Unterjoch (Allgäu)	000917	Göttingen	\N	\N	\N		\N	\N	\N	27	\N	Y			\N		\N	article	2016-05-04 11:08:14.001	\N	1982	\N	2505	f
2546	Lorenz_1993_000933	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen der Schneeheide-Kiefernwälder im Landkreis Garmisch-Partenkirchen	000933	Regensburg	\N	\N	\N		\N	\N	\N	142	\N	Y			\N		\N	article	2016-05-04 11:08:17.861	\N	1993	\N	2547	f
2719	Zeidler_Straub_1967_000998	\N	\N	\N	\N	\N	Waldgesellschaften mit Kiefer in der heutigen potentiellen natürlichen Vegetation des mittleren Main-Gebietes	000998		\N	\N	\N		HEINKEN_DIP	\N	\N	69	\N	Y			\N		\N	article	2016-05-04 11:08:32.866	\N	1967	\N	2720	f
2436	Kuerschner_Runge_1997_000892	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen ausgewählter Binnendünen- und Talsandstandorte im Dahme-Seengebiet (Brandenburg) und ihre Entwicklungspotentiale	000892		\N	\N	\N		HEINKEN_DIP	\N	\N	93	\N	Y			\N		\N	article	2016-05-04 11:08:07.896	\N	1997	\N	2437	f
1898	Dengler_Eisenberg_Schroeder_2007_000690	\N	\N	\N	\N	\N	Die grundwasserfernen Saumgesellschaften Nordost-Niedersachsens im europäischen Kontext - Teil II: Säume nährstoffreicher Standorte (Artemisietea vulgaris) und vergleichende Betrachtung der Saumgesellschaften insgesamt	000690	Göttingen	\N	\N	\N		vegetweb	\N	\N	200	\N	Y			\N		\N	article	2017-01-23 16:31:18.43	\N	2007	\N	1899	f
2732	Braun_1978_001004	\N	\N	\N	\N	\N	Die Pflanzendecke. In: Bodenkarte von Bayern 1:25.000. Erläuterungen zum Kartenblatt Nr. 7029 Oettingen i. Bay.	001004	München	\N	\N	\N	Booktitle	\N	\N	\N	3	\N	N	In: Bodenkarte von Bayern 1:25.000. Erläuterungen zum Kartenblatt Nr. 7029 Oettingen i. Bay.: 56-72. München		\N		\N	book	2016-09-22 11:34:27.701	\N	1978	\N	2733	f
2779	Pott_1985_001021	\N	\N	\N	\N	\N	Vegetationsgeschichtliche und pflanzensoziologische Untersuchungen zur Niederwaldwirtschaft in Westfalen	001021	Münster	\N	\N	\N		\N	\N	\N	47	\N	Y			\N		\N	article	2016-05-04 11:08:38.316	\N	1985	\N	2780	f
3028	Berg_Mahn_1990_001108	\N	\N	\N	\N	\N	Anthropogene Vegetationsveränderungen der Straßenrandvegetation in den letzten 30 Jahren - die Glatthaferwiesen des Raumes Halle/Saale	001108	Göttingen	Alle Aufnahmen auch in Berg, C. 1990 Diss. Univ. Halle. TVref 001133	\N	0000000011		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:00.572	\N	1990	\N	3029	f
3089	Wattendorf_1960_001129	\N	\N	\N	\N	\N	Über die Verbreitung der Edelkastanie im Buchen-Traubeneichen-Wald der Hohen Mark bei Haltern in Westfalen	001129	Stolzenau	\N	\N	\N		HEINKEN2	\N	\N	4	\N	Y			\N		\N	article	2016-05-04 11:09:06.005	\N	1960	\N	3090	f
3183	Hofmeisetr_Zacharias_1999_001163	\N	\N	\N	\N	\N	Die Weidelgras-Weiden des Lolio-Cynosuretum Br. Bl. & De Leeuw 1936 nom. inv. auf dem Standortübungsplatz Hildesheim (Niedersachsen)	001163	Göttingen	\N	\N	\N		\N	\N	\N	64	\N	Y			\N		\N	article	2016-05-04 11:09:14.498	\N	1999	\N	3184	f
3241	Glunk_Rudner_2010_001185	\N	\N	\N	\N	\N	Zur Vegetation von Kleinstrukturen im Mittleren Schwarzwald und deren Eignung als Refugium für Arten der Magerweiden	001185	Göttingen	\N	\N	\N		VW_T303	\N	\N	60	\N	Y			\N		\N	article	2016-05-04 11:09:20.1	\N	2010	\N	3242	f
3919	Krisch_1987_100174	\N	\N	\N	\N	\N	Zur Ausbreitung und Soziologie von Corispermum leptopterum (ASCHERSON)ILJIN an der südlichen Ostseeküste	100174	Berlin	\N	\N	\N		\N	\N	\N	31	31	Y			\N		\N	article	2016-05-04 11:10:26.231	\N	1987	\N	3920	f
2799	Dierssen_Hoeper_1984_001030	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen im NSG "Reher Kratt"	001030	Kiel	\N	\N	\N		HEINKEN	\N	\N	17	\N	Y			\N		\N	article	2016-05-04 11:08:40.492	\N	1984	\N	2800	f
3278	Hammes_Remy_Kratochwil_2012_001196	\N	\N	\N	\N	\N	Untersuchungen zur längerfristigen Etablierung von Corynephorus canescens-Populationen in einem großflächigen Restitutionsgebiet einer Auenlandschaft Nordwestdeutschlands	001196	Göttingen	\N	\N	\N		Hammes_2012_Tuexenia (m�sste T327_Hammes hei�en)	TUEXENIA	\N	102	\N	Y			\N		\N	article	2016-05-04 11:09:22.842	\N	2012	\N	3279	f
3296	Schmale_1939_001203	\N	\N	\N	\N	\N	Das Naturschutzgebiet Golmer Luch. eine pflanzensoziologisch-ökologische Studie	001203	Berlin-Dahlem	\N	\N	\N		Schmale_1939	\N	\N	100	\N	Y			\N		\N	article	2016-05-04 11:09:24.65	\N	1939	\N	3297	f
3330	Gerth_Merten_Baumbach_2011_001210	\N	\N	\N	\N	\N	Verbreitung, Vergesellschaftung und genetische Populationsdifferenzierung des Berg-Steinkrautes (Alyssum montanum L.) auf Schwermetallstandorten im östlichen Harzvorland	001210		\N	\N	\N		\N	\N	\N	27	\N	Y			\N		\N	article	2016-05-04 11:09:26.384	\N	2011	\N	3331	f
3364	Korneck_1956_001222	\N	\N	\N	\N	\N	Die Rabenkanzel bei Uffhofen - ein übersehener Steppenheide-Wuchsort Rheinhessens	001222	Darmstadt	\N	\N	\N		Korneck_1956	\N	\N	3	\N	Y			\N		\N	article	2016-05-04 11:09:29.155	\N	1956	\N	3365	f
4131	Thomas_1979_100270	\N	\N	\N	\N	\N	Pflanzensoziologisch-ökologische Analyse der Vegetation beweideter Strandwälle im NSG "Ostufer der Müritz"	100270	Berlin	\N	\N	\N		\N	\N	\N	5	5	Y			\N		\N	article	2016-05-04 11:10:48.962	\N	1979	\N	4132	f
3415	Dierschke_2013_001241	\N	\N	\N	\N	\N	Konstanz und Dynamik in einem artenreichen Kalkbuchenwald. Veränderungen in einem Großtransekt 1981-2011	001241		\N	\N	\N		T333_Dierschke	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:33.592	\N	2013	\N	3416	f
3461	Roscher_2001_001258	\N	\N	\N	\N	\N	Die Halbtrockenrasen derOrlasenke (Thüringen) unter besonderer Berücksichtigung der Sesleria albicans-reichen Ausbildungen	001258		\N	\N	\N		Roscher_2001	\N	\N	168	\N	Y			\N		\N	article	2016-05-04 11:09:37.335	\N	2001	\N	3462	f
3480	Altenfelder_Raabe_Albrecht_2014_001264	\N	\N	\N	\N	\N	Effects of water regime and agricultural land use on diversity and species composition of vascular plants inhabiting temporary ponds in northeastern Germany	001264		\N	\N	\N		T348_Altenfelder	\N	\N	174	\N	Y			\N		\N	article	2016-05-04 11:09:38.738	\N	2014	\N	3481	f
3560	Bolbrinker_1979_100016	\N	\N	\N	\N	\N	Ergebnisse der floristischen Kartierung einiger Bach- und Flußtäler des Kreises Teterow (Mecklenburg) IV. Das obere Tal der Neukalener Peene zwischen Granzow und Küsserow	100016		\N	\N	\N		\N	\N	\N	2	2	Y			\N		\N	article	2016-05-04 11:09:46.114	\N	1979	\N	3561	f
3610	Doll_1978_100038	\N	\N	\N	\N	\N	Paschen- und Langhagen-See - zwei bemerkenswerte Gewässer im Kr.Lübz	100038	Rostock	\N	\N	\N		\N	\N	\N	21	21	Y			\N		Oberdorfer 1994	article	2016-05-04 11:09:51.926	\N	1978	\N	3611	f
3630	Doll_1989_100048	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der stehenden Gewässer in Mecklenburg-Vorpommern.Teil 1.Die Gesellschaften des offenen Wassers (Characeen-Gesellschaften)	100048	Berlin	\N	\N	\N		\N	\N	\N	159	159	Y			\N		\N	article	2016-05-04 11:09:54.658	\N	1989	\N	3631	f
3655	Duty_Schmidt_1964_100058	\N	\N	\N	\N	\N	Das Vegetationsgefüge von Niedermoorwiesen des Warnowtales	100058		\N	\N	\N		\N	\N	\N	42	42	Y			\N		\N	article	2016-05-04 11:09:57.337	\N	1964	\N	3656	f
3674	Funk_1977_100065	\N	\N	\N	\N	\N	Neufunde von Cyperus fuscus L. und Limosella aquatica L. im Kreis Teterow	100065		\N	\N	\N		\N	\N	\N	8	8	Y			\N		\N	article	2016-05-04 11:09:59.057	\N	1977	\N	3675	f
3722	Holst_1979_100084	\N	\N	\N	\N	\N	Ein Beitrag zur Adventivflora im mittelmecklenburgischen Raum am Beispiel des Auftretens landschaftsfremder Pflanzenarten im Bereich von Bahnhöfen	100084	Rostock	\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:03.861	\N	1979	\N	3723	f
3764	Jeschke_1960_100100	\N	\N	\N	\N	\N	Die Vegetation der als Vogelschutzgebiet geplanten Insel Oie im Barther Bodden	100100		\N	\N	\N		\N	\N	\N	44	44	Y			\N		\N	article	2016-05-04 11:10:07.929	\N	1960	\N	3765	f
3780	Jeschke_1966_100108	\N	\N	\N	\N	\N	Über die Vegetation des Naturschutzgebietes Röggeliner See und Kuhlrader Moor	100108		\N	\N	\N		\N	\N	\N	27	27	Y			\N		\N	article	2016-05-04 11:10:09.915	\N	1966	\N	3781	f
3837	Kintzel_1990_100135	\N	\N	\N	\N	\N	Zur aktuellen Verbreitung und zum soziologischen Verhalten einiger Trockenrasenpflanzen im Kreis Lübz	100135		\N	\N	\N		\N	\N	\N	59	59	Y			\N		\N	article	2016-05-04 11:10:16.915	\N	1990	\N	3838	f
5265	Malkomes_2008_100712	\N	\N	\N	\N	\N	Geobotanische Untersuchungen im Naturschutzgebiet Karlsburger und Oldenburger Holz	100712	\N	\N	\N	\N	\N	\N	\N	Institut für Botanik und Landschaftsökologie	\N	\N	\N	\N	\N	\N	Universität Greifswald	\N	unknown	2016-07-22 08:55:49.938	\N	2008	\N	\N	t
3934	Kudoke_1967_100181	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen in der Ackerlandschaft des mittleren Teils der Grundmoräne Mecklenburgs.I.Ökologisch-soziologische Zeigergruppen in den Ackerflächen der Flurgem	100181		\N	\N	\N		\N	\N	\N	115	105	Y			\N		\N	article	2016-05-04 11:10:27.885	\N	1967	\N	3935	f
3994	Passarge_1959_100205	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen in den Wäldern der Jungmoränenlandschaft um Dargun/Ost- Mecklenburg	100205	Berlin	\N	\N	\N		DUBLETTE000607	\N	\N	0	170	Y			000607		\N	article	2016-05-04 11:10:33.782	\N	1959	\N	3995	f
4014	Passarge_1963_100216	\N	\N	\N	\N	\N	Wege zur planmäßigen Vegetationstypenforschung, dargestellt an Hand des Beispiels von Trittpflanzengesellschaften	100216	Berlin	\N	\N	\N		\N	\N	\N	19	19	Y			\N		\N	article	2016-05-04 11:10:36.448	\N	1963	\N	4015	f
4028	Passarge_Hofmann_1968_100224	\N	\N	\N	\N	\N	Pflanzengesellschaften des nordostdeutschen Flachlandes II	100224		\N	\N	\N		\N	\N	\N	1	\N	Y			\N		\N	article	2016-05-04 11:10:38.313	\N	1968	\N	4029	f
4034	Precker_1993_100226	\N	\N	\N	\N	\N	Das Große Göldenitzer Moor und das Teufelsmoor bei Horst.Ein Beitrag zur Entstehungs- und Nutzungsgeschichte Mecklenburger Regenmoore und zu ihrer gegenwärtigen ökologischen Situat	100226		\N	\N	\N		\N	\N	\N	103	103	Y			\N		\N	article	2016-05-04 11:10:38.726	\N	1993	\N	4035	f
4090	Slobodda_1983_100252	\N	\N	\N	\N	\N	Zur aktuellen Vegetation der Peeneniederung nördlich von Anklam und ihren ökologischen Aussagen, dargestellt an einem Aufnahmen-Transekt unter Einbeziehung der Mülldeponie Anklam	100252		\N	\N	\N		\N	\N	\N	62	62	Y			\N		\N	article	2016-05-04 11:10:44.533	\N	1983	\N	4091	f
4183	Wollert_1967_100293	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der Oser Mittelmecklenburgs unter besonderer Berücksichtigung der Trockenrasengesellschaften	100293		\N	\N	\N		DUBLETTE000562	\N	\N	259	259	Y			000562	Universität Rostock	\N	article	2016-05-04 11:10:54.767	\N	1967	\N	4184	f
4205	Wollert_1984_100304	\N	\N	\N	\N	\N	Zum Vorkommen von Hainbuchen- Ulmenhangwäldern im baltischen Buchenwaldgebiet am Rande des Malchiner Beckens (Mittelmecklenburg)	100304		\N	\N	\N		\N	\N	\N	11	11	Y			\N		\N	article	2016-05-04 11:10:57.564	\N	1984	\N	4206	f
4211	Wollert_1988_100307	\N	\N	\N	\N	\N	Zur gegenwärtigen Verbreitung und zum soziologischen Verhalten von Puccinellia distans (JACQ.) PARL. in Mittelmecklenburg	100307		\N	\N	\N		\N	\N	\N	40	40	Y			\N		\N	article	2016-05-04 11:10:58.408	\N	1988	\N	4212	f
4296	Doll_1992_100335	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der stehenden Gewässer in Mecklenburg-Vorpommern, Teil I.4 Littorelletea Br.-Bl. et Tx. 43 - Strandlingsgesellschaften	100335	Berlin	\N	\N	\N		\N	\N	\N	94	94	Y			\N		\N	article	2016-05-04 11:11:05.537	\N	1992	\N	4297	f
4362	Hundt_1968_100356	\N	\N	\N	\N	\N	Die slavische Wallanlage der Jaromarsburg und ihre Vegetation bei Arkona auf der Insel Rügen	100356		\N	\N	\N		\N	\N	\N	12	12	Y			\N		\N	article	2016-05-04 11:11:10.551	\N	1968	\N	4363	f
4156	Voigtlaender_1978_100281	\N	\N	\N	\N	\N	Die Vegetation des Großen Schwerin - Teil des NSG "Großer Schwerin und Steinhorn"	100281		\N	\N	\N		\N	\N	\N	53	53	Y			\N		\N	article	2016-05-04 11:10:51.743	\N	1978	\N	4157	f
4831	Rehbein_Berg_1999_100518	\N	\N	\N	\N	\N	Bemerkenswerte Pflanzen und Pflanzengesellschaften aus dem Rostocker Raum III: Euphorbia palustris L	100518	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	21	21	Y			\N		\N	article	2016-05-04 11:11:51.407	\N	1999	\N	4832	f
4851	Doll_2000_100527	\N	\N	\N	\N	\N	Bemerkenswerte Pflanzenarten und ihre Vergesellschaftungen in Mecklenburg-Vorpommern	100527	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	35	35	Y			\N		\N	article	2016-05-04 11:11:53.637	\N	2000	\N	4852	f
4917	Libbert_1933_100561	\N	\N	\N	\N	\N	Die Vegetationseinheiten der neumärkischen Staubeckenlandschaft unter Berücksichtigung angrenzender Landschaften - 2. Teil	100561		\N	\N	\N		DUBLETTE000032	\N	\N	65	65	Y			000032		\N	article	2016-05-04 11:12:00.275	\N	1933	\N	4918	f
5202	Hensen_Kentrup_1998_100679	\N	\N	\N	\N	\N	Teucrio botryos - Melicetum ciliatae (Traubengamander-Wimpernperlgrasflur) - Lebensstrategien in einer xerothermen Fels-Pioniergesellschaft	100679	Göttingen	\N	\N	\N		Hensen_Kentrup_1998	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:12:24.88	\N	1998	\N	5203	f
5225	Oehmke_2006_100703	\N	\N	\N	\N	\N	Grünland im NSG Ahlbecker Seegrund-Schutz und Pflege gefährdeter Pflanzengemeinschaften	100703	Greifswald	\N	\N	unpubliziert	\N	\N	\N	\N	0	58	Y	\N	\N	\N	Ernst-Moritz-Arndt-Universität	\N	thesis	2016-05-04 11:12:27.052	\N	2006	5226	\N	f
5264	Steinhard_2001_100711	\N	\N	\N	\N	\N	Landschaftsökologische Untersuchungen an Standorten submerser Makrophytenvegetation im Salzhaff (Wismarbucht)	100711	\N	\N	\N	\N	\N	\N	\N	Botanisches Institut	\N	131	Y	\N	\N	\N	Universität Greifswald	\N	thesis	2016-05-11 10:30:27.739	\N	2001	\N	\N	f
5267	Succow_1970_100264b	\N	\N	\N	\N	\N	Die Vegetation nordmecklenburgischer Flußtalmoore und ihre anthropogene Umwandlung.\n- unveröffentlichte Aufnahmen	100263	\N	\N	\N	\N	\N	\N	\N	\N	\N	198	\N	\N	\N	\N	\N	\N	thesis	2016-05-15 08:17:58.85	\N	1970	\N	\N	f
5262	Adler_2001_null	\N	\N	\N	\N	\N	Kronwald	100709	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Universität Greifswald	\N	thesis	2016-05-15 12:59:10.015	\N	2001	\N	\N	f
4838	Bluemel_1999_100521	\N	\N	\N	\N	\N	Zur aktuellen Flora und Vegetation der nährstoffarmen Seen Mecklenburg-Vorpommerns Teil 1: Vom Aussterben bedrohte Sippen	100521	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	36	36	Y			\N		\N	article	2016-05-04 11:11:52.166	\N	1999	\N	4839	f
5263	Schroeder_2008_100710	\N	\N	\N	\N	\N	Die Heidegebiete Hiddensees -Ein Überblick über die vergangenen 150 Jahre Kulturlandschaftsgeschichte	100710	\N	\N	\N	\N	\N	\N	\N	\N	\N	14	\N	\N	\N	\N	Universität Greifswald	\N	thesis	2016-05-11 10:24:16.26	\N	2008	\N	\N	f
2361	Springer._S._1993_000865	\N	\N	\N	\N	\N	Vegetationsaufnahmen bisher unbekannter oder wenig beachteter Pflanzengesellschaften aus den östlichen bayerischen Alpen	000865	Göttingen	\N	\N	\N		\N	\N	\N	10	\N	Y			\N		\N	article	2016-05-04 11:08:01.282	\N	1993	\N	2362	f
3534	Bartz_Bolbrinker_Et_Al._1973_100005	\N	\N	\N	\N	\N	Zur Verbreitung und zum soziologischen Verhalten der Trollblume (Trollius europaeus L.) in Mittelmecklenburg	100005		\N	\N	\N		\N	\N	\N	26	26	Y			\N		\N	article	2016-05-04 11:09:43.34	\N	1973	\N	3535	f
3645	Doll_Kaschube_Et_Al._1982_100054	\N	\N	\N	\N	\N	Die Orchideen des Kreises Altentreptow	100054		\N	\N	\N		\N	\N	\N	2	2	Y			\N		\N	article	2016-05-04 11:09:56.244	\N	1982	\N	3646	f
4226	Wollert_Krauss_Et_Al._1992_100314	\N	\N	\N	\N	\N	Erfolgreiche Regeneration von Feuchtwiesen im Naturschutzgebiet Neukalener Moorwiesen	100314		\N	\N	\N		\N	\N	\N	5	5	Y			\N		\N	article	2016-05-04 11:11:00.358	\N	1992	\N	4227	f
5228	AG_Geoboatnik_MV_2013_100704	\N	\N	\N	\N	\N	Küstenkartierung MV	100704	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N	\N	\N	\N	\N	\N	unpublished	2016-05-04 11:12:27.308	\N	2013	\N	\N	f
3572	Borowiec_Kaussmann_Et_Al._1987_100021	\N	\N	\N	\N	\N	Eine Übersicht zu bestimmten Ackerunkraut-Leitgesellschaften in den jungpleistozänen Ackerfluren des Nordwestens der VR Polen und des Nordens der DDR	100021	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:47.413	\N	1987	\N	3573	f
3852	Kleinke_Succow_Et_Al._1974_100141	\N	\N	\N	\N	\N	Der Wasserstufenzeigerwert von Grünlandpflanzen im nördlichen Teil der DDR	100141	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:18.475	\N	1974	\N	3853	f
3950	Lange_Jeschke_Et_Al._1986_100188	\N	\N	\N	\N	\N	Ralswiek und Rügen.Landschaftsentwicklung und Siedlungsgeschichte der Ostseeinsel. Teil 1: Die Landschaftsgeschichte der Insel Rügen seit dem Spätglacial	100188		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:29.569	\N	1986	\N	3951	f
418	Bueker_1939_000140	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Meßtischblattes Lengerich in Westfalen	000140	Münster	\N	BUE	\N		\N	\N	\N	58	\N	Y			\N		\N	article	2016-05-04 11:05:04.839	\N	1939	\N	419	f
4827	Wollert_Sluschny_Et_Al._1999_100516	\N	\N	\N	\N	\N	Zum Vorkommen einiger kontinental verbreiteter Pflanzengesellschaften im mecklenburgischen Tal der Elbe sowie in den Niederungen ihrer Nebenflüsse Sude und Löcknitz	100516	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:11:50.847	\N	1999	\N	4828	f
1794	Haerdtle_Bracht_Hobohm_1996_000654	\N	\N	\N	\N	\N	Vegetation und Erhaltungszustand von Hartholzauen (Querco-Ulmetum Issl 1924) im Mittelelbegebiet zwischen Lauenburg und Havelberg	000654	Göttingen	\N	\N	\N		\N	\N	\N	26	\N	Y			\N		\N	article	2016-05-04 11:07:10.945	\N	1996	\N	1795	f
1960	Klaege_1993_000711	\N	\N	\N	\N	\N	Der Weinberg bei Fürstlich Drehna - Dokumentation und Nutzungskonzept	000711	Luckau	\N	KLG	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:07:24.929	\N	1993	\N	1961	f
3234	Kraemer_Fartmann_207_001183	\N	\N	\N	\N	\N	Syntaxonomie und Ökologie der flussbegleitenden Pioniergesellschaften des Chaenopodion glauci (Klasse: Bidentetea tripartitae) an der unteren Oder	001183		\N	\N	\N		T274	\N	\N	112	\N	Y			\N		\N	article	2016-05-04 11:09:19.623	\N	207	\N	3235	f
3701	Hilbig_Voigtlaender_1984_100075	\N	\N	\N	\N	\N	Die ökologisch- soziologischen Artengruppen und die Vegetationsformen des Ackers im Gebiet der DDR	100075		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:01.788	\N	1984	\N	3702	f
4150	Voigtlaender_1967_100278	\N	\N	\N	\N	\N	Legousia hybrida (L.)DELARBE in Mecklenburg	100278	Rostock	\N	\N	\N		\N	\N	\N	2	2	Y			\N		\N	article	2016-05-04 11:10:50.991	\N	1967	\N	4151	f
4154	Voigtlaender_1977_100280	\N	\N	\N	\N	\N	Die gegenwärtige Verbreitung der Mehlprimel (Primula farinosa L.) in der DDR	100280		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:51.519	\N	1977	\N	4155	f
4158	Voigtlaender_1981_100282	\N	\N	\N	\N	\N	Das Köhntoptal zwischen Trebenow und Bandelow - ein neues Naturschutzgebiet im Kreis Straßburg, Bezirk Neubrandenburg	100282		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:10:52.011	\N	1981	\N	4159	f
4160	Voigtlaender_1988_100283	\N	\N	\N	\N	\N	Wiederentdeckung von Cardamine parviflora L. in Mecklenburg	100283		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:52.275	\N	1988	\N	4161	f
4162	Voigtlaender_1992_100284	\N	\N	\N	\N	\N	Carex tomentosa L. - neu für Mecklenburg-Vorpommern	100284		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	article	2016-05-04 11:10:52.54	\N	1992	\N	4163	f
5216	LANUV_-_Braun-Blanquetskala_null_100700	\N	\N	\N	\N	\N	?	100700	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	Y	\N	\N	\N	\N	\N	unpublished	2016-05-04 11:12:26.42	\N	1500	\N	\N	t
5218	LANUV_-_Braun-Blanquet_(old)_null_100701	\N	\N	\N	\N	\N	?	100701	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	Y	\N	\N	\N	\N	\N	unknown	2016-05-04 11:12:26.622	\N	1500	\N	\N	t
5402	Runge_1994_101008	\N	\N	\N	\N	\N	Dauerquadrat-Untersuchungen auf den Inseln Baltrum und Langeoog.	101008	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:12.461	\N	1994	\N	5403	f
5404	Bernhardt_1994_101009	\N	\N	\N	\N	\N	Soziologie und Dynamik der Claytonia perfoliata-Bestände auf der ostfriesischen Insel Baltrum	101009	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:12.532	\N	1994	\N	5405	f
227	Ruehl_1960_000074	\N	\N	\N	\N	\N	Über die Waldgesellschaften der Kalkgebiete nordwestdeutscher Mittelgebirge	000074	Bonn	\N	\N	\N		\N	\N	\N	320	\N	Y			\N		\N	article	2016-05-04 11:04:48.649	\N	1960	\N	228	f
420	Bueker_1942_000141	\N	\N	\N	\N	\N	Beiträge zur Vegetationskunde des südwestfälischen Berglandes	000141		\N	\N	\N		\N	\N	\N	136	\N	Y			\N		\N	article	2016-05-04 11:05:05.099	\N	1942	\N	421	f
423	Bueker_Engel_1951_000142	\N	\N	\N	\N	\N	Die wichtigsten Pflanzengesellschaften der Dauerweiden an der Ems im nördlichen Westfalen	000142		\N	\N	\N		THOMAS	\N	\N	17	\N	Y			\N		\N	article	2016-05-04 11:05:05.362	\N	1951	\N	424	f
447	Dierschke_Tuexen_1975_000152	\N	\N	\N	\N	\N	Die Vegetation des Langholter- und Rhauder Meeres und seiner Randgebiete	000152	Göttingen	\N	\N	\N		\N	\N	\N	55	\N	Y			\N		\N	article	2016-05-04 11:05:08.044	\N	1975	\N	448	f
543	Horst_Krausch_Mueller-Stoll_1966_000186	\N	\N	\N	\N	\N	Die Wasser- und Sumpfpflanzengesellschaften im Elb-Havel-Winkel	000186	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:16.281	\N	1966	\N	544	f
546	Huelbusch_1970_000187	\N	\N	\N	\N	\N	Die Floh-Segge (Carex pulicaris L.) in einem Kalk-Kleinseggenried bei Eisbergen/Wesertal	000187	Münster	\N	\N	\N		\N	\N	\N	4	\N	Y			\N		\N	article	2016-05-04 11:05:16.504	\N	1970	\N	547	f
700	Mueller-Stoll_Fischer_Krausch_1962_000247	\N	\N	\N	\N	\N	Verbreitungkarten brandenburgischer Leitpflanzen. 4. Reihe	000247	Potsdam	\N	\N	\N		\N	\N	\N	7	\N	Y			\N		\N	article	2016-05-04 11:05:30.899	\N	1962	\N	701	f
702	Mueller-Stoll_Fischer_Krausch_1992_000248	\N	\N	\N	\N	\N	Die Grünlandgesellschaften des Spreewaldes. Teil 1	000248	Berlin	\N	\N	\N		\N	\N	\N	196	\N	Y			\N		\N	article	2016-05-04 11:05:31.128	\N	1992	\N	703	f
704	Mueller-Stoll_Fischer_Krausch_1992_000249	\N	\N	\N	\N	\N	Die Grünlandgesellschaften des Spreewaldes. Teil 2	000249	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:31.357	\N	1992	\N	705	f
706	Mueller-Stoll_Fischer_Krausch_1992_000250	\N	\N	\N	\N	\N	Die Grünlandgesellschaften des Spreewaldes. Teil 3	000250	Berlin	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:05:31.634	\N	1992	\N	707	f
708	Mueller-Stoll_Fischer_Krausch_1992_000251	\N	\N	\N	\N	\N	Die Grünlandgesellschaften des Spreewaldes. Teil 4	000251	Berlin	\N	\N	\N		\N	\N	\N	50	\N	Y			\N		\N	article	2016-05-04 11:05:31.917	\N	1992	\N	709	f
953	Tuexen_1954_000345	\N	\N	\N	\N	\N	Pflanzengesellschaften und Grundwasserganglinien	000345	Stolzenau/Weser	\N	\N	\N		\N	\N	\N	135	\N	Y			\N		\N	article	2016-05-04 11:05:55.189	\N	1954	\N	954	f
957	Tuexen_1974_000347	\N	\N	\N	\N	\N	Die Haselünner Kuhweide	000347	Göttingen	\N	\N	\N		\N	\N	\N	16	\N	Y			\N		\N	article	2016-05-04 11:05:55.709	\N	1974	\N	958	f
959	Tuexen_Dierschke_1968_000348	\N	\N	\N	\N	\N	Das Bullerbachtal in Sennestadt, eine pflanzensoziologische Lehranlage	000348	Göttingen	\N	\N	\N		\N	\N	\N	10	\N	Y			\N		\N	article	2016-05-04 11:05:55.976	\N	1968	\N	960	f
975	Verbuecheln_1987_000354	\N	\N	\N	\N	\N	Die Mähwiesen und Flutrasen der Westfälischen Bucht und des Nordsauerlandes	000354	Münster	\N	\N	\N		\N	\N	\N	441	\N	Y			\N		\N	article	2016-05-04 11:05:57.44	\N	1987	\N	976	f
1461	Ruether_1968_000531	\N	\N	\N	\N	\N	Der Enzian-Zwenkenrasen auf dem Franckenberg bei Vinsebeck/Kr. Höxter	000531	Münster	\N	RUR	\N		TABGESNE	\N	\N	1	\N	Y			\N		\N	article	2016-05-04 11:06:41.329	\N	1968	\N	1462	f
1467	Schlueter_1955_000533	\N	\N	\N	\N	\N	Das Naturschutzgebiet Strausberg. Vegetationskundliche Monographie einer märkischen Jungdiluviallandschaft	000533	Berlin	\N	SLU	\N		TABGESNE	\N	\N	10	\N	Y			\N		\N	article	2016-05-04 11:06:41.761	\N	1955	\N	1468	f
2032	Tuexen_1962_000740	\N	\N	\N	\N	\N	Der Maujahn. Skizze der Pflanzengesellschaften eines wendländischen Moores	000740	Zürich	\N	\N	\N		\N	TV	\N	17	\N	Y			\N		\N	article	2016-05-04 11:07:31.556	\N	1962	\N	2033	f
2055	Kuehn_2000_000749	\N	\N	\N	\N	\N	Ökologisch-numerische Untersuchungen an Wäldern in der westfälischen Bucht. - Ein Beitrag zur Biodiversitäts- und Altwald-Forschung	000749	Nümbrecht	\N	\N	\N		\N	\N	\N	79	\N	Y			\N		\N	book	2016-05-04 11:07:33.81	\N	2000	\N	2056	f
2065	Mueller-Stoll_Krausch_1960_000752	\N	\N	\N	\N	\N	Verbreitungskarten brandenburgischer Leitpflanzen. Dritte Reihe	000752	Potsdam	\N	\N	\N		\N	\N	\N	29	\N	Y			\N		\N	article	2016-05-04 11:07:34.57	\N	1960	\N	2066	f
2191	Mueller_1991_000797	\N	\N	\N	\N	\N	Verbreitung, Vergesellschaftung und Rückgang des Zwergrohrkolbens (Typha minima Hoppe)	000797	Regensburg	\N	\N	\N		\N	\N	\N	23	\N	Y			\N		\N	article	2016-05-04 11:07:45.213	\N	1991	\N	2192	f
2705	Tuerk_1994_000993	\N	\N	\N	\N	\N	Das "Höllental" im Frankenwald - Flora und Vegetation eines floristisch bemerkenswerten Mittelgebirgstales	000993	Göttingen	\N	\N	\N		HEINKEN_DIP	\N	\N	11	\N	Y			\N		\N	article	2016-05-04 11:08:31.573	\N	1994	\N	2706	f
2790	Schlueter_1993_001026	\N	\N	\N	\N	\N	Zur Einbürgerung des Perückenstrauches (Cotinus coggygria Scop.)	001026	Berlin, Stuttgart	\N	\N	\N		\N	\N	\N	6	\N	Y			\N		\N	article	2016-05-04 11:08:39.496	\N	1993	\N	2791	f
3066	Pott_Hueppe_1991_001121	\N	\N	\N	\N	\N	Die Hudelandschaften Nordwestdeutschlands	001121	Münster	\N	\N	\N		HEINKEN2	\N	\N	51	\N	Y			\N		\N	article	2016-05-04 11:09:03.796	\N	1991	\N	3067	f
3217	Wotke_Buecking_1999_001176	\N	\N	\N	\N	\N	Aufnahmen der Vegetation im "Bannwald Sommerberg"	001176		\N	\N	\N		\N	\N	\N	60	\N	Y			\N		\N	article	2016-05-04 11:09:17.987	\N	1999	\N	3218	f
3227	Bueltmann_2005_001180	\N	\N	\N	\N	\N	Strategien und Artenreichtum von Erdflechten in Sandtrockenrasen	001180	Göttingen	\N	\N	\N		VW_T253	\N	\N	120	\N	Y			\N		\N	article	2016-05-04 11:09:18.951	\N	2005	\N	3228	f
3975	Pankow_Huelsmeyer_1976_100198	\N	\N	\N	\N	\N	Über die Entstehung, Entwicklungsgeschichte und Vegetation des Großen Moores bei Graal-Müritz	100198	Berlin	\N	\N	\N		\N	\N	\N	156	156	Y			\N		\N	article	2016-05-04 11:10:31.999	\N	1976	\N	3976	f
4849	Bluemel_Brozio_2000_100526	\N	\N	\N	\N	\N	Floristische Mitteilungen aus Greifswald und Umgebung II	100526	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	2	2	Y			\N		\N	article	2016-05-04 11:11:53.409	\N	2000	\N	4850	f
4872	Pietsch_Mueller-stoll_1968_100535	\N	\N	\N	\N	\N	Die Zwergbinsen-Gesellschaft der nackten Teichböden im östlichen Mitteleuropa, Eleocharito-Caricetum bohemicae	100535	Floristisch-soziologischne AG N.F	\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:11:55.841	\N	1968	\N	4873	f
4836	Bluemel_Kiphuth_Et_Al._1999_100520	\N	\N	\N	\N	\N	Floristische Mitteilungen aus Greifswald und Umgebung I	100520	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	4	4	Y			\N		\N	article	2016-05-04 11:11:51.892	\N	1999	\N	4837	f
4955	Mueller-stoll_Scharf_Et_Al._1987_100592	\N	\N	\N	\N	\N	Die Erlen- und Birkenbrüche in Nordwest-Brandenburg und ihre Bodenverhältnisse	100592	Berlin	\N	\N	\N		\N	\N	\N	7	7	Y			\N		\N	article	2016-05-04 11:12:03.919	\N	1987	\N	4956	f
201	Tuexen_1928_000065	\N	\N	\N	\N	\N	Bericht über die pflanzensoziologische Excursion der floristisch-soziologischen Arbeitsgemeinschaft nach dem Plesswalde bei Göttingen. 14. August 1927	000065		\N	\N	\N		\N	\N	\N	18	\N	Y			\N		\N	article	2016-05-04 11:04:46.284	\N	1928	\N	202	f
697	Mueller_1986_000246	\N	\N	\N	\N	\N	Floristisch-vegetationskundliche Untersuchungen an Pflanzengesellschaften des Caricion davallianae Klika 1934 in der nördlichen Kalkeifel	000246	Göttingen	\N	\N	\N		\N	\N	\N	35	\N	Y			\N		\N	article	2016-05-04 11:05:30.609	\N	1986	\N	698	f
1550	Zuendorf_1980_000565	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen im oberen Werratal bei Themar	000565		\N	ZUE	\N		TABGESNE	\N	\N	24	\N	Y			\N		\N	article	2016-05-04 11:06:49.243	\N	1980	\N	1551	f
1687	Schlueter_1959_000616	\N	\N	\N	\N	\N	Waldgesellschaften und Wuchsbezirksgliederung im Grenzbereich der Eichen-Buchen- zur Buchenstufe am Nordwestabfall des Thüringer Waldes	000616	Berlin	\N	SH	\N		BOHN	\N	\N	56	\N	Y			\N		\N	article	2016-05-04 11:07:01.817	\N	1959	\N	1688	f
2709	Walentowski_Mueller_Obermeier_1994_000994	\N	\N	\N	\N	\N	Some remarks to the Leucobryo-Pinetum sylvestris Matuszk. 1962 on its southwestern limit in Bavaria	000994		\N	\N	\N		HEINKEN_DIP	\N	\N	47	\N	Y			\N		\N	article	2016-05-04 11:08:31.865	\N	1994	\N	2710	f
3251	Suess_Storm_Schwabe_2010_001188	\N	\N	\N	\N	\N	Sukzessionslinien in basenreicher offener Sandvegetation des Binnenlandes: Ergebnisse aus Untersuchungen von Dauerbeobachtungsflächen	001188	Göttingen	\N	\N	\N		VW_T307	\N	\N	64	\N	Y			\N		\N	article	2016-05-04 11:09:20.882	\N	2010	\N	3252	f
3487	Mueller_Et_Al._1992_001267	\N	\N	\N	\N	\N	Auswirkungen unterschiedlicher Flußbaumaßnahmen auf Flußmorphologie und Auenvegetation des Lech - eine Bilanz nach 100 Jahren Wasserbau	001267		http://www.fh-erfurt.de/lgf/fileadmin/LA/Personen/Mueller/prevPub/Mueller_et_al_1992_Flussbauauswirkungen_Lech_web.pdf	\N	0000000014		\N	\N	\N	0	\N	Y			\N		\N	article	2016-05-04 11:09:39.493	\N	1992	\N	3488	f
3321	Springemann_Meyer_chmidt_Karste_Thiel_Weckesser_Schultz_Marten_Schikora_Schmidt_Meineke_Wimmer_Wuestemann_Spaeth_Raimer_2012_001207	\N	\N	\N	\N	\N	Waldforschung im Nationalpark Harz - Waldforschungsfläche Bruchberg: Methodik und Aufnahme 2008/09	001207		\N	\N	\N		\N	\N	\N	73	\N	Y			\N		\N	article	2016-05-04 11:09:25.657	\N	2012	\N	3322	f
157	Boensel_Schmidt_1991_000051	\N	\N	\N	\N	\N	Vegetation und Flora des Naturschutzgebietes "Ernstberg" bei Sichenhausen	000051	Frankfurt am Main	\N	\N	\N		\N	\N	\N	46	\N	Y			\N		\N	article	2016-05-04 11:04:42.867	\N	1991	\N	158	f
175	Boensel_Gregor_1992_000057	\N	\N	\N	\N	\N	Die Schalksbachteiche bei Herbstein	000057	Frankfurt am Main	\N	\N	\N		\N	\N	\N	67	\N	Y			\N		\N	article	2016-05-04 11:04:44.334	\N	1992	\N	176	f
185	Foerster_Hepting_2007_000060	\N	\N	\N	\N	\N	Weitere Beiträge zur Kenntnis hessischer Pflanzengesellschaften: Borstgrasrasen und Zwergstrauchheiden	000060	Frankfurt am Main	\N	\N	\N		\N	\N	\N	27	\N	Y			\N		\N	article	2016-05-04 11:04:45.092	\N	2007	\N	186	f
198	Boeger_1991_000064	\N	\N	\N	\N	\N	Grünlandvegetation im Hessischen Ried - Pflanzensoziologische Verhältnisse und Naturschutzkonzeption	000064		\N	\N	\N		\N	\N	\N	567	\N	Y			\N		\N	article	2016-05-04 11:04:46.052	\N	1991	\N	199	f
211	Reichhoff_Boehnert_Knapp_1979_000068	\N	\N	\N	\N	\N	Die Vegetation des Naturschutzgebietes "Tote Täler"	000068		\N	\N	\N		\N	\N	\N	13	\N	Y			\N		\N	article	2016-05-04 11:04:47.088	\N	1979	\N	212	f
300	Schoenfelder_1978_000099	\N	\N	\N	\N	\N	Vegetationsverhältnisse auf Gips im südwestlichen Harzvorland	000099	Hannover	\N	\N	\N		\N	\N	\N	232	\N	Y			\N		\N	article	2016-05-04 11:04:54.888	\N	1978	\N	301	f
495	Goers_1961_000170	\N	\N	\N	\N	\N	Das Pfrunger Ried. Die Pflanzengesellschaften eines oberschwäbischen Moorgebietes	000170	Stuttgart, Tübingen	\N	\N	\N		\N	\N	\N	54	\N	Y			\N		\N	article	2016-05-04 11:05:12.392	\N	1961	\N	496	f
497	Goers_1968_000171	\N	\N	\N	\N	\N	Der Wandel der Vegetation im Naturschutzgebiet Schwenninger Moos unter dem Einfluß des Menschen in zwei Jahrhunderten	000171	Karlsruhe	\N	\N	\N		\N	\N	\N	131	\N	Y			\N		\N	article	2016-05-04 11:05:12.697	\N	1968	\N	498	f
504	Goetz_Riegel_1989_000173	\N	\N	\N	\N	\N	Die Vegetation der Bachtäler im Einzugsbereich der Ilz im Bayerischen Wald	000173	Regensburg	\N	\N	\N		\N	\N	\N	478	\N	Y			\N		\N	article	2016-05-04 11:05:13.169	\N	1989	\N	505	f
1923	Moelder_Bernhardt-Roemermann_Schmidt._W._2006_000698	\N	\N	\N	\N	\N	Forest ecosystem research in Hainich National Park (Thuringia): First results on flora and vegetation in stands with contrasting tree species diversity	000698		Auch Diss. Universität Göttingen, 2008	\N	0000000004		\N	\N	\N	12	\N	Y			\N		\N	article	2016-05-04 11:07:22.029	\N	2006	\N	1924	f
3438	Doering-mederake_1991_001249	\N	\N	\N	\N	\N	Feuchtwälder im nordwestdeutschen Tiefland. Gliederung - Ökologie - Schutz	001249	Göttingen	auch 1990: Diss. Univ. Göttingen	\N	0000000013		D�ring_Mederake_1991	\N	\N	834	\N	Y			\N		\N	article	2016-05-04 11:09:35.174	\N	1991	\N	3439	f
5342	Damm_1994_001305	\N	\N	\N	\N	\N	Vegetation und Florenbestand des Brockengebietes	001305	\N	\N	\N	auch Diplomarb. Universität Göttingen	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 20:50:20.475	\N	1994	\N	5343	f
5345	Reif_Weiskopf_1988_001306	\N	\N	\N	\N	\N	Ökologische Untersuchungen an der Verschiedenblättrigen Kratzdistel (Cirsium helenioides L.) in Oberfranken. Teil I: Vergesellschaftung und Standort	001306	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 20:51:59.226	\N	1988	\N	5346	f
1917	Schmitt_Fartman_Hoelzel_2010_000696	\N	\N	\N	\N	\N	Vergesellschaftung und Ökologie der Sumpf-Siegwurz (Gladiolus palustris) in Südbayern	000696	Göttingen	\N	\N	\N		vegetweb	\N	\N	151	\N	Y			\N		\N	article	2016-05-04 11:07:21.49	\N	2010	\N	1918	f
3072	Roedel_1987_001123	\N	\N	\N	\N	\N	Vegetationsentwicklung nach Grundwasserabsenkungen. Dargestellt am Beispiel des Fuhrberger Feldes in Niedersachsen	001123	Berlin	\N	\N	\N		HEINKEN2	\N	\N	42	\N	Y			\N		\N	article	2016-05-04 11:09:04.514	\N	1987	\N	3073	f
5282	Horn_2012_null	\N	\N	\N	\N	\N	Vegetationsentwicklung der Boddeninsel 'Schmidts Builten' von 1993 bis 2012 im Kontext ihrer Nutzung	100715	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Bachelor-Arbeit im Studiengang Agrarökologie	\N	Universität Rostock	\N	thesis	2016-08-24 14:41:25.82	\N	2012	\N	\N	f
5284	Manegold_2007_null	\N	\N	\N	\N	\N	Standörtliche und floristische Unterschiede zwischen Wäldern unterschiedlicher Bestandeskontinuität im Südschwarzwald (Gemarkung Hinterzarten)	001281	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-12 17:45:02.383	\N	2007	\N	\N	f
5289	Glaeser_2005_001283	\N	\N	\N	\N	\N	Untersuchungen zur historischen Entwicklung und Vegetation mitteldeutscher Auenwälder	001283	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	UFZ	\N	thesis	2016-09-12 17:54:28.521	\N	2005	\N	\N	f
5291	Ewald_Endres_2015_001284	\N	\N	\N	\N	\N	Waldvegetation der Sassau im Walchensee: Vergleich von Naturwald und Wirtschaftswald, Insel und Halbinsel	001284	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 17:58:28.064	\N	2015	\N	5292	f
5293	Mueller_1988_1-176	\N	\N	\N	\N	\N	Südbayerische Parkrasen. Soziologie und Dynamik bei unterschiedlicher Pflege	001285	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2016-09-12 18:04:01.182	\N	1988	\N	5294	f
5295	Springer_1997_001286	\N	\N	\N	\N	\N	Pflanzengesellschaften der Almen des Berchtesgadener Landes	001286	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	GH Kassel, FB Stadtplanung/Landschaftsplanung	\N	thesis	2016-09-12 18:09:25.086	\N	1997	\N	5296	f
5298	Hellwig_2000_001287	\N	\N	\N	\N	\N	Auenregeneration an der Elbe, Untersuchungen zur Syndynamik und Bioindikation von Pflanzengesellschaften an der Unteren Mittelelbe bei Lenzen	001287	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Universität Hannover	\N	thesis	2016-09-12 18:12:45.413	\N	2000	\N	\N	f
5299	Krumbiegel_2014_001288	\N	\N	\N	\N	\N	Aktuelle Situation einiger Vorkommen von Fritillaria meleagris in Sachsen-Anhalt	001288	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 18:17:17.757	\N	2014	\N	5300	f
5301	Ruether_2006_001289	\N	\N	\N	\N	\N	Zum Wiederfund des Karlszepters (Pedicularis sceptrum-carolinum) im Vorderen Bayerischen Wald bei Oberbreitenau (Landkreis Regen):historischer Rückblick und aktuelle Situation.	001289	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-12 18:20:44.596	\N	2006	\N	5302	f
5303	Springer_2008_001290	\N	\N	\N	\N	\N	Neophytenreiche Pflanzengesellschaften in München. Ein Beitrag im Rahmen des Projektes "Flora von München".	001290	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-12 18:23:58.252	\N	2008	\N	5304	f
5305	Springer_1996_001291	\N	\N	\N	\N	\N	Bemerkenswerte Pflanzengesellschaften in Südbayern.	001291	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-12 18:29:52.655	\N	1996	\N	5306	f
5308	Springer_Wiesner_2000_001292	\N	\N	\N	\N	\N	Pflanzengesellschaften der Hurlacher Haide	001292	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-12 18:32:10.79	\N	2000	\N	5309	f
5311	Wagner_Wagner_2000_001293	\N	\N	\N	\N	\N	Juncus stygius L. - ein aktueller Nachweis für Bayern und die Bundesrepublik	001293	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-12 18:35:14.255	\N	2000	\N	5312	f
5314	Rueckerte_Wittig_1984_001294	\N	\N	\N	\N	\N	Der Flattergras-Buchenwald im Spessart	001294	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-12 18:40:58.041	\N	1984	\N	5315	f
5317	Mayer_Urban_1992_001295	\N	\N	\N	\N	\N	Floristische und vegetationskundliche Besonderheiten aus den Bayerischen Alpen,  Funde im Rahmen der Alpenbiotopkartierung Teil 1	001295	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-12 18:46:15.507	\N	1992	\N	5318	f
5319	Urban_Mayer_2008_001296	\N	\N	\N	\N	\N	Floristische und vegetationskundliche Besonderheiten aus den Bayerischen Alpen,  Funde im Rahmen der Alpenbiotopkartierung Teil 3	001296	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-12 18:49:07.873	\N	2008	\N	5320	f
5322	Murmann-Kristel_1987_001297	\N	\N	\N	\N	\N	Das Vegetationsmosaik im Nordschwarzwälder Waldgebiet	001297	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2016-09-12 18:52:39.997	\N	1987	\N	5323	f
5325	Rehder_1982_001298	\N	\N	\N	\N	\N	Nitrogen relations of ruderal communities (Rumicion alpini) in the northern calcareous Alps	001298	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 18:54:38.233	\N	1982	\N	5326	f
5327	Wittig_2000_001299	\N	\N	\N	\N	\N	Das Luzulo luzuloidis-Thelypteridetum limbospermae, eine azidokline Saumgesellschaft der höheren Mittelgebirge	001299	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 19:26:24.173	\N	2000	\N	5328	f
5330	Reif_Kuespert_1993_001300	\N	\N	\N	\N	\N	Die Flachmoore im Weißenstädter Becken (Fichtelgebirge) : Vegetation, historische und heutige Standortbedingungen, Schutzwürdigkeit. (Daten aus Küspert, B. 1989: Diplomarb. Universität Bayreuth	001300	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-12 19:28:26.016	\N	1993	\N	5331	f
5332	Reif_Hetzel_1994_001301	\N	\N	\N	\N	\N	Die Vegetation der Waldaußenränder des Großen Kappeler Tales bei Freiburg, Südschwarzwald	001301	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 19:43:20.95	\N	1994	\N	5333	f
5334	Vogel_1981_001304	\N	\N	\N	\N	\N	Zur Vergesellschaftung von Cicerbita alpina und Ranunculus platanifolius im Westharz	001304	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 19:45:47.467	\N	1981	\N	5335	f
5337	Woerz_1989_001302	\N	\N	\N	\N	\N	Zur geographischen Gliederung hochmontaner und subalpiner Hochstaudenfluren und Goldhaferwiesen	001302	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 19:49:09.598	\N	1989	\N	5338	f
5339	Kaestner_1938_001303	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des westsächsischen Berg- und Hügellandes (Flußgebiet der Freiberger und Zwickauer Mulde).  IV. Teil. Die Pflanzengesellschaften der Quellfluren und Bachufer und der Verband der Schwarzerlen-Gesellschaften	001303	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 19:50:43.884	\N	1938	\N	5340	f
5348	Woike_1958_001307	\N	\N	\N	\N	\N	Pflanzensoziologische Studien in der Hildener Heide	001307	\N	\N	\N	Diss.	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 20:59:34.728	\N	1958	\N	5349	f
5350	Walentowski_Obermeier_1992_001308	\N	\N	\N	\N	\N	Rasen mit Dactylorhiza sambucina (L.) Sóo am Brotjacklriegel im Vorderen Bayerischen Wald (Bayern)	001308	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 21:01:16.185	\N	1992	\N	5351	f
5277	de_Mol_von_Redwitz_Gerowitt_2015_null	\N	\N	\N	\N	\N	Weed species composition of maize fields in Germany is influenced by site and crop sequence	100714	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	unknown	2016-07-22 08:56:11.292	\N	2015	\N	5278	t
5352	Krumbiegel_2015_001309	\N	\N	\N	\N	\N	Wiederfund von Lindernia procumbens am Bleddiner Dorfteich (Lkrs. Wittenberg)	001309	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 21:02:22.357	\N	2015	\N	5353	f
5354	Klauck_2014_001310	\N	\N	\N	\N	\N	Beitrag zur Kenntnis der Bärwurz-Bergwiesen im westlichen Erzgebirge	001310	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 21:03:15.897	\N	2014	\N	5355	f
5356	Raehse_2011_001311	\N	\N	\N	\N	\N	Veränderungen der hessischen Grünlandvegetation seit Beginn der 50er Jahre am Beispiel ausgewählter Tal- und Bergregionen Nord- und Mittelhessens	001311	\N	\N	\N	\N		\N	\N	\N	\N	\N	\N	\N	Kassel, university press GmbH	\N	Diss. Universität Kassel (1999)	\N	book	2016-09-12 21:04:55.917	\N	2011	\N	5357	f
5359	Blueml_2011_001312	\N	\N	\N	\N	\N	Langfristige Veränderungen von Flora und Vegetation des Grünlandes in der Dümmerniedeung (Niedersachsen) unter dem Einfluss von Naturschutzmaßnahmen	001312	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Universität Bremen	\N	thesis	2016-09-12 21:11:31.756	\N	2011	\N	5360	f
5362	Gausmann_2012_001313	\N	\N	\N	\N	\N	Ökologie, Floristik, Phytosoziologie und Altersstruktur von Industriewäldern des Ruhrgebietes	001313	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Diss. Ruhr-Universität Bochum, Fak. Geowissenschaften	\N	thesis	2016-09-12 21:13:15.007	\N	2012	\N	5363	f
5369	Morkel_2001_001316	\N	\N	\N	\N	\N	Raum-zeitliche Variation der Wanzenassoziationen Insecta: Heteroptera) eines Biotopkomplexes im Vogelsberg (Hessen)	001316	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Cuvillier Verlag Göttingen	\N	Diss. Universität Gießen	\N	book	2016-09-12 21:16:29.744	\N	2001	\N	5370	f
5371	Fuchs_2013_001317	\N	\N	\N	\N	\N	Dynamik der Erlenbruchwälder, Moorbirken-Moorwälder und Gagelgebüsche im Übergang Niederrhein-Ruhrgebiet, Eine vegetationskundliche Analyse unter besonderer Berücksichtigung der Moose	001317	\N	\N	\N	[auch Diss. Ruhr-Universität Bochum 2012, 240 S.]	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 21:17:50.431	\N	2013	\N	5372	f
5373	Kaule_Pfadenhauer_1973_001318	\N	\N	\N	\N	\N	Die Vegetation und Ökologie eines Hochmoorrandbereichs im Naturschutzgebiet Eggstätt-Hemhofer Seenplatte	001318	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 21:19:04.674	\N	1973	\N	5374	f
5376	Kubitz_1999_001319	\N	\N	\N	\N	\N	Beitrag zur Kenntnis der Supf- und Wasserpflanzengesellschaften im Bereich der Elster-Luppe-Aue bei Leipzig	001319	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-12 21:19:56.511	\N	1999	\N	5377	f
5379	Breitschwerdt_Jandt_2011_001320	\N	\N	\N	\N	\N	Vegetationsaufnahmen 2011 des BERICH-Projektes in den Deutschen Biodiversitäts-Exploratorien	001320	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-12 21:20:56.351	\N	2011	\N	\N	f
5383	Haese_1996_101002	\N	\N	\N	\N	\N	Pflanzenökologische und bodenökologische Untersuchungen an Thero-Salicornietea-Gesellschaften auf der Nordseeinsel Mellum	101002	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:11.984	\N	1996	\N	5384	f
5386	Hobohm_1993_101003	\N	\N	\N	\N	\N	Die Pflanzengesellschaften von Norderney	101003	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:12.068	\N	1993	\N	5387	f
5391	Metzing_Kuhbier_Kuever_2011_101004	\N	\N	\N	\N	\N	Crassula tillaea (Crassulaceae) auf Baltrum - Erstnachweis für Niedersachsen.	101004	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:12.145	\N	2011	\N	5392	f
5394	Tuexen_Boeckelmann_1957_101005	\N	\N	\N	\N	\N	Scharhörn. Die Vegetation einer jungen ostfriesischen Vogelinsel.	101005	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:12.244	\N	1957	\N	5395	f
5397	Gerhardt_1973_101006	\N	\N	\N	\N	\N	Pflanzensoziologische Arbeiten in Düne und Heller der ostfriesischen Inseln.	101006	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:12.316	\N	1973	\N	5398	f
5408	von_Ahnen_Roettger_1990_101010	\N	\N	\N	\N	\N	Kartierung der Trampelpfade auf Spiekeroog	101010	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:12.611	\N	1990	\N	5409	f
5411	Schnaidt_1990_101011	\N	\N	\N	\N	\N	Vergleichende Standortuntersuchungen ausgewählter Pflanzengesellschaften auf Langeoog	101011	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:12.68	\N	1990	\N	5412	f
5414	Kiffe_1989_101012	\N	\N	\N	\N	\N	Der Einfluß der Kaninchenbeweidung auf die Vegetation am Beispiel des Straußgras-Dünenrasens der Ostfriesischen Inseln	101012	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:12.757	\N	1989	\N	5415	f
5416	Kuever_1989_101013	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchung über Alopecurus bulbosus-Verbreitung und Ökologie in Nordwestdeutschland	101013	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:12.826	\N	1989	\N	5417	f
5419	Peters_1997_101014	\N	\N	\N	\N	\N	Aspekte der Entwicklung initialer Bestände des Junco baltici-Schoenetum nigricantis auf der Nordseeinsel Borkum	101014	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:12.897	\N	1997	\N	5420	f
5422	Kuhbier_Weber_2003_101015	\N	\N	\N	\N	\N	Senecio inaequidens DC. als Bestandteil der natürlichen Dünenvegetation auf den Ostfriesischen Inseln.	101015	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:12.967	\N	2003	\N	5423	f
5425	Schwabe_1984_101016	\N	\N	\N	\N	\N	Vegetationskundliche und blütenökologische Untersuchungen in Salzrasen der Nordseeinsel Borkum	101016	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:13.055	\N	1984	\N	\N	f
5427	Schnaidt_Kreeb_1993_101017	\N	\N	\N	\N	\N	Vergleichende Untersuchungen zu den Standortsverhältnissen der Außengrodenvegetation auf Langeoog	101017	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:13.136	\N	1993	\N	5428	f
5432	Janiesch_2003_101019	\N	\N	\N	\N	\N	Biomasseproduktion, Wasser- und Mineralstoffhaushalt der Empetrum-Heiden auf Norderney	101019	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:13.28	\N	2003	\N	5433	f
5435	Kinder_1997_101020	\N	\N	\N	\N	\N	Zur Biologie und Ökologie des Knollenfuchsschwanzes (Alopecurus bulbosus Gouan) an der deutschen Nordseeküste.	101020	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:13.345	\N	1997	\N	5436	f
5438	Klinger_1976_101021	\N	\N	\N	\N	\N	Vier Neufunde von Campylopus introflexus (Hedw.) Brid. im Weser-Ems-Gebiet (Bryophyta)	101021	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:13.411	\N	1976	\N	5439	f
5441	Weeda_1986_101022	\N	\N	\N	\N	\N	Die Punktierte Segge Carex punctata Gaudin auf Langeoog wieder gefunden.	101022	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:13.479	\N	1986	\N	5442	f
5444	Muehl_1993_101023	\N	\N	\N	\N	\N	Zur Synsystematik der Krähenbeerheiden auf den Ostfriesischen Inseln	101023	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:13.545	\N	1993	\N	5445	f
5447	Ringer_1994_101024	\N	\N	\N	\N	\N	Untersuchungen von Birken-Buschwäldern auf en ostfriesischen Inseln Borkum und Norderney unter vorwiegend synstematischen und syntaxonomischen Gesichtspunkten	101024	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:13.61	\N	1994	\N	5448	f
5449	Tuexen_1952_101025	\N	\N	\N	\N	\N	Lütje Hörn. Die Vegetation einer alten ostfriesischen Sandbank	101025	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:13.706	\N	1952	\N	5450	f
5452	Christiansen_1955_101026	\N	\N	\N	\N	\N	Salicornietum	101026	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:13.772	\N	1955	\N	5453	f
5454	Tuexen_1957_101027	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Außendeichlandes von Neuwerk.	101027	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:13.839	\N	1957	\N	5455	f
5456	Loetschert_1973_101028	\N	\N	\N	\N	\N	Das Beto-Atriplicetum laciniatae Tx. (1950) 1967 in Deutschland.	101028	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:13.909	\N	1973	\N	5457	f
5458	Schwabe_1975_101029	\N	\N	\N	\N	\N	Dauerquadrat-Beobachtungen in den Salzwiesen der Nordseeinsel Trischen	101029	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:13.976	\N	1975	\N	5459	f
5461	Tuexen_Westhoff_1963_101030	\N	\N	\N	\N	\N	Saginetea maritimae, eine Gesellschaftsgruppe im wechselhalinen Grenzbereich der europäischen Meeresküsten	101030	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:14.045	\N	1963	\N	5462	f
5464	von_Glahn_1986_101031	\N	\N	\N	\N	\N	Queckengesellschaften (Astero tripolii-Agropyretum repentis ass.nov. und Agropyretum litoralis Br.-Bl. & De Leeuw 1936) im oldenburgisch-ostfriesischen Küstenbereich.	101031	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:14.115	\N	1986	\N	5465	f
5466	Menke_1969_101032	\N	\N	\N	\N	\N	Vegetationskundliche und vegetationsgeschichtliche Untersuchungen an Strandwällen	101032	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:14.182	\N	1969	\N	5467	f
5468	Christiansen_1960_101033	\N	\N	\N	\N	\N	Polygonum raji Bab. ssp. raji Scholz auf Helgoland	101033	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:14.249	\N	1960	\N	5469	f
5471	Kahle_1999_101034	\N	\N	\N	\N	\N	Kompartimentierung von Stickstoff und Phosphor als Pflanzennährstoffe unter dem Einfluß von Kaninchen (Oryctolagus cuniculus L.)	101034	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:14.318	\N	1999	\N	5472	f
5476	Kamm_1983_101036	\N	\N	\N	\N	\N	Untersuchungen zur fremdenverkehrsbedingten Belastung und Veränderung der Vegetation auf der Insel Norderney.	101036	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:14.456	\N	1983	\N	5477	f
5478	Tuerk_1991_101037	\N	\N	\N	\N	\N	Beitrag zur Kenntnis der Vegetationsverhältnisse der Nordfriesischen Insel Amrum. Pflanzengesellschaften der Geest und Marsch.	101037	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:14.524	\N	1991	\N	5479	f
5481	Beinker_1998_101038	\N	\N	\N	\N	\N	Zur Vegetationskunde der Dünen im Listland der Insel Sylt.	101038	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:14.595	\N	1998	\N	5482	f
5484	Eigner_1973_101039	\N	\N	\N	\N	\N	Zur Standorts-, Ausbreitungs- und Keimungsökologie des Meerkohls (Crambe maritima L.)	101039	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:14.665	\N	1973	\N	5485	f
5487	Heykena_1965_101040	\N	\N	\N	\N	\N	Vegetationstypen der Küstendünen an der östlichen und südlichen Nordsee.	101040	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:14.733	\N	1965	\N	5488	f
5491	Wiemann_Domke_1967_101041	\N	\N	\N	\N	\N	Pflanzengesellschaften der ostfriesischen Insel Spiekeroog.	101041	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:14.797	\N	1967	\N	5492	f
5493	Scherfose_1986_101042	\N	\N	\N	\N	\N	Pflanzensoziologische und ökologische Untersuchungen in Salzrasen der Nordseeinsel Spiekeroog. I. Die Pflanzengesellschaften.	101042	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:14.871	\N	1986	\N	5494	f
5495	Neumann_2005_101043	\N	\N	\N	\N	\N	Die Verbreitung von Deschampsia flexuosa in den Grau- und Braundünen der ostfriesischen Insel Spiekeroog	101043	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:14.938	\N	2005	\N	5496	f
5498	Hartlieb_1961_101044	\N	\N	\N	\N	\N	Heide und Krähenbeere auf der Nordseeinsel Spiekeroog.	101044	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:15.003	\N	1961	\N	5499	f
5501	Warnecke_1960_101045	\N	\N	\N	\N	\N	Beobachtungen und Untersuchungen in der Kampfzone der Dünen- und Salzpflanzen auf Spiekeroog.	101045	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:15.074	\N	1960	\N	5502	f
5504	Klement_1953_101046	\N	\N	\N	\N	\N	Die Vegetation der Nordseeinsel Wangerooge	101046	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:15.139	\N	1953	\N	5505	f
5506	Rosskamp_1992_101047	\N	\N	\N	\N	\N	Die Kleinschmielenrasen im Landkreis Friesland und Vorschläge für eine Neugliederung dieser Gesellschaften.	101047	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:15.207	\N	1992	\N	5507	f
5509	Wolbers_2010_101048	\N	\N	\N	\N	\N	Junco-Caricetum extensae auf Spiekeroog. Standortsfaktoren und Kontaktgesellschaften.	101048	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:15.278	\N	2010	\N	5510	f
5473	Lienenbecker_2003_101035	\N	\N	\N	\N	\N	Ein Vorkommen des Hundszahngrases (Cynodon dactylon (L.) Pers.) auf Spiekeroog/Ostfriesland.	101035	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-12-21 10:15:35.036	\N	2003	\N	5474	f
5515	Spalke_2008_101050	\N	\N	\N	\N	\N	Biomasse- und Nährstoffallokation von Dünen- und Salzwiesenpflanzen in Bezug zu ihrer Umwelt	101050	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:15.409	\N	2008	\N	5516	f
5518	Scherfose_1984_101051	\N	\N	\N	\N	\N	Lebensbedingungen, inbesondere Stickstoffversorgung der Salzmarsch-Pflanzengesellschaften auf der Insel Spiekeroog.	101051	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:15.473	\N	1984	\N	5519	f
5520	Scherfose_1984_101052	\N	\N	\N	\N	\N	Die Vegetation des NSG Südstrandpolder auf Norderney.	101052	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:15.541	\N	1984	\N	5521	f
5523	Mueller_2009_101053	\N	\N	\N	\N	\N	Pflanzensoziologische und pedologische Untersuchungen sogenannter Green Beaches auf den Ostfriesischen Inseln - am Beispiel von Juist und Norderney.	101053	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:15.61	\N	2009	\N	5524	f
5526	Hansen_2009_101054	\N	\N	\N	\N	\N	Pflanzensoziologische und pedologogische Untersuchungen an sogenannten Green Beaches auf den Ostfriesischen Inseln; am Beispiel Spiekeroog.	101054	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:15.685	\N	2009	\N	5527	f
5529	Riediger_2009_101055	\N	\N	\N	\N	\N	Pflanzensoziologische und pedologische Untersuchungen von sogenannten Green Beaches auf den Ostfriesischen Inseln - am Beispiel von Borkum und Langeoog.	101055	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:15.751	\N	2009	\N	5530	f
5533	von_Lemm_Wolff_1991_101056	\N	\N	\N	\N	\N	Vegetationskundliche Auswertung der CIR-Luftbilder	101056	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:15.814	\N	1991	\N	\N	f
5535	Runge_1983_101057	\N	\N	\N	\N	\N	Sukzessionsstudien an einigen Pflanzengesellschaften auf Wangerooge II	101057	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:15.881	\N	1983	\N	\N	f
5536	Runge_1976_101058	\N	\N	\N	\N	\N	Sukzessionsstudien an einigen Pflanzengesellschaften Wangerooges.	101058	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:15.948	\N	1976	\N	5537	f
5539	Runge_1987_101060	\N	\N	\N	\N	\N	Dauerquadrat-Beobachtungen an Küsten-Assoziationen	101060	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:16.08	\N	1987	\N	5540	f
5542	Hahn_2001_101061	\N	\N	\N	\N	\N	Die Dünen und Dünentäler im Einflussbereich der Grundwasserbewirtschaftung auf Norderney	101061	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:16.147	\N	2001	\N	5543	f
5545	Fromke_1996_101062	\N	\N	\N	\N	\N	Vergleichende geobotanische Untersuchungen der Ostfriesischen nseln Baltrum und Langeoog im Nationalpark "Niedersächsisches Wattenmeer"	101062	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:16.214	\N	1996	\N	5546	f
5548	Pott_Peters_1996_101063	\N	\N	\N	\N	\N	Umweltverträgliche Raumordnung und Raumnutzung im norddeutschen Küstenraum am Beispiel der Nordseeinsel Norderney	101063	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:16.28	\N	1996	\N	5549	f
5552	Arens_1994_101065	\N	\N	\N	\N	\N	Ökologische Untersuchungen im deichnahen Salzwiesenbereich des westlichen Jadebusens im Zusammenhang mit Deicherhöhungs- und Deichverstärkungsmaßnahmen (Cäciliengroden-Dangast)	101065	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:16.413	\N	1994	\N	5553	f
5555	Grella_2000_101066	\N	\N	\N	\N	\N	Dynamikorientierter Naturschutz am Beispiel aquatischer Sekundärbiotope im Nationalpark Niedersächsisches Wattenmeer	101066	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:16.481	\N	2000	\N	5556	f
5558	Preiss_1991_101067	\N	\N	\N	\N	\N	Die Vegetation des Orchideenstandortes "Vogelweide" und anderer Innengrodenbereiche auf Wangerooge. Aufnahme, synsystematische Bearbeitung, Kartierung und Vorschläge für ein Pflegeprogramm.	101067	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:16.547	\N	1991	\N	5559	f
5564	Loewe_1993_101069	\N	\N	\N	\N	\N	Die Xeroserie der Insel Spiekeroog unter Berücksichtigung der Gebüschformationen.	101069	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:16.714	\N	1993	\N	5565	f
5573	Oltmanns_1996_101072	\N	\N	\N	\N	\N	Untersuchungen zu Vegetationsveränderungen auf der Insel Memmert	101072	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:16.911	\N	1996	\N	5574	f
5575	Hahn_1989_101073	\N	\N	\N	\N	\N	Vegetationskundliche Interpretation von CIR-Luftbildern der Inseln Baltrum	101073	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2016-09-21 15:56:16.981	\N	1989	\N	5576	f
5578	Wolfram_1996_101074	\N	\N	\N	\N	\N	Die Vegetation des Bottsandes.	101074	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:17.046	\N	1996	\N	5579	f
5581	Haerdtle_1984_101075	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen in Salzwiesen der ostholsteinischen Ostseeküste	101075	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:17.118	\N	1984	\N	5582	f
5583	Peters_1996_101076	\N	\N	\N	\N	\N	Vergleichende Vegetationskartierung der Insel Borkum und beispielhafte Erfassung der Veränderung von Landschaft und Vegetation einer Nordseeinsel.	101076	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:17.186	\N	1996	\N	5584	f
5585	Moeller_1975_101077	\N	\N	\N	\N	\N	Soziologisch-ökologische Untersuchungen der Sandküstenvegetation an der Schleswig-Holsteinischen Ostsee.	101077	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:17.252	\N	1975	\N	5586	f
5588	Isermann_2014_101079	\N	\N	\N	\N	\N	Vegetationsökologie der Dünendeiche auf Langeoog und Spiekeroog	101079	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:17.32	\N	2014	\N	5589	f
5591	Simone_1999_101080	\N	\N	\N	\N	\N	Vegetationsentwicklung einer Schutzdüne im Pirolatal auf Langeoog	101080	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:17.386	\N	1999	\N	5592	f
5594	Schwederski_2012_101081	\N	\N	\N	\N	\N	Die Renaturierung der Vegetation feuchter Dünentäler auf Wangerooge.	101081	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:17.453	\N	2012	\N	5595	f
5597	Heemann_2003_101082	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchung der Artenvielflat und Vegetationsstruktur in Sanddorngebüschen im Vergleich zu artenreichen Trockenrasen auf den ostfriesischen Inseln Norderney und Spiekeroog.	101082	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:17.519	\N	2003	\N	5598	f
5599	Hahn_2006_101083	\N	\N	\N	\N	\N	Neophyten der Ostfriesischen Inseln.	101083	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:17.585	\N	2006	\N	5600	f
5601	Isermann_1992_101084	\N	\N	\N	\N	\N	Vergleichende Untersuchungen zur Dünenvegetation auf Spiekeroog.	101084	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:17.651	\N	1992	\N	5602	f
5604	Janowsky_1996_101085	\N	\N	\N	\N	\N	Die natürliche und naturnahe Gehölzvegetation der Tertiärdünenlandschaften von Spiekeroog, Langeoog und Juist im vegetationsgeographischen Vergleich.	101085	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:17.719	\N	1996	\N	5605	f
5607	Weisskamp_2005_101086	\N	\N	\N	\N	\N	Renaturierung des Langeooger Sommerpolders. Auswirkungen auf die Vegetation des Großen und Kleinen Schlopps.	101086	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:17.781	\N	2005	\N	\N	f
5608	Petersen_2013_101087	\N	\N	\N	\N	\N	Biomonitoring 2012 von Dünentalvegetation im Bereich der Trinkwassergewinnung und entsprechender Referenzflächen auf der Insel Borkum.	101087	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:17.847	\N	2013	\N	5609	f
5610	Petersen_2013_101088	\N	\N	\N	\N	\N	Monitoring (2012) der Vorkommen von Liparis loeselii auf Borkum im Rahmen der FFH-Berichtspflicht.	101088	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:17.915	\N	2013	\N	5611	f
5612	Petersen_2003_101089	\N	\N	\N	\N	\N	Monitoring der Vorkommen von Liparis loeselii auf Borkum im Rahmen der FFH-Berichtspflich.	101089	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 17:13:51.588	\N	2003	\N	5613	f
5614	Libbert_1940_101090	\N	\N	\N	\N	\N	Pflanzensoziologische Beobachtungen während einer Reise durch Schleswig-Holstein im Juli 1939.	101090	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:18.057	\N	1940	\N	5615	f
5616	Jeschke_1962_101091	\N	\N	\N	\N	\N	Vegetationskundliche Beobachtungen in Listland (Insel Sylt).	101091	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:18.126	\N	1962	\N	5617	f
5619	Gericke_2000_101092	\N	\N	\N	\N	\N	Beeinflussung der Dünenvegetation durch Kaninchen auf der Insel Norderney.	101092	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:18.194	\N	2000	\N	5620	f
5622	Rexin_1990_101093	\N	\N	\N	\N	\N	Die Entwicklung der Phosphor-Vorräte in einer Dünensukzession auf Spiekeroog.	101093	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:18.257	\N	1990	\N	5623	f
5625	Eis_1990_101094	\N	\N	\N	\N	\N	Bodenentwicklung in einer Dünensukzession.	101094	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:18.323	\N	1990	\N	5626	f
5628	Munderloh_1992_101095	\N	\N	\N	\N	\N	Synusien der Bodenmesofauna in Dünen unterschiedlichen Sukzessionsalters der Insel Spiekeroog (Ostfriesland)	101095	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:18.387	\N	1992	\N	\N	f
5630	Albers_1989_101096	\N	\N	\N	\N	\N	Die Stickstoff-Nettomineralisation in Küstendünen auf Spiekeroog.	101096	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2016-09-21 15:56:18.452	\N	1989	\N	5631	f
5633	Schwabe-Kratochwil_2001_101097	\N	\N	\N	\N	\N	Vegetationsökologie Borkum.	101097	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:18.517	\N	2001	\N	5634	f
5635	Schwabe-Kratochwil_2004_101098	\N	\N	\N	\N	\N	Vegetationsökologie Borkum.	101098	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:18.58	\N	2004	\N	5636	f
5637	Schwabe-Kratochwil_2006_101099	\N	\N	\N	\N	\N	Vegetationsökologie Borkum.	101099	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:18.645	\N	2006	\N	5638	f
5639	Schwabe-Kratochwil_2008_101100	\N	\N	\N	\N	\N	Vegetationsökologie Borkum.	101100	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:18.715	\N	2008	\N	5640	f
5641	Schwabe-Kratochwil_2010_101101	\N	\N	\N	\N	\N	Vegetationsökologie Borkum.	101101	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:18.78	\N	2010	\N	5642	f
5643	Schwabe-Kratochwil_2012_101102	\N	\N	\N	\N	\N	Vegetationsökologie Borkum.	101102	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:18.86	\N	2012	\N	5644	f
5645	Isermann_2004_101103	\N	\N	\N	\N	\N	Wacholder auf den Ostfriesischen Inseln.	101103	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:18.973	\N	2004	\N	5646	f
5647	Christiansen_1937_101104	\N	\N	\N	\N	\N	Beobachtungen auf der Lotseninsel Schleimünde.	101104	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:19.036	\N	1937	\N	\N	f
5648	Christiansen_1960_101105	\N	\N	\N	\N	\N	Vegetationsstudien auf Helgoland	101105	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:19.106	\N	1960	\N	5649	f
5653	Bernhardt_1989_101107	\N	\N	\N	\N	\N	Pflanzensoziologische Exkursion ins Emsland und Emsmündung vom 17.07.-23.07.1989	101107	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:19.24	\N	1989	\N	5654	f
5655	Bernhardt_1996_101108	\N	\N	\N	\N	\N	Exkursionsprotokoll: Ems- und Friesland. SoSe 1996. 15.07.-24.07.1996	101108	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:19.306	\N	1996	\N	5656	f
5658	Ritz_1988_101109	\N	\N	\N	\N	\N	Die Vegetationseinheiten der Außendeichswiesen von Neuwerk.	101109	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:19.376	\N	1988	\N	5659	f
5661	Ikemeyer_1986_101110	\N	\N	\N	\N	\N	Die Dünenvegetation der Insel Wangerooge.	101110	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:19.44	\N	1986	\N	5662	f
5664	Petersen._J._2003_101111	\N	\N	\N	\N	\N	Erfassung der Dauerbeobachtungsflächen, Erstellung eines Pflegekonzeptes und Ausweisung der Mahdbereiche im Bereich Dreebargen auf Langeoog.	101111	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:19.505	\N	2003	\N	5665	f
5667	Meijer_1955_101112	\N	\N	\N	\N	\N	Kortenhoef. Een veldbiologische studie van een Hollands verlandingsgebied	101112	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2016-09-21 15:56:19.592	\N	1955	\N	5668	f
5670	Ninot_2000_101113	\N	\N	\N	\N	\N	Syntaxonomic conspectus of the vegetation of Catalonia and Andorra. I: Hygrophilous herbaceous communities	101113	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:19.656	\N	2000	\N	5671	f
5673	Hommel_1994_101114	\N	\N	\N	\N	\N	Excursieverslagen 1992. Plantensociologische Kring Nederland	101114	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2016-09-21 15:56:19.72	\N	1994	\N	5674	f
5676	Vroman_1972_101115	\N	\N	\N	\N	\N	Taraxacum limnanthes Haglund supsp. limnanthoides Van Soestop Schiermonnikoog	101115	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:19.784	\N	1972	\N	5677	f
5679	Meltzer_1947_101116	\N	\N	\N	\N	\N	De binnenpolder bij Terheijden (N.B.)	101116	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	book	2016-09-21 15:56:19.862	\N	1947	\N	5680	f
5682	Hommes_1972_101117	\N	\N	\N	\N	\N	Taraxacum europhyllum (Dahlst.) Christ. op Schiermonnikoog	101117	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:19.929	\N	1972	\N	5683	f
2730	Braun_1975_001003	\N	\N	\N	\N	\N	Die Pflanzendecke. In: Bodenkarte von Bayern 1:25.000. Erläuterungen zum Kartenblatt Nr. 6938 Regensburg	001003	München	\N	\N	\N	Booktitle	\N	\N	\N	30	\N	N	In: Bodenkarte von Bayern 1:25.000. Erläuterungen zum Kartenblatt Nr. 6938 Regensburg: 66-81. München		\N		\N	book	2016-09-22 11:33:49.727	\N	1975	\N	2731	f
2726	Braun_1972_001001	\N	\N	\N	\N	\N	Die Pflanzendecke. In: Bodenkarte von Bayern 1:25.000. Erläuterungen zum Blatt Nr. 5737 Schwarzenbach a. d. sächsischen Saale	001001	München	\N	\N	\N	Booktitle	\N	\N	\N	9	\N	N	In: Bodenkarte von Bayern 1:25.000. Erläuterungen zum Blatt Nr. 5737 Schwarzenbach a. d. sächsischen Saale: 51-71. München		\N		\N	book	2016-09-22 11:33:58.217	\N	1972	\N	2727	f
2133	Braun_1969_000776	\N	\N	\N	\N	\N	Die Pflanzendecke. In: Erläuterungen zur Bodenkarte von Bayern 1:25000. Blatt Nr. 6640 Neuenburg vorm Wald	000776		\N	\N	\N	Booktitle	\N	\N	\N	20	\N	Y	\N	Publisher	\N		\N	book	2016-09-22 11:35:00.812	\N	1969	\N	2134	f
3664	Froede_1957_100061	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der Insel Hiddensee	100061		\N	\N	\N		DUBLETTE000162	\N	\N	237	237	Y			000162		\N	article	2016-09-22 11:37:31.428	\N	1957	\N	3665	f
1323	Koehler_Schubert_1964_000479	\N	\N	\N	\N	\N	Die Pflanzengesellschaften im Einzugsgebiet der Luhne im Bereich des oberen Unstruttales	000479	Halle (Saale)	\N	KSU	\N		TABGESNE	\N	\N	7	\N	Y			\N		\N	article	2016-09-22 11:38:57.83	\N	1964	\N	1324	f
1332	Krausch_1968_000483	\N	\N	\N	\N	\N	Die Sandtrockenrasen (Sedo-Scleranthetea) in Brandenburg	000483		\N	KRA	\N		TABGESNE	\N	\N	93	3	Y			\N		\N	article	2016-09-22 11:39:31.012	\N	1968	\N	1333	f
3369	Libbert_1938_001224	\N	\N	\N	\N	\N	Flora und Vegetation des Neumärkischen Plönetales	001224		\N	\N	\N		Libbert_1938	\N	\N	87	\N	Y			\N		\N	article	2016-09-22 11:39:47.523	\N	1938	\N	3370	f
4000	Passarge_1960_100208	\N	\N	\N	\N	\N	Waldgesellschaften NW-Mecklenburgs	100208	Berlin	\N	\N	\N		DUBLETTE000871	\N	\N	0	70	Y			000871		\N	article	2016-09-22 11:40:59.305	\N	1960	\N	4001	f
5684	Jandt_Bruelheide_2012_null	\N	10.7809/b-e.00146	EU-DE-014	\N	\N	German Vegetation Reference Database (GVRD)	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Y	\N	\N	\N	\N	\N	article	2016-10-04 16:25:04.944	http://www.biodiversity-plants.de/biodivers_ecol/article_meta.php?DOI=10.7809/b-e.00146	2012	\N	5685	f
3270	Becker_Schmiege_Bergmeier_E._Dengler_Nowak_2012_001194	\N	\N	\N	\N	\N	Nutrient-poor grasslands on siliceous soil in the lower Aar valley (Middle Hesse, Germany) - neglected vegetation types in the intersection range of four classes	001194	Göttingen	\N	\N	\N		Becker_2012_Tuexenia_neu (müssteT3214_Becker heißen)	TUEXENIA	\N	104	\N	Y			\N		\N	article	2017-01-23 16:33:31.551	\N	2012	\N	3271	f
5268	Jansen_Dengler_Berg_2012_null	\N	http://doi.org/10.7809/b-e.00070	\N	\N	\N	VegMV - the vegetation database of Mecklenburg-Vorpommern	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-12-20 20:39:22.465	\N	2012	\N	5269	f
4716	Wollert_1998_100471	\N	\N	\N	\N	\N	Zur Zusammensetzung des Urtico-Cruciatietum laevipis Dierschke 73 im mittleren Mecklenburg	100471	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	23	23	Y			\N		\N	article	2017-01-25 10:52:56.347	\N	1998	\N	4717	f
5693	Wagner_Wagner_1996_001339	\N	\N	\N	\N	\N	Pfrunger-Burgweiler Ried. Pflege- und Entwicklungsplan ; ökologische Grundlagen und Konzeption zum Schutz einer oberschwäbischen Moorlandschaft.	001339	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Y	\N	\N	\N	\N	\N	article	2016-12-22 11:49:14.158	\N	1996	\N	5694	f
5696	Kiesel_1980_001338	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen im Raum Harzgerode un der Umgebung von Greifenhagen.	001338	\N	\N	\N	\N	\N	\N	\N	\N	140	\N	N	\N	\N	\N	Diplomarbeit. Martin-Luther-Universität Halle-Wittenberg. 71 S.	\N	thesis	2016-12-22 11:56:03.38	\N	1980	\N	5697	f
5701	Tippmann_2001_001326	\N	\N	\N	\N	\N	Die Vegetation des Naturschutzgebiets Alte See Grethen	001326	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Y	\N	\N	\N	\N	\N	article	2016-12-23 12:10:30.345	\N	2001	\N	5702	f
5705	Mayer_Noritzsch_2001_001325	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen im NSG "Trebnitzgrund" - Sächsische Schweiz - MTB 5148	001325	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Y	\N	\N	\N	\N	\N	article	2016-12-23 12:13:12.499	\N	2001	\N	5706	f
5709	Warthemann_Bischoff_Winter_2009_001321	\N	\N	\N	\N	\N	Renaturierung von Brenndolden-Auenwiesen durch Mahdgut-Übertragung in der Elbeaue bei Dessau.	001321	\N	\N	\N	\N	\N	\N	\N	\N	4	\N	Y	\N	\N	\N	\N	\N	article	2016-12-23 12:17:09.344	\N	2009	\N	5710	f
5711	Lienenbecker_null_null	\N	\N	\N	\N	\N	Die Moorlilie (Narthecium ossifragum) im Teutoburger Wald bei Riesenbeck/Kreis Steinfurt	001323	\N	\N	\N	\N	\N	\N	\N	\N	7	\N	Y	\N	\N	\N	\N	\N	article	2016-12-23 12:23:42.688	\N	1986	\N	5713	f
5714	Dierschke_1986_001324	\N	\N	\N	\N	\N	Untersuchungen zur Populationsdynamik der Gentianella-Arten in einem Enzian - Zwenken - Kalkmagerrasen	001324	\N	\N	\N	\N	\N	\N	\N	\N	1	\N	Y	\N	\N	\N	\N	\N	article	2016-12-23 12:26:02.966	\N	1986	\N	5715	f
1180	Dierschke_null_000426	\N	\N	\N	\N	\N	Saumgesellschaften im Vegetations- und Standortsgefälle an Waldrändern	000426	Göttingen	\N	DIE	\N		TABGESNE	\N	\N	17	\N	Y			\N		\N	article	2016-12-23 12:27:30.818	\N	1974	\N	1181	f
5717	Guergens_1966_001322	\N	\N	\N	\N	\N	Zur Verbreitung, Ökologie und Vergesellschaftung der Botrychium-Arten im Voigtland	001322	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Y	\N	\N	\N	\N	\N	article	2016-12-23 12:31:34.329	\N	1966	\N	5718	f
5726	Dengler_Bedall_Bruchmann_Hoeft_Lang_2004_200003	\N	\N	\N	\N	\N	Artenzahl-Areal-Beziehungen in uckermärkischen Trockenrasen unter Berücksichtigung von Kleinstflächen – eine neue Methode und erste Ergebnisse	200003	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-01-25 17:22:08.044	\N	2004	\N	5727	f
4910	Dengler_1998_100558	\N	\N	\N	\N	\N	Der Geschützte Landschaftsbestandteil ?Mühlenberg? bei Brodowin ? Flora, Vegetation und Bedeutung für den Naturschutz	100558		\N	\N	\N		\N	\N	\N	19	19	Y			\N		\N	article	2017-01-23 16:30:25.598	\N	1998	\N	4911	f
5286	Hopp_Dengler_2015_null	\N	\N	\N	\N	\N	Scale-dependent species diversity in a semi-dry basiphilous grassland (Bromion erecti) of Upper Franconia (Germany)	001282	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-01-23 16:33:46.024	\N	2015	\N	5287	f
1879	Dengler_Eisenberg_Schroeder_2006_000684	\N	\N	\N	\N	\N	Die grundwasserfernen Saumgesellschaften Nordostniedersachsens im europäischen Kontext - Teil I: Säume magerer Standorte (Trifolio-Geranietea sanguinei)	000684	Göttingen	\N	\N	\N		vegetweb	\N	\N	246	\N	Y			\N		\N	article	2017-01-23 16:31:09.288	\N	2006	\N	1880	f
26	Drews_Dengler_2004_000008	\N	\N	\N	\N	\N	Steilufer an der nordoldenburgischen Küste: Artenausstattung, Vegetation und Pflegekonzept unter besonderer Berücksichtigung der Kalkhabltrockenrasen und der wärmeliebenden Säume	000008		\N	\N	\N		\N	\N	\N	21	\N	Y			\N		\N	article	2017-01-23 16:34:00.08	\N	2004	\N	27	f
4856	Dengler_Lemke_Wollert_2000_100529	\N	\N	\N	\N	\N	Zwei Stipa-Arten im Uecker-Randow-Gebiet (M-V) wieder entdeckt - Mit Anmerkungen zum Stipa pennata-Aggregat in Nordostdeutschland	100529	Berlin	\N	\N	\N		\N	\N	\N	2	2	Y			\N		\N	article	2017-01-23 16:34:50.986	\N	2000	\N	4857	f
4739	Dengler_1987_100481	\N	\N	\N	\N	\N	Gedanken zur synsystematischen Arbeitsweise und zur Gliederung der Ruderalgesellschaften (Artemisietea vulgaris s.l.). Mit der Beschreibung des Elymo-Rubetum caesii ass. nova	100481	Göttingen	\N	\N	\N		\N	\N	\N	4	4	Y			\N		\N	article	2017-01-23 16:32:33.279	\N	1987	\N	4740	f
1098	Becker_1996_000397	\N	\N	\N	\N	\N	Flora und Vegetation von Felsfluren und Magerrasen im unteren Unstruttal (Sachsen-Anhalt)	000397		\N	BEK	\N		TABGESNE	\N	\N	207	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen	\N	thesis	2017-01-25 10:15:30.601	\N	1996	\N	1099	f
4718	Kintzel_1998_100472	\N	\N	\N	\N	\N	Zur Ruderalvegetation in einigen Dörfern des Altkreises Lübz	100472	Mecklenburg-Vorpommern	\N	\N	\N		\N	\N	\N	242	242	Y			\N		\N	article	2017-01-25 10:51:44.657	\N	1998	\N	4719	f
5230	Leyer_Wesche_2007_100705	\N	\N	\N	\N	\N	Multivariate Statistik in der Ökologie	100705	\N	\N	\N	\N	\N	\N	\N	\N	0	\N	\N	Berlin, Springer, 221.	\N	\N	\N	\N	book	2017-02-23 15:02:11.353	\N	2007	\N	\N	f
5188	Tanghe_1970_100674	\N	\N	\N	\N	\N	La végetation forestiére de la vallée de la semois ardennaise. 3. Partie: Les associations forestières stationelles de plateau et de plaine	100674		\N	TAN	unpubliziert		Heinken2	\N	\N	38	\N	N			\N		\N	unpublished	2017-03-15 15:49:35.073	\N	1970	\N	5189	f
5719	Dengler_2014_null	\N	\N	\N	\N	\N	Vegetationsaufnahmen des Geoökologie-Praktikums "Standortkundliche Feldmethoden" 2014, Neubürg bei Bayreuth	200001	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Universität Bayreuth	\N	report	2017-04-09 16:58:12.296	\N	2014	\N	5745	f
5185	Sougnez_1974_100673	\N	\N	\N	\N	\N	Les chenaies silicoles de Belgique (Quercion robori-petraeae (Malc. 1929) Br. –Bl. 1932)	100673		\N	SOU	unpubliziert		Heinken2	\N	\N	814	\N	N			\N		\N	unpublished	2017-03-15 15:51:56.054	\N	1975	\N	5186	f
5744	WWK-Umweltplanung,_Warendorf_2003_001344	\N	\N	\N	\N	\N	Vegetationsaufnahmen an der Aller	001344	\N	\N	\N	Custos Peter Horchler, BfG Koblenz	\N	Aller_2003	\N	BfG Koblenz	\N	\N	N	\N	\N	\N	\N	\N	thesis	2017-04-05 15:44:15.7	\N	2003	\N	\N	f
5753	Conradi_Temperton_Kollmann_2017_200011	\N	\N	\N	\N	\N	Beta diversity of plant species in human-transformed landscapes: Control of community assembly by regional productivity and historical connectivity	200011	\N	\N	\N	\N		\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-04-09 17:09:14.908	\N	2017	\N	5754	f
5752	Conradi_null_null	\N	\N	\N	\N	\N	Vegetationsaufnahmen von Trockenrasen der südbayerischen Schotterebenen	200013	\N	\N	\N	\N	Unpublished	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2017-04-09 17:10:35.923	\N	2013	\N	\N	f
5746	Dengler_2001_null	\N	\N	\N	\N	\N	Vegetationsaufnahmen der krautigen Xerothermvegetation Nordost-Deutschlands (Brandenburg, Berlin, Mecklenburg-Vorpommern) [Diplomarbeit + Dissertation]	200012	\N	\N	\N	\N	Unpublished	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	thesis	2017-04-09 17:11:22.537	\N	2001	\N	5747	f
5756	Dengler_Eisenberg_Kraft_Loebel_2002_200008	\N	\N	\N	\N	\N	Die Ilmenauniederung „Düvelsbrook“ – Standort, Vegetation und Naturschutz eines Feuchtwiesengebietes bei Lüneburg	200008	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2017-04-09 17:15:31.727	\N	2002	\N	5757	f
9	Stemmler_1994_000003	\N	\N	\N	\N	\N	Vergleichende phänologische Untersuchungen in Grünlandgesellschaften verschiedener Höhenstufen des westlichen Harzes	000003		\N	\N	\N		\N	\N	\N	28	\N	Y			\N	Dipl.-Arb. Universität Göttingen	\N	thesis	2016-05-04 11:04:28.619	\N	1994	\N	\N	f
41	Meier-Schomburg_1993_000013	\N	\N	\N	\N	\N	Laubwald-Geselschaften im Bereich des Höheberges (Nordwest-Thüringen)	000013		\N	\N	\N		\N	\N	\N	137	\N	Y			\N	Dipl.-Arb. Universität Göttingen	\N	thesis	2016-05-04 11:04:31.93	\N	1993	\N	\N	f
44	Becker_1992_000014	\N	\N	\N	\N	\N	Gutachten NSG "Mittelberg" bei Geismar	000014		\N	\N	unpubliziert		TABGESNE	\N	\N	12	\N	Y	Unveröff		\N		\N	unpublished	2016-05-04 11:04:32.166	\N	1992	\N	\N	f
47	Baufeld_1991_000015	\N	\N	\N	\N	\N	Laubwald-Gesellschaften im unteren Werraland	000015		\N	\N	\N		\N	\N	\N	155	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen	\N	thesis	2016-05-04 11:04:32.435	\N	1991	\N	\N	f
53	Hundt_1998_000017	\N	\N	\N	\N	\N	Vegetationskundliche Modelluntersuchung am Grünland der Vorderen Rhön als Grundlage für eine umweltgerechte Nutzung und deren ökologisch fundierte Förderung	000017		\N	\N	\N		\N	\N	\N	159	\N	Y	Mitteilungen aus dem Biosphärenreservat Rhön/Thüringen	Mitteilungen aus dem Biosphärenreservat Rhön/Thüringen	\N		\N	book	2016-05-04 11:04:32.932	\N	1998	\N	\N	f
60	Kuhn_1937_000019	\N	\N	\N	\N	\N	Die Pflanzengesellschaften im Neckargebiet der Schwäbischen Alb	000019		\N	\N	\N		\N	\N	\N	300	\N	Y	Württ. Landesstelle Naturschutz u. Verein f. vaterländische Naturk. Württ., 340 S	Württ. Landesstelle Naturschutz u. Verein f. vaterländische Naturk. Württ., 340 S	\N		\N	book	2016-05-04 11:04:33.529	\N	1937	\N	\N	f
74	Becker_Heinken_Nacke_Schmidt_1993_000023	\N	\N	\N	\N	\N	Schutzwürdigkeitsgutachten Kalkmagerrasen bei Roßbach, Werra-Meißner-Kreis	000023		\N	\N	\N		TABGESNE	\N	\N	0	\N	Y			\N		\N	report	2016-05-04 11:04:34.533	\N	1993	75	\N	f
102	Seifert_1992_000033	\N	\N	\N	\N	\N	Tagfalterzönosen und Phänologie der Grünlandvegetation im Meißnerbergland	000033		\N	\N	\N		THOMAS	\N	\N	38	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 162 S. (s. auch Seifert 1994	\N	thesis	2016-05-04 11:04:37.171	\N	1992	\N	\N	f
126	Vogel_1977_000041	\N	\N	\N	\N	\N	Die Wiesengesellschaften des Westharzes	000041		\N	\N	\N		LITTAB	\N	\N	149	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen	\N	thesis	2016-05-04 11:04:40.548	\N	1977	\N	\N	f
153	Heidenreich_1997_000050	\N	\N	\N	\N	\N	Der Einfluß von Pflegemaßnahmen auf Bergwiesen in der Umgebung von Sankt Andreasberg (Harz)	000050		\N	HRV	\N		TAB	\N	\N	214	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 91 S	\N	thesis	2016-05-04 11:04:42.639	\N	1997	\N	\N	f
144	Knuever_1993_000047	\N	\N	\N	\N	\N	Vegetation und Flora dreier Bachtäler im Kaufunger Wald	000047		\N	\N	\N		LITTAB	\N	\N	60	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 149 S	\N	thesis	2016-05-04 11:04:41.953	\N	1993	\N	\N	f
264	Hoegel_1991_000087	\N	\N	\N	\N	\N	Vegetationskundliche Analyse und Kartierung im Mansfelder Seengebiet	000087		\N	HGL	\N		TABGESNE	TV	\N	525	\N	Y			\N	Diss. Universität Halle. 135 S	\N	thesis	2016-05-04 11:04:51.933	\N	1991	\N	\N	f
244	Knapp_1944_000080	\N	\N	\N	\N	\N	Vegetationsaufnahmen von Wäldern des Unter-Harzes	000080		\N	\N	unpubliziert		\N	\N	\N	34	\N	Y	Mskr. Halle (Saale). 13 S		\N		\N	unpublished	2016-05-04 11:04:50.252	\N	1944	\N	\N	f
253	Trautmann_1954_000083	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte des Forstamtes Lauterberg (Harz)	000083		\N	\N	unpubliziert		\N	\N	\N	26	\N	Y	Mskr. Stolzenau		\N		\N	unpublished	2016-05-04 11:04:50.966	\N	1954	\N	\N	f
255	Sauer_1948_000084	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des braunschweigischen Forstamtsbezirks Seesen II	000084		\N	\N	unpubliziert		\N	\N	\N	126	\N	Y	Mskr		\N		\N	unpublished	2016-05-04 11:04:51.187	\N	1948	\N	\N	f
262	Hartmann_Jahn_1967_000086	\N	\N	\N	\N	\N	Waldgesellschaften des mitteleuropäischen Gebirgsraumes nördlich der Alpen	000086		\N	HJ	\N		BOHN	TV	\N	999	\N	Y	1. Aufl., 636 S. Stuttgart	1. Aufl., 636 S. Stuttgart	\N		\N	book	2016-05-04 11:04:51.669	\N	1967	\N	\N	f
270	Knapp_1978_000089	\N	\N	\N	\N	\N	Geobotanische Studien an grundwasserfernen Waldgrenzstandorten des hercynischen Florengebietes	000089		\N	KNH	\N		TABGESNE	TV	\N	107	\N	Y			\N	Diss. Universität Halle	\N	thesis	2016-05-04 11:04:52.424	\N	1978	\N	\N	f
273	Krauss_1962_000090	\N	\N	\N	\N	\N	Pflanzengesellschaften der Äcker, Wiesen und des Waldes um Harzgerode	000090		\N	\N	\N		\N	\N	\N	38	\N	Y			\N	Dipl.-Arb. Universität Halle	\N	thesis	2016-05-04 11:04:52.693	\N	1962	\N	\N	f
279	Peppler_1984_000092	\N	\N	\N	\N	\N	Die Vegetation von Sieber- und Lonautal im Harz	000092		\N	\N	\N		\N	\N	\N	191	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 193 S	\N	thesis	2016-05-04 11:04:53.172	\N	1984	\N	\N	f
282	Pieper_1996_000093	\N	\N	\N	\N	\N	Laubwaldgesellschaften im mittleren Bodetal zwischen Wendefurth und Thale (Mittelharz)	000093		\N	\N	\N		\N	\N	\N	181	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 118 S	\N	thesis	2016-05-04 11:04:53.423	\N	1996	\N	\N	f
295	Stoecker_1960_000097	\N	\N	\N	\N	\N	Vorarbeit zu einer Vegetationsmonographie des Naturschutzgebietes "Bodetal"	000097		\N	\N	\N		\N	\N	\N	68	\N	Y			\N	Dipl.-Arb. Universität Halle	\N	thesis	2016-05-04 11:04:54.447	\N	1960	\N	\N	f
287	Wendlandt_1995_000095	\N	\N	\N	\N	\N	Waldvegetation des Iberges bei Bad Grund (Harz)	000095		\N	\N	\N		\N	\N	\N	60	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 104 S	\N	thesis	2016-05-04 11:04:53.926	\N	1995	\N	\N	f
297	Knapp_1988_000098	\N	\N	\N	\N	\N	Das Naturschutzgebiet "Alter Stolberg" am Südharz. Geobotanische Studie	000098		\N	KNH	unpubliziert		\N	\N	\N	122	\N	Y	Unveröff. Mskr. Waren/Müritz, 23 S		\N		\N	unpublished	2016-05-04 11:04:54.666	\N	1988	\N	\N	f
309	Schmidt_1958_000102	\N	\N	\N	\N	\N	Die Waldgesellschaften des Wipper- und Eine-Gebietes im Bereich der Ostabdachung des Harzes	000102		\N	\N	\N		\N	\N	\N	1	\N	Y			\N	Dipl.-Arb. Universität Halle	\N	thesis	2016-05-04 11:04:55.725	\N	1958	\N	\N	f
239	Buerger-arndt_1994_000078	\N	\N	\N	\N	\N	Zur Bedeutung von Stickstoffeinträgen für naturnahe Vegetationseinheiten in Mitteleuropa	000078		\N	\N	\N		\N	\N	\N	0	\N	Y	Diss. Bot. 220: 1-226. Berlin, Stuttgart	Diss. Bot. 220: 1-226. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:04:49.709	\N	1994	\N	\N	f
315	Thomas_1990_000104	\N	\N	\N	\N	\N	Grünlandgesellschaften und Grünlandbrachen in der nordbadischen Rheinaue	000104		\N	\N	\N		\N	\N	\N	708	\N	Y	Diss. Bot. 162: 257 S. Berlin, Stuttgart	Diss. Bot. 162: 257 S. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:04:56.211	\N	1990	\N	\N	f
318	Borck_1999_000105	\N	\N	\N	\N	\N	?	000105		\N	\N	\N		\N	\N	\N	115	\N	Y			\N	Dipl.-Arb. Saarland	\N	thesis	2016-05-04 11:04:56.433	\N	1999	\N	\N	f
321	Meyer_1998_000106	\N	\N	\N	\N	\N	?	000106		\N	\N	\N		\N	\N	\N	176	\N	Y			\N	Dipl.-Arb. Saarland	\N	thesis	2016-05-04 11:04:56.691	\N	1998	\N	\N	f
326	Kunzmann_1989_000108	\N	\N	\N	\N	\N	Der ökologische Feuchtegrad als Kriterium zur Beurteilung von Grünlandstandorten	000108		\N	\N	\N		\N	\N	\N	377	\N	Y	Diss. Bot. 134: 1-254. Berlin, Stuttgart	Diss. Bot. 134: 1-254. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:04:57.148	\N	1989	\N	\N	f
329	Beer_1995_000109	\N	\N	\N	\N	\N	Methodische und standortökologische Untersuchungen zum Nährstoffumsatz im Grünland	000109		\N	\N	\N		\N	\N	\N	8	\N	Y	Diss. Bot. 242: 1-210. Berlin, Stuttgart	Diss. Bot. 242: 1-210. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:04:57.379	\N	1995	\N	\N	f
332	Wattendorff_1958_000110	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte der weiteren Schutzzone des Wasserwerks Kusenhorst	000110		\N	\N	unpubliziert		\N	\N	\N	128	\N	Y	Unveröff. Mskr. Bot. Institut Universität Münster, 32.S		\N		\N	unpublished	2016-05-04 11:04:57.866	\N	1958	\N	\N	f
354	Baufeld_2005_000117	\N	\N	\N	\N	\N	GIS-gestützte Prognose der Biotopentwicklung auf Grundlage von Biotoptypen- und Vegetationserhebungen auf geplanten Rückdeichungsflächen an der Mittleren Elbe in Sachsen-Anhalt	000117		\N	\N	\N		\N	\N	\N	207	\N	Y			\N	Diss. Universität Potsdam. 149 S. + Anh	\N	thesis	2016-05-04 11:04:59.542	\N	2005	\N	\N	f
375	Amani_1980_000125	\N	\N	\N	\N	\N	Vegetationskundliche und ökologische Untersuchungen im Grünland der Bachtäler um Suderburg	000125		\N	\N	\N		\N	\N	\N	129	\N	Y			\N	Diss. Universität Göttingen. 116 S	\N	thesis	2016-05-04 11:05:01.413	\N	1980	\N	\N	f
394	Bock_1983_000131	\N	\N	\N	\N	\N	?	000131		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y			\N		\N	unpublished	2016-05-04 11:05:02.843	\N	1983	\N	\N	f
397	Borstel_1974_000132	\N	\N	\N	\N	\N	Untersuchungen zur Vegetationsentwicklung auf ökologisch verschiedenen Grünland- und Ackerbrachen hessischer Mittelgebirge (Westerwald, Rhön, Vogelsberg)	000132		\N	\N	\N		\N	\N	\N	119	\N	Y			\N	Diss. Universität Gießen. 159 S	\N	thesis	2016-05-04 11:05:03.103	\N	1974	\N	\N	f
517	Harm_1988_000177	\N	\N	\N	\N	\N	Feuchtgrünland-Gesellschaften des Südwestharzes	000177		\N	\N	\N		\N	\N	\N	158	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 121 S	\N	thesis	2016-05-04 11:05:14.107	\N	1988	\N	\N	f
522	Hauser_1988_000179	\N	\N	\N	\N	\N	Pflanzengesellschaften der mehrschürigen Wiesen (Molinio-Arrhenatheretea) Nordbayerns	000179		\N	\N	\N		\N	\N	\N	885	\N	Y	Diss. Bot. 128: 1-156. Berlin, Stuttgart	Diss. Bot. 128: 1-156. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:05:14.577	\N	1988	\N	\N	f
535	Hofmeister_1970_000184	\N	\N	\N	\N	\N	Pflanzengesellschaften der Weserniederung oberhalb Bremens	000184		\N	\N	\N		\N	\N	\N	260	\N	Y	Diss. Bot. 10: 1-116. Lehre	Diss. Bot. 10: 1-116. Lehre	\N		\N	book	2016-05-04 11:05:15.742	\N	1970	\N	\N	f
565	Ilschner_1959_000196	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Wurzacher Riedes	000196		\N	\N	\N		\N	\N	\N	67	\N	Y			\N	Diss. Universität Tübingen 1-207	\N	thesis	2016-05-04 11:05:18.627	\N	1959	\N	\N	f
576	Klapp_1951_000200	\N	\N	\N	\N	\N	Pflanzengesellschaften des Wirtschaftsgrünlandes	000200		\N	\N	unpubliziert		\N	\N	\N	64	\N	Y	Mskr. Braunschweig-Völkenrode. Unveröff		\N		\N	unpublished	2016-05-04 11:05:19.614	\N	1951	\N	\N	f
594	Knapp_1946_000205	\N	\N	\N	\N	\N	Die Wiesen- und Weide-Gesellschaften der Umgebung von Halle (Saale) und ihre landwirtschaftliche Bedeutung. 1: Die verbreiteten und wirtschaftlich wichtigsten Wiesen- und Weidengesellschaften	000205		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y	Mskr. vervielf. ?		\N		\N	unpublished	2016-05-04 11:05:20.785	\N	1946	\N	\N	f
596	Knapp_1946_000206	\N	\N	\N	\N	\N	Über Wiesen der nordöstlichen Oberrhein-Ebene und ihre wirtschaftliche Bedeutung	000206		\N	\N	unpubliziert		\N	\N	\N	21	\N	Y	Mskr. vervielf. Erbach. 32 S		\N		\N	unpublished	2016-05-04 11:05:21.055	\N	1946	\N	\N	f
598	Knapp_1946_000207	\N	\N	\N	\N	\N	Über Pflanzengesellschaften der Wiesen und Weiden im Odenwald	000207		\N	\N	unpubliziert		\N	\N	\N	11	\N	Y	Mskr. vervielf. Erbach. 29 S		\N		\N	unpublished	2016-05-04 11:05:21.28	\N	1946	\N	\N	f
600	Knapp_1946_000208	\N	\N	\N	\N	\N	Zur Systematik einiger Pflanzengesellschaften der Moore und Ufer	000208		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y	Mskr. vervielf. Heidelberg		\N		\N	unpublished	2016-05-04 11:05:21.541	\N	1946	\N	\N	f
604	Knapp_1954_000210	\N	\N	\N	\N	\N	Über Pflanzengesellschaften der Wiesen in Trockengebieten Deutschlands	000210		\N	\N	\N		\N	\N	\N	0	\N	Y	Festschrift f. E. Aichinger 2: 1145-1186. Wien	Festschrift f. E. Aichinger 2: 1145-1186. Wien	\N		\N	book	2016-05-04 11:05:22.034	\N	1954	\N	\N	f
652	Kuhn_1954_000229	\N	\N	\N	\N	\N	Die Verlandungsgesellschaften des Federseerieds bei Buchau in Oberschwaben	000229		\N	\N	\N		\N	\N	\N	95	\N	Y			\N	Diss. Universität Tübingen	\N	thesis	2016-05-04 11:05:26.69	\N	1954	\N	\N	f
685	Meisel_1960_000242	\N	\N	\N	\N	\N	Die Auswirkung der Grundwasserabsenkung auf die Pflanzengesellschaften im Gebiete um Moers (Niederrhein)	000242		\N	\N	\N		\N	\N	\N	0	\N	Y	Stolzenau	Stolzenau	\N		\N	book	2016-05-04 11:05:29.714	\N	1960	\N	\N	f
748	Pardey_1992_000267	\N	\N	\N	\N	\N	Vegetationsentwicklung kleinflächiger Sekundärgewässer. Untersuchungen zur Flora, Vegetation und Sukzession von Kleingewässerneuanlagen unter Berücksichtigung der Standortsverhältnisse in Norddeutschl	000267		\N	\N	\N		THOMAS	\N	\N	14	\N	Y	Diss. Bot. 195: 155 S	Diss. Bot. 195: 155 S	\N		\N	book	2016-05-04 11:05:35.753	\N	1992	\N	\N	f
771	Dorda_1995_000277	\N	\N	\N	\N	\N	Heuschreckenzönosen als Bioindikatoren auf Sand- und submediterranen Kalk-Magerrasen des saarländisch-lothringischen Schichtstufenlandes	000277		\N	\N	\N		\N	\N	\N	31	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:05:38.279	\N	1995	\N	\N	f
774	Pfadenhauer_1969_000278	\N	\N	\N	\N	\N	Edellaubholzreiche Wälder im Jungmoränengebiet des Bayerischen Alpenvorlandes und in den Bayerischen Alpen	000278		\N	\N	\N		\N	\N	\N	291	\N	Y	Diss. Bot. 3: 213 S	Diss. Bot. 3: 213 S	\N		\N	book	2016-05-04 11:05:38.549	\N	1969	\N	\N	f
777	Pfalzgraf_1934_000279	\N	\N	\N	\N	\N	Die Vegetation des Meißners und seine Waldgeschichte	000279		\N	\N	\N		\N	\N	\N	7	\N	Y	Repert. Spec. Nov. Regni Veg. Beih. 75: 1-80. Berlin-Dahlem	Repert. Spec. Nov. Regni Veg. Beih. 75: 1-80. Berlin-Dahlem	\N		\N	book	2016-05-04 11:05:38.841	\N	1934	\N	\N	f
783	Pfrogner_1973_000281	\N	\N	\N	\N	\N	Grünlandgesellschaften und Grundwasser der Innaue südlich von Rosenheim	000281		\N	\N	\N		\N	\N	\N	89	\N	Y	Diss. Bot. 23: Lehre	Diss. Bot. 23: Lehre	\N		\N	book	2016-05-04 11:05:39.356	\N	1973	\N	\N	f
790	Philippi_1972_000284	\N	\N	\N	\N	\N	Erläuterungen zur vegetationskundlichen Karte 1:25 000 Blatt 6617 Schwetzingen	000284		\N	\N	\N		\N	\N	\N	9	\N	Y	Landesvermessungsamt Baden-Württ. Stuttgart. 60 S	Landesvermessungsamt Baden-Württ. Stuttgart. 60 S	\N		\N	book	2016-05-04 11:05:40.081	\N	1972	\N	\N	f
792	Philippi_1983_000285	\N	\N	\N	\N	\N	Erläuterungen zur vegetationskundlichen Karte 1:25 000: 6323 Tauberbischofsheim-West	000285		\N	\N	\N		\N	\N	\N	102	\N	Y	Landesvermessungsamt Baden-Württ. Stuttgart. 200 S	Landesvermessungsamt Baden-Württ. Stuttgart. 200 S	\N		\N	book	2016-05-04 11:05:40.341	\N	1983	\N	\N	f
812	Raabe_1946_000293	\N	\N	\N	\N	\N	Über Pflanzengesellschaften des Grünlandes in Schleswig-Holstein	000293		\N	\N	\N		\N	\N	\N	95	\N	Y			\N	Diss. Universität Kiel	\N	thesis	2016-05-04 11:05:42.411	\N	1946	\N	\N	f
890	Schwabe_1987_000322	\N	\N	\N	\N	\N	Fluß- und bachbegleitende Pflanzengesellschaften und Vegetationskomplexe im Schwarzwald	000322		\N	\N	\N		\N	\N	\N	180	\N	Y	Diss. Bot. 102: 1-368. Berlin, Stuttgart	Diss. Bot. 102: 1-368. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:05:49.588	\N	1987	\N	\N	f
1658	F.a._Obereimer_1967_000604	\N	\N	\N	\N	\N	F.A. Obereimer	000604		\N	NN	\N		BOHN	\N	\N	78	\N	Y			\N		\N	report	2016-05-04 11:06:58.905	\N	1967	1659	\N	f
899	Schwickerath_1933_000325	\N	\N	\N	\N	\N	Die Vegetation des Landkreises Aachen und ihre Stellung im nördlichen Westdeutschland	000325		\N	\N	\N		\N	\N	\N	0	\N	Y	Aachener Beiträge zur Heimatkunde. Aachen	Aachener Beiträge zur Heimatkunde. Aachen	\N		\N	book	2016-05-04 11:05:50.302	\N	1933	\N	\N	f
911	Sebald_1974_000330	\N	\N	\N	\N	\N	Erläuterungen zur vegetationskundlichen Karte 1:25 000 Blatt 6923 Sulzbach/Murr (Mainhardter Wald)	000330		\N	\N	\N		\N	\N	\N	70	\N	Y	Staatl. Museum f. Naturkunde Stuttgart Stuttgart	Staatl. Museum f. Naturkunde Stuttgart Stuttgart	\N		\N	book	2016-05-04 11:05:51.523	\N	1974	\N	\N	f
928	Spatz_1970_000336	\N	\N	\N	\N	\N	Pflanzengesellschaften, Leistungen und Leistungspotential von Allgäuer Alpweiden in Abhängigkeit von Standort und Bewirtschaftung	000336		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	Diss. TH München. 145 S	\N	thesis	2016-05-04 11:05:53.062	\N	1970	\N	\N	f
1014	Weissbecker_1993_000370	\N	\N	\N	\N	\N	Fließgewässermakrophyten, bachbegleitende Pflanzengesellschaften und Vegetationskomplex im Odenwald - eine Fließgewässertypologie	000370		\N	\N	\N		\N	\N	\N	0	\N	Y	Umweltplanung, Arbeits-u.Umweltschutz 150: Wiesbaden. Hessische Lande ?	Umweltplanung, Arbeits-u.Umweltschutz 150: Wiesbaden. Hessische Lande ?	\N		\N	book	2016-05-04 11:06:01.15	\N	1993	\N	\N	f
1023	Wey_1988_000373	\N	\N	\N	\N	\N	Die Vegetation von Quellgebieten im Raum Trier und ihre Beeinflusssung durch land- und forstwirtschaftliche Bodennutzung der Einzugsgebiete	000373		\N	\N	\N		\N	\N	\N	49	\N	Y	Diss. Bot. 125: 1-170. Berlin, Stuttgart	Diss. Bot. 125: 1-170. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:06:01.906	\N	1988	\N	\N	f
1063	Ahrens_Brinkoch_Gohde_Jork_Kartes_Lauser_Lohse_Schafmeister_Siekmann_Wette_1981_000384	\N	\N	\N	\N	\N	Gipskarstlandschaft Hainholz/Beierstein	000384		\N	HBS	\N		TABGESNE	\N	\N	40	\N	Y			\N		\N	report	2016-05-04 11:06:04.709	\N	1981	1064	\N	f
1071	Andres_1994_000387	\N	\N	\N	\N	\N	Flora und Vegetation im Naturschutzgebiet "Badraer Lehde - Großer Eller" am Kyffhäuser	000387		\N	AND	\N		TABGESNE	\N	\N	163	\N	N			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 152 S	\N	thesis	2016-05-04 11:06:05.457	\N	1994	\N	\N	f
1079	Bartram_1997_000390	\N	\N	\N	\N	\N	Artenreiche Grünlandgesellschaften im nördlichen Sollingvorland und Teilen des Ith-Hils-Berglandes	000390		\N	BRT	\N		TABGESNE	\N	\N	21	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 124 S	\N	thesis	2016-05-04 11:06:06.195	\N	1997	\N	\N	f
1104	Berlin-Wolf_1978_000399	\N	\N	\N	\N	\N	Wiesen- und Rasengesellschaften der Dransfelder Hochfläche im Einzugsbereich der Weser	000399		\N	BEW	\N		TABGESNE	\N	\N	30	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 68 S	\N	thesis	2016-05-04 11:06:08.573	\N	1978	\N	\N	f
1608	Korneck_1971_000587	\N	\N	\N	\N	\N	?	000587		\N	KO	unpubliziert		BOHN	\N	\N	58	\N	Y	Mskr		\N		\N	unpublished	2016-05-04 11:06:54.587	\N	1971	\N	\N	f
1107	Blank_1984_000400	\N	\N	\N	\N	\N	Vergesellschaftung und Standortsbedingungen seltener Pflanzenarten südlich von Göttingen	000400		\N	BLA	\N		TABGESNE	\N	\N	30	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 134 S	\N	thesis	2016-05-04 11:06:08.827	\N	1984	\N	\N	f
1132	Brinkoch_Jork_1985_000410	\N	\N	\N	\N	\N	Kalk-Magerrasen am Nordrand der deutschen Mittelgebirge	000410		\N	BRI	\N		TABGESNE	\N	\N	123	\N	Y			\N	Dipl.-Arb. Institut f. Landschaftspflege u. Naturschutz u. Institut f. Geobot., Universität Hannover. 221 S	\N	thesis	2016-05-04 11:06:11.46	\N	1985	\N	\N	f
1149	Burkart_1991_000416	\N	\N	\N	\N	\N	Grünlandgesellschaften im Unteren Werraland	000416		\N	BUK	\N		TABGESNE	\N	\N	152	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 73 S	\N	thesis	2016-05-04 11:06:12.942	\N	1991	\N	\N	f
1153	Burkart_1998_000418	\N	\N	\N	\N	\N	Die Grünlandvegetation der unteren Havelaue in synökologischer und syntaxonomischer Sicht	000418		\N	BUK	\N		TABGESNE	\N	\N	26	\N	Y	Diss. Bot. 7: 1-157	Diss. Bot. 7: 1-157	\N		\N	book	2016-05-04 11:06:13.4	\N	1998	\N	\N	f
1163	Czygan_Scholtissk_Wehner_1994_000421	\N	\N	\N	\N	\N	Pflege- und Entwicklungsplan "Breiter Berg"	000421		\N	CSW	\N		TABGESNE	\N	\N	41	\N	Y			\N	3. Projektarbeit Institut f. Landschaftspflege u. Naturschutz d. Universität Hannover 134 S	\N	thesis	2016-05-04 11:06:14.169	\N	1994	\N	\N	f
1169	Dahlmann_Hahn_Hardes_Kornelius_1985_000422	\N	\N	\N	\N	\N	Pflege- und Entwicklungsplanung für den Bereich "Juliushütte" im Rahmen der geplanten Erweiterung des Naturschutzgebietes "Itelteich" bei Walkenried	000422		\N	DHH	\N		TABGESNE	\N	\N	56	\N	Y			\N		\N	report	2016-05-04 11:06:14.425	\N	1985	1170	\N	f
1200	Ernst-Moritz_1986_000433	\N	\N	\N	\N	\N	Flora und Vegetation im Bereich der ehemaligen Bahnlinie Göttingen/Dransfeld bei Ossenfeld	000433		\N	EMZ	\N		TABGESNE	\N	\N	27	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 118 S	\N	thesis	2016-05-04 11:06:16.977	\N	1986	\N	\N	f
1212	Frede_1987_000437	\N	\N	\N	\N	\N	Verbreitung und Ökologie der Sesleria varia (Jacq.) Wettst. - Vorkommen im Raume Edersee/Nordhessen	000437		\N	FRE	\N		TABGESNE	\N	\N	176	\N	Y			\N	Dipl.-Arb. FB Biol., Universität Marburg. 160 S	\N	thesis	2016-05-04 11:06:17.922	\N	1987	\N	\N	f
1228	Haack_1989_000442	\N	\N	\N	\N	\N	Vegetationsänderungen auf einem Halb-Trockenrasen unter Einfluß extensiver Schafbeweidung	000442		\N	HAA	\N		TABGESNE	\N	\N	50	\N	Y			\N	Dipl.-Arb. Universität Göttingen. 138 S	\N	thesis	2016-05-04 11:06:19.077	\N	1989	\N	\N	f
1240	Heide_1984_000446	\N	\N	\N	\N	\N	Die Vegetation der Kalkmagerrasen im Raum Witzenhausen	000446		\N	HEK	\N		TABGESNE	\N	\N	181	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 91 S	\N	thesis	2016-05-04 11:06:19.951	\N	1984	\N	\N	f
1243	Heinken_1995_000447	\N	\N	\N	\N	\N	Naturnahe Laub- und Nadelwälder grundwasserferner Standorte im niedersächsischen Tiefland: Gliederung, Standortsbedingungen, Dynamik	000447		\N	HEI	\N		TABGESNE	\N	\N	823	\N	Y	Diss. Bot. 239: 1-311. Berlin, Stuttgart	Diss. Bot. 239: 1-311. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:06:20.158	\N	1995	\N	\N	f
1249	Helmecke_1972_000449	\N	\N	\N	\N	\N	Ökologische Untersuchungen an Pflanzengesellschaften im NSG "Ochsenburg-Ziegelhüttental"	000449		\N	HEL	\N		TABGESNE	\N	\N	47	\N	Y			\N	Diss. Universität Halle	\N	thesis	2016-05-04 11:06:20.667	\N	1972	\N	\N	f
1255	Henze_Schlufter_1993_000451	\N	\N	\N	\N	\N	Schutzwürdigkeitsgutachten für die Erweiterung des Naturschutzgebietes "Schloßberg-Solwiesen"	000451		\N	HEU	\N		TABGESNE	\N	\N	9	\N	Y			\N		\N	report	2016-05-04 11:06:21.143	\N	1993	1256	\N	f
1259	Hessler_1978_000452	\N	\N	\N	\N	\N	Flora und Vegetation der "Kripplöcher" im Landkreis Eschwege	000452		\N	HER	\N		TABGESNE	\N	\N	29	\N	Y			\N	Wiss. Hausarbeit Gesamthochschule Kassel	\N	thesis	2016-05-04 11:06:21.399	\N	1978	\N	\N	f
1270	Hoelzel_1997_000456	\N	\N	\N	\N	\N	Untersuchungen zu Vegetationsverhältnissen und Veränderungen im NSG "Steinklöbe" (Unteres Unstruttal)	000456		\N	HZM	\N		TABGESNE	\N	\N	86	\N	Y			\N	Dipl.-Arb. Institut f. Geobot., Universität Halle. 167 S	\N	thesis	2016-05-04 11:06:22.347	\N	1997	\N	\N	f
1273	Horch_1986_000457	\N	\N	\N	\N	\N	Kalkhalbtrockenrasen als prägendes Element der Vegetation am Südhang des Burgberges bei Bevern, Kreis Holzminden	000457		\N	HOD	\N		TABGESNE	\N	\N	47	\N	Y			\N	Dipl.-Arb. Universität GH Paderborn, Abt. Höxter. 85 S	\N	thesis	2016-05-04 11:06:22.605	\N	1986	\N	\N	f
1276	Hullen_1983_000458	\N	\N	\N	\N	\N	Renaturierungsvorschläge für Gipssteinbrüche auf ökologischer Grundlage (Erarbeitet für das Abbaugebiet im Harzvorland)	000458		\N	HUM	\N		TABGESNE	\N	\N	42	\N	Y			\N	Dipl.-Arb. Universität Hannover. 158 S	\N	thesis	2016-05-04 11:06:22.818	\N	1983	\N	\N	f
1284	Jandt_1992_000461	\N	\N	\N	\N	\N	Vegetation und Flora von Kalkmagerrasen im westlichen Teil des Landkreises Heiligenstadt	000461		\N	JAU	\N		TABGESNE	\N	\N	344	\N	N			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen 103 S	\N	thesis	2016-05-04 11:06:23.49	\N	1992	\N	\N	f
1295	Kilian_1972_000466	\N	\N	\N	\N	\N	Die Kalk-Halbtrockenrasen auf Gips westlich Osterode/Harz	000466		\N	KIJ	\N		TABGESNE	\N	\N	53	\N	Y			\N	Staatsex.-Arb. Universität Hamburg. 44 S	\N	thesis	2016-05-04 11:06:24.758	\N	1972	\N	\N	f
1318	Knoop_1984_000477	\N	\N	\N	\N	\N	Flora und Vegetation der Kalkhügel bei Bad Harzburg am Nordharzrand unter Berücksichtigung des Naturschutzes	000477		\N	KNO	\N		TABGESNE	\N	\N	101	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 187 S	\N	thesis	2016-05-04 11:06:27.537	\N	1984	\N	\N	f
1341	Langenhorst_1990_000487	\N	\N	\N	\N	\N	Kalk-Magerrasen im Landkreis Göttingen und ihre Brache-Stadien	000487		\N	LAN	\N		TABGESNE	\N	\N	248	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 195 S	\N	thesis	2016-05-04 11:06:30.19	\N	1990	\N	\N	f
1361	Mahn_1959_000494	\N	\N	\N	\N	\N	Vegetations- und standortkundliche Untersuchungen an Felsfluren, Trocken- und Halbtrockenrasen Mitteldeutschlands	000494		\N	MAH	\N		TABGESNE	\N	\N	5	\N	Y			\N	Diss. Universität Halle. 215 S	\N	thesis	2016-05-04 11:06:31.882	\N	1959	\N	\N	f
1385	Nagler_1983_000504	\N	\N	\N	\N	\N	Floristisch-vegetationskundliche Untersuchungen ausgewählter Bereiche der Dransfelder Hochfläche unter besonderer Berücksichtigung des Naturschutzes	000504		\N	NAG	\N		TABGESNE	\N	\N	92	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 168 S	\N	thesis	2016-05-04 11:06:34.303	\N	1983	\N	\N	f
1388	Neuenroth_1988_000505	\N	\N	\N	\N	\N	Die Vegetation der Wiesen und Magerrasen im westlichen Meißnervorland (Nordhessen)	000505		\N	NER	\N		TABGESNE	\N	\N	18	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 74 S	\N	thesis	2016-05-04 11:06:34.551	\N	1988	\N	\N	f
1403	Pfeiffer_1997_000511	\N	\N	\N	\N	\N	Geranio sanguinei-Dictamnetum - Lebensstrategien in Diptam-Säumen des unteren Unstruttals (Sachsen-Anhalt)	000511		\N	PFF	\N		TABGESNE	\N	\N	45	\N	Y			\N	Dipl.-Arb. Syst. Bot. Institut Pflanzengeogr., FU Berlin. 136 S	\N	thesis	2016-05-04 11:06:36.074	\N	1997	\N	\N	f
1610	Krause_1972_000588	\N	\N	\N	\N	\N	Siebengebirge	000588		\N	KR	\N		BOHN	\N	\N	38	\N	Y			\N		\N	report	2016-05-04 11:06:54.802	\N	1972	1611	\N	f
1408	Pless_1994_000513	\N	\N	\N	\N	\N	Pflanzensoziologische Untersuchungen der Trockenrasen an den Hängen des Odertales im Kreis Seelow (Brandenburg)	000513		\N	PLS	\N		TABGESNE	\N	\N	158	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 179 S	\N	thesis	2016-05-04 11:06:36.68	\N	1994	\N	\N	f
1413	Protz_1992_000515	\N	\N	\N	\N	\N	Floristisch-vegetationskundliche Untersuchungen isolierter Kalkmagerrasen im Raum Iversheim/Wachendorf (Kreis Euskirchen/Eifel) und deren Bedeutung für ausgewählte tagaktive Schetterlingsarten	000515		\N	PRB	\N		TABGESNE	\N	\N	28	\N	Y			\N	Dipl.-Arb. Math.-Nat. Fak., Universität Bonn	\N	thesis	2016-05-04 11:06:37.157	\N	1992	\N	\N	f
1419	Reding_1972_000517	\N	\N	\N	\N	\N	Untersuchungen zur Vegetationsdynamik einiger Xerothermrasen im Naturschutzgebiet Leutratal	000517		\N	REG	\N		TABGESNE	\N	\N	15	\N	Y			\N	Dipl.-Arb. Universität Halle	\N	thesis	2016-05-04 11:06:37.704	\N	1972	\N	\N	f
1426	Reichhoff_1977_000520	\N	\N	\N	\N	\N	Mikroklimatische und ökophysiologische Untersuchungen im NSG "Leutratal" - Zur Differenzierung der Trocken- und Halbtrockenrasen	000520		\N	REI	\N		TABGESNE	\N	\N	0	\N	Y			\N	Diss. Universität Halle. 152 S	\N	thesis	2016-05-04 11:06:38.424	\N	1977	\N	\N	f
1458	Rueden_1952_000530	\N	\N	\N	\N	\N	Wald-, Trocken- und Halbtrockenrasengesellschaften des nordöstlichen Sauerlandes und seiner Randgebiete	000530		\N	RUV	\N		TABGESNE	\N	\N	23	\N	Y			\N	Diss. Universität Münster. 147 S	\N	thesis	2016-05-04 11:06:41.113	\N	1952	\N	\N	f
1439	Rogge_1985_000524	\N	\N	\N	\N	\N	Biotopmanagementplan für das NSG "Wulsenberg" (Hochsauerlandkreis)	000524		\N	ROG	\N		TABGESNE	\N	\N	17	\N	Y			\N		\N	report	2016-05-04 11:06:39.655	\N	1985	1440	\N	f
1446	Rohde_1984_000526	\N	\N	\N	\N	\N	Flora und Vegetation der Weper	000526		\N	RHD	\N		TABGESNE	\N	\N	197	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 169 S	\N	thesis	2016-05-04 11:06:40.203	\N	1984	\N	\N	f
1452	Rost_1955_000528	\N	\N	\N	\N	\N	Die Trockenrasen und Heiden auf den Kreidesandsteinhügeln des Quedlinburger Sattels	000528		\N	ROJ	\N		TABGESNE	\N	\N	109	\N	Y			\N	Dipl.-Arb. Universität Halle. 82 S	\N	thesis	2016-05-04 11:06:40.66	\N	1955	\N	\N	f
1377	Moeller_1992_000501	\N	\N	\N	\N	\N	Grünlandvegetation in der Umgebung von Göttingen - ein Vergleich nach 20 Jahren	000501		\N	MOC	\N		TABGESNE	\N	\N	88	\N	N			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 106 S	\N	thesis	2016-05-04 11:06:33.585	\N	1992	\N	\N	f
1464	Scharf_1993_000532	\N	\N	\N	\N	\N	Laubwaldgesellschaften in Teilbereichen des Ohmgebirges (Nordwestthüringen)	000532		\N	SCA	\N		TABGESNE	\N	\N	62	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 94 S	\N	thesis	2016-05-04 11:06:41.543	\N	1993	\N	\N	f
1429	Roedel_1970_000521	\N	\N	\N	\N	\N	Waldgesellschaften der Sieben Berge bei Alfeld und ihre Ersatzgesellschaften	000521		\N	ROH	\N		TABGESNE	\N	\N	303	\N	Y	Diss. Bot. 7: 1-144	Diss. Bot. 7: 1-144	\N		\N	book	2016-05-04 11:06:38.93	\N	1970	\N	\N	f
1474	Schneeweiss_1971_000536	\N	\N	\N	\N	\N	Pflanzenverbreitung des Naturschutzgebietes "Alter Stolberg"	000536		\N	SCI	\N		TABGESNE	\N	\N	2	\N	Y			\N	Staatsex.-Arb. Universität Halle. 57 S	\N	thesis	2016-05-04 11:06:42.567	\N	1971	\N	\N	f
1480	Schorr_1986_000539	\N	\N	\N	\N	\N	Pflege- und Entwicklungsplan für das weidebedingte Mesobromion am Rieseberg (Stadt Königslutter am Elm, Landkr. Helmstedt)	000539		\N	SCR	\N		TABGESNE	\N	\N	10	\N	Y			\N	4. Projektarbeit Institut f. Landschaftspflege u. Naturschutz d. Universität Hannover 70 S	\N	thesis	2016-05-04 11:06:42.99	\N	1986	\N	\N	f
1490	Schwochow_1997_000543	\N	\N	\N	\N	\N	Pflanzensoziologische Untersuchungen im Gipskarstgebiet des südlichen Harzvorlandes	000543		\N	SWH	\N		TABGESNE	\N	\N	224	\N	Y			\N	Dipl.-Arb. Geogr. Institut, Universität Hannover. 139 S	\N	thesis	2016-05-04 11:06:43.961	\N	1997	\N	\N	f
1493	Sommer_1971_000544	\N	\N	\N	\N	\N	Wald- und Ersatzgesellschaften im östlichen Niedersachsen	000544		\N	SOM	\N		TABGESNE	\N	\N	84	\N	Y	Diss. Bot. 12: 1-101. Berlin, Stuttgart	Diss. Bot. 12: 1-101. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:06:44.166	\N	1971	\N	\N	f
1496	Suchodoletz_1973_000545	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen in den NSG "Steinklöbe" und "Neue Göhle"	000545		\N	SUZ	\N		TABGESNE	\N	\N	40	\N	Y			\N	Dipl.-Arb. Universität Halle	\N	thesis	2016-05-04 11:06:44.458	\N	1973	\N	\N	f
1508	Voll_1988_000549	\N	\N	\N	\N	\N	Pionier- und Grünlandgesellschaften bei Donsbach (Mittelhessen)	000549		\N	VOC	\N		TABGESNE	\N	\N	59	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen	\N	thesis	2016-05-04 11:06:45.445	\N	1988	\N	\N	f
1511	Vollmer_1990_000550	\N	\N	\N	\N	\N	Vegetationskundliche und standortökologische Untersuchungen an den Keuperscharren bei Bitburg (Eifel)	000550		\N	VOI	\N		TABGESNE	\N	\N	73	\N	Y			\N	Dipl.-Arb. Math.-Nat. Fak., Universität Bonn	\N	thesis	2016-05-04 11:06:45.711	\N	1990	\N	\N	f
1519	Weidner_1990_000553	\N	\N	\N	\N	\N	Beziehungen zwischen Vegetation und tagaktiven Schmetterlingen	000553		\N	WEA	\N		TABGESNE	\N	\N	32	\N	Y			\N	Dipl.-Arb. Math.-Nat. Fak., Universität Bonn	\N	thesis	2016-05-04 11:06:46.379	\N	1990	\N	\N	f
1521	Weinert_Gulich_1993_000554	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen zur Umweltverträglichkeitsstudie im südharzer Gipsgebiet im Bereich des Bergwerkseigentums Südharzer Gipswerke GmbH Ellrich	000554		doppelte Bibref	WGU	\N		TABGESNE	\N	\N	30	\N	Y			\N		\N	report	2016-05-04 11:06:46.637	\N	1993	1522	\N	f
1525	Wellnitz_1995_000555	\N	\N	\N	\N	\N	Die subkontinentalen Poa badensis-Bestände des Mitteldeutschen Trockengebietes (Thüringen, Sachsen-Anhalt): Untersuchungen zur Soziologie und zu den Lebensstrategien	000555		\N	WLN	\N		TABGESNE	\N	\N	43	\N	Y			\N	Dipl.-Arb. Institut Syst. Botanik u. Pflanzengeographie, FU Berlin. 94 S	\N	thesis	2016-05-04 11:06:46.875	\N	1995	\N	\N	f
1552	Zuendorf_1976_000566	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen im oberen Werratal bei Themar	000566		\N	ZUE	\N		TABGESNE	\N	\N	221	\N	Y			\N	Dipl.-Arb. Mskr. Universität Halle	\N	thesis	2016-05-04 11:06:49.448	\N	1976	\N	\N	f
1556	Bohn_1976_000568	\N	\N	\N	\N	\N	?	000568		\N	BO	unpubliziert		BOHN	\N	\N	60	\N	Y	Mskr., Bundesforschungsanstalt f. Naturschutz u. Landschaftsökologie		\N		\N	unpublished	2016-05-04 11:06:49.948	\N	1976	\N	\N	f
1566	Dudeck_1982_000572	\N	\N	\N	\N	\N	Groß- und kleinräumige Vegetationsübergänge auf Muschelkalk- und Buntsandstein-Standorten im Bereich Hönstollen-Hellekopf östlich von Göttingen	000572		\N	DU	\N		BOHN	\N	\N	33	\N	Y			\N	Dipl.-Arb. Göttingen. 91 S	\N	thesis	2016-05-04 11:06:51.058	\N	1982	\N	\N	f
1571	Glavac_1969_000574	\N	\N	\N	\N	\N	Erläuterungen zu den Wald- und Forstgesellschaften des Forstamtes Attendorn	000574		\N	GL	\N		BOHN	\N	\N	14	\N	Y			\N		\N	report	2016-05-04 11:06:51.515	\N	1969	1572	\N	f
1601	Kuenne_1969_000584	\N	\N	\N	\N	\N	Laubwaldgesellschaften der Frankenalb	000584		\N	KN	\N		BOHN	\N	\N	589	\N	Y	Diss. Bot. 2: 177 S. Berlin, Stuttgart	Diss. Bot. 2: 177 S. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:06:53.9	\N	1969	\N	\N	f
1603	Kuenne_1973_000585	\N	\N	\N	\N	\N	Erläuterungen zu den Vegetationsaufnahmen im Staatlichen Forstamt Monschau	000585		\N	KN	\N		BOHN	\N	\N	42	\N	Y			\N		\N	report	2016-05-04 11:06:54.105	\N	1973	1604	\N	f
1588	Jahn_1951_000580	\N	\N	\N	\N	\N	Die Vegetation im Stadtwald Meschede und der angrenzenden Staatsforsten unter bes. Berücksichtigung der Kalkungsflächen d. Mescheder Stadtwaldes	000580		\N	JS	\N		BOHN	\N	\N	106	\N	Y			\N		\N	report	2016-05-04 11:06:52.891	\N	1951	1589	\N	f
1591	Jahn_1952_000581	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Reviers Einsiedelei. F.A. Olpe	000581		\N	JS	\N		BOHN	\N	\N	30	\N	Y			\N		\N	report	2016-05-04 11:06:53.183	\N	1952	1592	\N	f
1594	Jahn_1952_000582	\N	\N	\N	\N	\N	Wald-, Hauberg- und Forstgesellschaften im Kreise Siegen	000582		\N	JS	\N		BOHN	\N	\N	78	\N	Y			\N		\N	report	2016-05-04 11:06:53.44	\N	1952	1595	\N	f
1598	Klauck_1985_000583	\N	\N	\N	\N	\N	Natürliche Laubwaldgesellschaften im südwestlichen Hunsrück. Eine vegetationskundliche Untersuchung im Schwarzwälder Hochwald	000583		\N	KL	\N		BOHN	\N	\N	24	\N	Y	1. Aufl., 74 S. Haag + Herchen, Frankfurt/Main	1. Aufl., 74 S. Haag + Herchen, Frankfurt/Main	\N		\N	book	2016-05-04 11:06:53.651	\N	1985	\N	\N	f
1616	Lohmeyer_1953_000590	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte der Genossenschaftsforsten bei Saalhausen	000590		\N	LO	\N		BOHN	\N	\N	32	\N	Y			\N		\N	report	2016-05-04 11:06:55.313	\N	1953	1617	\N	f
1619	Lohmeyer_1955_000591	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte des Forstamtes Corvey	000591		\N	LO	\N		BOHN	\N	\N	80	\N	Y			\N		\N	report	2016-05-04 11:06:55.572	\N	1955	1620	\N	f
1622	Lohmeyer_1956_000592	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte des Kottenforstes	000592		\N	LO	\N		BOHN	\N	\N	14	\N	Y			\N		\N	report	2016-05-04 11:06:55.868	\N	1956	1623	\N	f
1625	Lohmeyer_1957_000593	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte des Forstamtes Wenau	000593		\N	LO	\N		BOHN	\N	\N	80	\N	Y			\N		\N	report	2016-05-04 11:06:56.132	\N	1957	1626	\N	f
1628	Lohmeyer_1959_000594	\N	\N	\N	\N	\N	Erläuterungstabelle zur Vegetationskarte des Waldgutes Burgholdinghausen	000594		\N	LO	\N		BOHN	\N	\N	70	\N	Y			\N		\N	report	2016-05-04 11:06:56.403	\N	1959	1629	\N	f
1631	Lohmeyer_1963_000595	\N	\N	\N	\N	\N	Zur Kenntnis der Wald- und Forstgesellschaften des Forstamtes Königsforst	000595		\N	LO	\N		BOHN	\N	\N	22	\N	Y			\N		\N	report	2016-05-04 11:06:56.622	\N	1963	1632	\N	f
1634	Lohmeyer_1966_000596	\N	\N	\N	\N	\N	Erläuterungen zu den Vegetationsaufnahmen im Stadtwald Menden	000596		\N	LO	\N		BOHN	\N	\N	12	\N	Y			\N		\N	report	2016-05-04 11:06:56.879	\N	1966	1635	\N	f
1637	Lohmeyer_1969_000597	\N	\N	\N	\N	\N	Erläuterungen zu den Vegetationsaufnahmen im Staatsforst Dalheim	000597		\N	LO	\N		BOHN	\N	\N	18	\N	Y			\N		\N	report	2016-05-04 11:06:57.144	\N	1969	1638	\N	f
1640	Lohmeyer_1971_000598	\N	\N	\N	\N	\N	Über einige Laubwaldgesellschaften im Revierförsterbezirk Burgholz	000598		\N	LO	\N		BOHN	\N	\N	22	\N	Y			\N		\N	report	2016-05-04 11:06:57.402	\N	1971	1641	\N	f
1643	Lohmeyer_1972_000599	\N	\N	\N	\N	\N	Erläuterungen zu den Vegetationsaufnahmen im Staatsforst Wünneberg	000599		\N	LO	\N		BOHN	\N	\N	90	\N	Y			\N		\N	report	2016-05-04 11:06:57.638	\N	1972	1644	\N	f
1646	Lohmeyer_Trautmann_1957_000600	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte des Forstamtes Minden	000600		\N	LT	\N		BOHN	\N	\N	86	\N	Y			\N		\N	report	2016-05-04 11:06:57.859	\N	1957	1647	\N	f
1654	Meisel_Trautmann_1956_000603	\N	\N	\N	\N	\N	Erläuterungen zu den Vegetationskarten der Abflußmeßanlagen "Solchbach" und "Oberer Wehebach" (Hürtgen-Wald)	000603		\N	MT	\N		BOHN	\N	\N	18	\N	Y			\N		\N	report	2016-05-04 11:06:58.658	\N	1956	1655	\N	f
1661	Nowak_1990_000605	\N	\N	\N	\N	\N	Ergebnisse der pflanzensoziologischen Sonntagsexkursion...  Bodensaure Eichen-Wälder (Quercion robori-sesseliflorae Braun-Blanquet 1932)	000605		\N	NO	unpubliziert		BOHN	\N	\N	101	\N	Y	Mskr.(Bohn-Daten) und Botanik u. Naturschutz in Hessen Beih. 2 : 147-152. Frankfurt a. M		\N		\N	unpublished	2016-05-04 11:06:59.171	\N	1990	\N	\N	f
1668	Petermann_Seibert_1984_000609	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Nationalparks Bayerischer Wald mit einer farbigen Vegetationskarte	000609		\N	PS	\N		BOHN	\N	\N	542	\N	Y	Nationalpark Bayerischer Wald, Schriftenreihe d. Bayerischen Staatsministeriums f. Ernährung, Landwirtschaft u. Forst., München	Nationalpark Bayerischer Wald, Schriftenreihe d. Bayerischen Staatsministeriums f. Ernährung, Landwirtschaft u. Forst., München	\N		\N	book	2016-05-04 11:06:59.914	\N	1984	\N	\N	f
1670	Petermann_1970_000610	\N	\N	\N	\N	\N	Montane Buchenwälder im westbayerischen Alpenvorland zwischen Iller und Ammersee	000610		\N	RP	\N		BOHN	\N	\N	843	\N	Y	Diss. Bot. 8: 227 S. Vaduz	Diss. Bot. 8: 227 S. Vaduz	\N		\N	book	2016-05-04 11:07:00.175	\N	1970	\N	\N	f
1672	Seibert_1951_000611	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte des Pfarrwaldes Lachweiler	000611		\N	SE	\N		BOHN	\N	\N	12	\N	Y			\N		\N	report	2016-05-04 11:07:00.449	\N	1951	1673	\N	f
1675	Seibert_1952_000612	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte des Revierförsterbezirks Arnsberg-West	000612		\N	SE	\N		BOHN	\N	\N	56	\N	Y			\N		\N	report	2016-05-04 11:07:00.702	\N	1952	1676	\N	f
1678	Seibert_1954_000613	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte einiger Privatwaldungen im Bereich des Landwirtschaftskammerforstamtes Arnsberg	000613		\N	SE	\N		BOHN	\N	\N	64	\N	Y			\N		\N	report	2016-05-04 11:07:01.008	\N	1954	1679	\N	f
1681	Seibert_1957_000614	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte der Waldungen bei Hainchen und Werthenbach	000614		\N	SE	\N		BOHN	\N	\N	14	\N	Y			\N		\N	report	2016-05-04 11:07:01.317	\N	1957	1682	\N	f
1685	Seifarth_1988_000615	\N	\N	\N	\N	\N	Laubwaldgesellschaften im Südwest-Harz	000615		\N	SF, SEF	\N		BOHN	SPFLUME	\N	472	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 157 S	\N	thesis	2016-05-04 11:07:01.563	\N	1988	\N	\N	f
1690	Streitz_1967_000617	\N	\N	\N	\N	\N	Bestockungswandel in Laubwaldgesellschaften des Rhein-Main-Tieflandes und der hessischen Rheinebene	000617		\N	ST	\N		BOHN	\N	\N	120	\N	Y			\N	Diss. Hann. Münden. 213 S	\N	thesis	2016-05-04 11:07:02.084	\N	1967	\N	\N	f
1694	Trautmann_1954_000619	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte des Forstamtes Lauterberg (Harz)	000619		\N	TR	\N		BOHN	\N	\N	62	\N	Y			\N		\N	report	2016-05-04 11:07:02.598	\N	1954	1695	\N	f
1697	Trautmann_1955_000620	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte des Forstamtes Lauenau	000620		\N	TR	\N		BOHN	\N	\N	46	\N	Y			\N		\N	report	2016-05-04 11:07:02.856	\N	1955	1698	\N	f
1700	Trautmann_1955_000621	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte des Staatsforstes Rumbeck	000621		\N	TR	\N		BOHN	\N	\N	90	\N	Y			\N		\N	report	2016-05-04 11:07:03.078	\N	1955	1701	\N	f
1703	Trautmann_1956_000622	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte des Forstamtes Neuenheerse	000622		\N	TR	\N		BOHN	\N	\N	19	\N	Y			\N		\N	report	2016-05-04 11:07:03.291	\N	1956	1704	\N	f
1706	Trautmann_1956_000623	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte des Stadtwaldes Düsseldorf	000623		\N	TR	\N		BOHN	\N	\N	19	\N	Y			\N		\N	report	2016-05-04 11:07:03.513	\N	1956	1707	\N	f
1712	Trautmann_1960_000625	\N	\N	\N	\N	\N	F.A. Wesel	000625		\N	TR	\N		BOHN	\N	\N	22	\N	Y			\N		\N	report	2016-05-04 11:07:04.036	\N	1960	1713	\N	f
1715	Trautmann_1964_000626	\N	\N	\N	\N	\N	Erläuterungen zu den Vegetationsaufnahmen im Staatlichen Forstamt Bredelar und im Stadtwald Niedermarsberg	000626		\N	TR	\N		BOHN	\N	\N	15	\N	Y			\N		\N	report	2016-05-04 11:07:04.293	\N	1964	1716	\N	f
1718	Trautmann_1966_000627	\N	\N	\N	\N	\N	Erläuterungen zu den Vegetationsaufnahmen im Staatlichen Forstamt Gemänd	000627		\N	TR	\N		BOHN	\N	\N	55	\N	Y			\N		\N	report	2016-05-04 11:07:04.509	\N	1966	1719	\N	f
1739	Moelder_2005_000634	\N	\N	\N	\N	\N	Flora, Vegetation und Bestandesstruktur im Naturwald "Großer Freeden", Teutoburger Wald	000634		\N	\N	\N		Naturwald	Gro�er Freeden	\N	43	\N	N			\N	Masterarbeit Institut f. Waldbau, Abt. I, Universität Göttingen	\N	thesis	2016-05-04 11:07:06.252	\N	2005	\N	\N	f
1724	Welss_1985_000629	\N	\N	\N	\N	\N	Waldgesellschaften im nördlichen Steigerwald	000629		\N	WE	\N		BOHN	\N	\N	66	\N	Y	Diss. Bot. 83: 174 S. Vaduz	Diss. Bot. 83: 174 S. Vaduz	\N		\N	book	2016-05-04 11:07:05.007	\N	1985	\N	\N	f
1730	Megner_1999_000631	\N	\N	\N	\N	\N	Erfassung und Bewertung der Diversität zweier Buchenwaldgebiete unterschiedlicher Bewirtschaftung für den Naturschutz - Naturwaldreservat "Wattenberg und Hundsberg"	000631		\N	\N	\N		Naturwald	Wattenberg & Hundsberg	\N	89	\N	N			\N	Dipl.-Arb. Geogr. Institut, Universität Göttingen	\N	thesis	2016-05-04 11:07:05.546	\N	1999	\N	\N	f
1733	Weckesser_1992_000632	\N	\N	\N	\N	\N	Flora und Vegetation im Naturwald "Bruchberg" (Nationalpark Harz)	000632		\N	\N	\N		Naturwald	Bruchberg	\N	207	\N	N			\N	Dipl.-Arb. Institut f. Waldbau, Abt. I, Universität Göttingen	\N	thesis	2016-05-04 11:07:05.757	\N	1992	\N	\N	f
1742	Soyka_1998_000635	\N	\N	\N	\N	\N	Flora und Vegetation im Naturwald "Hasbruch"	000635		\N	\N	\N		Naturwald	Hasbruch	\N	230	\N	N			\N	Dipl.-Arb. Spez. Bot., FB Biologie/Chemie, Universität Osnabrück	\N	thesis	2016-05-04 11:07:06.477	\N	1998	\N	\N	f
1748	Ermert_2003_000637	\N	\N	\N	\N	\N	Die Dynamik der Flora und Vegetation im Naturwald "Hünstollen"	000637		\N	\N	\N		Naturwald	H�nstollen	\N	95	\N	N			\N	Masterarbeit Institut f. Waldbau, Abt. I, Universität Göttingen	\N	thesis	2016-05-04 11:07:06.946	\N	2003	\N	\N	f
1751	Ebrecht_1999_000638	\N	\N	\N	\N	\N	Flora und Vegetation im Natur- und Wirtschaftswald der "Pretzetzer Landwehr"	000638		\N	\N	\N		Naturwald	Landwehr	\N	47	\N	N			\N	Dipl.-Arb. Institut f. Waldbau, Abt. I, Universität Göttingen	\N	thesis	2016-05-04 11:07:07.192	\N	1999	\N	\N	f
1757	Garbitz_1990_000640	\N	\N	\N	\N	\N	Vegetation und Standortsbedingungen im Naturwald "Staufenberg"	000640		\N	\N	\N		Naturwald	Staufenberg	\N	198	\N	N			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen	\N	thesis	2016-05-04 11:07:07.733	\N	1990	\N	\N	f
1760	Melcher_1999_000641	\N	\N	\N	\N	\N	Flora und Vegetation im Naturwald "Großer Staufenberg" (Forstamt Walkenried, Revier Staufenberg)	000641		\N	\N	\N		Naturwald	Staufenberg	\N	198	\N	N			\N	Dipl.-Arb. Institut f. Waldbau, Abt. I, Universität Göttingen	\N	thesis	2016-05-04 11:07:08.023	\N	1999	\N	\N	f
1766	Fischer_2008_000643	\N	\N	\N	\N	\N	Dynamik der Vegetation, Flora und Bestandesstruktur im Naturwald Totenberg	000643		\N	\N	\N		Naturwald	Totenberg	\N	254	\N	N			\N	Masterarbeit Burckhardt-Institut, Abt. Waldbau u. Waldökologie d. gemäßigten Zonen, Universität Göttingen	\N	thesis	2016-05-04 11:07:08.486	\N	2008	\N	\N	f
1768	Grosser_Glotz_1960_000644	\N	\N	\N	\N	\N	Waldvegetationseinheiten des Oberlausitzer Hügellandes in der Umgebung von Görlitz	000644		\N	\N	\N		\N	TV	\N	123	\N	Y			\N		\N	report	2016-05-04 11:07:08.743	\N	1960	1769	\N	f
1772	Franke_2003_000645	\N	\N	\N	\N	\N	Grünland an der unteren Mittelelbe Vegetationsökologie und landwirtschaftliche Nutzbarkeit	000645		\N	\N	\N		\N	TV	\N	131	\N	Y	Diss. Bot. 370: 1-181. Berlin, Stuttgart	Diss. Bot. 370: 1-181. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:07:08.953	\N	2003	\N	\N	f
1721	Tuexen_1958_000628	\N	\N	\N	\N	\N	Erläuterungen zur Punktkarte des Forstamtes Escherode	000628		\N	TX	\N		BOHN	\N	\N	36	\N	Y			\N		\N	report	2016-05-04 11:07:04.772	\N	1958	1722	\N	f
1929	Schuhwerk_1988_000700	\N	\N	\N	\N	\N	Naturnahe Vegetation im Hotzenwald (Südöstlicher Schwarzwald)	000700		\N	\N	\N		LITTAB	Schuhwerk_1988	\N	1687	\N	Y			\N	Diss. Universität Regensburg. 526 S	\N	thesis	2016-05-04 11:07:22.52	\N	1988	\N	\N	f
1838	Schaaf_1992_000671	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchung an Fichten- und Fichten-Buchen-Mischbeständen im Forstamt Westerhof	000671		\N	\N	\N		\N	\N	\N	109	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut Universität Göttingen. 79 S	\N	thesis	2016-05-04 11:07:15.21	\N	1992	\N	\N	f
1855	Luckwald_Et_Al._1992_000677	\N	\N	\N	\N	\N	Flora und Vegetation der Fichtenwälder des Hochharzes. Floristische und vegetationskundliche Unteruchung ausgewählter Probeflächen in Fichtenwäldern des Harzes der montanen bis hochmontanen Stufe	000677		\N	\N	\N		\N	\N	\N	45	\N	Y			\N		\N	report	2016-05-04 11:07:16.769	\N	1992	1856	\N	f
1943	Boehnert_Walther_1997_000704	\N	\N	\N	\N	\N	NSG "Großer Weidenteich"	000704		\N	BRW	\N		TAB	\N	\N	77	\N	Y	Materialien zu Naturschutz u. Landschaftspflege 1. Dresden	Materialien zu Naturschutz u. Landschaftspflege 1. Dresden	\N		\N	book	2016-05-04 11:07:23.515	\N	1997	\N	\N	f
1946	Herter_1996_000705	\N	\N	\N	\N	\N	Die Xerothermvegetation der Oberen Donautals	000705		\N	HTD	unpubliziert		TAB	\N	\N	118	\N	Y	Landesanstalt Umweltschutz Baden-Württ		\N		\N	unpublished	2016-05-04 11:07:23.785	\N	1996	\N	\N	f
1948	Knapp_1944_000706	\N	\N	\N	\N	\N	Vegetationsaufnahmen von Wäldern des Thüringer Waldes	000706		\N	KNA	unpubliziert		\N	TV	\N	53	\N	Y	Mskr. Halle (Saale). 18 S		\N		\N	unpublished	2016-05-04 11:07:24.047	\N	1944	\N	\N	f
1972	Wesche_2008_000716	\N	\N	\N	\N	\N	Changes in eastern German moist meadows	000716		\N	\N	unpubliziert		Karsten_dict Nr. 000020	\N	\N	261	\N	N	Unveröff		\N		\N	unpublished	2016-05-04 11:07:26.18	\N	2008	\N	\N	f
1975	Krause_2008_000718	\N	\N	\N	\N	\N	Changes in western German moist meadows	000718		\N	\N	unpubliziert		Karsten_dict Nr. 000021	\N	\N	120	\N	N	Unveröff		\N		\N	unpublished	2016-05-04 11:07:26.396	\N	2008	\N	\N	f
1979	Knapp_1944_000721	\N	\N	\N	\N	\N	Vegetationsaufnahmen von Wäldern des Mitteldeutschen Trocken-Gebietes	000721		\N	\N	unpubliziert		\N	\N	\N	53	\N	Y	Mskr. Halle (Saale). 20 S		\N		\N	unpublished	2016-05-04 11:07:26.846	\N	1944	\N	\N	f
1991	Paul_2003_000725	\N	\N	\N	\N	\N	Die Vegetation in Kalkbuchenwäldern in Abhängigkeit von Standort und forstlicher Nutzung	000725		\N	\N	\N		\N	TV	\N	277	\N	Y			\N	Diss. Universität Freiburg. 257 S	\N	thesis	2016-05-04 11:07:27.784	\N	2003	\N	\N	f
2014	Messerschmidt_1985_000733	\N	\N	\N	\N	\N	Zum geschichtlichen Waldbild des Westharzes und den potentiuell - natürlichen Bäumen und Sträuchern der Betriebsklasse "Harzhochlagen"	000733		\N	\N	\N		\N	\N	\N	34	\N	Y			\N	Dipl.-Arb. Forstwiss. FB, Universität Göttingen. 71 S	\N	thesis	2016-05-04 11:07:29.667	\N	1985	\N	\N	f
2096	Woellner_1987_000763	\N	\N	\N	\N	\N	Die Naturwaldzelle "Mörderhäufel" - Standort, Vegetation und Waldentwicklung	000763		\N	\N	\N		\N	\N	\N	28	\N	N			\N	Dipl.-Arb. Institut f. Waldbau, Abt. I, Universität Göttingen. 68 S	\N	thesis	2016-05-04 11:07:37.266	\N	1987	\N	\N	f
2040	Meisberger_2003_000743	\N	\N	\N	\N	\N	Pflanzensoziologische Typisierung der Kalkhalbtrockenrasen des Bliesgaues	000743		\N	\N	\N		BLIESGAU	\N	\N	170	\N	N			\N	Dipl.-Arb. Universität Saarland, FB Geografie. Auch Anh. Delattinia 29: 131-146	\N	thesis	2016-05-04 11:07:32.248	\N	2003	\N	\N	f
2052	Jackel_1999_000748	\N	\N	\N	\N	\N	Strategien der Pflanzenarten einer fragmentierten Trockenrasengesellschaft. Vegetationskundliche und populationsbiologische Untersuchungen im Thymo-Festucetum cinereae	000748		\N	\N	\N		\N	\N	\N	8	\N	Y	Diss. Bot. 309: 213 S	Diss. Bot. 309: 213 S	\N		\N	book	2016-05-04 11:07:33.521	\N	1999	\N	\N	f
2073	Diekjobst_1960_000755	\N	\N	\N	\N	\N	Der Halbtrockenrasenkomplex und seine Genese in den Beckumer Bergen	000755		\N	\N	\N		\N	TV	\N	63	\N	Y			\N	Staastexamensarbeit Bot. Institut Universität Münster. 136 S	\N	thesis	2016-05-04 11:07:35.374	\N	1960	\N	\N	f
2079	Krause_1953_000757	\N	\N	\N	\N	\N	Unveröff. Originalaufnahmen zu Heiden im Südschwarzwald	000757		\N	\N	unpubliziert		\N	TV	\N	86	\N	Y	Staatliche Versuchsanstalt f. Grünlandwirtschaft u. Futterbau, Abt. Höhenlandwirtschaft Donaueschingen. Zitiert in: Bode, F. (2005): Diss. Universität Freiburg		1. pr�fen,		\N	unpublished	2016-05-04 11:07:35.837	\N	1953	\N	\N	f
2081	Krause_1953_000758	\N	\N	\N	\N	\N	Unveröff. Originalaufnahmen zu Weiden im Südschwarzwald	000758		\N	\N	unpubliziert		\N	TV	\N	81	\N	Y	Staatliche Versuchsanstalt f. Grünlandwirtschaft u. Futterbau, Abt. Höhenlandwirtschaft Donaueschingen. Zitiert in: Bode, F. (2005): Diss. Universität Freiburg		2. pr�fen,		\N	unpublished	2016-05-04 11:07:36.044	\N	1953	\N	\N	f
2084	Biederbick_1991_000759	\N	\N	\N	\N	\N	Untersuchungen zur reliefbedingten Variation von Vegetation und Standort	000759		\N	\N	\N		\N	TV	\N	18	\N	Y	Diss. Bot. 176: Berlin, Stuttgart. 189 S	Diss. Bot. 176: Berlin, Stuttgart. 189 S	\N		\N	book	2016-05-04 11:07:36.288	\N	1991	\N	\N	f
2093	Goebel_1995_000762	\N	\N	\N	\N	\N	Die Vegetation der Wiesen, Magerrasen und Rieder im Rhein-Main-Gebiet	000762		\N	\N	\N		\N	\N	\N	1239	\N	Y	Diss. Bot. 237: 1-456	Diss. Bot. 237: 1-456	\N		\N	book	2016-05-04 11:07:37.064	\N	1995	\N	\N	f
2104	Duty_1956_000766	\N	\N	\N	\N	\N	Pulsatilla vulgaris MILL. s.l. Untersuchungen zur Systematik und Verbreitung im mitteldeutschen Raum	000766		\N	\N	\N		\N	\N	\N	16	\N	Y			\N	Dipl.-Arb. Universität Halle. 181 S	\N	thesis	2016-05-04 11:07:37.979	\N	1956	\N	\N	f
2107	Merkel_1979_000767	\N	\N	\N	\N	\N	Die Vegetation des Messtischblattes 6434 Hersbruck	000767		\N	\N	\N		\N	\N	\N	374	\N	Y	Diss. Bot. 51: 1-174	Diss. Bot. 51: 1-174	\N		\N	book	2016-05-04 11:07:38.189	\N	1979	\N	\N	f
2110	Kaiser_1997_000768	\N	\N	\N	\N	\N	Vegetationsaufnahmen aus dem Naturschutzgebiet Lüneburger Heide	000768		\N	\N	\N		\N	\N	\N	233	\N	Y	Floristische Notizen aus d. Lüneburger Heide. Beih. 1: 67 S	Floristische Notizen aus d. Lüneburger Heide. Beih. 1: 67 S	\N		\N	book	2016-05-04 11:07:38.39	\N	1997	\N	\N	f
2124	Faber_1933_000773	\N	\N	\N	\N	\N	Pflanzensoziologische Untersuchung in Süddeutschland. Über Waldgesellschaften in Württemberg	000773		\N	\N	\N		\N	\N	\N	99	\N	Y			\N	Diss. Naturwissenschaftliches Fak. Universität Tübingen. Schweizerbarth, Stuttgart. 68 S	\N	thesis	2016-05-04 11:07:39.522	\N	1933	\N	\N	f
2115	Goennert_1989_000770	\N	\N	\N	\N	\N	Ökologische Bedingungen verschiedener Laubwaldgesellschaften des Nordwestdeutschen Tieflandes	000770		\N	\N	\N		\N	\N	\N	56	\N	Y	Diss. Bot. 136: 200 S. Berlin. (entspricht Diss. Universität Göttingen 1988	Diss. Bot. 136: 200 S. Berlin. (entspricht Diss. Universität Göttingen 1988	\N		\N	book	2016-05-04 11:07:38.842	\N	1989	\N	\N	f
2131	Braun_1968_000775	\N	\N	\N	\N	\N	Die Kalkflachmoore und ihre wichtigsten Kontaktgesellschaften im Bayerischen Alpenvorland	000775		\N	\N	\N		\N	\N	\N	525	\N	Y	Diss. Bot. 1: Lehre	Diss. Bot. 1: Lehre	\N		\N	book	2016-05-04 11:07:40.092	\N	1968	\N	\N	f
2177	Nebel_1986_000792	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen in Hohenlohe	000792		\N	\N	\N		\N	\N	\N	285	\N	Y	Diss. Bot. 97: 253 S. Berlin, Stuttgart	Diss. Bot. 97: 253 S. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:07:44.119	\N	1986	\N	\N	f
2188	Meyer_1985_000796	\N	\N	\N	\N	\N	Die Flora und Vegetation des NSG "Hamm- und Puddemeer" und seiner Umgebung	000796		\N	\N	\N		\N	\N	\N	40	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 205. S	\N	thesis	2016-05-04 11:07:44.999	\N	1985	\N	\N	f
2202	Peltzer_1996_000801	\N	\N	\N	\N	\N	Vegetation im Naturwaldreservat Mittlere Ith	000801		\N	\N	\N		Naturwald	Ith	\N	122	\N	N			\N	Dipl.-Arb. Institut f. Waldbau, Abt. I, Universität Göttingen	\N	thesis	2016-05-04 11:07:46.175	\N	1996	\N	\N	f
2205	Lemke_2001_000802	\N	\N	\N	\N	\N	Flora und Vegetation im Naturwald Königsbuche	000802		\N	\N	\N		Naturwald	K�nigsbuche	\N	123	\N	N			\N	Dipl.-Arb. Institut f. Waldbau, Abt. I, Universität Göttingen	\N	thesis	2016-05-04 11:07:46.42	\N	2001	\N	\N	f
2214	Schnelle_1992_000805	\N	\N	\N	\N	\N	Gutachten Nedlitzer Niederung	000805		\N	\N	\N		\N	\N	\N	100	\N	Y			\N		\N	report	2016-05-04 11:07:47.191	\N	1992	2215	\N	f
2222	Abdank_1995_000807	\N	\N	\N	\N	\N	Struktur und Wandel der Vegetation im östlichen Teil der Elster-Luppe-Aue im Verlauf der vergangenen vier Jahrzehnte	000807		\N	\N	\N		\N	\N	\N	106	\N	Y			\N	Dipl.-Arb. Mskr. Universität Halle	\N	thesis	2016-05-04 11:07:47.701	\N	1995	\N	\N	f
2228	Walther_1958_000810	\N	\N	\N	\N	\N	Pflanzensoziologische und landwirtschaftliche Ertragsermittlungen in den Grünland-Gesellschaften der Staustufe Langwedel	000810		\N	\N	\N		\N	\N	\N	105	\N	Y			\N		\N	report	2016-05-04 11:07:48.423	\N	1958	2229	\N	f
2237	Ostermann_1989_000813	\N	\N	\N	\N	\N	Floristisch-vegetationskundliche Untersuchungen zur Auswirkung der Schafbeweidung auf brachgefallene Kalkmagerrasen am Beispiel des Naturschutzgebietes "Alendorfer Kaltriften"/Eifel	000813		\N	\N	\N		\N	\N	\N	123	\N	Y			\N	Hausarbeit landwirtsch. Fak., Universität Bonn	\N	thesis	2016-05-04 11:07:49.189	\N	1989	\N	\N	f
2242	Wahrenburg_1986_000815	\N	\N	\N	\N	\N	Vegetation der Wiesentäler des Goldersbaches und seiner Zuflüsse im südlichen Schönbuch bei Tübingen	000815		\N	\N	\N		\N	\N	\N	1	\N	Y			\N	Dipl.-Arb. Universität Göttingen. 83 S	\N	thesis	2016-05-04 11:07:49.652	\N	1986	\N	\N	f
2304	Vigano_1997_000843	\N	\N	\N	\N	\N	Grünlandgesellschaften im Rothaargebirge im Beziehungsgefüge geoökologischer Prozeßgrößen	000843		\N	\N	\N		\N	\N	\N	352	\N	Y	Diss. Bot. 275: 1-212. Berlin	Diss. Bot. 275: 1-212. Berlin	\N		\N	book	2016-05-04 11:07:56.045	\N	1997	\N	\N	f
2311	Roll_1938_000846	\N	\N	\N	\N	\N	Quellvegetation und Pflanzensoziologie	000846		\N	\N	\N		\N	\N	\N	0	\N	Y	Forschungen u. Fortschritte 14, Nr. 30. Berlin	Forschungen u. Fortschritte 14, Nr. 30. Berlin	\N		\N	book	2016-05-04 11:07:56.68	\N	1938	\N	\N	f
2331	Broecker_1987_000854	\N	\N	\N	\N	\N	Die Ufervegetation der Fließgewässer zwischen Leine und Weser	000854		Auch Philippia 6: 271-312 - 1993	\N	\N		\N	\N	\N	20	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. (Auch Philippia 6: 271-312 - 1993	\N	thesis	2016-05-04 11:07:58.691	\N	1987	\N	\N	f
2320	Meyer_1984_000850	\N	\N	\N	\N	\N	Flora und Vegetation der Waldquellsümpfe auf Buntsandstein	000850		\N	\N	\N		\N	\N	\N	141	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 84 S	\N	thesis	2016-05-04 11:07:57.647	\N	1984	\N	\N	f
2337	Hagen_1996_000856	\N	\N	\N	\N	\N	Vegetationsveränderungen in Kalk-Magerrasen des Fränkischen Jura	000856		\N	\N	\N		\N	\N	\N	227	\N	Y	Laufener Forschungsbericht 4. Bayerische Akademie f. Naturschutz u. Landschaftspflege, Laufen, Salzach. 218 S	Laufener Forschungsbericht 4. Bayerische Akademie f. Naturschutz u. Landschaftspflege, Laufen, Salzach. 218 S	\N		\N	book	2016-05-04 11:07:59.22	\N	1996	\N	\N	f
2342	Suck_1991_000858	\N	\N	\N	\N	\N	Beiträge zur Syntaxonomie und Chorologie des Kalk-Buchenwaldes im ausseralpinen Deutschland	000858		\N	\N	\N		\N	\N	\N	567	\N	Y	Diss. Bot. 175: 1-212	Diss. Bot. 175: 1-212	\N		\N	book	2016-05-04 11:07:59.646	\N	1991	\N	\N	f
2345	Leyer_2002_000859	\N	\N	\N	\N	\N	Auengrünland der Mittelelbe-Niederung. Vegetationskundliche und -ökologische Untersuchungen in der rezenten Aue, Altaue und am Auenrand der Elbe	000859		\N	\N	\N		\N	\N	\N	487	\N	Y	Dissertationes Botanicae 363: 1-193	Dissertationes Botanicae 363: 1-193	\N		\N	book	2016-05-04 11:07:59.864	\N	2002	\N	\N	f
2352	Lohmeyer_1967_000862	\N	\N	\N	\N	\N	Über den Stieleichen-Hainbuchenwald des Kern-Münsterlandes und einige seiner Gehölz-Kontaktgesellschaften	000862		\N	\N	\N		\N	\N	\N	145	\N	Y	\N	Publisher	\N		\N	book	2016-05-04 11:08:00.572	\N	1967	\N	\N	f
2326	Luckwald_Et_Al._1992_000852	\N	\N	\N	\N	\N	Gutachten im Auftr. der Bezirksregierung Hannover, (Holzbergwiesen)	000852		\N	\N	unpubliziert		\N	\N	\N	15	\N	Y	Unveröff		\N		\N	unpublished	2016-05-04 11:07:58.179	\N	1992	\N	\N	f
2424	Kersting_1986_000887	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des unteren Schwarza- und Schlüchttales im Südostschwarzwald	000887		\N	\N	\N		HEINKEN_DIP	\N	\N	19	\N	N			\N	Dipl.-Arb. Universität Freiburg	\N	thesis	2016-05-04 11:08:06.69	\N	1986	\N	\N	f
2443	Marckardt_1995_000894	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen in den Kiefernforsten im Bereich des Neuendorfer Sees (Unterspreewald)	000894		\N	\N	\N		HEINKEN_DIP	\N	\N	100	\N	N			\N	Staatsex.-Arb. Universität Potsdam	\N	thesis	2016-05-04 11:08:08.44	\N	1995	\N	\N	f
2458	Aichinger_1933_000900	\N	\N	\N	\N	\N	Vegetationskunde der Karawanken	000900		\N	\N	\N		\N	\N	\N	0	\N	Y	Jena	Jena	\N		\N	book	2016-05-04 11:08:09.925	\N	1933	\N	\N	f
2461	Balazc_1941_000901	\N	\N	\N	\N	\N	?	000901		\N	\N	\N		\N	\N	\N	0	\N	Y	Acta geobot. hung. 4. Koloszvar	Acta geobot. hung. 4. Koloszvar	\N		\N	book	2016-05-04 11:08:10.188	\N	1941	\N	\N	f
2464	Braun-Blanquet_1964_000902	\N	\N	\N	\N	\N	Pflanzensoziologische und bodenkundliche Beobachtungen im Samnaun	000902		\N	\N	\N		\N	\N	\N	0	\N	Y	Jber. Naturforsch. Gesellschaft Graubündens 90, Chur	Jber. Naturforsch. Gesellschaft Graubündens 90, Chur	\N		\N	book	2016-05-04 11:08:10.483	\N	1964	\N	\N	f
2466	Braun-Blanquet_1926_000903	\N	\N	\N	\N	\N	Etudes phytosociologiques en Auvergne	000903		\N	\N	\N		\N	\N	\N	0	\N	Y	Zitat aus Oberdorfer, Band 1, Clermont-Ferrand, 94 S	Zitat aus Oberdorfer, Band 1, Clermont-Ferrand, 94 S	\N		\N	book	2016-05-04 11:08:10.701	\N	1926	\N	\N	f
2468	Braun-Blanquet_1930_000904	\N	\N	\N	\N	\N	Recherches Pytogeographiques sur le Massif du Gross Glockner (Hohe Tauern)	000904		\N	\N	\N		\N	\N	\N	0	\N	Y	Stat. Intern. Geobot. Medit. Alp. Montpellier, 13, Grenoble	Stat. Intern. Geobot. Medit. Alp. Montpellier, 13, Grenoble	\N		\N	book	2016-05-04 11:08:10.913	\N	1930	\N	\N	f
2472	Braun-Blanquet_1953_000906	\N	\N	\N	\N	\N	Essai sur la Vegetation du Mont Lozere comparee a Celle de L`Aigouàl	000906		\N	\N	\N		\N	\N	\N	0	\N	Y	Bull. Soc. Bot. France 100	Bull. Soc. Bot. France 100	\N		\N	book	2016-05-04 11:08:11.434	\N	1953	\N	\N	f
2474	Braun-Blanquet_1954_000907	\N	\N	\N	\N	\N	La Vegetation alpine et nivale des Alpes francais	000907		\N	\N	\N		\N	\N	\N	0	\N	Y	Stat. Intern. Geobot. Medit. Alp. Montpellier, Comm. 125., Bayeusaine	Stat. Intern. Geobot. Medit. Alp. Montpellier, Comm. 125., Bayeusaine	\N		\N	book	2016-05-04 11:08:11.69	\N	1954	\N	\N	f
2477	Dierschke_Lewejohann_1969_000908	\N	\N	\N	\N	\N	Alpenexkursion in das Fimbertal (Silvretta) vom 17.-31. Juli 1969	000908		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y	Unveröff. Bericht, 66 S., Göttingen		\N		\N	unpublished	2016-05-04 11:08:11.913	\N	1969	\N	\N	f
2480	Dutoit_1983_000909	\N	\N	\N	\N	\N	La Vetegetation de l∩etage subalpin du Vallon de Nant	000909		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	Universite de Lausanne, 122 S., Lausanne	\N	thesis	2016-05-04 11:08:12.137	\N	1983	\N	\N	f
2484	Giacomini_Pigniatti_1955_000910	\N	\N	\N	\N	\N	Flora e vegetatione dell ∩Alta Valle del Braulio	000910		\N	\N	\N		\N	\N	\N	0	\N	Y	Supplemento agli ATTI, Ser. 5, Vol. I, Pavia	Supplemento agli ATTI, Ser. 5, Vol. I, Pavia	\N		\N	book	2016-05-04 11:08:12.425	\N	1955	\N	\N	f
2488	Krausch_1933_000912	\N	\N	\N	\N	\N	?	000912		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y			\N		\N	unpublished	2016-05-04 11:08:12.85	\N	1933	\N	\N	f
2499	Szafer_Sokolowski_1927_000915	\N	\N	\N	\N	\N	Die Pflanzenassoziationen des Tatra-Gebirges - V. Teil: Die Pflanzenassoziationen der nördlich vom Giewont gelegenen Täler	000915		\N	\N	\N		\N	\N	\N	0	\N	Y	Bull. Int. l∩Academie Polonaise Sci. Lettr., Cl. Sci. Math. Nat., Ser. B, Cracovie	Bull. Int. l∩Academie Polonaise Sci. Lettr., Cl. Sci. Math. Nat., Ser. B, Cracovie	\N		\N	book	2016-05-04 11:08:13.555	\N	1927	\N	\N	f
2439	Loeffler_1996_000893	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen der Wälder im Nationalpark Sächsische Schweiz	000893		\N	\N	\N		HEINKEN_DIP	\N	\N	106	\N	N			\N		\N	report	2016-05-04 11:08:08.148	\N	1996	2440	\N	f
2496	Puscaru_Et_Al._1956_000914	\N	\N	\N	\N	\N	Pasunile alpine din Muntii Bucegi	000914		\N	\N	\N		\N	\N	\N	0	\N	Y	Acad. Rep. Pop. Romine, Institut de Cercetari Agron., Trattate Monografii 4, 512 S	Acad. Rep. Pop. Romine, Institut de Cercetari Agron., Trattate Monografii 4, 512 S	\N		\N	book	2016-05-04 11:08:13.337	\N	1956	\N	\N	f
2535	Hoelzel_1990_000929	\N	\N	\N	\N	\N	Vegetationsentwicklung auf Erosionsstandorten einer pleistozänen Talverfüllung im Lainbachtal bei Benediktbeuern/Obb.	000929		\N	\N	unpubliziert		\N	\N	\N	41	\N	Y	Unveröff. Dipl.-Arb. 121 S		\N		\N	unpublished	2016-05-04 11:08:16.932	\N	1990	\N	\N	f
2510	Ewald_1993_000919	\N	\N	\N	\N	\N	Unveröffentlichte Vegetationsaufnahmen aus den Mittleren Bayerischen Alpen (1992 und 1993)	000919		\N	\N	unpubliziert		\N	\N	\N	10	\N	Y	Unveröff		\N		\N	unpublished	2016-05-04 11:08:14.445	\N	1993	\N	\N	f
2512	Ewald_1995_000920	\N	\N	\N	\N	\N	Vegetationsaufnahmen aus dem FoA Kreuth 1993-1995	000920		\N	\N	unpubliziert		\N	\N	\N	373	\N	Y	Unveröff. Vegetationsaufnahmen		\N		\N	unpublished	2016-05-04 11:08:14.74	\N	1995	\N	\N	f
2514	Ewald_1997_000921	\N	\N	\N	\N	\N	Unveröffentlichte Vegetationsaufnahmen aus dem Werdenfelser Land	000921		\N	\N	unpubliziert		\N	\N	\N	96	\N	Y	Unveröff. Vegetationsaufnahmen		\N		\N	unpublished	2016-05-04 11:08:14.996	\N	1997	\N	\N	f
2525	Freiberg_1980_000925	\N	\N	\N	\N	\N	Pflanzensoziologische Untersuchungen im Bereich der alpinen Baumgrenze am Glunkerer im Funtenseegebiet, Nationalpark Berchtesgaden	000925		\N	\N	\N		\N	\N	\N	71	\N	Y			\N	Dipl.-Arb. LMU München. 43 S	\N	thesis	2016-05-04 11:08:15.967	\N	1980	\N	\N	f
2530	Herter_1990_000927	\N	\N	\N	\N	\N	Zur aktuellen Vegetation der Allgäuer Alpen: Die Pflanzengesellschaften des Hintersteiner Tales	000927		\N	\N	\N		\N	\N	\N	426	\N	Y	Diss. Bot. 147: 124 S	Diss. Bot. 147: 124 S	\N		\N	book	2016-05-04 11:08:16.472	\N	1990	\N	\N	f
2533	Hofmann_1997_000928	\N	\N	\N	\N	\N	Vegetations- und Standortentwicklung auf Dolomitschuttkegeln in der Langen Au (Tegernseer Alpen)	000928		\N	\N	\N		\N	\N	\N	95	\N	Y			\N	Dipl.-Arb. LMU München	\N	thesis	2016-05-04 11:08:16.683	\N	1997	\N	\N	f
2541	Ketterer_1998_000931	\N	\N	\N	\N	\N	Waldgesellschaften auf dem Bergsturz Eibsee-Grainau und ihre Standorte	000931		\N	\N	\N		\N	\N	\N	75	\N	Y			\N	Dipl.-Arb. LMU München. 76 S	\N	thesis	2016-05-04 11:08:17.35	\N	1998	\N	\N	f
2549	Mages_1994_000934	\N	\N	\N	\N	\N	Vegetationsaufnahmen aus der Muldenzone des FoA Kreuth 1994	000934		\N	\N	unpubliziert		\N	\N	\N	44	\N	Y	Unveröff. Vegetationsaufnahmen		\N		\N	unpublished	2016-05-04 11:08:18.067	\N	1994	\N	\N	f
2551	Michiels_1992_000935	\N	\N	\N	\N	\N	Die Stellung einiger Baum- und Straucharten in der Struktur und Dynamik der Vegetation im Bereich der hochmontanen und subalpinen Waldstufe der Bayerischen Kalkalpen	000935		\N	\N	\N		\N	\N	\N	285	\N	Y			\N	Diss. 313 S	\N	thesis	2016-05-04 11:08:18.32	\N	1992	\N	\N	f
2553	Michiels_1986_000936	\N	\N	\N	\N	\N	Vegetationstabelle NWR Wettersteinwald	000936		\N	\N	unpubliziert		\N	\N	\N	25	\N	Y	Unveröff. Vegetationstabelle		\N		\N	unpublished	2016-05-04 11:08:18.568	\N	1986	\N	\N	f
2538	Hoelzel_1994_000930	\N	\N	\N	\N	\N	Schneeheide-Kiefernwälder in den mittleren Nördlichen Kalkalpen	000930		\N	\N	\N		\N	\N	\N	213	\N	Y			\N	Diss. 218 S	\N	thesis	2016-05-04 11:08:17.143	\N	1994	\N	\N	f
2560	Puchner_1998_000940	\N	\N	\N	\N	\N	Wärmebegünstigte Bergwälder am Kramer bei Garmisch-Partenkirchen	000940		\N	\N	\N		\N	\N	\N	40	\N	Y			\N	Dipl.-Arb. LMU München. 84 S	\N	thesis	2016-05-04 11:08:19.28	\N	1998	\N	\N	f
2562	Seibert_Siede_1960_000941	\N	\N	\N	\N	\N	Unveröffentlichte Vegetationsaufnahmen vom nördlichen Alpenrand (Bayern)	000941		\N	\N	unpubliziert		\N	\N	\N	124	\N	Y	Unveröff. Vegetationsaufnahmen		\N		\N	unpublished	2016-05-04 11:08:19.507	\N	1960	\N	\N	f
2564	Siede_1959_000942	\N	\N	\N	\N	\N	Unveröffentlichte Vegetationsaufnahmen dem Ostertal/Allgäuer Alpen	000942		\N	\N	unpubliziert		\N	\N	\N	26	\N	Y	Unveröff. Vegetationsaufnahmen		\N		\N	unpublished	2016-05-04 11:08:19.806	\N	1959	\N	\N	f
2566	Siede_1960_000943	\N	\N	\N	\N	\N	Unveröffentlichte Vegetationsaufnahmen aus Buching-Trauchgau/Allgäu	000943		\N	\N	unpubliziert		\N	\N	\N	15	\N	Y	Unveröff. Vegetationsaufnahmen		\N		\N	unpublished	2016-05-04 11:08:20.03	\N	1960	\N	\N	f
2569	Simmerding_1980_000944	\N	\N	\N	\N	\N	Der Lärchen-Zirbenwald und die Strauchformationen der Reiteralm (Alpennationalpark Berchtesgaden)	000944		\N	\N	\N		\N	\N	\N	140	\N	Y			\N	Dipl.-Arb. LMU München. 59 S	\N	thesis	2016-05-04 11:08:20.276	\N	1980	\N	\N	f
2572	Still_1991_000945	\N	\N	\N	\N	\N	Die Pflanzengesellschaften am Wank und ihre Standorte	000945		\N	\N	\N		\N	\N	\N	272	\N	Y			\N	Diss. TU München. 150 S	\N	thesis	2016-05-04 11:08:20.519	\N	1991	\N	\N	f
2575	Storch_1978_000946	\N	\N	\N	\N	\N	Sind die Waldgesellschaften der nördlichen Kalkalpen pflanzensoziologisch kartierbar?	000946		\N	\N	unpubliziert		\N	\N	\N	167	\N	Y	Unveröff. Dipl.-Arb. 86 S		\N		\N	unpublished	2016-05-04 11:08:20.821	\N	1978	\N	\N	f
2577	Storch_1983_000947	\N	\N	\N	\N	\N	Zur floristischen Struktur der Pflanzengesellschaften in der Waldstufe des Nationalparks Berchtesgaden und ihre Abh"ngigkeit vom Standort und der Einwirkung des Menschen	000947		\N	\N	\N		\N	\N	\N	870	\N	Y			\N	Diss. LMU München. 407 S	\N	thesis	2016-05-04 11:08:21.032	\N	1983	\N	\N	f
2580	Thiele_1978_000948	\N	\N	\N	\N	\N	Vegetationskundliche und pflanzenökologische Untersuchungen im Wimbachgries	000948		\N	\N	\N		\N	\N	\N	69	\N	Y	Aus d. Naturschutzgebieten Bayerns. 73 S	Aus d. Naturschutzgebieten Bayerns. 73 S	\N		\N	book	2016-05-04 11:08:21.236	\N	1978	\N	\N	f
2586	Zanker_1995_000950	\N	\N	\N	\N	\N	Vegetationsaufnahmen aus der kalkalpinen Zone des FoA Kreuth	000950		\N	\N	unpubliziert		\N	\N	\N	46	\N	Y	Unveröff. Vegetationsaufnahmen		\N		\N	unpublished	2016-05-04 11:08:21.714	\N	1995	\N	\N	f
2610	Ferber_1994_000960	\N	\N	\N	\N	\N	?	000960		\N	\N	unpubliziert		\N	\N	\N	72	\N	Y	Transektaufnahmen		\N		\N	unpublished	2016-05-04 11:08:23.572	\N	1994	\N	\N	f
2604	Doerpinghaus_2006_000958	\N	\N	\N	\N	\N	?	000958		\N	\N	unpubliziert		\N	\N	\N	429	\N	Y	Feldbuch		\N		\N	unpublished	2016-05-04 11:08:23.119	\N	2006	\N	\N	f
2616	Guntermann_1999_000962	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen des Spreiberges bei Arnsberg (Müschede)	000962		\N	\N	\N		\N	\N	\N	18	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:24.034	\N	1999	\N	\N	f
2622	Hauswirth_2006_000964	\N	\N	\N	\N	\N	Jahresbericht über die Kontrolle der Bestände des Kriechenden Scheiberichs (Apium repens) im Jahre 2006 - Kreis Soest	000964		\N	\N	\N		\N	\N	\N	10	\N	Y			\N		\N	report	2016-05-04 11:08:24.525	\N	2006	2623	\N	f
2628	Hinterlang_1989_000966	\N	\N	\N	\N	\N	?	000966		\N	\N	unpubliziert		\N	\N	\N	28	\N	Y	Feldbuch		\N		\N	unpublished	2016-05-04 11:08:24.934	\N	1989	\N	\N	f
2631	Jaletztke_1993_000967	\N	\N	\N	\N	\N	Die Vegetation der Bockholter Berge bei Gimbte	000967		\N	\N	\N		\N	\N	\N	113	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:25.18	\N	1993	\N	\N	f
2634	Keller_2002_000968	\N	\N	\N	\N	\N	Der Einfluß einer kurzfristigen Beweidung auf die Vegetation des Heideweiergebietes im NSG "Heiliges Meer" bei Hopsten	000968		\N	\N	\N		\N	\N	\N	50	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:25.412	\N	2002	\N	\N	f
2637	Kleikamp_1994_000969	\N	\N	\N	\N	\N	?	000969		\N	\N	unpubliziert		\N	\N	\N	568	\N	Y	Feldbuch		\N		\N	unpublished	2016-05-04 11:08:25.655	\N	1994	\N	\N	f
2640	Lauber_2002_000970	\N	\N	\N	\N	\N	Vegetationkundliche Untersuchungen der Grünlandflächen im NSG "Fürstenkuhle"	000970		\N	\N	\N		\N	\N	\N	245	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:25.902	\N	2002	\N	\N	f
2646	Lisges_1996_000972	\N	\N	\N	\N	\N	Vegetationsökologische Untersuchungen im Zwischenmoor NSG "Giller" und angrenzender Heideflächen im Kreis Siegen-Wittgenstein	000972		\N	\N	\N		\N	\N	\N	123	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:26.38	\N	1996	\N	\N	f
2649	Poetschke_1997_000973	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen des NSG "Bleikuhlen" bei Blankenrode und der angrenzenden Abraumhalden	000973		\N	\N	\N		\N	\N	\N	60	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:26.666	\N	1997	\N	\N	f
2652	Poth_1995_000974	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen in der Nassen Wiese, NSG "Hunau", Hochsauerlandkreis	000974		\N	\N	\N		\N	\N	\N	190	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:26.868	\N	1995	\N	\N	f
2655	Ratai_1994_000975	\N	\N	\N	\N	\N	Vegetationsökologische Untersuchungen der Randbereiche und Kontaktzonen des NSG "Witte Venn", Kreis Borken	000975		\N	\N	\N		\N	\N	\N	93	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:27.077	\N	1994	\N	\N	f
2658	Schneiders_1999_000976	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen im Uferbereich einer Kiesgrube bei Burlo, Kreis Borken	000976		\N	\N	\N		\N	\N	\N	97	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:27.333	\N	1999	\N	\N	f
2607	Doersing_2004_000959	\N	\N	\N	\N	\N	Vegetationsökologische Untersuchungen und Biotoptypenkartierung des NSG "Haseniederung" (Kreis Steinfurt) zur Veränderung des Grünlandes und daraus resultierende Massnahmen des Naturschutzes	000959		\N	\N	\N		\N	\N	\N	108	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:23.327	\N	2004	\N	\N	f
2667	Tobias_1994_000979	\N	\N	\N	\N	\N	Vegetationsökologische Untersuchungen des Hündfelder Moores unter besonderer Berücksichtigung der bisher erfolgten Optimierungsmaßnahmen	000979		\N	\N	\N		\N	\N	\N	14	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:28.054	\N	1994	\N	\N	f
2677	Windisch_1996_000982	\N	\N	\N	\N	\N	Vegetationsökologische Untersuchungen extensiv bewirtschafteter Grünlandflächen im Gelängebachtal, Medebacher Bucht, Hochsauerlandkreis	000982		\N	\N	\N		\N	\N	\N	202	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:28.723	\N	1996	\N	\N	f
2680	Wittjen_1994_000983	\N	\N	\N	\N	\N	Vegetationsökologische Untersuchungen des Hündfelder Moores unter besonderer Berücksichtigung der Hochmoorvegetationsreste	000983		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:28.928	\N	1994	\N	\N	f
2695	Schaumann_2000_000989	\N	\N	\N	\N	\N	Endozoochorie durch Damhirsch (Cervus dama), Wildschwein (Sus scrofa) und Marder (Martes foina bzw. Martes martes) in bodensauren und mesophilen Wäldern Brandenburgs	000989		\N	\N	\N		HEINKEN_DIP	\N	\N	57	\N	N			\N	Dipl.-Arb. FU Berlin	\N	thesis	2016-05-04 11:08:30.534	\N	2000	\N	\N	f
2613	Gruener_1996_000961	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen des Schöppinger Berges	000961		\N	\N	\N		\N	\N	\N	155	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:23.777	\N	1996	\N	\N	f
2738	Krausch_1961_001007	\N	\N	\N	\N	\N	Unveröff. Vegetationsaufnahmen Niederlausitz & Spreewaldrand. Zusammengestellt von Thilo Heinken	001007		\N	\N	unpubliziert		\N	\N	\N	40	\N	N	Unveröff		\N		\N	unpublished	2016-05-04 11:08:35.063	\N	1961	\N	\N	f
2741	Beier_1993_001008	\N	\N	\N	\N	\N	Vergleichende vegetationskundliche Untersuchungen an Felsmassiven Westfalens und des niedersächsischen Wesergebietes	001008		\N	\N	\N		\N	\N	\N	187	\N	N			\N	Dipl.-Arb. Ruhr-Universität Bochum. 186 S	\N	thesis	2016-05-04 11:08:35.315	\N	1993	\N	\N	f
2744	Bruns_1992_001009	\N	\N	\N	\N	\N	Die Laubwaldgesellschaften am Lengenberg südwestlich von Heiligenstadt und im Revier Stein bei Bad Sooden-Allendorf	001009		\N	\N	\N		\N	\N	\N	63	\N	N			\N	Dipl.-Arb. Syst.-Geobot. Institut Universität Göttingen. 124 S	\N	thesis	2016-05-04 11:08:35.534	\N	1992	\N	\N	f
2749	Ebben_1984_001011	\N	\N	\N	\N	\N	Die Felsgrusfluren Nordhessens, ihr Aufbau und ihre Lebensbedingungen	001011		\N	\N	\N		\N	\N	\N	57	\N	N			\N	Staatsex.-Arb. GH Kasse. 110 S	\N	thesis	2016-05-04 11:08:36.039	\N	1984	\N	\N	f
2752	Henrion_1986_001012	\N	\N	\N	\N	\N	Biotopmanagementplan NSG "Steinbruch Helmke" -Iserlohn/Lethmathe	001012		\N	\N	\N		\N	\N	\N	11	\N	N			\N		\N	report	2016-05-04 11:08:36.244	\N	1986	2753	\N	f
2873	Sperle_Seifert_2010_001056	\N	\N	\N	\N	\N	2. Erfolgskontrolle des Projektgebietes "Filmersbach" (2008)	001056		\N	\N	\N		\N	\N	\N	13	\N	N			\N		\N	report	2016-05-04 11:08:46.923	\N	2010	2874	\N	f
2776	Plessmann_1989_001020	\N	\N	\N	\N	\N	Untersuchungen zur Morphologie und Verbreitung von Amelanchier ovalis MED. in Nordhessen	001020		\N	\N	\N		\N	\N	\N	49	\N	N			\N	Dipl.-Arb. Syst - Geobot. Institut Universität Göttingen: 86 S	\N	thesis	2016-05-04 11:08:38.049	\N	1989	\N	\N	f
2763	Leippert_1981_001016	\N	\N	\N	\N	\N	Gutachten über die Auswirkungen des Kletterns auf die Vegetation der Felsen im Hönnetal	001016		\N	\N	\N		\N	\N	\N	119	\N	N			\N		\N	report	2016-05-04 11:08:37.103	\N	1981	2764	\N	f
2767	Ortmanns_1995_001017	\N	\N	\N	\N	\N	Laubwald-Gesellschaften im nordwestlichen Landkreis Kassel	001017		\N	\N	\N		\N	\N	\N	23	\N	N			\N	Dipl.-Arb. Syst.-Geobot. Institut Universität Göttingen: 105 S	\N	thesis	2016-05-04 11:08:37.343	\N	1995	\N	\N	f
2773	Peters_1996_001019	\N	\N	\N	\N	\N	Untersuchungen zur Vegetationsdynamik des Bergsturzes am Schickeberg (Nordhessen)	001019		\N	\N	\N		\N	\N	\N	57	\N	N			\N	Dipl.-Arb. Syst.-Geobot. Institut Universität Göttingen: 100 S	\N	thesis	2016-05-04 11:08:37.802	\N	1996	\N	\N	f
2760	Koehler_1967_001015	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen der natürlichen Waldgesellschaften des oberen und mittleren Eichsfeldes und der Randgebiete des Thüringer Beckens	001015		\N	\N	\N		\N	\N	\N	24	\N	N			\N	Diss. Universität Halle. 266. S	\N	thesis	2016-05-04 11:08:36.858	\N	1967	\N	\N	f
2786	Schubert_1954_001024	\N	\N	\N	\N	\N	Aufnahmen von Waltraud Schubert, 1954	001024		\N	\N	unpubliziert		\N	\N	\N	164	\N	N	Unveröff. Mskr		\N		\N	unpublished	2016-05-04 11:08:38.978	\N	1954	\N	\N	f
2794	Winterhoff_1960_001028	\N	\N	\N	\N	\N	Die Waldgesellschaften des Göttinger Waldes	001028		\N	\N	\N		\N	\N	\N	105	\N	N			\N	Hausarb. Syst.-Geobot. Institut Universität Göttingen. 104 S	\N	thesis	2016-05-04 11:08:39.981	\N	1960	\N	\N	f
2826	Hartmann_1982_001040	\N	\N	\N	\N	\N	Vegetationskundliche Gliederungen der Laubwälder des Forstamtes Grünenplan (Hils-Bergland)	001040		\N	\N	\N		\N	\N	\N	62	\N	Y			\N	Staatsex.-Arb. Universität Göttingen: 59 S	\N	thesis	2016-05-04 11:08:42.907	\N	1982	\N	\N	f
2838	Detzel_Rtske_W._Krug_Ludemann_2005_001044	\N	\N	\N	\N	\N	Naturschutzgroßprojekt Feldberg-Belchen-Oberes Wiesental. Pflege- und Entwicklungsplan	001044		\N	\N	\N		\N	\N	\N	19	\N	Y			\N		\N	report	2016-05-04 11:08:43.863	\N	2005	2839	\N	f
2842	Schwabe-Braun_1979_001045	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Bannwaldes "Flüh" bei Schönau (Schwarzwald)	001045		\N	\N	\N		\N	\N	\N	87	\N	Y	"Waldschutzgebiete" im Rahmen der Mitteilungen der Forstlichen Versuchs- und Forschungsanstalt Baden-Württemberg Band 1: 1-68	"Waldschutzgebiete" im Rahmen der Mitteilungen der Forstlichen Versuchs- und Forschungsanstalt Baden-Württemberg Band 1: 1-68	\N		\N	book	2016-05-04 11:08:44.084	\N	1979	\N	\N	f
2845	Wagner_1999_001046	\N	\N	\N	\N	\N	Biozönologische Untersuchungen an sieben Halbtrockenrasenkomplexen im Raum Heidenheim (Baden-Württemberg: Ostablkreis)	001046		\N	\N	\N		\N	\N	\N	52	\N	Y			\N	Dipl.-Arb. Universität Ulm. 141 S	\N	thesis	2016-05-04 11:08:44.376	\N	1999	\N	\N	f
2847	Wagner_2003_001047	\N	\N	\N	\N	\N	Einnischungsmechanismen bei Rotwidderchen (Lepidoptera: Zygaenidae) auf Kalkmagerrasen der Schwäbischen Alb (Baden-Württemberg)	001047		\N	\N	\N		\N	\N	\N	7	\N	Y			\N	Diss. Universität Ulm. 146 S	\N	thesis	2016-05-04 11:08:44.64	\N	2003	\N	\N	f
2863	Flintrop_1997_001053	\N	\N	\N	\N	\N	Gutachten zur Schutzwürdigkeit des geplanten NSG "Dörlinbacher Grund/Münstergraben"	001053		\N	\N	\N		\N	\N	\N	30	\N	Y			\N		\N	report	2016-05-04 11:08:46.133	\N	1997	2864	\N	f
2867	Sperle_Seifert_2010_001054	\N	\N	\N	\N	\N	Pflege- und Entwicklungsplan des Projektgebietes " Altdorfer Ried" (2001/2005)	001054		\N	\N	\N		\N	\N	\N	3	\N	N			\N		\N	report	2016-05-04 11:08:46.451	\N	2010	2868	\N	f
2870	Sperle_Seifert_2010_001055	\N	\N	\N	\N	\N	2. Erfolgskontrolle des Projektgebietes "Altdorfer Ried" (2010)	001055		\N	\N	\N		\N	\N	\N	8	\N	N			\N		\N	report	2016-05-04 11:08:46.689	\N	2010	2871	\N	f
2876	Sperle_Seifert_2010_001057	\N	\N	\N	\N	\N	2. Erfolgskontrolle des Projektgebietes "Johannisberg" (2010)	001057		\N	\N	\N		\N	\N	\N	6	\N	N			\N		\N	report	2016-05-04 11:08:47.163	\N	2010	2877	\N	f
2879	Sperle_2004_001058	\N	\N	\N	\N	\N	FNP-Erweiterung Neuberg; Vergleich der Wiesen im Münstertal	001058		\N	\N	\N		\N	\N	\N	6	\N	N			\N		\N	report	2016-05-04 11:08:47.388	\N	2004	2880	\N	f
2882	Sperle_Seifert_2010_001059	\N	\N	\N	\N	\N	1. Erfolgskontrolle des Projektgebiets "Osterbach" (2005)	001059		\N	\N	\N		\N	\N	\N	3	\N	N			\N		\N	report	2016-05-04 11:08:47.655	\N	2010	2883	\N	f
2885	Sperle_Seifert_2010_001060	\N	\N	\N	\N	\N	2. Erfolgskontrolle mit Aufnahme neuer Maßnahmen im Projektgebiet "Osterbach" (2008)	001060		\N	\N	\N		\N	\N	\N	5	\N	N			\N		\N	report	2016-05-04 11:08:47.929	\N	2010	2886	\N	f
2892	Sperle_Seifert_2009_001062	\N	\N	\N	\N	\N	Erfolgskontrolle der Ausgleichsmaßnahmen im Projektgebiet "Rittmatten" im Jahr 2008	001062		\N	\N	\N		\N	\N	\N	18	\N	N			\N		\N	report	2016-05-04 11:08:48.486	\N	2009	2893	\N	f
2895	Sperle_Seifert_2007_001063	\N	\N	\N	\N	\N	1. Erfolgskontrolle des Projektgebiet "Stadthalde/ Heilbadquelle"	001063		\N	\N	\N		\N	\N	\N	8	\N	N			\N		\N	report	2016-05-04 11:08:48.76	\N	2007	2896	\N	f
2898	Sperle_Seifert_2010_001064	\N	\N	\N	\N	\N	3. Erfolgskontrolle der Maßnahmen im Teilgebiet "Stadthalde" des Projektgebietes "Stadthalde/ Heilbadquelle" (2010)	001064		\N	\N	\N		\N	\N	\N	21	\N	N			\N		\N	report	2016-05-04 11:08:49.04	\N	2010	2899	\N	f
2901	Sperle_Seifert_2004_001065	\N	\N	\N	\N	\N	3. Erfolgskontrolle und Abschlussbericht des 1. Maßnahmepaketes des Pflegeplanes "Teichanlage Ettenheimweiler"	001065		\N	\N	\N		\N	\N	\N	2	\N	N			\N		\N	report	2016-05-04 11:08:49.273	\N	2004	2902	\N	f
2907	Sperle_Seifert_2004_001067	\N	\N	\N	\N	\N	Vegetation der Probeflächen im Projektgebiet "Schambachtal" (2004)	001067		\N	\N	\N		\N	\N	\N	6	\N	N			\N		\N	report	2016-05-04 11:08:49.8	\N	2004	2908	\N	f
2910	Sperle_Seifert_2008_001068	\N	\N	\N	\N	\N	Vegetation der Probeflächen im Projektgebiet "Schambachtal" (2008)	001068		\N	\N	\N		\N	\N	\N	7	\N	N			\N		\N	report	2016-05-04 11:08:50.031	\N	2008	2911	\N	f
2913	Flintrop_2000_001069	\N	\N	\N	\N	\N	Erfassung gefährdeter Moosarten im Südschwarzwald und angrenzenden Gebieten sowie Entwicklung von Schutzkonzepten	001069		\N	\N	\N		\N	\N	\N	11	\N	N			\N		\N	report	2016-05-04 11:08:50.295	\N	2000	2914	\N	f
2858	Taeglich_1956_001051	\N	\N	\N	\N	\N	Die Wiesen- und Salpflanzengesellschaften der Elster-Luppe-Aue	001051		\N	\N	\N		\N	\N	\N	328	\N	Y			\N	Diss. Universität Halle	\N	thesis	2016-05-04 11:08:45.669	\N	1956	\N	\N	f
2919	Flintrop_Seifert_1995_001071	\N	\N	\N	\N	\N	Schutzwürdigkeitsgututachten für das geplante NSG "Taubenmoos"	001071		\N	\N	\N		\N	\N	\N	43	\N	N			\N		\N	report	2016-05-04 11:08:50.802	\N	1995	2920	\N	f
2922	Flintrop_Seifert_2000_001072	\N	\N	\N	\N	\N	Monitoring von Betula nana	001072		\N	\N	\N		\N	\N	\N	2	\N	N			\N		\N	report	2016-05-04 11:08:51.035	\N	2000	2923	\N	f
2932	Flintrop_Seifert_Harm_1992_001075	\N	\N	\N	\N	\N	Schutzwürdigkeitsgutachten für das gepl. NSG "Hirschbergwiesen bei Wickenrode"	001075		\N	\N	\N		\N	\N	\N	27	\N	N			\N		\N	report	2016-05-04 11:08:51.858	\N	1992	2933	\N	f
2935	Flintrop_Seifert_1993_001076	\N	\N	\N	\N	\N	Schutzwürdigkeitsgutachten für das gepl. NSG "Lichtenauer Hochland"	001076		\N	\N	\N		\N	\N	\N	90	\N	N			\N		\N	report	2016-05-04 11:08:52.092	\N	1993	2936	\N	f
2938	Flintrop_Seifert_Harm_1993_001077	\N	\N	\N	\N	\N	Schutzwürdigkeitsgutachten für das geplante NSG "Reichenbacher Kalkberge"	001077		\N	\N	\N		\N	\N	\N	34	\N	N			\N		\N	report	2016-05-04 11:08:52.329	\N	1993	2939	\N	f
2941	Flintrop_Seifert_1993_001078	\N	\N	\N	\N	\N	Schutzwürdigkeitsgutachten für das geplante NSG "Quellmulde Glimmerode"	001078		\N	\N	\N		\N	\N	\N	37	\N	N			\N		\N	report	2016-05-04 11:08:52.571	\N	1993	2942	\N	f
2944	Flintrop_Seifert_1994_001079	\N	\N	\N	\N	\N	Schutzwürdigkeitsgutachten für das geplante NSG "Sohlberg bei Lüderbach"	001079		\N	\N	\N		\N	\N	\N	8	\N	N			\N		\N	report	2016-05-04 11:08:52.842	\N	1994	2945	\N	f
2947	Flintrop_Seifert_1994_001080	\N	\N	\N	\N	\N	Pflegeplan für das NSG "Moor bei Wehrda"	001080		\N	\N	\N		\N	\N	\N	37	\N	N			\N		\N	report	2016-05-04 11:08:53.252	\N	1994	2948	\N	f
2950	Flintrop_Seifert_1995_001081	\N	\N	\N	\N	\N	Pflegeplan für das NSG "Quellgebiet der Weißen Gelster"	001081		\N	\N	\N		\N	\N	\N	21	\N	N			\N		\N	report	2016-05-04 11:08:53.528	\N	1995	2951	\N	f
2953	Flintrop_Seifert_1997_001082	\N	\N	\N	\N	\N	Dauerflächenuntersuchungen am Gottesberg bei Hundelshausen	001082		\N	\N	\N		\N	\N	\N	6	\N	N			\N		\N	report	2016-05-04 11:08:53.813	\N	1997	2954	\N	f
2956	Flintrop_Seifert_1997_001083	\N	\N	\N	\N	\N	Schutzwürdigkeitsgutachten für das geplante NSG "Rendagraben"	001083		\N	\N	\N		\N	\N	\N	29	\N	N			\N		\N	report	2016-05-04 11:08:54.101	\N	1997	2957	\N	f
2959	Flintrop_Seifert_1998_001084	\N	\N	\N	\N	\N	Pflegeplan für das NSG "Weißbachtal bei Reichenbach"	001084		\N	\N	\N		\N	\N	\N	28	\N	N			\N		\N	report	2016-05-04 11:08:54.364	\N	1998	2960	\N	f
2962	Flintrop_Seifert_1999_001085	\N	\N	\N	\N	\N	Schutzwürdigkeitsgutachten für das geplante NSG "Niedermoor unter∩m Eisberg bei Reichenbach"	001085		\N	\N	\N		\N	\N	\N	22	\N	N			\N		\N	report	2016-05-04 11:08:54.68	\N	1999	2963	\N	f
2965	Flintrop_Seifert_2000_001086	\N	\N	\N	\N	\N	Pflegeplan für das NSG "Der Bunte Berg bei Eberschütz"	001086		\N	\N	\N		\N	\N	\N	7	\N	N			\N		\N	report	2016-05-04 11:08:54.984	\N	2000	2966	\N	f
2968	Flintrop_Seifert_2000_001087	\N	\N	\N	\N	\N	Pflegeplan für das NSG "Schottenbruchbei Niedermeiser"	001087		\N	\N	\N		\N	\N	\N	13	\N	N			\N		\N	report	2016-05-04 11:08:55.218	\N	2000	2969	\N	f
2971	Flintrop_Seifert_2000_001088	\N	\N	\N	\N	\N	Pflegeplan für das NSG "Meßhagen bei Niedermeiser"	001088		\N	\N	\N		\N	\N	\N	3	\N	N			\N		\N	report	2016-05-04 11:08:55.459	\N	2000	2972	\N	f
2974	Flintrop_Andres_2001_001089	\N	\N	\N	\N	\N	Grunddatenerfassung des FFH-Gebietes "Quellgebiet bei Oberkaufungen"	001089		\N	\N	\N		\N	\N	\N	6	\N	N			\N		\N	report	2016-05-04 11:08:55.729	\N	2001	2975	\N	f
2977	Flintrop_Andres_2001_001090	\N	\N	\N	\N	\N	Grunddatenerfassung des FFH-Gebietes "Kalksumpf bei Motzfeld"	001090		\N	\N	\N		\N	\N	\N	4	\N	N			\N		\N	report	2016-05-04 11:08:55.963	\N	2001	2978	\N	f
2980	Flintrop_Andres_2001_001091	\N	\N	\N	\N	\N	Grunddatenerfassung des FFH-Gebietes "Kalksumpf bei Oberlengsfeld"	001091		\N	\N	\N		\N	\N	\N	3	\N	N			\N		\N	report	2016-05-04 11:08:56.231	\N	2001	2981	\N	f
2983	Flintrop_Andres_Seifert_2001_001092	\N	\N	\N	\N	\N	Grunddatenerfassung des FFH-Gebietes "Lichtenauer Hochland"	001092		\N	\N	\N		\N	\N	\N	44	\N	N			\N		\N	report	2016-05-04 11:08:56.476	\N	2001	2984	\N	f
3008	Grafe_1967_001101	\N	\N	\N	\N	\N	Die Feuchtigkeitsverhältnisse unter den Wiesengesellschaften im östlichen Teil der Elster-Luppe-Aue	001101		\N	\N	\N		\N	\N	\N	48	\N	Y			\N	Dipl.-Arb. Universität Halle	\N	thesis	2016-05-04 11:08:58.814	\N	1967	\N	\N	f
3011	Heinken_2001_001102	\N	\N	\N	\N	\N	Vegetationsentwicklung von Auengrünland nach Wiederüberflutung	001102		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	Diss. Math.-Nat. Fak. Humboldt-Universität Berlin. 161 S	\N	thesis	2016-05-04 11:08:59.037	\N	2001	\N	\N	f
2929	Flintrop._T._Seifert_Harm_1992_001074	\N	\N	\N	\N	\N	Schutzwürdigkeitsgutachten für das geplante NSG "Mathesberg bei Wüstensachsen"	001074		\N	\N	\N		\N	\N	\N	25	\N	N			\N		\N	report	2016-05-04 11:08:51.579	\N	1992	2930	\N	f
3137	Grosse_1970_001146	\N	\N	\N	\N	\N	Floristische und pflanzensoziologische Untersuchungen im Bergholz bei Halle	001146		\N	\N	\N		LAU_ST	\N	\N	25	\N	N			\N	Dipl.-Arb. Universität Halle	\N	thesis	2016-05-04 11:09:09.974	\N	1970	\N	\N	f
3047	Krohne_1979_001114	\N	\N	\N	\N	\N	Laubmischwaldgesellschaften des Hildesheimer Waldes, der Harplage und der Niederen Berge	001114		\N	\N	\N		HEINKEN1	\N	\N	51	\N	Y			\N	Staatsex.-Arb. Univ. Göttingen: 68 S	\N	thesis	2016-05-04 11:09:02.128	\N	1979	\N	\N	f
3116	Boehnert_1998_001139	\N	\N	\N	\N	\N	unveröff. Vegetationsaufnahmen Wälder an der Unstrut für das LAU Sachsen Anhalt	001139		\N	\N	unpubliziert		LAU_ST	\N	\N	12	\N	N	Unveröff		\N		\N	unpublished	2016-05-04 11:09:08.347	\N	1998	\N	\N	f
3069	Rasmus_1992_001122	\N	\N	\N	\N	\N	Die Verbreitung der Rotbuche (Fagus sylvatica) und ihre Vergesellschaftung im zentralen Fläming	001122		\N	\N	\N		HEINKEN2	\N	\N	101	\N	Y			\N	Staatsex.-Arb. FU Berlin	\N	thesis	2016-05-04 11:09:04.011	\N	1992	\N	\N	f
3075	Seewald_1977_001124	\N	\N	\N	\N	\N	Wald- und Gründlandgesellschaften im Drömling (Ostniedersachsen)	001124		\N	\N	\N		HEINKEN2	\N	\N	14	\N	Y	Diss. Bot. 41: 1-93. Vaduz	Diss. Bot. 41: 1-93. Vaduz	\N		\N	book	2016-05-04 11:09:04.797	\N	1977	\N	\N	f
3086	Vedder_1991_001128	\N	\N	\N	\N	\N	Die Waldgesellschaften der Täler des Oberbergischen Kreises	001128		\N	\N	\N		HEINKEN2	\N	\N	72	\N	Y			\N	Dipl. Arb. Syst.-Geobot. Inst. Univ. Göttingen: 119 S	\N	thesis	2016-05-04 11:09:05.799	\N	1991	\N	\N	f
3096	Plass_1960_001131	\N	\N	\N	\N	\N	Die Ackerunkrautgesellschaften des Gebietes nördlich von Halle (Meßtischblatt Halle Nord)	001131		\N	\N	\N		LAU_ST	\N	\N	103	\N	N			\N	Dipl.-Arb. Universität Halle, FB. Biologie. 129 S	\N	thesis	2016-05-04 11:09:06.511	\N	1960	\N	\N	f
3099	Schneider_1994_001132	\N	\N	\N	\N	\N	Die Ruderalvegetation des Ortes Halle-Dölau	001132		\N	\N	\N		LAU_ST	\N	\N	98	\N	N			\N	Wiss. Hausarb. Universität Halle	\N	thesis	2016-05-04 11:09:06.716	\N	1994	\N	\N	f
3101	Berg_1989_001133	\N	\N	\N	\N	\N	Geobotanische Studien an Straßen- und Wegrändern im Flach- und Hügelland der DDR	001133		\N	\N	\N		LAU_ST	\N	\N	142	\N	N			\N	Diss. Universität Halle. (enthält auch unveröff. Aufnahmen Mahn 1956	\N	thesis	2016-05-04 11:09:06.967	\N	1989	\N	\N	f
3106	Krausch_1984_001135	\N	\N	\N	\N	\N	Dauerbeobachtungsflächen im Naturschutzgebiet "Saalehänge bei Dobis"	001135		\N	\N	\N		LAU_ST	Krausch_1984	\N	10	\N	N			\N	Diplomarbeit Univ. Halle, 53 S	\N	thesis	2016-05-04 11:09:07.433	\N	1984	\N	\N	f
3119	Baumann_1995_001140	\N	\N	\N	\N	\N	Die Vegetation waldfreier Quellsümpfe in Teilbereichen des Unterharzes (Sachsen-Anhalt)	001140		\N	\N	\N		LAU_ST	\N	\N	187	\N	N			\N	Dipl.-Arb. Universität Göttingen, Albrecht-von-Haller-Inst. für Pflanzenwissenschaften. 140 S	\N	thesis	2016-05-04 11:09:08.599	\N	1995	\N	\N	f
3122	Federschmidt_1999_001141	\N	\N	\N	\N	\N	Unveröff. Vegetationsaufnahmen für LPR Reichhoff, i.A. des LAU Sachsen-Anhalt	001141		\N	\N	unpubliziert		LAU_ST	\N	\N	24	\N	N	Unveröff		\N		\N	unpublished	2016-05-04 11:09:08.808	\N	1999	\N	\N	f
3125	Kommraus_2008_001142	\N	\N	\N	\N	\N	Unveröff. Vegetationsaufnahmen Jurinea-Kartierung für das LAU Sachsen-Anhalt	001142		\N	\N	unpubliziert		LAU_ST	\N	\N	26	\N	N	Unveröff		\N		\N	unpublished	2016-05-04 11:09:09.016	\N	2008	\N	\N	f
3131	Lebender_1998_001144	\N	\N	\N	\N	\N	Vegetations- und standortkundliche Untersuchungen an naturschutzrelevanten Arten in Tagebaufolgelandschaften am Beispiel der Ophioglossaceaen als Grundlage für naturschutzfachliche Planungen	001144		\N	\N	\N		LAU_ST	\N	\N	9	\N	N			\N	Dipl.-Arb. FH Bernburg	\N	thesis	2016-05-04 11:09:09.429	\N	1998	\N	\N	f
3134	Lange_1967_001145	\N	\N	\N	\N	\N	Die Feuchtigkeitsverhältnisse unter den Wiesengesellschaften des westlichen Teiles der Elster-Luppe-Aue	001145		\N	\N	\N		LAU_ST	\N	\N	80	\N	N			\N	Dipl.-Arb. Universität Halle	\N	thesis	2016-05-04 11:09:09.687	\N	1967	\N	\N	f
3144	Reichhoff_1998_001149	\N	\N	\N	\N	\N	Unveröff. Vegetationsaufnahmen für das LAU Sachsen-Anhalt (LPR Landschaftsplanung)	001149		Vegetationsaufnahmen für das LAU Sachsen-Anhalt (LPR Landschaftsplanung).	\N	\N		LAU_ST	\N	\N	4	\N	N	Unveröff		\N		\N	unpublished	2016-05-04 11:09:10.693	\N	1998	\N	\N	f
3147	Schlag_1963_001150	\N	\N	\N	\N	\N	Die Halophytenvegetation der Salzstellen bei Hecklingen, Sülldorf und Artern	001150		\N	\N	\N		LAU_ST	\N	\N	33	\N	N			\N	Dipl.-Arb. Universität Halle	\N	thesis	2016-05-04 11:09:11.267	\N	1963	\N	\N	f
3149	Schnelle_1989_001151	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen in den Naturschutzgebieten Rahmbruch, Platzbruch und Rathsbruch am Südwestrand des Fläming 1988/89	001151		\N	\N	\N		LAU_ST	\N	\N	216	\N	N	Zerbst	Zerbst	\N		\N	book	2016-05-04 11:09:11.474	\N	1989	\N	\N	f
3152	Spangenberg_1994_001152	\N	\N	\N	\N	\N	Vegetationsgeographische Untersuchungen auf schwermetallhaltigen Abraumhalden des Sangerhäuser Reviers und der Mansfelder Mulde	001152		\N	\N	\N		LAU_ST	\N	\N	33	\N	N			\N	Dipl.-Arb. Universität Erlangen-Nürnberg. 58 S	\N	thesis	2016-05-04 11:09:11.725	\N	1994	\N	\N	f
3155	Weigl_1966_001153	\N	\N	\N	\N	\N	Unveröff. Vegetationsaufnahmen NSG "Borntal" bei Allstedt (Ludwig Weigl)	001153		Vegetationsaufnahmen NSG "Borntal" bei Allstedt (Ludwig Weigl).	\N	\N		LAU_ST	\N	\N	16	\N	N	Unveröff. LAU Sachsen-Anhalt		\N		\N	unpublished	2016-05-04 11:09:11.977	\N	1966	\N	\N	f
3166	Strahl_1987_001157	\N	\N	\N	\N	\N	Die Straßenbegleitvegetation im Spessart und Spessartvorland	001157		\N	\N	\N		\N	\N	\N	23	\N	Y			\N	Dipl.-Arb. Würzburg. 96 S	\N	thesis	2016-05-04 11:09:12.967	\N	1987	\N	\N	f
3205	Koelbel_1995_001172	\N	\N	\N	\N	\N	Feuchtgrünland-Vegetationstypen und die Charakterisierung ihrer Standorte in Schleswig-Holstein	001172		\N	\N	\N		\N	\N	\N	47	\N	Y			\N	Diss. Univ. Kiel, math.-nat. Fak. 198 S	\N	thesis	2016-05-04 11:09:17.018	\N	1995	\N	\N	f
3191	Baumann_2000_001166	\N	\N	\N	\N	\N	Vegetation und Ökologie der Kleinseggenriede des Harzes. Wissenschaftliche Grundlagen und Anwendungen im Naturschutz	001166		\N	\N	\N		\N	\N	\N	209	\N	Y			\N	Diss. Univ. Göttingen und Cuvillier Verlag Göttingen. 219 S	\N	thesis	2016-05-04 11:09:15.565	\N	2000	\N	\N	f
3197	Schmidt_Becker_1994_001169	\N	\N	\N	\N	\N	Vegetationskundliche Kontroll-Untersuchungen zur Pflege von Kalkmagerrasen im Landkreis Göttingen. Bericht für das Jahr 1994	001169		\N	\N	\N		\N	\N	\N	45	\N	N			\N		\N	report	2016-05-04 11:09:16.334	\N	1994	3198	\N	f
3200	Knapp_1944_001170	\N	\N	\N	\N	\N	Über Zwergstrauch-Heiden im Mitteldeutschen Trocken-Gebiet	001170		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y	Mskr. Halle (Saale). 6 S		\N		\N	unpublished	2016-05-04 11:09:16.552	\N	1944	\N	\N	f
3281	Schulze_1977_001197	\N	\N	\N	\N	\N	Standortkundliche Untersuchungen in Pflanzengesellschaften des mittleren Zschopautales zwischen Flöha und Frankenberg	001197		\N	\N	\N		Schulze_1977	\N	\N	39	\N	Y			\N	Diss. Universität Halle	\N	thesis	2016-05-04 11:09:23.049	\N	1977	\N	\N	f
3283	Kompa_1997_001198	\N	\N	\N	\N	\N	Grünlandvegetation in der Saale-Aue bei Holleben (Saalkreis) unter dem Einfluss unterschiedlicher Nutzungsgeschichte und -intensität	001198		\N	\N	\N		Kompa_1997	\N	\N	390	\N	N			\N	Dipl.-Arb.  MLU Halle-Wittenberg, 146 S	\N	thesis	2016-05-04 11:09:23.298	\N	1997	\N	\N	f
3286	Poschlod_1990_001199	\N	\N	\N	\N	\N	Vegetationsentwicklung in abgetorften Hochmooren des bayerischen Alpenvorlandes unter besonderer Berücksichtigung standortskundlicher und populationsbiologischer Faktoren	001199		\N	\N	\N		Poschlod_1990	\N	\N	644	\N	Y	Dissertationes Botanicae152: 1-331	Dissertationes Botanicae152: 1-331	\N		\N	book	2016-05-04 11:09:23.508	\N	1990	\N	\N	f
3289	Kleyer_1991_001200	\N	\N	\N	\N	\N	Die Vegetation linienförmiger Kleinstrukturen in Beziehung zur landwirtschaftlichen Produktionsintensität	001200		\N	\N	\N		Kleyer_1991	\N	\N	123	\N	Y	Dissertationes Botanicae 169: 1-142	Dissertationes Botanicae 169: 1-142	\N		\N	book	2016-05-04 11:09:23.713	\N	1991	\N	\N	f
3291	Klaege_1999_001201	\N	\N	\N	\N	\N	Segetalarten und -gesellschaften der nordwestlichen Lausitz und die Naturschutzstrategie zu ihrer Erhaltung	001201		\N	\N	\N		Klaege_1999	\N	\N	823	\N	Y	Dissertationes Botanicae 304: 1-142	Dissertationes Botanicae 304: 1-142	\N		\N	book	2016-05-04 11:09:23.964	\N	1999	\N	\N	f
3336	Weber_1997_001212	\N	\N	\N	\N	\N	Zur Vegetationsentwicklung auf brachgefallenen Weinbergen im Oberen Elbtal	001212		\N	\N	\N		\N	\N	\N	538	\N	N			\N	Dipl.-Arb. Geobotanik, Universität Halle. 119 S	\N	thesis	2016-05-04 11:09:26.84	\N	1997	\N	\N	f
3338	Gulich_1992_001213	\N	\N	\N	\N	\N	Die Vegetation der Umgebung des Goldberger Sees im Biosphärenreservat Mittlere Elbe bei Lödderitz	001213		\N	\N	\N		\N	\N	\N	0	\N	N			\N	Staatsex.-Arb. Geobotanik, Universität Halle	\N	thesis	2016-05-04 11:09:27.042	\N	1992	\N	\N	f
3341	Ehmer_1960_001214	\N	\N	\N	\N	\N	Die Vegetations-und Standortverhältnisse des Naturschutzgebietes Alperstedter Ried bei Erfurt, nebst Anhang	001214		\N	\N	\N		\N	\N	\N	0	\N	N			\N	Staatsex.-Arb. Geobotanik, Universität Halle	\N	thesis	2016-05-04 11:09:27.291	\N	1960	\N	\N	f
3344	Zuther_1977_001215	\N	\N	\N	\N	\N	Die Wiesenvegetation der Muldeaue zwischen Eilenburg und Dessau und ihre Beeinflussung durch die industrielle Luftverschmutzung	001215		\N	\N	\N		\N	\N	\N	0	\N	N			\N	Abschlussarbeit Geobotanik, Universität Halle	\N	thesis	2016-05-04 11:09:27.546	\N	1977	\N	\N	f
3347	Winter_1992_001216	\N	\N	\N	\N	\N	Vegetations-und standortkundliche Untersuchungen im unteren Saaletal bei Halle-Kröllwitz	001216		\N	\N	\N		\N	\N	\N	0	\N	N			\N	Abschlussarbeit Geobotanik, Universität Halle	\N	thesis	2016-05-04 11:09:27.761	\N	1992	\N	\N	f
3350	Zinner_1997_001217	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen auf den Porphyrhängen Nordwest-Sachsens aus naturschutzfachlicher Sicht	001217		\N	\N	\N		\N	\N	\N	112	\N	N			\N	Dipl.-Arb. Fachhochschule Anhalt, Abt. Bernburg, Landespflege. 147 S	\N	thesis	2016-05-04 11:09:28.016	\N	1997	\N	\N	f
3393	Hilbig_1958_001234	\N	\N	\N	\N	\N	Ackerunkrautgesellschaften im Gebiet zwischen Huy und Hakel	001234		\N	\N	\N		Hilbig_1958	\N	\N	165	\N	N			\N	Dipl.-Arb. Universität Halle	\N	thesis	2016-05-04 11:09:31.919	\N	1958	\N	\N	f
3435	Faber_1936_001248	\N	\N	\N	\N	\N	Über Waldgesellschaften auf Kalksteinböden und ihre Entwicklung im Schwäbisch-Fränkischen Stufenland und auf der Alb	001248		\N	\N	\N		Faber_1936	\N	\N	16	\N	Y	Versammlungsbericht 1936 der Landesgruppe Württemberg des Deutschen Forstvereins	Versammlungsbericht 1936 der Landesgruppe Württemberg des Deutschen Forstvereins	\N		\N	book	2016-05-04 11:09:34.921	\N	1936	\N	\N	f
3441	Matthews_1997_001250	\N	\N	\N	\N	\N	Pollenanalytische und pflanzensoziologische Untersuchungen in der Flußauenlandschaft der mittleren Elbe	001250		\N	\N	\N		Matthews_1997	\N	\N	340	\N	Y			\N	Diss. Fachber. Biologie Univ. Hannover, 213 S	\N	thesis	2016-05-04 11:09:35.401	\N	1997	\N	\N	f
3396	Jaeger_1998_001235	\N	\N	\N	\N	\N	Struktur und Dynamik von Weichholzauen im Bereich von mittlerer Elbe und unterer Mulde	001235		\N	\N	\N		J�ger_1998	\N	\N	63	\N	N			\N	Dipl.-Arb. Universität Halle	\N	thesis	2016-05-04 11:09:32.162	\N	1998	\N	\N	f
3483	Henning_2012_001265	\N	\N	\N	\N	\N	Die Frischwiesen (LRT 6510) des Biosphärenreservates. "Karstlandschaft Südharz" - Bodenkennwerte, Vegetationszusammensetzung und Futterwert	001265		\N	\N	\N		Henning_2012_S�dharz	\N	\N	0	\N	N			\N	Masterarbeit Hochschule Anhalt, 100 S	\N	thesis	2016-05-04 11:09:39.002	\N	2012	\N	\N	f
3495	Riegel_1991_001270	\N	\N	\N	\N	\N	Pflege- und Entwicklungsplan NSG "Stadtwald Augsburg" Kernzone Siebenbrunner Quellflur	001270		\N	\N	\N		Riegel_1991	\N	\N	94	\N	Y			\N		\N	report	2016-05-04 11:09:40.254	\N	1991	3496	\N	f
3502	Bfg_Koblenz_2009_001272	\N	\N	\N	\N	\N	Vegetationsaufnahmen an der Elbe bei Klöden	001272		\N	\N	\N		Elbe_Kl�den	\N	\N	229	\N	Y			\N		\N	report	2016-05-12 18:28:02.618	\N	2009	3503	\N	f
3506	Ludewig_1999_001273	\N	\N	\N	\N	\N	Direkte und indirekte anthropogene Beeinflussung und Veränderung der Ufervegetation der Saar	001273		\N	\N	\N		Ludewig_1999	\N	\N	657	\N	Y	Dissertationes Botanicae 301. 271 S	Dissertationes Botanicae 301. 271 S	\N		\N	book	2016-05-04 11:09:41.002	\N	1999	\N	\N	f
3509	Wagner_2000_001274	\N	\N	\N	\N	\N	Minerotrophe Bergkiefernmoore im süddeutschen Alpenvorland-Die Carex lasiocarpa-Pinus rotundata-Gesellschaft	001274		\N	\N	\N		Wagner_2000	\N	\N	192	\N	Y			\N	Dissertation Lehrstuhl für Vegetationsökologie,Technische Universität München. 185 S	\N	thesis	2016-05-04 11:09:41.296	\N	2000	\N	\N	f
3525	Arendt_1978_100001	\N	\N	\N	\N	\N	Makrophyten und Makrophytengesellschaften als Bioindikatoren in Fliessgewässern, dargestellt am Beispiel des Uecker- und Havelsystems	100001		\N	\N	\N		\N	\N	\N	60	60	Y			\N	Universität Greifswald, Diss. A	\N	thesis	2016-05-04 11:09:42.298	\N	1978	\N	\N	f
3537	Behrens_1982_100006	\N	\N	\N	\N	\N	Soziologische und produktionsbiologische Untersuchungen an den submersen Pflanzengesellschaften der Darss-Zingster Boddengewässer	100006		\N	\N	\N		\N	\N	\N	561	561	Y			\N	Universität Rostock, Diss. A	\N	thesis	2016-05-04 11:09:43.619	\N	1982	\N	\N	f
3540	Berg_1990_100007	\N	\N	\N	\N	\N	Geobotanische Studien an Straßen- und Wegrändern im Flach- und Hügelland der DDR	100007		\N	\N	\N		\N	\N	\N	186	186	Y			\N	Diss. A., Universität Halle	\N	thesis	2016-05-04 11:09:43.89	\N	1990	\N	\N	f
3581	Braeuer_1972_100024	\N	\N	\N	\N	\N	Vergleichende ökologische Untersuchungen an drei Ausbildungsformen des Grünlandes in der Grundmoräne des LPG-Bereiches Brodersdorf	100024		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	Universität Rostock, Diss. A	\N	thesis	2016-05-04 11:09:48.194	\N	1972	\N	\N	f
2596	Haerdtle_null_000953	\N	\N	\N	\N	\N	?	000953		\N	\N	unpubliziert		\N	\N	\N	7	\N	Y			\N		\N	unpublished	2016-05-04 11:08:22.404	\N	1500	\N	\N	t
3714	Holst_1968_100080	\N	\N	\N	\N	\N	Die Vegetationsverhältnisse des Wirtschaftsgrünlandes im Nebeltal	100080		\N	\N	\N		\N	\N	\N	561	561	Y			\N	Universität Rostock, Diss. A	\N	thesis	2016-05-04 11:10:02.994	\N	1968	\N	\N	f
3857	Klemm_1977_100143	\N	\N	\N	\N	\N	Vegetationskundlliche Studien in Hoch- und Flachmooren im Raume Moor (Kr. Grevesmühlen)	100143		\N	\N	\N		\N	\N	\N	475	475	Y			\N	Universität Rostock, Diss. A	\N	thesis	2016-05-04 11:10:18.991	\N	1977	\N	\N	f
3888	Knapp_Succow_1986_100159	\N	\N	\N	\N	\N	Gefährdete Pflanzengesellschaften auf dem Territorium der DDR	100159		\N	\N	\N		\N	\N	\N	3	3	Y	Kulturbund d. DDR	Kulturbund d. DDR	\N		\N	book	2016-05-04 11:10:22.563	\N	1986	\N	\N	f
3917	Krisch_1987_100173	\N	\N	\N	\N	\N	Die Vegetation der ostmecklenburgischen Boddenküste	100173		\N	\N	\N		\N	\N	\N	346	346	Y			\N	Universität Greifswald, Diss. B	\N	thesis	2016-05-04 11:10:25.978	\N	1987	\N	\N	f
3936	Kudoke_1976_100182	\N	\N	\N	\N	\N	Angewandt-vegetationskundliche und ökologische Untersuchungen in den Ackerfluren der Grundmoräne im Norden der DDR, dargestellt an Hand der Meßtischblätter des Rostocker Raumes	100182		\N	\N	\N		\N	\N	\N	110	110	Y			\N	Universität Rostock, Diss. B	\N	thesis	2016-05-04 11:10:28.107	\N	1976	\N	\N	f
3960	Lohr_1985_100192	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen im Wirtschaftsgrünland des Unteren Recknitztales zwischen Marlow und Ribnitz-Damgarten	100192		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	Universität Rostock, Diss. A	\N	thesis	2016-05-04 11:10:30.549	\N	1985	\N	\N	f
4048	Ribbe_1971_100232	\N	\N	\N	\N	\N	Die Vegetationsverhältnisse im Wirtschaftsgrünland der Lewitz	100232		\N	\N	\N		\N	\N	\N	931	931	Y			\N	Universität Rostock, Diss. A	\N	thesis	2016-05-04 11:10:40.157	\N	1971	\N	\N	f
4070	Schmidt_1987_100244	\N	\N	\N	\N	\N	Über Flora und Vegetation in der Umgebung des Düngemittelwerkes Rostock-Poppendorf vor dessen Inbetriebnahme	100244		\N	\N	\N		\N	\N	\N	221	221	Y			\N	Universität Rostock, Diss. A	\N	thesis	2016-05-04 11:10:42.472	\N	1987	\N	\N	f
4082	Slobodda_1977_100248	\N	\N	\N	\N	\N	Untersuchungen zur Ökologie und Stoffproduktion ausgewählter Niedermoor- und Feuchtwiesen- Pflanzengesellschafen im NSG "Peenewiesen" bei Gützkow	100248		\N	\N	\N		\N	\N	\N	160	160	Y			\N	Universität Greifswald, Diss. A	\N	thesis	2016-05-04 11:10:43.389	\N	1977	\N	\N	f
4125	Succow_1988_100268	\N	\N	\N	\N	\N	Landschaftsökologische Moorkunde	100268		\N	\N	\N		\N	\N	\N	0	\N	Y	340 S. Fischer, Jena	340 S. Fischer, Jena	\N		\N	book	2016-05-04 11:10:48.461	\N	1988	\N	\N	f
4263	Bohlke_Hansen_1993_100324	\N	\N	\N	\N	\N	Entwicklungskonzept Untere Sude zwischen Besitz und Boizenburg	100324		\N	\N	\N		\N	\N	\N	111	111	Y			\N		\N	report	2016-05-04 11:11:02.895	\N	1993	4264	\N	f
4677	Scheller_Voigtlaender_1995_100457	\N	\N	\N	\N	\N	Pflege- und Entwicklungsplan für das Naturschutzgebiet "Heiligensee und Hütelmoor"	100457		\N	\N	\N		\N	\N	\N	151	151	Y			\N		\N	report	2016-05-04 11:11:36.606	\N	1995	4678	\N	f
4661	Hurtig_1963_100452	\N	\N	\N	\N	\N	Beiträge zur Kenntnis von Waldvegetation und Waldstandorten im Gebiet der ebenen Grundmoräne Nordostmecklenburgs als Grundlage für die Waldbauliche Planung	100452		\N	\N	\N		\N	\N	\N	251	251	Y			\N	Universität Berlin, Forstwirtschaft Eberswalde, Diss	\N	thesis	2016-05-04 11:11:35.373	\N	1963	\N	\N	f
4663	Bolbrinker_1997_100453	\N	\N	\N	\N	\N	Pflege- und Entwicklungsplan für das Naturschutzgebiet "Binsenbrink-Sauerwerder"	100453		\N	\N	\N		\N	\N	\N	84	84	Y			\N		\N	report	2016-05-04 11:11:35.589	\N	1997	4664	\N	f
4668	Rowinsky_Neumann_1996_100454	\N	\N	\N	\N	\N	Moorkundliche Gutachten "Plauer Stadtwald"	100454		\N	\N	\N		\N	\N	\N	7	7	Y			\N		\N	report	2016-05-04 11:11:35.81	\N	1996	4669	\N	f
4674	Uhle_1996_100456	\N	\N	\N	\N	\N	Effizienzuntersuchungen für naturschutzgerechte Grünlandnutzung Freesendorfer Wiesen/Struck	100456		\N	\N	\N		\N	\N	\N	64	64	Y			\N		\N	report	2016-05-04 11:11:36.385	\N	1996	4675	\N	f
4690	Schmiedel_1996_100460	\N	\N	\N	\N	\N	Naturschutzfachliches Rahmenkonzept für das geplante Naturschutzgebiet Moorheide bei Melkof im Naturpark Elbetal	100460		\N	\N	\N		\N	\N	\N	16	16	Y			\N		\N	report	2016-05-04 11:11:37.352	\N	1996	4691	\N	f
4707	Paulson_Raskin_1998_100467	\N	\N	\N	\N	\N	Vegetationskundlicher Teil des Pflege- und Entwicklungsplanes Ostrügensche Boddenlandschaft	100467		\N	\N	\N		\N	\N	\N	1413	1413	Y			\N		\N	report	2016-05-04 11:11:39.079	\N	1998	4708	\N	f
4710	Wollert_1993_100468	\N	\N	\N	\N	\N	Flora und Vegetation des Kummerower Beckens und des Peenetals zwischen Salem und Upost	100468		\N	\N	unpubliziert		\N	\N	\N	681	681	Y	\N		\N		\N	unpublished	2016-05-04 11:11:39.359	\N	1993	\N	\N	f
4712	Wollert_1992_100469	\N	\N	\N	\N	\N	Zur Flora und Vegetation des Tals der Oberwarnow bei Schwaan	100469		\N	\N	unpubliziert		\N	\N	\N	299	299	Y	\N		\N		\N	unpublished	2016-05-04 11:11:39.643	\N	1992	\N	\N	f
4656	Mordhorst_Et_Al._1997_100450	\N	\N	\N	\N	\N	Pflege- und Entwicklungsplan Naturpark Schaalsee-Landschaft	100450		\N	\N	\N		\N	\N	\N	515	515	Y			\N		\N	report	2016-05-04 11:11:34.746	\N	1997	4657	\N	f
4259	Bluemel_Teppke_1996_100323	\N	\N	\N	\N	\N	Seentypen in Mecklenburg-Vorpommern - eine vegetationsökologische Bestandsaufnahme und Zustandserfassung an ausgewählten Beispielen	100323		\N	\N	\N		\N	\N	\N	446	446	Y			\N	Dipl.-Arb. Bot. Institut, Universität Greifswald	\N	thesis	2016-05-04 11:11:02.644	\N	1996	\N	\N	f
4247	Berg_1993_100320	\N	\N	\N	\N	\N	Effizienzkontrolle zur naturschutzgerechten Grünlandnutzung in der Lewitz (3 Berichte)	100320		\N	\N	\N		\N	\N	\N	73	73	Y			\N		\N	report	2016-05-04 11:11:01.921	\N	1993	4248	\N	f
4251	Berg_Thiess_1992_100321	\N	\N	\N	\N	\N	Pflege- und Entwicklungskonzept für das Mirower Torfmoor	100321		\N	\N	\N		\N	\N	\N	76	76	Y			\N		\N	report	2016-05-04 11:11:02.132	\N	1992	4252	\N	f
4254	Berg_Thiess_1993_100322	\N	\N	\N	\N	\N	Pflege- und Entwicklungsplan "Wakenitzniederung". Teil 1	100322		\N	\N	\N		\N	\N	\N	131	131	Y			\N		\N	report	2016-05-04 11:11:02.349	\N	1993	4255	\N	f
4266	Bolbrinker_1992_100325	\N	\N	\N	\N	\N	Abschnittsbericht 3. Trebel und Trebelkanal südlich Tribsees. Vegetationskartierung	100325		\N	\N	\N		\N	\N	\N	320	320	Y			\N		\N	report	2016-05-04 11:11:03.123	\N	1992	4267	\N	f
4269	Bolbrinker_1992_100326	\N	\N	\N	\N	\N	Abschnittsbericht 4. Tal der Blinden Trebel nordöstlich Tribsees, Vegetationskartierung	100326		\N	\N	\N		\N	\N	\N	201	201	Y			\N		\N	report	2016-05-04 11:11:03.349	\N	1992	4270	\N	f
4274	Bolman_Nijhof_1993_100327	\N	\N	\N	\N	\N	De vegetatiekartering van het Peenedal ten noorden van Anklam op basis vanluchtfoto- interpretatie en vegetatie-opnamen	100327		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	Dipl.-Arb. Universität Groningen	\N	thesis	2016-05-04 11:11:03.621	\N	1993	\N	\N	f
4277	Brandt_1993_100328	\N	\N	\N	\N	\N	NSG "Moorer Busch": Entstehung, Biotopzustand und Konzept zur Pflege und Entwicklung eines Bruchwald-Moorgebietes	100328		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	Dipl.-Arb. Humboldt-Universität Berlin	\N	thesis	2016-05-04 11:11:03.875	\N	1993	\N	\N	f
4279	Brandt_1993_100329	\N	\N	\N	\N	\N	Hydrologische und vegetationskundliche Kartierung der Regenmoore Mecklenburg und Vorpommerns. Teilbericht	100329		\N	\N	\N		\N	\N	\N	246	246	Y			\N		\N	report	2016-05-04 11:11:04.122	\N	1993	4280	\N	f
4283	Brielmann_1994_100330	\N	\N	\N	\N	\N	Untersuchungen der Pflanzen, Tiere und Biotope im Naturschutzgebiet "Marienfließ". Jahresbericht 1993/1994	100330		\N	\N	\N		\N	\N	\N	143	143	Y			\N		\N	report	2016-05-04 11:11:04.327	\N	1994	4284	\N	f
4287	Clausnitzer_1993_100331	\N	\N	\N	\N	\N	Vegetationskundlich-ökologische Untersuchungen an der Ufervegetation in einem ausgewählten Bereich des Warnow-Einzugsgebietes	100331		\N	\N	\N		\N	\N	\N	251	251	Y			\N	Dipl.-Arb. TU Dresden, Institut f. Hydrobiologie	\N	thesis	2016-05-04 11:11:04.583	\N	1993	\N	\N	f
4310	Duks_1993_100341	\N	\N	\N	\N	\N	Gutachten für die Erweiterung des NSG "Unteres Warnowtal"	100341		\N	\N	\N		\N	\N	\N	76	76	Y			\N		\N	report	2016-05-04 11:11:06.831	\N	1993	4311	\N	f
4313	Duks_1995_100342	\N	\N	\N	\N	\N	Ökologische Begleitstudie zum Vorhaben "Hochwassersicherung Rosenort" (NSG "Heiligersee-Hütelmoor")	100342		\N	\N	\N		\N	\N	\N	42	42	Y			\N		\N	report	2016-05-04 11:11:07.085	\N	1995	4314	\N	f
4317	Fischer_1995_100343	\N	\N	\N	\N	\N	Das NSG "Peenewiesen bei Gützkow". Zur Vegetationsentwicklung im mittleren Peene- Tal	100343		\N	\N	\N		\N	\N	\N	247	247	Y			\N	Dipl.-Arb. Bot. Institut, Universität Greifswald	\N	thesis	2016-05-04 11:11:07.333	\N	1995	\N	\N	f
4304	Duecker_1995_100339	\N	\N	\N	\N	\N	Vegetation an der Landower Wedde, West-Rügen im Nationalpark Vorpommersche Boddenlandschaft	100339		\N	\N	\N		\N	\N	\N	223	223	Y			\N		\N	report	2016-05-04 11:11:06.331	\N	1995	4305	\N	f
4330	Hansen_1995_100347	\N	\N	\N	\N	\N	Ökologisches Fachgutachten zum NSG "Rögnitzwiesen"	100347		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	report	2016-05-04 11:11:08.411	\N	1995	4331	\N	f
4334	Harder_1993_100348	\N	\N	\N	\N	\N	Vegetationskundliche Kartierung des Dornbusch auf Hiddensee	100348		\N	\N	\N		\N	\N	\N	122	122	Y			\N		\N	report	2016-05-04 11:11:08.624	\N	1993	4335	\N	f
4337	Harder_1993_100349	\N	\N	\N	\N	\N	Die Fährinsel - eine landschaftsökologische Untersuchung	100349		\N	\N	\N		\N	\N	\N	149	149	Y			\N	Dipl.-Arb. Bot. Institut, Universität Greifswald	\N	thesis	2016-05-04 11:11:08.839	\N	1993	\N	\N	f
4344	Hemprich_Schumann_1995_100351	\N	\N	\N	\N	\N	Pflanzensoziologische Kartierung ausgewählter Gebiete am Goldberger See (Südufer), in der Borgsee-Niederung und der Höllwiese	100351		\N	\N	\N		\N	\N	\N	75	75	Y			\N		\N	report	2016-05-04 11:11:09.274	\N	1995	4345	\N	f
4347	Henker_1990_100352	\N	\N	\N	\N	\N	Botanisches Gutachten für Wallensteingraben und Rethmoor/Poel	100352		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	report	2016-05-04 11:11:09.537	\N	1990	4348	\N	f
4243	AG_Geobotanik_Mecklenburg-Vorpommern_1995_100319	\N	\N	\N	\N	\N	Erfassung und Bewertung von Trocken- und Magerbiotopen unter Berücksichtigung vom Aussterben bedrohter Pflanzenarten. Herausgegeben von H. Wollert	100319		\N	\N	\N		\N	\N	\N	423	423	Y			\N		\N	report	2016-05-04 11:11:01.653	\N	1995	4244	\N	f
4378	Kaestner_1994_100361	\N	\N	\N	\N	\N	Abschlußbericht NSG "Klädener Plage". Pflege- und Entwicklungskonzept	100361		\N	\N	\N		\N	\N	\N	43	43	Y			\N		\N	report	2016-05-04 11:11:11.766	\N	1994	4379	\N	f
4359	Holst_1994_100355	\N	\N	\N	\N	\N	Erarbeitung einer botanischen Bestandserfassung im Naturschutzgebiet "Sabelsee"	100355		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	report	2016-05-04 11:11:10.291	\N	1994	4360	\N	f
4367	Isermann_1994_100358	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen auf dem Darß und auf dem Gellen	100358		\N	\N	\N		\N	\N	\N	20	20	Y			\N		\N	report	2016-05-04 11:11:11.032	\N	1994	4368	\N	f
4382	Kaestner_Koch_Brielmann_1994_100362	\N	\N	\N	\N	\N	Bericht über Untersuchungen zur botanischen Ersterfassung des NSG "Klinker Plage"	100362		\N	\N	\N		\N	\N	\N	42	42	Y			\N		\N	report	2016-05-04 11:11:12.027	\N	1994	4383	\N	f
4375	Jaschhof_1990_100360	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen an der Warnow	100360		\N	\N	\N		\N	\N	\N	209	209	Y			\N	Dipl.-Arb. Sektion Biologie, Universität Rostock	\N	thesis	2016-05-04 11:11:11.515	\N	1990	\N	\N	f
4408	Krauss_1994_100371	\N	\N	\N	\N	\N	Vereinfachter Pflege- und Entwicklungsplan für die Freesendorfer Wiesen - Erweiterungsflächen im NSG "Peenemünder Haken, Struck und Ruden"	100371		\N	\N	\N		\N	\N	\N	87	87	Y			\N		\N	report	2016-05-04 11:11:14.324	\N	1994	4409	\N	f
4401	Kirsch_1992_100369	\N	\N	\N	\N	\N	Biotop- und Artenerfassung im LSG Woezer See. Gutachten zur Bewertung der Schutzwürdigkeit als Grundlage für ein Pflege- und Entwicklungskonzept	100369		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	report	2016-05-04 11:11:13.844	\N	1992	4402	\N	f
4356	Herden_Juergensen_Et_Al._1994_100354	\N	\N	\N	\N	\N	Pflege- und Entwicklungsplan für das NSG "Wustrow", Kreis Bad Doberan	100354		\N	\N	\N		\N	\N	\N	46	46	Y			\N		\N	report	2016-05-04 11:11:10.058	\N	1994	4357	\N	f
4412	Kremp_1993_100372	\N	\N	\N	\N	\N	Geobotanische Studien zur Vegetationsentwicklung im Seedorfer Hügelland - ein Beitrag zum Pflege- und Entwicklungsplan des Biosphärenreservates Südost- Rügen	100372		\N	\N	\N		\N	\N	\N	378	378	Y			\N	Dipl.-Arb. Universität Rostock	\N	thesis	2016-05-04 11:11:14.535	\N	1993	\N	\N	f
4415	Kurschat_1993_100373	\N	\N	\N	\N	\N	Einfluß des Tourismus auf die Pflanzendecke im Biosphärenreservat Süd-Ost- Rügen	100373		\N	\N	\N		\N	\N	\N	505	505	Y			\N	Dipl.-Arb. Oldenburg	\N	thesis	2016-05-04 11:11:14.778	\N	1993	\N	\N	f
4418	Lange_1994_100374	\N	\N	\N	\N	\N	Das Kieshofer Moor - eine landschaftsökologische Analyse	100374		\N	\N	\N		\N	\N	\N	186	186	Y			\N	Dipl.-Arb. Bot. Institut, Universität Greifswald	\N	thesis	2016-05-04 11:11:15	\N	1994	\N	\N	f
4421	Linke_1990_100375	\N	\N	\N	\N	\N	Vegetationskundliches Gutachten zur Wasserregulierung im Borken, Krs. Pasewalk	100375		\N	\N	\N		\N	\N	\N	671	671	Y			\N		\N	report	2016-05-04 11:11:15.281	\N	1990	4422	\N	f
4428	Martin_1992_100377	\N	\N	\N	\N	\N	Effizienzkontrolle zur naturschutzgerechten Grünlandnutzung im NSG "Zehlendorfer Moor"	100377		\N	\N	\N		\N	\N	\N	11	11	Y			\N		\N	report	2016-05-04 11:11:15.761	\N	1992	4429	\N	f
4439	Mohr_1993_100380	\N	\N	\N	\N	\N	Vegetationskundliche Bearbeitung des NSG "Landgrabenwiesen bei Werder" unter Einbeziehung des Quellmoorkomplexes "Binsenberg" bei Siedenbollentin mit dem umgebenden Grünland	100380		\N	\N	\N		\N	\N	\N	114	114	Y			\N		\N	report	2016-05-04 11:11:16.501	\N	1993	4440	\N	f
4442	Mohr_1994_100381	\N	\N	\N	\N	\N	Vegetationskundliche Bearbeitung für eine Pflege- und Entwicklungskonzeption des NSG "Hellberge"	100381		\N	\N	\N		\N	\N	\N	75	75	Y			\N		\N	report	2016-05-04 11:11:16.749	\N	1994	4443	\N	f
4446	Mohr_Hofmann_1992_100382	\N	\N	\N	\N	\N	Floristische, pflanzensoziologische und moorkundliche Untersuchung des beantragten NSG "Kucks- und Lapitzer See"	100382		\N	\N	\N		\N	\N	\N	40	40	Y			\N		\N	report	2016-05-04 11:11:16.968	\N	1992	4447	\N	f
4450	Mrotzek_1995_100383	\N	\N	\N	\N	\N	Landschaftsökologische Untersuchungen an Gewässern im Feldberger Sandergebiet (Krüselinsee, Made, Rohrpöhle, Gaschsee)	100383		\N	\N	\N		\N	\N	\N	426	426	Y			\N	Dipl.-Arb. Bot. Institut, Universität Greifswald	\N	thesis	2016-05-04 11:11:17.216	\N	1995	\N	\N	f
4468	Paulson_Raskin_1994_100388	\N	\N	\N	\N	\N	Bestandserfassung und Bewertung der Vegetation auf vier Transekten des Südteils der Sundischen Wiese im Nationalpark Vorpommersche Boddenlandschaft	100388		\N	\N	\N		\N	\N	\N	114	114	Y			\N		\N	report	2016-05-04 11:11:18.401	\N	1994	4469	\N	f
4371	Jacob_Et_Al._1996_100359	\N	\N	\N	\N	\N	Ökologisches Gutachten zum geplanten Naturschutzgebiet "Trockenhänge zwischen Jülchendorf und Schönlager See"	100359		\N	\N	\N		\N	\N	\N	81	81	Y			\N		\N	report	2016-05-04 11:11:11.297	\N	1996	4372	\N	f
4475	Paulson_Tautz_1995_100390	\N	\N	\N	\N	\N	Vegetationskundliches Gutachten zu Moorrenaturierungsprojekten im Serrahner Teil des Müritz- Nationalpark	100390		\N	\N	\N		\N	\N	\N	598	598	Y			\N		\N	report	2016-05-04 11:11:18.863	\N	1995	4476	\N	f
4481	Pivarci_1992_100392	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen im Naturschutzgebiet Kösterbeck - Fresendorf	100392		\N	\N	\N		\N	\N	\N	178	178	Y			\N	Dipl.-Arb. Universität Rostock	\N	thesis	2016-05-04 11:11:19.367	\N	1992	\N	\N	f
4486	Precker_1993_100394	\N	\N	\N	\N	\N	Moorschutzprogramm Mecklenburg- Vorpommern. Moorinventur/Teilbericht	100394		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	report	2016-05-04 11:11:19.819	\N	1993	4487	\N	f
4489	Precker_1994_100395	\N	\N	\N	\N	\N	Hydrologisches und standortkundliches Gutachten in Moorschutzgebieten des Rostocker Raumes im Rahmen der "Regenmoorinventur für das Land Mecklenburg-Vorpommern"	100395		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	report	2016-05-04 11:11:20.097	\N	1994	4490	\N	f
4509	Scheller_Voigtlaender_1993_100402	\N	\N	\N	\N	\N	Pflege- und Entwicklungsplan "Trebeltal" Stufe 1 - 3	100402		\N	\N	\N		\N	\N	\N	155	155	Y			\N		\N	report	2016-05-04 11:11:21.846	\N	1993	4510	\N	f
4497	Richter_1993_100398	\N	\N	\N	\N	\N	Pflanzensoziologische Untersuchung auf fünf Testflächen des ehemaligen Grenzstreifens am Naturpark Schaalsee	100398		\N	\N	\N		\N	\N	\N	19	19	Y			\N		\N	report	2016-05-04 11:11:20.798	\N	1993	4498	\N	f
4503	Rudat_1995_100400	\N	\N	\N	\N	\N	Landschaftsökologische Untersuchungen an Gewässern im Feldberger Sandergebiet (Waschsee, Weutschsee, Kl. Mechow, Gr. Mechow)	100400		\N	\N	\N		\N	\N	\N	324	324	Y			\N	Dipl.-Arb. Bot. Institut, Universität Greifswald	\N	thesis	2016-05-04 11:11:21.331	\N	1995	\N	\N	f
4517	Schoettelndreier_1995_100404	\N	\N	\N	\N	\N	Bestandsstruktur eines Naturwaldes auf der Insel Vilm	100404		\N	\N	\N		\N	\N	\N	149	149	Y			\N	Dipl.-Arb. Universität Göttingen	\N	thesis	2016-05-04 11:11:22.374	\N	1995	\N	\N	f
4533	Voigtlaender_1992_100409	\N	\N	\N	\N	\N	Abschnittsbericht 7. Trebeltal nordwestlich Demmin	100409		\N	\N	\N		\N	\N	\N	82	82	Y			\N		\N	report	2016-05-04 11:11:23.558	\N	1992	4534	\N	f
4524	Stamm_1994_100406	\N	\N	\N	\N	\N	Die salzbeeinflußte Vegetation der Halbinsel Mönchgut	100406		\N	\N	\N		\N	\N	\N	449	449	Y			\N	Dipl.-Arb. Universität Göttingen	\N	thesis	2016-05-04 11:11:22.876	\N	1994	\N	\N	f
4527	Stuhr_1993_100407	\N	\N	\N	\N	\N	Vegetationskartierung von Flächen bei Bliesenrade	100407		\N	\N	\N		\N	\N	\N	48	48	Y			\N		\N	report	2016-05-04 11:11:23.085	\N	1993	4528	\N	f
4531	Uhle_1996_100408	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen in der Sundischen Wiese	100408		\N	\N	\N		\N	\N	\N	133	133	Y			\N	Dipl.-Arb. Universität Rostock	\N	thesis	2016-05-04 11:11:23.332	\N	1996	\N	\N	f
4520	Spiess_1992_100405	\N	\N	\N	\N	\N	Effizienzkontrolle zur naturschutzgerechten Grünlandnutzung auf dem "Großen Schwerin"	100405		\N	\N	\N		\N	\N	\N	80	80	Y			\N		\N	report	2016-05-04 11:11:22.582	\N	1992	4521	\N	f
4579	Wefing_1993_100424	\N	\N	\N	\N	\N	Bestandserfassung und Bewertung der Vegetation der Borner und Neuendorfer Bülten im Saaler Bodden sowie des Salzgraslandes der Borner Kaase	100424		\N	\N	\N		\N	\N	\N	63	63	Y			\N		\N	report	2016-05-04 11:11:27.969	\N	1993	4580	\N	f
4542	Voigtlaender_1993_100412	\N	\N	\N	\N	\N	Vorschläge zur Renaturierung des Darzer Moores	100412		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	report	2016-05-04 11:11:24.24	\N	1993	4543	\N	f
4583	Wiehle_1993_100425	\N	\N	\N	\N	\N	Botanische Inventarisierung des NSG "Tiefwaren und Falkenhägener Bruch"	100425		\N	\N	\N		\N	\N	\N	74	74	Y			\N		\N	report	2016-05-04 11:11:28.218	\N	1993	4584	\N	f
4587	Lamarck_1992_100426	\N	\N	\N	\N	\N	Effizienzkontrolle zur naturschutzgerechten Grünlandnutzung im NSG "Peenewiesen bei Gützkow östlich des Fährdammes" und im NSG "Schwingetal"	100426		\N	\N	\N		\N	\N	\N	27	27	Y			\N		\N	report	2016-05-04 11:11:28.471	\N	1992	4588	\N	f
4591	Woidig_1995_100427	\N	\N	\N	\N	\N	Vegetations- und standortkundliche Untersuchungen im Gebiet der Zickerniß-Niederung im Biosphärenreservat Südost-Rügen	100427		\N	\N	\N		\N	\N	\N	133	133	Y			\N	Dipl.-Arb. Bot. Institut, Universität Greifswald	\N	thesis	2016-05-04 11:11:28.721	\N	1995	\N	\N	f
4593	Wollert_1992_100428	\N	\N	\N	\N	\N	Abschnittsbericht 1. Recknitztal nördlich Laage, Recknitz/Augrabental südlich Laage, Vegetationskartierung	100428		\N	\N	\N		\N	\N	\N	213	213	Y			\N		\N	report	2016-05-04 11:11:28.996	\N	1992	4594	\N	f
4596	Wollert_1992_100429	\N	\N	\N	\N	\N	Flora und Vegetation des Nienhagen- Carlsdorfer Forstes	100429		\N	\N	\N		\N	\N	\N	205	205	Y			\N		\N	report	2016-05-04 11:11:29.232	\N	1992	4597	\N	f
4599	Wollert_1992_100430	\N	\N	\N	\N	\N	Effizienzkontrolle zur naturschutzgerechten Grünlandnutzung im Feuchtgebiet "Neukalener Moorwiesen" und "Os Alt Gatschow"	100430		\N	\N	\N		\N	\N	\N	11	11	Y			\N		\N	report	2016-05-04 11:11:29.471	\N	1992	4600	\N	f
4602	Wollert_1993_100431	\N	\N	\N	\N	\N	Zur Flora und Vegetation des Endmoränengebietes zwischen Langhagen und Klein Luckow	100431		\N	\N	\N		\N	\N	\N	124	124	Y			\N		\N	report	2016-05-04 11:11:29.755	\N	1993	4603	\N	f
4607	Wollert_1994_100433	\N	\N	\N	\N	\N	Zur aktuellen Vegetation und Flora der Freesendorfer Wiesen	100433		\N	\N	\N		\N	\N	\N	163	163	Y			\N		\N	report	2016-05-04 11:11:30.247	\N	1994	4608	\N	f
4610	Wollert_1994_100434	\N	\N	\N	\N	\N	Die Vegetation im Raum Rogeez	100434		\N	\N	\N		\N	\N	\N	184	184	Y			\N		\N	report	2016-05-04 11:11:30.529	\N	1994	4611	\N	f
4613	Wollert_1995_100435	\N	\N	\N	\N	\N	Untersuchungen zur Entwicklung der Vegetation im NSG "Wallberge bei Alt-Gatschow"	100435		\N	\N	\N		\N	\N	\N	77	77	Y			\N		\N	report	2016-05-04 11:11:30.761	\N	1995	4614	\N	f
4616	Wollert_1995_100436	\N	\N	\N	\N	\N	Biotoptypen und Vegetationskartierung im Rahmen der UVU Thermische Müllverwertungsanlage Neubrandenburg	100436		\N	\N	\N		\N	\N	\N	156	156	Y			\N		\N	report	2016-05-04 11:11:31.015	\N	1995	4617	\N	f
4623	Wollert_Bolbrinker_1994_100439	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen an einigen naturnahen Bachabschnitten im Landkreis Güstrow	100439		\N	\N	\N		\N	\N	\N	268	268	Y			\N		\N	report	2016-05-04 11:11:31.808	\N	1994	4624	\N	f
4631	Ibs_schwerin_1994_100442	\N	\N	\N	\N	\N	Pflege- und Entwicklungskonzeption für die Warnow	100442		\N	\N	\N		\N	\N	\N	68	68	Y			\N		\N	report	2016-05-04 11:11:32.681	\N	1994	4632	\N	f
4634	Ibs_schwerin_1995_100443	\N	\N	\N	\N	\N	Pflege und Entwicklungskonzept NSG "Stecknitz-Delvenau"	100443		\N	\N	\N		\N	\N	\N	47	47	Y			\N		\N	report	2016-05-04 11:11:32.987	\N	1995	4635	\N	f
4638	Geisel_1986_100444	\N	\N	\N	\N	\N	Pflanzensoziologische Untersuchungen am Makrophytobentos des Greifswalder Boddens	100444		\N	\N	\N		\N	\N	\N	194	194	Y			\N	Universität Rostock, Dipl.-Arbeit	\N	thesis	2016-05-04 11:11:33.201	\N	1986	\N	\N	f
4641	Lampe_1989_100445	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen an stehenden Kleingewässern in der Umgebung des Düngemittelwerkes Rostock-Poppendorf	100445		\N	\N	\N		\N	\N	\N	253	253	Y			\N	Universität Rostock, Dipl.-Arbeit	\N	thesis	2016-05-04 11:11:33.403	\N	1989	\N	\N	f
4647	Isermann_1997_100447	\N	\N	\N	\N	\N	Vegetations- und standortkundliche Untersuchungen in Küstendünen Vorpommerns	100447		\N	\N	\N		\N	\N	\N	1446	\N	Y			\N	Universität Greifswald, Diss	\N	thesis	2016-05-04 11:11:33.938	\N	1997	\N	\N	f
4652	Bolbrinker_1996_100449	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen der submersen Vegetation von Klarwasserseen im NP "Nossentiner-Schwinzer Heide"	100449		\N	\N	\N		\N	\N	\N	155	155	Y			\N		\N	report	2016-05-04 11:11:34.479	\N	1996	4653	\N	f
4649	Schumann_Et_Al._1996_100448	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen in fünf NSG des "NP Nossentiner-Schwinzer Heide"	100448		\N	\N	\N		\N	\N	\N	399	399	Y			\N		\N	report	2016-05-04 11:11:34.206	\N	1996	4650	\N	f
4644	Schaefer_1989_100446	\N	\N	\N	\N	\N	Phytocönologisch-ökologische Untersuchungen in den ackerbaulich genutzten, geringwertigen Sandböden des Rostocker Raumes	100446		\N	\N	\N		\N	\N	\N	443	443	Y			\N	Universität Rostock, Dipl.-Arbeit	\N	thesis	2016-05-04 11:11:33.65	\N	1989	\N	\N	f
4743	Wollert_1998_100483	\N	\N	\N	\N	\N	Erfassung und Bewertung vom Aussterben bedrohter Pflanzenarten der Feucht- und Waldbiotope Mecklenburg-Vorpommerns	100483		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y	\N		\N		\N	unpublished	2016-05-04 11:11:43.227	\N	1998	\N	\N	f
4746	Manthey_1999_100484	\N	\N	\N	\N	\N	Vegetationsformen von Äckern in Mecklenburg-Vorpommern	100484		\N	\N	\N		\N	\N	\N	693	\N	Y			\N	Universität Greifswald	\N	thesis	2016-05-04 11:11:43.48	\N	1999	\N	\N	f
4749	John_1999_100486	\N	\N	\N	\N	\N	Ackerwildkrautgesellschaften auf dem Bioland-Hof bei Bisdamitz	100486		\N	\N	\N		\N	\N	\N	75	75	Y			\N	Universität Greifswald	\N	thesis	2016-05-04 11:11:43.728	\N	1999	\N	\N	f
4752	Spangenberg_1999_100487	\N	\N	\N	\N	\N	Waldgesellschaften im Naturschutzgebiet Eldena	100487		\N	\N	\N		\N	\N	\N	128	128	Y			\N	Universität Greifswald	\N	thesis	2016-05-04 11:11:43.974	\N	1999	\N	\N	f
4754	Spangenberg_1999_100488	\N	\N	\N	\N	\N	Waldmonitoring Mecklenburg-Vorpommern	100488		\N	\N	\N		\N	\N	\N	126	126	Y			\N		\N	report	2016-05-04 11:11:44.178	\N	1999	4755	\N	f
4765	Polte_1998_100492	\N	\N	\N	\N	\N	Vegetationsökologische Untersuchungen in der Wismarbucht	100492		\N	\N	\N		\N	\N	\N	113	113	Y			\N	Universität Greifswald	\N	thesis	2016-05-04 11:11:45.09	\N	1998	\N	\N	f
4768	Berg_1997_100493	\N	\N	\N	\N	\N	Vegetationsaufnahmen Mecklenburg-Vorpommern	100493		\N	\N	unpubliziert		\N	\N	\N	498	498	Y	Unveröff		\N		\N	unpublished	2016-05-04 11:11:45.377	\N	1997	\N	\N	f
4774	Bohnacker_1998_100495	\N	\N	\N	\N	\N	Vegetations- und standortkundliche Untersuchungen zur natürlichen und anthropogenen Entwicklung des Talmoorkomplexes Kleiner Landgraben (Mecklenburg-Vorpommern)	100495		\N	\N	\N		\N	\N	\N	149	149	Y			\N	Universität Greifswald	\N	thesis	2016-05-04 11:11:45.888	\N	1998	\N	\N	f
4776	Clausnitzer_1998_100496	\N	\N	\N	\N	\N	Vegetationskundlich-landschaftsökologische Untersuchungen im Flußtalmoor der Recknitz als Grundlage für Maßnahmen zur Renaturierung und naturgemäßen Bewirtschaftung	100496		\N	\N	\N		\N	\N	\N	476	\N	Y			\N	Universität Greifswald	\N	thesis	2016-05-04 11:11:46.095	\N	1998	\N	\N	f
4779	Folkowski_1998_100497	\N	\N	\N	\N	\N	Untersuchungen zur Struktur und Zusammensetzung der aktuellen Vegetation und der Diasporenbank auf Salzgrünlandflächen der Halbinsel Zingst	100497		\N	\N	\N		\N	\N	\N	341	341	Y			\N	Universität Greifswald	\N	thesis	2016-05-04 11:11:46.301	\N	1998	\N	\N	f
4782	Jansen_1998_100498	\N	\N	\N	\N	\N	Standort und Vegetation der Ziesenniederung. Ein Vergleich 1964 - 1996	100498		\N	\N	\N		\N	\N	\N	144	144	Y			\N	Universität Greifswald	\N	thesis	2016-05-04 11:11:46.504	\N	1998	\N	\N	f
4785	Knapp_1997_100499	\N	\N	\N	\N	\N	Standorts- und vegetationskundliche Untersuchungen an Torfstichen der Großen Friedländer Wiese	100499		\N	\N	\N		\N	\N	\N	94	94	Y			\N	Universität Greifswald	\N	thesis	2016-05-04 11:11:46.714	\N	1997	\N	\N	f
4788	Latsch_1998_100500	\N	\N	\N	\N	\N	Die Bedeutung der Sölle in der Kulturlandschaft als Ausbreitungs- und Trittsteinbiotop für Pflanzenarten	100500		\N	\N	\N		\N	\N	\N	209	209	Y			\N	Universität Greifswald	\N	thesis	2016-05-04 11:11:46.955	\N	1998	\N	\N	f
4791	Roth_1995_100501	\N	\N	\N	\N	\N	Ökosystemmanagement für Niedermoore, Teilprojekt Friedländer Große Wiese	100501		\N	\N	unpubliziert		\N	\N	\N	335	335	Y	\N		\N		\N	unpublished	2016-05-04 11:11:47.177	\N	1995	\N	\N	f
4735	Froede_1949_100479	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der Insel Hiddensee	100479		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	Universität Greifswald	\N	thesis	2016-05-04 11:11:42.184	\N	1949	\N	\N	f
4758	Roesler_1997_100489	\N	\N	\N	\N	\N	Vegetationsökologische Kartierung und Bewertung der Küste Mönchguts (Insel Rügen)	100489		\N	\N	\N		\N	\N	\N	394	394	Y			\N	Universität Greifswald	\N	thesis	2016-05-04 11:11:44.391	\N	1997	\N	\N	f
4803	Knapp_1999_100506	\N	\N	\N	\N	\N	Zur Etablierung von Feuchtwiesen auf extensiviertem ehemaligen Niedermoor-Saatgrasland	100506		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	Universität Greifswald	\N	thesis	2016-05-04 11:11:48.404	\N	1999	\N	\N	f
4806	Vegelin_Nijhof_1996_100507	\N	\N	\N	\N	\N	Landschaftsanalyse zur Renaturierung des Tales der (Blinden) Trebel	100507		\N	\N	unpubliziert		\N	\N	\N	103	103	Y	\N		\N		\N	unpublished	2016-05-04 11:11:48.654	\N	1996	\N	\N	f
4808	Woidig_1997_100508	\N	\N	\N	\N	\N	Waldmonitoring Müritz-Nationalpark	100508		\N	\N	unpubliziert		\N	\N	\N	281	281	Y	\N		\N		\N	unpublished	2016-05-04 11:11:48.903	\N	1997	\N	\N	f
4796	Bluemel_1993_100503	\N	\N	\N	\N	\N	Vegetationskartierung Karrendorf vor dem Deichrückbau	100503		\N	\N	unpubliziert		\N	\N	\N	102	102	Y	\N		\N		\N	unpublished	2016-05-04 11:11:47.682	\N	1993	\N	\N	f
4821	Landesarbeitsgemeinschaft_Vegetationskunde_MV_2000_100513	\N	\N	\N	\N	\N	Arbeitstreffen Elbtal	100513		\N	\N	unpubliziert		\N	\N	\N	38	38	Y	Unveröff		\N		\N	unpublished	2016-05-04 11:11:50.072	\N	2000	\N	\N	f
4799	Ostendorp_Bluemel_1996_100504	\N	\N	\N	\N	\N	Vegetations- und standortkundliche Untersuchungen am Alten Bessin (Hiddensee)	100504		\N	\N	unpubliziert		\N	\N	\N	53	53	Y	\N		\N		\N	unpublished	2016-05-04 11:11:47.937	\N	1996	\N	\N	f
4853	Roesler_1997_100528	\N	\N	\N	\N	\N	Vegetationsökologische Kartierung und Bewertung der Küste Mönchsguts (Insel Rügen)	100528		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	Universität Greifswald	\N	thesis	2016-05-04 11:11:53.896	\N	1997	\N	\N	f
4892	Koeber_2001_100549	\N	\N	\N	\N	\N	Vegetations- und standortkundliche Untersuchungen der Hudelandschaft am Ostufer der Müritz	100549		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:11:57.534	\N	2001	\N	\N	f
4889	Kiesslich_2001_100541	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen von Zweizahngesellschaften (Bidentetea tripartiti) in Mecklenburg-Vorpommern	100541		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y	\N		\N		\N	unpublished	2016-05-04 11:11:57.286	\N	2001	\N	\N	f
4863	Friedel_1999_100532	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen in einem Buchenwald (Luzulo-Fagetum) im Müritz-Nationalpark	100532		\N	\N	unpubliziert		\N	\N	\N	40	40	Y	\N		\N		\N	unpublished	2016-05-04 11:11:55.088	\N	1999	\N	\N	f
4866	Seidenschnur_2000_100533	\N	\N	\N	\N	\N	Vegetations- und standortkundliche Untersuchungen auf eingedeichten Niederungsstandorten der Wismarbucht	100533		\N	\N	\N		\N	\N	\N	274	274	Y			\N	Universität Greifswald	\N	thesis	2016-05-04 11:11:55.385	\N	2000	\N	\N	f
4880	Petersen_1999_100538	\N	\N	\N	\N	\N	Die Dünentalvegetation der Wattenmeer-Inseln in der südlichen Nordsee. Eine pflanzensoziologische und ökologische Vergleichsuntersuchung unter Berücksichtigung von Nutzung und Naturschutz	100538		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y	\N		\N		\N	unpublished	2016-05-04 11:11:56.634	\N	1999	\N	\N	f
4886	Linke_2000_100540	\N	\N	\N	\N	\N	nicht publiziert	100540		\N	\N	unpubliziert		\N	\N	\N	25	25	Y	\N		\N		\N	unpublished	2016-05-04 11:11:57.08	\N	2000	\N	\N	f
4895	Kratzert_1998_100550	\N	\N	\N	\N	\N	Die Trockenrasen der Gabower Hänge am Oderbruch	100550		\N	\N	\N		\N	\N	\N	128	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:11:57.738	\N	1998	\N	\N	f
4900	Hoffmann_1999_100552	\N	\N	\N	\N	\N	Vergleichende Pflanzensoziologische und ökologische Untersuchungen auf Trocken- und Halbtrockenrasen im NSG "Pimpinellenberg" bei Oderberg und GLB Mühlenberg bei Brodowin	100552		\N	\N	\N		\N	\N	\N	115	115	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:11:58.244	\N	1999	\N	\N	f
4930	Clausnitzer_2002_100568	\N	\N	\N	\N	\N	Moorbuch-Selektion	100568		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:01.875	\N	2002	\N	\N	f
5009	Joerns_2003_100611	\N	\N	\N	\N	\N	Vergleichende vegetationsökologische Untersuchungen von Ackerbrachen im Müritzgebiet	100611		\N	\N	\N		\N	\N	\N	90	90	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:08.415	\N	2003	\N	\N	f
4902	Dengler_1994_100553	\N	\N	\N	\N	\N	Trockenrasen im Biosphärenreservat Schorfheide-Chorin. Flora - Vegetation - Böden - Naturschutz	100553		\N	\N	\N		DUBLETTEN?	\N	\N	475	475	Y			\N	ob Theses?	\N	thesis	2016-12-20 20:39:01.703	\N	1994	\N	\N	f
4938	Diedrich_1998_100571	\N	\N	\N	\N	\N	Ökologische und ökonomische Bewertung unbeeinflußter Wiederbewaldung am Beispiel der Ackerbrachen bei Federow und Ankershagen	100571		\N	\N	\N		\N	\N	\N	88	88	Y			\N	Dipl.-Arb. Universität Greifswald	\N	thesis	2016-05-04 11:12:02.663	\N	1998	\N	\N	f
5012	Klussmann_2004_100612	\N	\N	\N	\N	\N	Vegetationsökologische Untersuchungen zur Re-Etablierung von Salzgrasland am Beispiel der Karrendorfer Wiesen bei Greifswald (Ostsee)	100612		\N	\N	\N		\N	\N	\N	0	226	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:08.698	\N	2004	\N	\N	f
4944	Neubauer_1999_100573	\N	\N	\N	\N	\N	Standortskennzeichnung auf Ackerbrachen bei Greifswald- Boden-, Vegetations- und Phytomasseuntersuchungen im Methodenvergleich	100573		\N	\N	\N		\N	\N	\N	31	31	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:03.123	\N	1999	\N	\N	f
4951	Koska_Kaffke_2005_100591	\N	\N	\N	\N	\N	ALNUS-Projekt, AG 2 Standort, Indikation, GIS: Untersuchungen in Erlenwäldern in MV	100591		\N	\N	unpubliziert		\N	\N	\N	85	85	Y	Mskr		\N		\N	unpublished	2016-05-04 11:12:03.639	\N	2005	\N	\N	f
4961	Fitschen_1993_100594	\N	\N	\N	\N	\N	Der Gartzsee: Vegetation und Startigraphie eines Moores in der Märkischen Schweiz (Ostbrandenburg)	100594		\N	\N	\N		\N	\N	\N	8	8	Y			\N	Dipl.-Arb. Institut f. Ökol. TU Berlin, 149 S	\N	thesis	2016-05-04 11:12:04.341	\N	1993	\N	\N	f
4963	Schumann_2004_100595	\N	\N	\N	\N	\N	Standörtliche Charakterisierung mesotropher Birken- und Erlenbruchwälder in der Uckermark	100595		\N	\N	\N		\N	\N	\N	12	12	Y			\N	Dipl.-Arb. Bot. Institut EMA-Universität Greifswald, 93 S	\N	thesis	2016-05-04 11:12:04.558	\N	2004	\N	\N	f
4967	Seuffert_Stolze_2001_100596	\N	\N	\N	\N	\N	Der Melln(see) - Vegetationsökologische, stratigrafische und palynologische Untersuchungen in einem Verlandungsmoor	100596		\N	\N	\N		\N	\N	\N	109	109	Y			\N	Dipl.-Arb. Bot. Institut EMA-Universität Greifswald: 200 S	\N	thesis	2016-05-04 11:12:04.858	\N	2001	\N	\N	f
4972	Beitz_2004_100598	\N	\N	\N	\N	\N	Vegetations- und Standortswandel im NSG "Birkbuschwiesen" bei Neubrandenburg	100598		\N	\N	\N		\N	\N	\N	63	63	Y			\N	Dipl.-Arb. Bot. Institut EMA-Universität Greifswald, 141 S	\N	thesis	2016-05-04 11:12:05.36	\N	2004	\N	\N	f
4975	Mickel_2005_100599	\N	\N	\N	\N	\N	Die Verlandungsvegetation des Naturschutzgebietes Galenbecker See / Vorpommern	100599		\N	\N	\N		\N	\N	\N	35	35	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:05.569	\N	2005	\N	\N	f
4978	Brinkmann_2002_100600	\N	\N	\N	\N	\N	Landschaftsökologische Untersuchungen des NSG "Lanken" (Kreis Ost-Vorpommern)	100600		\N	\N	\N		\N	\N	\N	2	2	Y			\N	Dipl.-Arb. Bot. Institut EMA-Universität Greifswald, 68 S	\N	thesis	2016-05-04 11:12:05.778	\N	2002	\N	\N	f
5117	Zahn_1975_100648	\N	\N	\N	\N	\N	Floristische Untersuchungen und Phänologie von Zwischenmooren im Bereich der Revierförstereien Hanshagen und Wrangelsburg	100648		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:17.675	\N	1975	\N	\N	f
4982	Hinz_Goldberg_2006_100601	\N	\N	\N	\N	\N	Trophiekennzeichnung im anhydromorphen Grünland durch Nährstoffgehalte der oberirdischen Biomasse (Hinz) bzw. Vegetations- und Bodenuntersuchungen auf anhydromorphen Grünlandstandorten als Basis für die Weiterentwicklung des Wasserstufenkonz.. (Goldberg)	100601		\N	\N	\N		\N	\N	\N	47	47	Y			\N	Dipl.-Arb. Universität Greifswald	\N	thesis	2016-05-04 11:12:05.997	\N	2006	\N	\N	f
4988	Neumann_2003_100603	\N	\N	\N	\N	\N	Der Einfluss der Wiedervernässung auf Schwarzerlenwälder im Peenetal	100603		\N	\N	\N		\N	\N	\N	5	5	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:06.521	\N	2003	\N	\N	f
4991	Rudolphi_2001_100604	\N	\N	\N	\N	\N	Landschaftsökologische Untersuchungen bewaldeter Feuchtgebiete in der Barther Heide (Forstreviere Barth, Fuhlendorf und Gätkenhagen)	100604		\N	\N	\N		\N	\N	\N	1	1	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:06.732	\N	2001	\N	\N	f
4997	Effenberger_1999_100606	\N	\N	\N	\N	\N	Renaturierung von degradierten Niedermooren - Landschaftsökologische Grundlagen und Chancen am Beispiel des Durchströmungsmoorbereichs der Mittleren Trebel	100606		\N	\N	\N		\N	\N	\N	0	40	Y			\N		\N	thesis	2016-05-04 11:12:07.246	\N	1999	\N	\N	f
5000	Bossert_2003_100607	\N	\N	\N	\N	\N	Hochleistungskühe: Landschaftsökologische Konsequenzen unt Tierethische Aspekte	100607		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:07.545	\N	2003	\N	\N	f
5003	Beil_2000_100608	\N	\N	\N	\N	\N	Landschaftsökologische und ökoonomische Bewertung der Bewirtschaftung von Grünladflächen der Agrarprodukt GmbH Poseritz auf Rügen	100608		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:07.808	\N	2000	\N	\N	f
5016	Labitzke_Peschel_1993_100613	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen von Moorwiesen im Biosphärenreservat "Schorfheide-Chorin" mit ökologischen Bewertungen und Vorschlägen zur Pflege	100613		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:08.979	\N	1993	\N	\N	f
5019	Laube_2005_100614	\N	\N	\N	\N	\N	Vegetations- und standortkundliche Untersuchungen in den Binnendünen bei Altwarp und angrenzenden Niedermoorstandorten	100614		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:09.29	\N	2005	\N	\N	f
5025	Buhl_2000_100616	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen im Staatsforst Abtshagen (NSG "Wittenhagen")	100616		\N	\N	\N		\N	\N	\N	0	267	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:09.762	\N	2000	\N	\N	f
5028	Romer_1997_100617	\N	\N	\N	\N	\N	Das NSG "Mannhagener Moor"	100617		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:10.027	\N	1997	\N	\N	f
5031	Ruth_1998_100618	\N	\N	\N	\N	\N	Untersuchungen zur Vegetationsdynamik der Salzweiden und Salzpioniergesellschaften auf den Karrendorfer Wiesen	100618		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	Dipl.-Arb. Bot. Institut, Universität Greifswald	\N	thesis	2016-05-04 11:12:10.285	\N	1998	\N	\N	f
4941	Hennek_1997_100572	\N	\N	\N	\N	\N	Projektarbeit Frauke Hennek	100572		\N	\N	\N		\N	\N	Institut für Botanik und Landschaftsökologie	27	27	Y			\N	Universität Greifswald	\N	thesis	2016-05-13 11:13:29.654	\N	1997	\N	\N	f
5037	Manthe_2002_100620	\N	\N	\N	\N	\N	Ökonomische und landschaftsökologische Analyse einer Beweidung zur Landschaftspflege mit Heckrindern der Peenewiesen bei Upost/Verchen	100620		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:10.793	\N	2002	\N	\N	f
5040	Lenk_2002_100621	\N	\N	\N	\N	\N	Landschaftspflegeverfahren auf Niedermoorstandortern: Vergleich von maschineller Pflege und Beweidung mit Heckrindern	100621		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:11.035	\N	2002	\N	\N	f
5043	Landgraf_1998_100622	\N	\N	\N	\N	\N	Vegetations- und standortkundliche Untersuchungen an einem seit Winter 1991/92 wiedervernäßten Niedermoor in der Nuthe-Nieplitz-Niederung	100622		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:11.323	\N	1998	\N	\N	f
5046	Michaelis_1996_100623	\N	\N	\N	\N	\N	Standort- und vegetationskundliche Untersuchungen im Naturschutzgebiet "Plagefenn"	100623		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:11.566	\N	1996	\N	\N	f
5049	Mikosch_2001_100624	\N	\N	\N	\N	\N	Bodenökologischer Vergleich von pferde- und traktorbewirtschafteten ökologischen Ackerflächen	100624		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:11.814	\N	2001	\N	\N	f
5078	Schoenemann_1999_100634	\N	\N	\N	\N	\N	Landschaftsökologische Untersuchung eines Küstenüberflutungsmoores der Vorpommerschen Boddenküste am Beispiel der Insel Struck	100634		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:14.322	\N	1999	\N	\N	f
5058	Timmermann_1992_100627	\N	\N	\N	\N	\N	Vegetationskundliche und stratigraphische Untersuchungen in der Meelake (Nordostbrandenburg)	100627		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:12.627	\N	1992	\N	\N	f
5061	Tesmer_1997_100628	\N	\N	\N	\N	\N	Vergleich der Methoden Pflanzensoziologie und Vegetationsform in ihrer Anwendung	100628		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:12.869	\N	1997	\N	\N	f
5064	Skriewe_2003_100629	\N	\N	\N	\N	\N	Kennzeichnung von Braunmoostorfen hinsichtlich ihrer Vegetation und Genese am Beispiel der Lieper Posse	100629		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:13.16	\N	2003	\N	\N	f
5069	Schurbohm_1998_100631	\N	\N	\N	\N	\N	Untersuchungen zur Vegetationsdynamik und Populationsstruktur von Röhrischten auf den Karrendorfer Wiesen	100631		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:13.647	\N	1998	\N	\N	f
5072	Schulz_2005_100632	\N	\N	\N	\N	\N	Vegetations- und Standortsentwicklung des wiedervernässten Grünlandes im Anklamer Stadtbruch (Mecklenburg-Vorpommern)	100632		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:13.843	\N	2005	\N	\N	f
5075	Schulz_1999_100633	\N	\N	\N	\N	\N	Landschaftsökologie des Jeeser Moores und des Söllkenmoores	100633		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:14.118	\N	1999	\N	\N	f
5081	Schlundt_1999_100635	\N	\N	\N	\N	\N	Untersuchung des ökologischen Zustandes und der gegenwärtigen Situation großer Seenverbundsysteme am Beispiel des Brückentinsee-Systems	100635		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:14.523	\N	1999	\N	\N	f
5084	Schleicher_2005_100636	\N	\N	\N	\N	\N	Lebensraumanalyse des Blaukehlchens (Luscinia svecica cyanecula) im Parsteinbecken (Nordostbrandenburg)	100636		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:14.767	\N	2005	\N	\N	f
5052	Muenich_1998_100625	\N	\N	\N	\N	\N	Vegetations- und standortkundliche Untersuchungen auf wiedervernäßtem Niedermoor im Naturparkprojekt Nuthe-Nieplitz	100625		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:12.055	\N	1998	\N	\N	f
5091	Hofemeister_1966_100638	\N	\N	\N	\N	\N	Die Pflanzengesellschften der Granitz	100638		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:15.267	\N	1966	\N	\N	f
5094	Helke_1999_100639	\N	\N	\N	\N	\N	Das Revitalisierungspotential des Polders "Randow-Rustow" (Peenetalmoor)	100639		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:15.508	\N	1999	\N	\N	f
5097	Kiputh_1995_100640	\N	\N	\N	\N	\N	Vegetation und Standort an periodisch trockenfallenden Seeufern der Uckermark	100640		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:15.792	\N	1995	\N	\N	f
5099	Kloss_1958_100641	\N	\N	\N	\N	\N	Ackerunkrautgesellschaften der Greifswalder Universitätsgüter in ihren Beziehungen zu Boden- und Kulturverhältnissen	100641		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:16.045	\N	1958	\N	\N	f
5101	Slobodda_1966_100642	\N	\N	\N	\N	\N	Die Vegetation der oligotrophen Moore um Greifswald unter besonderer Berücksichtigung der Bryophyten	100642		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:16.249	\N	1966	\N	\N	f
5106	Prager_2000_100644	\N	\N	\N	\N	\N	Vegetationsentwicklung auf wiedervernässtem Moorgrünland in Nordostdeutschland	100644		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:16.71	\N	2000	\N	\N	f
5055	Wuest_2001_100626	\N	\N	\N	\N	\N	Die geplante Erweiterung des NSG "Planetal" Fläming - Vegetationskundliche und ökofaunistische Untersuchungen	100626		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:12.342	\N	2001	\N	\N	f
5112	Zimmermann_2001_100646	\N	\N	\N	\N	\N	Landschaftsökologische Untersuchungen des Küsten-Überflutungsmoores Kooser Wiesen	100646		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:17.167	\N	2001	\N	\N	f
5114	Wollert_1964_100647	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der Oser Mittelmecklenburgs unter besonderer Berücksichtigung der Trockenrasengesellschaften	100647		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:17.412	\N	1964	\N	\N	f
5088	Schaefer_Hornschuch_1998_100637	\N	\N	\N	\N	\N	Standort und Vegetation der Wälder, Moore und Sümpfe im Naturschutzgebiet "Grumsiner Forst"	100637		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:15.013	\N	1998	\N	\N	f
5128	Gremer_Koska_2002_100652	\N	\N	\N	\N	\N	Monitoring im "Rauhen Moor" (Synonym: NSG "Grenztalmoor") 1996 bis 2001. Vegetationsentwicklung im bewaldeten NSG 4 Jahre nach Wiedervernässung	100652		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:18.701	\N	2002	\N	\N	f
5130	Timmermann_1999_100653	\N	\N	\N	\N	\N	Sphagnum-Moore in Nordostbrandenburg: Stratigraphisch-hydrodynamische Typisierung und Vegetationswandel seit 1923	100653		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:18.913	\N	1999	\N	\N	f
5125	Praktikum_Wackerow_2001_100651	\N	\N	\N	\N	\N	Aufnahmen aus dem Vegetationsökologischen Praktikum ab 2001	100651		\N	\N	\N		\N	\N	\N	213	134	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:18.453	\N	2001	\N	\N	f
4994	Hacker_2002_100605	\N	\N	\N	\N	\N	Schlucht- und Hangwälder im Naturpark Mecklenburgische Schweiz und Kummerower See, im Nebeltal und auf Südostrügen	100605		\N	\N	\N		\N	\N	\N	1	1	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:06.985	\N	2002	\N	\N	f
5134	Gremer_Clausnitzer_2001_100655	\N	\N	\N	\N	\N	Renaturierung im Flußtalmoor der Recknitz zwischen Dudendorf und Bad Sülze - landschaftsökologische Begleitforschung (LIFE-Projekt)	100655		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	report	2016-05-04 11:12:19.347	\N	2001	5135	\N	f
5137	Ibs_schwerin_1993_100657	\N	\N	\N	\N	\N	Pflege- und Entwicklungskonzept für das Wickendorfer Moor	100657		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	report	2016-05-04 11:12:19.648	\N	1993	5138	\N	f
5140	Ibs_schwerin_1993_100658	\N	\N	\N	\N	\N	Pflege- und Entwicklungskonzept NSG "Wakenitzniederung"	100658		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	report	2016-05-04 11:12:19.874	\N	1993	5141	\N	f
5153	Lemke_Hobbhahn_2004_100662	\N	\N	\N	\N	\N	Die Vegetation salzbeeinflusster Überflutungsräume auf dem Gebiet der Hansestadt Rostock	100662		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:20.834	\N	2004	\N	\N	f
5156	Troeltsch_2003_100663	\N	\N	\N	\N	\N	Segetalflora in Abhängigkeit von der Bewirtschaftung in Nordwestpolen	100663		\N	\N	\N		\N	\N	\N	0	\N	Y		Botanisches Institut, Universität Greifswald	\N	Dipl.-Arb	\N	thesis	2017-03-05 23:17:39.354	\N	2003	\N	\N	f
5147	Muenchmeyer_1996_100660	\N	\N	\N	\N	\N	Bodenbiologische und standortkundliche Untersuchungen an Wald- und Wiesenstandorten des Niedermoores Friedländer Große Wiese	100660		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:20.326	\N	1996	\N	\N	f
5149	Mueller-stoll_Neubauer_1965_100661	\N	\N	\N	\N	\N	Die Pflanzengesellschaften auf Grundwasserstandorten im Bereich der Fercher Berge südwestlich von Potsdam	100661		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:20.579	\N	1965	\N	\N	f
5167	Doing_1969_100667	\N	\N	\N	\N	\N	?	100667		\N	DOI	unpubliziert		Heinken1	\N	\N	74	\N	Y			\N		\N	unpublished	2016-05-04 11:12:21.979	\N	1969	\N	\N	f
5173	Jansen_1984_100669	\N	\N	\N	\N	\N	?	100669		\N	JAS	unpubliziert		Heinken1	\N	\N	29	\N	Y			\N		\N	unpublished	2016-05-04 11:12:22.469	\N	1984	\N	\N	f
5179	Meijer-drees_1936_100671	\N	\N	\N	\N	\N	?	100671		\N	MED	unpubliziert		Heinken2	\N	\N	18	\N	Y			\N		\N	unpublished	2016-05-04 11:12:22.951	\N	1936	\N	\N	f
5195	Vollmar_1943_100677	\N	\N	\N	\N	\N	?	100677		\N	VOM	unpubliziert		Thomax_Box3	\N	\N	0	\N	Y			\N		\N	unpublished	2016-05-04 11:12:24.379	\N	1943	\N	\N	f
5198	Markowski_1982_100678	\N	\N	\N	\N	\N	?	100678		\N	MAK	unpubliziert		Heinken2	\N	\N	29	\N	Y			\N		\N	unpublished	2016-05-04 11:12:24.626	\N	1982	\N	\N	f
87	Grosser_Glotz_1958_000028	\N	\N	\N	\N	\N	Die Vegetationsverhältnisse des Meßtischblattbereiches Weißwasser/Oberlausitz (MBl. 4453)	000028		\N	\N	unpubliziert		\N	\N	\N	202	\N	Y	Unveröff. Mskr. Bericht über den Forschungsauftrag 2155 15 h-8-04 d. Institutes f. Systemaitik u. Pflanzengeographie d. Martin-Luther-Universität Halle-Wittenberg. Bearbeitet am Staatlichen Museum f. Naturkunde Görlitz		\N		\N	unpublished	2016-05-04 11:04:35.842	\N	1958	\N	\N	f
119	Braun-Blanquet_1915_000039	\N	\N	\N	\N	\N	Der Titel ist entweder Braun, J. 1915 Les Cévennes Méridionales oder unbekannt?	000039		\N	\N	\N		LITTAB	\N	\N	13	\N	Y		Publisher	\N		\N	book	2016-05-04 11:04:38.692	\N	1915	\N	\N	f
150	Markgraf_1988_000049	\N	\N	\N	\N	\N	Grünland-Gesellschaften um St. Andreasberg (Oberharz) unter besonderer Berücksichtigung der Brachestadien	000049		\N	\N	\N		LITTAB	\N	\N	246	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 91 S	\N	thesis	2016-05-04 11:04:42.41	\N	1988	\N	\N	f
236	Schwartze_1992_000077	\N	\N	\N	\N	\N	Nordwestdeutsche Feuchtgrünlandgesellschaften unter kontrollierten Nutzungsbedingungen	000077		\N	\N	\N		\N	\N	\N	0	\N	Y	Diss. Bot. 183: 1-196. Berlin, Stuttgart	Diss. Bot. 183: 1-196. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:04:49.488	\N	1992	\N	\N	f
242	Wulf_1992_000079	\N	\N	\N	\N	\N	Vegetationskundliche und ökologische Untersuchungen zum Vorkommen gefährdeter Pflanzenarten in Feuchtwäldern Nordwestdeutschlands	000079		\N	\N	\N		\N	\N	\N	415	\N	Y	Diss. Bot. 185: 1-245. Berlin, Stuttgart	Diss. Bot. 185: 1-245. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:04:49.927	\N	1992	\N	\N	f
291	Weinert_Gulich_1993_000096	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen zur Umweltverträglichkeitsstudie im südharzer Gipsgebiet im Bereich des Bergwerkseigentums Südharzer Gipswerke GmbH Ellrich	000096		doppelte Bibref	\N	\N		\N	\N	\N	17	\N	Y			\N		\N	report	2016-05-04 11:04:54.203	\N	1993	292	\N	f
323	Vogel_1981_000107	\N	\N	\N	\N	\N	Klimabedingungen und Stickstoff- Versorgung von Wiesengesellschaften verschiedener Höhenstufen des Westharzes	000107		\N	\N	\N		\N	\N	\N	32	\N	Y	Diss. Bot. 60: 1-167. Vaduz	Diss. Bot. 60: 1-167. Vaduz	\N		\N	book	2016-05-04 11:04:56.91	\N	1981	\N	\N	f
335	Gradmann_1936_000111	\N	\N	\N	\N	\N	Das Pflanzenleben der Schwäbischen Alb. Erster Band: Pflanzengeographische Darstellung	000111		\N	\N	\N		\N	\N	\N	112	\N	Y	Strecker & Schröder, Stuttgart, 470 S	Strecker & Schröder, Stuttgart, 470 S	\N		\N	book	2016-05-04 11:04:58.077	\N	1936	\N	\N	f
352	Sundermeier_1999_000116	\N	\N	\N	\N	\N	Zur Vegetationsdichte der Xerothermrasen nordwestlich von Halle/Saale	000116		\N	\N	\N		\N	\N	\N	78	\N	Y	Diss. Bot. 316: 1-192. Berlin, Stuttgart	Diss. Bot. 316: 1-192. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:04:59.285	\N	1999	\N	\N	f
356	Hundt_2001_000118	\N	\N	\N	\N	\N	Ökologisch-geobotanische Untersuchungen an den mitteldeutschen Wiesengesellschaften unter besonderer Berücksichtigung ihres Wasserhaushaltes und ihrer Veränderung durch die Intensivbewirtschaftung	000118		\N	\N	\N		\N	\N	\N	76	\N	Y	Mitteilungen aus dem Biosphärenreservat Rhön/Thüringen. 3. Monographie. 366 S	Mitteilungen aus dem Biosphärenreservat Rhön/Thüringen. 3. Monographie. 366 S	\N		\N	book	2016-05-04 11:04:59.775	\N	2001	\N	\N	f
449	Dierssen_1972_000153	\N	\N	\N	\N	\N	Die Vegetation des Gildehauser Venns (Krs. Grafschaft Bentheim)	000153		\N	\N	\N		\N	\N	\N	19	\N	Y			\N	Diss. Universität Hannover	\N	thesis	2016-05-04 11:05:08.327	\N	1972	\N	\N	f
558	Hundt_1969_000193	\N	\N	\N	\N	\N	Wiesenvegetation, Wasserverhältnisse und Ertragsverhältnisse im Rückhaltebecken bei Kelbra an der Helme. Vegetation, Wasserstufen und Bodendurchfeuchtung der Wiesenflächen eines Grabenstauversuchs bei ?	000193		\N	\N	\N		\N	\N	\N	92	\N	Y		Publisher	\N		\N	book	2016-05-04 11:05:17.913	\N	1969	\N	\N	f
562	Hundt_1980_000195	\N	\N	\N	\N	\N	Die Bergwiesen des herzynischen niederösterreichischen Waldviertels in vergleichender Betrachtung mit der Wiesenvegetation der herzynischen Mittelgebirge der DDR (Harz, Thüringer Wald, Erzgebirge)	000195		\N	\N	\N		\N	\N	\N	0	\N	Y	Ph ?	Ph ?	\N		\N	book	2016-05-04 11:05:18.367	\N	1980	\N	\N	f
634	Krausch_1960_000221	\N	\N	\N	\N	\N	Die Vegetation des Spreewaldes	000221		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y			\N		\N	unpublished	2016-05-04 11:05:24.625	\N	1960	\N	\N	f
640	Krausch_1974_000224	\N	\N	\N	\N	\N	Das Eubruch bei Linum	000224		\N	\N	unpubliziert		\N	\N	\N	15	\N	Y			\N		\N	unpublished	2016-05-04 11:05:25.342	\N	1974	\N	\N	f
642	Krause_1972_000225	\N	\N	\N	\N	\N	Laubwaldgesellschaften im östlichen Hunsrück. Natürlicher Aufbau und wirtschaftsbedingte Abwandlungsformen	000225		\N	KR	\N		BOHN	SPFLUME	\N	117	\N	Y	Diss. Bot. 15: 117 S. Berlin, Stuttgart	Diss. Bot. 15: 117 S. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:05:25.604	\N	1972	\N	\N	f
723	Nowak_Schulz_1992_000257	\N	\N	\N	\N	\N	3. Bericht zum Untersuchungsprogramm zur Wirkungskontrolle von Projekten des Vertragsnaturschutzes in Teilen das Regierungsbezirks Freiburg	000257		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y	Mskr. 82 S. Unveröff		\N		\N	unpublished	2016-05-04 11:05:33.336	\N	1992	\N	\N	f
765	Peppler_1992_000275	\N	\N	\N	\N	\N	Die Borstgrasrasen (Nardetalia) Westdeutschlands	000275		\N	\N	0000000002		LITTAB	\N	\N	145	\N	Y	Diss. Bot. 193: 1-404. Berlin, Stuttgart	Diss. Bot. 193: 1-404. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:05:37.841	\N	1992	\N	\N	f
848	Rosenthal_1992_000306	\N	\N	\N	\N	\N	Erhaltung und Regeneration von Feuchtwiesen. Vegetationsökologische Untersuchungen auf Dauerflächen	000306		\N	\N	\N		\N	\N	\N	319	\N	Y	Diss. Bot. 182: 1-283. Berlin, Stuttgart	Diss. Bot. 182: 1-283. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:05:45.56	\N	1992	\N	\N	f
913	Sebald_1983_000331	\N	\N	\N	\N	\N	Erläuterungen zur vegetationskundlichen Karte 1:25 000 Blatt 7919 Mühlheim a. d. Donau. b: Beilagen (Tabellen) zu den Erläuterungen	000331		\N	\N	\N		\N	\N	\N	59	\N	Y	Staatl. Museum f. Naturkunde Stuttgart Stuttgart. 87 S	Staatl. Museum f. Naturkunde Stuttgart Stuttgart. 87 S	\N		\N	book	2016-05-04 11:05:51.84	\N	1983	\N	\N	f
1089	Becker_1994_000393	\N	\N	\N	\N	\N	Vegetation und Flora der Magerrasen auf Zechstein am südlichen Harzrand (Thüringen)	000393		\N	BEC	\N		TABGESNE	\N	\N	206	\N	N			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 193 S	\N	thesis	2016-05-04 11:06:06.956	\N	1994	\N	\N	f
1101	Beinhauer_1965_000398	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des rechten Saalehanges nördlich Rothenburg	000398		\N	BHK	\N		TABGESNE	\N	\N	21	\N	Y			\N	Staatsex.-Arb. Universität Halle. 55 S	\N	thesis	2016-05-04 11:06:08.274	\N	1965	\N	\N	f
1135	Bruelheide_1995_000411	\N	\N	\N	\N	\N	Die Grünlandgesellschaften des Harzes und ihre Standortsbedingungen. Mit einem Beitrag zum Gliederungsprinzip auf der Basis von statistisch ermittelten Artengruppen	000411		\N	BRU	\N		TABGESNE	\N	\N	580	\N	N	Diss. Bot. 244: 1-338. Berlin, Stuttgart	Diss. Bot. 244: 1-338. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:06:11.705	\N	1995	\N	\N	f
1158	Butzke_1992_000420	\N	\N	\N	\N	\N	Halbtrockenrasen des Mesobromion am nordwestlichen Eifelrand, ihre Schutzwürdigkeit und ihre Berücksichtigung in der Landschaftsplanung	000420		\N	BUT	\N		TABGESNE	\N	\N	42	\N	Y			\N	Dipl.-Arb. Landschaftsökol. Landschaftsgestalt., RWTH Aachen	\N	thesis	2016-05-04 11:06:13.915	\N	1992	\N	\N	f
1194	Eichholz_1997_000431	\N	\N	\N	\N	\N	Flora und Vegetation der Wiesen und Magerrasen am Südhang des Hohen Hagen (Landkreis Göttingen)	000431		\N	EIH	\N		TABGESNE	\N	\N	33	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 115 S	\N	thesis	2016-05-04 11:06:16.511	\N	1997	\N	\N	f
1209	Flecks_1981_000436	\N	\N	\N	\N	\N	Flora und Vegetation der Kalkmagerrasen im Bratental bei Göttingen	000436		\N	FLE	\N		TABGESNE	\N	\N	105	\N	Y			\N	Staatsex.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 51 S	\N	thesis	2016-05-04 11:06:17.713	\N	1981	\N	\N	f
1217	Geringhoff_1992_000439	\N	\N	\N	\N	\N	Vegetationsökologische Untersuchungen der Kalkkuppen der Briloner Hochebene, Sauerland	000439		\N	GEF	\N		TABGESNE	\N	\N	55	\N	Y			\N	Dipl.-Arb. Universität Münster. 139 S	\N	thesis	2016-05-04 11:06:18.371	\N	1992	\N	\N	f
1279	Ihl_1994_000459	\N	\N	\N	\N	\N	Grünland und angrenzende Gesellschaften im Gartetal (Landkreis Göttingen)	000459		\N	IHL	\N		TABGESNE	\N	\N	152	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 135 S	\N	thesis	2016-05-04 11:06:23.028	\N	1994	\N	\N	f
1292	Kaiser_1930_000465	\N	\N	\N	\N	\N	Die Steppenheiden in Thüringen und Franken zwischen Saale und Main	000465		\N	KAI	\N		TABGESNE	\N	\N	30	\N	Y	Sonderschrift d. Akademie gemeinnütziger Wissenschaften. Carl Villaret, Erfurt. 75 S	Sonderschrift d. Akademie gemeinnütziger Wissenschaften. Carl Villaret, Erfurt. 75 S	\N		\N	book	2016-05-04 11:06:24.551	\N	1930	\N	\N	f
1306	Knapp_1942_000471	\N	\N	\N	\N	\N	Zur Systematik der Wälder, Zwergstrauchheiden und Trockenrasen des eurosibirischen Vegetationskreises	000471		\N	KNA	unpubliziert		TABGESNE	\N	\N	0	\N	Y	Mskr. (Beilage 12 Rundbr. Zentralstelle Vegetationskartierung Hannover). 102 S		\N		\N	unpublished	2016-05-04 11:06:25.949	\N	1942	\N	\N	f
1344	Lauckner_1989_000488	\N	\N	\N	\N	\N	Vegetationsanalyse des Mühlberges bei Niedersachswerfen	000488		\N	LAU	\N		TABGESNE	\N	\N	12	\N	Y			\N	Dipl.-Arb. Universität Halle. 61 S	\N	thesis	2016-05-04 11:06:30.493	\N	1989	\N	\N	f
1374	Moebus_1985_000500	\N	\N	\N	\N	\N	Trockenrasen und angrenzende Pflanzengesellschaften auf Eruptivgestein im Südwesten Rheinhessens	000500		\N	MOR	\N		TABGESNE	\N	\N	189	\N	Y			\N	Dipl.-Arb. Universität Göttingen. 107. S	\N	thesis	2016-05-04 11:06:33.372	\N	1985	\N	\N	f
1396	Opitz_1993_000508	\N	\N	\N	\N	\N	Die Vegetation der Kalkmagerrasen zwischen Weyer und Zingsheim (Sötenicher Kalkmulde, Eifel) und ihre Schmetterlingsfauna (Rhopalocera, Hesperidae und Zygaenidae)	000508		\N	OPH	\N		TABGESNE	\N	\N	51	\N	Y			\N	Dipl.-Arb. Math.-Nat. Fak., Universität Bonn	\N	thesis	2016-05-04 11:06:35.255	\N	1993	\N	\N	f
1443	Rohde_1996_000525	\N	\N	\N	\N	\N	Flora und Vegetation der Magerrasen und Wälder des "Alten Stolberg" (Südharzer Zechsteingürtel)	000525		\N	RHT	\N		TABGESNE	\N	\N	29	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 205 S	\N	thesis	2016-05-04 11:06:39.953	\N	1996	\N	\N	f
2916	Flintrop_Seifert_1997_001070	\N	\N	\N	\N	\N	Schutzwürdigkeitsgutachten für das geplante NSG "Platzmoos-Tannenmatt"	001070		\N	\N	\N		\N	\N	\N	32	\N	N			\N		\N	report	2016-05-04 11:08:50.567	\N	1997	2917	\N	f
1455	Rost_1994_000529	\N	\N	\N	\N	\N	Die Magerrasen des Devonkalkgebiets um Elbingerode und Rübeland (Harz)	000529		\N	ROT	\N		TABGESNE	\N	\N	199	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 149 S	\N	thesis	2016-05-04 11:06:40.907	\N	1994	\N	\N	f
1471	Schmidt_1992_000535	\N	\N	\N	\N	\N	Vegetation und Flora der Kalkmagerrasen im mittleren Werratal (Thüringen)	000535		\N	SCT	\N		TABGESNE	\N	\N	168	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen 150 S	\N	thesis	2016-05-04 11:06:42.268	\N	1992	\N	\N	f
1477	Schneider_1996_000537	\N	\N	\N	\N	\N	Untersuchungen zur Struktur und Dynamik der Gehölzvegetation an Waldgrenzstandorten auf Zechstein des Kyffhäusergebirges	000537		\N	SCD	\N		TABGESNE	\N	\N	65	\N	Y			\N	Dipl.-Arb. Universität Halle. 100 S	\N	thesis	2016-05-04 11:06:42.782	\N	1996	\N	\N	f
1499	Tigges_1985_000546	\N	\N	\N	\N	\N	Vergleichende floristische, vegetationskundliche und standortkundliche Untersuchungen in ausgewählten süd- und südostniedersächsischen Halbtrockenrasen unter besonderer Berücksichtigung des Naturschutzes. Erläuterungsbericht	000546		\N	TIG	\N		TABGESNE	\N	\N	281	\N	Y			\N		\N	report	2016-05-04 11:06:44.667	\N	1985	1500	\N	f
1514	Weber_1992_000551	\N	\N	\N	\N	\N	Biozönologische Untersuchungen der Vegetation und der tagaktiven Schmetterlingsfauna in unterschiedlich genutzten Kalkmagerrasen der Prümer Kalkmulde (Naturschutzgebiet "Schönecker Schweiz" und Naturschutzgebiet "Niesenberg")	000551		\N	WET	\N		TABGESNE	\N	\N	33	\N	Y			\N	Dipl.-Arb. Math.-Nat. Fak., Universität Bonn	\N	thesis	2016-05-04 11:06:45.923	\N	1992	\N	\N	f
1558	Bohn_1980_000569	\N	\N	\N	\N	\N	?	000569		\N	BO	unpubliziert		BOHN	\N	\N	184	\N	Y	Mskr., Bundesforschungsanstalt f. Naturschutz u. Landschaftsökologie		\N		\N	unpublished	2016-05-04 11:06:50.243	\N	1980	\N	\N	f
1582	Jahn_1972_000578	\N	\N	\N	\N	\N	Forstliche Wuchsraumgliederung und waldbauliche Rahmenplanung in der Nordeifel auf vegetationskundlich-standörtlicher Grundlage	000578		\N	JG	\N		BOHN	\N	\N	404	\N	Y	Diss. Bot. 16: 288 S. Berlin, Stuttgart	Diss. Bot. 16: 288 S. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:06:52.468	\N	1972	\N	\N	f
1652	Meusel_1954_000602	\N	\N	\N	\N	\N	Vegetationskundliche Studien ü?ber mitteleuroäische Waldgesellschaften. 4. Die Laubwaldgesellschaften des Harzgebietes	000602		\N	ME	\N		BOHN	\N	\N	36	\N	Y	Festschrift f. E. Aichinger zum 60. Geburtstag, Sonderfolge d. Schriftenreihe Angewandte Pflanzensoziologie Wien	Festschrift f. E. Aichinger zum 60. Geburtstag, Sonderfolge d. Schriftenreihe Angewandte Pflanzensoziologie Wien	\N		\N	book	2016-05-04 11:06:58.407	\N	1954	\N	\N	f
1663	Passarge_1956_000606	\N	\N	\N	\N	\N	Die Wälder von Magdeburgerforth (NW-Fläming)	000606		\N	PA	\N		BOHN	\N	\N	193	\N	Y	Wissenschaftliche Abhandlungen, Deutsche Akademie d. Landwirtschaftswissenschaften zu Berlin, Nr. 16, 112 S., Akademie-Verl., Berlin	Wissenschaftliche Abhandlungen, Deutsche Akademie d. Landwirtschaftswissenschaften zu Berlin, Nr. 16, 112 S., Akademie-Verl., Berlin	\N		\N	book	2016-05-04 11:06:59.405	\N	1956	\N	\N	f
1709	Trautmann_1960_000624	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte des Staatsforstamtes Schleiden	000624		\N	TR	\N		BOHN	\N	\N	88	\N	Y			\N		\N	report	2016-05-04 11:07:03.816	\N	1960	1710	\N	f
1727	Schreiber_1997_000630	\N	\N	\N	\N	\N	Natur und Vegetation im Natur- und Wirtschaftswald "Hohestein"	000630		\N	\N	\N		Naturwald	Hohestein	\N	68	\N	N			\N	Dipl.-Arb.Institut f. Waldbau, Abt. I, Universität Göttingen	\N	thesis	2016-05-04 11:07:05.26	\N	1997	\N	\N	f
1736	Kohls_1994_000633	\N	\N	\N	\N	\N	Geobotanische Untersuchungen in Wäldern des Forstamtes Sellhorn (Lüneburger Heide)	000633		\N	\N	\N		Naturwald	Ehrhorner D�nen, Meninger Holz	\N	141	\N	N			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen	\N	thesis	2016-05-04 11:07:06.004	\N	1994	\N	\N	f
1745	Lambertz_1993_000636	\N	\N	\N	\N	\N	Vegetation und Vegetationsdynamik im Naturwald "Hünstollen"	000636		\N	\N	\N		Naturwald	H�nstollen	\N	72	\N	N			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen	\N	thesis	2016-05-04 11:07:06.694	\N	1993	\N	\N	f
1754	Albrecht_2000_000639	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen im Naturwaldreservat "Meninger Holz" unter besonderer Berücksichtigung der Vegetationsentwicklung	000639		\N	\N	\N		Naturwald	Meninger Holz	\N	66	\N	N			\N	Dipl.-Arb. FG Naturschutz, FB Biologie, Universität Hamburg	\N	thesis	2016-05-04 11:07:07.444	\N	2000	\N	\N	f
1763	Happe_1995_000642	\N	\N	\N	\N	\N	Vegetation und Standortsverhältnisse im Naturwald und Naturschutzgebiet "Totenberg" (Bramwald)	000642		\N	\N	\N		Naturwald	Totenberg	\N	80	\N	N			\N	Dipl.-Arb. Institut f. Waldbau, Abt. I, Universität Göttingen	\N	thesis	2016-05-04 11:07:08.267	\N	1995	\N	\N	f
1774	Knapp_1944_000646	\N	\N	\N	\N	\N	Vegetationsaufnahmen von Wäldern aus den Raume der mittleren Saale und dem Kyffhäuser	000646		\N	\N	unpubliziert		\N	TV	\N	59	\N	Y	Unveröff. Mskr. Halle (Saale)		\N		\N	unpublished	2016-05-04 11:07:09.224	\N	1944	\N	\N	f
1575	Goennert_1974_000575	\N	\N	\N	\N	\N	Laubwaldgesellschaften der Flottsandgebiete Nordost-Niedersachsens	000575		\N	GN	\N		BOHN	\N	\N	66	\N	Y			\N	Staatsex.-Arb. Göttingen	\N	thesis	2016-05-04 11:06:51.803	\N	1974	\N	\N	f
1851	Brand_2000_000676	\N	\N	\N	\N	\N	Untersuchungen zur synsytematischen Umgrenzung und Untergliederung sowie zur standörtlichen und landschaftsräumlichen Bindung von Feuchtwäldern im norwestdeutschen Tiefland	000676		\N	\N	\N		\N	\N	\N	630	\N	Y	Diss. Bot. 323: 1-344. Berlin, Stuttgart	Diss. Bot. 323: 1-344. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:07:16.492	\N	2000	\N	\N	f
1934	Bettinger_1994_000702	\N	\N	\N	\N	\N	Standörtliche und vegetationskundliche Typisierung der Auenwiesen im Saarland	000702		Auch 1996: Die Auenwiesen d. Saarlandes. Tuexenia 16: 251-297\n	\N	0000000005		\N	TV	\N	447	\N	Y			\N	Diss. Universität Gießen. 143 S	\N	thesis	2016-05-04 11:07:22.986	\N	1994	\N	\N	f
1951	Kessling_1994_000708	\N	\N	\N	\N	\N	Vegetationsaufnahmen und Wildverbiß auf ausgewählten Flächen im Nationalpark Hochharz (Sachsen-Anhalt)	000708		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	Dipl.-Arb. FH Hildesh./Holzminden, FB Forstwirtschaft Göttingen. 45 S. + Anh	\N	thesis	2016-05-04 11:07:24.264	\N	1994	\N	\N	f
1977	Knapp_1945_000720	\N	\N	\N	\N	\N	Über Wald-Gesellschaften auf Zechstein im südlichen Harz-Vorland bei Nordhausen	000720		\N	\N	unpubliziert		\N	\N	\N	21	\N	Y	Mskr. Halle (Saale). 16 S		\N		\N	unpublished	2016-05-04 11:07:26.615	\N	1945	\N	\N	f
2000	Nitschke_1956_000728	\N	\N	\N	\N	\N	Die Wiesengesellschaften des Unteren Unstruttales zwischen Artern und Naumburg (Saale)	000728		\N	\N	\N		\N	\N	\N	82	\N	Y			\N	Dipl.-Arb. Universität Halle. 71 S	\N	thesis	2016-05-04 11:07:28.417	\N	1956	\N	\N	f
2076	Bode_2005_000756	\N	\N	\N	\N	\N	Subrezenter Vegetations- und Landschaftswandel im Südschwarzwald . untersucht am Beispiel des Menzenschwander Tals	000756		\N	\N	\N		\N	TV	\N	179	\N	Y			\N	Diss. Universität Freiburg, Fak. Biologie. 145 S	\N	thesis	2016-05-04 11:07:35.626	\N	2005	\N	\N	f
2113	Schuster_1980_000769	\N	\N	\N	\N	\N	Analyse und Bewertung von Pflanzengesellschaften im Nördlichen Frankenjura. - Ein Beitrag zum Problem der Quantifizierung unterschiedlich anthropogen beeinflußter Ökosysteme	000769		\N	\N	\N		\N	\N	\N	291	\N	Y	Diss. Bot. 53: 464 S. Vaduz	Diss. Bot. 53: 464 S. Vaduz	\N		\N	book	2016-05-04 11:07:38.638	\N	1980	\N	\N	f
2180	Kassebaum_2007_000793	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen an Kiefernwäldern der "Oberen Senne"	000793		\N	\N	\N		\N	\N	\N	102	\N	Y			\N	Dipl.-Arb. Universität Kiel, Biologie. 90 S	\N	thesis	2016-05-04 11:07:44.332	\N	2007	\N	\N	f
2199	Scheunert_1999_000800	\N	\N	\N	\N	\N	Flora und Vegetation in den Naturwäldern Limker Strang und Dreyberg (Solling)	000800		\N	\N	\N		Naturwald	Limker Strang, Dreyberg	\N	184	\N	N			\N	Dipl.-Arb. Institut f. Waldbau, Abt. I, Universität Göttingen	\N	thesis	2016-05-04 11:07:45.88	\N	1999	\N	\N	f
2208	Kompa_2004_000803	\N	\N	\N	\N	\N	Die Initialphase der Vegetationsentwicklung nach Windwurf in Buchen-Wäldern auf Zechstein- und Buntsandstein-Standorten des südwestlichen Harzvorlandes	000803		\N	\N	\N		Naturwald	K�nigsbuche	\N	156	\N	N			\N	Dipl.-Arb. Institut f. Waldbau, Abt. I, Universität Göttingen	\N	thesis	2016-05-04 11:07:46.626	\N	2004	\N	\N	f
2211	Emmerich_1997_000804	\N	\N	\N	\N	\N	Die Vegetation im Naturwaldreservat Sonnenkopf im Vergleich zu benachbarten Waldbeständen des Nationalparks Niedersächsischer Harz	000804		\N	\N	\N		Naturwald	Sonnenkopf	\N	232	\N	N			\N	Dipl.-Arb. Institut f. Waldbau, Abt. I, Universität Göttingen	\N	thesis	2016-05-04 11:07:46.928	\N	1997	\N	\N	f
3865	Kloss_1963_100147	\N	\N	\N	\N	\N	Die Vegetation der Friedländer Großen Wiese unter Berücksichtigung von Kalkflachmoorstandorten ostmecklenburgischer Flußtäler	100147		\N	\N	\N		\N	\N	\N	432	432	Y			\N	Universität Greifswald, Diss. A	\N	thesis	2016-05-04 11:10:19.885	\N	1963	\N	\N	f
2301	Benz_1994_000842	\N	\N	\N	\N	\N	Die Pflanzengesellschaften und Tagfaltergemeinschaften eines Kalkmagerrasen-Gehölz-Komplexes einschließlich ihrer phänologischen Entwicklung und Wechselbeziehungen, untersucht am Beispiel des Apfelberges (Taubertal, Baden Württemberg)	000842		\N	\N	\N		\N	\N	\N	58	\N	Y			\N	Dipl.-Arb. Syst.-Geobot. Institut, Universität Göttingen. 192 S	\N	thesis	2016-05-04 11:07:55.786	\N	1994	\N	\N	f
2306	Nowak_Schulz_2002_000844	\N	\N	\N	\N	\N	Wiesen. Nutzung, Vegetation, Biologie und Naturschutz am Beispiel der Wiesen des Südschwarzwaldes und Hochrheingebietes	000844		\N	\N	\N		\N	\N	\N	193	\N	Y	Hrsg.: Landesanstalt f. Umweltschutz Baden-Württemberg. Naturschutz-Spectrum Themen 93. 368 S. Ubstadt-Weiher	Hrsg.: Landesanstalt f. Umweltschutz Baden-Württemberg. Naturschutz-Spectrum Themen 93. 368 S. Ubstadt-Weiher	\N		\N	book	2016-05-04 11:07:56.254	\N	2002	\N	\N	f
2350	Trautmann_1973_000861	\N	\N	\N	\N	\N	Vegetationskarte der Bundesrepublik Deutschland 1:200000-Potentielle natürliche Vegetation-Blatt CC 5502 Köln	000861		\N	\N	\N		\N	\N	\N	274	\N	Y	Bundesanstalt f. Vegetationskunde, Naturschutz u. Landschaftspflege	Bundesanstalt f. Vegetationskunde, Naturschutz u. Landschaftspflege	\N		\N	book	2016-05-04 11:08:00.312	\N	1973	\N	\N	f
2383	Amarell_2000_000873	\N	\N	\N	\N	\N	Kiefernforste der Dübener Heide. Ursachen und Verlauf der Entstehung und Veränderung von Forstgesellschaften	000873		Orig.-Aufn. i. Diss von 1998	\N	\N		HEINKEN_DIP	\N	\N	511	\N	Y	Diss. Bot. 325: 1-246. (Orig.-Aufn. i. Diss von 1998	Diss. Bot. 325: 1-246. (Orig.-Aufn. i. Diss von 1998	\N		\N	book	2016-05-04 11:08:03.322	\N	2000	\N	\N	f
2470	Braun-Blanquet_1948_000905	\N	\N	\N	\N	\N	La végétation alpine des Pyrénées orientales	000905		\N	\N	0000000008		\N	\N	\N	0	\N	Y	Monogr. Estacion est. piren., Barcelona. Comm. Sigma 98, 360 S	Monogr. Estacion est. piren., Barcelona. Comm. Sigma 98, 360 S	\N		\N	book	2016-05-04 11:08:11.175	\N	1948	\N	\N	f
2493	Pawlowskki_Sokolowski_Wallisch_1928_000913	\N	\N	\N	\N	\N	Die Pflanzenassoziationen des Tatra-Gebirges - VII. Teil: Die Pflanzenassoziationen und die Flora des Morskie Oko-Tales	000913		\N	\N	\N		\N	\N	\N	0	\N	Y	Bull. Int. l∩Academie Polonaise Sci. Lettr., Cl. Sci. Math. Nat., Ser. B, Cracovie	Bull. Int. l∩Academie Polonaise Sci. Lettr., Cl. Sci. Math. Nat., Ser. B, Cracovie	\N		\N	book	2016-05-04 11:08:13.111	\N	1928	\N	\N	f
2519	Feldner_1978_000923	\N	\N	\N	\N	\N	Waldgesellschaften, Wald- und Forstgeschichte und Schlußfolgerungen für die waldbauliche Planung im N.S.G. Ammergauer Berge	000923		\N	\N	\N		\N	\N	\N	494	\N	Y			\N	Diss. Wien. 369 S	\N	thesis	2016-05-04 11:08:15.465	\N	1978	\N	\N	f
2557	Pfadenhauer_1975_000939	\N	\N	\N	\N	\N	Beziehungen zwischen Standortseinheiten, Klima, Stickstoffernährung und potentieller Wuchsleistung der Fichte im bayerischen Flyschgebiet	000939		\N	\N	\N		\N	\N	\N	168	\N	Y	Diss. Bot. 30: 239 S	Diss. Bot. 30: 239 S	\N		\N	book	2016-05-04 11:08:19.071	\N	1975	\N	\N	f
2601	Bach_1992_000957	\N	\N	\N	\N	\N	Vegetationsökologische Untersuchungen der Wälder des Renautales, Hochsauerland, unter besonderer Berücksichtigung der Epiphyten	000957		\N	\N	\N		\N	\N	\N	236	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:22.91	\N	1992	\N	\N	f
2619	Hallmann_2002_000963	\N	\N	\N	\N	\N	Untersuchungen zur Auswirkung von Wildverbiss auf Flora und Vegetation des Luerwaldes bei Arnsberg Hochsauerlandkreis/Märkischer Kreis)	000963		\N	\N	\N		\N	\N	\N	82	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:24.281	\N	2002	\N	\N	f
2643	Linnemann_1995_000971	\N	\N	\N	\N	\N	Wälder, Bäume und Epiphyten im Negertal, Hochsauerland - eine vegetationsökologische Untersuchung	000971		\N	\N	\N		\N	\N	\N	128	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:26.164	\N	1995	\N	\N	f
2661	Schnell_1998_000977	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen: Der Scheffelberg bei Brilon, Hochsauerlandkreis, unter besonderer Berücksichtigung des Kleinen Scheffelberges	000977		\N	\N	\N		\N	\N	\N	32	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:27.626	\N	1998	\N	\N	f
2674	Wilhelm_1995_000981	\N	\N	\N	\N	\N	Untersuchungen zur Ökologie von Viola guestphalica Nauenburg: Vergesellschaftung und Struktur der Vegetation	000981		\N	\N	\N		\N	\N	\N	118	\N	Y	Untersuchungen zur Ökologie von Viola guestfalica Nauenburg: Vergesellschaftung u. Struktur d. Vegetation	Untersuchungen zur Ökologie von Viola guestfalica Nauenburg: Vergesellschaftung u. Struktur d. Vegetation	\N		\N	book	2016-05-04 11:08:28.52	\N	1995	\N	\N	f
3905	Krisch_1972_100167	\N	\N	\N	\N	\N	Geobotanische Untersuchungen des Grünlandes im Rycktalzungenbecken	100167		\N	\N	\N		\N	\N	\N	64	64	Y			\N	Universität Greifswald, Diss. A	\N	thesis	2016-05-04 11:10:24.319	\N	1972	\N	\N	f
2401	Foerster_1968_000879	\N	\N	\N	\N	\N	Über xerotherme Eichenmischwälder des deutschen Mittelgebirgsraumes	000879		\N	\N	\N		HEINKEN_DIP	\N	\N	10	\N	Y			\N	Diss. Universität Göttingen, 424 S. + Anhang, Hann. Münden	\N	thesis	2016-05-04 11:08:04.73	\N	1968	\N	\N	f
2758	Junglas_1988_001014	\N	\N	\N	\N	\N	Eichen-reiche Laubmischwald-Gesellschaften im Unteren Werrabergland	001014		\N	\N	\N		\N	\N	\N	83	\N	N			\N	Dipl.-Arb. Syst.-Geobot. Institut Universität Göttingen: 98 S	\N	thesis	2016-05-04 11:08:36.652	\N	1988	\N	\N	f
2807	Dinter_1982_001033	\N	\N	\N	\N	\N	Waldgesellschaften der Niederrheinischen Sandplatten	001033		\N	\N	\N		\N	\N	\N	313	\N	Y	Diss. Bot. 64: 1-111. Vaduz	Diss. Bot. 64: 1-111. Vaduz	\N		\N	book	2016-05-04 11:08:41.208	\N	1982	\N	\N	f
2813	Finkeldey_1954_001035	\N	\N	\N	\N	\N	Die Pflanzengesellschaften und Böden der Wälder im Bereich der Wupper und einiger Nachbargebiete	001035		\N	\N	\N		\N	\N	\N	60	\N	Y			\N	Diss. Universität Köln: 196 S	\N	thesis	2016-05-04 11:08:41.655	\N	1954	\N	\N	f
2828	Heinken_1992_001041	\N	\N	\N	\N	\N	Relevés from Heinken (1995) which are not published there (only included in synoptic tables). Vegetationsaufnahmen Buchenwälder Teutoburger Wald, Wiehengebierge & Rehunger Berge, Aufnahmejahr 1991	001041		\N	\N	unpubliziert		\N	\N	\N	37	\N	Y	Unveröff. Tab-Tabelle von (Thilo Heinken "hei95a26")		\N		\N	unpublished	2016-05-04 11:08:43.162	\N	1992	\N	\N	f
2889	Sperle_Seifert_Rennwald_2006_001061	\N	\N	\N	\N	\N	Erfolgskontrolle der Ausgleichsmaßnahmen im Projektgebiet "Rittmatten" im Jahr 2004	001061		\N	\N	\N		\N	\N	\N	13	\N	N			\N		\N	report	2016-05-04 11:08:48.203	\N	2006	2890	\N	f
2926	Sayer_2000_001073	\N	\N	\N	\N	\N	Die Ökologie der Flaumeiche (Quercus pubescens Willd.) und ihrer Hybriden auf Kalkstandorten an ihrer nördlichen Arealgrenze	001073		\N	\N	\N		\N	\N	\N	254	\N	Y	Diss. Bot. 340: 1-198	Diss. Bot. 340: 1-198	\N		\N	book	2016-05-04 11:08:51.298	\N	2000	\N	\N	f
2986	Sperle_2004_001093	\N	\N	\N	\N	\N	Steppenkiefernwälder im Planungskorridor der A44 im Raum Sontra	001093		\N	\N	unpubliziert		\N	\N	\N	4	\N	N	Unveröff. Vegetationsaufnahmen		\N		\N	unpublished	2016-05-04 11:08:56.7	\N	2004	\N	\N	f
3005	Schnelle_1972_001100	\N	\N	\N	\N	\N	Standortsuntersuchungen zur Aufklärung der Beziehungen zwischen Grundwassergang und Vegetationsdifferenzierung in der Elster-Luppe-Aue	001100		\N	\N	\N		\N	\N	\N	104	\N	Y			\N	Diss. Universität Halle	\N	thesis	2016-05-04 11:08:58.567	\N	1972	\N	\N	f
3031	Schnelle_1976_001109	\N	\N	\N	\N	\N	Die Pflanzen- und Forstgesellschaften des Naturschutzgebietes "Steckby-Lödderitzer Forst"	001109		\N	\N	\N		TV	\N	\N	178	\N	Y			\N	Dipl.-Arb. Universität Halle, Sekt. Biowissenschaften	\N	thesis	2016-05-04 11:09:00.776	\N	1976	\N	\N	f
3034	Rombach_1999_001110	\N	\N	\N	\N	\N	Auswirkungen verschiedener Formen der Bewirtschaftung von Halbtrockenrasen auf die Zikaden (Homoptera, Auchenorrhyncha) am Beispiel der Enzian-Schillergras-Rasen (Gentiano-Koelerietum) der Nordeifel (Nordrhein-Westfalen)	001110		\N	\N	\N		TV	\N	\N	70	\N	Y			\N	Diss. Universität Bonn	\N	thesis	2016-05-04 11:09:01.03	\N	1999	\N	\N	f
3128	Krumbiegel_1986_001143	\N	\N	\N	\N	\N	Floristische Untersuchungen im Gebiet der Fuhneaue zwischen Gröbzig und Zehbitz (Saalkreis, Kreise Köthen und Bitterfeld) und/oder Vegetationsaufnahmen 1983 für das LAU-Sachsen-Anhalt	001143		\N	\N	\N		LAU_ST	\N	\N	230	\N	N			\N	Dipl.-Arb. PH Köthen	\N	thesis	2016-05-04 11:09:09.223	\N	1986	\N	\N	f
3139	Amarell_1992_001147	\N	\N	\N	\N	\N	Vegetationsdynamik in Segetalbiozönosen des Gebietes um Friedeburg (Saale)	001147		\N	\N	\N		LAU_ST	\N	\N	98	\N	N			\N	Wiss. Hausarb. Universität Halle	\N	thesis	2016-05-04 11:09:10.179	\N	1992	\N	\N	f
3158	Wolf_1995_001154	\N	\N	\N	\N	\N	Untersuchungen zur Diversität von Flora und Vegetation im städtischen Randbereich von Halle-Neustadt	001154		\N	\N	\N		LAU_ST	\N	\N	85	\N	N			\N	Wiss. Hausarb. Universität Halle	\N	thesis	2016-05-04 11:09:12.237	\N	1995	\N	\N	f
4320	Griesel_Et_Al._1993_100344	\N	\N	\N	\N	\N	Die Vegetation des Ralower Holzes	100344		\N	\N	\N		\N	\N	\N	84	84	Y			\N		\N	report	2016-05-04 11:11:07.597	\N	1993	4321	\N	f
3202	Knapp_1944_001171	\N	\N	\N	\N	\N	Über die Fundorte der Flaum-Eiche (Quercus pubescens Willd.) und der wimperblättrigen Segge (Carex pilos Soop.) bei Jena in Thüringen und ihre Vegetation	001171		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y	Mskr. Halle (Saale). 6 S		\N		\N	unpublished	2016-05-04 11:09:16.769	\N	1944	\N	\N	f
3114	Kossak_1999_001138	\N	\N	\N	\N	\N	Gutachten für LAU oder Populationsbiologische Untersuchungen auf Schwermetallhalden in Mitteldeutschland	001138		\N	\N	\N		LAU_ST	\N	\N	64	\N	N			\N	Dipl.-Arb. Universität Halle, FB Biologie. 66 S	\N	thesis	2016-05-04 11:09:08.098	\N	1999	\N	\N	f
3353	Suk_1992_001218	\N	\N	\N	\N	\N	Die Vegetation des Gebietes der Weißen Elster zwischen Zeitz und Wetterzeube	001218		\N	\N	\N		\N	\N	\N	368	\N	N			\N	Dipl.-Arb. Geobotanik, Universität Halle. 97 S	\N	thesis	2016-05-04 11:09:28.266	\N	1992	\N	\N	f
3373	Lange_1967_001226	\N	\N	\N	\N	\N	Die Feuchtigkeitsverhältnisse unter den Wiesengesellschaften des westlichen Teiles der Elster-Luppe-Aue	001226		\N	\N	\N		\N	\N	\N	80	\N	Y			\N	Dipl.-Arb. Institut f. Systematische Botanik und Pflanzengeographie Universität Halle. 66 S. und Anhang	\N	thesis	2016-05-04 11:09:30.062	\N	1967	\N	\N	f
3399	Lemme_1983_001236	\N	\N	\N	\N	\N	Das Naturschutzgebiet "Arneburger Hang"	001236		\N	\N	\N		Lemme_1983	\N	\N	87	\N	N			\N	Dipl.-Arb. Universität Halle	\N	thesis	2016-05-04 11:09:32.358	\N	1983	\N	\N	f
3699	Henker_1972_100074	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen in der nordwestmecklenburgischen Jungmoränenlandschaft	100074		\N	\N	\N		\N	\N	\N	1078	1078	Y			\N	Universität Greifswald, Diss. A	\N	thesis	2016-05-04 11:10:01.499	\N	1972	\N	\N	f
3433	Taeuber_2000_001247	\N	\N	\N	\N	\N	Zwergbinsen-Gesellschaften (Isoeto-Manojuncetea) in Niedersachsen. Verbreitung, Gliederung, Dynamik, Keimungsbedingungen der Arten und Schutzkonzepte	001247		auch Diss. Univ. Göttingen	\N	0000000012		T�uber_2000	\N	\N	429	\N	Y	Cuvillier-Verlag Göttingen, 228. S	Cuvillier-Verlag Göttingen, 228. S	\N		\N	book	2016-05-04 11:09:34.719	\N	2000	\N	\N	f
3356	Kuehn_1997_001219	\N	\N	\N	\N	\N	Renaturierung artenarmer Glatthaferwiesen im Tertiärhügelland	001219		\N	\N	\N		\N	\N	\N	43	\N	Y			\N	Diss. Fak. Landwirtschaft und Gartenbau TU München	\N	thesis	2016-05-04 11:09:28.477	\N	1997	\N	\N	f
4065	Schmidt_1977_100241	\N	\N	\N	\N	\N	Pflanzensoziologische und ökologische Untersuchungen der Gewässer um Güstrow	100241		auch: Natur u. Naturschutz i. Mecklenburg-Vorpommern, 17, 3-130.	\N	\N		\N	\N	\N	562	562	Y			\N	Universität Greifswald, Diss. A	\N	thesis	2016-05-04 11:10:42.004	\N	1977	\N	\N	f
4117	Succow_1970_100264	\N	\N	\N	\N	\N	Die Vegetation nordmecklenburgischer Flußtalmoore und ihre anthropogene Umwandlung	100264		\N	\N	\N		\N	\N	\N	904	\N	Y			\N	Universität Greifswald, Diss. A	\N	thesis	2016-05-04 11:10:47.522	\N	1970	\N	\N	f
4351	Henker_Henker_1993_100353	\N	\N	\N	\N	\N	Effizienzkontrolle zur naturschutzgerechten Grünlandnutzung im NSG "Rustwerder/Boinsdorfer Werder"	100353		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	report	2016-05-04 11:11:09.804	\N	1993	4352	\N	f
4471	Paulson_Raskin_1995_100389	\N	\N	\N	\N	\N	Bestandserfassung und Bewertung der Vegetation auf der Insel Großer Werder im Nationalpark Vorpommersche Boddenlandschaft	100389		\N	\N	\N		\N	\N	\N	190	190	Y			\N		\N	report	2016-05-04 11:11:18.651	\N	1995	4472	\N	f
4484	Polte_1996_100393	\N	\N	\N	\N	\N	Vegetationsökologische Untersuchungen zur Bewertung der biologischen Vielfalt an der Binnenküste Mönchguts (Rügen)	100393		\N	\N	\N		\N	\N	\N	641	641	Y			\N	Dipl.-Arb. Bot. Institut, Universität Greifswald	\N	thesis	2016-05-04 11:11:19.571	\N	1996	\N	\N	f
4506	Schacht_1994_100401	\N	\N	\N	\N	\N	Kreidebrüche auf der Halbinsel Jasmund (Rügen) - vegetationskundliche Untersuchung mit Hinweisen zu Schutz- und Pflegemaßnahmen	100401		\N	\N	\N		\N	\N	\N	155	155	Y			\N	Dipl.-Arb. TU Berlin, Institut f. Ökologie	\N	thesis	2016-05-04 11:11:21.578	\N	1994	\N	\N	f
4659	Bochnig_1957_100451	\N	\N	\N	\N	\N	Forstliche Vegetations- und Standortuntersuchungen in der Universitätsforst Greifswald	100451		\N	\N	\N		\N	\N	\N	289	289	Y			\N	Universität Greifswald, Diss	\N	thesis	2016-05-04 11:11:35.116	\N	1957	\N	\N	f
5381	Tuexen_null_101001	\N	\N	\N	\N	\N	unbekannt	101001	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:11.736	\N	1500	\N	\N	t
4687	Langrock_1997_100459	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen von Trockenrasen und Brachflächen auf der Halbinsel Buhlitz (Rügen)	100459		\N	\N	\N		\N	\N	\N	235	235	Y			\N	Dipl.-Arb. Universität Oldenburg	\N	thesis	2016-05-04 11:11:37.108	\N	1997	\N	\N	f
4385	Kaestner_Koch_Brielmann_1995_100363	\N	\N	\N	\N	\N	Bericht über Bestandsuntersuchungen an den Biotopen, Pflanzen und Tieren des Naturschutzgebietes "Müritz-Steilufer bei Rechlin"	100363		\N	\N	\N		\N	\N	\N	48	48	Y			\N		\N	report	2016-05-04 11:11:12.305	\N	1995	4386	\N	f
4539	Voigtlaender_1993_100411	\N	\N	\N	\N	\N	Hydrologische und vegetationskundliche Kartierung des Darzer Moores, des Zerninsee-Moores und des Thurbruches als Vorstudie für ein Renaturierungskonzept	100411		\N	\N	\N		\N	\N	\N	48	48	Y			\N		\N	report	2016-05-04 11:11:24.015	\N	1993	4540	\N	f
4325	Gruenbauer_Cheung_1994_100345	\N	\N	\N	\N	\N	Geobotanische Gebietsanalyse des Naturschutzgebietes Anklamer Stadtbruch (Mecklenburg- Vorpommern)	100345		\N	\N	\N		\N	\N	\N	293	293	Y			\N	Dipl.-Arb. Bot. Institut, Universität Greifswald	\N	thesis	2016-05-04 11:11:07.855	\N	1994	\N	\N	f
4771	Barth_1997_100494	\N	\N	\N	\N	\N	Vegetationskundlich-ökologische Untersuchungen zur Charakterisierung offener Maschinentorfstiche im Recknitz-Flußtalmoor	100494		\N	\N	\N		\N	\N	\N	55	55	Y			\N	Universität Greifswald	\N	thesis	2016-05-04 11:11:45.636	\N	1997	\N	\N	f
4794	Mey_1998_100502	\N	\N	\N	\N	\N	Untersuchungen der Auswirkung von Seenverbindungen in kleinen Seenverbundsystemen	100502		\N	\N	\N		\N	\N	\N	96	96	Y			\N	Universität Greifswald	\N	thesis	2016-05-04 11:11:47.425	\N	1998	\N	\N	f
4811	Mecklenburg-vorpommern_1998_100509	\N	\N	\N	\N	\N	Erfassung vom Aussterben bedrohter Arten der Feucht- und Waldbiotope	100509		\N	\N	unpubliziert		\N	\N	\N	763	763	Y	\N		\N		\N	unpublished	2016-05-04 11:11:49.109	\N	1998	\N	\N	f
4704	Degen_Al._(biota_Gmbh)_1997_100466	\N	\N	\N	\N	\N	Gewässerentwicklungsplan "Obere Mildenitz"	100466		\N	\N	\N		\N	\N	\N	100	100	Y			\N		\N	report	2016-05-04 11:11:38.868	\N	1997	4705	\N	f
4870	Schwarz_Dietrich_1996_100534	\N	\N	\N	\N	\N	Untersuchungen in dem NSG "Grünzer Berge" und den geplanten Erweiterungsflächen	100534		\N	\N	unpubliziert		\N	\N	\N	127	127	Y	\N		\N		\N	unpublished	2016-05-04 11:11:55.589	\N	1996	\N	\N	f
4897	Pless_1994_100551	\N	\N	\N	\N	\N	Pflanzensoziologische Untersuchngen der Trockenrasen an den Hängen des Odertales im Kreis Seelow (Brandenburg)	100551		\N	\N	\N		DUBLETTE000513	\N	\N	210	210	Y			000513	Georg-August-Universität, Institut f. Systematik u. Geobotanik	\N	thesis	2016-05-04 11:11:57.937	\N	1994	\N	\N	f
4935	Raabe_1944_100570	\N	\N	\N	\N	\N	Über Pflanzengesellschaften der Umgebung von Wolgast in Pommern	100570		\N	\N	\N		\N	\N	\N	0	\N	Y	Rundbrief d. Zentralstelle f. Vegetationskartierung	Rundbrief d. Zentralstelle f. Vegetationskartierung	\N		\N	book	2016-05-04 11:12:02.455	\N	1944	\N	\N	f
4958	Gottschling_1995_100593	\N	\N	\N	\N	\N	Bodenbiologische und standortskundliche Untersuchungen an Niedermoorstandorten der Friedländer Großen Wiese	100593		\N	\N	\N		\N	\N	\N	1	1	Y			\N	Dipl.-Arb. Bot. Institut, EMA-Universität Greifswald: 91 S	\N	thesis	2016-05-04 11:12:04.135	\N	1995	\N	\N	f
4969	Fischer_Vegelin_1997_100597	\N	\N	\N	\N	\N	Landschaftsanalyse zur Renaturierung des Recknitztales. EU-LIFE-Projekt "Erhaltung und Wiederherstellung des Trebeltalmoores einschließlich vorbereitender Untersuchungen für das Recknitztalmoor"	100597		\N	\N	unpubliziert		\N	\N	\N	1	1	Y	Unveröff. Forschungsprojekt		\N		\N	unpublished	2016-05-04 11:12:05.149	\N	1997	\N	\N	f
5006	Herzberg_1998_100610	\N	\N	\N	\N	\N	Wie wirkt sich der Badeverkehr auf die Unterwasservegetation, die Diasporen und das Sediment mesotroph-alkalischer Klarwasserseen an den Beispielen Wittwesee, Krummer See und Stechlinsee aus?	100610		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:08.073	\N	1998	\N	\N	f
5033	Manthey_1996_100619	\N	\N	\N	\N	\N	Standorts- und vegetationskundliche Untersuchungen an Ackerbrachen im Stechlinsee-Gebiet (Nordbrandenburg) unter besonderer Berücksichtigung der Erosionsprozesse	100619		\N	\N	\N		\N	\N	\N	34	\N	Y			\N	Diplomarbeit	\N	thesis	2016-05-04 11:12:10.534	\N	1996	5034	\N	f
5066	Seiberling_1997_100630	\N	\N	\N	\N	\N	Landschaftsökologische Untersuchungen an entwässerten Mooren einer Agrarlandschaft im Biosphärenreservat Schorfheide-Chorin / Brandenburg	100630		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:13.402	\N	1997	\N	\N	f
5103	Seiberling_2003_100643	\N	\N	\N	\N	\N	Auswirkungen veränderter Überflutungsdynamik auf Polder- und Salzgraslandvegetation der Vorpommerschen Bodedenlandschaft	100643		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:16.501	\N	2003	\N	\N	f
5164	Drafehn_2003_100666	\N	\N	\N	\N	\N	Bewirtschaftung und vegetationsökologische Bewertung ertragsschwacher Sandäcker in Südwest-Mecklenburg	100666		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:21.779	\N	2003	\N	\N	f
1528	Wersche_Gmbh_1991_000556	\N	\N	\N	\N	\N	Antrag auf Genehmigung nach ⌡ 19 Bundesimmissionsschutzgesetz für die Ausbeutung der Lagerstätte Truppenübungsplatz Osterode. Teilantrag 1: Kreuzstiege. Teil B: Bestandeserhebungen und Bewertung, Floristisch-Faunistische Erfassung	000556		\N	WRS	\N		TABGESNE	\N	\N	15	\N	Y			\N		\N	report	2016-05-04 11:06:47.084	\N	1991	1529	\N	f
5190	Goers_1955_100675	\N	\N	\N	\N	\N	?	100675		\N	GOE	unpubliziert		Thomas_Box3	\N	\N	8	\N	Y			\N		\N	unpublished	2016-05-04 11:12:23.893	\N	1955	\N	\N	f
2328	Luckwald_Et_Al._1992_000853	\N	\N	\N	\N	\N	Gutachten im Auftr. der Bezirksregierung Hannover, Materialband (Hellental im Solling)	000853		\N	\N	unpubliziert		\N	\N	\N	2	\N	Y	Unveröff		\N		\N	unpublished	2016-05-04 11:07:58.413	\N	1992	\N	\N	f
2366	Von_Rochow_1951_000867	\N	\N	\N	\N	\N	Die Vegetation des Kaiserstuhls. Pflanzensoziologische Gebitsmonographie mit einer Karte der Pflanzengesellschaften im Maßstab 1:25.000	000867		\N	\N	\N		\N	\N	\N	13	\N	Y			\N	Diss. Universität Freiburg. 255 S	\N	thesis	2016-05-04 11:08:01.793	\N	1951	\N	\N	f
2722	Zum_Felde_1999_000999	\N	\N	\N	\N	\N	Untersuchungen zur Vegetationsstruktur auf Binnendünen und vegetationskundliche Einordnung der Kiefernwälder der Elbtaldünen zwischen Neuhaus und Dömitz	000999		\N	\N	\N		HEINKEN_DIP	\N	\N	99	\N	N			\N	Dipl.-Arb. Universität Göttingen	\N	thesis	2016-05-04 11:08:33.072	\N	1999	\N	\N	f
4327	Hacker_Knapp_Et_Al._1990_100346	\N	\N	\N	\N	\N	Die Vegetation des Naturschutzgebietes Insel Vilm	100346		\N	\N	\N		\N	\N	\N	12	12	Y			\N		\N	report	2016-05-04 11:11:08.088	\N	1990	4328	\N	f
4390	Kinder_Mittendorf_Et_Al._1994_100364	\N	\N	\N	\N	\N	Halbinsel Gnitz - Ein Beitrag zum Landschaftsplan	100364		\N	\N	\N		\N	\N	\N	50	50	Y			\N	Institut f. Landschaftspflege u. Naturschutz d. Universität Hannover	\N	thesis	2016-05-04 11:11:12.588	\N	1994	\N	\N	f
4404	Kirsch_Et_Al._1993_100370	\N	\N	\N	\N	\N	Grundlagenerhebungen für ein Schutz-, Pflege- und Entwicklungskonzept für den Woezer See und angrenzende Flächen, einschlißlich Kübenmoor	100370		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	report	2016-05-04 11:11:14.111	\N	1993	4405	\N	f
4453	Naturschutzgesellschaft_Hiddensee_Und_Boddenlandschaft_1993_100384	\N	\N	\N	\N	\N	Kartierung Dünenheide	100384		\N	\N	\N		\N	\N	\N	82	82	Y			\N		\N	report	2016-05-04 11:11:17.466	\N	1993	4454	\N	f
4456	Naturschutzgesellschaft_Hiddensee_Und_Boddenlandschaft_1993_100385	\N	\N	\N	\N	\N	Kartierung Nordgellen	100385		\N	\N	\N		\N	\N	\N	66	66	Y			\N		\N	report	2016-05-04 11:11:17.724	\N	1993	4457	\N	f
5161	Litterski_Et_Al._2002_100665	\N	\N	\N	\N	\N	Unpublizierte Aufnahmen EASE Projekt	100665		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y	Unveröff		\N		\N	unpublished	2016-05-04 11:12:21.526	\N	2002	\N	\N	f
3499	Bfg_Koblenz_2011_001271	\N	\N	\N	\N	\N	Vegetationsaufnahmen an der Elbe bei Coswig	001271		\N	\N	\N		Elbe_Coswig	\N	Bfg Koblenz	241	\N	Y			\N		\N	report	2016-12-22 07:27:07.291	\N	2011	3500	\N	f
4239	Abel_Hippke_Rieger_Et_Al._1995_100318	\N	\N	\N	\N	\N	Renaturierungs- und Pflegeplanung für Grünland und Quellbereiche im LSG "Landgrabental" mit Untersuchungen zur Flora und Vegetation sowie Laufkäfer- und Spinnenfauna	100318		\N	\N	\N		\N	\N	\N	18	18	Y			\N		\N	report	2016-05-04 11:11:01.435	\N	1995	4240	\N	f
4425	Machatzki_Et_Al._1994_100376	\N	\N	\N	\N	\N	Floristisch- vegetationskundliche Untersuchung von ausgewählten Flächen auf der Insel Ummanz (Westrügen), ihrer Nebeninseln sowie der Insel Liebitz	100376		\N	\N	\N		\N	\N	\N	175	175	Y			\N		\N	report	2016-05-04 11:11:15.549	\N	1994	4426	\N	f
4464	Paulson_Lennartz_Et_Al._1994_100387	\N	\N	\N	\N	\N	Vegetationskundliche Erfassung und Bewertung im Nationalpark Vorpommersche Boddenlandschaft 1993 - Insel Kirr, Insel Oie, Schmidtbülten, Salzgrasland am Prerowstrom, Grünland am Prerowstrom, Sundische	100387		\N	\N	\N		\N	\N	\N	547	547	Y			\N		\N	report	2016-05-04 11:11:18.175	\N	1994	4465	\N	f
4683	Coester_Degen_Thiele_Et_Al._1996_100458	\N	\N	\N	\N	\N	Pflege- und Entwicklungsplan NSG "Bockhorst"	100458		\N	\N	\N		\N	\N	\N	45	45	Y			\N		\N	report	2016-05-04 11:11:36.841	\N	1996	4684	\N	f
2334	Projektgruppe_Halbtrockenrasen_(xoexter)_1993_000855	\N	\N	\N	\N	\N	Pflege und Entwicklung von Kalkmagerrasen als Beitrag zur Kulturlandschaftspflege in Ostwestfalen (Kreise Höxter, Lippe und Paderborn). unveröff. Abschlußbericht	000855		\N	\N	\N		\N	\N	\N	222	\N	Y			\N	Universität-Gesamthochschule Paderborn, Abteilung Höxter, 263 S	\N	thesis	2016-05-04 11:07:58.96	\N	1993	\N	\N	f
1267	Hollaender_1993_000455	\N	\N	\N	\N	\N	Beiträge zu einer biologischen Monographie der Gattung Globularia (L.) (Familie Globulariaceaea DC.) unter besonderer Berücksichtigung der Art Globularia punctata LAPEYR	000455		\N	HOK	\N		TABGESNE	\N	\N	60	\N	Y			\N	Dipl.-Arb. Universität Halle. 98 S	\N	thesis	2016-05-04 11:06:22.094	\N	1993	\N	\N	f
1282	Jaeger_1998_000460	\N	\N	\N	\N	\N	Die Vegetation der Halbtrockenrasen im Raum Questenberg (Südharz) in Beziehung zu ihrer historischen und aktuellen Nutzung	000460		\N	JAC	\N		TABGESNE	\N	\N	262	\N	Y			\N	Dipl.-Arb. Universität Halle. 162 S	\N	thesis	2016-05-04 11:06:23.277	\N	1998	\N	\N	f
1585	Jaeger_1979_000579	\N	\N	\N	\N	\N	Laubwaldgesellschaften des Bramwaldes	000579		\N	JR	\N		BOHN	\N	\N	330	\N	Y			\N	Staatsexam.-Arb. 78 S. Göttingen	\N	thesis	2016-05-04 11:06:52.68	\N	1979	\N	\N	f
4152	Voigtlaender_1970_100279	\N	\N	\N	\N	\N	Die Ackerunkrautgesellschaften Mecklenburgs	100279		\N	\N	\N		\N	\N	\N	679	679	Y			\N	Universität Greifswald, Diss. A	\N	thesis	2016-05-04 11:10:51.256	\N	1970	\N	\N	f
4545	Voigtlaender_1993_100413	\N	\N	\N	\N	\N	Geobotanisches Gutachten für das NSG "Blüchersches Bruch"	100413		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	report	2016-05-04 11:11:24.464	\N	1993	4546	\N	f
4548	Voigtlaender_1993_100414	\N	\N	\N	\N	\N	Vegetationskundliche Kartierung des Warnoweinzugsbereiches mit dem Brühler Bach im Raum Weitendorf	100414		\N	\N	\N		\N	\N	\N	232	232	Y			\N		\N	report	2016-05-04 11:11:24.693	\N	1993	4549	\N	f
4551	Voigtlaender_1994_100415	\N	\N	\N	\N	\N	Die Vegetation des Müritz- Nationalparks: Teil I - Übersichtskartierung der aktuellen Vegetation	100415		\N	\N	\N		\N	\N	\N	1	1	Y			\N		\N	report	2016-05-04 11:11:25.428	\N	1994	4552	\N	f
4554	Voigtlaender_1994_100416	\N	\N	\N	\N	\N	Die Vegetation des Müritz- Nationalparks: Teil II - Die Vegetation des Graslandes, der Äcker und Ackerbrachen sowie ausgewählter Monitoring- Flächen	100416		\N	\N	\N		\N	\N	\N	851	851	Y			\N		\N	report	2016-05-04 11:11:25.776	\N	1994	4555	\N	f
4557	Voigtlaender_1994_100417	\N	\N	\N	\N	\N	Floristische und vegetationskundliche Bewertung des westlichsten Abschnittes des NSG "Müritzsteilufer bei Rechlin"	100417		\N	\N	\N		\N	\N	\N	29	29	Y			\N		\N	report	2016-05-04 11:11:26.088	\N	1994	4558	\N	f
4560	Voigtlaender_1994_100418	\N	\N	\N	\N	\N	Vegetationskundliche Kartierung des Großschutzgebietes Peenetal-Landschaft	100418		\N	\N	\N		\N	\N	\N	0	\N	Y			\N		\N	report	2016-05-04 11:11:26.357	\N	1994	4561	\N	f
4563	Voigtlaender_1994_100419	\N	\N	\N	\N	\N	Die Vegetation des Grünlandes im NSG "Krüselin See - Mechow See" als Teil einer Pflege- und Entwicklungsplanung	100419		\N	\N	\N		\N	\N	\N	36	36	Y			\N		\N	report	2016-05-04 11:11:26.594	\N	1994	4564	\N	f
4566	Voigtlaender_1995_100420	\N	\N	\N	\N	\N	Die Vegetation des Naturschutzgebietes "Zerninseesenke" und ihre Beeinflussung durch Maßnahmen zur Wiedervernässung	100420		\N	\N	\N		\N	\N	\N	109	109	Y			\N		\N	report	2016-05-04 11:11:26.875	\N	1995	4567	\N	f
4569	Voigtlaender_1995_100421	\N	\N	\N	\N	\N	Bodenkundliche, hydrologische und vegetationskundliche Situation im Bereich des Naturschutzgebietes "Plöwensches Seebruch"	100421		\N	\N	\N		\N	\N	\N	100	100	Y			\N		\N	report	2016-05-04 11:11:27.112	\N	1995	4570	\N	f
4572	Voigtlaender_1996_100422	\N	\N	\N	\N	\N	Vergleichende vegetationskundliche Untersuchungen der Wald- und Saumgesellschaften im NSG "Stauchendmoräne nördlich Remplin"	100422		\N	\N	\N		\N	\N	\N	120	120	Y			\N		\N	report	2016-05-04 11:11:27.423	\N	1996	4573	\N	f
4575	Voigtlaender_Schmidt_1994_100423	\N	\N	\N	\N	\N	Gutachterliche Stellungnahme zur Hydrologie und Vegetation von 4 Moorkomplexen im Raum Bäbelin - Passee, Landkreis Nordwestmecklenburg	100423		\N	\N	\N		\N	\N	\N	170	170	Y			\N		\N	report	2016-05-04 11:11:27.707	\N	1994	4576	\N	f
5109	Paezolt_1996_100645	\N	\N	\N	\N	\N	Der Beesenberg - Standort und Vegetation eines Quellmoorkomplexes im Ückertal (Brandenburg)	100645		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:16.917	\N	1996	\N	\N	f
5119	Voigtlaender_1964_100649	\N	\N	\N	\N	\N	Ackerunkrautgesellschaften im Gebiet um Feldberg - Dissertationsschrift	100649		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:17.989	\N	1964	\N	\N	f
5122	Graef_2004_100650	\N	\N	\N	\N	\N	Vegetation und Standort des Waldschutzgebietes Goor im Biosphärenreservat Südost-Rügen	100650		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:18.245	\N	2004	\N	\N	f
2283	Kaestner_1938_000833	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des westsächsischen Berg- und Hügellandes. III. und IV. Teil	000833		\N	\N	\N		\N	\N	\N	0	\N	Y	Veröff. Landesver. Sächs. Heimatsch. zur Erforsch. Pflanzenges. Sachsens. Dresden	Veröff. Landesver. Sächs. Heimatsch. zur Erforsch. Pflanzenges. Sachsens. Dresden	\N		\N	book	2016-05-04 11:07:53.94	\N	1938	\N	\N	f
940	Steinfuehrer_1945_000340	\N	\N	\N	\N	\N	Die Pflanzengesellschaften der Schleiufer und ihre Beziehung zum Satzgehalt des Bodens	000340		\N	\N	\N		\N	\N	\N	17	\N	Y			\N	Diss. Universität Kiel	\N	thesis	2016-05-04 11:05:53.989	\N	1945	\N	\N	f
949	Tuerk_1993_000343	\N	\N	\N	\N	\N	Pflanzengesellschaften und Vegetationsmosaike im nördlichen Oberfranken	000343		\N	\N	\N		\N	\N	\N	634	\N	Y	Diss. Bot. 207: 1-290. Berlin, Stuttgart	Diss. Bot. 207: 1-290. Berlin, Stuttgart	\N		\N	book	2016-05-04 11:05:54.708	\N	1993	\N	\N	f
2027	Buerger_2004_000738	\N	\N	\N	\N	\N	Veränderung von Waldökosystemen aufgrund historischer Nutzung im Schwarzwald und in den Vogesen	000738		\N	\N	\N		\N	TV	\N	33	\N	Y			\N	Diss. Fak. Forst- u. Umweltwissenschaften Universität Freiburg. 145 S	\N	thesis	2016-05-04 11:07:30.991	\N	2004	\N	\N	f
2218	Ingenieurbuero_Wasser_und_Umwelt_1996_000806	\N	\N	\N	\N	\N	Pflege- und Entwicklungsplan für das Naturschutzgebiet "Nedlitzer Niederung"	000806		\N	\N	\N		\N	\N	\N	8	\N	Y			\N		\N	report	2016-05-04 11:07:47.448	\N	1996	2219	\N	f
2555	Mueller_1976_000937	\N	\N	\N	\N	\N	Unveröffentlichte Vegetationsaufnahmen von Bergmischwäldern aus dem Oberallgäu	000937		\N	\N	unpubliziert		\N	\N	\N	29	\N	Y	Unveröff. Vegetationsaufnahmen. 10 S		\N		\N	unpublished	2016-05-04 11:08:18.82	\N	1976	\N	\N	f
2598	Tuexen_1958_000956	\N	\N	\N	\N	\N	Erläuterungen zur Punktkarte des Forstamtes Escherode	000956		\N	\N	unpubliziert		\N	\N	\N	36	\N	Y			\N		\N	unpublished	2016-05-04 11:08:22.703	\N	1958	\N	\N	f
2664	Suesselbeck_1998_000978	\N	\N	\N	\N	\N	Vegetationsökologische Untersuchungen ausgewählter Teilgebiete des NSG "Briloner Kalkkuppen", Hochsauerlandkreis	000978		\N	\N	\N		\N	\N	\N	170	\N	Y			\N	Dipl.-Arb. Universität Münster	\N	thesis	2016-05-04 11:08:27.835	\N	1998	\N	\N	f
2670	Verbuecheln_1983_000980	\N	\N	\N	\N	\N	Gutachten zur Naturschutzwürdigkeit des geplanten Naturschutzgebietes	000980		\N	\N	\N		\N	\N	\N	48	\N	Y			\N		\N	report	2016-05-04 11:08:28.316	\N	1983	2671	\N	f
4307	Duecker_1996_100340	\N	\N	\N	\N	\N	Vegetation und ausgewählte Aspekte der Fauna an der Landower Wedde, Westrügen, im Nationalpark Vorpommersche Boddenlandschaft	100340		\N	\N	\N		\N	\N	\N	0	\N	Y			\N	Dipl.-Arb. Universität Paderborn	\N	thesis	2016-05-04 11:11:06.585	\N	1996	\N	\N	f
4801	Ostendorp_Bluemel_1998_100505	\N	\N	\N	\N	\N	Vegetations- und standortkundliche Untersuchungen zur Verschilfung der Salzweiden zwischen Vitte und Neuendorf (Insel Hiddensee)	100505		\N	\N	unpubliziert		\N	\N	\N	60	60	Y	\N		\N		\N	unpublished	2016-05-04 11:11:48.191	\N	1998	\N	\N	f
4816	Bluemel_2000_100511	\N	\N	\N	\N	\N	Vegetationsaufnahmen Mecklenburg-Vorpommern	100511		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y	Unveröff		\N		\N	unpublished	2016-05-04 11:11:49.594	\N	2000	\N	\N	f
4823	Schlueter_Sluschny_2000_100514	\N	\N	\N	\N	\N	Vegetationsaufnahmen Mecklenburg-Vorpommern	100514		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y	Unveröff		\N		\N	unpublished	2016-05-04 11:11:50.328	\N	2000	\N	\N	f
4947	Mueller_1996_100590	\N	\N	\N	\N	\N	Standort und Vegetation auf tiefentwässertem Niedermoor am Beispiel des Gartzer Bruchs	100590		\N	\N	\N		\N	\N	\N	55	\N	Y			\N	Dipl.-Arb. Bot. Institut, Universität Greifswald	\N	thesis	2016-05-04 11:12:03.382	\N	1996	\N	\N	f
4985	Suetering_2003_100602	\N	\N	\N	\N	\N	Landschaftsökologische Untersuchungen in Schwarz-Erlen-Wäldern der Waldlewitz im Rahmen eines Wiedervernässungsprojektes	100602		\N	\N	\N		\N	\N	\N	94	94	Y			\N	ob Theses?	\N	thesis	2016-05-04 11:12:06.313	\N	2003	\N	\N	f
2583	Wuehrer_1991_000949	\N	\N	\N	\N	\N	Die subalpinen Waldgesellschaften der Chiemgauer Alpen unter besonderer Berücksichtigung der anthropogenen Beeinflussung	000949		\N	\N	\N		\N	\N	\N	59	\N	Y			\N	Dipl.-Arb. LMU München. 61 S	\N	thesis	2016-05-04 11:08:21.5	\N	1991	\N	\N	f
3453	Mueller_1988_001255	\N	\N	\N	\N	\N	Floristisch-Pflanzensoziologische und vegetationsökologische Untersuchungen der Kalksümpfe (Caricion davallianae) in Nordrhein-Westfalen und Rheinland-Pfalz	001255		\N	\N	\N		M�ller_w_1988	\N	\N	111	\N	Y			\N	Diss. Univ. Bonn	\N	thesis	2016-05-04 11:09:36.627	\N	1988	\N	\N	f
3489	Mueller_2002_001268	\N	\N	\N	\N	\N	Auswertung der Langzeituntersuchungen von Dauerflächen im Augsburger Stadtgebiet zur Renaturierung von Lechhaiden	001268		\N	\N	\N		Mueller_2002	\N	\N	193	\N	Y	Ber. Bayer. Landesamt Umweltschutz (Hrsg.):  97 S	Ber. Bayer. Landesamt Umweltschutz (Hrsg.):  97 S	\N		\N	book	2016-05-04 11:09:39.756	\N	2002	\N	\N	f
2234	Bundesanstalt_fuer_Vegetationskunde_(verschied._Autoren)_1950_000812	\N	\N	\N	\N	\N	(Gutachten aus verschieden Jahren)	000812		\N	\N	unpubliziert		\N	\N	\N	27	\N	Y	Unveröff., Archiv BfN, Bonn		\N		\N	unpublished	2016-05-04 11:07:48.923	\N	1950	\N	\N	f
2861	Lwf_-_Bayerische_Landesanstalt_Fuer_Wald-_Und_Forstwirtschaft_null_001052	\N	\N	\N	\N	\N	Datenbestand Naturwaldreservate Bayerns	001052		\N	\N	unpubliziert		\N	\N	\N	1708	\N	Y			\N		\N	unpublished	2016-05-17 21:20:51.837	\N	2005	\N	\N	f
4513	Schlueter_Et_Al._1993_100403	\N	\N	\N	\N	\N	Botanische Bestandserfassung auf dem Truppenübungsplatz in Lübtheen, Kreis Hagenow	100403		\N	\N	\N		\N	\N	\N	41	41	Y			\N		\N	report	2016-05-04 11:11:22.118	\N	1993	4514	\N	f
221	Hoegel_1990_000072	\N	\N	\N	\N	\N	Vegetationskartierung im Raum Friedeburg/Friedeburgerhütte im MTB Könnern (4336) unter Verwendung von Luftbildaufnahmen bei Darstellung des Ausmaßes anthropogener Einflüsse in der Landschaft	000072		\N	\N	\N		\N	\N	\N	305	\N	Y			\N	Dipl.-Arb. Mskr. Universität Halle. 1-77 S	\N	thesis	2016-05-04 11:04:48.122	\N	1990	\N	\N	f
533	Hoelzer_1977_000183	\N	\N	\N	\N	\N	Vegetationskundliche und ökologische Untersuchungen im Blindensee-Moor bei Schonach	000183		\N	\N	\N		THOMAS	TV	\N	104	\N	Y	Diss. Bot. 36: Vaduz	Diss. Bot. 36: Vaduz	\N		\N	book	2016-05-04 11:05:15.481	\N	1977	\N	\N	f
1449	Roehlig_1995_000527	\N	\N	\N	\N	\N	Flora und Vegetation des Naturschutzgebietes "Hasenwinkel" bei Unterrißdorf	000527		\N	RHG	\N		TABGESNE	\N	\N	23	\N	Y			\N	Staatsex.-Arb. Universität Halle. 74 S	\N	thesis	2016-05-04 11:06:40.453	\N	1995	\N	\N	f
1435	Roegener._J._Benz_1997_000523	\N	\N	\N	\N	\N	Pflege- & Entwicklungsplan NSG "Questenberg"	000523		\N	ROB	\N		TABGESNE	\N	\N	115	\N	Y			\N		\N	report	2016-05-04 11:06:39.406	\N	1997	1436	\N	f
3093	Boettcher_Ranft_1986_001130	\N	\N	\N	\N	\N	Waldgesellschaften im nördlichen Ith	001130		\N	\N	unpubliziert		HEINKEN1	\N	\N	16	\N	Y	Unveröff. Manuskript erstellt im Auftr. der Bezirksregierung Hannover. Höxter: 81 S		\N		\N	unpublished	2016-05-04 11:09:06.214	\N	1986	\N	\N	f
1191	Egersdoerfer_1996_000430	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen der Feinstruktur von Extremstandorten auf Gips, Zechsteinkalk und Kupferschiefer am Beispiel von Hainrode, Landkreis Sangerhausen (Sachsen-Anhalt)	000430		\N	EGM	\N		TABGESNE	\N	\N	40	\N	Y			\N	Dipl.-Arb. Institut Botanik Pharmazeutische Biologie, Universität Erlangen-Nürberg ?	\N	thesis	2016-05-04 11:06:16.259	\N	1996	\N	\N	f
2262	Hachmoeller_2000_000824	\N	\N	\N	\N	\N	Vegetation,Schutz u.Regeneration von Bergwiesen im Osterzgebirge - eine Fallstudie zu Entwicklung und Dynamik montaner Grünlandgesellschaften	000824		\N	\N	\N		\N	\N	\N	136	\N	Y	Diss. Bot. 338: 1-300. Stuttgart	Diss. Bot. 338: 1-300. Stuttgart	\N		\N	book	2016-05-04 11:07:51.732	\N	2000	\N	\N	f
3103	Koehler_1959_001134	\N	\N	\N	\N	\N	Ackerunkrautgesellschaften einiger Auengebiete an Elbe und Mulde	001134		\N	\N	\N		LAU_ST	\N	\N	202	\N	N			\N	Dipl.-Arb. Universität Halle, FB Biologie. 133 S	\N	thesis	2016-05-04 11:09:07.175	\N	1959	\N	\N	f
2415	Grosser_1966_000884	\N	\N	\N	\N	\N	Altteicher Moor und Große Jeseritzen	000884		\N	\N	\N		HEINKEN_DIP	\N	\N	24	\N	Y	Brandenburgische Naturschutzgebiete 1. Postdam	Brandenburgische Naturschutzgebiete 1. Postdam	\N		\N	book	2016-05-04 11:08:05.89	\N	1966	\N	\N	f
4536	Voigtlaender_1992_100410	\N	\N	\N	\N	\N	Abschnittsbericht 9. Peenetal zwischen Loitz und Demmin sowie Loitz und Jarmen	100410		\N	\N	\N		\N	\N	\N	299	299	Y			\N		\N	report	2016-09-07 21:57:08.589	\N	1992	4537	\N	f
4818	Dengler_2001_100512	\N	\N	\N	\N	\N	Die krautige Xerothermvegetation Nordostdeutschlands	100512		\N	\N	unpubliziert		DUBLETTEN?	\N	\N	55	55	Y	Universität Kiel		\N		\N	unpublished	2017-01-23 16:31:43.578	\N	2001	\N	\N	f
5650	Tuexen_null_101106	\N	\N	\N	\N	\N	Tüxen-Archive	101106	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:19.172	\N	1500	\N	5651	t
5538	Runge_null_101059	\N	\N	\N	\N	\N	Dauerquadrat-Untersuchungen von Küsten-Gesellschaften	101059	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:16.014	\N	1500	\N	\N	t
5550	Wiemann_null_101064	\N	\N	\N	\N	\N	Vegetationsaufnahmen Spiekeroog (Nachlass Original Aufzeichnungen)	101064	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	report	2016-09-21 15:56:16.346	\N	1500	\N	\N	t
5561	Niedringhaus_null_101068	\N	\N	\N	\N	\N	Kleingewässer der Ostfriesischen Inseln	101068	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	article	2016-09-21 15:56:16.612	\N	1500	\N	5562	t
5692	Partzsch_null_null	\N	\N	\N	\N	\N	Vegetationsaufnahmen Praktikum Amselgrund Halle-Kröllwitz, unveröff.	001340	\N	\N	\N	\N	\N	\N	\N	\N	16	\N	N	\N	\N	\N	\N	\N	report	2016-12-22 11:40:29.553	\N	1500	\N	\N	t
77	Hundt_null_000024	\N	\N	\N	\N	\N	Vegetationskundliche Untersuchungen der Grünlandbestände des Universitätsgutes Bärenrode	000024		\N	\N	unpubliziert		\N	\N	\N	29	\N	Y	Unveröff. Mskr. Aus: II: Anlagenband. 12 Manuskripte von vegetationskundlichen Grünlanduntersuchungen. 29-70. Halle (Saale)		\N		\N	unpublished	2016-05-04 11:04:34.781	\N	1500	\N	\N	t
79	Hundt_null_000025	\N	\N	\N	\N	\N	Das Alperstedter Ried	000025		\N	\N	unpubliziert		\N	\N	\N	10	\N	Y	Unveröff. Mskr. Aus: II: Anlagenband. 12 Manuskripte von vegetationskundlichen Grünlanduntersuchungen. 84-106. Halle (Saale)		\N		\N	unpublished	2016-05-04 11:04:35.08	\N	1500	\N	\N	t
81	Hundt_null_000026	\N	\N	\N	\N	\N	Das Helsunger Bruch	000026		\N	\N	unpubliziert		\N	\N	\N	15	\N	Y	Unveröff. Mskr. Aus: II: Anlagenband. 12 Manuskripte von vegetationskundlichen Grünlanduntersuchungen. 107-128. Halle (Saale)		\N		\N	unpublished	2016-05-04 11:04:35.315	\N	1500	\N	\N	t
83	Hundt_null_000027	\N	\N	\N	\N	\N	Das Grünland der Struth im oberen Eichsfeld	000027		\N	\N	unpubliziert		\N	\N	\N	10	\N	Y	Unveröff. Mskr. Aus: II: Anlagenband. 12 Manuskripte von vegetationskundlichen Grünlanduntersuchungen. 143-159. Halle (Saale)		\N		\N	unpublished	2016-05-04 11:04:35.604	\N	1500	\N	\N	t
1606	Korneck_null_000586	\N	\N	\N	\N	\N	?	000586		\N	KO	unpubliziert		BOHN	\N	\N	10	\N	Y	Mskr		\N		\N	unpublished	2016-05-04 11:06:54.374	\N	1500	\N	\N	t
312	Biologische_Station_Kreis_Steinfurt_null_000103	\N	\N	\N	\N	\N	Vegetationsaufnahmen aus der Datenbank der Biologischen Station, Peter Schwartze	000103		\N	\N	unpubliziert		\N	\N	\N	1491	\N	N	Unveröff. digitale Daten		\N		\N	unpublished	2016-05-04 11:04:55.939	\N	1500	\N	\N	t
400	Braun_null_000134	\N	\N	\N	\N	\N	?	000134		\N	\N	unpubliziert		\N	\N	\N	0	\N	Y	Mskr		\N		\N	unpublished	2016-05-04 11:05:03.358	\N	1500	\N	\N	t
511	Weicherding_Bettinger_null_000175	\N	\N	\N	\N	\N	BZE-Waldaufnahmen Saarland 2006-2008	000175		\N	\N	unpubliziert		BZE	\N	\N	0	\N	N			\N		\N	unpublished	2016-05-04 11:05:13.612	\N	1500	\N	\N	t
694	Mueller_null_000245	\N	\N	\N	\N	\N	Caricetum davallianae in Baden-Württemberg	000245		\N	\N	unpubliziert		\N	\N	\N	116	\N	Y	Unveröff. Mskr		\N		\N	unpublished	2016-05-04 11:05:30.354	\N	1500	\N	\N	t
1308	Knapp_null_000472	\N	\N	\N	\N	\N	Vegetations-Studien im Rheingau und in den angrenzenden Landschaften	000472		\N	KNA	unpubliziert		TABGESNE	\N	\N	54	\N	Y	Mskr. Halle (Saale). 36 S		\N		\N	unpublished	2016-05-04 11:06:26.214	\N	1500	\N	\N	t
1310	Knapp_null_000473	\N	\N	\N	\N	\N	Vegetationsaufnahmen von Trockenrasen und Felsfluren Mitteldeutschlands. Teil 2: Atlantisch-Submediterrane und Dealpine Trockenrasen (Bromion erecti)	000473		\N	KNA	unpubliziert		TABGESNE	\N	\N	106	\N	Y	Mskr. Halle (Saale). 56 S		\N		\N	unpublished	2016-05-04 11:06:26.535	\N	1500	\N	\N	t
1312	Knapp_null_000474	\N	\N	\N	\N	\N	Vegetationsaufnahmen von Trockenrasen und Felsfluren Mitteldeutschlands. Teil 3: Kontinentale Felsfluren und Trockenrasen (Seslerio-Festucion glaucae, Astragalo-Stipion)	000474		\N	KNA	unpubliziert		TABGESNE	\N	\N	200	\N	Y	Mskr. Halle (Saale). 59 S		\N		\N	unpublished	2016-05-04 11:06:26.774	\N	1500	\N	\N	t
1554	Bohn_null_000567	\N	\N	\N	\N	\N	?	000567		\N	BO	unpubliziert		BOHN	\N	\N	140	\N	Y	Mskr., Bundesforschungsanstalt f. Naturschutz u. Landschaftsökologie		\N		\N	unpublished	2016-05-04 11:06:49.701	\N	1500	\N	\N	t
1650	Ludwig_null_000601	\N	\N	\N	\N	\N	?	000601		\N	LU	\N		BOHN	\N	\N	16	\N	Y			\N	Dipl.-Arb. ?	\N	thesis	2016-05-04 11:06:58.152	\N	1500	\N	\N	t
1578	Haerdtle_null_000576	\N	\N	\N	\N	\N	?	000576		\N	HA	unpubliziert		BOHN	\N	\N	47	\N	Y	Mskr		\N		\N	unpublished	2016-05-04 11:06:52.009	\N	1500	\N	\N	t
1817	Bettinger_null_000663	\N	\N	\N	\N	\N	Biotop- und FFH-Kartierung im Saarland (bis 2006)	000663		\N	\N	unpubliziert		\N	Starnual	\N	49	\N	N			\N		\N	unpublished	2016-05-04 11:07:13.248	\N	1500	\N	\N	t
1824	Wolff_null_000666	\N	\N	\N	\N	\N	Vegetationsaufnahmen NSG "Jägersburger Moor", Saarland	000666		\N	\N	unpubliziert		\N	TV	\N	29	\N	Y	Unveröff. Mskr. (Bettinger)		\N		\N	unpublished	2016-05-04 11:07:13.915	\N	1500	\N	\N	t
1826	Sauer_null_000667	\N	\N	\N	\N	\N	Vegetationsaufnahmen aus dem Saarland	000667		\N	\N	unpubliziert		\N	TV	\N	131	\N	Y	Unveröff. Mskr. (Bettinger)		\N		\N	unpublished	2016-05-04 11:07:14.168	\N	1500	\N	\N	t
1832	Warthemann_null_000669	\N	\N	\N	\N	\N	Unveröff. Vegetationsaufnahmen aus Sachsen-Anhalt	000669		\N	\N	unpubliziert		\N	TV	\N	1060	\N	N	Private Datenbank Guido Warthemann		\N		\N	unpublished	2016-05-04 11:07:14.713	\N	1500	\N	\N	t
1962	Bettinger_Weicherding_null_000712	\N	\N	\N	\N	\N	BZE-Waldaufnahmen Saarland	000712		\N	\N	unpubliziert		\N	\N	\N	42	\N	N	Unveröff		\N		\N	unpublished	2016-05-04 11:07:25.218	\N	1500	\N	\N	t
2137	Braun_null_000778	\N	\N	\N	\N	\N	Manuskript	000778		\N	\N	unpubliziert		\N	\N	\N	16	\N	Y	TAB-Aufnahmen i. d. Datenbank von Thomas Sperle - CDA-Deutschlanddatensatz		\N		\N	unpublished	2016-05-04 11:07:40.809	\N	1500	\N	\N	t
2226	Fischer_null_000809	\N	\N	\N	\N	\N	Naturnahe Vegetationsformen der Gülper Havelaue und ihre Gefährdung (Teil 2)	000809		\N	\N	unpubliziert		\N	\N	\N	140	\N	Y	Unveröff		\N		\N	unpublished	2016-05-04 11:07:48.173	\N	1500	\N	\N	t
2270	Philippi_null_000828	\N	\N	\N	\N	\N	Die Vegetation des unteren Taubergebietes	000828		\N	\N	\N		\N	\N	\N	6	\N	Y			\N	Unveröff. Habilarbeit, Universität Tübingen (?	\N	thesis	2016-05-04 11:07:52.678	\N	1500	\N	\N	t
2594	Haerdtle_null_000952	\N	\N	\N	\N	\N	?	000952		\N	\N	unpubliziert		\N	\N	\N	40	\N	Y			\N		\N	unpublished	2016-05-04 11:08:22.177	\N	1500	\N	\N	t
2692	Schubert_null_000988	\N	\N	\N	\N	\N	Unveröff. Aufnahmen Dicrano-Pinion, zusammengestellt von Thilo Heinken	000988		\N	\N	unpubliziert		HEINKEN_DIP	\N	\N	27	\N	Y	\N		\N		\N	unpublished	2016-05-04 11:08:30.275	\N	1500	\N	\N	t
2724	Braun_null_001000	\N	\N	\N	\N	\N	Aufnahmen von W. Braun, Karlsfeld aus Kartierung der PNV in Bayern, zusammengestellt von Thilo Heinken	001000		\N	\N	unpubliziert		HEINKEN_DIP	\N	\N	87	\N	N	Unveröff		\N		\N	unpublished	2016-05-04 11:08:33.323	\N	1500	\N	\N	t
2804	Dierschke_null_001032	\N	\N	\N	\N	\N	Vegetationsaufnahmen Gr. Krain, Ostheide bei Gifhorn	001032		\N	\N	unpubliziert		\N	\N	\N	4	\N	Y	Unveröff. Exkursionsskript		\N		\N	unpublished	2016-05-04 11:08:40.953	\N	1500	\N	\N	t
2792	Schlueter_null_001027	\N	\N	\N	\N	\N	Aufnahmen von Heinz Schlüter	001027		\N	\N	unpubliziert		\N	\N	\N	57	\N	N	Unveröff. Mskr		\N		\N	unpublished	2016-05-04 11:08:39.708	\N	1500	\N	\N	t
3049	Heinken_null_001115	\N	\N	\N	\N	\N	Buchenwälder (Luzulo-Fagion i.w.S) und bodensaure Eichenwälder (Quercion roboris) Brandenburg 1997 ff	001115		\N	\N	unpubliziert		HEINKEN2	\N	\N	145	\N	N	Unveröff. Vegetationsaufnahmen aus Tabelle LF-BR und QR-BR		\N		\N	unpublished	2016-05-04 11:09:02.383	\N	1500	\N	\N	t
3108	Schubert_null_001136	\N	\N	\N	\N	\N	Unveröff. Vegetationsaufnahmen für das LAU Sachsen-Anhalt	001136		\N	\N	unpubliziert		LAU_ST	\N	\N	1915	\N	N	Unveröff		\N		\N	unpublished	2016-05-04 11:09:07.685	\N	1500	\N	\N	t
5170	Evers_null_100668	\N	\N	\N	\N	\N	?	100668		\N	EVE	unpubliziert		Heinken1	\N	\N	35	\N	Y			\N		\N	unpublished	2016-05-04 11:12:22.271	\N	1500	\N	\N	t
5192	Philippi_null_100676	\N	\N	\N	\N	\N	?	100676		\N	PHI	unpubliziert		Thomax_Box3	\N	\N	23	\N	Y			\N		\N	unpublished	2016-05-04 11:12:24.127	\N	1500	\N	\N	t
1580	Jahn_null_000577	\N	\N	\N	\N	\N	?	000577		\N	JG	unpubliziert		BOHN	\N	\N	126	\N	Y	Mskr		\N		\N	unpublished	2016-05-04 11:06:52.218	\N	1500	\N	\N	t
3142	Quitt_null_001148	\N	\N	\N	\N	\N	Unveröff. Vegetationsaufnahmen für das LAU Sachsen-Anhalt (auch mit Büro für Umweltplanung, Wernigerode)	001148		Vegetationsaufnahmen für das LAU Sachsen-Anhalt (auch mit Büro für Umweltplanung, Wernigerode).	\N	\N		LAU_ST	\N	\N	69	\N	N	Unveröff		\N		\N	unpublished	2016-05-04 11:09:10.402	\N	1500	\N	\N	t
2855	Lanuv_Nrw_null_001050	\N	\N	\N	\N	\N	LINFOS-Daten Landesamt für Natur, Umwelt und Verbraucherschutz NRW	001050		\N	\N	unpubliziert		\N	\N	\N	11353	\N	Y			\N		\N	unpublished	2016-05-04 11:08:45.425	\N	1500	\N	\N	t
5208	Dubletten_Knapp_null_999999	\N	\N	\N	\N	\N	Viele Dubletten zu den Manuskripten Knapp 44ff aus der LAU-Datenbank	999999		\N	\N	unpubliziert		LAU_ST	\N	\N	176	\N	N	\N		\N		\N	unpublished	2016-05-04 11:12:25.364	\N	1500	\N	\N	t
1964	Mueller_null_000713	\N	\N	\N	\N	\N	Vegetationsaufnahmen von Trockenrasen (ca. 1936-1970). Als Stetigkeitsspalten Festuco-Brometea in Oberdorfer Pflanzengesellschaften	000713		\N	\N	unpubliziert		TAB	\N	\N	798	\N	Y	Mskr		\N		\N	unpublished	2016-05-04 11:07:25.473	\N	1500	\N	\N	t
5236	E-Mail_von_Ute_1111_001280	\N	\N	\N	\N	\N	Vegetationsaufnahmen Naturwaldreservat Hünstollen. Unveröff.	001280	\N		\N	\N	\N	Naturwald	\N	\N	0	\N	N	\N	\N	\N	\N	\N	unknown	2016-11-09 15:51:38.769	\N	2012	\N	\N	t
5721	Dengler_2016_200002	\N	\N	\N	\N	\N	Unpublizierte Vegetationsaufnahmen des Pflanzenökologie-Praktikums 2016 der Universität Bayreuth (BSc. Biologie, Gruppe A1, Thema: Pflanzendiversität in mesischen Grasländern auf dem Uni-Campus in Abhängigkeit von der Mahdhäufigkeit)	200002	\N	\N	\N	\N	\N	\N	\N		\N	\N	\N	\N	\N	\N	Universität Bayreuth	\N	unknown	2017-01-23 15:22:38.121	\N	2016	\N	\N	t
5729	Jensen_Dengler_2010_200004	\N	\N	\N	\N	\N	Daten vom MSc.-Kurs "Ökologie terrestrischer Lebensräume"  2010 der Universität Hamburg (Phytodiversität in Vegetationstypen der schleswig-holsteinischen Nordseeküste)	200004	\N	\N	\N	\N	Unpublished	\N	\N	\N	\N	\N	\N	\N	\N	\N	Universität Hamburg	\N	unknown	2017-04-09 17:18:27.097	\N	2010	\N	\N	t
5698	Mueller_2001_001327	\N	\N	\N	\N	\N	Verbreitung, Soziologie und Gefährdung von Carex pulicaris L. in Sachsen.	001327	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Y	\N	\N	\N	\N	\N	unknown	2016-12-23 12:07:55.208	\N	2001	\N	5699	t
5740	Noirfalise_Vanesse_null_null	\N	\N	\N	\N	\N	La hetraie naturelle a luzule blanche en Belgique.	001343	\N	\N	\N	\N	\N	Heinken2	\N	\N	\N	\N	N	\N	\N	\N	\N	\N	unknown	2017-03-15 15:46:18.149	\N	1977	\N	5741	t
5737	Waesch_null_null	\N	\N	\N	\N	\N	Montane Graslandvegetation des Thüringer Waldes: Aktueller Zustand, historische Analyse und Entwicklungmöglichkeiten.	001342	\N	Aus Excel-Tabelle von Gunnar Waesch. Nummern sind Originalnummern des Autors, passen nicht zu Nummern der Veröffentlichung. Die in der Veröffentlichung in den Tabellen auf der CD eingebauten Daten anderer Autoren sind nicht enthalten.	\N	\N	\N	Waesch_2003	\N	\N	245	\N	N	\N	Cuvillier Verlag Göttingen	\N	\N	\N	unknown	2017-03-15 15:43:22.553	\N	2003	\N	5738	t
5755	Dengler_Loebel_2001_200007	\N	10.13140/RG.2.1.3093.1281	\N	\N	\N	Die Ilmenauniederung im „Wilschenbruch“ bei Lüneburg – Ergebnisse des vegetationsökologischen Praktikums im Studiengang Diplom-Umweltwissenschaften, Sommersemester 2000	200007	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	Universität Lüneburg	\N	unknown	2017-04-09 17:13:13.486	\N	2001	\N	\N	t
458	Flintrop_1990_000156	\N	\N	\N	\N	\N	Oligo- bis mesotraphente Kleinseggen- und Niedermoorgesellschaften (Scheuchzerio-Caricetea fuscae Tüxen 1937)	000156		\N	\N	\N	Booktitle	\N	\N	\N	0	\N	Y	In: Nowak, B. (Hrsg.): Beitr. zur Kenntnis hessischer Pflanzengesellschaften. Ergebniss	In: Nowak, B. (Hrsg.): Beitr. zur Kenntnis hessischer Pflanzengesellschaften. Ergebniss	\N		\N	unknown	2016-05-04 11:05:09.145	\N	1990	\N	\N	t
736	Oberdorfer_1971_000263	\N	\N	\N	\N	\N	Die Pflanzenwelt des Wutachgebietes	000263		\N	\N	\N	Booktitle	\N	\N	\N	10	\N	Y	IN: Die Wutach: Die Natur- u. Landschaftsschutzgebiete Baden-Württembergs 6: 261-321. Freiburgi.Br.	IN: Die Wutach: Die Natur- u. Landschaftsschutzgebiete Baden-Württembergs 6: 261-321. Freiburgi.Br.	\N		\N	unknown	2016-05-04 11:05:34.807	\N	1971	\N	\N	t
896	Schwartze_Schreiber_Vogel_1991_000324	\N	\N	\N	\N	\N	Optimizing the vegetation of impaired wet grassland areas in the region of Münster by different management and impoverishment	000324		\N	\N	\N	Booktitle	\N	\N	\N	4	\N	Y	In: Ravera, O. (ed.): Terrestrial and aquatic ecosystems. New York [u.a.]: Horwood, 1991. - S. 576-582	In: Ravera, O. (ed.): Terrestrial and aquatic ecosystems. New York [u.a.]: Horwood, 1991. - S. 576-582	\N		\N	unknown	2016-05-04 11:05:50.088	\N	1991	\N	\N	t
1185	Dierssen_1996_000428	\N	\N	\N	\N	\N	?	000428		\N	DIS	\N	Booktitle	TABGESNE	\N	\N	0	\N	Y	In: Vegetation Nordeuropas. E. Ulmer, Stuttgart. 838 S	In: Vegetation Nordeuropas. E. Ulmer, Stuttgart. 838 S	\N		\N	unknown	2016-05-04 11:06:15.832	\N	1996	\N	\N	t
3788	Jeschke_1969_100112	\N	\N	\N	\N	\N	Die Pflanzenwelt der Seen	100112		\N	\N	\N	Booktitle	\N	\N	\N	0	\N	Y	in: Das Naturschutzgebiet Serrahn. Natur u. Naturschutz i. Mecklenburg-Vorpommern, Sonderheft, 39-47	in: Das Naturschutzgebiet Serrahn. Natur u. Naturschutz i. Mecklenburg-Vorpommern, Sonderheft, 39-47	\N		\N	unknown	2016-05-04 11:10:10.927	\N	1969	\N	\N	t
1813	Haffner_1960_000661	\N	\N	\N	\N	\N	Zur Pflanzengeographie der "Unteren Saar" mit besonderer Berücksichtigung des LSG "Saarschleife"	000661		\N	\N	\N	Booktitle	\N	TV	\N	29	\N	Y	In: Kremp, W. (Hrsg.): Untersuchungsergebnisse aus Landschafts- u. Naturschutzgebieten im Saarland 2: 58-65	In: Kremp, W. (Hrsg.): Untersuchungsergebnisse aus Landschafts- u. Naturschutzgebieten im Saarland 2: 58-65	\N		\N	unknown	2016-05-04 11:07:12.734	\N	1960	\N	\N	t
1819	Haffner_1964_000664	\N	\N	\N	\N	\N	Pflanzensoziologische und pflanzengeographische Untersuchungen in denTalauen der Mosel, Saar, Nied, Prims und Blies	000664		\N	\N	\N	Booktitle	\N	TV	\N	141	\N	Y	In: Kremp, W. (Hrsg.): Untersuchungsergebnisse aus Landschafts- u. Naturschutzgebieten im Saarland 3: 7-65	In: Kremp, W. (Hrsg.): Untersuchungsergebnisse aus Landschafts- u. Naturschutzgebieten im Saarland 3: 7-65	\N		\N	unknown	2016-05-04 11:07:13.459	\N	1964	\N	\N	t
2266	Mueller_1974_000826	\N	\N	\N	\N	\N	Zur Kenntnis einiger Pioniergesellschaften im Taubergießengebiet	000826		\N	\N	\N	Booktitle	\N	\N	\N	12	\N	Y	In: Das Taubergießengebiet; Natur- u. Landschaftsschutzgeb. Baden-Württemberg 7: 284-305	In: Das Taubergießengebiet; Natur- u. Landschaftsschutzgeb. Baden-Württemberg 7: 284-305	\N		\N	unknown	2016-05-04 11:07:52.262	\N	1974	\N	\N	t
4492	Rehbein_1987_100396	\N	\N	\N	\N	\N	Zur Populationsdynamik von Gentiana pneumonanthe	100396		\N	\N	\N	Booktitle	\N	\N	\N	4	4	Y	In: Populationsökologie u. Florenschutz, 41- 47	In: Populationsökologie u. Florenschutz, 41- 47	\N		\N	unknown	2016-05-04 11:11:20.302	\N	1987	\N	\N	t
410	Buchwald_1983_000137	\N	\N	\N	\N	\N	Ökologische Untersuchungen an Libellen im westlichen Bodenseeraum	000137		\N	\N	\N	Booktitle	\N	\N	\N	23	\N	Y	In: Der Mindelsee bei Radolfzell. Die Natur- u. Landschaftsschutzgebiete Baden-Württemberg 11: 539-637. Karlsruhe	In: Der Mindelsee bei Radolfzell. Die Natur- u. Landschaftsschutzgebiete Baden-Württemberg 11: 539-637. Karlsruhe	\N		\N	unknown	2016-05-04 11:05:04.059	\N	1983	\N	\N	t
525	Hausfeld_1984_000180	\N	\N	\N	\N	\N	Die Vegetation nordwest-niedersächsischer Bachtäler in Abhängigkeit von landwirtschaftlicher Nutzung und wasserbaulichen Eingriffen	000180		\N	\N	\N	Booktitle	\N	\N	\N	8	\N	Y	In. Naturschutz u. Landschaftspflege 4: 137-170. Wardenburg	In. Naturschutz u. Landschaftspflege 4: 137-170. Wardenburg	\N		\N	unknown	2016-05-04 11:05:14.788	\N	1984	\N	\N	t
645	Krisch_1967_000226	\N	\N	\N	\N	\N	Die Grünland- und Salzpflanzengesellschaften der Werraaue bei Bad Salzungen	000226		\N	\N	\N	Booktitle	\N	\N	\N	0	\N	Y	In: Die Grünlandgesellschaften. Hercynia N.F. 4 (4): 375-413. Leipzig	In: Die Grünlandgesellschaften. Hercynia N.F. 4 (4): 375-413. Leipzig	\N		\N	unknown	2016-05-04 11:05:25.87	\N	1967	\N	\N	t
796	Philippi_1989_000287	\N	\N	\N	\N	\N	Die Pflanzengesellschaften des Belchen-Gebietes im Schwarzwald	000287		\N	\N	\N	Booktitle	\N	\N	\N	239	\N	Y	In: Der Belchen - Geschichtlich naturkundliche Monographie d. schönsten Schwarzwaldberges. Die Natur- u. Landschaftsschutzgebiete Baden-Württemberg ?	In: Der Belchen - Geschichtlich naturkundliche Monographie d. schönsten Schwarzwaldberges. Die Natur- u. Landschaftsschutzgebiete Baden-Württemberg ?	\N		\N	unknown	2016-05-04 11:05:40.862	\N	1989	\N	\N	t
1011	Wedra_1990_000369	\N	\N	\N	\N	\N	Zwergstrauchheiden und Borstgrasrasen (Calluno-Ulicetea Braun-Blanquet & Tüxen 1943)	000369		\N	\N	\N	Booktitle	\N	\N	\N	8	\N	Y	In: Nowak, B. (Hrsg.): Beitr. zur Kenntnis hessischer Pflanzengesellschaften. Bot. Naturschutz Hessen Beih. 2: 100	In: Nowak, B. (Hrsg.): Beitr. zur Kenntnis hessischer Pflanzengesellschaften. Bot. Naturschutz Hessen Beih. 2: 100	\N		\N	unknown	2016-05-04 11:06:00.901	\N	1990	\N	\N	t
1246	Heinrich_Marstaller_1997_000448	\N	\N	\N	\N	\N	Exkursion 3: Mittleres Saaletal	000448		\N	HEW	\N	Booktitle	TABGESNE	\N	\N	16	\N	Y	In: Thüringer Landesanstalt f. Umwelt u. Institut f. spezielle Botanik d. Universität Jena: Exkursionsführer zur 47. Jahrestagung d. Floristisch-soziologischen AG e.V. i. Jena: 52-74. Jena	In: Thüringer Landesanstalt f. Umwelt u. Institut f. spezielle Botanik d. Universität Jena: Exkursionsführer zur 47. Jahrestagung d. Floristisch-soziologischen AG e.V. i. Jena: 52-74. Jena	\N		\N	unknown	2016-05-04 11:06:20.419	\N	1997	\N	\N	t
1811	Haffner_1958_000660	\N	\N	\N	\N	\N	Seltene Pflanzen der Naturschutzgebiete unserer Saarheimat in pflanzengeographischer Betrachtung	000660		\N	\N	\N	Booktitle	\N	TV	\N	6	\N	Y	In: Altmeyer, Klaus; et al. (Hrsg.), Das Saarland, Saarbrücken 1958, S. 511-528	In: Altmeyer, Klaus; et al. (Hrsg.), Das Saarland, Saarbrücken 1958, S. 511-528	\N		\N	unknown	2016-05-04 11:07:12.475	\N	1958	\N	\N	t
1815	Haffner_1960_000662	\N	\N	\N	\N	\N	"Pflanzensoziolog. und pflanzengeogr. Untersuchungen im Muschelkalkgebiet des Saarlandes mit besonderer Berücksichtigung der Grenzgebiete von Lothringen und Luxemburg	000662		\N	\N	\N	Booktitle	\N	TV	\N	108	\N	Y	In: Kremp, W. (Hrsg.): Untersuchungsergebnisse aus Landschafts- u. Naturschutzgebieten im Saarland 2: 66-164	In: Kremp, W. (Hrsg.): Untersuchungsergebnisse aus Landschafts- u. Naturschutzgebieten im Saarland 2: 66-164	\N		\N	unknown	2016-05-04 11:07:13.033	\N	1960	\N	\N	t
2118	Meisterhans_1999_000771	\N	\N	\N	\N	\N	Wälder - der landschaftsprägende Vegetationstyp	000771		\N	\N	\N	Booktitle	\N	\N	\N	68	\N	Y	In: Landesanstalt f. Umweltschutz Baden-Württemberg: Der Rohrhardsberg: Neue Wege im Naturschutz f. den Mittleren Schwarzwald. Verl. Regionalkultur, Ubstadt-Weiher. S. 153-218	In: Landesanstalt f. Umweltschutz Baden-Württemberg: Der Rohrhardsberg: Neue Wege im Naturschutz f. den Mittleren Schwarzwald. Verl. Regionalkultur, Ubstadt-Weiher. S. 153-218	\N		\N	unknown	2016-05-04 11:07:39.051	\N	1999	\N	\N	t
3512	Sander_2002_001275	\N	\N	\N	\N	\N	Die Sandrasen des Saarlandes	001275		vgl. auch: Sander, Piers: Sandrasen-Gesellschaften auf Sekundärstandorten und naturnahen Vergleichsstandorten im Saarland. - 1999	\N	\N	Booktitle	Sander_2002	\N	\N	0	\N	Y	In Bettinger, A. & Wolff, P. (2002): Die Vegetation des Saarlandes und seiner Randgebiete - Teil 1. vgl. auch: Sander, Piers: Sandrasen-Gesellschaften auf Sekundärstandorten und naturnahen Vergleichsstandorten im Saarland. - 1999	In Bettinger, A. & Wolff, P. (2002): Die Vegetation des Saarlandes und seiner Randgebiete - Teil 1. vgl. auch: Sander, Piers: Sandrasen-Gesellschaften auf Sekundärstandorten und naturnahen Vergleichsstandorten im Saarland. - 1999	\N		\N	unknown	2016-05-04 11:09:41.575	\N	2002	\N	\N	t
3938	Kudoke_1976_100183	\N	\N	\N	\N	\N	Karte der Ackerunkraut-Leitgesellschaften des Nordens der DDR	100183		\N	\N	\N	Booktitle	\N	\N	\N	0	\N	Y	In: Karte d. Ackerunkraut- Leitgesellschaften d. DDR, Atlas d. DDR, Karte 14.2	In: Karte d. Ackerunkraut- Leitgesellschaften d. DDR, Atlas d. DDR, Karte 14.2	\N		\N	unknown	2016-05-04 11:10:28.333	\N	1976	\N	\N	t
5732	Fanigliulo_Seitz_null_null	\N	\N	\N	\N	\N	unbekannt	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	unknown	2017-02-16 13:25:14.966	\N	1500	\N	\N	t
955	Tuexen_1969_000346	\N	\N	\N	\N	\N	Erläuterungen zur Vegetationskarte des Naturlehrparks	000346		\N	\N	\N	Booktitle	\N	\N	\N	4	\N	Y	In: Erforschung d. Naturlehrparks Haus Wildenrath. Schriftenreihe Landkr. Erkelenz 4: 35-44. Köln	In: Erforschung d. Naturlehrparks Haus Wildenrath. Schriftenreihe Landkr. Erkelenz 4: 35-44. Köln	\N		\N	unknown	2016-05-04 11:05:55.402	\N	1969	\N	\N	t
1382	Moeseler_1990_000503	\N	\N	\N	\N	\N	The differentiation of the Mesobromion in the Eifel mountains (FRG) in relation to soil constitution and land utilisation	000503		\N	MOS	\N	Booktitle	TABGESNE	\N	\N	0	\N	Y	In: Bohn, U., Neuhäusl, R. (eds.): Vegetation and flora of temperate zones: 35-42. SPB Academic Publishing, The Hague	In: Bohn, U., Neuhäusl, R. (eds.): Vegetation and flora of temperate zones: 35-42. SPB Academic Publishing, The Hague	\N		\N	unknown	2016-05-04 11:06:34.044	\N	1990	\N	\N	t
500	Goers_Mueller_1975_000172	\N	\N	\N	\N	\N	Flora der Farn- und Blütenpflanzen des Taubergießengebietes	000172		\N	\N	\N	Booktitle	\N	\N	\N	0	\N	Y	In: Das Taubergießengebiet. Die Natur- u. Landschaftsschutzgebiete Baden-Württemberg 7: 209-283. Ludwigsburg	In: Das Taubergießengebiet. Die Natur- u. Landschaftsschutzgebiete Baden-Württemberg 7: 209-283. Ludwigsburg	\N		\N	unknown	2016-05-04 11:05:12.935	\N	1975	\N	\N	t
5703	Mayer_null_null	\N	\N	\N	\N	\N	unbekannt	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	unknown	2016-12-23 12:11:17.737	\N	1500	\N	\N	t
\.


--
-- Data for Name: publication_authors; Type: TABLE DATA; Schema: public; Owner: quarasek
--

COPY public.publication_authors (publications_id, authors_id, authors_key) FROM stdin;
3	1	0
3	2	1
6	5	0
9	8	0
13	11	0
13	12	1
16	15	0
19	18	0
22	21	0
26	24	0
26	25	1
29	28	0
32	31	0
35	34	0
38	37	0
41	40	0
44	43	0
47	46	0
50	49	0
53	52	0
57	55	0
57	56	1
60	59	0
63	62	0
66	65	0
69	68	0
74	43	0
74	71	1
74	72	2
74	73	3
77	52	0
79	52	0
81	52	0
83	52	0
87	85	0
87	86	1
90	89	0
93	92	0
96	95	0
99	98	0
102	101	0
105	104	0
108	107	0
110	107	0
112	11	0
112	31	1
116	114	0
116	115	1
119	118	0
123	121	0
123	122	1
126	125	0
129	128	0
132	131	0
135	134	0
138	137	0
141	140	0
144	143	0
147	146	0
150	149	0
153	152	0
157	155	0
157	156	1
160	159	0
163	162	0
166	165	0
169	168	0
173	171	0
173	172	1
175	155	0
175	168	1
178	177	0
181	168	0
181	180	1
185	183	0
185	184	1
188	187	0
191	28	0
191	190	1
195	193	0
195	194	1
198	197	0
201	200	0
203	98	0
206	205	0
211	208	0
211	209	1
214	213	0
216	15	0
221	220	0
224	223	0
227	226	0
230	229	0
233	232	0
236	235	0
239	238	0
242	241	0
244	213	0
247	246	0
250	249	0
253	252	0
255	62	0
258	257	0
262	260	0
262	261	1
264	220	0
267	1	0
267	266	1
270	269	0
273	272	0
276	275	0
279	278	0
282	281	0
284	200	0
287	286	0
291	289	0
291	290	1
295	294	0
297	269	0
300	299	0
303	302	0
306	305	0
309	308	0
312	311	0
315	314	0
318	317	0
321	320	0
323	125	0
326	325	0
329	328	0
332	331	0
335	334	0
338	337	0
341	340	0
345	343	0
345	344	1
349	347	0
349	348	1
352	351	0
354	46	0
356	52	0
359	358	0
362	361	0
365	364	0
367	364	0
370	369	0
372	369	0
375	374	0
378	377	0
381	380	0
384	383	0
387	386	0
391	390	1
211	269	2
394	393	0
397	396	0
400	399	0
407	406	0
410	409	0
413	305	0
413	412	1
416	415	0
418	415	0
420	415	0
423	415	0
423	422	1
426	425	0
429	428	0
431	11	0
433	11	0
435	11	0
437	11	0
439	11	0
443	11	0
443	441	1
443	442	2
445	11	0
445	125	1
447	11	0
447	200	1
449	122	0
452	451	0
455	454	0
458	457	0
461	460	0
464	463	0
467	466	0
470	466	0
470	469	1
473	472	0
476	475	0
479	478	0
482	481	0
485	484	0
488	487	0
491	490	0
493	89	0
495	89	0
497	89	0
500	89	0
500	499	1
504	502	0
504	503	1
507	506	0
511	509	0
514	513	0
517	516	0
519	516	0
522	521	0
525	524	0
527	71	0
530	529	0
533	532	0
535	187	0
538	537	0
543	540	0
546	545	0
548	52	0
550	52	0
552	52	0
554	52	0
556	52	0
558	52	0
560	52	0
562	52	0
565	564	0
568	567	0
571	570	0
574	573	0
576	140	0
582	140	0
582	68	1
582	578	2
582	579	3
582	580	4
582	581	5
586	584	0
586	585	1
589	588	0
592	591	0
594	213	0
596	213	0
598	213	0
600	213	0
602	213	0
604	213	0
606	213	0
608	213	0
614	613	0
617	616	0
619	616	0
623	621	0
623	622	1
627	625	0
627	626	1
630	629	0
642	266	0
645	644	0
647	644	0
649	484	0
652	651	0
656	654	0
656	655	1
659	658	0
661	98	0
663	98	0
666	665	0
668	146	0
671	670	0
674	673	0
677	676	0
680	679	0
682	679	0
685	684	0
688	687	0
691	690	0
694	693	0
697	696	0
700	699	1
702	699	1
704	699	1
706	699	1
708	699	1
711	710	0
714	713	0
716	390	0
718	390	0
720	390	0
511	1933	1
723	390	0
723	722	1
725	390	0
725	180	1
632	3900	0
634	3900	0
636	3900	0
638	3900	0
640	3900	0
404	3027	1
727	390	0
727	180	1
730	729	0
732	729	0
734	729	0
736	729	0
738	729	0
741	740	0
745	740	0
745	743	1
745	744	2
748	747	0
763	278	0
765	278	0
768	767	0
771	770	0
774	773	0
777	776	0
780	779	0
783	782	0
786	785	0
788	785	0
790	785	0
792	785	0
794	785	0
796	785	0
799	798	0
801	798	0
803	798	0
806	805	0
809	808	0
812	811	0
814	811	0
817	816	0
820	819	0
823	822	0
827	822	0
827	825	1
827	826	2
830	829	0
833	832	0
835	832	0
838	837	0
841	840	0
843	840	0
845	840	0
848	847	0
851	850	0
853	226	0
856	855	0
860	858	0
860	859	1
862	28	0
864	28	0
867	866	0
870	869	0
873	872	0
876	875	0
878	875	0
880	875	0
882	875	0
884	875	0
887	886	0
890	889	0
893	889	0
893	892	1
896	235	0
896	895	1
896	125	2
899	898	0
901	898	0
903	898	0
905	898	0
908	907	0
911	910	0
913	910	0
916	915	0
918	101	0
921	920	0
925	923	0
925	924	1
928	927	0
931	930	0
934	933	0
937	936	0
940	939	0
943	942	0
946	945	0
949	948	0
951	200	0
953	200	0
955	200	0
957	200	0
959	200	0
959	11	1
962	961	0
964	961	0
967	961	0
967	966	1
970	969	0
973	972	0
975	972	0
978	977	0
981	980	0
983	980	0
986	985	0
989	988	0
991	988	0
993	988	0
995	988	0
997	988	0
999	988	0
1002	1001	0
1005	1004	0
1007	1004	0
1009	1004	0
1011	180	0
1014	1013	0
1017	1016	0
1020	1019	0
1020	710	1
1023	1022	0
1026	1025	0
1028	1025	0
1031	1030	0
1034	1033	0
1036	172	0
1039	1038	0
1041	55	0
1044	55	0
1044	1043	1
1048	302	0
1048	1046	1
1048	1047	2
1051	1050	0
1063	1053	0
1063	1054	1
1063	1055	2
1063	1056	3
1063	1057	4
1063	1058	5
1063	1059	6
1063	1060	7
1063	1061	8
1063	1062	9
1066	364	0
1068	364	0
1071	1070	0
1074	1073	0
1076	1073	0
1079	1078	0
1084	1081	0
1084	1082	1
1084	1083	2
1087	1086	0
1089	43	0
1091	43	0
1094	1093	0
1096	1093	0
1098	1093	0
1101	1100	0
1104	1103	0
1107	1106	0
1110	1109	0
1112	209	0
1112	208	1
1115	1114	0
1117	1114	0
1119	1114	0
1121	1114	0
1124	1114	0
1124	1123	1
1132	1054	0
1132	1131	1
1135	1134	0
1137	1134	0
1139	305	0
1144	1141	0
1144	1142	1
1144	1143	2
1147	1146	0
1149	859	0
1151	859	0
1153	859	0
1155	425	0
1158	1157	0
1163	1160	0
1163	1161	1
1163	1162	2
1169	1165	0
1169	1166	1
1169	1167	2
1169	1168	3
1178	11	0
1180	11	0
1183	11	0
1183	1182	1
1185	122	0
1188	1187	0
1191	1190	0
1194	1193	0
1197	1196	0
1200	1199	0
1203	1202	0
1206	1205	0
1209	1208	0
1212	1211	0
1214	361	0
1217	1216	0
1221	1	0
1221	1219	1
1221	1220	2
1225	1223	0
1225	1224	1
1225	208	2
1225	1082	3
1228	1227	0
1231	1230	0
1234	1233	0
1237	1236	0
1240	1239	0
1246	1245	0
1246	134	1
1249	1248	0
1251	1248	0
1255	1253	0
1255	1254	1
1259	1258	0
1262	1261	0
1267	1266	0
1270	1269	0
1273	1272	0
1276	1275	0
1279	1278	0
1282	1281	0
1284	673	0
1286	1046	0
1288	1046	0
1290	137	0
1292	137	0
1295	1294	0
1306	213	0
1308	213	0
1310	213	0
1312	213	0
1314	213	0
1316	213	0
1318	1182	0
1321	1320	0
1323	610	0
1323	403	1
1326	1325	0
1328	1325	0
1330	1325	0
1341	1340	0
1344	1343	0
1347	1346	0
1350	889	0
1350	892	1
1350	1349	2
1353	1352	0
1356	1355	0
1365	134	0
1367	134	0
1369	134	0
1371	275	0
1374	1373	0
1377	1376	0
1380	1379	0
1382	1379	0
1385	1384	0
1388	1387	0
1391	1390	0
1393	390	0
1396	1395	0
1398	747	0
1403	1402	0
1405	785	0
1408	1407	0
1410	1407	0
1413	1412	0
1416	1415	0
1419	1418	0
1422	1421	0
1298	269	0
1243	71	0
1173	25	0
1424	208	0
1426	208	0
1429	1428	0
1431	840	0
1435	1433	0
1435	1434	1
1439	1438	0
1443	1442	0
1446	1445	0
1449	1448	0
1452	1451	0
1359	3027	0
1361	3027	0
1363	3027	0
1400	3987	0
1264	3987	0
1455	1454	0
1458	1457	0
1461	1460	0
1464	1463	0
1467	1466	0
1469	73	0
1471	73	0
1474	1473	0
1477	1476	0
1480	1479	0
1482	403	0
1484	403	0
1487	1486	0
1490	1489	0
1493	1492	0
1496	1495	0
1499	1498	0
1502	1498	0
1505	1504	0
1508	1507	0
1511	1510	0
1514	1513	0
1516	1082	0
1519	1518	0
1521	289	0
1521	290	1
1525	1524	0
1528	1527	0
1531	1030	0
1534	1533	0
1536	1533	0
1538	1533	0
1540	1533	0
1542	55	0
1544	55	0
1547	1546	0
1550	1549	0
1552	1549	0
1554	1109	0
1556	1109	0
1558	1109	0
1561	1560	0
1561	308	1
1563	425	0
1563	172	1
1566	1565	0
1569	1568	0
1571	1	0
1575	1574	0
1580	261	0
1582	261	0
1585	1584	0
1588	1587	0
1591	1587	0
1594	1587	0
1598	1597	0
1601	1600	0
1603	1600	0
1606	1325	0
1608	1325	0
1610	266	0
1614	266	0
1614	1613	1
1616	1355	0
1619	1355	0
1622	1355	0
1625	1355	0
1628	1355	0
1631	1355	0
1634	1355	0
1637	1355	0
1640	1355	0
1643	1355	0
1646	1355	0
1646	252	1
1650	1649	0
1652	275	0
1654	684	0
1654	252	1
1658	1657	0
1661	390	0
1668	1667	0
1668	915	1
1670	1667	0
1672	915	0
1675	915	0
1678	915	0
1681	915	0
1685	1684	0
1687	1466	0
1690	1689	0
1692	308	0
1694	252	0
1697	252	0
1700	252	0
1703	252	0
1706	252	0
1709	252	0
1712	252	0
1715	252	0
1718	252	0
1721	200	0
1724	1016	0
1727	1726	0
1730	1729	0
1733	1732	0
1736	1735	0
1739	1738	0
1742	1741	0
1745	1744	0
1748	1747	0
1751	1750	0
1754	1753	0
1757	1756	0
1760	1759	0
1763	1762	0
1766	1765	0
1768	85	0
1768	86	1
1772	1771	0
1774	213	0
1777	1776	0
1777	409	1
1780	1779	0
1780	1261	1
1783	1782	0
1787	1261	0
1790	1789	0
1794	1792	1
1798	1796	0
1798	1797	1
1802	1800	0
1802	1801	1
1804	85	0
1806	85	0
1809	1808	0
1811	92	0
1813	92	0
1815	92	0
1819	92	0
1821	92	0
1824	1823	0
1826	62	0
1832	1831	0
1835	1834	0
1835	886	1
1838	1837	0
1841	1840	0
1844	1843	0
1846	187	0
1484	3027	1
1663	3987	0
1785	3987	0
1851	1850	0
1855	1853	0
1855	1854	1
1862	1093	0
1865	1864	0
1871	1867	0
1871	1868	1
1871	1869	2
1871	1870	3
1875	1873	0
1875	1874	1
1882	1881	0
1882	11	1
1889	1887	0
1892	1891	0
1896	1894	0
1896	626	1
1896	1895	2
1901	1047	0
1901	1900	1
1904	1093	0
1904	1903	1
1904	11	2
1906	11	0
1906	1093	1
1910	1908	0
1910	1909	1
1913	1912	0
1917	1887	0
1917	1915	1
1917	1916	2
1923	1738	0
1923	1921	1
1923	1922	2
1926	1925	0
1929	1928	0
1931	930	0
1934	1933	0
1940	822	0
1940	1936	1
1940	1937	2
1940	1938	3
1940	1939	4
1943	209	0
1943	1942	1
1946	1945	0
1948	213	0
1951	1950	0
1954	1953	0
1957	1956	0
1960	1959	0
1964	693	0
1966	364	0
1969	1968	0
1972	1971	0
1975	1974	0
1977	213	0
1979	213	0
1982	1981	0
1985	1984	0
1988	1987	0
1991	1990	0
1994	1993	0
1994	441	1
1997	1996	0
2000	1999	0
2003	2002	0
2009	2008	0
2011	1765	0
2014	2013	0
2017	2016	0
2024	2023	0
2027	2026	0
2030	2029	0
2032	200	0
2035	2034	0
2037	1808	0
2040	2039	0
2044	930	0
2047	2046	0
2052	2051	0
2055	2054	0
2058	2057	0
2062	2060	0
2070	2069	0
2073	2072	0
2076	2075	0
2079	2078	0
2081	2078	0
2084	2083	0
2086	1874	0
2090	2088	0
2090	2089	1
2093	2092	0
2096	2095	0
2101	2100	0
2104	2103	0
2107	2106	0
2110	2109	0
2113	2112	0
2115	1574	0
2118	2117	0
2121	2120	0
2124	2123	0
2129	2126	0
2129	2127	1
2129	2128	2
2131	399	0
2133	399	0
2135	399	0
2137	399	0
2140	2139	0
2143	2142	0
2148	2145	0
2148	2146	1
2148	2100	2
2148	2147	3
2062	1797	1
1817	1933	0
1829	1933	1
1885	1202	0
2154	31	0
2156	567	0
2159	2158	0
2162	2161	0
2165	2164	0
2168	2167	0
2170	2167	0
2172	2167	0
2174	2167	0
2177	2176	0
2180	2179	0
2182	162	0
2185	2184	0
2188	2187	0
2191	2190	0
2194	2193	0
2196	785	0
2199	2198	0
2202	2201	0
2205	2204	0
1889	1915	1
2006	3900	0
2017	3900	1
1860	2820	0
1919	3987	0
1858	3987	0
1848	3987	0
1898	1877	1
2208	2207	0
2211	2210	0
2214	2213	0
2218	2217	0
2222	2221	0
2224	699	0
2226	699	0
2228	988	0
2231	1808	0
2234	2233	0
2237	2236	0
2239	798	0
2242	2241	0
2244	1030	0
2246	699	0
2248	409	0
2251	2250	0
2253	2139	0
2255	89	0
2257	89	0
2262	2261	0
2264	275	0
2266	693	0
2268	729	0
2270	785	0
2273	915	0
2273	2272	1
2276	2275	0
2276	2008	1
2278	2275	0
2280	1925	0
2283	2282	0
2285	2282	0
2287	98	0
2289	1355	0
2289	266	1
2291	275	0
2293	798	0
2296	2295	0
2299	2298	0
2301	1434	0
2304	2303	0
2306	390	0
2306	722	1
2308	785	0
2311	2310	0
2313	910	0
2315	1466	0
2317	200	0
2317	205	1
2320	2319	0
2323	2322	0
2326	2325	0
2326	1854	1
2328	2325	0
2328	1854	1
2331	2330	0
2334	2333	0
2337	2336	0
2340	822	0
2340	2339	1
2342	2008	0
2345	2344	0
2348	2347	0
2350	252	0
2352	1355	0
2355	2354	0
2358	2357	0
2358	948	1
2361	2360	0
2363	118	0
2366	2365	0
2369	2368	0
2372	2371	0
2375	2374	0
2380	2379	0
2383	2382	0
2386	2385	0
2388	1109	0
2388	1355	1
2391	2390	0
2391	729	1
2395	2393	0
2395	2394	1
2399	2397	0
2399	2398	1
2401	183	0
2404	2403	0
2406	86	0
2410	2408	0
2410	2409	1
2418	2417	0
2421	2420	0
2424	2423	0
2426	584	0
2428	584	0
2430	1325	0
2436	2434	0
2436	2435	1
2439	2438	0
2443	2442	0
2445	134	0
2447	2106	0
2451	785	0
2455	2453	0
2455	2454	1
2458	2457	0
2461	2460	0
2477	11	0
2477	2476	1
2480	2479	0
2484	2482	0
2484	2483	1
2486	1320	0
2493	2490	0
2493	2491	1
2493	2492	2
2496	2495	0
2496	1854	1
2499	2498	0
2499	2491	1
2501	200	0
2501	729	1
2152	2408	1
2504	2503	0
2504	1114	1
2508	2506	0
2508	2507	1
2510	1874	0
2512	1874	0
2514	1874	0
2516	1874	0
2516	1205	1
2519	2518	0
2522	2521	0
2525	2524	0
2527	260	0
2530	2529	0
2533	2532	0
2535	1916	0
2541	2540	0
2543	213	0
2546	2545	0
2549	2548	0
2551	1840	0
2553	1840	0
2555	499	0
2557	744	0
2560	2559	0
2259	3900	0
2538	1916	0
2449	3987	0
2377	3987	0
2562	915	0
2562	920	1
2564	920	0
2566	920	0
2569	2568	0
2572	2571	0
2575	2574	0
2577	2574	0
2580	2579	0
2583	2582	0
2586	2585	0
2592	2588	0
2592	2589	1
2592	2590	2
2592	2591	3
2598	200	0
2601	2600	0
2604	2603	0
2607	2606	0
2610	2609	0
2613	2612	0
2616	2615	0
2619	2618	0
2622	2621	0
2626	2625	0
2628	2625	0
2631	2630	0
2634	2633	0
2637	2636	0
2640	2639	0
2643	2642	0
2646	2645	0
2649	2648	0
2652	2651	0
2655	2654	0
2658	2657	0
2661	2660	0
2664	2663	0
2667	2666	0
2670	2669	0
2674	2673	0
2677	2676	0
2680	2679	0
2683	71	0
2683	2682	1
2687	840	0
2690	2689	0
2692	403	0
2695	2694	0
2698	2697	0
2700	294	0
2703	2702	0
2705	948	0
2709	985	0
2709	2707	1
2709	2708	2
2711	2142	0
2713	2142	0
2713	1030	1
2715	1141	0
2719	2717	0
2719	2718	1
2722	2721	0
2724	399	0
2726	399	0
2728	399	0
2730	399	0
2732	399	0
2734	399	0
2736	71	0
2741	2740	0
2744	2743	0
2746	1211	0
2749	2748	0
2752	2751	0
2755	5	0
2758	2757	0
2760	610	0
2763	2762	0
2767	2766	0
2770	2769	0
2773	2772	0
2776	2775	0
2779	2778	0
2782	2781	0
2784	226	0
2786	1486	0
2788	1466	0
2790	1466	0
2792	1466	0
2794	1030	0
2796	425	0
2799	122	0
2799	2798	1
2807	2806	0
2810	2809	0
2813	2812	0
2815	183	0
2818	2817	0
2821	2820	0
2823	2820	0
2826	2825	0
2832	613	0
2838	2834	0
2838	2835	1
2838	1937	2
2838	2836	3
2838	2837	4
2842	2841	0
2845	2844	0
2847	2844	0
2850	2849	0
2852	2689	0
2855	2854	0
2858	2857	0
2861	2860	0
2863	457	0
2867	2866	0
2867	101	1
2870	2866	0
2870	101	1
2873	2866	0
2873	101	1
2876	2866	0
2876	101	1
2879	2866	0
2882	2866	0
2882	101	1
2885	2866	0
2885	101	1
2889	2866	0
2889	101	1
2889	2888	2
2892	2866	0
2892	101	1
2895	2866	0
2895	101	1
2898	2866	0
2898	101	1
2901	2866	0
2901	101	1
2904	2866	0
2907	2866	0
2907	101	1
2910	2866	0
2910	101	1
2913	457	0
2916	457	0
2916	101	1
2919	457	0
2919	101	1
2738	3900	0
2594	2820	0
2685	3987	0
2922	457	0
2922	101	1
2926	2925	0
2929	2928	0
2929	101	1
2929	516	2
2932	457	0
2932	101	1
2932	516	2
2935	457	0
2935	101	1
2938	457	0
2938	101	1
2938	516	2
2941	457	0
2941	101	1
2944	457	0
2944	101	1
2947	457	0
2947	101	1
2950	457	0
2950	101	1
2953	457	0
2953	101	1
2956	457	0
2956	101	1
2959	457	0
2959	101	1
2962	457	0
2962	101	1
2965	457	0
2965	101	1
2968	457	0
2968	101	1
2971	457	0
2971	101	1
2974	457	0
2974	1070	1
2977	457	0
2977	1070	1
2980	457	0
2980	1070	1
2983	457	0
2983	1070	1
2983	101	2
2986	2866	0
2988	1908	0
2990	2272	0
2995	2992	0
2995	2993	1
2995	2994	2
2998	2997	0
2998	172	1
3001	3000	0
3003	1801	0
3005	2213	0
3008	3007	0
3011	3010	0
3014	3013	0
3017	3016	0
3019	889	0
3022	3021	0
3024	1938	0
3031	3030	0
3034	3033	0
3038	3036	0
3038	3037	1
3041	3040	0
3044	3043	0
3047	3046	0
3049	71	0
3052	1355	0
3052	3051	1
3055	3054	0
3061	2778	0
3063	2778	0
3066	2778	0
3066	3065	1
3069	3068	0
3072	3071	0
3075	3074	0
3078	3077	0
3081	3080	0
3083	200	0
3086	3085	0
3089	3088	0
3093	3091	0
3093	3092	1
3096	3095	0
3099	3098	0
3103	610	0
3106	3105	0
3108	403	0
3111	3110	0
3114	3113	0
3116	209	0
3119	3118	0
3122	3121	0
3125	3124	0
3128	3127	0
3131	3130	0
3134	3133	0
3137	3136	0
3139	2382	0
3142	3141	0
3144	208	0
3147	3146	0
3149	2213	0
3152	3151	0
3155	3154	0
3158	3157	0
3161	3160	0
3163	961	0
3166	3165	0
3170	3168	0
3170	3169	1
3172	2272	0
3175	2145	0
3175	3174	1
3177	936	0
3180	3179	0
3180	961	1
3186	3185	0
3189	3188	0
3189	1445	1
3191	3118	0
3193	2347	0
3195	822	0
3195	729	1
3197	73	0
3197	43	1
3200	213	0
3202	213	0
3205	3204	0
3208	3207	0
3211	3210	0
3211	1093	1
3213	1874	0
3217	3215	0
3217	3216	1
3219	3118	0
3222	441	0
3222	3221	1
3224	1868	0
3227	3226	0
3229	3127	0
3231	1597	0
3234	3233	0
3237	172	0
3237	3236	1
3241	3239	0
3241	3240	1
3234	1915	1
3183	187	1
3245	3243	0
3245	3244	1
3247	1864	0
3251	3249	0
3251	3250	1
3251	889	2
3254	3253	0
3256	3253	0
3258	2272	0
3262	172	0
3262	3260	1
3262	3261	2
3265	3264	0
3265	822	1
3270	1093	0
3270	3267	1
3274	3272	0
3274	3273	1
3278	3276	0
3278	3277	1
3278	892	2
3281	3280	0
3283	2207	0
3286	3285	0
3289	3288	0
3291	1959	0
3293	399	0
3296	3295	0
3299	3298	0
3303	1211	0
3303	3301	1
3303	3302	2
3305	699	0
3321	3307	0
3321	3308	1
3321	3309	2
3321	3310	3
3321	3311	4
3321	1732	5
3321	3312	6
3321	3313	7
3321	3314	8
3321	3315	9
3321	3316	10
3321	3317	11
3321	3318	12
3321	3319	13
3321	3320	14
3323	3127	0
3325	3127	0
3330	3327	0
3330	3328	1
3330	3329	2
3336	3335	0
3338	290	0
3341	3340	0
3344	3343	0
3347	3346	0
3350	3349	0
3353	3352	0
3356	3355	0
3359	3358	0
3364	1325	0
3367	3366	0
3369	98	0
3371	1325	0
3373	3133	0
3375	52	0
3378	3377	0
3381	3380	0
3384	3383	0
3386	172	0
3389	172	0
3389	3388	1
3389	3236	2
3391	172	0
3393	5	0
3396	3395	0
3399	3398	0
3404	441	0
3404	3401	1
3404	3402	2
3404	3403	3
3407	985	0
3407	3406	1
3407	1205	2
3407	1874	3
3409	11	0
3413	3411	0
3413	3412	1
3415	11	0
3420	168	0
3420	155	1
3420	3417	2
3420	3418	3
3420	172	4
3420	3419	5
3422	3277	0
3426	3424	0
3426	3425	1
3426	1874	2
3430	3240	0
3430	3428	1
3430	3429	2
3433	3432	0
3435	2123	0
3438	3437	0
3441	3440	0
3446	1938	0
3448	52	0
3451	3450	0
3453	696	0
3456	3455	0
3458	3455	0
3461	3460	0
3463	915	0
3466	3465	0
3469	3468	0
3469	172	1
3472	1874	0
3472	3471	1
3475	3474	0
3480	3477	0
3480	3478	1
3480	3479	2
3483	3482	0
3485	2190	0
3487	2190	0
3487	1854	1
3489	2190	0
3493	3491	0
3493	3492	1
3495	503	0
3499	3498	0
3506	3505	0
3509	3508	0
3512	3511	0
3514	936	0
3522	3516	0
3522	3517	1
3522	3518	2
3522	3519	3
3522	3520	4
3522	3521	5
3525	3524	0
3527	3524	0
3529	3524	0
3531	3524	0
3534	3533	0
3534	56	1
3534	1854	2
3444	1216	0
3537	3536	0
3540	3539	0
3543	3542	0
3546	3545	0
3548	3545	0
3550	56	0
3552	56	0
3554	56	0
3556	56	0
3558	56	0
3560	56	0
3562	56	0
3564	56	0
3566	56	0
3568	56	0
3572	3570	0
3572	3571	1
3572	1854	2
3576	3570	0
3576	3574	1
3576	3575	2
3578	3570	0
3578	3574	1
3578	3575	2
3581	3580	0
3584	3583	0
3586	2139	0
3588	2139	0
3590	2139	0
3592	2139	0
3594	2139	0
3596	2139	0
3598	2139	0
3600	2139	0
3602	2139	0
3604	2139	0
3606	2139	0
3608	2139	0
3610	2139	0
3612	2139	0
3614	2139	0
3616	2139	0
3618	2139	0
3620	2139	0
3622	2139	0
3624	2139	0
3626	2139	0
3628	2139	0
3630	2139	0
3632	2139	0
3634	2139	0
3638	2139	0
3638	3636	1
3638	3637	2
3641	2139	0
3641	3640	1
3643	2139	0
3643	3637	1
3643	1854	2
3645	2139	0
3645	3637	1
3645	1854	2
3647	2139	0
3647	3037	1
3649	2139	0
3649	3037	1
3652	2139	0
3652	3651	1
3655	2103	0
3655	3654	1
3659	3657	0
3659	3658	1
3661	699	0
3664	3663	0
3666	478	0
3668	478	0
3671	478	0
3671	3670	1
3674	3673	0
3676	3673	0
3679	3678	0
3679	2139	1
3682	3681	0
3685	3684	0
3688	3687	0
3688	610	1
3691	3690	0
3691	3684	1
3694	3693	0
3694	3640	1
3697	3696	0
3699	3696	0
3701	5	0
3701	3670	1
3705	3703	0
3705	3037	1
3705	3704	2
3707	1261	0
3710	3709	0
3712	537	0
3714	3036	0
3716	3036	0
3718	3036	0
3720	3036	0
3722	3036	0
3724	3036	0
3727	3036	0
3727	3726	1
3727	1854	2
3730	3036	0
3730	3729	1
3733	3732	0
3733	3658	1
3737	3735	0
3737	3736	1
3739	52	0
3741	52	0
3743	52	0
3746	52	0
3750	3748	0
3750	3749	1
3753	3752	0
3746	107	1
3753	567	1
3756	3755	0
3758	3755	0
3760	3755	0
3762	3755	0
3764	3755	0
3766	3755	0
3768	3755	0
3770	3755	0
3772	3755	0
3774	3755	0
3776	3755	0
3778	3755	0
3780	3755	0
3782	3755	0
3784	3755	0
3786	3755	0
3788	3755	0
3790	3755	0
3792	3755	0
3794	3755	0
3796	3755	0
3798	3755	0
3800	3755	0
3802	3755	0
3804	3755	0
3806	3755	0
3808	3755	0
3810	3755	0
3813	567	0
3813	3812	1
3816	567	0
3816	3815	1
3819	3571	0
3819	3818	1
3821	3571	0
3821	3574	1
3823	3571	0
3823	3574	1
3825	3571	0
3825	3574	1
3827	3571	0
3827	3574	1
3827	3818	2
3829	3571	0
3829	3574	1
3829	3818	2
3831	3571	0
3831	3574	1
3831	3818	2
3833	3571	0
3833	829	1
3835	3037	0
3837	3037	0
3839	3037	0
3841	3037	0
3841	829	1
3843	3037	0
3843	829	1
3846	3845	0
3846	567	1
3846	1854	2
3849	3848	0
3849	3574	1
3852	3851	0
3855	3854	0
3857	3854	0
3859	3854	0
3861	591	0
3863	591	0
3865	591	0
3867	591	0
3869	591	0
3871	591	0
3873	591	0
3875	591	0
3877	269	0
3879	269	0
3881	269	0
3888	269	0
3890	269	0
3890	3670	1
3892	269	0
3892	3670	1
3895	3894	0
3898	3897	0
3901	3900	0
3903	3900	0
3905	644	0
3907	644	0
3909	644	0
3911	644	0
3913	644	0
3915	644	0
3917	644	0
3919	644	0
3921	644	0
3923	644	0
3925	644	0
3927	644	0
3930	3929	0
3932	3574	0
3934	3574	0
3936	3574	0
3938	3574	0
3940	3574	0
3942	3574	0
3944	3574	0
3947	3946	0
3950	3949	0
3950	567	1
3950	1854	2
3952	98	0
3955	3954	0
3957	3954	0
3960	3959	0
3962	3729	0
3965	3964	0
3968	3967	0
3970	275	0
3975	3658	0
3975	3974	1
3978	3658	0
3978	3977	1
3981	3658	0
3981	3980	1
3984	3658	0
3984	3983	1
3984	1854	2
3988	3986	0
3988	3987	1
3883	269	0
3886	269	1
4028	3987	0
4028	1261	1
4031	4030	0
4034	4033	0
4037	4033	0
4040	4039	0
4040	3539	1
4042	829	0
4044	829	0
4046	829	0
4048	829	0
4050	829	0
4052	1782	0
4054	1782	0
4056	1782	0
4058	1782	0
4060	1782	0
4062	1782	0
4065	4064	0
4067	3654	0
4070	4069	0
4073	1476	0
4073	4072	1
4076	4075	0
4079	4078	0
4082	4081	0
4084	4081	0
4086	4081	0
4088	4081	0
4090	4081	0
4092	4081	0
4095	4081	0
4095	4094	1
4099	4097	0
4099	4098	1
4099	1854	2
4101	3651	0
4014	3987	0
4101	2139	1
4103	3651	0
4103	2139	1
4105	3651	0
4105	2139	1
4107	2139	0
4107	3651	1
4117	107	0
4128	4127	0
4131	4130	0
4134	4133	0
4136	4133	0
4136	3658	1
4139	4138	0
4142	4138	0
4142	4141	1
4144	4138	0
4144	4141	1
4146	4138	0
4146	4141	1
4148	3670	0
4150	3670	0
4152	3670	0
4154	3670	0
4156	3670	0
4158	3670	0
4160	3670	0
4162	3670	0
4165	4164	0
4168	4167	0
4171	4170	0
4173	4170	0
4175	4170	0
4177	55	0
4179	55	0
4181	55	0
4183	55	0
4185	55	0
4187	55	0
4189	55	0
4191	55	0
4193	55	0
4195	55	0
4197	55	0
4199	55	0
4201	55	0
4203	55	0
4205	55	0
4207	55	0
4209	55	0
4211	55	0
4213	55	0
4215	55	0
4217	55	0
4219	55	0
4221	55	0
4221	56	1
4223	55	0
4223	1043	1
4226	55	0
4226	4225	1
4226	1854	2
4228	55	0
4231	4230	0
4234	4230	0
4234	4233	1
4239	4236	0
4239	4237	1
4239	4238	2
4239	1854	3
4243	4242	0
4247	4246	0
4251	4246	0
4251	4250	1
4254	4246	0
4254	4250	1
4263	4261	0
4263	4262	1
4266	56	0
4269	56	0
4274	4272	0
4274	4273	1
4277	4276	0
4279	4276	0
4283	4282	0
4287	4286	0
4289	4286	0
4291	2139	0
4293	2139	0
4298	2139	0
4301	2139	0
4301	4300	1
4304	4303	0
4307	4303	0
4310	4309	0
4313	4309	0
4317	4316	0
4320	4319	0
4320	1854	1
4325	4323	0
4325	4324	1
4327	3885	0
4327	269	1
4327	1854	2
4330	4262	0
4334	4333	0
4109	107	0
4037	269	1
4296	2139	0
4337	4333	0
4339	4333	0
4344	4342	0
4344	4343	1
4347	3696	0
4351	4350	0
4351	3696	1
4356	4354	0
4356	4355	1
4356	1854	2
4359	3036	0
4362	52	0
4371	4370	0
4371	1854	1
4375	4374	0
4378	4377	0
4382	4377	0
4382	4381	1
4382	4282	2
4385	4377	0
4385	4381	1
4385	4282	2
4390	4388	0
4390	4389	1
4390	1854	2
4392	3037	0
4394	3037	0
4396	3037	0
4398	3037	0
4398	3036	1
4401	4400	0
4404	4400	0
4404	1854	1
4408	4407	0
4412	4411	0
4415	4414	0
4418	4417	0
4421	4420	0
4425	4424	0
4425	1854	1
4428	3729	0
4432	4431	0
4432	3670	1
4435	4434	0
4439	4438	0
4442	4438	0
4446	4438	0
4446	4445	1
4450	4449	0
4453	4452	0
4456	4452	0
4460	4459	0
4464	4462	0
4464	4463	1
4464	1854	2
4468	4462	0
4468	4467	1
4471	4462	0
4471	4467	1
4475	4462	0
4475	4474	1
4478	798	0
4481	4480	0
4484	4483	0
4486	4033	0
4489	4033	0
4492	4039	0
4495	4039	0
4495	4494	1
4495	1854	2
4497	4300	0
4500	4300	0
4503	4502	0
4506	4505	0
4509	4508	0
4509	3670	1
4513	4512	0
4513	1854	1
4517	4516	0
4520	4519	0
4524	4523	0
4527	4526	0
4531	4530	0
4533	3670	0
4536	3670	0
4539	3670	0
4542	3670	0
4545	3670	0
4548	3670	0
4551	3670	0
4554	3670	0
4557	3670	0
4560	3670	0
4563	3670	0
4566	3670	0
4569	3670	0
4572	3670	0
4575	3670	0
4575	3315	1
4579	4578	0
4583	4582	0
4587	4586	0
4591	4590	0
4593	55	0
4596	55	0
4599	55	0
4602	55	0
4605	55	0
4605	56	1
4607	55	0
4610	55	0
4613	55	0
4616	55	0
4619	55	0
4621	55	0
4621	56	1
4623	55	0
4623	56	1
4626	55	0
4626	56	1
4628	3036	0
4628	4230	1
4631	4630	0
4634	4630	0
4638	4637	0
4641	4640	0
4644	4643	0
4647	4646	0
4649	4343	0
4649	1854	1
4652	56	0
4656	4655	0
4656	1854	1
4659	3545	0
4661	3748	0
4663	56	0
4668	4666	0
4668	4667	1
4671	3670	0
4674	4530	0
4677	4508	0
4677	3670	1
4683	4680	0
4683	4681	1
4683	4682	2
4683	1854	3
4687	4686	0
4690	4689	0
4693	55	0
4695	3037	0
4697	4286	0
4699	3037	0
4701	4039	0
4701	3539	1
4704	4681	0
4704	4703	1
4707	4462	0
4707	4467	1
4710	55	0
4712	55	0
4714	1376	0
4716	55	0
4718	3037	0
4722	4720	0
4722	4721	1
4724	4039	0
4724	3539	1
4726	56	0
4728	56	0
4730	4316	0
4732	3037	0
4735	4734	0
4737	2139	0
4741	798	0
4743	55	0
4749	4748	0
4752	4751	0
4754	4751	0
4758	4757	0
4760	798	0
4763	4762	0
4765	4483	0
4771	4770	0
4774	4773	0
4776	4286	0
4779	4778	0
4782	4781	0
4785	4784	0
4788	4787	0
4791	4790	0
4794	4793	0
4799	4798	0
4801	4798	0
4803	4784	0
4806	4805	0
4806	4273	1
4808	4590	0
4811	4810	0
4813	2139	0
4816	4815	0
4821	4820	0
4823	4512	0
4823	4097	1
4825	3539	0
4825	3037	1
4827	55	0
4827	4097	1
4827	1854	2
4829	4097	0
4831	4039	0
4831	3539	1
4833	4762	0
4836	4835	1
4840	3037	0
4842	56	0
4842	55	1
4844	4316	0
4846	4512	0
4846	4097	1
4851	2139	0
4853	4757	0
4863	4862	0
4866	4865	0
4870	4868	0
4870	4869	1
4872	798	0
4875	4874	0
4877	3432	0
4880	4879	0
4886	4885	0
4889	4888	0
4892	4891	0
4895	4894	0
4897	1407	0
4900	4899	0
4906	377	0
4908	1114	0
4915	4914	0
4917	98	0
4926	4925	0
4926	942	1
4928	1082	0
4930	4286	0
4932	1466	0
4938	4937	0
4941	4940	0
4944	4943	0
4947	4946	0
4951	4949	0
4951	4950	1
4955	4954	1
4958	4957	0
4961	4960	0
4963	4343	0
4967	4965	0
4967	4966	1
4969	4316	0
4969	4805	1
4972	4971	0
4935	811	0
4796	4815	0
4883	4781	0
4739	25	0
4818	25	0
4902	25	0
4910	25	0
4975	4974	0
4978	4977	0
4982	4980	0
4982	4981	1
4985	4984	0
4988	4987	0
4991	4990	0
4994	4993	0
4997	4996	0
5000	4999	0
5003	5002	0
5006	5005	0
5009	5008	0
5012	5011	0
5016	5014	0
5016	5015	1
5019	5018	0
5022	5021	0
5025	5024	0
5028	5027	0
5031	5030	0
5037	5036	0
5040	5039	0
5043	5042	0
5046	5045	0
5049	5048	0
5052	5051	0
5055	5054	0
5058	5057	0
5061	5060	0
5064	5063	0
5066	4721	0
5069	5068	0
5072	5071	0
5075	5074	0
5078	5077	0
5081	5080	0
5084	5083	0
5088	5086	0
5088	5087	1
5091	5090	0
5094	5093	0
5097	5096	0
5099	591	0
5101	4081	0
5103	4721	0
5106	5105	0
5109	5108	0
5112	5111	0
5114	55	0
5117	5116	0
5119	3670	0
5122	5121	0
5125	5124	0
5128	5127	0
5128	4949	1
5130	5057	0
5132	5127	0
5132	4286	1
5134	5127	0
5134	4286	1
5137	4630	0
5140	4630	0
5144	5143	0
5147	5146	0
5153	5151	0
5153	5152	1
5156	5155	0
5159	4494	0
5159	5158	1
5159	1854	2
5161	4494	0
5161	1854	1
5164	5163	0
5167	5166	0
5170	5169	0
5173	5172	0
5176	5175	0
5179	5178	0
5182	5175	0
4912	1915	0
4904	3900	0
4923	3987	0
4921	3987	0
4919	3987	0
4860	3987	0
4856	25	1
5182	5181	1
5185	5184	0
5188	5187	0
5190	89	0
5192	785	0
5195	5194	0
5198	5197	0
5202	5200	0
5202	5201	1
5205	5204	0
5208	5207	0
5211	5210	0
5214	2393	0
5216	5215	0
5218	5217	0
5222	5219	0
5222	5220	1
5222	5221	2
5225	5224	0
5228	5227	0
5230	2344	0
5233	5231	0
5233	5232	1
5236	5235	0
5239	5238	0
4746	4720	0
5033	4720	0
3362	811	0
5261	5260	0
2464	118	0
2466	118	0
2468	118	0
2470	118	0
2472	118	0
2474	118	0
1127	118	0
1129	118	0
391	386	2
2019	386	0
2021	386	0
2049	386	0
3028	3539	1
3101	3539	0
4768	3539	0
1962	1933	1
5262	5158	0
5263	5254	0
5264	5251	0
5265	5250	0
4259	4815	1
4799	4815	1
4801	4815	1
4836	4815	2
4838	4815	0
4849	4815	1
3502	5266	1
5267	107	0
3852	107	2
3875	107	1
3888	107	1
4111	107	0
4113	107	0
4119	107	0
4121	107	0
4123	107	0
4125	107	0
218	269	1
1300	269	0
1302	269	0
1304	269	1
5230	1971	1
2828	71	0
2830	71	0
5268	4781	0
5268	25	1
5268	3539	2
5270	4781	0
5272	5271	0
5277	5274	0
5277	5275	1
5277	5273	2
5279	5276	0
5279	5273	1
5282	5281	0
2802	11	0
2804	11	0
700	3450	2
702	3450	2
704	3450	2
706	3450	2
708	3450	2
3972	3450	1
4955	3450	2
3059	3450	1
4872	3450	1
5149	3450	1
2065	3450	1
2413	85	0
2415	85	0
2042	1915	0
3245	1915	2
2067	3900	0
2098	3900	0
2432	3900	0
2488	3900	0
543	3900	2
1332	3900	0
1334	3900	0
1336	3900	0
1338	3900	0
5222	1916	3
1578	2820	0
1794	2820	2
2596	2820	0
4365	4646	0
4367	4646	0
3994	3987	0
761	3987	0
5212	3987	0
4858	3987	0
4026	3987	0
4024	3987	0
4022	3987	0
4020	3987	0
4018	3987	0
4016	3987	0
4012	3987	0
4010	3987	0
4008	3987	0
4006	3987	0
4004	3987	0
4002	3987	0
4000	3987	0
3998	3987	0
3996	3987	0
3992	3987	0
3990	3987	0
1665	3987	0
757	3987	0
755	3987	0
753	3987	0
751	3987	0
759	3987	0
5284	5283	0
5286	5285	0
5286	25	1
5289	5288	0
5291	1874	0
5291	5290	1
5293	2190	0
5295	936	0
5298	5297	0
5299	3127	0
5301	1908	0
5303	936	0
5305	936	0
5308	936	0
5308	5307	1
5311	3508	0
5311	5310	1
5314	5313	0
5314	172	1
5317	5316	0
5317	969	1
5327	172	0
5330	822	0
5330	5329	1
5352	3127	0
5356	1415	0
5319	969	0
5319	5316	1
5322	5321	0
5325	5324	0
5332	822	0
5332	3179	1
5334	125	0
5337	5336	0
5339	2282	0
5342	5341	0
5345	822	0
5345	5344	1
5348	5347	0
5350	985	0
5350	2708	1
5354	1597	0
5359	5358	0
5362	5361	0
5366	403	0
5369	5368	0
5371	1868	0
5373	2158	0
5373	773	1
5376	5375	0
5379	5378	0
5379	673	1
5381	5380	0
5383	5382	0
5386	5385	0
5391	5388	0
5391	5389	1
5391	5390	2
5394	5380	0
5394	5393	1
5397	5396	0
5402	855	0
5408	5406	0
5408	5407	1
5411	5410	0
5414	5413	0
5416	5390	0
5422	5389	0
5422	5421	1
5425	5424	0
5427	5410	0
5427	5426	1
5432	5431	0
5435	5434	0
5438	5437	0
5441	5440	0
5444	5443	0
5447	5446	0
5449	5380	0
5452	5451	0
5454	5380	0
5456	146	0
5458	889	0
5461	5380	0
5461	5460	1
5464	5463	0
5466	687	0
5468	5451	0
5471	5470	0
5473	1352	0
5476	5475	0
5478	948	0
5481	5480	0
5484	5483	0
5487	5486	0
5491	5489	0
5491	5490	1
5493	869	0
5495	4987	0
5498	5497	0
5501	5500	0
5504	5503	0
5506	2354	0
5509	5508	0
5512	5511	0
5515	5514	0
5518	5517	0
5520	5517	0
5523	5522	0
5526	5525	0
5529	5528	0
5533	5531	0
5533	5532	1
5535	5534	0
5536	5534	0
5538	855	0
5539	855	0
5542	5541	0
5545	5544	0
5548	5547	0
5548	2772	1
5550	5489	0
5552	5551	0
5555	5554	0
5558	5557	0
5561	5560	0
5564	5563	0
5567	5566	0
5570	5569	0
5573	5572	0
5575	1166	0
5578	5577	0
5581	5580	0
5583	2772	0
5585	2322	0
5588	5587	0
5591	5590	0
5594	5593	0
5597	5596	0
5599	5541	0
5601	5587	0
5604	5603	0
5607	5606	0
5608	4879	0
5610	4879	0
5612	4879	0
5614	98	0
5616	567	0
5619	5618	0
5622	5621	0
5625	5624	0
5628	5627	0
5630	5629	0
5633	5632	0
5635	5632	0
5637	5632	0
5639	5632	0
5641	5632	0
5643	5632	0
5645	5587	0
5647	5451	0
5648	5451	0
5650	5380	0
5658	5657	0
5661	5660	0
5664	5663	0
5667	5666	0
5670	5669	0
5673	5672	0
5676	5675	0
5679	5678	0
5682	5681	0
5684	673	0
5684	1134	1
5419	2772	0
5653	1797	0
5655	1797	0
5400	1797	0
5404	1797	0
1879	1877	1
1879	25	0
1898	25	0
4856	4855	0
3270	386	2
3270	25	3
5692	1912	0
5693	3508	0
5693	5310	1
5696	5695	0
5698	626	0
5701	5700	0
5703	3207	0
5705	3207	0
5705	5704	1
5709	1831	0
5709	5707	1
5709	5708	2
5711	1352	0
5714	11	0
5717	5716	0
5719	25	0
5721	25	0
1176	25	0
5726	25	0
5726	5722	1
5726	5723	2
5726	5724	3
5726	5725	4
5729	5728	0
5729	25	1
5732	5730	0
5732	5731	1
5737	5734	0
5740	5175	0
5740	5739	1
5744	5743	0
5746	25	0
5752	3411	0
5753	3411	0
5753	5748	1
5753	5749	2
5755	25	0
5755	5750	1
5756	25	0
5756	1877	1
5756	5751	2
5756	5750	3
5767	5758	0
5767	5759	1
5767	5760	2
5767	5761	3
5767	5762	4
5767	5763	5
5767	5764	6
5767	5765	7
5767	5766	8
5770	5769	0
5774	5772	0
5774	3539	1
5774	5773	2
5779	5776	0
5779	5777	1
5779	5778	2
5785	5781	0
5785	5782	1
5785	5783	2
5785	386	3
5785	5784	4
5788	5787	0
5800	5790	0
5800	5791	1
5800	5792	2
5800	5793	3
5800	5794	4
5800	5795	5
5800	5796	6
5800	5797	7
5800	5798	8
5800	5799	9
5802	1874	0
5806	5804	0
5806	5805	1
5806	1971	2
5808	1971	0
5808	5805	1
5812	5810	0
5812	5811	1
5812	5769	2
5818	5784	0
5818	5814	1
5818	5815	2
5818	5816	3
5818	5817	4
5822	5820	0
5822	5821	1
5828	5824	0
5828	1379	1
5828	5825	2
5828	5826	3
5828	5827	4
5835	5830	0
5835	5831	1
5835	5832	2
5835	5833	3
5835	5834	4
5838	5830	0
5838	5831	1
5838	5832	2
5838	5837	3
5838	5833	4
5838	5834	5
5844	5840	0
5844	5841	1
5844	5842	2
5844	5843	3
5848	5846	0
5848	5847	1
5851	5850	0
5855	5853	0
5855	5854	1
5858	5857	0
5863	5860	0
5863	5861	1
5863	5862	2
5866	25	0
5866	5865	1
5869	5868	0
5876	5871	0
5876	5872	1
5876	5873	2
5876	5874	3
5876	5875	4
5879	5878	0
5882	5824	0
5882	5881	1
5885	5884	0
5890	5887	0
5890	5888	1
5890	5889	2
5890	5728	3
5892	5887	0
5892	5889	1
5892	5728	2
5898	5894	0
5898	5895	1
5898	5896	2
5898	5897	3
5900	5853	0
5904	5902	0
5904	5903	1
5908	25	0
5908	5906	1
5908	5907	2
5924	25	0
5924	5910	1
5924	1093	2
5924	5911	3
5924	5853	4
5924	5912	5
5924	5850	6
5924	5913	7
5924	5914	8
5924	5915	9
5924	5916	10
5924	5917	11
5924	5918	12
5924	5750	13
5924	5919	14
5924	5815	15
5924	5920	16
5924	5921	17
5924	5922	18
5924	5923	19
5924	5781	20
5928	5926	0
5928	5235	1
5928	5927	2
5931	5930	0
5933	5930	0
5935	5930	0
5942	5937	0
5942	5938	1
5942	5939	2
5942	5940	3
5942	5941	4
5948	5944	0
5948	5945	1
5948	5946	2
5948	5947	3
5951	5944	0
5951	5945	1
5951	5946	2
5951	5950	3
5951	5947	4
5953	1093	0
5960	5955	0
5960	5956	1
5960	5941	2
5960	5957	3
5960	5958	4
5960	5959	5
5963	5962	0
5966	5965	0
5969	5968	0
5975	5971	0
5975	5972	1
5975	5973	2
5975	5974	3
5980	5977	0
5980	5978	1
5980	5979	2
5983	5977	0
5983	5978	1
5983	5982	2
5986	5977	0
5986	5978	1
5986	5985	2
5989	5977	0
5989	5978	1
5989	5988	2
5992	5977	0
5992	5991	1
5992	5978	2
5999	25	0
5999	5994	1
5999	4781	2
5999	5853	3
5999	1874	4
5999	5995	5
5999	5996	6
5999	5997	7
5999	5920	8
5999	5998	9
6003	6001	0
6003	6002	1
6007	6005	0
6007	6006	1
6021	5824	0
6021	6009	1
6021	6010	2
6021	6011	3
6021	6012	4
6021	6013	5
6021	6014	6
6021	6015	7
6021	6016	8
6021	6017	9
6021	1379	10
6021	6018	11
6021	6019	12
6021	6020	13
6024	6023	0
6031	6026	0
6031	6027	1
6031	6028	2
6031	6029	3
6031	6030	4
6036	5997	0
6036	6033	1
6036	6034	2
6036	6035	3
6038	386	0
6040	5996	0
6040	25	1
6040	4781	2
6040	5994	3
6040	5920	4
6044	5923	0
6044	386	1
6044	6042	2
6044	6043	3
6046	6043	0
6046	5923	1
6050	6048	0
6050	5923	1
6050	386	2
6050	6049	3
6056	6052	0
6056	6053	1
6056	6054	2
6056	6055	3
6060	6058	0
6060	6059	1
6063	6062	0
6068	5991	0
6068	6065	1
6068	6066	2
6068	6067	3
6072	6070	0
6072	6071	1
6079	6074	0
6079	6075	1
6079	6076	2
6079	6077	3
6079	6078	4
6081	386	0
6084	6083	0
6089	6086	0
6089	6087	1
6089	6088	2
6093	5830	0
6093	5831	1
6093	6091	2
6093	5833	3
6093	5834	4
6093	6092	5
6097	5941	0
6097	6095	1
6097	6096	2
6101	5850	0
6101	6099	1
6101	6100	2
6105	6103	0
6105	5941	1
6105	6104	2
6105	5959	3
6108	6107	0
6113	6110	0
6113	6111	1
6113	6112	2
6115	5810	0
6115	5769	1
6119	6117	0
6119	6118	1
6123	6121	0
6123	6122	1
6128	6125	0
6128	6126	1
6128	6083	2
6128	6127	3
6133	6130	0
6133	6131	1
6133	6132	2
6145	6135	0
6145	6136	1
6145	6137	2
6145	6138	3
6145	6139	4
6145	6140	5
6145	6141	6
6145	6142	7
6145	6143	8
6145	6144	9
6150	6147	0
6150	6148	1
6150	6149	2
6153	4781	0
6153	5996	1
6153	5853	2
6153	6152	3
6153	1874	4
6153	5995	5
6153	5997	6
6153	5994	7
6153	5920	8
6153	5998	9
6153	25	10
6155	5922	0
6157	5996	0
6160	6159	0
6163	6162	0
6165	6162	0
6168	6167	0
6171	6170	0
6175	6173	0
6175	6174	1
6182	6177	0
6182	6178	1
6182	6179	2
6182	6180	3
6182	6181	4
6192	6184	0
6192	6185	1
6192	6186	2
6192	6187	3
6192	6188	4
6192	6189	5
6192	6190	6
6192	6191	7
6195	6194	0
6197	6086	0
6197	6087	1
6197	6088	2
6201	6199	0
6201	6200	1
6204	6203	0
6206	386	0
6208	5865	0
6211	6210	0
6213	5971	0
6213	5974	1
6216	5824	0
6216	1379	1
6216	6012	2
6216	6009	3
6216	6014	4
6216	6015	5
6216	6010	6
6216	6019	7
6216	6011	8
6216	6013	9
6216	6016	10
6216	6018	11
6216	6215	12
6216	6020	13
6219	6218	0
6221	6218	0
6224	6223	0
6233	6226	0
6233	6227	1
6233	6228	2
6233	6229	3
6233	5772	4
6233	6230	5
6233	6159	6
6233	6223	7
6233	6231	8
6233	6232	9
6280	6235	0
6280	6236	1
6280	6237	2
6280	6238	3
6280	6239	4
6280	6240	5
6280	6241	6
6280	6242	7
6280	6243	8
6280	6244	9
6280	6245	10
6280	6246	11
6280	6247	12
6280	6248	13
6280	6249	14
6280	6250	15
6280	6251	16
6280	6252	17
6280	6253	18
6280	6254	19
6280	6255	20
6280	6256	21
6280	6257	22
6280	6258	23
6280	6259	24
6280	6260	25
6280	6261	26
6280	6262	27
6280	6263	28
6280	6264	29
6280	6265	30
6280	6266	31
6280	6267	32
6280	6268	33
6280	6269	34
6280	6270	35
6280	6271	36
6280	6272	37
6280	6273	38
6280	6274	39
6280	6275	40
6280	6276	41
6280	6277	42
6280	6278	43
6280	6279	44
6284	5998	0
6284	6282	1
6284	6283	2
6294	25	0
6294	6286	1
6294	386	2
6294	5853	3
6294	6287	4
6294	4781	5
6294	6288	6
6294	6289	7
6294	6290	8
6294	6291	9
6294	5998	10
6294	6292	11
6294	6293	12
6300	6296	0
6300	6297	1
6300	6298	2
6300	6299	3
6353	6302	0
6353	6303	1
6353	6304	2
6353	6305	3
6353	6306	4
6353	6307	5
6353	6308	6
6353	6309	7
6353	6310	8
6353	6311	9
6353	6312	10
6353	6313	11
6353	6314	12
6353	6315	13
6353	6316	14
6353	6317	15
6353	6318	16
6353	6319	17
6353	6320	18
6353	6321	19
6353	6322	20
6353	6323	21
6353	6324	22
6353	6325	23
6353	6326	24
6353	6327	25
6353	6328	26
6353	6329	27
6353	6330	28
6353	6331	29
6353	6332	30
6353	6333	31
6353	6334	32
6353	6335	33
6353	6336	34
6353	6337	35
6353	6338	36
6353	6339	37
6353	6340	38
6353	6341	39
6353	6342	40
6353	6343	41
6353	6344	42
6353	6345	43
6353	6346	44
6353	6347	45
6353	6348	46
6353	6349	47
6353	6350	48
6353	6351	49
6353	6352	50
6365	5761	0
6365	6355	1
6365	5765	2
6365	6356	3
6365	6357	4
6365	6358	5
6365	6359	6
6365	6360	7
6365	6361	8
6365	6362	9
6365	5762	10
6365	6363	11
6365	6364	12
6368	6367	0
6370	5889	0
6372	5889	0
6375	6292	0
6375	6289	1
6375	6291	2
6375	6374	3
6378	6377	0
6381	6380	0
6384	6383	0
6386	5918	0
6391	5920	0
6391	6388	1
6391	6389	2
6391	6390	3
6396	6393	0
6396	6394	1
6396	6395	2
6399	6398	0
6404	6401	0
6404	6402	1
6404	6403	2
6404	5872	3
6406	1801	0
6408	1801	0
6410	6398	0
6412	5897	0
6412	5895	1
6412	5896	2
6412	5894	3
6414	386	0
6418	6416	0
6418	6417	1
6420	6416	0
6422	1801	0
6426	6424	0
6426	6425	1
6428	71	0
6431	6430	0
6434	6433	0
6440	6436	0
6440	6437	1
6440	6438	2
6440	6439	3
6443	6442	0
6446	6445	0
6448	6445	0
6451	6450	0
6454	6453	0
6459	6456	0
6459	6457	1
6459	6458	2
6461	71	0
6465	6463	0
6465	6464	1
6468	6467	0
6470	5896	0
6474	6472	0
6474	6473	1
6476	5995	0
6478	5926	0
6478	5235	1
6478	5927	2
6480	5926	0
6480	5235	1
6480	5927	2
6483	6482	0
6485	5926	0
6485	5235	1
6485	5927	2
6488	5941	0
6488	5956	1
6488	5955	2
6488	6487	3
6491	5846	0
6491	6490	1
6494	5921	0
6494	6493	1
6494	5922	2
6497	6450	0
6497	6496	1
6499	6445	0
6508	6501	0
6508	6502	1
6508	6503	2
6508	6504	3
6508	6505	4
6508	6506	5
6508	6507	6
6512	6510	0
6512	6511	1
6517	6514	0
6517	6515	1
6517	6516	2
6519	1874	0
6523	6521	0
6523	6522	1
6523	5235	2
6534	6525	0
6534	6526	1
6534	6527	2
6534	6528	3
6534	6529	4
6534	6530	5
6534	6531	6
6534	6532	7
6534	6533	8
6540	5920	0
6540	6388	1
6540	6536	2
6540	6537	3
6540	6538	4
6540	6539	5
6546	6053	0
6546	6542	1
6546	6543	2
6546	6544	3
6546	6545	4
6549	1874	0
6549	6548	1
6549	2636	2
6551	4781	0
6551	25	1
6551	3539	2
6556	6553	0
6556	6554	1
6556	6555	2
6558	1093	0
6563	6560	0
6563	6561	1
6563	6562	2
6566	6565	0
6575	6568	0
6575	6569	1
6575	6570	2
6575	6571	3
6575	6572	4
6575	6573	5
6575	6574	6
6575	1874	7
6581	6577	0
6581	6578	1
6581	6579	2
6581	6580	3
6583	4781	0
6583	25	1
6596	6595	0
6603	6595	0
6605	2379	0
6608	6607	0
6614	6607	0
6615	6607	0
6617	6607	0
6633	6607	0
6637	6607	0
6639	6607	0
6643	6607	0
6647	6607	0
6649	6607	0
6651	6607	0
6655	6607	0
6657	6607	0
6659	6607	0
6661	6607	0
6663	6607	0
6665	6607	0
6667	6607	0
6673	6607	0
6675	6607	0
6677	6607	0
6679	6607	0
6681	6607	0
6694	6607	0
6700	6595	0
6701	6607	0
6702	6607	0
6704	6607	0
6712	6607	0
6714	6607	0
6718	6607	0
6720	6607	0
6722	6607	0
6724	6607	0
\.


--
-- Data for Name: publication_databases; Type: TABLE DATA; Schema: public; Owner: quarasek
--

COPY public.publication_databases (publications_id, databases_id) FROM stdin;
4902	5687
5268	5687
1862	5690
3499	5691
5721	5720
4910	5687
4910	5720
1879	5720
1898	5720
4818	5687
4818	5720
1173	5720
4739	5690
4739	5720
1176	5720
3270	5720
5286	5720
26	5720
4856	5687
4856	5720
5726	5720
5230	5691
5156	5687
5719	5720
5753	5720
5752	5720
5746	5720
5755	5720
5756	5720
5729	5720
\.


--
-- Data for Name: publication_files; Type: TABLE DATA; Schema: public; Owner: quarasek
--

COPY public.publication_files (publication_id, files_id) FROM stdin;
\.


--
-- Data for Name: publication_keywords; Type: TABLE DATA; Schema: public; Owner: quarasek
--

COPY public.publication_keywords (publication_id, keywords_id) FROM stdin;
\.


--
-- Data for Name: publication_platforms; Type: TABLE DATA; Schema: public; Owner: quarasek
--

COPY public.publication_platforms (publication_id, platforms_id) FROM stdin;
6	1
9	1
13	1
16	1
19	1
90	1
29	1
32	1
35	1
38	1
41	1
44	1
47	1
53	1
57	1
60	1
63	1
69	1
74	1
77	1
79	1
81	1
83	1
93	1
96	1
99	1
102	1
108	1
22	1
105	1
112	1
116	1
123	1
126	1
135	1
138	1
141	1
129	1
153	1
160	1
163	1
169	1
173	1
178	1
181	1
188	1
191	1
195	1
203	1
206	1
214	1
216	1
218	1
224	1
147	1
230	1
233	1
144	1
5270	1
5279	1
264	1
244	1
247	1
250	1
253	1
255	1
258	1
262	1
387	1
267	1
270	1
273	1
279	1
282	1
295	1
287	1
297	1
303	1
306	1
309	1
239	1
315	1
318	1
321	1
326	1
329	1
332	1
338	1
341	1
345	1
312	1
354	1
359	1
362	1
365	1
367	1
370	1
372	1
375	1
378	1
381	1
384	1
391	1
394	1
397	1
400	1
404	1
407	1
413	1
426	1
429	1
431	1
433	1
435	1
437	1
439	1
443	1
445	1
416	1
452	1
458	1
461	1
464	1
467	1
473	1
479	1
482	1
488	1
491	1
470	1
511	1
514	1
517	1
519	1
522	1
527	1
574	1
530	1
535	1
538	1
476	1
493	1
548	1
550	1
552	1
554	1
556	1
759	1
507	1
560	1
565	1
568	1
571	1
576	1
582	1
589	1
592	1
594	1
596	1
598	1
600	1
602	1
604	1
606	1
608	1
614	1
617	1
619	1
623	1
652	1
656	1
659	1
627	1
586	1
5567	1
663	1
666	1
668	1
677	1
680	1
682	1
685	1
688	1
691	1
711	1
714	1
716	1
718	1
720	1
725	1
727	1
730	1
732	1
734	1
736	1
738	1
741	1
748	1
751	1
753	1
755	1
757	1
671	1
694	1
763	1
768	1
771	1
774	1
777	1
780	1
783	1
786	1
790	1
792	1
794	1
799	1
801	1
803	1
809	1
812	1
814	1
817	1
820	1
823	1
827	1
830	1
833	1
835	1
838	1
841	1
843	1
845	1
851	1
806	1
856	1
860	1
862	1
853	1
867	1
876	1
878	1
880	1
882	1
884	1
887	1
890	1
1658	1
1931	1
896	1
899	1
901	1
903	1
905	1
908	1
911	1
916	1
918	1
921	1
925	1
928	1
931	1
934	1
937	1
943	1
946	1
873	1
951	1
970	1
967	1
978	1
983	1
986	1
989	1
991	1
993	1
995	1
999	1
1002	1
1005	1
1007	1
1009	1
1014	1
1017	1
1020	1
1023	1
1026	1
1028	1
1031	1
1034	1
1036	1
1039	1
1041	1
1044	1
1048	1
1051	1
1063	1
2006	1
973	1
1071	1
1074	1
1076	1
1079	1
1084	1
1087	1
1091	1
1094	1
1096	1
1104	1
1107	1
1115	1
1117	1
1119	1
1121	1
1124	1
1127	1
1132	1
1137	1
1139	1
1144	1
1147	1
1149	1
1151	1
1153	1
1155	1
1163	1
1169	1
1112	1
1178	1
1183	1
1185	1
1197	1
1200	1
1203	1
1206	1
1212	1
1214	1
1221	1
1225	1
1228	1
1188	1
1237	1
1240	1
1173	1
1176	1
1243	1
1249	1
1251	1
1255	1
1259	1
1262	1
1264	1
1270	1
1273	1
1276	1
1284	1
1231	1
1288	1
1290	1
1295	1
1298	1
1300	1
1302	1
1304	1
1308	1
1310	1
1312	1
1314	1
1316	1
1318	1
1321	1
1347	1
1326	1
1328	1
1334	1
1336	1
1338	1
2377	1
1341	1
1353	1
1356	1
1359	1
1361	1
1363	1
1365	1
1367	1
1369	1
1371	1
1385	1
1388	1
1391	1
1393	1
1398	1
1400	1
1403	1
1405	1
1408	1
1413	1
1416	1
1419	1
1422	1
1424	1
1426	1
1431	1
1458	1
1439	1
1446	1
1452	1
1377	1
1380	1
1464	1
1429	1
1469	1
1474	1
1480	1
1482	1
1487	1
1490	1
1493	1
1496	1
1502	1
1505	1
1508	1
1511	1
1516	1
1519	1
1521	1
1525	1
1552	1
1531	1
1534	1
1536	1
1538	1
1540	1
1542	1
1544	1
1547	1
1554	1
1556	1
1561	1
1563	1
1566	1
1569	1
1571	1
1601	1
1603	1
1588	1
1591	1
1594	1
1598	1
1606	1
1608	1
1610	1
1616	1
1619	1
1622	1
1625	1
1628	1
1631	1
1634	1
1637	1
1640	1
1643	1
1646	1
1650	1
1654	1
1578	1
1661	1
1665	1
1668	1
1670	1
1672	1
1675	1
1678	1
1681	1
1685	1
1690	1
1692	1
1694	1
1697	1
2174	1
1700	1
1703	1
1706	1
1712	1
1715	1
1718	1
1739	1
1724	1
1730	1
1733	1
1742	1
1748	1
1751	1
1757	1
1760	1
1766	1
1768	1
1772	1
1777	1
1780	1
1783	1
1785	1
1787	1
1790	1
1721	1
1798	1
1804	1
1806	1
1809	1
1813	1
1817	1
1929	1
1819	1
1821	1
1824	1
1826	1
1829	1
1832	1
1835	1
1838	1
1841	1
1846	1
1848	1
1860	1
1858	1
1896	1
1865	1
1871	1
1875	1
1844	1
1882	1
1885	1
1892	1
2399	1
1855	1
1901	1
1910	1
1906	1
1940	1
1862	1
1913	1
1919	1
1904	1
1943	1
1946	1
1948	1
1954	1
1957	1
1926	1
1962	1
1966	1
1969	1
1972	1
1975	1
1979	1
1982	1
1985	1
1988	1
1991	1
1994	1
1997	1
2003	1
2011	1
2014	1
2017	1
2021	1
2024	1
2096	1
2030	1
2035	1
2037	1
2040	1
2044	1
2047	1
2049	1
2052	1
2406	1
2058	1
2062	1
2067	1
2070	1
2073	1
2079	1
2081	1
2084	1
2086	1
2090	1
2093	1
2104	1
2107	1
2110	1
2152	1
2121	1
2124	1
2115	1
2131	1
2135	1
2137	1
2140	1
2143	1
2148	1
2154	1
2156	1
2159	1
2162	1
2165	1
2168	1
2170	1
2172	1
2177	1
2182	1
2185	1
2188	1
2194	1
2196	1
2202	1
2205	1
2129	1
2214	1
2285	1
2222	1
2224	1
2226	1
2228	1
2231	1
2266	1
2237	1
2239	1
2242	1
2244	1
2248	1
2251	1
2253	1
2259	1
2264	1
2255	1
2268	1
2270	1
2273	1
2276	1
2278	1
2257	1
2280	1
2287	1
2289	1
2291	1
2293	1
2296	1
2299	1
2304	1
2308	1
2311	1
2313	1
2323	1
2331	1
2320	1
2355	1
2315	1
2317	1
2337	1
2340	1
2342	1
2345	1
2348	1
2352	1
2358	1
2363	1
2375	1
2369	1
2372	1
2380	1
2386	1
2388	1
2391	1
2395	1
2326	1
2404	1
2418	1
2421	1
2424	1
2426	1
2428	1
2430	1
2432	1
2413	1
2443	1
2445	1
2447	1
2449	1
2451	1
2458	1
2461	1
2464	1
2466	1
2468	1
2472	1
2474	1
2477	1
2480	1
2484	1
2486	1
2488	1
2501	1
2596	1
2499	1
2439	1
2496	1
2535	1
2510	1
2512	1
2514	1
2516	1
2522	1
2525	1
2527	1
2530	1
2533	1
2541	1
2543	1
2549	1
2551	1
2553	1
2538	1
2560	1
2562	1
2564	1
2566	1
2569	1
2572	1
2575	1
2577	1
2580	1
2586	1
2592	1
2508	1
2594	1
2610	1
2604	1
2616	1
2622	1
2626	1
2628	1
2631	1
2634	1
2637	1
2640	1
2646	1
2649	1
2652	1
2655	1
2658	1
2607	1
2667	1
2700	1
2677	1
2680	1
2683	1
2685	1
2687	1
2690	1
2692	1
2695	1
2698	1
2703	1
2613	1
2711	1
2713	1
2715	1
2784	1
2724	1
2736	1
2738	1
2741	1
2744	1
2746	1
2749	1
2752	1
2755	1
2873	1
2776	1
2763	1
2767	1
2773	1
2782	1
2760	1
2786	1
2794	1
2796	1
2802	1
2804	1
2788	1
2792	1
2810	1
2821	1
2815	1
2826	1
2830	1
2832	1
2838	1
2842	1
2734	1
2728	1
2845	1
2847	1
2850	1
2852	1
2823	1
2818	1
2863	1
2867	1
2870	1
2876	1
2879	1
2882	1
2885	1
2892	1
2895	1
2898	1
2901	1
2904	1
2907	1
2910	1
2913	1
2858	1
2919	1
2922	1
2988	1
2932	1
2935	1
2938	1
2941	1
2944	1
2947	1
2950	1
2953	1
2956	1
2959	1
3078	1
2962	1
2965	1
2968	1
2971	1
2974	1
2977	1
2980	1
2983	1
3001	1
2990	1
2995	1
2998	1
3003	1
3008	1
3011	1
3014	1
3017	1
3019	1
3022	1
3024	1
2929	1
3038	1
3137	1
3044	1
3047	1
3049	1
3052	1
3055	1
3041	1
3061	1
3063	1
3116	1
3069	1
3075	1
3081	1
3086	1
3096	1
3099	1
3101	1
3106	1
3108	1
3111	1
3119	1
3122	1
3125	1
3131	1
3134	1
3059	1
3083	1
3144	1
3147	1
3149	1
3152	1
3155	1
3161	1
3163	1
3166	1
3170	1
3172	1
3205	1
3177	1
3180	1
3186	1
3191	1
3193	1
3195	1
3197	1
3782	1
3200	1
3208	1
3211	1
3213	1
3219	1
3222	1
3224	1
3229	1
3231	1
3175	1
3237	1
3189	1
3245	1
3247	1
3254	1
3256	1
3258	1
3262	1
3265	1
3274	1
3281	1
3283	1
3286	1
3289	1
3293	1
3299	1
3303	1
3305	1
3323	1
3325	1
3291	1
3336	1
3338	1
3341	1
3344	1
3347	1
3350	1
3359	1
3362	1
3367	1
3371	1
3375	1
3378	1
3381	1
3384	1
3386	1
3389	1
3391	1
3393	1
3451	1
3404	1
3407	1
3409	1
3413	1
3430	1
3422	1
3426	1
3435	1
3441	1
3444	1
3446	1
3448	1
3420	1
3456	1
3458	1
3396	1
3463	1
3466	1
3469	1
3472	1
3475	1
3483	1
3485	1
3493	1
3495	1
3522	1
3502	1
3506	1
3509	1
3514	1
3525	1
3527	1
3529	1
3531	1
3537	1
3540	1
3543	1
3546	1
3548	1
3550	1
3552	1
3554	1
3556	1
3558	1
3562	1
3564	1
3566	1
3568	1
3576	1
3578	1
3584	1
3586	1
3588	1
3590	1
3592	1
3594	1
3596	1
3598	1
3600	1
3602	1
3604	1
3606	1
3608	1
3612	1
3614	1
3616	1
3618	1
3620	1
3622	1
3624	1
3626	1
3628	1
3632	1
3634	1
3638	1
3641	1
3581	1
3647	1
3649	1
3652	1
3643	1
3659	1
3661	1
3666	1
3668	1
3676	1
3679	1
3682	1
3685	1
3691	1
3694	1
3697	1
3688	1
3705	1
3707	1
3710	1
3712	1
3714	1
3716	1
3718	1
3720	1
3724	1
3671	1
3730	1
3733	1
3737	1
3739	1
3741	1
3743	1
3746	1
3750	1
3753	1
3756	1
3758	1
3760	1
3762	1
3727	1
3766	1
3768	1
3770	1
3772	1
3774	1
3776	1
3778	1
3784	1
3786	1
3790	1
3792	1
3794	1
3796	1
3798	1
3800	1
3802	1
3804	1
3806	1
3808	1
3810	1
3813	1
3816	1
3819	1
3821	1
3823	1
3825	1
3827	1
3829	1
3831	1
3833	1
3835	1
3839	1
3841	1
3843	1
3849	1
3846	1
3855	1
3857	1
3859	1
3861	1
3863	1
3867	1
3869	1
3871	1
3873	1
4067	1
3875	1
3877	1
3879	1
3881	1
3883	1
3886	1
3888	1
3895	1
3898	1
3901	1
3903	1
3907	1
3909	1
3911	1
3913	1
3915	1
3917	1
3921	1
3923	1
3925	1
3927	1
3930	1
3932	1
3936	1
3890	1
3892	1
3940	1
3942	1
3944	1
3947	1
3952	1
3955	1
3957	1
3960	1
3962	1
3965	1
3970	1
3968	1
3978	1
3981	1
3972	1
3988	1
3990	1
3992	1
3996	1
3998	1
4002	1
4004	1
4006	1
4008	1
4010	1
5570	1
4012	1
4016	1
4018	1
4020	1
4022	1
4024	1
4026	1
3984	1
4031	1
4037	1
4040	1
4042	1
4044	1
4046	1
4048	1
4050	1
4052	1
4054	1
4056	1
4058	1
4060	1
4062	1
4070	1
4073	1
4079	1
4082	1
4084	1
4086	1
4088	1
4092	1
4095	1
4076	1
4101	1
4103	1
4105	1
4107	1
4109	1
4111	1
4113	1
4099	1
4119	1
4121	1
4123	1
4125	1
4128	1
4134	1
4136	1
4139	1
4142	1
4144	1
4146	1
4165	1
4168	1
4171	1
4173	1
4175	1
4177	1
4179	1
4181	1
4185	1
4187	1
4189	1
4191	1
4193	1
4195	1
4197	1
4199	1
4201	1
4203	1
4148	1
4207	1
4209	1
4263	1
4213	1
4215	1
4217	1
4219	1
4221	1
4223	1
4671	1
4677	1
4661	1
4663	1
4668	1
4714	1
4674	1
4690	1
4693	1
4695	1
4697	1
4699	1
4701	1
4707	1
4710	1
4712	1
4722	1
4724	1
4726	1
4728	1
4730	1
4656	1
4228	1
4231	1
4234	1
4259	1
4247	1
4251	1
4254	1
4266	1
4269	1
4274	1
4277	1
4279	1
4283	1
4287	1
4289	1
4291	1
4293	1
4298	1
4301	1
4310	1
4313	1
4317	1
4304	1
4330	1
4334	1
4337	1
4339	1
4344	1
4347	1
4243	1
4378	1
4359	1
4365	1
4367	1
4382	1
4375	1
4408	1
4432	1
4392	1
4394	1
4396	1
4398	1
4401	1
4356	1
4412	1
4415	1
4418	1
4421	1
4428	1
4435	1
4439	1
4442	1
4446	1
4450	1
4460	1
4468	1
4371	1
4475	1
4478	1
4481	1
5366	1
4486	1
4489	1
4492	1
4509	1
4497	1
4500	1
4503	1
4517	1
4533	1
4524	1
4527	1
4531	1
4520	1
4579	1
4495	1
4542	1
4583	1
4587	1
4591	1
4593	1
4596	1
4599	1
4602	1
4605	1
4607	1
4610	1
4613	1
4616	1
4619	1
4621	1
4623	1
4626	1
4628	1
4631	1
4634	1
4638	1
4641	1
4647	1
4652	1
4649	1
4644	1
4737	1
4741	1
4743	1
4746	1
4749	1
4752	1
4754	1
4760	1
4763	1
4765	1
4768	1
4774	1
4776	1
4779	1
4782	1
4785	1
4788	1
4791	1
4735	1
4758	1
4803	1
4806	1
4808	1
4813	1
4796	1
4825	1
4829	1
4821	1
4799	1
4833	1
4877	1
4840	1
4842	1
4844	1
4853	1
4892	1
4889	1
4858	1
4860	1
4863	1
4866	1
4875	1
4846	1
4880	1
4883	1
4886	1
4895	1
4900	1
4904	1
4906	1
4908	1
4912	1
4915	1
4919	1
4921	1
4923	1
4926	1
4928	1
4930	1
5009	1
5512	1
4902	1
4938	1
5012	1
4944	1
4951	1
4932	1
4961	1
4963	1
4967	1
4972	1
4975	1
4978	1
4982	1
4988	1
4991	1
4997	1
5000	1
5003	1
5016	1
5019	1
5022	1
5025	1
5028	1
5031	1
4941	1
5037	1
5040	1
5043	1
5046	1
5049	1
5078	1
5058	1
5061	1
5064	1
5069	1
5072	1
5075	1
5081	1
5084	1
5052	1
5091	1
5094	1
5097	1
5099	1
5101	1
5106	1
5055	1
5112	1
5114	1
5117	1
5088	1
5128	1
5130	1
5132	1
5125	1
5239	1
5261	1
3	1
4994	1
5134	1
5137	1
5140	1
5144	1
5222	1
5153	1
5156	1
5147	1
5149	1
5167	1
5170	1
5173	1
5179	1
5192	1
5195	1
5198	1
5205	1
5212	1
5214	1
5233	1
5159	1
5211	1
50	1
66	1
87	1
110	1
119	1
132	1
150	1
166	1
236	1
242	1
276	1
291	1
323	1
335	1
349	1
5176	1
5182	1
352	1
356	1
410	1
449	1
455	1
485	1
525	1
558	1
562	1
4732	1
630	1
632	1
634	1
636	1
638	1
640	1
642	1
645	1
647	1
649	1
661	1
674	1
723	1
745	1
761	1
765	1
788	1
796	1
848	1
864	1
870	1
893	1
913	1
962	1
964	1
981	1
997	1
284	1
1011	1
1066	1
1068	1
1089	1
1101	1
1110	1
1129	1
1135	1
1158	1
1194	1
1209	1
1217	1
1234	1
1246	1
1279	1
1286	1
1292	1
2770	1
1306	1
1330	1
1344	1
1350	1
1374	1
1396	1
1410	1
1443	1
1455	1
1471	1
1477	1
1484	1
1499	1
1514	1
1558	1
1580	1
1582	1
1652	1
1663	1
1709	1
1727	1
1736	1
1745	1
1754	1
1763	1
1774	1
5400	1
1802	1
1811	1
1575	1
1614	1
1815	1
1851	1
1889	1
1934	1
1951	1
1977	1
2000	1
2009	1
2019	1
2042	1
2076	1
2098	1
2101	1
2113	1
2118	1
2180	1
2199	1
2208	1
2211	1
2246	1
3865	1
2301	1
2306	1
2350	1
2383	1
2410	1
2455	1
2470	1
2493	1
2504	1
2519	1
2546	1
2557	1
2601	1
2619	1
2643	1
2661	1
2674	1
2719	1
3905	1
2436	1
2401	1
2758	1
1898	1
2732	1
2779	1
2807	1
2813	1
2828	1
2889	1
2916	1
2926	1
2986	1
3005	1
3028	1
3031	1
3034	1
3089	1
3128	1
3139	1
3142	1
3158	1
3183	1
4320	1
3202	1
3241	1
3919	1
2799	1
3114	1
3278	1
3296	1
3330	1
3353	1
3364	1
4131	1
3373	1
3399	1
3415	1
3461	1
3480	1
3512	1
3560	1
3610	1
3630	1
3655	1
3674	1
3699	1
3722	1
3764	1
3780	1
3788	1
3837	1
5265	1
3433	1
3356	1
3934	1
3938	1
3994	1
4014	1
4028	1
4034	1
4065	1
4090	1
4117	1
4183	1
4205	1
4211	1
4296	1
4351	1
4362	1
4471	1
4484	1
4506	1
4659	1
4687	1
4156	1
4385	1
4539	1
4325	1
4771	1
4794	1
4811	1
4704	1
4831	1
4851	1
4870	1
4897	1
4917	1
4935	1
4958	1
4969	1
5006	1
5033	1
5066	1
5103	1
5164	1
5202	1
5225	1
5264	1
5267	1
5262	1
1528	1
4838	1
5190	1
5263	1
2328	1
2361	1
2366	1
2722	1
2855	1
3534	1
3645	1
4226	1
4327	1
4390	1
4404	1
4453	1
4456	1
5161	1
5208	1
5216	1
5218	1
5228	1
3499	1
3572	1
3852	1
3950	1
4239	1
4425	1
4464	1
418	1
4683	1
2334	1
4827	1
1267	1
1282	1
1585	1
1794	1
1960	1
3234	1
3701	1
4150	1
4152	1
4154	1
4158	1
4160	1
4162	1
4545	1
4548	1
4551	1
4554	1
4557	1
4560	1
4563	1
5402	1
5404	1
4566	1
4569	1
4572	1
4575	1
5109	1
5119	1
5122	1
2283	1
227	1
420	1
423	1
447	1
543	1
546	1
700	1
702	1
704	1
706	1
708	1
940	1
949	1
953	1
957	1
959	1
975	1
1461	1
1467	1
1964	1
2027	1
2032	1
2055	1
2065	1
2191	1
2218	1
2555	1
2598	1
2664	1
2670	1
2705	1
2790	1
3066	1
3217	1
3227	1
3975	1
4307	1
4801	1
4816	1
4823	1
4849	1
4872	1
4836	1
4955	1
4947	1
4985	1
201	1
697	1
955	1
1550	1
1687	1
2583	1
2709	1
3251	1
3453	1
3489	1
2234	1
2861	1
3487	1
4513	1
3321	1
157	1
175	1
185	1
198	1
211	1
221	1
300	1
495	1
497	1
504	1
533	1
1449	1
1435	1
1923	1
3093	1
3438	1
1191	1
5345	1
1382	1
1917	1
2262	1
3072	1
3103	1
500	1
2415	1
5277	1
5282	1
4536	1
5284	1
5289	1
5291	1
5293	1
5295	1
5298	1
5299	1
5301	1
5303	1
5305	1
5308	1
5311	1
5314	1
5317	1
5319	1
5322	1
5325	1
5327	1
5332	1
5334	1
5337	1
5339	1
5348	1
5350	1
5352	1
5354	1
5356	1
5359	1
5362	1
5369	1
5371	1
5373	1
5376	1
5379	1
5381	1
5383	1
5386	1
5391	1
5394	1
5397	1
5408	1
5411	1
5414	1
5416	1
5419	1
5422	1
5425	1
5427	1
5432	1
5435	1
5438	1
5441	1
5444	1
5447	1
5449	1
5452	1
5454	1
5456	1
5458	1
5461	1
5464	1
5466	1
5468	1
5471	1
5476	1
5478	1
5481	1
5484	1
5487	1
5491	1
5493	1
5495	1
5498	1
5501	1
5504	1
5506	1
5509	1
5473	1
5515	1
5518	1
5520	1
5523	1
5526	1
5529	1
5533	1
5535	1
5536	1
5538	1
5539	1
5542	1
5545	1
5548	1
5550	1
5552	1
5555	1
5558	1
5561	1
5564	1
5573	1
5575	1
5578	1
5581	1
5583	1
5585	1
5588	1
5591	1
5594	1
5597	1
5599	1
5601	1
5604	1
5607	1
5608	1
5610	1
5612	1
5614	1
5616	1
5619	1
5622	1
5625	1
5628	1
5630	1
5633	1
5635	1
5637	1
5639	1
5641	1
5643	1
5645	1
5647	1
5648	1
5650	1
5653	1
5655	1
5658	1
5661	1
5664	1
5667	1
5670	1
5673	1
5676	1
5679	1
5682	1
2730	1
2726	1
2133	1
3664	1
1323	1
1332	1
3369	1
4000	1
5684	1
5236	1
5721	1
3270	1
4716	1
5729	1
1180	1
5726	1
4910	1
5286	1
1879	1
26	1
4856	1
4818	1
4739	1
1098	1
4718	1
5230	1
5188	1
5719	1
5185	1
5753	1
5752	1
5746	1
5755	1
5756	1
5767	2
5770	2
5774	2
5779	2
5785	2
5788	2
5800	2
5802	2
5806	2
5808	2
5812	2
5818	2
5822	2
5828	2
5835	2
5838	2
5844	2
5848	2
5851	2
5855	2
5858	2
5863	2
5866	2
5869	2
5876	2
5879	2
5882	2
5885	2
5890	2
5892	2
5898	2
5900	2
5904	2
5908	2
5924	2
5928	2
5931	2
5933	2
5935	2
5942	2
5948	2
5951	2
5953	2
5960	2
5963	2
5966	2
5969	2
5975	2
5980	2
5983	2
5986	2
5989	2
5992	2
5999	2
6003	2
6007	2
6021	2
6024	2
6031	2
6036	2
5684	2
6038	2
6040	2
6044	2
6046	2
6050	2
6056	2
6060	2
6063	2
6068	2
6072	2
6079	2
6081	2
6084	2
6089	2
6093	2
6097	2
6101	2
6105	2
6108	2
6113	2
6115	2
6119	2
6123	2
6128	2
6133	2
6145	2
6150	2
6153	2
6155	2
6157	2
6160	2
6163	2
6165	2
6168	2
6171	2
6175	2
6182	2
6192	2
6195	2
6197	2
6201	2
6204	2
6206	2
6208	2
6211	2
6213	2
6216	2
6219	2
6221	2
6224	2
6233	2
6280	2
6284	2
6294	2
6300	2
6353	2
6365	2
6368	2
6370	2
6372	2
6375	2
6378	2
6381	2
6384	2
6386	2
6391	2
6396	2
6399	2
6404	2
6406	2
6408	2
6410	2
6412	2
6414	2
6418	2
6420	2
6422	2
6426	2
6428	2
6431	2
6434	2
6440	2
6443	2
6446	2
6448	2
6451	2
6454	2
6459	2
6461	2
6465	2
6468	2
6470	2
6474	2
6476	2
6478	2
6480	2
6483	2
6485	2
6488	2
6491	2
6494	2
6497	2
6499	2
6508	2
6512	2
6517	2
6519	2
6523	2
6534	2
6540	2
6546	2
6549	2
6551	2
6556	2
6558	2
6563	2
6566	2
6575	2
6581	2
6583	2
6596	1
6603	1
6605	1
6700	1
6701	1
\.


--
-- Data for Name: schema_version; Type: TABLE DATA; Schema: public; Owner: quarasek
--

COPY public.schema_version (version_rank, installed_rank, version, description, type, script, checksum, installed_by, installed_on, execution_time, success) FROM stdin;
1	1	1	<< Flyway Baseline >>	BASELINE	<< Flyway Baseline >>	\N	quarasek	2017-06-30 11:39:15.839047	0	t
2	2	20170406.01	Platfrom	SQL	V20170406_01__Platfrom.sql	-1213310373	quarasek	2017-06-30 11:42:57.723446	227	t
3	3	20170407.01	Add case insenitiv unique constraint on platform names	SQL	V20170407_01__Add_case_insenitiv_unique_constraint_on_platform_names.sql	801632632	quarasek	2017-06-30 11:42:57.997012	3	t
4	4	20170606.01	UniqueConstraints	SQL	V20170606_01__UniqueConstraints.sql	-1484828825	quarasek	2017-06-30 11:42:58.008174	18	t
5	5	20170708.01	AddNeedReview	SQL	V20170708_01__AddNeedReview.sql	-245232207	quarasek	2017-06-30 11:42:58.034148	30	t
6	6	20170708.02	RemoveEmptyJournals	SQL	V20170708_02__RemoveEmptyJournals.sql	-661523782	quarasek	2017-06-30 11:42:58.071842	435	t
7	7	20170708.03	DefaultValueForYear	SQL	V20170708_03__DefaultValueForYear.sql	-1063898579	quarasek	2017-06-30 11:42:58.515114	23	t
8	8	20170708.04	AddISSNToJournal	SQL	V20170708_04__AddISSNToJournal.sql	-1103816607	quarasek	2017-06-30 11:42:58.545794	1	t
9	9	20190130.01	AddPlatformFloraMV	SQL	V20190130_01__AddPlatformFloraMV.sql	-1580847664	quarasek	2019-01-30 20:42:56.397231	164	t
10	10	20190206.01	AddExternalRefs	SQL	V20190206_01__AddExternalRefs.sql	1998596016	quarasek	2019-02-06 16:43:02.473473	2556	t
\.


--
-- Name: author_pkey; Type: CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.author
    ADD CONSTRAINT author_pkey PRIMARY KEY (id);


--
-- Name: database_pkey; Type: CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.database
    ADD CONSTRAINT database_pkey PRIMARY KEY (id);


--
-- Name: external_reference_pkey; Type: CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.external_reference
    ADD CONSTRAINT external_reference_pkey PRIMARY KEY (id);


--
-- Name: institution_pkey; Type: CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.institution
    ADD CONSTRAINT institution_pkey PRIMARY KEY (id);


--
-- Name: journal_pkey; Type: CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.journal
    ADD CONSTRAINT journal_pkey PRIMARY KEY (id);


--
-- Name: keyword_pkey; Type: CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.keyword
    ADD CONSTRAINT keyword_pkey PRIMARY KEY (id);


--
-- Name: pdf_link_pkey; Type: CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.pdf_link
    ADD CONSTRAINT pdf_link_pkey PRIMARY KEY (id);


--
-- Name: platform_name_key; Type: CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.platform
    ADD CONSTRAINT platform_name_key UNIQUE (name);


--
-- Name: platform_pkey; Type: CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.platform
    ADD CONSTRAINT platform_pkey PRIMARY KEY (id);


--
-- Name: publication_authors_pkey; Type: CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.publication_authors
    ADD CONSTRAINT publication_authors_pkey PRIMARY KEY (publications_id, authors_key);


--
-- Name: publication_cite_id_key; Type: CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.publication
    ADD CONSTRAINT publication_cite_id_key UNIQUE (cite_id);


--
-- Name: publication_pkey; Type: CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.publication
    ADD CONSTRAINT publication_pkey PRIMARY KEY (id);


--
-- Name: publication_platforms_pkey; Type: CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.publication_platforms
    ADD CONSTRAINT publication_platforms_pkey PRIMARY KEY (publication_id, platforms_id);


--
-- Name: schema_version_pk; Type: CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.schema_version
    ADD CONSTRAINT schema_version_pk PRIMARY KEY (version);


--
-- Name: uk_ib29vdbakldqx78qlgx7wjl2n; Type: CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.publication_files
    ADD CONSTRAINT uk_ib29vdbakldqx78qlgx7wjl2n UNIQUE (files_id);


--
-- Name: database_name_key; Type: INDEX; Schema: public; Owner: quarasek
--

CREATE UNIQUE INDEX database_name_key ON public.database USING btree (name);


--
-- Name: ix_publication_cite_id; Type: INDEX; Schema: public; Owner: quarasek
--

CREATE INDEX ix_publication_cite_id ON public.publication USING btree (cite_id);


--
-- Name: keyword_keyword_key; Type: INDEX; Schema: public; Owner: quarasek
--

CREATE UNIQUE INDEX keyword_keyword_key ON public.keyword USING btree (keyword);


--
-- Name: pdf_link_url_key; Type: INDEX; Schema: public; Owner: quarasek
--

CREATE UNIQUE INDEX pdf_link_url_key ON public.pdf_link USING btree (url);


--
-- Name: platfrom_name_lower_key; Type: INDEX; Schema: public; Owner: quarasek
--

CREATE UNIQUE INDEX platfrom_name_lower_key ON public.platform USING btree (lower((name)::text));


--
-- Name: publication_doi_key; Type: INDEX; Schema: public; Owner: quarasek
--

CREATE UNIQUE INDEX publication_doi_key ON public.publication USING btree (doi) WHERE (doi IS NOT NULL);


--
-- Name: schema_version_ir_idx; Type: INDEX; Schema: public; Owner: quarasek
--

CREATE INDEX schema_version_ir_idx ON public.schema_version USING btree (installed_rank);


--
-- Name: schema_version_s_idx; Type: INDEX; Schema: public; Owner: quarasek
--

CREATE INDEX schema_version_s_idx ON public.schema_version USING btree (success);


--
-- Name: schema_version_vr_idx; Type: INDEX; Schema: public; Owner: quarasek
--

CREATE INDEX schema_version_vr_idx ON public.schema_version USING btree (version_rank);


--
-- Name: external_reference_publication_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.external_reference
    ADD CONSTRAINT external_reference_publication_id_fkey FOREIGN KEY (publication_id) REFERENCES public.publication(id);


--
-- Name: fk_1jdr989sx7f6h8dv8j5c90qpv; Type: FK CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.publication_files
    ADD CONSTRAINT fk_1jdr989sx7f6h8dv8j5c90qpv FOREIGN KEY (publication_id) REFERENCES public.publication(id);


--
-- Name: fk_3noxntkljainybfx4xw9mdg9l; Type: FK CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.publication
    ADD CONSTRAINT fk_3noxntkljainybfx4xw9mdg9l FOREIGN KEY (institution_id) REFERENCES public.institution(id);


--
-- Name: fk_45imx5mo2sqbjxvmtndg9j5ey; Type: FK CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.publication_keywords
    ADD CONSTRAINT fk_45imx5mo2sqbjxvmtndg9j5ey FOREIGN KEY (keywords_id) REFERENCES public.keyword(id);


--
-- Name: fk_997cpu12no8bilukk9rw7hpbn; Type: FK CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.publication_authors
    ADD CONSTRAINT fk_997cpu12no8bilukk9rw7hpbn FOREIGN KEY (publications_id) REFERENCES public.publication(id);


--
-- Name: fk_9s9a8c8hv8w3gd89n39kb2bll; Type: FK CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.publication_authors
    ADD CONSTRAINT fk_9s9a8c8hv8w3gd89n39kb2bll FOREIGN KEY (authors_id) REFERENCES public.author(id);


--
-- Name: fk_hptdyuucepu4hej00n222n5f5; Type: FK CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.publication
    ADD CONSTRAINT fk_hptdyuucepu4hej00n222n5f5 FOREIGN KEY (journal_id) REFERENCES public.journal(id);


--
-- Name: fk_hu7oer2bvmhstf8lhwtl3iyqi; Type: FK CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.keyword_publication
    ADD CONSTRAINT fk_hu7oer2bvmhstf8lhwtl3iyqi FOREIGN KEY (publication_id) REFERENCES public.publication(id);


--
-- Name: fk_ib29vdbakldqx78qlgx7wjl2n; Type: FK CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.publication_files
    ADD CONSTRAINT fk_ib29vdbakldqx78qlgx7wjl2n FOREIGN KEY (files_id) REFERENCES public.pdf_link(id);


--
-- Name: fk_r5p5f606efhves77vmdjym45p; Type: FK CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.publication_keywords
    ADD CONSTRAINT fk_r5p5f606efhves77vmdjym45p FOREIGN KEY (publication_id) REFERENCES public.publication(id);


--
-- Name: fk_ss1p3acwfm5sma1l9niuksgnr; Type: FK CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.keyword_publication
    ADD CONSTRAINT fk_ss1p3acwfm5sma1l9niuksgnr FOREIGN KEY (keyword_id) REFERENCES public.keyword(id);


--
-- Name: publication_platforms_platforms_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.publication_platforms
    ADD CONSTRAINT publication_platforms_platforms_id_fkey FOREIGN KEY (platforms_id) REFERENCES public.platform(id);


--
-- Name: publication_platforms_publication_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: quarasek
--

ALTER TABLE ONLY public.publication_platforms
    ADD CONSTRAINT publication_platforms_publication_id_fkey FOREIGN KEY (publication_id) REFERENCES public.publication(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: TABLE database; Type: ACL; Schema: public; Owner: quarasek
--

REVOKE ALL ON TABLE public.database FROM PUBLIC;
REVOKE ALL ON TABLE public.database FROM quarasek;
GRANT ALL ON TABLE public.database TO quarasek;
GRANT ALL ON TABLE public.database TO floradb;


--
-- Name: TABLE publication_databases; Type: ACL; Schema: public; Owner: quarasek
--

REVOKE ALL ON TABLE public.publication_databases FROM PUBLIC;
REVOKE ALL ON TABLE public.publication_databases FROM quarasek;
GRANT ALL ON TABLE public.publication_databases TO quarasek;
GRANT ALL ON TABLE public.publication_databases TO floradb;


--
-- PostgreSQL database dump complete
--

